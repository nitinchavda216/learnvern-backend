<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'api' => [
        'secret_key' => env('API_SECRET_KEY', 'xCHaftG3rx4ZMHf6VpTsZWr4vTCUXr6P'),
    ],
    'razorpay' => [
        'key' => env('RAZOR_KEY', 'rzp_test_v7SJ034pg7z00H'),
        'secret' => env('RAZOR_SECRET', 'srt5LYrtOZKtaXWP8j9XcDz3')
    ],
    'bolt' => [
        'merchant_key' => env('BOLT_MERCHANT_KEY', "mxR32BLh"),
        'merchant_salt' => env('BOLT_MERCHANT_SALT', "Wjn8IgeZzw"),
        'header' => env('BOLT_HEADER', "ePPWG1rhTgyemFQhLJgbMpUqs0jtjHazzSg6b1sNAdM="),
        'script_url' => env('BOLT_SCRIPT_URL', "https://checkout-static.citruspay.com/bolt/run/bolt.min.js"),
        'logo' => env('BOLT_LOGO', "https://www.learnvern.com/images/logo-blue.png"),
        'modal_color' => env('BOLT_MODAL_COLOR', "03386F"),
    ]
];
