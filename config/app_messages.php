<?php

return
    [
        'blocked_user' => [
            'title' => 'Notice',
            'description' => 'Your account has been deactivated by the administrator. Please contact our customer support team.',
        ],
        'account_not_activated' => [
            'title' => 'Action Required',
            'description' => 'Your account exists but it is not activated. You must activate your account before proceeding. We have sent the activation email to your email which you used at the time of registration.',
        ],
        'incorrect_password' => [
            'title' => 'Action Required',
            'description' => 'It seems that you have entered incorrect password. Please try again with Valid Credentials or Login with Gmail.',
        ],
        'sign_up_success' => [
            'title' => 'Action Required',
            'description' => 'You have registered successfully. Please verify your email-address by clicking on the activation link.',
        ],
        'sign_in' => [
            'invalid_credentials' => [
                'title' => 'Invalid Credentials',
                'description' => 'You entered wrong username or password.',
            ],
            'account_not_exist' => [
                'title' => 'Invalid Credentials',
                'description' => 'No Account Found with this email address.',
            ],
        ],
        'social_auth_error' => [
            'title' => 'Error',
            'description' => 'Can not get email address.',
        ],
        'forgot_password' => [
            'account_not_exist' => [
                'title' => 'Error',
                'description' => 'User Not Exist!',
            ],
            'success' => [
                'title' => 'Action Required',
                'description' => 'Reset password link sent to your email address. Follow email instructions and reset your password.',
            ],
            'empty_email' => [
                'title' => 'Error',
                'description' => "Email address is empty",
            ],
        ],
        'contact_form_submit' => [
            'title' => 'Success',
            'description' => 'We have received your contact request successfully.',
        ],
        'update_profile' => [
            'success' => [
                'title' => 'Success',
                'description' => 'User Profile Updated Successfully',
            ],
            'unique_number_error' => [
                'title' => 'Error',
                'description' => 'The mobile number has already been taken.',
            ],
            'birth_year_error' => [
                'title' => 'Error',
                'description' => "Your age must be over 10 years.",
            ],
        ],
        'send_otp' => [
            'success' => [
                'title' => 'Success',
                'description' => 'OTP Send Successfully.',
            ],
            'error' => [
                'title' => 'Error',
                'description' => "Invalid Mobile Number.",
            ],
        ],
        'certificate_verification' => [
            'empty_record' => [
                'title' => 'Success',
                'description' => 'No Record Found.',
            ]
        ],
    ];
