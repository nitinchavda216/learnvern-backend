<?php

return [
    'activation_link' => [
        'NAME' => 'Name of the User',
        'ACTIVATION_URL' => 'Activation URL Link.',
        'DATE_TIME' => "Date and Time of Request"
    ],
    'partner_request_approve' => [
        'NAME' => 'Name of the User',
        'STATUS' => 'Application Status'
    ],
    'reset_password' => [
        'NAME' => 'Name of the User',
        'RESET_URL' => 'Reset password link URL.',
        'DATE_TIME' => "Date and Time of Request"
    ],
    'admin_set_password_mail' => [
        'NAME' => 'Name of the User',
        'PASSWORD_LINK' => 'Set Password Link URL.',
    ],
    'contact_form_submission' => [
        'NAME' => 'Name of the User',
        'EMAIL' => 'Email Address',
        'PHONE' => 'Phone Number',
        'MESSAGE' => 'Custom Message entered by the user',
    ],
    'payment_conformation_mail' => [
        'NAME' => 'Name of the User',
        'AMOUNT' => 'Amount of Transaction',
        'COURSE_NAME' => 'Name of the Course',
        'CERTIFICATE_NAME' => 'Name as appearing on the certificate',
    ],
    'ambassador_process_email' => [
        'NAME' => 'Name of the User',
    ],
    'assign_ambassador_badge' => [
        'NAME' => 'Name of the User',
        'BADGE_TITLE' => 'Referral Badge Title',
        'REFERRED_COUNT' => 'Referred count',
    ],
    'payment_cancelled_alert' => [
        'NAME' => 'Name of the User',
        'COURSE_NAME' => 'Course Name',
        'EMAIL' => 'Email of the User',
        'MOBILE_NUMBER' => 'Mobile Number of the User',
        'AMOUNT' => 'Course price',
        'DATE' => 'Date of payment cancelled',
    ],
    'new_course_create_mail'=>[
        'COURSE_NAME'=>'Course Name',
        'CATEGORY'=>'Category',
        'LANGUAGE'=>'Language',
        'PRICE'=>'Price',
        'COURSE_TYPE'=>'Course Type',
        'TINY_DESCRIPTION'=>'Tiny Description',
        'BRIEF_DESCRIPTION'=>'Brief Description',
        'VIMEO_VIDEOLINK'=>'Vimeo Video Link',
        'COURSE_CONTENT'=>'Course Content',
        'PRE_REQUISITES'=>'Pre-Requisites',
        'HOW_CAN_LEARNVERN_HELP'=>'How Can Learnvern Help',
        'PRE_COURSES'=>'Pre Courses',
        'RELATES_COURSES'=>'Relates Courses'
    ],
    'partner_request_submitted' => [
        'NAME' => 'Name of the User',
        'EMAIL' => 'Partner Contact Email Address',
        'MOBILE_NUMBER' => 'Partner Mobile Number',
        'WEBSITE' => 'Partner Website Address',
        'CURRENT_STUDENT_COUNT'=>'Current Count of Students',
        'ADDRESS_LINE1' => 'Address Line 1',
        'ADDRESS_LINE2' => 'Address Line 2',
        'CITY' => 'City',
        'STATE' => 'State',
        'COUNTRY' => 'Country',
        'ZIP_CODE' => 'Zip Code',
        'BRANCH_ADDRESS_LINE1' => 'Branch Address Line 1',
        'BRANCH_ADDRESS_LINE2' => 'Branch Address Line 2',
        'BRANCH_CITY' => 'Branch City',
        'BRANCH_STATE' => 'Branch State',
        'BRANCH_COUNTRY' => 'Branch Country',
        'BRANCH_ZIP_CODE' => 'Branch Zip Code',
    ],
    'partner_request_approved' => [
        'NAME' => 'Name of the Partner',
        'ACTIVATION_LINK' => 'Activation Link for the Partner'
    ],
    'partner_request_rejected' => [
        'NAME' => 'Name of the Partner'
    ],
    'partner_configuration_completed' => [
        'NAME' => 'Name of the User',
        'BUSINESS_NAME' => 'Business Name',
        'EMAIL' => 'Email of the User',
        'DOMAIN' => 'Domain of the website'
    ],
    'partner_payment_successful' => [
        'NAME' => 'Name of the User',
        'BUSINESS_NAME' => 'Business Name',
        'EMAIL' => 'Email of the User',
    ],
    'partner_payment_successful_to_partner' => [
        'NAME' => 'Name of the User',
        'BUSINESS_NAME' => 'Business Name',
        'EMAIL' => 'Email of the User',
    ],
    'exit_popup' => [
        'PHONE' => 'User Phone Number',
        'CITY' => 'User City',
        'STATE' => 'User State',
        'COUNTRY' => 'User Country',
        'LINK_ACCESS' => 'User Visited URL'
    ],
    'daily_statistics' => [
        'DATE' => 'Statistics of date'
    ],
    'update_course_slug_mail' => [
        'ADMIN_NAME' => 'Admin User Name',
        'COURSE_NAME' => 'Course Name',
        'OLD_SLUG_URL' => 'Course old slug url',
        'NEW_SLUG_URL' => 'Course new slug url',
    ],
    'update_unit_slug_mail' => [
        'ADMIN_NAME' => 'Admin User Name',
        'COURSE_NAME' => 'Course Name',
        'UNIT_NAME' => 'Unit Name',
        'OLD_SLUG_URL' => 'Unit old slug url',
        'NEW_SLUG_URL' => 'Unit new slug url',
    ]
];

