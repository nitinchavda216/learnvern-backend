<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseProgressLogByDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_progress_log_by_day', function (Blueprint $table) {
            $table->id();
            $table->integer('course_user_id');
            $table->integer('course_id');
            $table->integer('user_id');
            $table->double('progress')->nullable();
            $table->string('type', 20)->nullable();
            $table->integer('curriculum_list_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_progress_log_by_day');
    }
}
