<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeIsManuallyMarkCompletedColRelatedChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unit_user', function (Blueprint $table) {
            $table->dropColumn('is_manually_mark_completed');
        });
        Schema::table('assignment_user', function (Blueprint $table) {
            $table->dropColumn('is_manually_mark_completed');
        });
        Schema::table('unit_user', function (Blueprint $table) {
            $table->boolean('is_manually_mark_completed')->default(0)->after('is_completed');
        });
        Schema::table('assignment_user', function (Blueprint $table) {
            $table->boolean('is_manually_mark_completed')->default(0)->after('is_completed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
