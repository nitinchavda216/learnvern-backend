<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToAffiliatedCallingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_users_calling', function (Blueprint $table) {
            $table->boolean('is_affiliated')->default(0)->after('first_call_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_users_calling', function (Blueprint $table) {
            $table->dropColumn('is_affiliated');
        });
    }
}
