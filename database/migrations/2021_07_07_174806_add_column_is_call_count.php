<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIsCallCount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_users_calling', function (Blueprint $table) {
            $table->boolean('is_call_consider')->default(1)->after('is_affiliated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_users_calling', function (Blueprint $table) {
            $table->dropColumn('is_call_consider');
        });
    }
}
