<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRefDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_ref_details', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('referral_code')->nullable();
            $table->integer('facebook_count')->nullable();
            $table->integer('twitter_count')->nullable();
            $table->integer('whatsapp_count')->nullable();
            $table->integer('linkedin_count')->nullable();
            $table->integer('share_count')->nullable();
            $table->integer('copy_count')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_ref_details');
    }
}
