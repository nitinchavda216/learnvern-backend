<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOtherCollegeNameIntoUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('college', 'college_id');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->integer('college_id')->nullable()->change();
            $table->string('other_college_name')->after('college_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('college_id', 'college');
            $table->dropColumn('other_college_name');
        });
    }
}
