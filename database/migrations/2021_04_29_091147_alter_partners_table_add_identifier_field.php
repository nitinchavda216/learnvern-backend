<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPartnersTableAddIdentifierField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partners', function (Blueprint $table) {
            $table->string('request_id')->after("mail_from_name")->nullable();
            $table->boolean('allow_all_courses')->after("allow_course_enrollment")->default(0);
            $table->boolean('allow_all_future_courses')->after("allow_all_courses")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
