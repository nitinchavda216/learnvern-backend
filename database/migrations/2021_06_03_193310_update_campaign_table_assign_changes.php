<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCampaignTableAssignChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign', function (Blueprint $table) {
                $table->dropColumn('assign_to');
                $table->dropColumn('accessible_to');
        });

        Schema::table('campaign', function (Blueprint $table) {
                $table->boolean('is_allow_duplicate')->nullable()->after('complete_date');
                $table->integer('created_by')->default(1)->after('is_allow_duplicate');
        });


        Schema::create('campaign_assign', function (Blueprint $table) {
            $table->id();
            $table->integer('campaign_id')->nullable();
            $table->integer('assign_to')->nullable();
            $table->integer('number_of_user')->nullable();
            $table->timestamps();
        });


        Schema::table('campaign_users_calling', function (Blueprint $table) {
                $table->integer('assign_to')->nullable()->after('calling_action');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('campaign', function (Blueprint $table) {
//            $table->dropColumn('created_by');
//            $table->integer('assign_to')->nullable()->after('id');
//            $table->string('accessible_to')->nullable()->after('assign_to');
//
//        });
//        Schema::dropIfExists('campaign_assign');
    }
}
