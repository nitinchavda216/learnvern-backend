<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAdminModulesAndRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_roles', function (Blueprint $table) {
            $table->dropColumn('default_redirect_route');
            $table->boolean('is_crm_manager')->default(0)->after('is_super_admin');
            $table->boolean('is_partner_admin')->default(0)->after('is_crm_manager');
            $table->enum('user_type',['lms','crm','super_admin'])->nullable()->after('is_partner_admin');
        });

        Schema::table('admin_modules', function (Blueprint $table) {
            $table->enum('accessible_for_user_type',['lms','crm','both'])->nullable()->after('parent_module');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
