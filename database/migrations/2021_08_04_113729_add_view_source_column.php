<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddViewSourceColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unit_user', function (Blueprint $table) {
            $table->enum('view_source', [1, 2, 3])->default(1)->after('is_manually_mark_completed');
        });
        Schema::table('assignment_user', function (Blueprint $table) {
            $table->enum('view_source', [1, 2, 3])->default(1)->after('is_manually_mark_completed');
        });
        Schema::table('quiz_user', function (Blueprint $table) {
            $table->enum('view_source', [1, 2, 3])->default(1)->after('is_completed');
        });
        Schema::table('question_answer_user', function (Blueprint $table) {
            $table->enum('view_source', [1, 2, 3])->default(1)->after('is_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unit_user', function (Blueprint $table) {
            $table->dropColumn('view_source');
        });
        Schema::table('assignment_user', function (Blueprint $table) {
            $table->dropColumn('view_source');
        });
        Schema::table('quiz_user', function (Blueprint $table) {
            $table->dropColumn('view_source');
        });
        Schema::table('question_answer_user', function (Blueprint $table) {
            $table->dropColumn('view_source');
        });
    }
}
