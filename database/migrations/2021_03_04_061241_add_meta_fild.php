<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMetaFild extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pages', function (Blueprint $table) {

            // 1. Create new column
            // You probably want to make the new column nullable
            $table->longtext('schema_script')->nullable()->after('meta_keywords');
            $table->string('canonical_url')->nullable()->after('schema_script');

            // 2. Create foreign key constraints
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
