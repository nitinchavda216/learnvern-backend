<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NullableAllPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pages', function (Blueprint $table) {
        $table->string('name')->nullable()->change();
        $table->string('page_type')->nullable()->change();
        $table->string('page_title')->nullable()->change();
        $table->string('slug')->nullable()->change();
        $table->text('meta_description')->nullable()->change();
        $table->text('meta_keywords')->nullable()->change();
        $table->longtext('content')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
