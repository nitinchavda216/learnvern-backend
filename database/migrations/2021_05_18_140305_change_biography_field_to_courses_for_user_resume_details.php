<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeBiographyFieldToCoursesForUserResumeDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_resume_details', function (Blueprint $table) {
            $table->renameColumn('biography', 'courses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_resume_details', function (Blueprint $table) {
            $table->renameColumn('courses', 'biography');
        });
    }
}
