<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CampaignUsersCalling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_users_calling', function (Blueprint $table) {
            $table->id();
            $table->integer('campaign_id');
            $table->integer('campaign_user_id');
            $table->string('calling_action',30)->nullable();
            $table->integer('action_taken_by')->nullable();
            $table->date('followup_date')->nullable();
            $table->longText('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_users_calling');
    }
}
