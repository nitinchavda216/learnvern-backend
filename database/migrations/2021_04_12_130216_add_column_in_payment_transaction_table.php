<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInPaymentTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('payment_transactions', function (Blueprint $table) {
            $table->integer('payment_method')->nullable()->after('payment_source');
        });

        Schema::table('all_transactions', function (Blueprint $table) {
            $table->integer('payment_method')->nullable()->after('payment_source');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_transactions', function (Blueprint $table) {
            $table->dropColumn('payment_method');
        });

        Schema::table('all_transactions', function (Blueprint $table) {
            $table->dropColumn('payment_method');
        });
    }
}
