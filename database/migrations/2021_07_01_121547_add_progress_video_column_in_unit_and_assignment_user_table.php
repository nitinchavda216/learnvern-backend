<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProgressVideoColumnInUnitAndAssignmentUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unit_user', function (Blueprint $table) {
            $table->string('video_progress')->after('user_id')->nullable();
        });
        Schema::table('assignment_user', function (Blueprint $table) {
            $table->string('video_progress')->after('user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unit_user', function (Blueprint $table) {
            $table->dropColumn('video_progress');
        });
        Schema::table('assignment_user', function (Blueprint $table) {
            $table->dropColumn('video_progress');
        });
    }
}
