<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferralVisitorsTrackingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referral_visitors_tracking', function (Blueprint $table) {
            $table->id();
            $table->string('referral_code')->nullable();
            $table->integer('user_id');
            $table->integer('referral_link_visits')->default(0);
            $table->integer('referral_registrations')->default(0);
            $table->date('tracking_date')->nullable();
            $table->dateTime('last_tracked_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referral_visitors_tracking');
    }
}
