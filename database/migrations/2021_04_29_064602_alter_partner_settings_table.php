<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPartnerSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partners_settings', function (Blueprint $table) {
            $table->renameColumn('fees_third_party', 'third_party_partner_fees');
            $table->renameColumn('fees_content', 'content_partner_fees');
            $table->renameColumn('document_third_party', 'third_party_agreement_document');
            $table->renameColumn('document_content', 'content_partner_agreement_document');
            $table->renameColumn('revenue_content', 'content_partner_revenue_details');

        });

        Schema::table('partners_settings', function (Blueprint $table) {
            $table->string('third_party_enrollment_fees_share_value')->after("content_partner_revenue_details")->nullable();
            $table->string('third_party_enrollment_fees_share_type')->after("third_party_enrollment_fees_share_value")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
