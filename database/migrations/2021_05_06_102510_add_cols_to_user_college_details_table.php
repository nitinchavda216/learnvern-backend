<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColsToUserCollegeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_college_details', function (Blueprint $table) {
            $table->integer('from_year')->nullable()->after('name');
            $table->integer('to_year')->nullable()->after('from_year');
            $table->string('education_title')->nullable()->after('to_year');
            $table->string('result_title')->nullable()->after('education_title');
            $table->string('result')->nullable()->after('result_title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_college_details', function (Blueprint $table) {
            $table->dropColumn('from_year');
            $table->dropColumn('to_year');
            $table->dropColumn('education_title');
            $table->dropColumn('result_title');
            $table->dropColumn('result');
        });
    }
}
