<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInitialTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('admins')) {
            Schema::create('admins', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 70)->nullable();
                $table->string('email', 100)->nullable();
                $table->string('password', 200)->nullable();
                $table->boolean('status')->default(0);
                $table->string('activation_token', 70)->nullable();
                $table->dateTime('activation_date')->nullable();
                $table->rememberToken();
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('first_name', 100)->nullable();
                $table->string('last_name', 100)->nullable();
                $table->string('certificate_name', 100)->nullable();
                $table->string('username', 100)->nullable();
                $table->string('email', 100)->nullable();
                $table->string('password', 200)->nullable();
                $table->string('ci_password', 200)->nullable();
                $table->string('gender', 10)->nullable();
                $table->string('isd_code', 10)->nullable();
                $table->string('mobile_number', 20)->nullable();
                $table->integer('country_id')->nullable();
                $table->integer('state_id')->nullable();
                $table->integer('city_id')->nullable();
                $table->string('country', 50)->nullable();
                $table->string('state', 50)->nullable();
                $table->string('city', 50)->nullable();
                $table->string('pin_code', 10)->nullable();
                $table->string('salutation', 10)->nullable();
                $table->date('birth_date')->nullable();
                $table->string('address')->nullable();
                $table->string('father_name', 70)->nullable();
                $table->string('cast_category', 20)->nullable();
                $table->text('area_of_interest')->nullable();
                $table->string('current_occupation', 20)->nullable();
                $table->string('id_proof', 50)->nullable();
                $table->string('id_number', 50)->nullable();
                $table->string('education', 50)->nullable();
                $table->string('candidate_id', 100)->nullable();
                $table->string('referral', 100)->nullable();
                $table->integer('domain_signup')->nullable();
                $table->string('signup_from', 50)->nullable();
                $table->boolean('is_social_signup')->default(0)->nullable();
                $table->string('social_id', 100)->nullable();
                $table->text('image')->nullable();
                $table->integer('user_type_id')->nullable();
                $table->string('own_referral_code', 100)->nullable();
                $table->boolean('email_unsubscribe')->default(0);
                $table->boolean('ambassador_email_send_flag')->default(0);
                $table->boolean('ambassador_50_email_send_flag')->default(0);
                $table->boolean('is_active')->default(0)->nullable();
                $table->text('activation_key')->nullable();
                $table->text('forgot_password_key')->nullable();
                $table->text('device_token')->nullable();
                $table->string('last_login_ip', 50)->nullable();
                $table->dateTime('last_login_date')->nullable();
                $table->boolean('is_login')->default(0)->nullable();
                $table->dateTime('send_activation_email_date')->nullable();
                $table->integer('login_count')->default(0)->nullable();
                $table->boolean('is_from_nsdc')->default(0)->nullable();
                $table->string('nsdc_user_id')->default(0)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('abandoned_payment_reports')) {
            Schema::create('abandoned_payment_reports', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->integer('course_id');
                $table->string('trans_id')->nullable();
                $table->float('amount')->nullable();
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('api_logs')) {
            Schema::create('api_logs', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->integer('course_id')->nullable();
                $table->string('api_name', 200)->nullable();
                $table->text('parameters')->nullable();
                $table->text('response')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('assignments')) {
            Schema::create('assignments', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('course_id');
                $table->integer('section_id');
                $table->string('name', 100)->nullable();
                $table->longText('content')->nullable();
                $table->string('video')->nullable();
                $table->string('video_id', 200)->nullable();
                $table->integer('mark')->nullable();
                $table->string('video_download_url_1', 400)->nullable();
                $table->string('video_download_url_2', 400)->nullable();
                $table->string('video_download_url_3', 400)->nullable();
                $table->time('time')->nullable();
                $table->boolean('include_course_evaluation')->default(0);
                $table->boolean('is_free')->default(0);
                $table->boolean('is_active')->default(0);
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('assignment_user')) {
            Schema::create('assignment_user', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('assignment_id');
                $table->integer('user_id');
                $table->integer('course_id');
                $table->boolean('is_completed')->default(0);
                $table->dateTime('complete_time')->nullable();
                $table->dateTime('last_used_date')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('categories')) {
            Schema::create('categories', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 100);
                $table->integer('sort_order')->nullable();
                $table->boolean('is_active')->default(1);
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('cities')) {
            Schema::create('cities', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('state_id');
                $table->string('name', 50)->nullable();
            });
        }

        if (!Schema::hasTable('comments')) {
            Schema::create('comments', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('course_id');
                $table->integer('user_id');
                $table->string('title')->nullable();
                $table->longText('content')->nullable();
                $table->double('rating')->nullable();
                $table->string('author')->nullable();
                $table->boolean('is_active')->default(1);
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('countries')) {
            Schema::create('countries', function (Blueprint $table) {
                $table->increments('id');
                $table->string('country_code', 5)->nullable();
                $table->string('name', 50)->nullable();
                $table->integer('isd_code')->nullable();
            });
        }

        if (!Schema::hasTable('cron_history')) {
            Schema::create('cron_history', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 100)->nullable();
                $table->dateTime('start_time')->nullable();
                $table->dateTime('end_time')->nullable();
                $table->integer('sent_email_count')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('curriculum')) {
            Schema::create('curriculum', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('course_id');
                $table->integer('section_id');
                $table->integer('curriculum_list_id');
                $table->string('name', 150)->nullable();
                $table->string('type', 100)->nullable();
                $table->integer('sort_order')->nullable();
                $table->boolean('is_active')->default(1);
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('domains')) {
            Schema::create('domains', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 50)->nullable();
                $table->string('url', 150)->nullable();
                $table->string('logo', 150)->nullable();
                $table->boolean('is_active')->default(0);
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('payment_transactions')) {
            Schema::create('payment_transactions', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->integer('course_id')->nullable();
                $table->float('amount')->default(0);
                $table->string('transaction_id', 50)->nullable();
                $table->string('mihpayid')->nullable();
                $table->date('transaction_date')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('quiz_question_options')) {
            Schema::create('quiz_question_options', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('quiz_id');
                $table->integer('question_id');
                $table->integer('option_id');
                $table->longText('content')->nullable();
                $table->boolean('is_active')->default(1);
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('quiz_questions')) {
            Schema::create('quiz_questions', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('quiz_id');
                $table->string('title', 150)->nullable();
                $table->longText('content')->nullable();
                $table->string('answer', 50)->nullable();
                $table->string('type', 50)->nullable();
                $table->integer('mark')->nullable();
                $table->boolean('is_active')->default(1);
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('question_answer_user')) {
            Schema::create('question_answer_user', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->integer('quiz_id');
                $table->integer('question_id');
                $table->string('user_answer_id', 11);
                $table->text('answer')->nullable();
                $table->boolean('is_active')->default(1);
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('quiz')) {
            Schema::create('quiz', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('course_id');
                $table->integer('section_id');
                $table->string('name')->nullable();
                $table->string('sub_title')->nullable();
                $table->longText('content')->nullable();
                $table->text('short_content')->nullable();
                $table->time('time')->nullable();
                $table->integer('evaluate')->nullable();
                $table->integer('retake')->nullable();
                $table->boolean('is_master_quiz')->default(0);
                $table->boolean('is_active')->default(1);
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('quiz_user')) {
            Schema::create('quiz_user', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('quiz_id');
                $table->integer('user_id');
                $table->integer('user_status')->nullable();
                $table->integer('marks')->nullable();
                $table->boolean('is_completed')->default(0);
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('sections')) {
            Schema::create('sections', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('course_id');
                $table->string('name');
                $table->boolean('is_active')->default(1);
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('states')) {
            Schema::create('states', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('country_id');
                $table->string('name', 50);
            });
        }

        if (!Schema::hasTable('units')) {
            Schema::create('units', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('course_id');
                $table->integer('section_id');
                $table->string('name')->nullable();
                $table->string('slug')->nullable();
                $table->longText('content')->nullable();
                $table->text('short_content')->nullable();
                $table->time('time')->nullable();
                $table->boolean('is_free')->default(0);
                $table->string('video_id')->nullable();
                $table->string('video_url', 500)->nullable();
                $table->text('video_download_url_1', 500)->nullable();
                $table->text('video_download_url_2', 500)->nullable();
                $table->text('video_download_url_3', 500)->nullable();
                $table->string('youtube_video_id', 100)->nullable();
                $table->string('youtube_video_url', 100)->nullable();
                $table->text('video_embed_url', 500)->nullable();
                $table->boolean('is_active')->default(0);
                $table->string('meta_title')->nullable();
                $table->text('meta_description')->nullable();
                $table->text('meta_keywords')->nullable();
                $table->longText('schema_script')->nullable();
                $table->string('canonical_url')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('unit_faqs')) {
            Schema::create('unit_faqs', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('unit_id');
                $table->text('content')->nullable();
                $table->boolean('is_active')->default(0);
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('unit_user')) {
            Schema::create('unit_user', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('unit_id');
                $table->integer('user_id');
                $table->boolean('is_completed')->default(0);
                $table->dateTime('time')->nullable();
                $table->dateTime('last_used_date')->nullable();
                $table->integer('next_curriculum_id')->nullable();
                $table->string('next_curriculum_type', 50)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('courses')) {
            Schema::create('courses', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('category_id');
                $table->string('name')->nullable();
                $table->string('slug')->nullable();
                $table->string('course_language', 50)->nullable();
                $table->longText('content')->nullable();
                $table->longText('who_can_take')->nullable();
                $table->longText('pre_requisites')->nullable();
                $table->float('average_rating')->nullable();
                $table->text('intro_video')->nullable();
                $table->text('intro_video_embed')->nullable();
                $table->string('intro_youtube_video_id')->nullable();
                $table->text('intro_youtube_video')->nullable();
                $table->text('intro_youtube_video_embed')->nullable();
                $table->time('time')->nullable();
                $table->integer('num_of_student')->nullable();
                $table->integer('course_retake')->nullable();
                $table->longText('what_you_learn')->nullable();
                $table->longText('why_you_learn')->nullable();
                $table->longText('how_can_learnvern_help')->nullable();
                $table->longText('faq')->nullable();
                $table->string('image', 200)->nullable();
                $table->boolean('is_free')->default(0);
                $table->integer('course_type')->nullable();
                $table->string('badge', 20)->nullable();
                $table->boolean('unit_youtube_video_flag')->default(0);
                $table->dateTime('start_date')->nullable();
                $table->dateTime('complete_date')->nullable();
                $table->text('pre_course')->nullable();
                $table->text('related_course')->nullable();
                $table->integer('comment_count')->default(0);
                $table->boolean('is_active')->default(0);
                $table->string('meta_title')->nullable();
                $table->text('meta_description')->nullable();
                $table->text('meta_keywords')->nullable();
                $table->longText('schema_script')->nullable();
                $table->string('canonical_url')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

//        if (!Schema::hasTable('course_language_mapping')) {
//            Schema::create('course_language_mapping', function (Blueprint $table) {
//                $table->increments('id');
//                $table->integer('course_id');
//                $table->integer('course_mapping_id');
//                $table->timestamps();
//                $table->softDeletes();
//            });
//        }

        if (!Schema::hasTable('course_user')) {
            Schema::create('course_user', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('course_id');
                $table->integer('user_id');
                $table->string('certificate_id', 50)->nullable();
                $table->boolean('paid_status')->default(0);
                $table->boolean('is_from_nsdc')->default(0)->nullable();
                $table->string('nsdc_course_id')->nullable();
                $table->integer('user_status_id')->nullable();
                $table->float('progress')->nullable();
                $table->dateTime('course_enroll_date')->nullable();
                $table->date('certificate_date')->nullable();
                $table->dateTime('last_used_date')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('course_user_status')) {
            Schema::create('course_user_status', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 100);
                $table->boolean('is_active')->default(1);
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('location_pages')) {
            Schema::create('location_pages', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('course_id')->nullable();
                $table->string('name')->nullable();
                $table->string('slug')->nullable();
                $table->longText('content')->nullable();
                $table->longText('course_intro_video')->nullable();
                $table->longText('course_intro_video_embed')->nullable();
                $table->boolean('is_active')->default(1);
                $table->string('meta_title')->nullable();
                $table->text('meta_description')->nullable();
                $table->text('meta_keywords')->nullable();
                $table->longText('schema_script')->nullable();
                $table->string('canonical_url')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

//        if (!Schema::hasTable('personal_coaching_payment')) {
//            Schema::create('personal_coaching_payment', function (Blueprint $table) {
//                $table->increments('id');
//                $table->integer('user_id')->nullable();
//                $table->string('transaction_id', 50)->nullable();
//                $table->string('course_name')->nullable();
//                $table->string('paid_amount', 10)->nullable();
//                $table->string('paid_amount_currency', 10)->nullable();
//                $table->string('payment_status', 25)->nullable();
//                $table->timestamps();
//                $table->softDeletes();
//            });
//        }

//        if (!Schema::hasTable('personal_coaching_user')) {
//            Schema::create('personal_coaching_user', function (Blueprint $table) {
//                $table->increments('id');
//                $table->string('transaction_id')->nullable();
//                $table->string('user_name', 50)->nullable();
//                $table->string('user_email')->nullable();
//                $table->string('mobile_no', 20)->nullable();
//                $table->text('address')->nullable();
//                $table->string('stripe_customer_id', 50)->nullable();
//                $table->boolean('payment_status')->default(0);
//                $table->timestamps();
//                $table->softDeletes();
//            });
//        }

        if (!Schema::hasTable('share_video_tracking')) {
            Schema::create('share_video_tracking', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('unit_id');
                $table->integer('course_id');
                $table->integer('send_user_id');
                $table->integer('receiver_user_id');
                $table->integer('receiver_video_open')->default(0);
                $table->dateTime('share_time')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('pincodes')) {
            Schema::create('pincodes', function (Blueprint $table) {
                $table->increments('id');
                $table->string('pincode', 50)->nullable();
                $table->string('division_name', 100)->nullable();
                $table->string('region_name', 100)->nullable();
                $table->string('circle_name', 100)->nullable();
                $table->string('taluk', 100)->nullable();
                $table->string('district_name', 100)->nullable();
                $table->string('state_name', 100)->nullable();
            });
        }

        if (!Schema::hasTable('all_transactions')) {
            Schema::create('all_transactions', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->integer('course_id');
                $table->double('amount', 8, 2)->default(0);
                $table->string('transaction_id', 50)->nullable();
                $table->string('mihpayid', 50)->nullable();
                $table->date('transaction_date')->nullable();
                $table->string('status', 50)->nullable();
                $table->text('server_response')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
        Schema::dropIfExists('api_logs');
        Schema::dropIfExists('abandoned_payment_reports');
        Schema::dropIfExists('assignments');
        Schema::dropIfExists('assignment_user');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('cities');
        Schema::dropIfExists('comments');
        Schema::dropIfExists('countries');
        Schema::dropIfExists('courses');
        Schema::dropIfExists('course_language_mapping');
        Schema::dropIfExists('course_user');
        Schema::dropIfExists('course_user_status');
        Schema::dropIfExists('cron_history');
        Schema::dropIfExists('curriculum');
        Schema::dropIfExists('domains');
        Schema::dropIfExists('payment_transactions');
        Schema::dropIfExists('quiz_question_options');
        Schema::dropIfExists('quiz_questions');
        Schema::dropIfExists('question_answer_user');
        Schema::dropIfExists('quiz');
        Schema::dropIfExists('quiz_user');
        Schema::dropIfExists('sections');
        Schema::dropIfExists('states');
        Schema::dropIfExists('units');
        Schema::dropIfExists('unit_faqs');
        Schema::dropIfExists('unit_user');
        Schema::dropIfExists('users');
        Schema::dropIfExists('location_pages');
        Schema::dropIfExists('personal_coaching_payment');
        Schema::dropIfExists('personal_coaching_user');
        Schema::dropIfExists('share_video_tracking');
        Schema::dropIfExists('pincodes');
        Schema::dropIfExists('all_transactions');
    }
}
