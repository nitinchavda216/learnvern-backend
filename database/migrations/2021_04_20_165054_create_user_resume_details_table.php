<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserResumeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_resume_details', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->string('name', 100)->nullable();
            $table->string('title', 100)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('phone_number', 20)->nullable();
            $table->string('image')->nullable();
            $table->text('address')->nullable();
            $table->longText('biography')->nullable();
            $table->longText('summary')->nullable();
            $table->longText('hobby_details')->nullable();
            $table->longText('language_details')->nullable();
            $table->longText('education_details')->nullable();
            $table->longText('experience_details')->nullable();
            $table->longText('key_skill_details')->nullable();
            $table->longText('technical_skill_details')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->integer('resume_template_id')->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_resume_details');

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('resume_template_id');
        });
    }
}
