<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('action')->nullable();
            $table->string('assign_to')->nullable();
            $table->string('marketing_goal')->nullable();
            $table->string('accessible_to')->nullable();
            $table->text('fields')->nullable();
            $table->text('conditions')->nullable();
            $table->text('sql_query')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign');
    }
}
