<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCampaignCallingUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_users_calling', function (Blueprint $table) {
            $table->integer('course_id')->nullable()->after('user_id');
            $table->integer('parent_id')->nullable()->after('course_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_users_calling', function (Blueprint $table) {
            $table->dropColumn('course_id');
            $table->dropColumn('parent_id');
        });
    }
}
