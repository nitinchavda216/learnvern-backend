<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUsersTableChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('district', 50)->nullable()->after('state');
            $table->string('marital_status', 50)->nullable()->after('salutation');
            $table->string('mother_name', 50)->nullable()->after('father_name');
            $table->string('religion', 50)->nullable()->after('cast_category');
            $table->boolean('disability')->default(0)->after('religion');
            $table->string('disability_type', 50)->nullable()->after('disability');
            $table->string('previous_experience_sector')->nullable()->after('current_occupation');
            $table->string('no_of_months_of_experience')->nullable()->after('previous_experience_sector');
            $table->boolean('is_employed')->default(0)->after('no_of_months_of_experience');
            $table->string('employment_status', 70)->nullable()->after('is_employed');
            $table->string('employment_details')->nullable()->after('employment_status');
            $table->string('heard_about_us', 50)->nullable()->after('employment_details');
            $table->integer('referred_count')->default(0)->after('own_referral_code');
            $table->boolean('is_complete_profile')->default(0)->after('referred_count');
            $table->string('college', 50)->nullable()->after('referred_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('district');
            $table->dropColumn('marital_status');
            $table->dropColumn('mother_name');
            $table->dropColumn('religion');
            $table->dropColumn('disability');
            $table->dropColumn('disability_type');
            $table->dropColumn('previous_experience_sector');
            $table->dropColumn('no_of_months_of_experience');
            $table->dropColumn('is_employed');
            $table->dropColumn('employment_status');
            $table->dropColumn('employment_details');
            $table->dropColumn('heard_about_us');
            $table->dropColumn('referred_count');
            $table->dropColumn('is_complete_profile');
        });
    }
}
