<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebinarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webinars', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('course_language')->nullable();
            $table->longtext('content')->nullable();
            $table->decimal('average_rating')->nullable();
            $table->text('intro_video')->nullable();
            $table->text('intro_video_embed')->nullable();
            $table->string('intro_youtube_video_id')->nullable();
            $table->text('intro_youtube_video')->nullable();
            $table->text('intro_youtube_video_embed')->nullable();
            $table->time('time')->nullable();
            $table->longtext('faq')->nullable();
            $table->string('image')->nullable();
            $table->boolean('is_free')->default(0);
            $table->boolean('unit_youtube_video_flag')->default(0);
            $table->datetime('start_date')->nullable();
            $table->datetime('complete_date')->nullable();
            $table->integer('comment_count')->default(0);
            $table->integer('num_of_student')->nullable();
            $table->boolean('is_active')->default(0);
            $table->string('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->longtext('schema_script')->nullable();
            $table->string('canonical_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webinars');
    }
}
