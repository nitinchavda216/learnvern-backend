<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWidgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widgets_settings', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('page_type')->nullable();
            $table->string('page_title')->nullable();
            $table->string('image')->nullable();
            $table->string('slug')->nullable();
            $table->longtext('content')->nullable();
            $table->boolean('is_active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('widgets_settings');
    }
}
