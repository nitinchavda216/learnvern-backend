<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPartnersChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('site_configuration', function (Blueprint $table) {
            $table->boolean('is_replicable_for_partner')->default(0)->after('control_type');
        });
        Schema::table('payment_transactions', function (Blueprint $table) {
            $table->integer('user_id')->nullable()->change();
            $table->string('payment_source')->nullable()->change();
            $table->string('payment_type')->default('nsdc_sales')->after('payment_method');
        });
        Schema::table('all_transactions', function (Blueprint $table) {
            $table->integer('user_id')->nullable()->change();
            $table->integer('course_id')->nullable()->change();
            $table->string('payment_type')->default('nsdc_sales')->after('payment_method');
        });
        Schema::create('partner_revenue_fees_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('partner_id');
            $table->string('revenue_fees_type')->nullable();
            $table->integer('from_value')->nullable();
            $table->integer('to_value')->nullable();
            $table->double('revenue_share_value')->nullable();
            $table->string('revenue_share_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('site_configuration', function (Blueprint $table) {
            $table->dropColumn('is_replicable_for_partner');
        });
        Schema::table('payment_transactions', function (Blueprint $table) {
            $table->dropColumn('payment_type');
        });
        Schema::table('all_transactions', function (Blueprint $table) {
            $table->dropColumn('payment_type');
        });
        Schema::dropIfExists('partner_revenue_fees_info');
    }
}
