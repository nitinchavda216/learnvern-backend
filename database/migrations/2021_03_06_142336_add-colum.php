<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('unit_user', function (Blueprint $table) {
            $table->tinyInteger('is_manually_mark_completed')->default(1);

        });

        Schema::table('assignment_user', function (Blueprint $table) {
            $table->tinyInteger('is_manually_mark_completed')->default(1);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
