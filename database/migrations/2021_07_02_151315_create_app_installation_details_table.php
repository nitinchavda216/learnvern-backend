<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppInstallationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_installation_details', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->string('serial')->nullable();
            $table->string('manufacturer')->nullable();
            $table->string('version')->nullable();
            $table->string('platform')->nullable();
            $table->string('model')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_installation_details');
    }
}
