<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyAssignmentQuizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::table('assignments', function (Blueprint $table) {
            $table->string('slug')->nullable()->after('name');
        });

        Schema::table('quiz', function (Blueprint $table) {
            $table->string('slug')->nullable()->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assignments', function (Blueprint $table) {
            $table->dropColumn('slug');
        });

        Schema::table('quiz', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
    }
}
