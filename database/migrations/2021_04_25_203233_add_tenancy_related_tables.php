<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTenancyRelatedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('domain')->unique()->nullable();
            $table->text('description')->nullable();
            $table->integer('partner_type')->nullable();
            $table->string('website')->nullable();
            $table->string('logo_image')->nullable();
            $table->text('about_information')->nullable();
            $table->boolean('is_master_franchisee')->default(0);
            $table->integer('parent_franchisee_id')->nullable();
            $table->string('current_students_count')->nullable();

            $table->string('contact_person')->nullable();
            $table->string('contact_person_email')->nullable();
            $table->string('contact_person_phone_number')->nullable();

            $table->string('address_line1')->nullable();
            $table->string('address_line2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('pin_code')->nullable();

            $table->string('branch_address_line1')->nullable();
            $table->string('branch_address_line2')->nullable();
            $table->string('branch_city')->nullable();
            $table->string('branch_state')->nullable();
            $table->string('branch_country')->nullable();
            $table->string('branch_pin_code')->nullable();

            $table->double('setup_fees')->nullable();
            $table->boolean('setup_fees_paid_status')->default(0);
            $table->string('revenue_sharing_type')->nullable();
            $table->boolean('use_own_certificate')->nullable();
            $table->string('certificate_image')->nullable();

            $table->boolean('partner_application_status')->default(0);
            $table->boolean('allow_course_enrollment')->default(0);
            $table->boolean('redirect_for_enrollment')->default(0);
            $table->boolean('show_webinars')->default(0);

            // Email Related Settings
            $table->string('mail_method')->nullable();
            $table->string('mail_host')->nullable();
            $table->string('mail_port')->nullable();
            $table->string('mail_username')->nullable();
            $table->string('mail_password')->nullable();
            $table->string('mail_encryption')->nullable();
            $table->string('mail_from_email_address')->nullable();
            $table->string('mail_from_name')->nullable();
            $table->enum('fixed_sharing_type',['flat','percent'])->nullable();
            $table->float('fixed_sharing_value')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('partner_courses', function (Blueprint $table) {
            $table->id();
            $table->integer('partner_id')->default(1);
            $table->integer('course_id')->nullable();
            $table->boolean('is_paid_course')->default(0);
            $table->double('course_fees')->default(0);
        });


        // Modifying the current tables

        Schema::table('abandoned_payment_reports', function (Blueprint $table) {
            $table->integer('partner_id')->after('id')->default(1);
        });


        Schema::table('password_resets', function (Blueprint $table) {
            $table->integer('partner_id')->after('email')->default(1);
        });


        Schema::table('site_configuration', function (Blueprint $table) {
            $table->integer('partner_id')->after('id')->default(1);

        });

        Schema::table('course_user', function (Blueprint $table) {
            $table->integer('partner_id')->after('course_id')->default(1);
        });

        Schema::table('all_transactions', function (Blueprint $table) {
            $table->integer('partner_id')->after('course_id')->default(1);
        });

        Schema::table('email_templates', function (Blueprint $table) {
            $table->integer('partner_id')->after('id')->default(1);
        });

        Schema::table('login_history', function (Blueprint $table) {
            $table->integer('partner_id')->after('user_id')->default(1);
        });

        Schema::table('pages', function (Blueprint $table) {
            $table->integer('partner_id')->after('id')->default(1);
        });

        Schema::table('payment_transactions', function (Blueprint $table) {
            $table->integer('partner_id')->after('course_id')->default(1);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->integer('partner_id')->after('id')->default(1);
        });

        Schema::table('admins', function (Blueprint $table) {
            $table->integer('partner_id')->after('id')->default(1);
        });

        Schema::table('admin_roles', function (Blueprint $table) {
            $table->integer('partner_id')->after('id')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}