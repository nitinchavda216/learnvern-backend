<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToCampaignCallingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_users_calling', function (Blueprint $table) {
            $table->integer('top_parent_id')->after('updated_at')->nullable();
            $table->dateTime('first_call_date')->nullable()->after('top_parent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_users_calling', function (Blueprint $table) {
            $table->dropColumn('top_parent_id');
            $table->dropColumn('first_call_date');
        });
    }
}
