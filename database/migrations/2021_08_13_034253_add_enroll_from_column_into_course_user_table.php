<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEnrollFromColumnIntoCourseUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_user', function (Blueprint $table) {
            $table->dropColumn('curriculum_type');
            $table->enum('enroll_source', [1, 2, 3])->nullable()->after('is_applicable_for_free_certificate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_user', function (Blueprint $table) {
            $table->dropColumn('enroll_source');
            $table->string('curriculum_type')->nullable()->after('is_applicable_for_free_certificate');
        });
    }
}
