<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnersSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners_settings', function (Blueprint $table) {
            $table->id();
            $table->string('fees_third_party')->nullable();
            $table->string('fees_content')->nullable();
            $table->string('document_third_party')->nullable();
            $table->string('document_content')->nullable();
            $table->string('revenue_content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners_settings');
    }
}
