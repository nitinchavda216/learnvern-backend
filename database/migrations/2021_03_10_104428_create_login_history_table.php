<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoginHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('login_history', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('ip', 30)->nullable();
            $table->string('hostname', 30)->nullable();
            $table->string('country_code', 10)->nullable();
            $table->string('country_name', 30)->nullable();
            $table->string('browser')->nullable();
            $table->text('browser_details')->nullable();
            $table->text('ip_details')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('login_history');
    }
}
