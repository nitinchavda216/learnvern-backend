<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Assignment;
use App\Models\Quiz;
use App\Models\Units;
use App\Models\Sluglists;

class CreateSluglistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sluglists', function (Blueprint $table) {
            $table->id();
            $table->integer('curriculum_id');
            $table->string('curriculum_type');
            $table->string('slug')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sluglists');
    }
}
