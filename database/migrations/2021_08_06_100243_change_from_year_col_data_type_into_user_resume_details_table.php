<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFromYearColDataTypeIntoUserResumeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_college_details', function (Blueprint $table) {
            $table->string('from_year', 50)->nullable()->change();
            $table->string('to_year', 50)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_college_details', function (Blueprint $table) {
            //
        });
    }
}
