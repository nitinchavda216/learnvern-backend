<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      self::AddAdminsData();
    }

    private function AddAdminsData()
    {
        \App\Models\Admins::truncate();
        $admin = [
            'name' => 'Malay Mehta',
            'email' => 'meetmalay@gmail.com',
            'password' => '123456uuzz',
            'status' => 1,
            'role_id'=>1,
            'activation_date' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ];
        \App\Models\Admins::create($admin);
    }
}


