<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KeywordSearchAnalyticsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $datas = [
            [
                'module_keys' => 'keyword_search_analytics',
                'module_title' => 'Keyword Search Analytics',
                'icon_class' => null,
                'route_name' => 'keyword-search-analytics',
                'description' => 'Analytical report of particular keyword search and search results.',
                'parent_module' => 2,
                'accessible_for_user_type' => 'lms'
            ],
        ];

        DB::table('admin_modules')->insert($datas);
        foreach ($datas as $data) {
            $insert_data = [
                [
                    'role_id' => 1, // Access for Super admin user.
                    'module_key' => $data['module_keys']
                ],
                [
                    'role_id' => 3, // Access for LMS user 
                    'module_key' => $data['module_keys']
                ]
            ];
            DB::table('role_wise_module_accesses')->insert($insert_data);
        }
    }

}
