<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_modules')->truncate();
        $data = [
            [
                'module_keys' => 'dashboard_menu', // parent_id =1
                'module_title' => 'Dashboard',
                'icon_class' => 'typcn typcn-clipboard',
                'route_name' => 'dashboard',
                'description' => 'Dashboard',
                'parent_module' => 0,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'course_catalog_menu',  // parent_id =2
                'module_title' => 'Course Catalog',
                'icon_class' => 'typcn typcn-book',
                'route_name' => NULL,
                'description' => 'Course Catalog',
                'parent_module' => 0,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'users_menu',   // parent_id =3
                'module_title' => 'Users',
                'icon_class' => 'typcn typcn-group',
                'route_name' => NULL,
                'description' => 'Users',
                'parent_module' => 0,
                'accessible_for_user_type' => 'both'
            ],
            [
                'module_keys' => 'crm_menu', // parent_id =4
                'module_title' => 'CRM',
                'icon_class' => 'typcn typcn-clipboard',
                'route_name' => NULL,
                'description' => 'CRM Menu',
                'parent_module' => 0,
                'accessible_for_user_type' => 'crm'
            ],
            [
                'module_keys' => 'reports_menu',  // parent_id =5
                'module_title' => 'Reports',
                'icon_class' => 'typcn typcn-chart-pie',
                'route_name' => NULL,
                'description' => 'Reports',
                'parent_module' => 0,
                'accessible_for_user_type' => 'crm'
            ],
            [
                'module_keys' => 'system_menu',  // parent_id =6
                'module_title' => 'System',
                'icon_class' => 'typcn typcn-book',
                'route_name' => NULL,
                'description' => 'System',
                'parent_module' => 0,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'admin_users_menu',  // parent_id =7
                'module_title' => 'Admin Users',
                'icon_class' => 'typcn typcn-book',
                'route_name' => NULL,
                'description' => 'Admin Users Settings',
                'parent_module' => 0,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'partners_menu',  // parent_id =8
                'module_title' => 'Partners',
                'icon_class' => 'typcn typcn-book',
                'route_name' => NULL,
                'description' => 'Partner Related Settings',
                'parent_module' => 0,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'categories_page',
                'module_title' => 'Categories',
                'icon_class' => NULL,
                'route_name' => 'categories',
                'description' => 'Categories',
                'parent_module' => 2,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'course_page',
                'module_title' => 'Courses',
                'icon_class' => NULL,
                'route_name' => 'courses',
                'description' => 'Courses',
                'parent_module' => 2,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'webinars_page',
                'module_title' => 'Webinars',
                'icon_class' => NULL,
                'route_name' => 'webinars',
                'description' => 'Webinars',
                'parent_module' => 2,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'section_page',
                'module_title' => 'Sections',
                'icon_class' => NULL,
                'route_name' => 'sections',
                'description' => 'Sections',
                'parent_module' => 2,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'unit_page',
                'module_title' => 'Units',
                'icon_class' => NULL,
                'route_name' => 'units',
                'description' => 'Units',
                'parent_module' => 2,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'quiz_page',
                'module_title' => 'Quiz',
                'icon_class' => NULL,
                'route_name' => 'quiz',
                'description' => 'Quiz',
                'parent_module' => 2,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'assignment_page',
                'module_title' => 'Assignments',
                'icon_class' => NULL,
                'route_name' => 'assignments',
                'description' => 'Assignments',
                'parent_module' => 2,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'reviews_page',
                'module_title' => 'Reviews',
                'icon_class' => NULL,
                'route_name' => 'reviews',
                'description' => 'Reviews',
                'parent_module' => 2,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'faqs_page',
                'module_title' => 'Course Faqs',
                'icon_class' => NULL,
                'route_name' => 'faqs',
                'description' => 'faqs',
                'parent_module' => 2,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'users_details_page',
                'module_title' => 'Users',
                'icon_class' => NULL,
                'route_name' => 'users',
                'description' => 'Users',
                'parent_module' => 3,
                'accessible_for_user_type' => 'both'
            ],
            [
                'module_keys' => 'NSDC_users_page',
                'module_title' => 'NSDC users',
                'icon_class' => NULL,
                'route_name' => 'nsdc_candidate',
                'description' => ' NSDC users',
                'parent_module' => 3,
                'accessible_for_user_type' => 'both'
            ],
            [
                'module_keys' => 'International_users_page',
                'module_title' => 'International users',
                'icon_class' => NULL,
                'route_name' => 'international_candidates',
                'description' => ' International Users Details Page',
                'parent_module' => 3,
                'accessible_for_user_type' => 'both'
            ],

            [
                'module_keys' => 'crm_dashboard',
                'module_title' => 'CRM Dashboard',
                'icon_class' => 'typcn typcn-book',
                'route_name' => 'crm.dashboard',
                'description' => 'CRM Dashboard',
                'parent_module' => 4,
                'accessible_for_user_type' => 'crm'
            ],
            [
                'module_keys' => 'crm_user_performance',
                'module_title' => 'CRM User Performance',
                'icon_class' => 'typcn typcn-book',
                'route_name' => 'crm.user_performance',
                'description' => 'CRM User Performance',
                'parent_module' => 4,
                'accessible_for_user_type' => 'crm'
            ],
            [
                'module_keys' => 'payment_history_page',
                'module_title' => 'Payment History',
                'icon_class' => NULL,
                'route_name' => 'payment_history',
                'description' => 'Payment History',
                'parent_module' => 5,
                'accessible_for_user_type' => 'both'
            ],

            [
                'module_keys' => 'referral_point_page',
                'module_title' => 'Ambassador Report',
                'icon_class' => NULL,
                'route_name' => 'referral-points',
                'description' => 'Ambassador Report',
                'parent_module' => 5,
                'accessible_for_user_type' => 'crm'
            ],
            [
                'module_keys' => 'users_reports',
                'module_title' => 'Users Reports',
                'icon_class' => NULL,
                'route_name' => 'user-report',
                'description' => 'Users Reports',
                'parent_module' => 5,
                'accessible_for_user_type' => 'crm'
            ],
            [
                'module_keys' => 'unenrollred_user_report',
                'module_title' => 'Un-enrolled Users Reports',
                'icon_class' => NULL,
                'route_name' => 'un-enrolled-users',
                'description' => 'Un-Enrolled Users',
                'parent_module' => 5,
                'accessible_for_user_type' => 'crm'
            ],
            [
                'module_keys' => 'course_report',
                'module_title' => 'Course Reports',
                'icon_class' => NULL,
                'route_name' => 'course-report',
                'description' => 'Course Report',
                'parent_module' => 5,
                'accessible_for_user_type' => 'crm'
            ],
            [
                'module_keys' => 'course_status_report',
                'module_title' => 'Course Status Reports',
                'icon_class' => NULL,
                'route_name' => 'course-status-report',
                'description' => 'Course Status Report',
                'parent_module' => 5,
                'accessible_for_user_type' => 'crm'
            ],
            [
                'module_keys' => 'referral_report',
                'module_title' => 'Referral Reports',
                'icon_class' => NULL,
                'route_name' => 'referral-report',
                'description' => 'Referral Reports',
                'parent_module' => 5,
                'accessible_for_user_type' => 'crm'
            ],
            [
                'module_keys' => 'monthly_user_report',
                'module_title' => 'Monthly User Report',
                'icon_class' => NULL,
                'route_name' => 'user-month-vise-report',
                'description' => 'Monthly User Report',
                'parent_module' => 5,
                'accessible_for_user_type' => 'crm'
            ],
            [
                'module_keys' => 'report_builder',
                'module_title' => 'Report Builder',
                'icon_class' => NULL,
                'route_name' => 'report-builder',
                'description' => 'Report Builder',
                'parent_module' => 5,
                'accessible_for_user_type' => 'crm'
            ],
            [
                'module_keys' => 'campaign',
                'module_title' => 'Campaigns',
                'icon_class' => NULL,
                'route_name' => 'campaign',
                'description' => 'Campaigns',
                'parent_module' => 5,
                'accessible_for_user_type' => 'crm'
            ],
            [
                'module_keys' => 'analytics_dashboard',
                'module_title' => 'Analytics Dashboard',
                'icon_class' => NULL,
                'route_name' => 'analytics',
                'description' => 'Analytics Dashboard',
                'parent_module' => 5,
                'accessible_for_user_type' => 'crm'
            ],
            [
                'module_keys' => 'abandoned_payment_report',
                'module_title' => 'Abandoned Payment Report',
                'icon_class' => NULL,
                'route_name' => 'abandoned-payments',
                'description' => 'Abandoned Payment Report',
                'parent_module' => 5,
                'accessible_for_user_type' => 'crm'
            ],
            [
                'module_keys' => 'activity_log_report',
                'module_title' => 'Activity Log Report',
                'icon_class' => NULL,
                'route_name' => 'activitylogs',
                'description' => 'Activity Log Report',
                'parent_module' => 5,
                'accessible_for_user_type' => 'both'
            ],
            [
                'module_keys' => 'anonymous_user',
                'module_title' => 'Anonymous User',
                'icon_class' => NULL,
                'route_name' => 'anonymous-user',
                'description' => 'Anonymous User',
                'parent_module' => 5,
                'accessible_for_user_type' => 'both'
            ],

            [
                'module_keys' => 'pages',
                'module_title' => 'Pages',
                'icon_class' => NULL,
                'route_name' => 'pages',
                'description' => 'Pages',
                'parent_module' => 6,
                'accessible_for_user_type' => 'lms'
            ],

            [
                'module_keys' => 'content_widgets',
                'module_title' => 'Content Widgets',
                'icon_class' => NULL,
                'route_name' => 'widget-setting',
                'description' => 'Content Widgets',
                'parent_module' => 6,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'location_pages',
                'module_title' => 'Location Pages',
                'icon_class' => NULL,
                'route_name' => 'location-pages',
                'description' => 'Location Pages',
                'parent_module' => 6,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'configuration',
                'module_title' => 'Configuration',
                'icon_class' => NULL,
                'route_name' => 'configuration',
                'description' => 'Configuration',
                'parent_module' => 6,
                'accessible_for_user_type' => 'lms'
            ],

            [
                'module_keys' => 'email_templates',
                'module_title' => 'Email Templates',
                'icon_class' => NULL,
                'route_name' => 'email-templates',
                'description' => 'Email Templates',
                'parent_module' => 6,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'languages',
                'module_title' => 'Languages',
                'icon_class' => NULL,
                'route_name' => 'languages',
                'description' => 'Languages',
                'parent_module' => 6,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'ambassador_program_settings_page',
                'module_title' => 'Ambassador Program Settings',
                'icon_class' => NULL,
                'route_name' => 'ambassador-program-settings',
                'description' => 'Ambassador Program Settings',
                'parent_module' => 6,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'admin_user',
                'module_title' => 'Admin Users ',
                'icon_class' => NULL,
                'route_name' => 'admin_users',
                'description' => 'Admin Users',
                'parent_module' => 7,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'admin_role',
                'module_title' => 'Admin Users Role',
                'icon_class' => NULL,
                'route_name' => 'admin_role',
                'description' => 'Admin Users Role',
                'parent_module' => 7,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'tenancy_setting',
                'module_title' => 'Tenancy Settings',
                'icon_class' => NULL,
                'route_name' => 'tenancy-settings',
                'description' => 'Tenancy Settings',
                'parent_module' => 8,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'partner_application_request',
                'module_title' => 'Partner Application Requests',
                'icon_class' => NULL,
                'route_name' => 'partner-application-request',
                'description' => 'Partner Application Requests',
                'parent_module' => 8,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'banner',
                'module_title' => 'App Banner',
                'icon_class' => NULL,
                'route_name' => 'banner',
                'description' => 'App Banner',
                'parent_module' => 6,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'announcement_management',
                'module_title' => 'Announcement Management',
                'icon_class' => NULL,
                'route_name' => 'announcement-management',
                'description' => 'Announcement Management Details',
                'parent_module' => 6,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'app_update_management',
                'module_title' => 'App Update Management',
                'icon_class' => NULL,
                'route_name' => 'app_updates',
                'description' => 'App Update Management',
                'parent_module' => 6,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'landing_pages',
                'module_title' => 'Course Landing Pages',
                'icon_class' => NULL,
                'route_name' => 'landing-pages',
                'description' => 'App Update Management',
                'parent_module' => 2,
                'accessible_for_user_type' => 'lms'
            ],
        ];
        DB::table('admin_modules')->insert($data);

        DB::table('role_wise_module_accesses')->where('role_id', 1)->delete();
        foreach ($data as $datum) {
            DB::table('role_wise_module_accesses')->insert([
                'role_id' => 1,
                'module_key' => $datum['module_keys']
            ]);
        }
    }
}
