<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon;
use Illuminate\Support\Facades\DB;
class SiteConfigurationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('site_configuration_groups')->truncate();
        $site_configuration_groups = [
            'Site Contact Information', 'SEO Settings', 'Share Messages' , 'Other Data'
        ];
        foreach ($site_configuration_groups as $value) {
            DB::table('site_configuration_groups')->insert([
                'title' => $value,
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
            ]);
        }

        DB::table('site_configuration')->truncate();
        $site_configuration = [
            //Website Details
            'email_address' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 1,
                'title' => "Site Email Address",
                'configuration_value' => 'info@learnvern.com',
                'control_type' => 'textfield'
            ],
            'web_site' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 1,
                'title' => "Website Link",
                'configuration_value' => 'https://www.learnvern.com/',
                'control_type' => 'textfield'
            ],
            'logo' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 1,
                'title' => 'LOGO Url',
                'configuration_value' => 'https://www.learnvern.com/images/logo.png',
                'control_type' => 'textfield'
            ],
            'sample_certificate' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 1,
                'title' => 'Sample Certificate Image Url',
                'configuration_value' => 'https://www.learnvern.com/images/sample_certificate.jpg',
                'control_type' => 'textfield'
            ],
            'contact_no' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 1,
                'title' => 'Contact Number',
                'configuration_value' => '8849004643',
                'control_type' => 'textfield'
            ],
            'facebook_link' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 1,
                'title' => 'Facebook Link',
                'configuration_value' => 'https://www.facebook.com/learnvern/',
                'control_type' => 'textfield'
            ],
            'youtube_link' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 1,
                'title' => 'Youtube Link',
                'configuration_value' => 'https://www.youtube.com/channel/UCGlUPA9kZBU0fEAJbbXwT1Q',
                'control_type' => 'textfield'
            ],
            'instagram_link' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 1,
                'title' => 'Instagram Link',
                'configuration_value' => 'https://www.instagram.com/learnvernofficial/',
                'control_type' => 'textfield'
            ],
            'linkedin_link' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 1,
                'title' => 'Linkedin Link',
                'configuration_value' => 'https://www.linkedin.com/company/learnvern/',
                'control_type' => 'textfield'
            ],
            'twitter_link' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 1,
                'title' => 'Twitter Link',
                'configuration_value' => 'https://twitter.com/learnvern',
                'control_type' => 'textfield'
            ],
            'telegram_link' => array(
                'config_group_id' => 1,
                'is_replicable_for_partner' => 1,
                'title' => 'Telegram Link',
                'configuration_value' => '',
                'control_type' => 'textfield'
            ),
            'footer_content' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 1,
                'title' => 'Footer Content',
                'configuration_value' => 'LearnVern is a training portal where anyone can learn any course in vernacular languages for free.',
                'control_type' => 'textfield'
            ],
            'footer_copyrighttext' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 1,
                'title' => 'Footer Copyright Text',
                'configuration_value' => '',
                'control_type' => 'textfield'
            ],
            'guest_user_video_time_limit' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 1,
                'title' => 'Guest User Video Watch Limit (second)',
                'configuration_value' => '120',
                'control_type' => 'textfield'
            ],
            'meta_title' => [
                'config_group_id' => 2,
                'is_replicable_for_partner' => 1,
                'title' => 'Meta Title',
                'configuration_value' => 'Free Online Courses | IT Training Classes with Certificate - LearnVern',
                'control_type' => 'textfield'
            ],
            'meta_keywords' => [
                'config_group_id' => 2,
                'is_replicable_for_partner' => 1,
                'title' => 'Meta Keywords',
                'configuration_value' => '',
                'control_type' => 'textfield'
            ],
            'meta_description' => [
                'config_group_id' => 2,
                'is_replicable_for_partner' => 1,
                'title' => 'Meta Description',
                'configuration_value' => 'Looking for free online courses with certificates for IT training? LearnVern offers web development courses, including PHP, Java, C++, Android, iOS, Testing, Excel & more.',
                'control_type' => 'textfield'
            ],
            'schema' => [
                'config_group_id' => 2,
                'is_replicable_for_partner' => 1,
                'title' => 'Schema',
                'configuration_value' => '',
                'control_type' => 'textarea'
            ],
            'canonical_url' => [
                'config_group_id' => 2,
                'is_replicable_for_partner' => 1,
                'title' => 'Canonical Url',
                'configuration_value' => 'https://www.learnvern.com/',
                'control_type' => 'textfield'
            ],
            'partner_application_receiver_email' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 0,
                'title' => 'Partner application receiver',
                'configuration_value' => 'niral@learnvern.com',
                'control_type' => 'textfield'
            ],
            'general_share_message' => [
                'config_group_id' => 3,
                'is_replicable_for_partner' => 0,
                'title' => 'General Share Message',
                'configuration_value' => 'Hi I have been using this 100% free learning app having more than 2000 videos in IT and non IT fields. Download and register on this LearnVern app and start learning.',
                'control_type' => 'textarea'
            ],
            'refer_share_message' => [
                'config_group_id' => 3,
                'is_replicable_for_partner' => 0,
                'title' => 'Referral Share Message',
                'configuration_value' => 'Hi I have been using this 100% free learning app having more than 2000 videos in IT and non IT fields. Download and register on this LearnVern app and start learning.',
                'control_type' => 'textarea'
            ],
            'course_share_message' => [
                'config_group_id' => 3,
                'is_replicable_for_partner' => 0,
                'title' => 'Course Share Message',
                'configuration_value' => 'I feel you will enjoy this course on @LearnVern - #COURSE_NAME#. Join Now!',
                'control_type' => 'textarea'
            ],
            'webinar_share_message' => [
                'config_group_id' => 3,
                'is_replicable_for_partner' => 0,
                'title' => 'Webinar Share Message',
                'configuration_value' => 'I feel you will enjoy this webinar on @LearnVern - #COURSE_NAME#. Join Now!',
                'control_type' => 'textarea'
            ],
            'certificate_share_message' => [
                'config_group_id' => 3,
                'is_replicable_for_partner' => 0,
                'title' => 'Certificate Share Message',
                'configuration_value' => 'Receive the Certificate of Attendance if you complete watching 5 or more sessions during the 2 days of #COURSE_NAME#. Eligible attendees will receive the certificate',
                'control_type' => 'textarea'
            ],
            'referral_popup_interval_day' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 0,
                'title' => 'Referral Popup Interval Days',
                'configuration_value' => '5',
                'control_type' => 'textfield'
            ],
            'referral_popup_progress' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 0,
                'title' => 'Show Referral Popup on Minimum Course Progress %',
                'configuration_value' => '2',
                'control_type' => 'textfield'
            ],
            'daily_deleted_users_email' => [
                'config_group_id' => 4,
                'is_replicable_for_partner' => 0,
                'title' => 'Daily Deleted Users Email (Multiple values using comma separator)',
                'configuration_value' => "prashant.urvam@gmail.com,kishan.urvam@gmail.com",
                'control_type' => 'textarea'
            ],
            'show_app_review_button_course_progress' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 0,
                'title' => 'Show review & rating popup on course progress (%) for APP',
                'configuration_value' => 35,
                'control_type' => 'textfield'
            ],
            'show_app_referral_popup_based_on_user_progress' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 0,
                'title' => 'Show Referral Popup Progress for APP (Multiple values using comma separator)',
                'configuration_value' => "4,20,35",
                'control_type' => 'textfield'
            ],
            'blocked_user_domains' => [
                'config_group_id' => 1,
                'is_replicable_for_partner' => 0,
                'title' => 'User Blocked Domains (Multiple values using comma separator)',
                'configuration_value' => "aceadd.com",
                'control_type' => 'textarea'
            ]
        ];

        foreach ($site_configuration as $key => $configuration) {
            $dataExist = DB::table('site_configuration')->where('partner_id', 1)->where('identifier', $key)->first();
            if (is_null($dataExist)){
                DB::table('site_configuration')->insert([
                    'partner_id' => 1,
                    'is_replicable_for_partner' => $configuration['is_replicable_for_partner'],
                    'config_group_id' => $configuration['config_group_id'],
                    'configuration_title' => $configuration['title'],
                    'identifier' => $key,
                    'configuration_value' => $configuration['configuration_value'],
                    'control_type' => $configuration['control_type'],
                    'created_at' => Carbon\Carbon::now(),
                    'updated_at' => Carbon\Carbon::now(),
                ]);
            }

        }

    }
}
