<?php

namespace Database\Seeders;

use App\Models\Course;
use App\Models\CourseKeywords;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CopyCourseStatusSlugSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $datas = Course::select('id as course_id', 'name as course_name', 'slug', 'is_career_plan_course', 'is_active')->get()->toArray();
        foreach ($datas as $data) {
            $course = CourseKeywords::where('course_id', (int) $data['course_id'])->update($data, ['upsert' => true]);
        }
    }

}
