<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SearchKeywordsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('admin_modules')->whereIn('module_keys', ['search_keywords', 'missing_search_keywords'])->delete();
        $datas = [
            [
                'module_keys' => 'search_keywords',
                'module_title' => 'Search Keywords',
                'icon_class' => null,
                'route_name' => 'search-keywords',
                'description' => 'Import and add/update search keywords for courses.',
                'parent_module' => 2,
                'accessible_for_user_type' => 'lms'
            ],
            [
                'module_keys' => 'missing_search_keywords',
                'module_title' => 'Missing Search Keywords',
                'icon_class' => null,
                'route_name' => 'missing-search-keywords',
                'description' => 'Map search keywords to relevant course.',
                'parent_module' => 2,
                'accessible_for_user_type' => 'lms'
            ],
        ];

        DB::table('admin_modules')->insert($datas);
        /*foreach ($datas as $data) {
            $insert_data = [
                [
                    'role_id' => 1, // Access for Super admin user.
                    'module_key' => $data['module_keys']
                ],
                [
                    'role_id' => 3, // Access for LMS user
                    'module_key' => $data['module_keys']
                ]
            ];
            DB::table('role_wise_module_accesses')->insert($insert_data);
        }*/
    }

}
