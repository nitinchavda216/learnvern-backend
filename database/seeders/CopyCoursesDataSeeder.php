<?php

namespace Database\Seeders;

use App\Models\Course;
use App\Models\CourseKeywords;
use Illuminate\Database\Seeder;

class CopyCoursesDataSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $datas = Course::select('id', 'name')->get()->toArray();
        $course_names = array_column($datas, 'name');
        $course_ids = array_column($datas, 'id');
        $course_datas = array_combine($course_ids, $course_names);
        $insData = [];
        foreach ($course_datas as $key => $value) {
            $insData[] = [
                'course_id' => $key,
                'course_name' => $value,
                'unit_ids' => null,
                'keywords' => json_encode([]),
                'created_at' => null,
                'updated_at' => null
            ];
        }
        CourseKeywords::truncate();
        CourseKeywords::insert($insData);
    }

}
