<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15-02-2021
 * Time: 03:23 PM
 */

namespace App\Mail;


use Illuminate\Mail\Mailable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailable extends Mailable
{

    use Queueable, SerializesModels;
    public $reset_password;

    public function __construct($reset_password)
    {
        $this->reset_password = $reset_password;
    }
    public function build()
    {
        return $this->view('reset_password_link');
    }

}
