<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26-02-2021
 * Time: 09:25 AM
 */

namespace App\Interfaces;

interface CourseStatusRepositoryInterface
{
    public function getAjaxListData($request);

    public function exportListData($request);
}
