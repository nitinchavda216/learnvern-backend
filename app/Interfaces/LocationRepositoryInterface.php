<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08-02-2021
 * Time: 02:23 PM
 */

namespace App\Interfaces;

interface LocationRepositoryInterface
{
    public function getCountries();

    public function getStates($country_id);

    public function getCities($state_id);

    public function getDistrict($state_id);

    public function getCityDetailByName($name);
    public function getCityDetailById($cityId);

    public function getStatesByCountry($request);

    public function getCitiesByState($request);
    public function getDistrictByState($request);

}
