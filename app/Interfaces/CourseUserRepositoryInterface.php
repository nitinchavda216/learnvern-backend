<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08-02-2021
 * Time: 02:23 PM
 */

namespace App\Interfaces;

interface CourseUserRepositoryInterface
{
    public function getCourseEnrollmentsByDate($start_date,$end_date);


}
