<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 03:01 PM
 */

namespace App\Interfaces;

interface UsersReportRepositoryInterface
{
    public function getAjaxListData($request);

    public function getReferralReportAjaxListData($request);
    public function exportReferralReportData($request);

    public function getMonthlyData($request);

    public function getAjaxExportData($request);

    public function getUnUnrolledUsersAjaxList($request);

    public function exportUnEnrolledUsersList($request);
}
