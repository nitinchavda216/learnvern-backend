<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:28 PM
 */

namespace App\Interfaces;


interface AssignmentRepositoryInterface
{
    public function getAjaxListData($request);

    public function storeAssignment($request);

    public function updateAssignment($request, $id);

    public function updateSlug($request);

    public function updateActiveStatus($request);

    public function getAssignmentDetails($id);

    public function getAjaxExportData($request);

    public function importUpdateAssignment($request);
}
