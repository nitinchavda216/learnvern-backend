<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:29 PM
 */

namespace App\Interfaces;

interface UserRepositoryInterface
{
    public function getCourseSearchListData($request);

    public function getCampaignUsersSearchListData($request, $adminUserId = null);

    public function getNSDCCandidatesList($request);

    public function getAjaxListData($request);

    public function downloadAmbassadorCertificate($user_id, $referral_count);

    public function updateBlockStatus($request);

    public function getUserByRegister($request);

    public function getUserDetails($id);

    public function updateUser($request, $id);

    public function getCourses($request);

    public function getAllUser();

    public function getActiveUser();

    public function getRevenue();

    public function getMonthRecord();

    public function getNSDCExportData($request);

    public function getNSDCUploadData($request);

    public function getRecord($request);

    public function getTotalReferral($id);

    public function updateNSDCCertificateDate($request);

    public function getDayWiseData($request);

    public function getTopReferralsUsers($request);

    public function getMonthWiseData();


    public function getInternationalCandidatesList($request);

    public function getInternationalExportData($request);

    public function getRegisteredDataByDate($start_date, $end_date);

    public function getUserQuizResults($user_id);
}
