<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:23 PM
 */

namespace App\Interfaces;

interface UnitRepositoryInterface
{
    public function getAjaxListData($request);

    public function storeUnit($request);

    public function storeSchemaScript($unit_id);

    public function getUnitDetails($id);

    public function getOldUnitSlugList($id);

    public function getUnitAttachment($id);

    public function updateUnit($request, $id);

    public function updateSlug($request);

    public function updateActiveStatus($request);

    public function getAjaxExportData($request);

    public function importUpdateUnit($request);

    public function exportSampleUnitExcel($request);

    public function importNewUnits($request);
}
