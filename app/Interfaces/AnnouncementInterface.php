<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:23 PM
 */

namespace App\Interfaces;

interface AnnouncementInterface
{
    public function getAjaxListData($request);

    public function getAnnouncementDetail($id);

    public function store($request);

    public function updateAnnouncementData($request, $id);

    public function updateActiveStatus($request);


}
