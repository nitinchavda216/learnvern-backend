<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:43 PM
 */

namespace App\Interfaces;

use Illuminate\Http\Request;

interface SiteConfigurationsRepositoryInterface
{
    public function getAllConfigurationGroups($request);

    public function updateConfigurations(Request $data);

    public function getConfigurationForApp();

    public function getConfigurationByKeys($keys = []);

    public function getBlockedUsersDomain();

    public function getCollegeList($college_search = null);

    public function getCountry();

    public function getIsdCode();

    public function getPageType();

    public function getCategoriesList();

    public function getPageUrl($page_type);

    public function getState($country_id);

    public function getCity($state_id);

    public function getDistrict($state_id);
}
