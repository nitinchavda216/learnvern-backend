<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:37 PM
 */

namespace App\Interfaces;


interface AmbassadorProgramRepositoryInterface
{
    public function getAllSettings();

    public function storeSettings($request);

    public function getSettingDetails($id);

    public function updateSettings($request, $id);

    public function updateActiveStatus($request);

    public function getAmbassadorLevelTitles();
}
