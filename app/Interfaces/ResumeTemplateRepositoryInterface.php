<?php

namespace App\Interfaces;

interface ResumeTemplateRepositoryInterface
{
    public function getResumeTemplates();

    public function storeTemplate($request);

    public function getTemplateDetail($id);

    public function updateTemplate($request, $id);

    public function deleteTemplate($id);
}
