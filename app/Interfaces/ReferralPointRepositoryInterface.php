<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:39 PM
 */

namespace App\Interfaces;


interface ReferralPointRepositoryInterface
{
    public function getAjaxListData($request);

    public function exportListData($request);
}
