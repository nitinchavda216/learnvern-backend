<?php

namespace App\Interfaces;

use Illuminate\Http\Request;

interface ReportBuilderRepositoryInterface
{
    public function getAllReports();

    public function getConstantData();

    public function getEditConstantDataDetail($requestData = []);

    public function getConstantDataDetail($requestData);

    public function validateReport(Request $request);

    public function storeReports(Request $request);

    public function storeNewReports(Request $request);

    public function getReportData($id);

    public function updateReports(Request $request, $id);

    public function deleteReport($id);

    public function buildQuery($requestData, $pageSize = null, $start = null);

    public function campaignBuildQuery($requestData,$campaign_id,$conditionApply);
}
