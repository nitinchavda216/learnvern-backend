<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 23-02-2021
 * Time: 03:42 PM
 */

namespace App\Interfaces;


interface ReviewRepositoryInterface
{
    public function getAjaxListData($request);

    public function updateActiveStatus($request);

    public function getReviewDetails($id);

    public function storeDefaultReviews($request);

    public function updateReviews($request, $id);

    public function getReview();
    public function getCountActive();

    public function createReview($request);
}
