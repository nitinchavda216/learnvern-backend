<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13-04-2021
 * Time: 10:00 AM
 */

namespace App\Interfaces;


interface AdminUserRoleRepositoryInterface
{
    public function getAdminParentModule();
    public function getAdminModulesByType($type);

    public function storeAllData($request);

    public function getAllUserRole();

    public function getAdminRoleDetails($id);

    public function getSelectedModule($id);

    public function updateAdminModule($request, $id);

    public function getRoleTitles();

}
