<?php

namespace App\Interfaces;

interface CourseKeywordRepositoryInterface {

    public function getListData();

    public function getCourseKeywordDetails($id);

    public function courseKeywordsImport($request);

    public function upsertcourseKeywordsData($request, $id);

    public function getMissingCourseKeywordsListData($request);

    public function missingCourseKeywordsupdate($request);

    public function missingCourseKeywordsdestroy($request, $id);

    public function keywordSearchAnalyticsIndex($request);
    
    public function keywordSearchAnalyticsList($request);
    
    public function getAjaxExportData($request);
    
    public function getKeywordExportData($request);
    
    public function missingKeywordHistoryList($id);
    
    public function missingKeywordHistoryListData($request);
}
