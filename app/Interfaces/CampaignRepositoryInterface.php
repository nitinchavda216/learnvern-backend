<?php

namespace App\Interfaces;

interface CampaignRepositoryInterface
{
    public function getCampaignDetails($id);

    public function getAllCampaign($request);

    public function getAllCampaignForCrm($request);

    public function getCampaignReportForCrm($request);

    public function storeCampaign($request);

    public function storeCampaignInCampaign($request);

    public function getReportData($id);

    public function exportData($id);

    public function viewReportAjaxDataReferral($request, $campaignDetail);

    public function viewReportAjaxDataRevenue($request, $campaignDetail);

    public function viewReportAjaxDataGrowth($request, $campaignDetail);

    public function viewCallingUserData($request);

    public function deleteCampaign($id);

    public function markAsCompleteCampaign($id);

    public function saveCallingDetail($request);

    public function getCallingStatisticsData($id);

    public function getCrmDashboardData($request);

    public function pendingCallingUserData($request);


    public function getFollowUpStatisticsReferral($request);

    public function getFollowUpStatisticsRevenue($request);

    public function getCrmUserList($request);

    public function userPerformance($id, $request);

    public function getUserAssociateWithCampaign($request);

    public function assignCampaignToUser($request,$isFollowUP);
}
