<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08-02-2021
 * Time: 02:23 PM
 */

namespace App\Interfaces;

interface LandingPagesRepositoryInterface
{
    public function getAllLandingPages($request);

    public function getCourseTitles($id = null);

    public function getCourseDetails($id);

    public function storeLandingPage($request);

    public function updateLandingPage($request, $id);

    public function getLandingPagesDetails($id);

    public function deleteLandingPage($id);

}
