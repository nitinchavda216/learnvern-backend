<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 03:04 PM
 */

namespace App\Interfaces;


interface AbandonedPaymentRepositoryInterface
{
    public function getAjaxListData($request);

    public function getAjaxExportData($request);
}
