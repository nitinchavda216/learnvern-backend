<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18-02-2021
 * Time: 11:14 AM
 */

namespace App\Interfaces;


interface LanguageRepositoryInterface
{
    public function getAllLanguage();

    public function storeLanguageData($request);

    public function getLanguageDetails($id);

    public function updateLanguage($request, $id);

    public function getLanguageTitles();

}
