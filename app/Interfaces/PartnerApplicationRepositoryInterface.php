<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:34 PM
 */

namespace App\Interfaces;

interface PartnerApplicationRepositoryInterface
{
    public function getPartnersName();

    public function getApplicationRequestList($request);

    public function updateApplicationStatus($request, $id, $status);

    public function storePartnerType($request);

    public function showApplicationStatus($id);

    public function getPartnerRevenueFees($id);

    public function getPartnerCoursesId($id);

    public function storePartnerRevenueInfoAndAccessibility($partnerId, $request);

}
