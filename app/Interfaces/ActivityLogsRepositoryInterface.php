<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:37 PM
 */

namespace App\Interfaces;

interface ActivityLogsRepositoryInterface
{
    public function getActivityLogsData($request);

    public function getActivityLogsChangesData($input);

    public function getActivityDataByModel($id, $log_name);
}
