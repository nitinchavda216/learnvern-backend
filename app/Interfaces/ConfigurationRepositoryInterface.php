<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:46 PM
 */

namespace App\Interfaces;


interface ConfigurationRepositoryInterface
{
    public function getdetails();

    public function update($request);

    public function getAllConfigurationGroups($request);

    public function updateConfigurations($request);

    public function getConfigurationForApp();

    public function getCountry();

    public function getIsdCode();

    public function getPageType();

    public function getPageUrl();

    public function getCategory();
}
