<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:15 PM
 */

namespace App\Interfaces;

interface CoursesRepositoryInterface
{
    public function getCourseList($request);

    public function getLatestCourses($count);

    public function getCourseTitles();

    public function getCourseBadges();

    public function getActiveCoursesListExceptCareerPlanCourses();

    public function getAllActiveCourseTitles();

    public function storeCourse($request);

    public function storeSchemaScript($id);

    public function getCourseDetails($id);

    public function updateCourse($request, $id);

    public function updateCourseSlug($request);

    public function updateCoursePrice($request);

    public function updateActiveStatus($request);

    public function getCurriculumData($course_id);

    public function getCurriculumDetail($id);

    public function getCurrentCurriculumRelatedCourses($course_id, $id, $type);

    public function getCareerPlanCurriculumData($course_id);

    public function storeCareerPlanCurriculum($request);

    public function deleteCurriculum($request);

    public function editMasterSection($request);

    public function updateCurriculum($request);

    public function updateSortOrder($request);

    public function updateHideFromBackendStatus($request);

    public function getCourseAjaxListData($request);

    public function getAjaxExportData($request);

    public function convertInternationalToNSDC($request);

    public function getSearchKeywords($course_id);
}
