<?php

namespace App\Interfaces;

interface AppUpdateRepositoryInterface
{
    public function getAppUpdateList();

    public function getLastVersionDetails();

    public function getAppLatestVersionDetails($request);

    public function getVersionDetails($id);

    public function storeAppUpdate($request);
}
