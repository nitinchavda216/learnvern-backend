<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:43 PM
 */

namespace App\Interfaces;


interface WidgetsSettingsRepositoryInterface
{
    public function getAllWidgets();

    public function storewidgetssettings($request);

    public function updateActiveStatus($request);

    public function getWidgetsSettingsDetails($id);

    public function updateWidgetsSettings($request, $id);


}
