<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 03:07 PM
 */

namespace App\Interfaces;


interface AnalyticsRepositoryInterface
{
    public function getAnalyticsReferralDataByDate($request);

    public function getHistoricalAnalyticsReferralData();
}
