<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:21 PM
 */

namespace App\Interfaces;

interface SectionRepositoryInterface
{
    public function getAjaxListData($request);

    public function updateActiveStatus($request);

    public function getParentSections($request);

    public function storeSectionData($request);

    public function getSectionDetail($id);

    public function updateSectionData($request, $id);

    public function getSectionTitles($course_id = null);

    public function getSectionsByCourse($request);

    public function getAjaxExportData($request);

    public function importUpdateUnit($request);
}
