<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08-02-2021
 * Time: 02:23 PM
 */

namespace App\Interfaces;

interface LocationPagesRepositoryInterface
{
    public function getAllLocationPages();

    public function storeLocationPage($request);

    public function updateActiveStatus($request);

    public function getLocationPagesDetails($id);

    public function updateLocationPage($request, $id);

}
