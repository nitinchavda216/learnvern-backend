<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:34 PM
 */

namespace App\Interfaces;

interface PaymentRepositoryInterface
{
    public function getPaymentHistoryList($request);

    public function getIncome($start_date,$end_date);

    public function getPaymentRecord();

    public function getCertificatePurchaseUsersAjaxData($request);

    public function sendPaymentLink($request);

    public function paymentSuccess($request);
}
