<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:15 PM
 */

namespace App\Interfaces;

interface WebinarsRepositoryInterface
{
    public function getWebinarCourseList($request);
    public function getCourseTitles();

    public function storeCourse($request);

    public function getCourseDetails($id);

    public function updateCourse($request, $id);

    public function updateActiveStatus($request);
}
