<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:25 PM
 */

namespace App\Interfaces;

interface QuizRepositoryInterface
{
    public function getAjaxListData($request);

    public function storeQuiz($request);

    public function getQuizDetails($id);

    public function updateQuiz($request, $id);

    public function updateActiveStatus($request);

    public function getQuizUser($id);

    public function getQuestionListByQuiz($id);

    public function getQuizQuestionDetails($id);

    public function storeQuizQuestion($request);

    public function updateQuizQuestion($request, $id);

    public function updateQuestionStatus($request);

    public function getAjaxExportData($request);

    public function importQuiz($request);

    public function downloadQuestionSampleFile($id);

    public function importQuestions($request);
}
