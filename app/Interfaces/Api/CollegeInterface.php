<?php


namespace App\Interfaces\Api;


interface CollegeInterface
{
    public function getById($id);
}
