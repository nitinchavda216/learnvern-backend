<?php

namespace App\Interfaces\Api;

interface UserRepositoryInterface
{
    public function validateUpdateRequest($request);

    public function validateMobileNumber($request);

    public function updateUser($request);

    public function getUserCertificatesList($request);

    public function downloadCertificate($request);

    public function verifyCertificate($request);

    public function getUserById($userId);

    public function updateVerifiedMobileNumber($request);
}
