<?php

namespace App\Interfaces\Api;

interface CoursesRepositoryInterface
{
    public function allCourseList();

    public function getCoursesByCategory($category_id);

    public function popularCourseList();

    public function upcomingWebinarList($webinar_id = null);

    public function getUserCoursesList($userId);

    public function getUserContinueCoursesList($userId);

    public function getNextContinueUnitDetail($course_id, $user_id);

    public function getNextCurriculumDetails($course_id, $user_id, $current_curriculum_id, $current_curriculum_type);

    public function getTopCourseWithCategoryWise();

    public function getTopSearchCourseList();

    public function getRecommendedCourses($userId, $courseId = null);

    public function getViewPageCourseDetails($courseDetail, $course_id, $user_id = null);

    public function getCourseDetails($course_id);

    public function getUnitDetails($curriculum_id, $curriculum_type, $user_id = null);

    public function markUnitAsCompleted($request);

    public function storeOfflineProgressData($request);

    public function courseEnroll($request);

    public function getFaqsCourse($courseId);

    public function getCurriculumList($userId, $courseId, $curriculum_id = null, $curriculum_type = null);

    public function getTestimonials($course_id);

    public function saveUnitProgress($requestData);

    public function setAssignmentProgress($requestData);
}
