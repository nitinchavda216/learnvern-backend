<?php

namespace App\Interfaces\Api;

interface QuizRepositoryInterface
{
    public function quizDetails($quiz_id, $user_id);

    public function userQuizResultCourseList($user_id);

    public function userQuizResultsByCourse($user_id, $course_id);

    public function getSingleQuizResults($user_id, $curriculum_id);

    public function submitQuiz($request);
}
