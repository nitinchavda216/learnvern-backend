<?php

namespace App\Interfaces\Api;

interface BannerRepositoryInterface
{
    public function getBannersListByType();

    public function getBannerDetails($id);

    public function getAllBanners();

    public function storeBannerData($request);

    public function updateBannerData($request, $id);

    public function deleteBanner($id);

    public function getPromotionBanners();

    public function getConfigurationBanners();

    public function getBannerImageByType($type);
}
