<?php


namespace App\Interfaces\Api;


interface PaymentInterface
{
    public function doPayment($requestData);

    public function abandonedPayment($requestData);
}
