<?php

namespace App\Interfaces\Api;

interface UserAuthInterface
{
    public function storeUser($request);

    public function validateRegister($request);

    public function authenticateUser($request);

    public function socialAuth($request);

    public function sendForgotPasswordLink($request);
}
