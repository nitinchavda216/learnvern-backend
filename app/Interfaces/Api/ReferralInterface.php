<?php


namespace App\Interfaces\Api;


interface ReferralInterface
{
    public function getAmbassadorSettings();

    public function getRecentAmbassadors($count = 10);

    public function myReferrals($ownReferralCode); // own_referral_code

    public function getAffiliateUser($userId);

    public function userAmbassadorBadges($userId);

    public function getFaqs();

    public function getUserAmbassadorSettings($user_id);
}
