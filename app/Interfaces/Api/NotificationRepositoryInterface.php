<?php

namespace App\Interfaces\Api;

interface NotificationRepositoryInterface
{
    public function getUserNotificationList($user_id);

    public function readNotification($request);

    public function deleteNotification($request);
}
