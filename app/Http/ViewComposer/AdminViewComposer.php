<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12-04-2021
 * Time: 02:20 PM
 */

namespace App\Http\ViewComposer;


use App\Models\AdminModules;
use App\Models\RoleWiseModuleAccess;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AdminViewComposer
{
    public function compose(View $view)
    {
        $user = Auth::guard('admins')->user();
        $data = AdminModules::with('child')->where(['parent_module' => 0])->get();
        $module_access = RoleWiseModuleAccess::where('role_id', $user->role_id)->pluck('module_key')->toArray();
        $is_super_admin = ($user->relatedRole->is_super_admin == 1) ? true : false;
        $view->with([
            'admin_module' => $data,
            'module_access' => $module_access,
            'is_super_admin' => $is_super_admin,
        ]);
    }
}
