<?php

namespace App\Http\Controllers;

use App\Interfaces\ResumeTemplateRepositoryInterface;
use Illuminate\Http\Request;

class ResumeTemplateController extends Controller
{
    protected $resumeTemplateRepository;

    public function __construct(ResumeTemplateRepositoryInterface $resumeTemplateRepository)
    {
        $this->resumeTemplateRepository = $resumeTemplateRepository;
    }

    public function index()
    {
        $templates = $this->resumeTemplateRepository->getResumeTemplates();
        return view('resume_templates.index', compact('templates'));
    }

    public function create()
    {
        return view('resume_templates.create');
    }

    public function store(Request $request)
    {
        $this->resumeTemplateRepository->storeTemplate($request);
        return redirect()->route('resume_templates')->with('success', "Data Stored Successfully!");
    }

    public function edit($id)
    {
        $template = $this->resumeTemplateRepository->getTemplateDetail($id);
        return view('resume_templates.edit', compact('template'));
    }

    public function update(Request $request, $id)
    {
        $this->resumeTemplateRepository->updateTemplate($request, $id);
        return redirect()->route('resume_templates')->with('success', "Data Updated Successfully!");
    }

    public function delete($id)
    {
        $this->resumeTemplateRepository->deleteTemplate($id);
        return redirect()->route('resume_templates')->with('success', "Data Deleted Successfully!");
    }
}
