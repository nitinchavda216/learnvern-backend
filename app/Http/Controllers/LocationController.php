<?php

namespace App\Http\Controllers;

use App\Interfaces\LocationRepositoryInterface;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    protected $locationRepository;

    public function __construct(LocationRepositoryInterface $locationRepository)
    {
        $this->locationRepository = $locationRepository;
    }

    public function getStatesByCountry(Request $request)
    {
        $data = $this->locationRepository->getStatesByCountry($request);
        echo json_encode($data);
    }

    public function getCitiesByState(Request $request)
    {
        $data = $this->locationRepository->getCitiesByState($request);
        echo json_encode($data);
    }

    public function getDistrictByState(Request $request)
    {
        $data = $this->locationRepository->getDistrictByState($request);
        echo json_encode($data);
    }
}
