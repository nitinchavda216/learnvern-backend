<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class MautikApiController extends Controller
{
    public function index()
    {
        $loginname = "1_3w5iknbeqrwg4scokcws4cco88scgg48ccow04gwwgkcksw044";
        $password = "4tgl3n88aq68gcskcw404g80kss0w0sskggc8ckg4kggk0ocws";

        $siteurl = "https://www.learnvern.com/tld";

        $email = "malay@urvam.com";
        $firstname = "Malay";
        $lastname = "Mehta";
        $tag = "Ambassador";
        $curl = curl_init();
        // Set some options - we are passing in a user agent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://" . $loginname . ":" . $password . "@" . $siteurl . "/api/contacts/new",
            CURLOPT_USERAGENT => 'Mautic Connector',
            CURLOPT_POST => 1,
            // posting the payload
            CURLOPT_POSTFIELDS => array(
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'tags' => $tag
            )
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $data = ["error" => true, "message" => $err];
            dd($data);
        }
        dd($response);
        return $response;
    }

    public function initWebhook(Request $request)
    {
        $date = formatDate(Carbon::now(), 'd_m_Y');
        $logEbayURL = new Logger('Mautic Webhook Error');
        $logEbayURL->pushHandler(new StreamHandler(storage_path('logs/mautic_webhook_details_' . $date . '.log')), Logger::INFO);
        $logEbayURL->info('Webhook Details:', [$request->all()]);
    }
}
