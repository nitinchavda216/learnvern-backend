<?php

namespace App\Http\Controllers;

use App\Interfaces\AmbassadorProgramRepositoryInterface;
use Illuminate\Http\Request;

class AmbassadorProgramSettingsController extends Controller
{
    protected $ambassadorProgramRepository;

    public function __construct(AmbassadorProgramRepositoryInterface $ambassadorProgramRepository)
    {
        $this->ambassadorProgramRepository = $ambassadorProgramRepository;
    }

    public function index()
    {
        $data = $this->ambassadorProgramRepository->getAllSettings();
        return view('ambassador-program.index', compact('data'));
    }

    public function create()
    {
        return view('ambassador-program.create');
    }

    public function store(Request $request)
    {
        $this->ambassadorProgramRepository->storeSettings($request);
        return redirect()->route('ambassador-program-settings')->with('success', "Data Created Successfully!");
    }

    public function edit($id)
    {
        $program_setting = $this->ambassadorProgramRepository->getSettingDetails($id);
        return view('ambassador-program.edit', compact('program_setting'));
    }

    public function update(Request $request, $id)
    {
        $this->ambassadorProgramRepository->updateSettings($request, $id);
        return redirect()->route('ambassador-program-settings')->with('success', "Data Updated Successfully!");
    }

    public function updateStatus(Request $request)
    {
        $this->ambassadorProgramRepository->updateActiveStatus($request);
        return response()->json(['status' => true]);
    }
}
