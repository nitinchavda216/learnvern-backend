<?php

namespace App\Http\Controllers;

use App\Interfaces\AdminUserRepositoryInterface;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    protected $AdminUserRepository;

    public function __construct(AdminUserRepositoryInterface $AdminUserRepository)
    {
        $this->AdminUserRepository = $AdminUserRepository;
    }


    public function getProfile()
    {
        $admin = getCurrentAdmin();
        return view('profile.index', compact('admin'));
    }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required_with:password|same:password|min:6'
        ],[
            'password.required' => "Please enter a password! ",
            'password_confirmation.required_with' => "Your confirm password does not match with your password!"
        ]);
        $this->AdminUserRepository->updateProfile($request);
        $user = getCurrentAdmin();
        $redirectRoute = $user->relatedRole->default_redirect_route ?? 'dashboard';
        return redirect()->route($redirectRoute)->with('success', "Profile updated successfully!");
    }
}
