<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\CertificateRepositoryInterface;

class CertificateController extends Controller
{
    protected $certificateRepository;

    public function __construct(CertificateRepositoryInterface $certificateRepository)
    {
        $this->certificateRepository = $certificateRepository;
    }

    public function downloadCertificate(Request $request)
    {
        $response = $this->certificateRepository->downloadCertificate($request);
        if ($response['status']) {
            return $response['pdf']->download($response['certificate_name']);
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }
}
