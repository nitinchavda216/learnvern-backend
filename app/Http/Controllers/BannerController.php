<?php

namespace App\Http\Controllers;

use App\Interfaces\Api\BannerRepositoryInterface;
use App\Interfaces\CoursesRepositoryInterface;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    protected $bannerRepository, $coursesRepository;

    public function __construct(BannerRepositoryInterface $bannerRepository, CoursesRepositoryInterface $coursesRepository)
    {
        $this->bannerRepository = $bannerRepository;
        $this->coursesRepository = $coursesRepository;
    }

    public function index()
    {
        $allbanners = $this->bannerRepository->getAllBanners();
        return view('banner.index',compact('allbanners'));
    }

    public function create(){
        $courses = $this->coursesRepository->getAllActiveCourseTitles();
        return view('banner.create', compact('courses'));
    }

    public function store(Request $request)
    {
        $this->bannerRepository->storeBannerData($request);
        return redirect()->route('banner')->with('success', "Data Stored Successfully!");
    }

    public function edit($id)
    {
        $banner = $this->bannerRepository->getBannerDetails($id);
        $courses = $this->coursesRepository->getAllActiveCourseTitles();
        return view('banner.edit', compact('banner', 'courses'));
    }

    public function update(Request $request, $id)
    {
        $this->bannerRepository->updateBannerData($request, $id);
        return redirect()->route('banner')->with('success', "Data Updated Successfully!");
    }

    public function destroy($id)
    {
        $this->bannerRepository->deleteBanner($id);
        return redirect()->route('banner')->with('success', 'Banner Deleted Successfully');
    }

}
