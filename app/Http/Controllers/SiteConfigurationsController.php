<?php

namespace App\Http\Controllers;
use App\Interfaces\SiteConfigurationsRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;


class SiteConfigurationsController extends Controller
{
    protected $siteConfigurationsRepository;

    public function __construct(SiteConfigurationsRepositoryInterface $siteConfigurationsRepository)
    {
        $this->siteConfigurationsRepository = $siteConfigurationsRepository;
    }

    public function index(Request $request)
    {
        $configuration_groups = $this->siteConfigurationsRepository->getAllConfigurationGroups($request);
        return view('configuration.index', compact('configuration_groups'));
    }

    public function update(Request $request)
    {
        $this->siteConfigurationsRepository->updateConfigurations($request);
        return redirect()->route('configuration')->with('success', 'Site Configuration Updated Successfully');
    }

    public function updateSiteMapSuccess()
    {
        return redirect()->route('configuration')->with('success', 'Site Map Updated Successfully');
    }

}
