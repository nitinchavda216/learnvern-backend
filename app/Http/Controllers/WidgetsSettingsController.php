<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\WidgetsSettingsRepositoryInterface;

class WidgetsSettingsController extends Controller
{
    protected $widgetssettingsRepository;
    protected $locationPagesRepository;

    /**
     * WidgetsSettingsController constructor.
     * @param WidgetsSettingsRepositoryInterface $widgetssettingsRepository
     */
    public function __construct(WidgetsSettingsRepositoryInterface $widgetssettingsRepository)
    {
        $this->widgetssettingsRepository = $widgetssettingsRepository;
    }
    public function index()
    {
        $widgetdetails = $this->widgetssettingsRepository->getAllWidgets();
//        dd($widgetdetails);
        return view('widgets-settings.index', compact('widgetdetails'));
    }

    public function create()
    {
        return view('widgets-settings.create');
    }

    public function store(Request $request)
    {
        $this->widgetssettingsRepository->storewidgetssettings($request);
        return redirect()->route('widget-setting')->with('success', "Data Created Successfully!");
    }

    public function updateStatus(Request $request)
    {
//        dd($request);
        $this->widgetssettingsRepository->updateActiveStatus($request);
        return response()->json(['status' => true]);
    }
    public function edit($id)
    {
        $widgetdetails = $this->widgetssettingsRepository->getWidgetsSettingsDetails($id);
//        dd($widgetdetails);
        return view('widgets-settings.edit' , compact('widgetdetails'));

    }
    public function update(Request $request, $id)
    {
        $this->widgetssettingsRepository->updateWidgetsSettings($request, $id);
        return redirect()->route('widget-setting')->with('success', "Data Updated Successfully!");
    }
}
