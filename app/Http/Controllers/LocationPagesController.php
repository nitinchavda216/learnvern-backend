<?php

namespace App\Http\Controllers;

use App\Interfaces\LocationPagesRepositoryInterface;
use App\Interfaces\CoursesRepositoryInterface;
use Illuminate\Http\Request;

class LocationPagesController extends Controller
{
    protected $coursesRepository;
    protected $locationPagesRepository;

    public function __construct(LocationPagesRepositoryInterface $locationPagesRepository, CoursesRepositoryInterface $coursesRepository)
    {
        $this->locationPagesRepository = $locationPagesRepository;
        $this->coursesRepository = $coursesRepository;
    }

    public function index()
    {
        $locationPages = $this->locationPagesRepository->getAllLocationPages();
        return view('location-pages.index', compact('locationPages'));
    }

    public function create()
    {
        $courses = $this->coursesRepository->getCourseTitles();
        return view('location-pages.create', compact('courses'));
    }

    public function edit($id)
    {
        $courses = $this->coursesRepository->getCourseTitles();
        $location_page = $this->locationPagesRepository->getLocationPagesDetails($id);
        return view('location-pages.edit', compact('courses', 'location_page'));
    }

    public function store(Request $request)
    {
        $this->locationPagesRepository->storeLocationPage($request);
        return redirect()->route('location-pages')->with('success', "Data Created Successfully!");
    }

    public function update(Request $request, $id)
    {
        $this->locationPagesRepository->updateLocationPage($request, $id);
        return redirect()->route('location-pages')->with('success', "Data Updated Successfully!");
    }

    public function getCourseDetails(Request $request)
    {
        $requestData = $request->all();
        $course = $this->coursesRepository->getCourseDetails($requestData['course_id']);
        $returnHtml = view('location-pages.ajax.update_form', compact('course'))->render();
        return response()->json(['status' => true, 'html' => $returnHtml]);
    }

}
