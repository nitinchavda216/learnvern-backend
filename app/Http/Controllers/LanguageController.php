<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18-02-2021
 * Time: 10:56 AM
 */

namespace App\Http\Controllers;


use App\Interfaces\LanguageRepositoryInterface;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    protected $languageRepository;

    public function __construct(LanguageRepositoryInterface $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }


    public function index()
    {
        $language=$this->languageRepository->getAllLanguage();
        return view('language.index',compact('language'));
    }


    public function store(Request $request)
    {
        $this->languageRepository->storeLanguageData($request);
        return redirect()->route('languages')->with('success', "Data Stored Successfully!");
    }

    public function edit($id)
    {
        $language = $this->languageRepository->getLanguageDetails($id);
        return view('language.edit', compact('language'));
    }

    public function update(Request $request,$id)
    {
        $this->languageRepository->updateLanguage($request, $id);
        return redirect()->route('languages')->with('success', "Data Updated Successfully!");

    }
}
