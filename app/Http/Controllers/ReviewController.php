<?php

namespace App\Http\Controllers;

use App\Interfaces\CoursesRepositoryInterface;
use App\Interfaces\ReviewRepositoryInterface;
use App\Interfaces\UsersReportRepositoryInterface;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    protected $reviewRepository;
    protected $coursesRepository;
    protected $userRepository;

    public function __construct(ReviewRepositoryInterface $reviewRepository, CoursesRepositoryInterface $coursesRepository,
                                UsersReportRepositoryInterface $userRepository)
    {
        $this->reviewRepository = $reviewRepository;
        $this->coursesRepository = $coursesRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $courses = $this->coursesRepository->getCourseTitles();
        $active = $this->reviewRepository->getCountActive();
        $rating = $this->reviewRepository->getReview();
        return view('review.index', compact('courses', 'active', 'rating'));
    }

    public function getAjaxListData(Request $request)
    {
        return $this->reviewRepository->getAjaxListData($request);
    }

    public function create()
    {
        $courses = $this->coursesRepository->getCourseTitles();
        return view('review.create', compact('courses'));
    }

    public function store(Request $request)
    {
        $this->reviewRepository->storeDefaultReviews($request);
        if (isset($request->redirect_course_page)){
            return redirect()->route('courses')->with('success', "Data Updated Successfully!");
        }else{
            return redirect()->route('reviews')->with('success', "Data Updated Successfully!");
        }
    }

    public function updateStatus(Request $request)
    {
        $this->reviewRepository->updateActiveStatus($request);
        return response()->json(['status' => true]);
    }

    public function edit($id)
    {
        $courses = $this->coursesRepository->getCourseTitles();
        $review = $this->reviewRepository->getReviewDetails($id);
        return view('review.edit', compact('courses', 'review'));
    }

    public function update(Request $request, $id)
    {
        $this->reviewRepository->updateReviews($request, $id);
        return redirect()->route('reviews')->with('success', "Data Updated Successfully!");
    }
}
