<?php

namespace App\Http\Controllers;

use App\Interfaces\ActivityLogsRepositoryInterface;
use App\Interfaces\CacheRepositoryInterface;
use App\Interfaces\CategoryRepositoryInterface;
use App\Interfaces\CoursesRepositoryInterface;
use App\Interfaces\LanguageRepositoryInterface;
use App\Models\CourseUser;
use App\Models\User;
use App\Services\GenerateCourseSchema;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    protected $categoryRepository, $coursesRepository, $languageRepository, $generateCourseSchema, $activityLogsRepository,
        $cacheRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository, CoursesRepositoryInterface $coursesRepository,
                                LanguageRepositoryInterface $languageRepository, GenerateCourseSchema $generateCourseSchema,
                                ActivityLogsRepositoryInterface $activityLogsRepository, CacheRepositoryInterface $cacheRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->coursesRepository = $coursesRepository;
        $this->languageRepository = $languageRepository;
        $this->generateCourseSchema = $generateCourseSchema;
        $this->activityLogsRepository = $activityLogsRepository;
        $this->cacheRepository = $cacheRepository;
    }

    public function index()
    {
        $categories = $this->categoryRepository->getCourseCategoriesTitles();
        return view('courses.index', compact('categories'));
    }

    public function getAjaxListData(Request $request)
    {
        return $this->coursesRepository->getCourseList($request);
    }

    public function create()
    {
        if (session('partner_id') != 1) {
            abort(401, 'This action is unauthorized.');
        }
        $categories = $this->categoryRepository->getCourseCategoriesTitles();
        $courses = $this->coursesRepository->getCourseTitles();
        $language = $this->languageRepository->getLanguageTitles();
        $badges = $this->coursesRepository->getCourseBadges();
        return view('courses.create', compact('categories', 'courses', 'language', 'badges'));
    }

    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        $validator = Validator::make($request->all(), [
            'slug' => "required|unique:courses,slug"
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error', "This slug already exists. Try with another slug.");
        }
        $response = $this->coursesRepository->storeCourse($request);
        if ($response['status']) {
            return redirect()->route('courses')->with('success', "Data Created Successfully!");
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function edit($id)
    {
        if (session('partner_id') != 1) {
            abort(401, 'This action is unauthorized.');
        }
        $course = $this->coursesRepository->getCourseDetails($id);
        $categories = $this->categoryRepository->getCourseCategoriesTitles();
        $courses = $this->coursesRepository->getCourseTitles();
        $language = $this->languageRepository->getLanguageTitles();
        $skills = $this->coursesRepository->getCourseSkills($id);
        $badges = $this->coursesRepository->getCourseBadges();
        $search_keywords = $this->coursesRepository->getSearchKeywords($id);
        return view('courses.edit', compact('course', 'categories', 'courses', 'language','skills', 'badges', 'search_keywords'));
    }

    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        $this->coursesRepository->updateCourse($request, $id);
        if (env('REDIS_CACHE_ENABLED')) {
            $flushTags = [
                'api_category_wise_courses',
                'api_all_courses',
                'api_courses_by_category_' . $request->category_id,
                'api_popular_courses',
                'api_course_faqs_' . $id,
                'api_course_details_' . $id,
                'api_course_testimonials_' . $id,
            ];
            $this->cacheRepository->removeCacheByTag($flushTags);
        }
        return redirect()->route('courses')->with('success', "Data Created Successfully!");
    }

    public function updateCourseSlug(Request $request): \Illuminate\Http\JsonResponse
    {
        $response = $this->coursesRepository->updateCourseSlug($request);
        return response()->json($response);
    }

    public function updateCoursePrice(Request $request): \Illuminate\Http\RedirectResponse
    {
        $this->coursesRepository->updateCoursePrice($request);
        return redirect()->route('courses')->with('success', "Course Price Updated Successfully!");
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $this->coursesRepository->updateActiveStatus($request);
        return response()->json(['status' => true]);
    }

    public function getCurriculum($course_id)
    {
        if (session('partner_id') != 1) {
            abort(401, 'This action is unauthorized.');
        }
        $course = $this->coursesRepository->getCourseDetails($course_id);
        if (isset($course)) {
            $sections = $this->coursesRepository->getCurriculumData($course_id);
            return view('courses.curriculum', compact('sections', 'course'));
        } else {
            return redirect()->back()->with('error', 'Invalid Request!');
        }
    }

    public function getCareerPlanCurriculum($course_id, Request $request)
    {
        if (session('partner_id') != 1) {
            abort(401, 'This action is unauthorized.');
        }
        $course = $this->coursesRepository->getCourseDetails($course_id);
        if (isset($course)) {
            $sectionList = $searchCourseCurriculumList = [];
            $currentCourseCurriculum = $this->coursesRepository->getCareerPlanCurriculumData($course_id);
            foreach ($currentCourseCurriculum as $courseSection) {
                if (is_null($courseSection->parent_section_id)){
                    $sectionList[$courseSection->curriculum_list_id] = $courseSection->name;
                }
            }
            $coursesList = $this->coursesRepository->getActiveCoursesListExceptCareerPlanCourses();
            if (isset($request->course_id)) {
                $searchCourseCurriculumList = $this->coursesRepository->getCurriculumData($request->course_id);
            }
            return view('courses.career_plan_curriculum', compact('currentCourseCurriculum', 'course', 'coursesList', 'sectionList', 'searchCourseCurriculumList'));
        } else {
            return redirect()->back()->with('error', 'Invalid Request!');
        }
    }

    public function storeCareerPlanCurriculum(Request $request): \Illuminate\Http\RedirectResponse
    {
        $this->coursesRepository->storeCareerPlanCurriculum($request);
        return redirect()->route('courses.career_plan_curriculum', $request->course_id)->with('success', 'Curriculum Updated Successfully.');
    }

    public function showActivityLog($id)
    {
        $course = $this->coursesRepository->getCourseDetails($id);
        $activityData = $this->activityLogsRepository->getActivityDataByModel($id, 'Course');
        return view('courses.activity', compact('course', 'activityData'));
    }

    public function editMasterSection(Request $request): \Illuminate\Http\RedirectResponse
    {
        $this->coursesRepository->editMasterSection($request);
        return redirect()->back()->with(['success' => "Master Section Updated!"]);
    }

    public function deleteCurriculum(Request $request): \Illuminate\Http\JsonResponse
    {
        $response = $this->coursesRepository->deleteCurriculum($request);
        return response()->json($response);
    }

    public function updateCurriculum(Request $request): \Illuminate\Http\JsonResponse
    {
        $response = $this->coursesRepository->updateCurriculum($request);
        return response()->json($response);
    }

    public function updateSortOrder(Request $request): \Illuminate\Http\JsonResponse
    {
        $response = $this->coursesRepository->updateSortOrder($request);
        return response()->json($response);
    }

    public function updateHideFromBackendStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $this->coursesRepository->updateHideFromBackendStatus($request);
        return response()->json(['status' => true, 'message' => 'Hidden Status Updated Successfully!']);
    }

    public function doManualPayment(Request $request): \Illuminate\Http\RedirectResponse
    {
        DB::beginTransaction();
        try {
            $requestData = $request->all();
            $courseUserDetail = CourseUser::where('id', '=', $requestData['payment_course_user_id'])->first();
            if ($courseUserDetail) {
                $courseUserDetail->update(['paid_status' => 1, 'updated_at' => date('Y-m-d H:i:s'), 'payment_completed_date' => date('Y-m-d H:i:s')]);
                $ins_trans = array(
                    "user_id" => $courseUserDetail->user_id,
                    "course_id" => $courseUserDetail->course_id,
                    "course_user_id" => $courseUserDetail->id,
                    "amount" => $courseUserDetail->courseDetail->price,
                    "transaction_id" => "Txn" . rand(10, 99) . date("YmdHis"),
                    "mihpayid" => $requestData['referance_id'],
                    "payment_source" => $requestData['referance_id'],
                    "payment_method" => $requestData['payment_method'],
                    "transaction_date" => date("Y-m-d"),
                    "created_at" => date("Y-m-d H:i:s")
                );
                DB::table('payment_transactions')->insert($ins_trans);
                if (isset($courseUserDetail->userDetail->referral)) {
                    User::where('own_referral_code', $courseUserDetail->userDetail->referral)->increment('nsdc_referred_count');
                }
                $ins_trans['status'] = 'success';
                $ins_trans['server_response'] = '';
                unset($ins_trans['course_user_id']);
                DB::table('all_transactions')->insert($ins_trans);
                DB::commit();
                return redirect()->route('course-report')->with('success', "Certificate Activated Successfully!");
            } else {
                DB::rollBack();
                return redirect()->back()->with('error', 'Invalid Request!');
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', $e->getMessage());
        }
    }
}
