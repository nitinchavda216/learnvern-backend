<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function index()
    {
        if (auth()->guard('admins')->check()) {
            return redirect()->route('dashboard');
        }
        return view('login');
    }

    public function authenticateAdmin(Request $request)
    {

        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);
        if (auth()->guard('admins')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            $user = getCurrentAdmin();
            $redirectRoute = ($user->relatedRole->user_type =='crm') ? 'crm.dashboard' : 'dashboard';
            return redirect()->route($redirectRoute);
        } else {
            return redirect()->back()->with('error', "Invalid Credentials!");
        }
    }
    public function logout()
    {
        Auth::guard('admins')->logout();
        Session::flush();
        return redirect()->route('login');
    }








}
