<?php

namespace App\Http\Controllers;

use App\Interfaces\LandingPagesRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LandingPagesController extends Controller
{
    protected $landingPagesRepository;

    public function __construct(LandingPagesRepositoryInterface $landingPagesRepository)
    {
        $this->landingPagesRepository = $landingPagesRepository;
    }

    public function index()
    {
        return view('landing-pages.index');
    }

    public function getAjaxListData(Request $request)
    {
        return $this->landingPagesRepository->getAllLandingPages($request);
    }

    public function getCourseDetails($id)
    {
        $course = $this->landingPagesRepository->getCourseDetails($id);
        return response()->json(['status' => true, 'course_data' => $course]);
    }

    public function create()
    {
        $courses = $this->landingPagesRepository->getCourseTitles();
        return view('landing-pages.create', compact('courses'));
    }

    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        $this->validate($request, [
            'slug' => "required|unique:landing_pages,slug",
            'title' => 'required',
            'description' => 'required',
            'course_id' => 'required',
            'video_url' => 'required',
            'what_you_learn' => 'required',
            'what_you_get' => 'required',
        ],[
            'course_id.required' => "Course id required."
        ]);
        $this->landingPagesRepository->storeLandingPage($request);
        return redirect()->route('landing-pages')->with('success', "Data Created Successfully!");
    }

    public function edit($id, Request $request)
    {
        $courses = $this->landingPagesRepository->getCourseTitles($id);
        $landing_page = $this->landingPagesRepository->getLandingPagesDetails($id);
        return view('landing-pages.edit', compact('courses', 'landing_page'));
    }

    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        $this->validate($request, [
            'slug' => "required|unique:landing_pages,slug," . $id,
            'title' => 'required',
            'description' => 'required',
            'course_id' => 'required',
            'video_url' => 'required',
            'what_you_learn' => 'required',
            'what_you_get' => 'required',
        ],[
            'course_id.required' => "Course id required."
        ]);
        $this->landingPagesRepository->updateLandingPage($request, $id);
        return redirect()->route('landing-pages')->with('success', "Data Updated Successfully!");
    }

    public function delete($id): \Illuminate\Http\RedirectResponse
    {
        $this->landingPagesRepository->deleteLandingPage($id);
        return redirect()->route('landing-pages')->with('success', "Data Deleted Successfully!");
    }
}
