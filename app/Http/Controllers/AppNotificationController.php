<?php

namespace App\Http\Controllers;

use App\Models\UserFcmToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\PushNotificationService;

class AppNotificationController extends Controller
{
    protected $pushNotificationService;

    public function __construct(PushNotificationService $pushNotificationService)
    {
        $this->pushNotificationService = $pushNotificationService;
    }
    public function index() {
        $addData = [
                        'image' => 'https://learnvernadmin.urvam.com/storage/course/app_image/77019.png',
                        'data' =>  ['app_dynamic_link' => env('FCM_DYNAMIC_LINK_DOMAIN').'?id=2595&type=course']
                   ];
        $fcmTokens = UserFcmToken::pluck('fcm_token')->toArray();
//        $fcmTokens = ['c3kSdg3PQTezJkQIjHl6v7:APA91bFnJN6Eh8t2Mjd4K6SJiXW4ZUraCJpVlhBsuGUEChsgvPN_ESZHqSxBZiax4CyKC4VCNUWA56lvqbvNZd9Sg-p74af1YkMumLMolwg-QYRgqCrSL8dFdzTeUPchmsnEUCeK-Kq8'];
        if (!empty($fcmTokens)){
            $title = "Core PHP Course";
            $text = 'Core PHP course will teach you to the Basics of PHP with Proper Theory and practical Examples and code real life projects.';
            $response = $this->pushNotificationService->notify_push($fcmTokens, $title, $text, $addData);
            dd($response);
        }
        dd('Token Not Available.');
        return view("app-notification.index");
    }
}
