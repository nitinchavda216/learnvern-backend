<?php

namespace App\Http\Controllers;

use App\Interfaces\LocationRepositoryInterface;
use App\Interfaces\PartnerApplicationRepositoryInterface;
use App\Interfaces\QuizRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\CoursesRepositoryInterface;
use App\Interfaces\CertificateRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use PDF;


class UsersController extends Controller
{
    protected $userRepository;
    protected $coursesRepository;
    protected $quizRepository;
    protected $locationRepository;
    protected $certificateRepository;
    protected $partnerApplicationRepository;

    public function __construct(UserRepositoryInterface $userRepository,
                                CoursesRepositoryInterface $coursesRepository,
                                PartnerApplicationRepositoryInterface $partnerApplicationRepository,
                                QuizRepositoryInterface $quizRepository,
                                LocationRepositoryInterface $locationRepository,
                                CertificateRepositoryInterface $certificateRepository)
    {
        $this->userRepository = $userRepository;
        $this->coursesRepository = $coursesRepository;
        $this->quizRepository = $quizRepository;
        $this->locationRepository = $locationRepository;
        $this->certificateRepository = $certificateRepository;
        $this->partnerApplicationRepository = $partnerApplicationRepository;
    }

    public function search(Request $request): JsonResponse
    {
       $data = $this->userRepository->getCourseSearchListData($request);
        return response()->json($data);
    }

    public function index()
    {
        $partners = $this->partnerApplicationRepository->getPartnersName();
        return view('user.index', compact('partners'));
    }

    public function getNSDCCertificate(Request $request)
    {
        $response = $this->userRepository->getNSDCCertificate($request);
        if ($response['status']) {
            return $response['data']->stream();
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function getAjaxListData(Request $request)
    {
        return $this->userRepository->getAjaxListData($request);
    }

    public function updateStatus(Request $request)
    {
        $this->userRepository->updateBlockStatus($request);
        return response()->json(['status' => true]);
    }

    public function getUserByRegister(Request $request)
    {
        return $this->userRepository->getUserByRegister($request);
    }

    public function getUserDetails(Request $request, $id)
    {
        $active_tab = $request->get('tab');
        $user = $this->userRepository->getUserDetails($id);
        $countries = $this->locationRepository->getCountries();
        $states = $this->locationRepository->getStates($user->country_id);
        $cities = $this->locationRepository->getCities($user->state_id);
        $districts = $this->locationRepository->getDistrict($user->district_id);
        $certificates = $this->certificateRepository->getCertificate($id);
        $course_user = [];
        $results = $this->userRepository->getUserQuizResults($id);
        $referralCount = $this->userRepository->getTotalReferral($user->own_referral_code);
        return view('user.detail', compact('user', 'course_user', 'results', 'countries', 'states', 'cities', 'districts', 'active_tab', 'referralCount', 'certificates'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'salutation' => 'required',
            'first_name' => 'required',
            'father_name' => 'required',
            'mobile_number' => 'required|min:10',
            'gender' => 'required',
            'birth_date' => 'required',
            'cast_category' => 'required',
            'address' => 'required',
            'id_proof' => 'required',
            'id_number' => 'required',
            'country_id' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'username' => 'string|max:255|unique:users,username,' . $id . ',id,deleted_at,NULL',
        ], [
            'country_id.required' => "Please select a country.",
            'state_id.required' => "Please select a state.",
            'city_id.required' => "Please select a city.",
        ]);
        $this->userRepository->updateUser($request, $id);
        return redirect()->route('users')->with('success', "Data Created Successfully!");

    }

    public function getCourse(Request $request)
    {
        $this->userRepository->getCourses($request);
    }

    public function getNSDCCandidatesList()
    {
        $partners = $this->partnerApplicationRepository->getPartnersName();
        return view('nsdc-candidate.index', compact('partners'));
    }

    public function getNSDCCandidatesListAjaxData(Request $request)
    {
        return $this->userRepository->getNSDCCandidatesList($request);
    }

    public function getInternationalCandidatesList()
    {
        $partners = $this->partnerApplicationRepository->getPartnersName();
        return view('international_candidate.index', compact('partners'));
    }

    public function getInternationalCandidatesLisAjaxData(Request $request)
    {
        return $this->userRepository->getInternationalCandidatesList($request);
    }

    public function getNSDCExportData(Request $request)
    {
        $response = $this->userRepository->getNSDCExportData($request);
        if (!$response['status']) {
            return redirect()->back()->with('error', 'Record Not Found! Please select different date.');
        } else {
            return Excel::download($response['data'], 'NSDC_Candidates.xlsx');
        }
    }

    public function getInternationalExportData(Request $request)
    {
        $response = $this->userRepository->getInternationalExportData($request);
        if (!$response['status']) {
            return redirect()->back()->with('error', 'Record Not Found! Please select different date.');
        } else {
            return Excel::download($response['data'], 'International_Candidates.xlsx');
        }
    }

    public function convertInternationalToNSDC(Request $request)
    {
        $this->coursesRepository->convertInternationalToNSDC($request);
        return response()->json(['status' => true]);
    }

    public function getNSDCUploadData(Request $request)
    {
        if (!isset($request->all()['file'])) {
            return redirect()->route('nsdc_candidate')->with(['error' => 'Please upload an excel file.', 'openModal' => true]);
        }
        $response = $this->userRepository->getNSDCUploadData($request);
        if ($response['status']) {
            return redirect()->route('nsdc_candidate')->with('success', $response['message']);
        } else {
            return redirect()->route('nsdc_candidate')->with('error', $response['message']);
        }
    }

    public function downloadAmbassadorCertificate($user_id, $referral_count)
    {
        $response = $this->userRepository->downloadAmbassadorCertificate($user_id, $referral_count);
        if ($response['status']) {
            return $response['data']->download($response['name']);
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function updateDate(Request $request)
    {
        $request->validate([
            'start_date' => 'required',
            'end_date' => "required|after_or_equal:start_date",
        ], [
            'start_date.required' => "Please Enter the Start Date",
            'end_date_date.required' => "Please Enter the End Date"
        ]);
        $this->userRepository->updateNSDCCertificateDate($request);
        $returnHtml = view('nsdc-candidate.index')->render();
        return response()->json(['status' => true, 'html' => $returnHtml]);
    }

    public function activateUser($activation_key)
    {
        $this->userRepository->activateUser($activation_key);
        return redirect(env('FRONT_URL'));
    }

    public function facebookUserDeleted(Request $request)
    {
        $date = formatDate(Carbon::now(), 'd_m_Y');
        $logEbayURL = new Logger('Deleted Facebook Users');
        $logEbayURL->pushHandler(new StreamHandler(storage_path('logs/deleted_facebook_users_' . $date . '.log')), Logger::ERROR);
        $logEbayURL->info('Deleted User:', [$request->all()]);
        return true;
    }

}
