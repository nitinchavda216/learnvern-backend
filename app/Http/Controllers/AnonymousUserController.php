<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\AnonymousUserInterface;
use App\Models\AnonymousUser;

class AnonymousUserController extends Controller
{
    //
    protected $anonymousUserRepository;

    public function __construct(AnonymousUserInterface $anonymousUserRepository)
    {
        $this->anonymousUserRepository = $anonymousUserRepository;


    }

    public function index()
    {
       return view('anonymous_user.index');
    }

    public function getAjaxListData(Request $request)
    {
        return $this->anonymousUserRepository->getAjaxListData($request);
    }
}
