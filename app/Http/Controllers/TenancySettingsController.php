<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\TenancyInterface;
use App\Models\PartnersSettings;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class TenancySettingsController extends Controller
{
    //
    protected $tenancyRepository;

    public function __construct(TenancyInterface $tenancyRepository)
    {
        $this->tenancyRepository = $tenancyRepository;


    }
    public function index()
    {
        $PartnersSettings=PartnersSettings::first();
        return view('tenancy-settings.index',compact('PartnersSettings'));
    }

    public function store(Request $request)
    {
        $this->tenancyRepository->storeSettings($request);
        return redirect()->route('tenancy-settings')->with('success', 'Settings Updated Successfully.');
    }


}
