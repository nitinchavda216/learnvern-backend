<?php

namespace App\Http\Controllers;

use App\Interfaces\CacheRepositoryInterface;
use App\Interfaces\CategoryRepositoryInterface;
use App\Interfaces\LanguageRepositoryInterface;
use App\Interfaces\WebinarsRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WebinarsController extends Controller
{
    protected $webinarsRepository, $languageRepository, $categoryRepository, $cacheRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository, WebinarsRepositoryInterface $webinarsRepository,
                                LanguageRepositoryInterface $languageRepository, CacheRepositoryInterface $cacheRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->languageRepository = $languageRepository;
        $this->webinarsRepository = $webinarsRepository;
        $this->cacheRepository = $cacheRepository;
    }

    public function index()
    {
        $categories = $this->categoryRepository->getWebinarCategoriesTitles();
        return view('webinars.index', compact('categories'));
    }

    public function getAjaxListData(Request $request)
    {
        return $this->webinarsRepository->getWebinarCourseList($request);
    }

    public function create()
    {
        $categories = $this->categoryRepository->getWebinarCategoriesTitles();
        $courses = $this->webinarsRepository->getCourseTitles();
        $languages = $this->languageRepository->getLanguageTitles();
        return view('webinars.create', compact('courses', 'categories', 'languages'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'slug' => "required|unique:courses,slug"
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error', "This slug already exists. Try with another slug.");
        }
        $this->webinarsRepository->storeCourse($request);
        return redirect()->route('webinars')->with('success', "Data Created Successfully!");
    }

    public function edit($id)
    {
        $categories = $this->categoryRepository->getWebinarCategoriesTitles();
        $course = $this->webinarsRepository->getCourseDetails($id);
        $languages = $this->languageRepository->getLanguageTitles();
        return view('webinars.edit', compact('course', 'categories', 'languages'));
    }

    public function update(Request $request, $id)
    {
        $this->webinarsRepository->updateCourse($request, $id);
        if (env('REDIS_CACHE_ENABLED')){
            $flushTags = [
                'api_category_wise_courses',
                'api_all_courses',
                'api_courses_by_category_'.$request->category_id,
                'api_popular_courses',
                'api_course_faqs_'.$id,
                'api_course_details_'.$id,
                'api_course_testimonials_'.$id,
            ];
            $this->cacheRepository->removeCacheByTag($flushTags);
        }
        return redirect()->route('webinars')->with('success', "Data Updated Successfully!");
    }

    public function updateStatus(Request $request)
    {
        $this->webinarsRepository->updateActiveStatus($request);
        return response()->json(['status' => true]);
    }

}
