<?php

namespace App\Http\Controllers;

use App\Interfaces\PartnerApplicationRepositoryInterface;
use App\Interfaces\PaymentRepositoryInterface;
use Illuminate\Http\Request;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class PaymentController extends Controller
{
    protected $paymentRepository;
    protected $partnerApplicationRepository;
    public function __construct(PaymentRepositoryInterface $paymentRepository, PartnerApplicationRepositoryInterface $partnerApplicationRepository)
    {
        $this->paymentRepository = $paymentRepository;
        $this->partnerApplicationRepository = $partnerApplicationRepository;
    }

    public function index()
    {
        $partners = $this->partnerApplicationRepository->getPartnersName();
        return view('payment.index', compact('partners'));
    }

    public function getAjaxData(Request $request)
    {
        return $this->paymentRepository->getPaymentHistoryList($request);
    }

    public function sendPaymentLink(Request $request)
    {
        $response = $this->paymentRepository->sendPaymentLink($request);
        return response()->json($response);
    }

    public function doPayment(Request $request)
    {
        $response = $this->paymentRepository->paymentSuccess($request);
        if ($response['status']){
            return redirect($response['url']);
        }
        $logEbayURL = new Logger('Captured Payment Details');
        $logEbayURL->pushHandler(new StreamHandler(storage_path('logs/payment_transactions.log')), Logger::Error);
        $logEbayURL->info('Payment Details:', ['message' => $response['message']]);
        return redirect(env('FRONT_URL'))->with('toast_error', $response['message']);
    }

    public function paymentCaptured(Request $request)
    {
        $logEbayURL = new Logger('Captured Payment Details');
        $logEbayURL->pushHandler(new StreamHandler(storage_path('logs/payment_transactions.log')), Logger::INFO);
        $logEbayURL->info('Payment Details:', ['data' => $request->all()]);
        return true;
    }
}
