<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\Api\BannerRepositoryInterface;
use App\Interfaces\Api\ReferralInterface;
use Illuminate\Http\Request;

class ReferralController extends Controller
{
    protected $referralRepository;
    protected $bannerRepository;

    public function __construct(ReferralInterface $referralRepository, BannerRepositoryInterface $bannerRepository)
    {
        $this->referralRepository = $referralRepository;
        $this->bannerRepository = $bannerRepository;
    }

    public function getReferralInfo(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $data['referral_info_banner'] = $this->bannerRepository->getBannerImageByType('referral_info_banner');
            $data['ambassador_settings'] = $this->referralRepository->getAmbassadorSettings();
            $data['recent_ambassadors'] = $this->referralRepository->getRecentAmbassadors();
            $data['faq'] = $this->referralRepository->getFaqs();
            if (isset($request['user_id'])) {
                $data['user'] = $this->referralRepository->getAffiliateUser($request->user_id);
                $data['user']['next_level_badge_exist'] = isset($data['ambassador_settings'][$data['user']['ambassador_badge_id']]);
                $next_level_badge_details = $data['user']['next_level_badge_exist'] ? $data['ambassador_settings'][$data['user']['ambassador_badge_id']] : null;
                if (isset($next_level_badge_details)) {
                    $data['user']['next_level_badge_title'] = $next_level_badge_details['title'];
                    $data['user']['left_users_count'] = $next_level_badge_details['referrals_count'] - $data['user']['referred_count'];
                }
                if (!is_null($data['user']['ambassador_badge_id'])) {
                    $data['ambassador_settings'] = $this->referralRepository->getUserAmbassadorSettings($data['user']['id']);
                }
                $data['my_referrals'] = $this->referralRepository->myReferrals($data['user']['own_referral_code']);
            }
            return response()->json(['status' => 200, 'message' => "Referral Info.", 'data' => $data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }
}
