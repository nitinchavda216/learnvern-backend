<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\Api\CoursesRepositoryInterface;
use App\Interfaces\Api\PaymentInterface;
use App\Interfaces\CacheRepositoryInterface;
use App\Interfaces\ReviewRepositoryInterface;
use App\Models\AbandonedPaymentReport;
use App\Models\AbandonedPaymentReports;
use App\Models\User;
use App\Services\RazorpayService;
use Illuminate\Http\Request;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class PaymentController extends Controller
{
    protected $razorpayService, $paymentRepository, $coursesRepository, $cacheRepository;

    public function __construct(CoursesRepositoryInterface $coursesRepository,
                                RazorpayService $razorpayService,
                                PaymentInterface $paymentRepository, CacheRepositoryInterface $cacheRepository)
    {
        $this->coursesRepository = $coursesRepository;
        $this->paymentRepository = $paymentRepository;
        $this->razorpayService = $razorpayService;
        $this->cacheRepository = $cacheRepository;
    }

    public function generatePaymentId(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $response = $this->razorpayService->getOrderId($request->all());
            if ($response['status'] == 100) {
                throw new \Exception($response['message']);
            }
            return response()->json([
                'status' => 200,
                'message' => $response['message'],
                'data' => $response['data']
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function doPayment(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $requestData = $request->all();
            $response = $this->paymentRepository->doPayment($requestData);
            if (!$response['status']) {
                throw new \Exception($response['message']);
            }
            if (env('REDIS_CACHE_ENABLED')){
                $flushTags = [
                    'user_courses_'.$request->user_id,
                    'api_course_details_'.$request->course_id,
                ];
                $this->cacheRepository->removeCacheByTag($flushTags);
            }
            return response()->json([
                'status' => 200,
                'message' => $response['message'],
                'data' => $response['data']
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function abandonedPayment(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $this->paymentRepository->abandonedPayment($request->all());
            return response()->json(['status' => 200, 'message' => 'Abandoned Payment Entry Created Successfully.']);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function paymentCaptured(Request $request)
    {
        $logEbayURL = new Logger('Razorpay Payment');
        $logEbayURL->pushHandler(new StreamHandler(storage_path('logs/payment_captured.log')), Logger::INFO);
        $logEbayURL->info('Payment Info:', [$request->all()]);
    }
}
