<?php

namespace App\Http\Controllers\Api;

use App\Events\ContactFormSubmissionEvent;
use App\Interfaces\Api\CollegeInterface;
use App\Interfaces\Api\CoursesRepositoryInterface;
use App\Interfaces\Api\ReferralInterface;
use App\Interfaces\Api\UserRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\Api\BannerRepositoryInterface;

class HomeController extends Controller
{
    protected $bannerRepository;
    protected $coursesRepository, $referralRepository, $collegeRepository;

    public function __construct(ReferralInterface $referralRepository,
                                BannerRepositoryInterface $bannerRepository,
                                CoursesRepositoryInterface $coursesRepository,
                                CollegeInterface $collegeRepository)
    {
        $this->bannerRepository = $bannerRepository;
        $this->coursesRepository = $coursesRepository;
        $this->referralRepository = $referralRepository;
        $this->collegeRepository = $collegeRepository;
    }

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $data['promotion_banner'] = $this->bannerRepository->getPromotionBanners();
            $data['recommended_courses'] = $data['continue_course'] = [];
            if (isset($request->user_id)) {
                $data['continue_course'] = $this->coursesRepository->getUserContinueCoursesList($request->user_id);
                $data['recommended_courses'] = $this->coursesRepository->getRecommendedCourses($request->user_id, null);
            }
            $data['top_courses'] = $this->coursesRepository->getTopCourseWithCategoryWise();
            $data['top_searches'] = $this->coursesRepository->getTopSearchCourseList();
            $data['upcoming_webinars'] = $this->coursesRepository->upcomingWebinarList();
            $data['all_courses'] = $this->coursesRepository->allCourseList();
            $data['popular_courses'] = $this->coursesRepository->popularCourseList();
            return response()->json([
                'status' => 200,
                'message' => "Home Page Data Sent Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function storeContactRequest(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            event(new ContactFormSubmissionEvent($request->all()));
            return response()->json([
                'status' => 200,
                'message' => config('app_messages.contact_form_submit.description'),
                'header_message' => config('app_messages.contact_form_submit.title'),
                'data' => null
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }
}
