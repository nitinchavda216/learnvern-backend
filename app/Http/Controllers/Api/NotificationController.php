<?php

namespace App\Http\Controllers\Api;

use App\Interfaces\Api\NotificationRepositoryInterface;
use App\Interfaces\Api\UserRepositoryInterface;
use App\Services\RapidSMSService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    protected $notificationRepository, $rapidSMSService, $userRepository;

    public function __construct(NotificationRepositoryInterface $notificationRepository, RapidSMSService $rapidSMSService, UserRepositoryInterface $userRepository)
    {
        $this->notificationRepository = $notificationRepository;
        $this->rapidSMSService = $rapidSMSService;
        $this->userRepository = $userRepository;
    }

    public function getUserNotificationList(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $returnData = $this->notificationRepository->getUserNotificationList($request->user_id);
            return response()->json(['status' => 200, 'message' => 'Notifications List.', 'data' => $returnData['data'], 'unread_notifications_count' => $returnData['unread_count']]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function readNotification(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $this->notificationRepository->readNotification($request);
            $returnData = $this->notificationRepository->getUserNotificationList($request->user_id);
            return response()->json(['status' => 200, 'message' => 'Read Notification Successfully.', 'data' => $returnData['data'], 'unread_notifications_count' => $returnData['unread_count']]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function deleteNotification(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $this->notificationRepository->deleteNotification($request);
            $returnData = $this->notificationRepository->getUserNotificationList($request->user_id);
            return response()->json(['status' => 200, 'message' => 'Delete Notification Successfully.', 'data' => $returnData['data'], 'unread_notifications_count' => $returnData['unread_count']]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function sendOtp(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $validateResponse = $this->userRepository->validateMobileNumber($request);
            if (!empty($validateResponse) && $validateResponse['status'] == false) {
                $errors = $validateResponse['result']->errors();
                return response()->json(['status' => 100, 'message' => $errors->all()[0], 'header_message' => config('app_messages.send_otp.error.title'), 'data' => null]);
            }
            $otp = random_int(100000, 999999);
            $data = [
                'mobile_number' => $request['mobile_number'],
                'otp_code' => $otp
            ];
            if (env('ENABLE_RAPID_SMS')){
                $this->rapidSMSService->sendSMS($data);
            }
            return response()->json(['status' => 200, 'message' => config('app_messages.send_otp.success.description'), 'header_message' => config('app_messages.send_otp.success.title'), 'data' => $data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => config('app_messages.send_otp.error.description'), 'header_message' => config('app_messages.send_otp.error.title'), 'data' => null]);
        }
    }
}
