<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Country;
use App\Models\State;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class LocationController extends Controller
{

    public function __construct()
    {

    }

    public function getCityDetails(Request $request): \Illuminate\Http\JsonResponse
    {
        $isd_code = $request->get('isd_code');
        $states = State::whereHas('countryDetail', function ($q) use ($isd_code){
            $q->where('isd_code', $isd_code);
        })->pluck('id')->toArray();
        $city_list = City::whereIn('state_id',$states)->groupBy('name')->orderBy('name')->get();
        return response()->json([
            'status' => 200,
            'message' => "City Data List",
            'data' => $city_list
        ]);
    }


}
