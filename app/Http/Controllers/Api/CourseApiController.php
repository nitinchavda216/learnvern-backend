<?php

namespace App\Http\Controllers\Api;

use App\Interfaces\Api\CoursesRepositoryInterface;
use App\Interfaces\Api\QuizRepositoryInterface;
use App\Interfaces\CacheRepositoryInterface;
use App\Interfaces\ReviewRepositoryInterface;
use App\Models\User;
use App\Services\MauticsService;
use App\Services\RazorpayService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CourseApiController extends Controller
{
    protected $coursesRepository;
    protected $reviewRepository, $razorpayService, $cacheRepository, $quizRepository, $mauticsService;

    public function __construct(CoursesRepositoryInterface $coursesRepository,
                                ReviewRepositoryInterface $reviewRepository,
                                RazorpayService $razorpayService, CacheRepositoryInterface $cacheRepository,
                                QuizRepositoryInterface $quizRepository, MauticsService $mauticsService)
    {
        $this->coursesRepository = $coursesRepository;
        $this->reviewRepository = $reviewRepository;
        $this->razorpayService = $razorpayService;
        $this->cacheRepository = $cacheRepository;
        $this->quizRepository = $quizRepository;
        $this->mauticsService = $mauticsService;
    }

    public function getCoursesByCategory(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $response = $this->coursesRepository->getCoursesByCategory($request->category_id);
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function courseDetails(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $requestData = $request->all();
            $course_id = $requestData['course_id'] ?? null;
            $user_id = $requestData['user_id'] ?? null;
            if (isset($course_id)) {
                $courseDetail = $this->coursesRepository->getCourseDetails($course_id);
                if (isset($courseDetail)) {
                    $returnData = $this->coursesRepository->getViewPageCourseDetails($courseDetail, $course_id, $user_id);
                    if ($returnData['course_type'] != 4) {
                        $returnData['faqs'] = $this->coursesRepository->getFaqsCourse($course_id);
                        $returnData['recommended_course'] = $this->coursesRepository->getRecommendedCourses($user_id, $course_id);
                    }
                    $returnData['testimonials'] = $this->coursesRepository->getTestimonials($course_id);

                    return response()->json(['status' => 200, 'message' => 'Course Details.', 'data' => $returnData]);
                } else {
                    return response()->json(['status' => 100, 'message' => 'Invalid Course.', 'data' => null]);
                }
            } else {
                return response()->json(['status' => 100, 'message' => 'Course id is empty.', 'data' => null]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function getMyCourses(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $data['my_courses'] = $this->coursesRepository->getUserCoursesList($request->user_id);
            $data['continue_course'] = $this->coursesRepository->getUserContinueCoursesList($request->user_id);
            $data['recommended_courses'] = $this->coursesRepository->getRecommendedCourses($request->user_id, null);
            $userDetails = User::where('id', $request->user_id)->select('id', 'isd_code', 'country')->first();
            $is_international_user = isset($userDetails) && !in_array($userDetails->isd_code, ['91', '+91']);
            $data['upgrade_course_modal_details'] = [
                'certificate_image' => $is_international_user ? env('FRONT_URL') . 'images/International_Certificate_View.png' : env('FRONT_URL') . 'images/sample_certificate.jpg',
                'title' => "Benefits of Certificate" . (!$is_international_user ? " NSDC" : ""),
                'description' => [
                    "Approved for Summer Training/Internships",
                    "Approved in Organization's For Jobs",
                    "Lifetime Value" . (!$is_international_user ? " and Verification on Skill India Portal" : ""),
                    "Present Certificate at all Interviews"
                ]
            ];
            return response()->json(['status' => 200, 'message' => "My courses list.", 'data' => $data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function courseEnroll(Request $request): \Illuminate\Http\JsonResponse
    {
        DB::beginTransaction();
        try {
            $enrollResponse = $this->coursesRepository->courseEnroll($request);
            DB::commit();
            if (env('REDIS_CACHE_ENABLED')) {
                $flushTags = [
                    'user_courses_' . $request->user_id,
                    'api_course_details_' . $request->course_id,
                ];
                $this->cacheRepository->removeCacheByTag($flushTags);
            }
            if (!is_null($enrollResponse['data'])) {
                $enrollResponse['data']['my_courses'] = $this->coursesRepository->getUserCoursesList($request->user_id);
                $enrollResponse['data']['continue_course'] = $this->coursesRepository->getUserContinueCoursesList($request->user_id);
                return response()->json($enrollResponse);
            } else {
                $courseDetail = $this->coursesRepository->getCourseDetails($request->course_id);
                $returnData = $this->coursesRepository->getViewPageCourseDetails($courseDetail, $request->course_id, $request->user_id);
                $returnData['testimonials'] = $this->coursesRepository->getTestimonials($request->course_id);
                return response()->json(['status' => 200, 'message' => 'Course Details.', 'data' => $returnData]);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function videoProgress(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $requestData = $request->all();
            $requestData['progress'] = round($requestData['progress']);
            if ($request->curriculum_type == 'assignment') {
                $this->coursesRepository->setAssignmentProgress($requestData);
            } elseif ($request->curriculum_type == 'unit') {
                $this->coursesRepository->saveUnitProgress($requestData);
            }
            $result = ['status' => 200, 'message' => 'Video Progress Stored Successfully.', 'data' => $response['data'] ?? []];
        } catch (\Exception $e) {
            $result = ['status' => 100, 'message' => $e->getMessage(), 'data' => null];
        }
        return response()->json($result);
    }

    public function getUnitDetails(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $response = $this->coursesRepository->getUnitDetails($request->curriculum_id, $request->curriculum_type, $request->user_id);
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function markUnitAsCompleted(Request $request): \Illuminate\Http\JsonResponse
    {
        DB::beginTransaction();
        try {
            $response = $this->coursesRepository->markUnitAsCompleted($request);
            if ($response['status'] == 200) {
                $this->mauticsService->storeUserData($request->user_id);
                $returnData = $response['otherData'];
                $finalData['next_curriculum_details'] = $response['data'];
                $finalData['show_course_complete_screen'] = $returnData['show_course_complete_screen'];
                if (!$returnData['show_course_complete_screen']) {
                    if ($finalData['next_curriculum_details']['curriculum_type'] == 'quiz') {
                        $quizResponse = $this->quizRepository->quizDetails($finalData['next_curriculum_details']['curriculum_id'], $request->user_id);
                        $redirectUnitData = $this->coursesRepository->getNextCurriculumDetails($quizResponse['data']['course_id'], $request->user_id, $finalData['next_curriculum_details']['curriculum_id'], 'quiz');
                        $quizResponse['data']['next_curriculum_details'] = $redirectUnitData;
                        $finalData['curriculum_response'] = $quizResponse['data'];
                    } else {
                        $unitResponse = $this->coursesRepository->getUnitDetails($finalData['next_curriculum_details']['curriculum_id'], $finalData['next_curriculum_details']['curriculum_type'], $request->user_id);
                        $finalData['curriculum_response'] = $unitResponse['data'];
                        $finalData['curriculum_response']['show_referral_popup'] = $returnData['show_referral_popup'];
                    }
                } else {
                    $finalData['course_complete_screen_details'] = $returnData;
                }
                DB::commit();
                if (env('REDIS_CACHE_ENABLED')) {
                    $flushTags = [
                        'user_courses_' . $request->user_id,
                    ];
                    $this->cacheRepository->removeCacheByTag($flushTags);
                }
                return response()->json(['status' => 200, 'message' => "Mark this completed successfully.", 'data' => $finalData]);
            } else {
                return response()->json($response);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function storeOfflineProgressData(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $this->coursesRepository->storeOfflineProgressData($request);
            $this->mauticsService->storeUserData($request->user_id);
            return response()->json(['status' => 200, 'message' => "Offline data stored successfully.", 'data' => null]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function courseReviewRating(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $data = $this->reviewRepository->createReview($request);
            if (env('REDIS_CACHE_ENABLED')) {
                $flushTags = [
                    'api_course_details_' . $request->course_id,
                    'api_course_testimonials_' . $request->course_id,
                ];
                $this->cacheRepository->removeCacheByTag($flushTags);
            }
            $this->mauticsService->storeUserData($request->user_id);
            return response()->json([
                'status' => 200,
                'message' => "Course Review Added.",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }
}
