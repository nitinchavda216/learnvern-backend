<?php

namespace App\Http\Controllers\Api;

use App\Interfaces\Api\UserRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CertificateController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getCertificates(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $response = $this->userRepository->getUserCertificatesList($request);
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function downloadCertificate(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $response = $this->userRepository->downloadCertificate($request);
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function verifyCertificate(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $response = $this->userRepository->verifyCertificate($request);
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }
}
