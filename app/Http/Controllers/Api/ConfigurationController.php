<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\Api\CoursesRepositoryInterface;
use App\Interfaces\Api\UserRepositoryInterface;
use App\Interfaces\AppUpdateRepositoryInterface;
use App\Interfaces\SiteConfigurationsRepositoryInterface;
use App\Interfaces\Api\BannerRepositoryInterface;
use App\Models\AppInstallationDetails;
use App\Models\UserFcmToken;
use App\Models\UserResumeDetails;
use Illuminate\Http\Request;

class ConfigurationController extends Controller
{
    protected $siteConfigurationsRepository;
    protected $coursesRepository;
    protected $bannerRepository;
    protected $userRepository;
    protected $appUpdateRepository;

    public function __construct(SiteConfigurationsRepositoryInterface $siteConfigurationsRepository,
                                CoursesRepositoryInterface $coursesRepository, UserRepositoryInterface $userRepository,
                                BannerRepositoryInterface $bannerRepository, AppUpdateRepositoryInterface $appUpdateRepository)
    {
        $this->siteConfigurationsRepository = $siteConfigurationsRepository;
        $this->coursesRepository = $coursesRepository;
        $this->bannerRepository = $bannerRepository;
        $this->userRepository = $userRepository;
        $this->appUpdateRepository = $appUpdateRepository;
    }

    public function index(): \Illuminate\Http\JsonResponse
    {
        try {
            $data['site_configurations'] = $this->siteConfigurationsRepository->getConfigurationForApp();
            $data['site_configurations']['webinar_certificate'] = env('FRONT_URL') . 'images/sample_free_certificate.jpg';
            $data['site_configurations']['ambassador_program_file_url'] = env('FRONT_URL') . 'uploads/learnvern_ambassador.pdf';
            $data['site_configurations']['nsdc_agreement_file_url'] = env('FRONT_URL') . 'uploads/NSDC_Agreement.pdf';
            $data['site_configurations']['sample_international_certificate'] = env('FRONT_URL') . 'images/International_Certificate_View.png';
            $data['site_configurations']['stripe_price'] = 4999;
            $data['site_configurations']['enable_google_auth'] = true;
            $data['site_configurations']['enable_facebook_auth'] = true;
            $data['site_configurations']['enable_apple_auth'] = true;
            $ipDetails = getUserIpDetails(getUserIP());
            $data['site_configurations']['default_isd_code'] = $ipDetails['location']['calling_code'] ?? 91;
            $data['site_configurations']['default_city'] = $ipDetails['city'] ?? null;
            $data['site_configurations']['deeplink_base_url'] = env('FCM_DYNAMIC_LINK_DOMAIN');
            $data['page_types'] = $this->siteConfigurationsRepository->getPageType();
            $data['mobile_page_types'] = [
                ['page_title' => 'About LearnVern', 'page_type' => 'about', 'icon_library' => 4, 'icon_name' => 'ios-information-circle-outline'],
                ['page_title' => 'Employers', 'page_type' => 'employers', 'icon_library' => 3, 'icon_name' => 'account-tie'],
                ['page_title' => 'Ambassador', 'page_type' => 'ambassador', 'icon_library' => 4, 'icon_name' => 'ribbon-outline'],
                ['page_title' => 'Privacy Policy', 'page_type' => 'privacy-policy-facebook', 'icon_library' => 3, 'icon_name' => 'folder-information-outline'],
            ];
            $data['contact_request_messages'] = [
                'whats_up_message' => 'Hello LearnVern',
                'email_message' => [
                    'subject' => 'Contact Request From App',
                    'body' => "Hello LearnVern"
                ]
            ];
            $data['country_list'] = $this->siteConfigurationsRepository->getCountry();
            $data['country_code_list'] = $this->siteConfigurationsRepository->getIsdCode();
            $data['category_list'] = $this->siteConfigurationsRepository->getCategoriesList();
            $data['banner_list'] = $this->bannerRepository->getConfigurationBanners();
            $data['application_assets'] =[
                'skeleton' => url('images/application-assets/skeleton.gif')
            ];
            return response()->json([
                'status' => 200,
                'message' => "Configuration List!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function getCurrentVersionDetails(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $data = $this->appUpdateRepository->getAppLatestVersionDetails($request);
            $data['play_store_link'] = '';
            $data['app_store_link'] = '';
            return response()->json([
                'status' => 200,
                'message' => "Latest Version Details!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function getFormDetails(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $infoConfig = config('info');
            $data['college_list'] = $this->siteConfigurationsRepository->getCollegeList($request->college_search ?? null);
            $data['genders'] = $infoConfig['genders'];
            $data['occupations'] = $infoConfig['occupations'];
            $data['marital_status'] = $infoConfig['marital_status'];
            $data['disability_categories'] = $infoConfig['disability_categories'];
            $data['cast_categories'] = $infoConfig['cast_categories'];
            $data['religions'] = $infoConfig['religions'];
            $data['education_level'] = $infoConfig['education_list'];
            $data['id_proof'] = $infoConfig['nsdc_id_proof'];
            $data['salutation'] = $infoConfig['salutations'];
            $data['employment_status'] = $infoConfig['employment_status'];
            $data['heard_about_us'] = $infoConfig['heard_about_us'];
            $data['city_list'] = $data['district_list'] = $data['state_list'] = [];
            if (isset($request->user_id)) {
                $user = $this->userRepository->getUserById($request->user_id);
                if (isset($user->country_id)) {
                    $data['state_list'] = $this->siteConfigurationsRepository->getState($user->country_id);
                }
                if (isset($user->state_id)) {
                    $data['city_list'] = $this->siteConfigurationsRepository->getCity($user->state_id);
                    $data['district_list'] = $this->siteConfigurationsRepository->getDistrict($user->state_id);
                }
                $resume = UserResumeDetails::query()->where('user_id', $request->user_id)->first();
                $data['previous_experiences'] = $resume->experience_details ?? [];
            }
            return response()->json([
                'status' => 200,
                'message' => "Form List Sent Successfully!",
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function getPageUrl($page_type): \Illuminate\Http\JsonResponse
    {
        try {
            $page_detail = $this->siteConfigurationsRepository->getPageUrl($page_type);
            if (isset($page_detail)) {
                return response()->json([
                    'status' => 200,
                    'message' => "Page Detail Web Successfully!",
                    'data' => [
                        "type" => $page_type,
                        "title" => isset($page_detail) && isset($page_detail['page_title']) ? $page_detail['page_title'] : '',
                        "page_url" => route('app-view.page-details', $page_type),
                        "content" => isset($page_detail) ? preg_replace('/(<[^>]+) style=".*?"/i', '$1', $page_detail['app_content']) : null,
                    ]
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => "Page not found!",
                    'data' => null]);
            }

        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function getPageAppView($page_type)
    {
        $page_detail = $this->siteConfigurationsRepository->getPageUrl($page_type);
        return view('app_view.page_details', compact('page_detail'));
    }

    public function stateDetails(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $page_detail = $this->siteConfigurationsRepository->getState($request->country_id);
            if (isset($page_detail)) {
                return response()->json([
                    'status' => 200,
                    'message' => "State Detail Successfully!",
                    'data' => $page_detail
                ]);
            } else {
                return response()->json([
                    'status' => 100,
                    'message' => "Page not found!",
                    'data' => null]);
            }

        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function cityDetails(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $page_detail['city_list'] = $this->siteConfigurationsRepository->getCity($request->state_id);
            $page_detail['district_list'] = $this->siteConfigurationsRepository->getDistrict($request->state_id);
            return response()->json([
                'status' => 200,
                'message' => "City & District Detail Successfully!",
                'data' => $page_detail
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function storeAppInstallationDetails(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $requestData = $request->all();
            AppInstallationDetails::create([
                'uuid' => $requestData['uuid'] ?? null,
                'ip_address' => getUserIP(),
                'serial' => $requestData['serial'] ?? null,
                'manufacturer' => $requestData['manufacturer'] ?? null,
                'version' => $requestData['version'] ?? null,
                'platform' => $requestData['platform'] ?? null,
                'model' => $requestData['model'] ?? null,
            ]);
            if (isset($requestData['fcm_token'])) {
                $tokenExist = UserFcmToken::where(['fcm_token' => $requestData['fcm_token']])->first();
                if (is_null($tokenExist)) {
                    UserFcmToken::create(['fcm_token' => $requestData['fcm_token']]);
                }
            }
            return response()->json([
                'status' => 200,
                'message' => "Installation Details Stored Successfully",
                'data' => null
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }
}
