<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\Api\CoursesRepositoryInterface;
use App\Interfaces\Api\QuizRepositoryInterface;
use App\Interfaces\CacheRepositoryInterface;
use App\Services\MauticsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuizController extends Controller
{
    protected $quizRepository, $coursesRepository, $cacheRepository, $mauticsService;

    public function __construct(QuizRepositoryInterface $quizRepository, CoursesRepositoryInterface  $coursesRepository,
        CacheRepositoryInterface $cacheRepository, MauticsService $mauticsService)
    {
        $this->quizRepository = $quizRepository;
        $this->coursesRepository = $coursesRepository;
        $this->cacheRepository = $cacheRepository;
        $this->mauticsService = $mauticsService;
    }

    public function quizDetails(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $response = $this->quizRepository->quizDetails($request->quiz_id, $request->user_id);
            if ($response['status'] == 200){
                $redirectUnitData = $this->coursesRepository->getNextCurriculumDetails($response['data']['course_id'], $request->user_id, $request->quiz_id, 'quiz');
                $response['data']['next_curriculum_details'] = $redirectUnitData;
            }
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function userQuizResultCourseList(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $response = $this->quizRepository->userQuizResultCourseList($request->user_id);
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function userQuizResultsByCourse(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $response = $this->quizRepository->userQuizResultsByCourse($request->user_id, $request->course_id);
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function getQuizResults(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $response = $this->quizRepository->getSingleQuizResults($request->user_id, $request->quiz_id);
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function submitQuiz(Request $request): \Illuminate\Http\JsonResponse
    {
        DB::beginTransaction();
        try {
            $user_id = $request->user_id;
            $course_id = $request->course_id;
            $curriculum_id = $request->quiz_id;
            $response = $this->quizRepository->submitQuiz($request);
            if ($response['status'] == 200){
                $this->mauticsService->storeUserData($request->user_id);
                if (env('REDIS_CACHE_ENABLED')){
                    $flushTags = [
                        'user_courses_'.$user_id,
                        'api_user_quiz_courses_'.$user_id,
                        'api_quiz_result_'.$user_id.'_'.$course_id,
                        'api_single_quiz_result_'.$user_id.'_'.$curriculum_id,
                    ];
                    $this->cacheRepository->removeCacheByTag($flushTags);
                }
                DB::commit();
                $quizResultResponse = $this->quizRepository->getSingleQuizResults($user_id, $curriculum_id);
                if ($quizResultResponse['status'] == 200){
                    $redirectUnitData = $this->coursesRepository->getNextCurriculumDetails($course_id, $user_id, $curriculum_id, 'quiz');
                    $quizResultResponse['data']['next_curriculum_details'] = $redirectUnitData;
                    $quizResultResponse['data']['show_course_complete_screen'] = $response['data']['show_course_complete_screen'];
                    $quizResultResponse['data']['course_complete_screen_details'] = $response['data'];
                }
                return response()->json($quizResultResponse);
            }
            DB::rollBack();
            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }
}
