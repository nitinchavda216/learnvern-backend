<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Interfaces\Api\UserRepositoryInterface;
use App\Interfaces\SiteConfigurationsRepositoryInterface;
use Illuminate\Http\Request;
use App\Interfaces\Api\UserAuthInterface;
use Illuminate\Support\Facades\DB;

class UserAuthController extends Controller
{
    protected $userAuthRepository, $userRepository, $siteConfigurationsRepository;

    public function __construct(UserAuthInterface $userAuthRepository, UserRepositoryInterface $userRepository,
    SiteConfigurationsRepositoryInterface $siteConfigurationsRepository)
    {
        $this->userRepository = $userRepository;
        $this->userAuthRepository = $userAuthRepository;
        $this->siteConfigurationsRepository = $siteConfigurationsRepository;
    }

    public function signUp(Request $request): \Illuminate\Http\JsonResponse
    {
        DB::beginTransaction();
        try {
            $is_valid = $this->userAuthRepository->validateRegister($request);
            if (!empty($is_valid) && $is_valid['status'] == false) {
                DB::rollBack();
                $errors = $is_valid['result']->errors();
                return response()->json(['status' => 100, 'message' => $errors->all()[0], 'header_message' => 'Sign Up Failed', 'data' => null]);
            }
            if (env('HARD_EMAIL_VALID') && (strpos($request->email, "+") !== false)) {
                return response()->json(['status' => 100, 'message' => 'Sorry, Only Letters (a-z), Numbers (0-9), and Periods (.) are Allowed in Email Address.', 'header_message' => 'Sign Up Failed', 'data' => null]);
            }
            $blockedUsersDomain = $this->siteConfigurationsRepository->getBlockedUsersDomain();
            $emailArray = explode('@', $request->email);
            if (isset($emailArray[1]) && in_array($emailArray[1], $blockedUsersDomain)){
                return response()->json(['status' => 100, 'message' => 'Sorry, but we are not accepting new accounts from this domain. This domain is marked as spam.', 'header_message' => 'Sign Up Failed', 'data' => null]);
            }
            $response = $this->userAuthRepository->storeUser($request);
            DB::commit();
            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'header_message' => 'Sign Up Failed', 'data' => null]);
        }
    }

    public function authenticate(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $response = $this->userAuthRepository->authenticateUser($request);
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null, 'login_type' => 3]);
        }
    }

    public function sendForgotPasswordLink(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $response = $this->userAuthRepository->sendForgotPasswordLink($request);
            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function getUserLocationDetails(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $data['city_list'] = $data['district_list'] = $data['state_list'] = [];
            $user = $this->userRepository->getUserById($request->user_id);
            if (isset($user->country_id)){
                $data['state_list'] = $this->siteConfigurationsRepository->getState($user->country_id);
            }
            if (isset($user->state_id)){
                $data['city_list'] = $this->siteConfigurationsRepository->getCity($user->state_id);
                $data['district_list'] = $this->siteConfigurationsRepository->getDistrict($user->state_id);
            }
            return response()->json(['status' => 200, 'message' => 'Data Sent Successfully', 'data' => $data]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function updateProfile(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $validateResponse = $this->userRepository->validateUpdateRequest($request);
            if (!empty($validateResponse) && $validateResponse['status'] == false) {
                $errors = $validateResponse['result']->errors();
                throw new \Exception($errors->all()[0]);
            }
            $response = $this->userRepository->updateUser($request);
            if (!$response['status']){
                return response()->json($response);
            }
            $user = $this->userRepository->getUserById($request->user_id);
            return response()->json(['status' => 200, 'message' => config('app_messages.update_profile.success.description'), 'header_message' => config('app_messages.update_profile.success.title'), 'data' => $user]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function updateVerifiedMobileNumber(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $this->userRepository->updateVerifiedMobileNumber($request);
            $user = $this->userRepository->getUserById($request->user_id);
            return response()->json(['status' => 200, 'message' => 'User Mobile Number Updated Successfully', 'data' => $user]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }

    public function socialAuth(Request $request): \Illuminate\Http\JsonResponse
    {
        DB::beginTransaction();
        try {
            if ($request->email != ""){
                $blockedUsersDomain = $this->siteConfigurationsRepository->getBlockedUsersDomain();
                $emailArray = explode('@', $request->email);
                if (isset($emailArray[1]) && in_array($emailArray[1], $blockedUsersDomain)){
                    DB::rollBack();
                    return response()->json(['status' => 100, 'message' => 'Sorry, but we are not accepting new accounts from this domain. This domain is marked as spam.',
                        "header_message" => config('app_messages.social_auth_error.title'), "data" => null, 'login_type' => $request->type]);
                }
            }
            $response = $this->userAuthRepository->socialAuth($request);
            if ($response['status']){
                DB::commit();
            }else{
                DB::rollBack();
            }
            return response()->json($response);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null, 'login_type' => $request->type]);
        }
    }


    public function myProfile(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $user = $this->userRepository->getUserById($request->user_id);
            return response()->json([
                'status' => 200,
                'message' => "Profile Details received successfully.",
                'data' => $user
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 100, 'message' => $e->getMessage(), 'data' => null]);
        }
    }
}
