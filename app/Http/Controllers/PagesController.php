<?php

namespace App\Http\Controllers;

use App\Interfaces\CacheRepositoryInterface;
use Illuminate\Http\Request;
use App\Interfaces\PagesRepositoryInterface;

class PagesController extends Controller
{
    protected $pagesRepository, $cacheRepository;

    public function __construct(PagesRepositoryInterface $pagesRepository, CacheRepositoryInterface $cacheRepository)
    {
        $this->pagesRepository = $pagesRepository;
        $this->cacheRepository = $cacheRepository;
    }
    public function index()
    {
        $pagedetails = $this->pagesRepository->getAllPages();
        return view('pages.index', compact('pagedetails'));
    }

    public function create()
    {
        return view('pages.create');
    }

    public function store(Request $request)
    {
        $this->pagesRepository->storePage($request);
        return redirect()->route('pages')->with('success', "Data Created Successfully!");
    }

    public function updateStatus(Request $request)
    {
        $this->pagesRepository->updateActiveStatus($request);
        return response()->json(['status' => true]);
    }
    public function edit($id)
    {
        $page = $this->pagesRepository->getPagesDetails($id);
        return view('pages.edit' , compact('page'));

    }
    public function update(Request $request, $id)
    {
        $this->pagesRepository->updatePage($request, $id);
        if (env('REDIS_CACHE_ENABLED')){
            $flushTags = [
                'api_page_details_'.$request->slug
            ];
            $this->cacheRepository->removeCacheByTag($flushTags);
        }
        return redirect()->route('pages')->with('success', "Data Updated Successfully!");
    }
}
