<?php

namespace App\Http\Controllers;

use App\Interfaces\AssignmentRepositoryInterface;
use App\Interfaces\CoursesRepositoryInterface;
use App\Interfaces\SectionRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class AssignmentsController extends Controller
{
    protected $coursesRepository;
    protected $assignmentRepository;
    protected $sectionRepository;


    public function __construct(CoursesRepositoryInterface $coursesRepository, AssignmentRepositoryInterface $assignmentRepository,
                                SectionRepositoryInterface $sectionRepository)
    {
        $this->coursesRepository = $coursesRepository;
        $this->assignmentRepository = $assignmentRepository;
        $this->sectionRepository = $sectionRepository;
    }

    public function index(Request $request)
    {
        $courses = $this->coursesRepository->getCourseTitles();
        $sections = [];
        if (isset($request->course_id)) {
            $sections = $this->sectionRepository->getSectionTitles($request->course_id);
        }
        return view('assignment.index', compact('courses', 'sections'));
    }

    public function getAjaxListData(Request $request)
    {
        return $this->assignmentRepository->getAjaxListData($request);
    }

    public function create()
    {
        $courses = $this->coursesRepository->getCourseTitles();
        $sections = [];
        return view('assignment.create', compact('courses', 'sections'));
    }

    public function edit($id)
    {
        $assignment = $this->assignmentRepository->getAssignmentDetails($id);
        $courses = $this->coursesRepository->getCourseTitles();
        $sections = $this->sectionRepository->getSectionTitles($assignment->course_id);
        $attachment = $this->assignmentRepository->getAssignmentAttachment($id);
        $relatedCourses = $this->coursesRepository->getCurrentCurriculumRelatedCourses($assignment->course_id, $assignment->id, 'assignment');
        return view('assignment.edit', compact('assignment', 'courses', 'sections', 'attachment', 'relatedCourses'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'slug' => "required|unique:sluglists,slug"
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error', "This slug already exists. Try with another slug.");
        }
        $response = $this->assignmentRepository->storeAssignment($request);
        if ($response['status']) {
            return redirect()->route('assignments', ['course_id' => $request->course_id, 'section_id' => $request->section_id])->with('success', "Data Stored Successfully!");
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        $this->assignmentRepository->updateAssignment($request, $id);
        return redirect()->route('assignments', ['course_id' => $request->course_id, 'section_id' => $request->section_id])->with('success', "Data Updated Successfully!");
    }

    public function updateSlug(Request $request): \Illuminate\Http\RedirectResponse
    {
        $response = $this->assignmentRepository->updateSlug($request);
        if ($response['status']){
            return redirect()->route('assignments.edit', $request->unit_id)->with(['success' => "Slug Updated Successfully!"]);
        }else{
            return redirect()->route('assignments.edit', $request->unit_id)->with(['error' => $response['message']]);
        }
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $this->assignmentRepository->updateActiveStatus($request);
        return response()->json(['status' => true]);
    }

    public function exportAssignmentData(Request $request)
    {
        return $this->assignmentRepository->getAjaxExportData($request);
    }

    public function assignmentImport(Request $request)
    {
        $response = $this->assignmentRepository->importUpdateAssignment($request);
        if ($response['status']) {
            return redirect()->route('assignments')->with('success', $response['message']);
        } else {
            return redirect()->route('assignments')->with(['error' => $response['message'], 'openModal' => true]);
        }
    }
}
