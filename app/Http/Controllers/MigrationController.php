<?php

namespace App\Http\Controllers;
use App\Models\Assignment;
use App\Models\Quiz;
use App\Models\Units;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MigrationController extends Controller
{

    public function __construct()
    {


    }

    public function checkUniqueForAssignmentQuiz($slug,$courseId)
    {


        $chk_assignment_count=Assignment::where('course_id',$courseId)->where('slug',$slug)->get();
        $chk_unit_count=Units::where('course_id',$courseId)->where('slug',$slug)->get();
        $chk_quiz_count=Quiz::where('course_id',$courseId)->where('slug',$slug)->get();


        if(count($chk_assignment_count) > 0 || count($chk_unit_count) > 0 || count($chk_quiz_count) > 0)
        {

            $slugname = $slug.'--1';
            return $this->checkUniqueForAssignmentQuiz($slugname,$courseId);
        }
        else {
            return $slug;
        }
    }


    public function slugMigrationAssignmentQuiz(){
        $allAssignment = Assignment::get();

        foreach ($allAssignment as $item){

            $slugname =  str_slug($item->name);
            $getUniqueSlug = $this->checkUniqueForAssignmentQuiz($slugname,$item->course_id);

            DB::table('assignments')->where('id',$item->id)->update([
                'slug' => $getUniqueSlug
            ]);

        }

        $allQuiz = Quiz::get();

        foreach ($allQuiz as $item){

            $slugname =  str_slug($item->name);
            $getUniqueSlug = $this->checkUniqueForAssignmentQuiz($slugname,$item->course_id);

            DB::table('quiz')->where('id',$item->id)->update([
                'slug' => $getUniqueSlug
            ]);

        }

        echo "slug migration successfully!";
        die;
    }
}
