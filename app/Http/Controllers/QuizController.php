<?php

namespace App\Http\Controllers;

use App\Interfaces\CoursesRepositoryInterface;
use App\Interfaces\QuizRepositoryInterface;
use App\Interfaces\SectionRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class QuizController extends Controller
{
    protected $coursesRepository;
    protected $quizRepository;
    protected $sectionRepository;

    public function __construct(QuizRepositoryInterface $quizRepository, CoursesRepositoryInterface $coursesRepository,
                                SectionRepositoryInterface $sectionRepository)
    {
        $this->coursesRepository = $coursesRepository;
        $this->quizRepository = $quizRepository;
        $this->sectionRepository = $sectionRepository;
    }

    public function index(Request $request)
    {
        $courses = $this->coursesRepository->getCourseTitles();
        $sections = [];
        if($request->course_id)
        {
            $sections = $this->sectionRepository->getSectionTitles($request->course_id);

        }

        return view('quiz.index', compact('courses', 'sections'));
    }

    public function getAjaxListData(Request $request)
    {
        return $this->quizRepository->getAjaxListData($request);
    }

    public function create()
    {
        $courses = $this->coursesRepository->getCourseTitles();
        $sections = [];
        return view('quiz.create', compact('courses', 'sections'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'slug' => "required|unique:sluglists,slug"
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error', "This slug already exists. Try with another slug.");
        }
        $this->quizRepository->storeQuiz($request);
        return redirect()->route('quiz',['course_id' => $request->course_id , 'section_id' => $request->section_id])->with('success', "Data Stored Successfully!");
    }

    public function edit($id)
    {
        $quiz = $this->quizRepository->getQuizDetails($id);
        $courses = $this->coursesRepository->getCourseTitles();
        $sections = $this->sectionRepository->getSectionTitles($quiz->course_id);
        $relatedCourses = $this->coursesRepository->getCurrentCurriculumRelatedCourses($quiz->course_id, $quiz->id, 'quiz');
        return view('quiz.edit', compact('quiz', 'courses', 'sections', 'relatedCourses'));
    }

    public function update(Request $request, $id)
    {
        $this->quizRepository->updateQuiz($request, $id);
        return redirect()->route('quiz' ,['course_id' => $request->course_id , 'section_id' => $request->section_id])->with('success', "Data Updated Successfully!");
    }

    public function exportQuizData(Request $request)
    {
        return $this->quizRepository->getAjaxExportData($request);
    }

    public function quizImport(Request $request): \Illuminate\Http\RedirectResponse
    {
        $response = $this->quizRepository->importQuiz($request);
        if ($response['status']){
            return redirect()->route('quiz')->with('success', $response['message']);
        }else{
            return redirect()->route('quiz')->with(['error' => $response['message'], 'openModal' => true]);
        }
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $this->quizRepository->updateActiveStatus($request);
        return response()->json(['status' => true]);
    }

    public function getQuestionListByQuiz($id)
    {
        $quiz = $this->quizRepository->getQuizDetails($id);
        $questions = $this->quizRepository->getQuestionListByQuiz($id);
        return view('questions.index', compact('quiz', 'questions'));
    }

    public function editQuizQuestion($id)
    {
        $question = $this->quizRepository->getQuizQuestionDetails($id);
        return view('questions.edit', compact('question'));
    }

    public function storeQuizQuestion(Request $request)
    {
        if (strip_tags($request->all()['content']) == ""){
            return redirect()->back()->with('error', "Please enter frontend question title.");
        }
        $this->quizRepository->storeQuizQuestion($request);
        return redirect()->route('quiz.questions', $request->quiz_id)->with('success', "Data Added Successfully!");
    }

    public function updateQuizQuestion(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        if (strip_tags($request->all()['content']) == ""){
            return redirect()->back()->with('error', "Please enter frontend question title.");
        }
        $this->quizRepository->updateQuizQuestion($request, $id);
        return redirect()->route('quiz.questions', $request->quiz_id)->with('success', "Data Updated Successfully!");
    }

    public function updateQuestionStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $this->quizRepository->updateQuestionStatus($request);
        return response()->json(['status' => true]);
    }

    public function downloadQuestionSampleFile($id)
    {
        return $this->quizRepository->downloadQuestionSampleFile($id);
    }

    public function importQuestions(Request $request): \Illuminate\Http\RedirectResponse
    {
        $response = $this->quizRepository->importQuestions($request);
        if ($response['status']){
            return redirect()->route('quiz.questions', $request->quiz_id)->with('success', $response['message']);
        }else{
            return redirect()->route('quiz.questions', $request->quiz_id)->with(['error' => $response['message'], 'openModal' => true]);
        }
    }
}
