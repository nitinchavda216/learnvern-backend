<?php

namespace App\Http\Controllers;

use App\Interfaces\CoursesRepositoryInterface;
use App\Interfaces\CourseStatusRepositoryInterface;
use App\Interfaces\PartnerApplicationRepositoryInterface;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CourseReportController extends Controller
{
    protected $courseRepository;
    protected $courseStatusRepository;
    protected $partnerApplicationRepository;

    public function __construct(CoursesRepositoryInterface $courseRepository, CourseStatusRepositoryInterface $courseStatusRepository,
            PartnerApplicationRepositoryInterface $partnerApplicationRepository)
    {
        $this->courseRepository = $courseRepository;
        $this->courseStatusRepository = $courseStatusRepository;
        $this->partnerApplicationRepository = $partnerApplicationRepository;
    }

    public function index()
    {
        $courses = $this->courseRepository->getCourseTitles();
        $partners = $this->partnerApplicationRepository->getPartnersName();
        return view('reports.course_report', compact('courses', 'partners'));
    }

    public function getAjaxListData(Request $request)
    {
        return $this->courseRepository->getCourseAjaxListData($request);
    }

    public function getAjaxExportData(Request $request)
    {
        $response = $this->courseRepository->getAjaxExportData($request);
        if (!$response['status']){
            return redirect()->back()->with('error', 'Record Not Found! Please select different date.');
        }else{
            return Excel::download($response['data'], 'Course_Report.xlsx');
        }
    }
}
