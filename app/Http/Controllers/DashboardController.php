<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 05-02-2021
 * Time: 03:09 PM
 */

namespace App\Http\Controllers;

use App\Interfaces\CacheRepositoryInterface;
use App\Interfaces\CoursesRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class DashboardController extends Controller
{
    protected $coursesRepository, $userRepository, $cacheRepository;

    public function __construct(UserRepositoryInterface $userRepository, CoursesRepositoryInterface $coursesRepository, CacheRepositoryInterface $cacheRepository)
    {
        $this->userRepository = $userRepository;
        $this->coursesRepository = $coursesRepository;
        $this->cacheRepository = $cacheRepository;
    }

    public function index()
    {
        $admin = getCurrentAdmin();
        if ($admin->relatedRole->user_type == "lms") {
            $courses = $this->coursesRepository->getLatestCourses(10);
            return view('dashboard.lms_dashboard', compact('courses'));
        } else {
            return view('dashboard.index');
        }
    }

    public function getAjaxData(Request $request)
    {
        $referralData = $this->userRepository->getTopReferralsUsers($request);
        $returnHtml = view('dashboard.referral', compact('referralData'))->render();
        return response()->json(['status' => true, 'html' => $returnHtml]);
    }

    public function flushAllCache()
    {
        if (env('REDIS_CACHE_ENABLED')){
            $this->cacheRepository->flushAllCache();
        }
        return redirect()->route('dashboard');
    }
}

