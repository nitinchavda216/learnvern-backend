<?php

namespace App\Http\Controllers;

use App\Events\PartnerApplicationApproved;
use App\Models\Partners;
use App\Models\PartnersSettings;
use Illuminate\Http\Request;
use App\Interfaces\PartnerApplicationRepositoryInterface;
use App\Interfaces\CategoryRepositoryInterface;
use Illuminate\Support\Facades\DB;

class PartnerApplicationRequestController extends Controller
{
    protected $partnerApplicationRepository;
    protected $categoryRepository;

    public function __construct(PartnerApplicationRepositoryInterface $partnerApplicationRepository, CategoryRepositoryInterface $categoryRepository)
    {
        $this->partnerApplicationRepository = $partnerApplicationRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $service_fees = PartnersSettings::select('third_party_partner_fees', 'content_partner_fees')->first();
        if (is_null($service_fees)) {
            return redirect()->route('tenancy-settings')->with('warning', 'Please insert your tenancy setting details.');
        }
        return view('partner_app_request.index', compact('service_fees'));
    }

    public function getAjaxData(Request $request)
    {
        return $this->partnerApplicationRepository->getApplicationRequestList($request);
    }

    public function updateApplicationStatus(Request $request, $id, $status)
    {
        $res = $this->partnerApplicationRepository->updateApplicationStatus($request, $id, $status);
        if ($res) {
            $status = ($status == 1) ? 'Approved' : (($status == 2) ? "Rejected" : "Pending");
            return redirect()->route('partner-application-request')->with('success', "Application '" . $status . "' Successfully!");
        }
    }

    public function showApplicationStatus($id)
    {
        $data = $this->partnerApplicationRepository->showApplicationStatus($id);
        $courses = $this->categoryRepository->getCoursesByCategory();
        $partner_revenue_fees = $this->partnerApplicationRepository->getPartnerRevenueFees($id);
        $partner_courses = $this->partnerApplicationRepository->getPartnerCoursesId($id);
        return view('partner_app_request.show', compact('data', 'courses', 'partner_courses', 'partner_revenue_fees'));
    }

    public function approve(Request $request)
    {
        DB::beginTransaction();
        try{
            $this->partnerApplicationRepository->storePartnerType($request);
            $partner = Partners::where('id', $request->all()['partner_user_id'])->first();
            event(new PartnerApplicationApproved($partner));
            DB::commit();
            return redirect()->back()->with(['success' => "Updated Successfully!"]);
        }catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }

    }

    public function update($partnerId,Request $request)
    {
        $response = $this->partnerApplicationRepository->storePartnerRevenueInfoAndAccessibility($partnerId,$request);
        if ($response['status']){
            return redirect()->route('partner-application-request')->with('success', "Partner Application Successfully Updated!");
        }else{
            return redirect()->back()->with('error', $response['message']);
        }
    }

}
