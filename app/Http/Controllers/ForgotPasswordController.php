<?php

namespace App\Http\Controllers;
use App\Mail\SendMailable;
use App\Models\Admins;
use Illuminate\Http\Request;
use App\Models\PasswordResets;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends Controller
{
   public function forgotPassword()
   {
       return view('forgot_password');
   }

    public function sendPasswordResetInstructions(Request $request)
    {
        $this->validate($request, [
            'email' => 'required'
        ]);
        $requestData = $request->all();
        $email = $requestData['email'];
        $token = $random = str_random(15);
        $admin = Admins::where('email', $email)->first();
        if (isset($admin)) {
            $reset_data = array('email' => $email, 'token' => $token, 'created_at' => date('Y-m-d'));
            $isEmailExist = PasswordResets::where('email', $email)->first();
            if (isset($isEmailExist)) {
                PasswordResets::where('email', '=', $email)->update($reset_data);
            } else {
                PasswordResets::create($reset_data);
            }
            $reset_data['name'] = $admin['name'];
            $reset_data['reset_link'] = route('reset_password', $token);
            Mail::to($reset_data['email'])->send(new SendMailable($reset_data['reset_link']));
            return redirect()->route('login')->with('success', "We have sent forgot password link to your email address. Please follow mail instructions.");
        } else {
            return redirect()->back()->with('error', "Sorry! This user does not exist!");
        }
    }

    public function resetPasswordLink($token = '')
    {
        $check_valid_token = PasswordResets::where('token', $token)->first();
        $result = array("success" => true);
        if (!empty($check_valid_token)) {
            $result['email'] = $check_valid_token->email;
            return view('reset_password', compact('result'));
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'confirmed|min:6'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $requestData = $request->all();
        $email = $requestData['email'];
        $admin = Admins::where('email', $email)->first();
        $exist_password_record = PasswordResets::where('email', $email)->first();
        if (isset($exist_password_record) && isset($admin)) {
            $admin->update($requestData);
            PasswordResets::where('email', $email)->delete();
            return redirect()->route("login")->with('success', "Your reset password process successfully completed. Please login with your new password!");
        }else{
            abort(403, 'Unauthorized action.');
        }
    }



}
