<?php

namespace App\Http\Controllers;

use App\Interfaces\CategoryRepositoryInterface;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $categories = $this->categoryRepository->getallCategory();
        $data =  $this->categoryRepository->getSortOrderData();
        $add_category = view('category.create');
        return view('category.index', compact('categories', 'add_category','data'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => "required"
        ]);
        $this->categoryRepository->storeCategoryData($request);
        return redirect()->route('categories')->with('success', "Data Stored Successfully!");
    }

    public function edit($id)
    {
        $category = $this->categoryRepository->getCategoryDetails($id);
        $returnHtml = view('category.edit', compact('category'))->render();
        return response()->json(['status' => true, 'html' => $returnHtml]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => "required",
        ]);
        $this->categoryRepository->updateCategoryData($request, $id);
        return redirect()->route('categories')->with('success', "Data Updated Successfully!");
    }

    public function updateStatus(Request $request)
    {
        $this->categoryRepository->updateActiveStatus($request);
        return response()->json(['status' => true]);
    }

    public function updateSortOrder(Request $request)
    {
        $response = $this->categoryRepository->updateSortOrder($request);
        return response()->json($response);
    }

    public function getSortOrder()
    {
        $category = $this->categoryRepository->getSortOrderData();
        return view('category.sort_order', compact( 'category'));
    }

}
