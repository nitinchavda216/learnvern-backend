<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 05-02-2021
 * Time: 03:09 PM
 */

namespace App\Http\Controllers;


use App\Interfaces\AdminUserRepositoryInterface;
use App\Interfaces\CampaignRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;

use App\Models\Admins;
use App\Models\Campaign;
use App\Models\CourseUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CrmController extends Controller
{
    protected $campaignRepository;
    protected $adminUserRepository;

    public function __construct(CampaignRepositoryInterface $campaignRepository,AdminUserRepositoryInterface $adminUserRepository)
    {
        $this->campaignRepository = $campaignRepository;
        $this->adminUserRepository = $adminUserRepository;
    }

    public function index(Request $request)
    {
        $statistics = $this->campaignRepository->getCrmDashboardData($request);
        return view('crm.index', compact('statistics'));
    }

    public function getCampaignUserDetail(Request $request,$userId)
    {
        $requestData = $request->all();
        if(isset($requestData['campaign_id']) && $requestData['campaign_id']){

           $result = $this->campaignRepository->assignCampaignToUser($requestData,false);
           if($result['status']){
               return redirect()->back()->with('success', "Campaign assign successfully.");
           }
           else
           {
             return redirect()->back()->with('error', $result['message']);
           }
        }

        $userDetail = User::where('id',$userId)->first();
        $buyCertificate = CourseUser::where('paid_status',1)->where('user_id',$userId)->count();
        $courseList = CourseUser::with(['courseDetail'])->whereHas('courseDetail')->where('user_id',$userId)->get();
        $adminNames = $this->adminUserRepository->getAllCRMUser();
        $campaignList = Campaign::where('is_fixed_campaign',1)->select('name','id','marketing_goal')->get();

        return view('crm.user_details',compact('userDetail','buyCertificate','courseList','adminNames','campaignList'));
    }

    public function getAjaxCampaignUserDetail(Request $request)
    {
        return $this->campaignRepository->getUserAssociateWithCampaign($request);
    }



    public function campaignReport(Request $request)
    {
        return view('crm.campaign');
    }


    public function getCampaignReportForCrm(Request $request)
    {
        return $this->campaignRepository->getCampaignReportForCrm($request);
    }


    public function getAjaxListCampaignData(Request $request)
    {
        return $this->campaignRepository->getAllCampaignForCrm($request);
    }

    public function calling($id, $assignTo = '')
    {
        $campaignDetail = $this->campaignRepository->getReportData($id);
        $statistics = $this->campaignRepository->getCallingStatisticsData($id);
        return view('crm.calling', compact('campaignDetail', 'statistics', 'assignTo'));
    }


    public function getAjaxCallingData(Request $request)
    {
        return $this->campaignRepository->viewCallingUserData($request);
    }

    public function saveCallingDetail(Request $request)
    {
        return $this->campaignRepository->saveCallingDetail($request);
    }

    public function pending()
    {
        return view('crm.pending');
    }

    public function getPendingAjaxCallingData(Request $request)
    {
        return $this->campaignRepository->pendingCallingUserData($request);
    }

    public function followUpStatisticsReferral(Request $request)
    {
        $requestData = $request->all();
        $type = $requestData['type'] ?? '';
        $campaignId = $requestData['campaign'] ?? '';
        $userId = $requestData['user'] ?? '';
        return view('crm.statistics-referral', compact('type', 'campaignId', 'userId'));
    }

    public function getFollowUpStatisticsReferral(Request $request)
    {
        return $this->campaignRepository->getFollowUpStatisticsReferral($request);
    }


    public function followUpStatisticsRevenue(Request $request, $type = null)
    {
        $requestData = $request->all();
        $type = $requestData['type'] ?? '';
        $campaignId = $requestData['campaign'] ?? '';
        $userId = $requestData['user'] ?? '';
        return view('crm.statistics-revenue', compact('type', 'campaignId', 'userId'));
    }

    public function getFollowUpStatisticsRevenue(Request $request)
    {
        return $this->campaignRepository->getFollowUpStatisticsRevenue($request);
    }

    public function crmUserList()
    {
        if (getCurrentAdmin()->relatedRole->is_super_admin == 0 && getCurrentAdmin()->relatedRole->is_crm_manager == 0) {
            $url = route('crm.userPerformance', getCurrentAdmin()->id) . '?date_range=' . date('d-m-Y').' - '.date('d-m-Y');
            return redirect($url);
        }
        return view('crm.user_list');
    }

    public function getCrmUserList(Request $request)
    {
        return $this->campaignRepository->getCrmUserList($request);
    }

    public function userPerformance($id, Request $request)
    {

        $userDetail = Admins::where('id', $id)->first();
        $userStatisticsResult = $this->campaignRepository->userPerformance($id, $request);
        return view('crm.user_performance', compact('userDetail', 'userStatisticsResult'));
    }

}
