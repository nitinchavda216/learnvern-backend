<?php

namespace App\Http\Controllers;

use App\Models\CourseKeywords;
use Illuminate\Http\Request;
use App\Models\MissingKeywordHistory;
use App\Models\User;
use Maatwebsite\Excel\Facades\Excel;
use App\Interfaces\CoursesRepositoryInterface;
use App\Interfaces\CourseKeywordRepositoryInterface;

class CourseKeywordsController extends Controller {

    protected $courseKeywordsRepository;
    protected $coursesRepository;

    public function __construct(CourseKeywordRepositoryInterface $courseKeywordsRepository, CoursesRepositoryInterface $coursesRepository) {
        $this->courseKeywordsRepository = $courseKeywordsRepository;
        $this->coursesRepository = $coursesRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = $this->courseKeywordsRepository->getListData();
        return view('course-keywords.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CourseKeywords  $courseKeywords
     * @return \Illuminate\Http\Response
     */
    public function show(CourseKeywords $courseKeywords) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CourseKeywords  $courseKeywords
     * @return \Illuminate\Http\Response
     */
    public function edit(CourseKeywords $courseKeywords, $id) {
        $courseKeywordsData = $this->courseKeywordsRepository->getCourseKeywordDetails($id);
        return view('course-keywords.edit', compact('courseKeywordsData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CourseKeywords  $courseKeywords
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->courseKeywordsRepository->upsertcourseKeywordsData($request, $id);
        return redirect()->route('search-keywords')->with('success', "Data Updated Successfully!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CourseKeywords  $courseKeywords
     * @return \Illuminate\Http\Response
     */
    public function destroy(CourseKeywords $courseKeywords) {
        //
    }

    public function getAjaxListData(Request $request) {
        return $this->courseKeywordsRepository->getAjaxListData($request);
    }

    public function courseKeywordsImport(Request $request) {
        if (!isset($request->all()['file'])) {
            return redirect()->route('search-keywords')->with(['error' => 'Please upload an excel file.', 'openModal' => true]);
        }
        $this->courseKeywordsRepository->courseKeywordsImport($request);
        return redirect()->route('search-keywords')->with('success', "Data Updated Successfully!");
    }

    public function missingCourseKeywordsList(Request $request) {
        $courses = $this->coursesRepository->getCourseTitles();
        return view('missing-course-keywords.index', compact('courses'));
    }

    public function getMissingCourseKeywordsListData(Request $request) {
        return $this->courseKeywordsRepository->getMissingCourseKeywordsListData($request);
    }

    public function missingCourseKeywordsupdate(Request $request) {
        $data = $this->courseKeywordsRepository->missingCourseKeywordsupdate($request);
        return redirect()->route('missing-search-keywords')->with('success', "Keyword assigned Successfully!");
    }

    public function missingCourseKeywordsdestroy(Request $request, $id) {
        $data = $this->courseKeywordsRepository->missingCourseKeywordsdestroy($request, $id);
        return redirect()->route('missing-search-keywords')->with('success', "Keyword deleted Successfully!");
    }

    public function keywordSearchAnalyticsIndex(Request $request) {
        $courses = $this->coursesRepository->getCourseTitles();
        return view('keyword-search-analytics.index', compact('courses'));
    }

    public function keywordSearchAnalyticsList(Request $request) {
        return $this->courseKeywordsRepository->keywordSearchAnalyticsList($request);
    }

    public function getAjaxExportData(Request $request) {
        $response = $this->courseKeywordsRepository->getAjaxExportData($request);
        if (!$response['status']) {
            return redirect()->back()->with('error', 'Record Not Found! Please select different date.');
        } else {
            return Excel::download($response['data'], 'keyword_search_history.xlsx');
        }
    }
    
    public function getKeywordExportData(Request $request) {
        $response = $this->courseKeywordsRepository->getKeywordExportData($request);
        if (!$response['status']) {
            return redirect()->back()->with('error', 'Record Not Found! Please select another course.');
        } else {
            return Excel::download($response['data'], 'keyword_search_history.xlsx');
        }
    }

    public function missingKeywordHistoryList(Request $request, $id) {
        $response = $this->courseKeywordsRepository->missingKeywordHistoryList($id);
        return view('missing-keyword-history.index', compact('response'));
    }

    public function missingKeywordHistoryListData(Request $request) {
        $response = $this->courseKeywordsRepository->missingKeywordHistoryListData($request);
        return $response;
    }

}
