<?php

namespace App\Http\Controllers;

use App\Interfaces\CoursesRepositoryInterface;
use App\Interfaces\CourseStatusRepositoryInterface;
use App\Interfaces\PartnerApplicationRepositoryInterface;
use App\Interfaces\PaymentRepositoryInterface;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CourseStatusController extends Controller
{
    protected $courseStatusRepository;
    protected $partnerApplicationRepository;
    protected $coursesRepository;
    protected $paymentRepository;

    public function __construct(CourseStatusRepositoryInterface $courseStatusRepository, PartnerApplicationRepositoryInterface $partnerApplicationRepository,
                                CoursesRepositoryInterface $coursesRepository, PaymentRepositoryInterface $paymentRepository)
    {
        $this->courseStatusRepository = $courseStatusRepository;
        $this->partnerApplicationRepository = $partnerApplicationRepository;
        $this->coursesRepository = $coursesRepository;
        $this->paymentRepository = $paymentRepository;
    }

    public function index()
    {
        $partners = $this->partnerApplicationRepository->getPartnersName();
        $courses = $this->coursesRepository->getCourseTitles();
        return view('reports.course_status_report', compact('partners', 'courses'));
    }

    public function getAjaxListData(Request $request)
    {
        return $this->courseStatusRepository->getAjaxListData($request);
    }

    public function exportList(Request $request)
    {
        $response = $this->courseStatusRepository->exportListData($request);
        if (!$response['status']){
            return redirect()->back()->with('error', 'Record Not Found!');
        }else{
            return Excel::download($response['data'], 'Course_Status_Report.xlsx');
        }
    }

    public function getCertificatePurchaseUsers()
    {
        $partners = $this->partnerApplicationRepository->getPartnersName();
        $courses = $this->coursesRepository->getCourseTitles();
        return view('reports.course_certificate_purchase_users', compact('partners', 'courses'));
    }

    public function getCertificatePurchaseUsersAjaxData(Request $request)
    {
        return $this->paymentRepository->getCertificatePurchaseUsersAjaxData($request);
    }
}
