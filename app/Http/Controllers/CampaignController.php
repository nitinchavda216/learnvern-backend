<?php

namespace App\Http\Controllers;

use App\Interfaces\AdminUserRepositoryInterface;
use App\Interfaces\CampaignRepositoryInterface;
use App\Interfaces\ReportBuilderRepositoryInterface;
use App\Models\Campaign;
use App\Models\CampaignAssign;
use App\Models\CampaignUserCallings;
use App\Models\CampaignUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CampaignController extends Controller
{
    protected $campaignRepository;
    protected $adminUserRepository;
    protected $reportBuilderRepository;

    public function __construct(CampaignRepositoryInterface $campaignRepository, AdminUserRepositoryInterface $adminUserRepository, ReportBuilderRepositoryInterface $reportBuilderRepository)
    {
        $this->campaignRepository = $campaignRepository;
        $this->adminUserRepository = $adminUserRepository;
        $this->reportBuilderRepository = $reportBuilderRepository;
    }

    public function index()
    {
        $adminNames = $this->adminUserRepository->getAllAdminNames();
        return view('campaign.index', compact('adminNames'));
    }

    public function edit($id)
    {
        $campaignDetail = $this->campaignRepository->getCampaignDetails($id);
        $campaignAssignDetail = CampaignAssign::where('campaign_id', $id)->get();
        $adminNames = $this->adminUserRepository->getAllAdminNames();
        if ($campaignDetail->action == "calling-user") {
            $numberOfUserCampaign = CampaignUserCallings::where('campaign_id', $campaignDetail->id)->whereNULL('parent_id')->count();
        } else {
            $numberOfUserCampaign = CampaignUsers::where('campaign_id', $campaignDetail->id)->count();
        }
        return view('campaign.edit', compact('campaignDetail', 'adminNames', 'campaignAssignDetail', 'numberOfUserCampaign'));
    }

    public function update(Request $request)
    {
        try {

            $requestData = $request->all();
            DB::beginTransaction();
            $campaignArray = [];
            $campaignArray['name'] = $requestData['name'];
            $campaignArray['marketing_goal'] = $requestData['marketing_goal'];
            $campaignArray['action'] = $requestData['action'];
            $campaignArray['is_allow_duplicate'] = $requestData['is_allow_duplicate'] ?? 0;
            $campaignDetail = $this->campaignRepository->getCampaignDetails($requestData['campaign_id']);
            Campaign::where('id', $requestData['campaign_id'])->update($campaignArray);
            DB::table('campaign_assign')->where('campaign_id', $requestData['campaign_id'])->delete();
            if ($requestData['action'] != 'calling-user') {
                DB::table('campaign_users_calling')->where('campaign_id', $requestData['campaign_id'])->delete();
                $campaignAssignData = [];
                $campaignAssignData['campaign_id'] = $requestData['campaign_id'];
                $campaignAssignData['assign_to'] = $requestData['assign_to'];
                $campaignAssignData['number_of_user'] = CampaignUsers::where('campaign_id', $requestData['campaign_id'])->count();
                CampaignAssign::create($campaignAssignData);
            } else {
                if ($campaignDetail->action == $requestData['action']) {
                    if ($requestData['chunk_type'] == 0) {

                        $campaignAssignData = [];
                        $campaignAssignData['campaign_id'] = $requestData['campaign_id'];
                        $campaignAssignData['assign_to'] = $requestData['assign_to'];
                        $campaignAssignData['number_of_user'] = CampaignUserCallings::where('campaign_id', $requestData['campaign_id'])->whereNULL('parent_id')->count();
                        CampaignAssign::create($campaignAssignData);
                        DB::table('campaign_users_calling')->where('campaign_id', $requestData['campaign_id'])->update(['assign_to' => $requestData['assign_to']]);
                    } else {
                        $start = 0;
                        foreach ($requestData['chunk_distribute']['name'] as $key => $value) {
                            if ($value && $requestData['chunk_distribute']['qty'][$key]) {
                                $offset = $requestData['chunk_distribute']['qty'][$key];
                                DB::update("UPDATE campaign_users_calling SET assign_to = '$value' WHERE id IN(SELECT id FROM (SELECT id FROM campaign_users_calling WHERE campaign_id = " . $requestData['campaign_id'] . " AND parent_id IS NULL LIMIT $start,$offset) as u)");
                                $start = $start + $offset;

                                $existing_campaign_assignee = CampaignAssign::where(['campaign_id'=>$requestData['campaign_id'],'assign_to'=>$value])->first();
                                if(!empty($existing_campaign_assignee))
                                {
                                    $assigned_users_count = $existing_campaign_assignee['number_of_user'] + $requestData['chunk_distribute']['qty'][$key];
                                    CampaignAssign::where(['campaign_id'=>$requestData['campaign_id'],'assign_to'=>$value])->update(['number_of_user'=>$assigned_users_count]);
                                }
                                else{
                                    $campaignAssignData = [];
                                    $campaignAssignData['campaign_id'] = $requestData['campaign_id'];
                                    $campaignAssignData['assign_to'] = $value;
                                    $campaignAssignData['number_of_user'] = $requestData['chunk_distribute']['qty'][$key];
                                    CampaignAssign::create($campaignAssignData);
                                }
                            }
                        }

                        $moveToFollowToNewAssigner = DB::select("select id,parent_id from campaign_users_calling WHERE campaign_id = " . $requestData['campaign_id'] . " AND parent_id IS NOT NULL");
                        if ($moveToFollowToNewAssigner) {
                            foreach ($moveToFollowToNewAssigner as $item) {
                                $assignTo = (array)DB::selectOne("select assign_to FROM campaign_users_calling WHERE id= $item->parent_id");
                                if ($assignTo) {
                                    DB::update("UPDATE campaign_users_calling SET assign_to = '" . $assignTo['assign_to'] . "' WHERE id =" . $item->id);
                                }
                            }
                        }
                    }
                } else {
                    $day_start = date('Y-m-d', strtotime("-6 months"));
                    $marketing_goal = $requestData['marketing_goal'];
                    $where = "WHERE campaign_users.campaign_id=" . $requestData['campaign_id'] . " AND blocktable.block_date IS NULL";
                    $leftJoin = '';
                    if ($campaignArray['is_allow_duplicate'] == 0) {
                        if ($marketing_goal == "referral") {
                            $leftJoin .= "LEFT JOIN (select campaign_users_calling.user_id,count(campaign_users_calling.id) as alrady_assign_calling from campaign_users_calling LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id WHERE calling_action IS NULL GROUP BY campaign_users_calling.user_id) callingtable ON campaign_users.user_id = callingtable.user_id ";
                        } else {
                            $leftJoin .= "LEFT JOIN (select campaign_users_calling.user_id,count(campaign_users_calling.id) as alrady_assign_calling from campaign_users_calling LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id WHERE calling_action IS NULL AND campaign.marketing_goal ='$marketing_goal' GROUP BY campaign_users_calling.user_id) callingtable ON campaign_users.user_id = callingtable.user_id ";
                        }
                        $where .= " AND alrady_assign_calling IS NULL";
                    }
                    if ($requestData['chunk_type'] == 1) {
                        $start = 0;
                        foreach ($requestData['chunk_distribute']['name'] as $key => $value) {
                            if ($value && $requestData['chunk_distribute']['qty'][$key]) {
                                $offset = $requestData['chunk_distribute']['qty'][$key];
                                $query = "SELECT campaign_users.campaign_id,campaign_users.id,campaign_users.user_id,campaign_users.course_id," . $value . " as assign_to,created_at FROM campaign_users
                                          LEFT JOIN (select user_id,created_at as block_date from campaign_block_user WHERE DATE(created_at) >='$day_start' GROUP BY user_id) blocktable ON campaign_users.user_id = blocktable.user_id
                                          $leftJoin
                                          $where";
                                DB::insert("INSERT INTO campaign_users_calling (campaign_id ,campaign_user_id,user_id,course_id,assign_to,created_at) select * from ($query) temp ORDER BY id LIMIT 0,$offset");
                                $campaignAssignData = [];
                                $campaignAssignData['campaign_id'] = $requestData['campaign_id'];
                                $campaignAssignData['assign_to'] = $value;
                                $campaignAssignData['number_of_user'] = $requestData['chunk_distribute']['qty'][$key];
                                CampaignAssign::create($campaignAssignData);
                                $start = $start + $offset;
                            }
                        }
                    } else {
                        $query = "SELECT campaign_users.campaign_id,campaign_users.id,campaign_users.user_id,campaign_users.course_id," . $requestData['assign_to'] . " as assign_to,created_at FROM campaign_users
                                  LEFT JOIN (select user_id,created_at as block_date from campaign_block_user WHERE DATE(created_at) >='$day_start' GROUP BY user_id) blocktable ON campaign_users.user_id = blocktable.user_id
                                  $leftJoin
                                  $where";
                        DB::insert("INSERT INTO campaign_users_calling (campaign_id ,campaign_user_id,user_id,course_id,assign_to,created_at) $query");

                        $query = "SELECT count(id) as totalRows FROM campaign_users_calling WHERE campaign_id=" . $requestData['campaign_id'];
                        $countRows = (array)DB::selectOne($query);
                        $campaignAssignData = [];
                        $campaignAssignData['campaign_id'] = $requestData['campaign_id'];
                        $campaignAssignData['assign_to'] = $requestData['assign_to'];
                        $campaignAssignData['number_of_user'] = $countRows['totalRows'];
                        CampaignAssign::create($campaignAssignData);
                    }
                }
            }
            DB::commit();
            return redirect()->route('campaign')->with('success', 'Campaign Updated Successfully');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('campaign')->with('error', $e->getMessage());
        }
    }

    public function getAjaxListData(Request $request)
    {
        return $this->campaignRepository->getAllCampaign($request);
    }

    public function store(Request $request)
    {
        $response = $this->campaignRepository->storeCampaign($request);
        if ($response['status']) {
            return redirect()->back()->with('success', 'Campaign Stored Successfully');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function verifyUser(Request $request)
    {

        $requestData = $request->all();

        $selectedData['report_id'] = $requestData['report_id'];
        $selectedData['fields'] = json_decode($requestData['fields']);
        $selectedData['conditions'] = json_decode($requestData['conditions'], true);
        $selectedData['action'] = $requestData['action'];
        $selectedData['marketing_goal'] = $requestData['marketing_goal'];
        $selectedData['is_allow_duplicate'] = $requestData['is_allow_duplicate'] ?? 0;

        if ($requestData['checkAllUsers'] == "true") {
            $extractID = isset($requestData['unselect_course_user_ids']) ? json_decode($requestData['unselect_course_user_ids'], true) : [];
            $conditionApply = '';
            if ($extractID) {
                $conditionApply = " AND course_user.id NOT IN (" . implode(',', $extractID) . ")";
            }
        } else {
            $importIDS = json_decode($requestData['course_user_ids'], true);
            $conditionApply = '';
            if ($importIDS) {
                $conditionApply = " AND course_user.id IN (" . implode(',', $importIDS) . ")";
            }

        }

        $totalResultCount = $this->reportBuilderRepository->campaignBuildQuery($selectedData, '', $conditionApply);
        return response()->json(['total_result_count' => $totalResultCount]);

    }

    public function verifyUserForUpdate(Request $request)
    {

        $requestData = $request->all();
        $requestData['is_allow_duplicate'] = $requestData['is_allow_duplicate'] ?? 0;
        $campaignDetail = $this->campaignRepository->getCampaignDetails($requestData['campaign_id']);
        if ($campaignDetail->action != $requestData['action']) {

            $day_start = date('Y-m-d', strtotime("-6 months"));
            $marketing_goal = $requestData['marketing_goal'];

            $where = "WHERE campaign_users.campaign_id=" . $requestData['campaign_id'] . " AND blocktable.block_date IS NULL";

            $leftJoin = '';
            if ($requestData['is_allow_duplicate'] == 0) {

                if ($marketing_goal == "referral") {
                    $leftJoin .= " LEFT JOIN (select campaign_users_calling.user_id,count(campaign_users_calling.id) as alrady_assign_calling from campaign_users_calling LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id WHERE calling_action IS NULL GROUP BY campaign_users_calling.user_id) callingtable ON campaign_users.user_id = callingtable.user_id";
                } else {
                    $leftJoin .= " LEFT JOIN (select campaign_users_calling.user_id,count(campaign_users_calling.id) as alrady_assign_calling from campaign_users_calling LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id WHERE calling_action IS NULL AND campaign.marketing_goal ='$marketing_goal' GROUP BY campaign_users_calling.user_id) callingtable ON campaign_users.user_id = callingtable.user_id";
                }
                $where .= " AND alrady_assign_calling IS NULL";
            }

            $query = "SELECT count(campaign_users.id) as totalRecordCount FROM campaign_users
                      LEFT JOIN (select user_id,created_at as block_date from campaign_block_user WHERE DATE(created_at) >='$day_start' GROUP BY user_id) blocktable ON campaign_users.user_id = blocktable.user_id
                      $leftJoin
                      $where";

            $result = (array)DB::selectOne($query);

            $numberOfUserCampaign = $result['totalRecordCount'];
        } else {
            $numberOfUserCampaign = CampaignUserCallings::where('campaign_id', $campaignDetail->id)->whereNULL('parent_id')->count();
        }
        return response()->json(['total_result_count' => $numberOfUserCampaign]);
    }


    public function calling($id)
    {

        $campaignDetail = $this->campaignRepository->getReportData($id);
        $statistics = $this->campaignRepository->getCallingStatisticsData($id);
        return view('campaign.calling', compact('campaignDetail', 'statistics'));
    }


    public function getAjaxCallingData(Request $request)
    {
        return $this->campaignRepository->viewCallingUserData($request);
    }

    public function storesubcampaign(Request $request)
    {
        $response = $this->campaignRepository->storeCampaignInCampaign($request);
        if ($response['status']) {
            return redirect()->back()->with('success', 'Campaign Stored Successfully');
        } else {
            return redirect()->back()->with('error', $response['message']);
        }
    }

    public function markascomplete($id)
    {

        $this->campaignRepository->markAsCompleteCampaign($id);
        return redirect()->route('campaign')->with('success', 'Campaign Completed Successfully');
    }


    public function saveCallingDetail(Request $request)
    {
        return $this->campaignRepository->saveCallingDetail($request);
    }

    public function destroy($id)
    {
        $this->campaignRepository->deleteCampaign($id);
        return redirect()->route('campaign')->with('success', 'Campaign Deleted Successfully');
    }

    public function viewReport($id)
    {
        $report = $this->campaignRepository->getReportData($id);
        $campaign_date = date('Y-m-d', strtotime($report->created_at));
        $adminNames = $this->adminUserRepository->getAllAdminNames();
        $summaryData = [];
        if ($report->marketing_goal == "referral") {
            $totalUserCountQuery = (array)DB::selectOne("select count(DISTINCT(user_id)) as totalNumberOfUser from campaign_users where campaign_id=" . $id);
            $count = $totalUserCountQuery['totalNumberOfUser'];


            $query = "SELECT SUM(t1.totalReferalCountBeforeCampaign) AS totalReferalCountBeforeCampaign ,SUM(t1.totalReferalCountAfterCampaign)  AS totalReferalCountAfterCampaign FROM (
                                    SELECT temp_ref.total_referral_before_campaign AS totalReferalCountBeforeCampaign,	temp_ref_after.total_referral_after_campaign AS totalReferalCountAfterCampaign
                                    FROM `campaign_users`
                                    LEFT JOIN (
		                                        SELECT COUNT(*) AS total_referral_before_campaign,referral,temp_user.user_id FROM `users`
				                                LEFT JOIN (SELECT id AS user_id,own_referral_code FROM users) temp_user ON temp_user.own_referral_code=users.referral
						                        WHERE referral !='' AND DATE(created_at) < '$campaign_date' GROUP BY referral) temp_ref ON temp_ref.user_id = campaign_users.user_id

			                    	LEFT JOIN (SELECT COUNT(*) AS total_referral_after_campaign,referral,temp_after_user.user_id FROM `users`
						                        LEFT JOIN (SELECT id AS user_id,own_referral_code FROM users) temp_after_user ON temp_after_user.own_referral_code=users.referral
						                        WHERE referral !='' AND DATE(created_at) >= '$campaign_date' GROUP BY referral)	temp_ref_after ON temp_ref_after.user_id = campaign_users.user_id
                                    WHERE campaign_id = $id GROUP BY campaign_users.user_id
                        ) t1";
            $summaryData = (array)DB::selectOne($query);

        } else if ($report->marketing_goal == "revenue") {
            $totalUserCountQuery = (array)DB::selectOne("select count(user_id) as totalNumberOfUser from campaign_users where campaign_id=" . $id);
            $count = $totalUserCountQuery['totalNumberOfUser'];


            $query = "SELECT count(campaign_users.course_user_id) as totalNumberOfPaymentCompleted FROM `campaign_users`
                      LEFT JOIN course_user ON campaign_users.course_user_id = course_user.id
                      WHERE campaign_id =$id AND course_user.paid_status=1";

            $summaryData = (array)DB::selectOne($query);
        } else {
            $totalUserCountQuery = (array)DB::selectOne("select count(user_id) as totalNumberOfUser from campaign_users where campaign_id=" . $id);
            $count = $totalUserCountQuery['totalNumberOfUser'];


//            $query = "SELECT count(campaign_users.course_user_id) as totalNumberOfPaymentCompleted FROM `campaign_users`
//                      LEFT JOIN course_user ON campaign_users.course_user_id = course_user.id
//                      WHERE campaign_id =$id AND course_user.paid_status=1";
//
//            $summaryData = (array)DB::selectOne($query);
        }

        return view('campaign.viewreport', compact('report', 'count', 'summaryData', 'adminNames'));
    }

    public function exportReport($id)
    {
        return $this->campaignRepository->exportData($id);
    }

    public function viewReportAjaxData(Request $request)
    {
        $requestData = $request->all();
        $campaignDetail = Campaign::where('id', $requestData['report_id'])->first();

        if ($campaignDetail->marketing_goal == "referral") {
            return $this->campaignRepository->viewReportAjaxDataReferral($request, $campaignDetail);
        } elseif ($campaignDetail->marketing_goal == "revenue") {
            return $this->campaignRepository->viewReportAjaxDataRevenue($request, $campaignDetail);
        } else {
            return $this->campaignRepository->viewReportAjaxDataGrowth($request, $campaignDetail);
        }

    }
}
