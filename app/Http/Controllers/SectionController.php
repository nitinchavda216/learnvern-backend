<?php

namespace App\Http\Controllers;

use App\Interfaces\CoursesRepositoryInterface;
use App\Interfaces\SectionRepositoryInterface;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    protected $coursesRepository;
    protected $sectionRepository;

    public function __construct(CoursesRepositoryInterface $coursesRepository, SectionRepositoryInterface $sectionRepository)
    {
        $this->coursesRepository = $coursesRepository;
        $this->sectionRepository = $sectionRepository;
    }

    public function index(Request $request)
    {
        $courses = $this->coursesRepository->getCourseTitles();
        return view('section.index', compact('courses'));
    }

    public function getAjaxListData(Request $request)
    {
        return $this->sectionRepository->getAjaxListData($request);
    }

    public function create()
    {
        $courses = $this->coursesRepository->getCourseTitles();
        return view('section.create', compact('courses'));
    }

    public function getParentSections(Request $request)
    {
        $sections = $this->sectionRepository->getParentSections($request);
        $returnHtml = "";
        if (!empty($sections)){
            $returnHtml = view('section.ajax.parent_sections', compact('sections'))->render();
        }
        return response()->json(['status' => (count($sections) > 0), 'html' => $returnHtml]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'course_id' => "required",
        ], [
            'course_id.required' => "Please select Course!"
        ]);
        $this->sectionRepository->storeSectionData($request);
        return redirect()->route('sections', ['course_id' => $request->course_id])->with('success', "Data Stored Successfully!");
    }

    public function edit($id)
    {
        $curriculum = $this->coursesRepository->getCurriculumDetail($id);
        $relatedCourses = $this->coursesRepository->getCurrentCurriculumRelatedCourses($curriculum->course_id, $curriculum->curriculum_list_id, 'section');
        $section = $this->sectionRepository->getSectionDetail($curriculum->curriculum_list_id);
        $courses = [$curriculum->course_id => $curriculum->courseDetail->name];
        return view('section.edit', compact('section', 'courses', 'curriculum', 'relatedCourses'));
    }

    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        $this->validate($request, [
            'name' => "required",
        ]);
        $this->sectionRepository->updateSectionData($request, $id);
        return redirect()->route('sections', ['course_id' => $request->course_id])->with('success', "Data Updated Successfully!");
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $this->sectionRepository->updateActiveStatus($request);
        return response()->json(['status' => true]);
    }

    public function getSectionsByCourse(Request $request)
    {
        return $this->sectionRepository->getSectionsByCourse($request);
    }

    public function exportSectionData(Request $request)
    {
        return $this->sectionRepository->getAjaxExportData($request);
    }

    public function sectionImport(Request $request): \Illuminate\Http\RedirectResponse
    {
        $response = $this->sectionRepository->importUpdateUnit($request);
        if ($response['status']) {
            return redirect()->route('sections')->with('success', $response['message']);
        } else {
            return redirect()->route('sections')->with(['error' => $response['message'], 'openModal' => true]);
        }
    }

}
