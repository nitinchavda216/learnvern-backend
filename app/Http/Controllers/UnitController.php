<?php

namespace App\Http\Controllers;

use App\Interfaces\CoursesRepositoryInterface;
use App\Interfaces\SectionRepositoryInterface;
use App\Interfaces\UnitRepositoryInterface;
use App\Models\UnitFaqs;
use App\Models\Sluglists;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;


class UnitController extends Controller
{
    protected $unitRepository;
    protected $coursesRepository;
    protected $sectionRepository;

    public function __construct(UnitRepositoryInterface $unitRepository, CoursesRepositoryInterface $coursesRepository,
                                SectionRepositoryInterface $sectionRepository)
    {
        $this->unitRepository = $unitRepository;
        $this->coursesRepository = $coursesRepository;
        $this->sectionRepository = $sectionRepository;
    }

    public function index(Request $request)
    {
        $courses = $this->coursesRepository->getCourseTitles();
        $sections = [];
        if (isset($request->course_id)) {
            $sections = $this->sectionRepository->getSectionTitles($request->course_id);
        }
        return view('units.index', compact('courses', 'sections'));
    }

    public function getAjaxListData(Request $request)
    {
        return $this->unitRepository->getAjaxListData($request);
    }

    public function create()
    {
        $courses = $this->coursesRepository->getCourseTitles();
        $sections = [];
        return view('units.create', compact('courses', 'sections'));
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'slug' => "required|unique:courses,slug"
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error', "This slug already exists. Try with another slug.");
        }
        $response = $this->unitRepository->storeUnit($request);
        if ($response['status']) {
            return redirect()->route('units', ['course_id' => $request->course_id, 'section_id' => $request->section_id])->with('success', "Data Stored Successfully!");

        }
        return redirect()->back()->with('error', $response['message']);
    }

    public function edit($id)
    {
        $unit = $this->unitRepository->getUnitDetails($id);
        $courses = $this->coursesRepository->getCourseTitles();
        $sections = $this->sectionRepository->getSectionTitles($unit->course_id);
        $attachment = $this->unitRepository->getUnitAttachment($id);
//        $oldSlugList = $this->unitRepository->getOldUnitSlugList($id);
        $oldSlugList = [];
        $relatedCourses = $this->coursesRepository->getCurrentCurriculumRelatedCourses($unit->course_id, $unit->id, 'unit');
        return view('units.edit', compact('unit', 'courses', 'oldSlugList', 'sections', 'attachment', 'relatedCourses'));
    }

    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        $response = $this->unitRepository->updateUnit($request, $id);
        if ($response['status']) {
            return redirect()->route('units', ['course_id' => $request->course_id, 'section_id' => $request->section_id])->with(['success' => "Data Updated Successfully!", 'course_id' => $request->course_id]);
        }
        return redirect()->back()->with('error', $response['message']);
    }

    public function updateSlug(Request $request): \Illuminate\Http\RedirectResponse
    {
        $response = $this->unitRepository->updateSlug($request);
        if ($response['status']){
            return redirect()->route('units.edit', $request->unit_id)->with(['success' => "Slug Updated Successfully!"]);
        }
        return redirect()->route('units.edit', $request->unit_id)->with(['error' => $response['message']]);
    }

    public function updateStatus(Request $request): \Illuminate\Http\JsonResponse
    {
        $this->unitRepository->updateActiveStatus($request);
        return response()->json(['status' => true]);
    }

    public function exportUnitData(Request $request)
    {
        $response = $this->unitRepository->getAjaxExportData($request);
        if (!$response['status']) {
            return redirect()->back()->with('error', 'Record Not Found!');
        } else {
            return Excel::download($response['data'], $response['name']);
        }
    }

    public function importUpdateUnits(Request $request): \Illuminate\Http\RedirectResponse
    {
        $this->unitRepository->importUpdateUnit($request);
        return redirect()->route('units')->with('success', "Data Updated Successfully!");
    }

    public function exportSampleUnitExcel(Request $request)
    {
        return $this->unitRepository->exportSampleUnitExcel($request);
    }

    public function importNewUnits(Request $request): \Illuminate\Http\RedirectResponse
    {
        $this->unitRepository->importNewUnits($request);
        return redirect()->route('units')->with('success', "Data Inserted Successfully!");
    }

    public function deleteUnitFaq($id): \Illuminate\Http\JsonResponse
    {
        UnitFaqs::destroy($id);
        return response()->json(['status' => true]);
    }

    public function checkSlugExists(Request $request): \Illuminate\Http\JsonResponse
    {
        $response = $this->unitRepository->CheckSlugExists($request);
        return response()->json($response);
    }

}
