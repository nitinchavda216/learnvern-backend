<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12-03-2021
 * Time: 11:32 AM
 */

namespace App\Http\Controllers;

use App\Interfaces\AdminUserRepositoryInterface;
use App\Interfaces\AdminUserRoleRepositoryInterface;
use App\Models\Admins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
class AdminUserController extends Controller
{
    protected $adminUserRepository;

    protected $adminUserRoleRepository;

    public function __construct(AdminUserRepositoryInterface $adminUserRepository, AdminUserRoleRepositoryInterface $adminUserRoleRepository)
    {
        $this->adminUserRepository = $adminUserRepository;
        $this->adminUserRoleRepository = $adminUserRoleRepository;

    }

    public function index(Request $request)
    {
        $admins = $this->adminUserRepository->getAllAdmins($request);
        return view('admin-users.index', compact('admins'));
    }

    public function createNewAdmin()
    {
        $roles= $this->adminUserRoleRepository->getRoleTitles();
        return view('admin-users.create',compact('roles'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:admins,email,NULL,id,deleted_at,NULL',
            'role_id' => "required",
        ],[
            'email.required' => "Email ID Is Already Exits",
            'role_id.required' => "Please select Course!"
        ]);

        $this->adminUserRepository->storeAdmin($request);
        return redirect()->route('admin_users')->with('success', 'Mail Has Been Sent ,Please Create Your Password.');
    }


    public function edit($id)
    {
        $admins = $this->adminUserRepository->getAdminDetail($id);
        $roles= $this->adminUserRoleRepository->getRoleTitles();
        return view('admin-users.edit',compact('admins','roles'));
    }

    public function update(Request $request, $id)
    {
        $this->adminUserRepository->updateAdmin($request, $id);
        return redirect()->route('admin_users')->with('success', "Data Created Successfully!");
    }

    public function delete($id)
    {
        Admins::destroy($id);
       /* Auth::guard('admins')->logout();*/
        /*return redirect()->route('login')->with('success', 'Account Deleted Successfully');*/
        return redirect()->route('dashboard');
    }

    public function setPasswordLink($token)
    {
        $user = $this->adminUserRepository->getByActivationToken($token);
        is_null($user) ? abort(403, 'Unauthorized action.') : null;
        return view('admin-users.add_password', compact('user'));
    }

    public function resetPassword(Request $request)
    {
        $request->validate([
            'activation_token' => 'required',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required_with:password|same:password|min:6'
            ],[
                'password.required' => "Please Enter Valid  Password! ",
                'password_confirmation.required_with' => "Your confirm password does not match with your password!"
        ]);
        $requestData = $request->all();
        $user = $this->adminUserRepository->getByActivationToken($request->activation_token);
        if (isset($user)) {
            $requestData['status'] = 1;
            $requestData['activation_token'] = null;
            $requestData['activation_date'] = Carbon::today()->toDateString();
            $user->update($requestData);
            Auth::guard('admins')->loginUsingId($user->id);
            $redirectUrl = $user->relatedRole->default_redirect_route ?? 'dashboard';
            return redirect()->route($redirectUrl)->with('success', "Your Password has Created Successful!");
        }else{
            abort(403, 'Unauthorized action.');
        }
    }
}
