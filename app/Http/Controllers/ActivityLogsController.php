<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\ActivityLogsRepositoryInterface;

class ActivityLogsController extends Controller
{
    protected $activityLogsRepository;

    public function __construct(ActivityLogsRepositoryInterface $activityLogsRepository)
    {
        $this->activityLogsRepository = $activityLogsRepository;
    }

    public function index()
    {
        return view('activity-logs.index');
    }

    public function getAjaxListData(Request $request)
    {
        return $this->activityLogsRepository->getActivityLogsData($request);
    }

    public function get_activity_changes(Request $request)
    {
        $input = $request->all();
        return $this->activityLogsRepository->getActivityLogsChangesData($input);
    }
}
