<?php
namespace App\Http\Controllers;

use App\Interfaces\AdminUserRepositoryInterface;
use App\Interfaces\CoursesRepositoryInterface;
use App\Interfaces\ReportBuilderRepositoryInterface;
use App\Models\AmbassadorProgramSettings;
use App\Models\Reports;
use App\Interfaces\LocationRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ReportExport;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Yajra\DataTables\Facades\DataTables;

class ReportBuilderController extends Controller
{
    protected $reportRepository, $locationRepository, $adminUserRepository, $autoSelectedFields, $coursesRepository;

    public function __construct(ReportBuilderRepositoryInterface $reportRepository, CoursesRepositoryInterface $coursesRepository,
                                LocationRepositoryInterface $locationRepository, AdminUserRepositoryInterface $adminUserRepository)
    {
        $this->reportRepository = $reportRepository;
        $this->locationRepository = $locationRepository;
        $this->adminUserRepository = $adminUserRepository;
        $this->coursesRepository = $coursesRepository;
        $this->autoSelectedFields = ['course_name', 'full_name', 'email', 'mobile_number', 'state', 'city'];
    }

    public function create()
    {
        $autoSelectedFields = $this->autoSelectedFields;
        $reportsData = $this->reportRepository->getConstantData();
        $states = $this->locationRepository->getStates(101);
        $ambassadorLevelList = AmbassadorProgramSettings::pluck('title','title');
        $cities = [];
        return view('report-builder.create', compact('reportsData', 'autoSelectedFields', 'states', 'cities','ambassadorLevelList'));
    }

    public function index()
    {
        $reports = $this->reportRepository->getAllReports();
        return view('report-builder.index', compact('reports'));
    }

    public function store(Request $request)
    {
        $is_valid = $this->reportRepository->validateReport($request);
        if (!empty($is_valid) && $is_valid['status'] == false) {
            return redirect()->back()->withErrors($is_valid['result'])->withInput();
        }
        if (isset($request->all()['selectField']) && !empty($request->all()['selectField'])) {
            $response = $this->reportRepository->storeReports($request);
            $requestData = $request->all();
            if ($requestData['submit_save'] == "save") {
                return redirect()->route('report-builder')->with('flash_message', 'Reports Created Successfully');
            } else {
                return redirect()->route('report-builder.viewreport', $response->id);
            }
        } else {
            return redirect()->back()->with('error', 'Please select at least one field.');
        }
    }

    public function storeNew(Request $request)
    {
        $is_valid = $this->reportRepository->validateReport($request);
        if (!empty($is_valid) && $is_valid['status'] == false) {
            return redirect()->back()->withErrors($is_valid['result'])->withInput();
        }
        if (isset($request->all()['fields'])) {
            $response = $this->reportRepository->storeNewReports($request);
            return redirect()->route('report-builder.viewreport', $response->id);
        } else {
            return redirect()->back()->with('error', 'Please select at least one field.');
        }
    }

    public function edit($id)
    {
        $report = $this->reportRepository->getReportData($id);
        $reportsData = $this->reportRepository->getConstantData();
        $selectedData['fields'] = json_decode($report->fields);
        $selectedData['conditions'] = json_decode($report->conditions, true);
        $condition_value = $this->reportRepository->getEditConstantDataDetail();
        $states = $this->locationRepository->getStates(101);
        $ambassadorLevelList = AmbassadorProgramSettings::pluck('title','title');
        $cities = [];
        if (isset($selectedData['conditions'][7]) && isset($selectedData['conditions'][7]['state_id'])) {
            $cities = $this->locationRepository->getCities($selectedData['conditions'][7]['state_id']);
        }
        return view('report-builder.edit', compact('report', 'selectedData', 'reportsData', 'condition_value', 'states', 'cities','ambassadorLevelList'));
    }

    public function update(Request $request, $id)
    {
        $is_valid = $this->reportRepository->validateReport($request);
        if (!empty($is_valid) && $is_valid['status'] == false) {
            return redirect()->back()->withErrors($is_valid['result'])->withInput();
        }
        if (isset($request->all()['selectField']) && !empty($request->all()['selectField'])) {
            $this->reportRepository->updateReports($request, $id);
            $requestData = $request->all();
            if ($requestData['submit_save'] == "update") {
                return redirect()->route('report-builder')->with('flash_message', 'Report Updated Successfully');
            } else {
                return redirect()->route('report-builder.viewreport', $id);
            }
        } else {
            return redirect()->back()->with('error', 'Please select at least one field.');
        }
    }

    public function destroy($id)
    {
        $this->reportRepository->deleteReport($id);
        return redirect()->route('report-builder')->with('flash_message', 'Report Deleted Successfully');
    }

    public function getConditionForm(Request $request)
    {
        $requestData = $request->all();
        $key = $requestData['key'];
        $element_type = $requestData['element_type'];
        $reportsData = $this->reportRepository->getConstantData();
        $condition_value = $this->reportRepository->getConstantDataDetail($requestData);
        $returnHTML = view('report-builder.partials.condition_rule', compact('key', 'element_type', 'reportsData', 'condition_value'))->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function viewReport($id, Request $request)
    {
        $requestData = $request->all();
        $adminNames = $this->adminUserRepository->getAllCRMUser();
        $fieldList = Reports::COLUMN_LIST_NAME;
        $report = $this->reportRepository->getReportData($id);
        $selectedData['report_id'] = $report->id;
        $selectedData['fields'] = isset($requestData['selectField']) ? $requestData['selectField'] : json_decode($report->fields, true);

        if (isset($requestData['conditions'])) {
            $selectedData['conditions'] = $requestData['conditions'];
        } else {
            $selectedData['conditions'] = json_decode($report->conditions, 1);
        }

        $states = $this->locationRepository->getStates(101);
        $ambassadorLevelList = AmbassadorProgramSettings::pluck('title','title');
        $cities = [];
        if (isset($selectedData['conditions'][7]) && isset($selectedData['conditions'][7]['state_id'])) {
            $cities = $this->locationRepository->getCities($selectedData['conditions'][7]['state_id']);
        }
        $reportsData = $this->reportRepository->getConstantData();
        $condition_value = $this->reportRepository->getEditConstantDataDetail();
        $all_course_titles = $this->coursesRepository->getAllActiveCourseTitles();
        return view('report-builder.viewreport', compact('adminNames', 'report', 'reportsData', 'condition_value', 'selectedData', 'fieldList',
            'states', 'cities','ambassadorLevelList', 'all_course_titles'));
    }

    public function viewReportAjaxData(Request $request)
    {
        $requestData = $request->all();
        $report = $this->reportRepository->getReportData($requestData['report_id']);
        $selectedData['report_id'] = $report->id;
        $selectedData['fields'] = json_decode($requestData['fields'], true);
        $selectedData['order']=["field"=>"course_user.id","dir"=>"DESC"];


        if(isset($requestData['order'][0]['column']) && $requestData['order'][0]['column'] > 0)
        {

            $selectedData['order']["field"] = $selectedData['fields'][$requestData['order'][0]['column']-1];
            $selectedData['order']["dir"] = $requestData['order'][0]['dir'];
        }

        if (!isset($requestData['conditions'])) {
            $selectedData['conditions'] = json_decode($report->conditions, 1);
        } else {
            $selectedData['conditions'] = json_decode($requestData['conditions'], true);
        }
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;

        $result = $this->reportRepository->buildQuery($selectedData, $pageSize, $start);
        $resultSet = collect($result['data'])->map(function($x) use ($selectedData){
            $data = (array) $x;
            $data['checkbox'] = '<input type="checkbox" class="user_id" id="'.$data['id'].'" value="'.$data['id'].'">';
            foreach ($selectedData['fields'] as $column) {
                if (in_array($column, ['course_enroll_date', 'certificate_date', 'last_used_date', 'payment_completed_date', 'created_at', 'last_login_date', 'last_badge_date', 'abandoned_payment_date'])){
                    $data[$column] = $data[$column] !='' ? formatDate($data[$column], 'd-m-Y') : '--';
                }else{
                    $data[$column] = $data[$column] !='' ? $data[$column] :'--';
                }
            }
            return $data;
        })->toArray();


        $count_filter = $result['count_total'];
        $count_total = $result['count_total'];


        $data_tables = [
            "draw"=>$requestData['draw'],
            "recordsTotal"=>$count_total,
            "recordsFiltered"=>$count_filter,
            "data"=>$resultSet
        ];

        return response()->json($data_tables);

    }


    public function export($id)
    {
        $report = $this->reportRepository->getReportData($id);
        $selectedData['report_id'] = $report->id;
        $selectedData['fields'] = json_decode($report->fields);
        $selectedData['conditions'] = json_decode($report->conditions, 1);
        $selectedData['order']=["field"=>"course_user.id","dir"=>"DESC"];
        $result = $this->reportRepository->buildQuery($selectedData);
        $resultSet = collect($result['data'])->map(function($x) use ($selectedData){
            $data = (array) $x;
            foreach ($selectedData['fields'] as $column) {
                if (in_array($column, Reports::DATE_TYPE_COLUMNS)){
                    $data[$column] = Date::dateTimeToExcel(Carbon::parse(formatDate($data[$column], 'Y-m-d') . " 00:00:00"));
                }
            }
            return $data;
        })->toArray();
        return Excel::download(new ReportExport($resultSet, $selectedData['fields']), $report->name . '.xlsx');
    }
}
