<?php

namespace App\Http\Controllers;

use App\Interfaces\CoursesRepositoryInterface;
use App\Interfaces\FaqsRepositoryInterface;
use App\Models\Faqs;
use Illuminate\Http\Request;

class FaqsController extends Controller
{
    protected $coursesRepository;
    protected $faqsRepository;

    public function __construct(CoursesRepositoryInterface $coursesRepository, FaqsRepositoryInterface $faqsRepository)
    {
        $this->coursesRepository = $coursesRepository;
        $this->faqsRepository = $faqsRepository;
    }

    public function index()
    {
        $courses = $this->coursesRepository->getCourseTitles();
        return view('faqs.index', compact('courses'));
    }

    public function getAjaxListData(Request $request)
    {
        return $this->faqsRepository->getAjaxListData($request);
    }

    public function create()
    {
        $courses = $this->coursesRepository->getCourseTitles();
        return view('faqs.create', compact('courses'));
    }

    public function store(Request $request)
    {
        $this->faqsRepository->storeFaqData($request);
        $this->coursesRepository->storeSchemaScript($request->course_id);
        return redirect()->route('faqs')->with('success', "Data Stored Successfully!");
    }

    public function edit($id)
    {
        $faq = $this->faqsRepository->getFaqDetail($id);
        $courses = $this->coursesRepository->getCourseTitles();
        return view('faqs.edit', compact('faq', 'courses'));
    }

    public function update(Request $request, $id)
    {
        $this->faqsRepository->updateFaqData($request, $id);
        $this->coursesRepository->storeSchemaScript($request->course_id);
        return redirect()->route('faqs')->with('success', "Data Updated Successfully!");
    }

    public function updateStatus(Request $request)
    {
        $course_id = $this->faqsRepository->updateActiveStatus($request);
        $this->coursesRepository->storeSchemaScript($course_id);
        return response()->json(['status' => true]);
    }

    public function getFaqsByCourse($course_id)
    {
        $data = $this->faqsRepository->getFaqsByCourse($course_id);
        $html = view('faqs.partials.table', compact('data'))->render();
        return response()->json(['status' => true, 'html' => $html]);
    }

    public function delete($id)
    {
        $faq = Faqs::find($id);
        if(isset($faq)){
            $faq->delete();
            $this->coursesRepository->storeSchemaScript($faq->course_id);
        }
        if (request()->ajax()) {
            return response()->json(['status' => true]);
        }
        return redirect()->route('faqs')->with('success', "Data Deleted Successfully!");
    }


}
