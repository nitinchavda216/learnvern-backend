<?php

namespace App\Http\Controllers;

use App\Interfaces\AdminUserRoleRepositoryInterface;
use Illuminate\Http\Request;

class AdminUserRoleController extends Controller
{
    protected $adminUserRoleRepository;

    public function __construct(AdminUserRoleRepositoryInterface $adminUserRoleRepository)
    {
        $this->adminUserRoleRepository = $adminUserRoleRepository;
    }

    public function index()
    {
        $admin_role = $this->adminUserRoleRepository->getAllUserRole();
        return view('admin-user-role.index', compact('admin_role'));
    }

    public function create()
    {
        $lms_modules = $this->adminUserRoleRepository->getAdminModulesByType('lms');
        $crm_modules = $this->adminUserRoleRepository->getAdminModulesByType('crm');

        return view('admin-user-role.create', compact('lms_modules', 'crm_modules'));
    }

    public function store(Request $request)
    {
        $this->adminUserRoleRepository->storeAllData($request);
        return redirect()->route('admin_role')->with('success', "Data Stored Successfully!");
    }

    public function edit($id)
    {
        $adminRole = $this->adminUserRoleRepository->getAdminRoleDetails($id);

        $lms_modules = $this->adminUserRoleRepository->getAdminModulesByType('lms');
        $crm_modules = $this->adminUserRoleRepository->getAdminModulesByType('crm');
        $current_selected_modules = $this->adminUserRoleRepository->getSelectedModule($id);
        return view('admin-user-role.edit', compact('crm_modules', 'lms_modules', 'current_selected_modules', 'adminRole'));
    }

    public function update(Request $request, $id)
    {
        $this->adminUserRoleRepository->updateAdminModule($request, $id);
        return redirect()->route('admin_role')->with('success', "Data Updated Successfully!");

    }

}
