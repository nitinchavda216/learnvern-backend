<?php

namespace App\Http\Controllers;

use App\Interfaces\AbandonedPaymentRepositoryInterface;
use App\Interfaces\PartnerApplicationRepositoryInterface;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class AbandonedPaymentsController extends Controller
{
    protected $abandonedPaymentRepository;
    protected $partnerApplicationRepository;

    public function __construct(AbandonedPaymentRepositoryInterface $abandonedPaymentRepository, PartnerApplicationRepositoryInterface $partnerApplicationRepository)
    {
        $this->abandonedPaymentRepository = $abandonedPaymentRepository;
        $this->partnerApplicationRepository = $partnerApplicationRepository;
    }

    public function index()
    {
        $partners = $this->partnerApplicationRepository->getPartnersName();
        return view('abandoned-reort.index', compact('partners'));
    }

    public function getAbandonedPaymentsAjaxData(Request $request)
    {
        return $this->abandonedPaymentRepository->getAjaxListData($request);
    }

    public function getAjaxExportData(Request $request)
    {
        $response = $this->abandonedPaymentRepository->getAjaxExportData($request);
        if (!$response['status']) {
            return redirect()->back()->with('error', 'Record Not Found! Please select different date.');
        } else {
            return Excel::download($response['data'], 'Abandoned_Payment.xlsx');
        }
    }

}
