<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\ConfigurationRepositoryInterface;

class ConfigurationController extends Controller
{
    protected $configurationRepository;

    public function __construct(ConfigurationRepositoryInterface $configurationRepository)
    {
        $this->configurationRepository = $configurationRepository;
    }
    public function index()
    {
        $configuration = $this->configurationRepository->getdetails();
//        dd($configuration);
        return view('configuration.index', compact('configuration'));
    }

    public function update(Request $request)
    {
        $configuration = $this->configurationRepository->update($request);
//        dd($configuration);
        return redirect()->route('configuration')->with('success', "Data Created Successfully!");
    }
}
