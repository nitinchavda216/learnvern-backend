<?php
namespace App\Http\Controllers;

use App\Interfaces\AppUpdateRepositoryInterface;
use Illuminate\Http\Request;

class AppUpdateController extends Controller
{
    protected $appUpdateRepository;

    public function __construct(AppUpdateRepositoryInterface $appUpdateRepository)
    {
        $this->appUpdateRepository = $appUpdateRepository;
    }

    public function index()
    {
        $updates = $this->appUpdateRepository->getAppUpdateList();
        return view('app_updates.index', compact('updates'));
    }

    public function create()
    {
        $last_version = $this->appUpdateRepository->getLastVersionDetails();
        return view('app_updates.create', compact('last_version'));
    }

    public function store(Request $request)
    {
        $this->appUpdateRepository->storeAppUpdate($request);
        return redirect()->route('app_updates')->with('success', 'Data Created Successfully!');
    }

    public function view($id)
    {
        $version_details = $this->appUpdateRepository->getVersionDetails($id);
        return view('app_updates.view', compact('version_details'));
    }
}
