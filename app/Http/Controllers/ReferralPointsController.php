<?php

namespace App\Http\Controllers;

use App\Interfaces\AmbassadorProgramRepositoryInterface;
use App\Interfaces\PartnerApplicationRepositoryInterface;
use App\Interfaces\ReferralPointRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Maatwebsite\Excel\Facades\Excel;

class ReferralPointsController extends Controller
{
    protected $referralPointRepository;
    protected $ambassadorProgramRepository;
    protected $partnerApplicationRepository;

    public function __construct(ReferralPointRepositoryInterface $referralPointRepository, AmbassadorProgramRepositoryInterface $ambassadorProgramRepository,
                PartnerApplicationRepositoryInterface $partnerApplicationRepository)
    {
        $this->referralPointRepository = $referralPointRepository;
        $this->ambassadorProgramRepository = $ambassadorProgramRepository;
        $this->partnerApplicationRepository = $partnerApplicationRepository;
    }

    public function index()
    {
        $ambassador_levels = $this->ambassadorProgramRepository->getAmbassadorLevelTitles();
        $partners = $this->partnerApplicationRepository->getPartnersName();
        return  view('referral-point.index', compact('ambassador_levels', 'partners'));
    }

    public function getAjaxListData(Request $request)
    {
        return $this->referralPointRepository->getAjaxListData($request);
    }

    public function sendAmbassadorMail()
    {
        Artisan::queue("send:ambassador_certificate");
        return redirect()->back()->with('success', 'Ambassador Mail Sent Successfully!');
    }

    public function exportData(Request $request)
    {
        $response = $this->referralPointRepository->exportListData($request);
        if (!$response['status']){
            return redirect()->back()->with('error', 'Record Not Found!');
        }else{
            return Excel::download($response['data'], 'Ambassador Report.xlsx');
        }
    }
}
