<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\AnnouncementInterface;
use App\Models\AnnouncementManagement;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class AnnouncementController extends Controller
{
    //
    protected $announcementRepository;

    public function __construct(AnnouncementInterface $announcementRepository)
    {
        $this->announcementRepository = $announcementRepository;
    }

    public function index()
    {
        return view('announcement_managment.index');
    }


    public function getAjaxListData(Request $request)
    {
        return $this->announcementRepository->getAjaxListData($request);
    }

    public function create()
    {
        return view('announcement_managment.create');
    }

    public function store(Request $request)
    {
        $this->announcementRepository->store($request);
        return redirect()->route('announcement-management')->with('success', "Data Stored Successfully!");
    }

    public function edit($id)
    {
        $announcement = $this->announcementRepository->getAnnouncementDetail($id);
        return view('announcement_managment.edit', compact('announcement'));
    }

    public function update(Request $request, $id)
    {
        $this->announcementRepository->updateAnnouncementData($request, $id);
        return redirect()->route('announcement-management')->with('success', "Data Updated Successfully!");
    }

    public function updateStatus(Request $request)
    {
        $this->announcementRepository->updateActiveStatus($request);
        return response()->json(['status' => true]);
    }

    public function delete($id)
    {
        $faq = AnnouncementManagement::find($id);
        if (isset($faq)) {
            $faq->delete();
        }
        if (request()->ajax()) {
            return response()->json(['status' => true]);
        }
        return redirect()->route('announcement-management')->with('success', "Data Deleted Successfully!");
    }


}
