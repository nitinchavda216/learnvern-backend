<?php

namespace App\Http\Controllers;

use App\Interfaces\PartnerApplicationRepositoryInterface;
use App\Interfaces\UsersReportRepositoryInterface;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UsersReportController extends Controller
{
    protected $userReportRepository;
    protected $partnerApplicationRepository;

    public function __construct(UsersReportRepositoryInterface $userReportRepository, PartnerApplicationRepositoryInterface $partnerApplicationRepository)
    {
        $this->userReportRepository = $userReportRepository;
        $this->partnerApplicationRepository = $partnerApplicationRepository;
    }

    public function index()
    {
        $partners = $this->partnerApplicationRepository->getPartnersName();
        return view('reports.user_report', compact('partners'));
    }

    public function getAjaxListData(Request $request)
    {
        return $this->userReportRepository->getAjaxListData($request);
    }

    public function getAjaxExportData(Request $request)
    {
        $response = $this->userReportRepository->getAjaxExportData($request);
        if (!$response['status']){
            return redirect()->back()->with('error', 'Record Not Found! Please select different date.');
        }else{
            return Excel::download($response['data'], 'Users_Report.xlsx');
        }
    }

    public function getReferralReport()
    {
        $partners = $this->partnerApplicationRepository->getPartnersName();
        return view('reports.referral_report', compact('partners'));
    }

    public function getReferralReportAjaxListData(Request $request)
    {
        return $this->userReportRepository->getReferralReportAjaxListData($request);
    }

    public function exportReferralReportData(Request $request)
    {
        $response = $this->userReportRepository->exportReferralReportData($request);
        if (!$response['status']){
            return redirect()->back()->with('error', 'Record Not Found! Please select different date.');
        }else{
            return Excel::download($response['data'], 'Referrals_Report.xlsx');
        }
    }

    public function userMonthReport(Request $request)
    {
        $data = $this->userReportRepository->getMonthlyData($request);
        return view('reports.user_month_vise_report', compact('data'));
    }

    public function getUnUnrolledUsers()
    {
        $partners = $this->partnerApplicationRepository->getPartnersName();
        return view('reports.un_enrolled_users_report', compact('partners'));
    }

    public function getUnUnrolledUsersAjaxList(Request $request)
    {
        return $this->userReportRepository->getUnUnrolledUsersAjaxList($request);
    }

    public function exportUnEnrolledUsersList(Request $request)
    {
        $response = $this->userReportRepository->exportUnEnrolledUsersList($request);
        if (!$response['status']){
            return redirect()->back()->with('error', 'Record Not Found! Please select different date.');
        }else{
            return Excel::download($response['data'], 'Un_Enrolled_Users_Report.xlsx');
        }
    }
}
