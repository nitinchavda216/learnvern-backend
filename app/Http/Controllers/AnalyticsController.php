<?php

namespace App\Http\Controllers;

use App\Interfaces\AnalyticsRepositoryInterface;
use Spatie\Analytics;
use Spatie\Analytics\Period;
use Illuminate\Http\Request;

class AnalyticsController extends Controller
{
    protected $analyticsRepository;

    public function __construct(AnalyticsRepositoryInterface $analyticsRepository)
    {
        $this->analyticsRepository = $analyticsRepository;
    }

    public function index(Request $request)
    {
        $analyticsReportData = $this->analyticsRepository->getAnalyticsReferralDataByDate($request);
        return view('analytics.index', compact('analyticsReportData'));
    }

    public function referralVisitors()
    {
        $analyticsReportData = $this->analyticsRepository->getHistoricalAnalyticsReferralData();
        echo "Cron run successfully!";
        die;
    }


}
