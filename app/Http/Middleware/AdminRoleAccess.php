<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13-04-2021
 * Time: 04:22 PM
 */

namespace App\Http\Middleware;

use App\Models\AdminModules;
use App\Models\RoleWiseModuleAccess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Closure;

class AdminRoleAccess
{
    public function handle(Request $request, Closure $next)
    {
        $user = auth()->guard('admins')->user();
        if ($user->relatedRole->is_super_admin == 1){
            return $next($request);
        }
        $module_access = RoleWiseModuleAccess::where('role_id', $user->role_id)->pluck('module_key')->toArray();
        $currentPath = \Request::route()->getName();
        $words = explode(".", $currentPath);
        if (isset($words[1]) && $words[1] != "") {
            array_splice($words, -1);
            $route_name = implode(".", $words);
        } else {
            $route_name = $currentPath;
        }
        $pageName = AdminModules::where('route_name', $route_name)->select('id', 'module_keys')->get()->toArray();
        if (!empty($pageName)) {
            $accessUrl = $pageName[0]['module_keys'];
            if (in_array($accessUrl, $module_access)) {
                return $next($request);
            } else {
                abort(401, 'This action is unauthorized.');
            }
        } else {
            return $next($request);
        }
    }

}
