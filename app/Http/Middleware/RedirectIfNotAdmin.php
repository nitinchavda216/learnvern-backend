<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'admins')
    {
        if (!Auth::guard($guard)->check()) {
            return redirect()->route('login');
        }
        session(['partner_id' => Auth::guard($guard)->user()->partner_id]);
        $request['partner_id'] = Auth::guard($guard)->user()->partner_id;
        return $next($request);
    }
}
