<?php

namespace App\Http\Middleware;

use App\Models\UsersToken;
use Closure;

class RedirectIfNotExistUserToken
{
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');
        if ($token) {
            $token = explode(' ', $token);
            if (@$token[1]) {
                $token = $token[1];
            }
            $user = UsersToken::where('token', $token)->first();
            if (isset($user)) {
                $request['user_id'] = $user['user_id'];
            } else {
//                $request['user_id'] = null;
                return response()->json([
                    'status' => 401,
                    'message' => "Invalid user access token!",
                    'data' => null,
                ]);
            }
        }
        return $next($request);
    }
}
