<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class AfterAPIMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        if ($response->getData()->status == 100) {
            $url = URL::current();
            $date = formatDate(Carbon::now(), 'd_m_Y');
            $logEbayURL = new Logger('API Error');
            $logEbayURL->pushHandler(new StreamHandler(storage_path('logs/api_error_' . $date . '.log')), Logger::ERROR);
            $logEbayURL->info('Erro Info:', ['api_url' => $url, 'request' => $request->all(), 'response' => $response->getData()]);
        }
        return $response;
    }
}
