<?php

namespace App\Http\Middleware;

use Closure;

class ValidateApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->header('apiSecretKey') == config('services.api.secret_key')) {
            return $next($request);
        }
        $response_data = [
            'status' => 100,
            'message' => 'Api header secret key not found.'
        ];

        return response()->json($response_data);
    }
}
