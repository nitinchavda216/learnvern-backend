<?php

namespace App\Http\Middleware;

use Closure;

class RequiredApiAuthURL
{
    public function handle($request, Closure $next)
    {
        if (isset($request->user_id)) {
            return $next($request);
        }
        return response()->json([
            'status' => 100,
            'message' => "You can't access without login.",
            'data' => null]);
    }
}
