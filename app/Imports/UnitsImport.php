<?php

namespace App\Imports;

use App\Models\Units;
//use App\Units;
use Maatwebsite\Excel\Concerns\ToModel;

class UnitsImport implements ToModel
{
    private $course_id;

    public function __construct($course_id)
    {
        $this->course_id = $course_id;

    }
    public function model(array $row)
    {
        return new Units([
            'id' => $row[0],
            'course_id' => $row[1],
            'name' =>$row[2],
            'video_url' => $row[3],
            'short_content' => $row[4],
            'meta_title' => $row[5],
            'meta_description' => $row[6],
            'meta_keywords' => $row[7],
            'schema_script' => $row[8],
            'canonical_url' =>$row[9],
            /*  'faq_content_1' => $row[10],
             'faq_content_2' => $row[11],
             'faq_content_3' => $row[12],
             'faq_content_4' => $row[13],
             'faq_content_5' => $row[14],*/
        ]);

    }
}
