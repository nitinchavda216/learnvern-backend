<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use App\Models\User;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class CsvImport implements ToModel,WithHeadingRow
{
    /**
    * @param Collection $collection
    */


    public function model(array $row)
    {
//        dd($row);
        return new User([
            'email'     => $row['emailid'],
            'gender'    => $row['gender'],
            'mobile_number'    => $row['mobilenumber'],
        ]);
    }
}
