<?php

namespace App\Imports;

use App\Assignment;
use Maatwebsite\Excel\Concerns\ToModel;

class AssignmentImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    private $course_id;

    public function __construct($course_id)
    {

    }

    public function model(array $row)
    {
        return new Assignment([
            'id' => $row[0],
            'course_id' => $row[1],
            'section_id' =>$row[2],
            'name' => $row[3],
            'slug' => $row[4],
            'video_id' => $row[5],
            'mark' => $row[6],

        ]);
    }
}
