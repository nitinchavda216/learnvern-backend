<?php

namespace App\Imports;

use App\Quiz;
use Maatwebsite\Excel\Concerns\ToModel;

class QuizImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    private $course_id;

    public function __construct($course_id)
    {
        $this->course_id = $course_id;

    }
    public function model(array $row)
    {
        return new Quiz([
            'id' => $row[0],
            'course_id' => $row[1],
            'section_id' =>$row[2],
            'name' => $row[3],
            'slug' => $row[4],
            'sub_title' => $row[5],
            'time' => $row[6],

        ]);
    }
}
