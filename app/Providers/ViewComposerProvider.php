<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12-04-2021
 * Time: 02:25 PM
 */

namespace App\Providers;


use App\Http\ViewComposer\AdminViewComposer;
use Carbon\Laravel\ServiceProvider;

class ViewComposerProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer(['layout.master'],AdminViewComposer::class);
    }

}
