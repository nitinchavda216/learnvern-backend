<?php

namespace App\Providers;

use App\Repositories\Api\CoursesRepository;
use App\Repositories\Api\QuizRepository;
use App\Repositories\SiteConfigurationsRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (env('REDIS_CACHE_ENABLED')) {
            $this->app->singleton(CoursesRepository::class, function () {
                $baseRepo = new CoursesRepository();
                $cachingRepo = new \App\Repositories\Decorators\CoursesRepository($baseRepo, $this->app['cache.store']);
                return $cachingRepo;
            });
            $this->app->singleton(SiteConfigurationsRepository::class, function () {
                $baseRepo = new SiteConfigurationsRepository();
                $cachingRepo = new \App\Repositories\Decorators\SiteConfigurationsRepository($baseRepo, $this->app['cache.store']);
                return $cachingRepo;
            });
            $this->app->singleton(QuizRepository::class, function () {
                $baseRepo = new QuizRepository();
                $cachingRepo = new \App\Repositories\Decorators\QuizRepository($baseRepo, $this->app['cache.store']);
                return $cachingRepo;
            });
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
