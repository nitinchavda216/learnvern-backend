<?php

namespace App\Providers;

use App\EventListeners\AdminSetPasswordEventListener;
use App\EventListeners\AssignAmbassadorReferralBadgeEventListener;
use App\EventListeners\ContactFormSubmissionHandler;
use App\EventListeners\PartnerApplicationApprovedEventListener;
use App\EventListeners\PaymentConfirmationEmailEventHandler;
use App\EventListeners\SendAmbassadorMailEventListener;
use App\EventListeners\SendDailyReportMailEventListener;
use App\EventListeners\SendResetPasswordEventHandler;
use App\EventListeners\UpdateCourseSlugMailEventListener;
use App\EventListeners\UpdateCourseTimeEventListener;
use App\EventListeners\UpdateUnitSlugMailEventListener;
use App\EventListeners\UserRegistrationHandler;
use App\EventListeners\SendAmbassadorProcessEmailEventListener;
use App\Events\AdminSetPasswordEvent;
use App\EventListeners\SendCourseMailEventListener;
use App\Events\AssignAmbassadorReferralBadgeEvent;
use App\Events\ContactFormSubmissionEvent;
use App\Events\PartnerApplicationApproved;
use App\Events\PaymentConfirmationEmailEvent;
use App\Events\SendAmbassadorMailEvent;
use App\Events\SendDailyReportMailEvent;
use App\Events\SendResetPasswordMailEvent;
use App\Events\UpdateCourseSlugMailEvent;
use App\Events\UpdateCourseTimeEvent;
use App\Events\SendCourseMailEvent;
use App\Events\UpdateUnitSlugMailEvent;
use App\Events\UserRegistration;
use App\Events\SendAmbassadorProcessEmailEvent;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        UpdateCourseTimeEvent::class => [
            UpdateCourseTimeEventListener::class,
        ],
        SendAmbassadorMailEvent::class => [
            SendAmbassadorMailEventListener::class,
        ],
        AdminSetPasswordEvent::class => [
            AdminSetPasswordEventListener::class,
        ],
        SendCourseMailEvent::class => [
            SendCourseMailEventListener::class,
        ],
        PartnerApplicationApproved::class => [
            PartnerApplicationApprovedEventListener::class
        ],
        UserRegistration::class => [
            UserRegistrationHandler::class
        ],
        SendAmbassadorProcessEmailEvent::class => [
            SendAmbassadorProcessEmailEventListener::class
        ],
        ContactFormSubmissionEvent::class => [
            ContactFormSubmissionHandler::class
        ],
        SendResetPasswordMailEvent::class => [
            SendResetPasswordEventHandler::class
        ],
        PaymentConfirmationEmailEvent::class => [
            PaymentConfirmationEmailEventHandler::class
        ],
        AssignAmbassadorReferralBadgeEvent::class => [
            AssignAmbassadorReferralBadgeEventListener::class
        ],
        SendDailyReportMailEvent::class => [
            SendDailyReportMailEventListener::class
        ],
        UpdateCourseSlugMailEvent::class => [
            UpdateCourseSlugMailEventListener::class
        ],
        UpdateUnitSlugMailEvent::class => [
            UpdateUnitSlugMailEventListener::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
