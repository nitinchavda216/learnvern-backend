<?php

namespace App\Jobs;

use App\Models\Course;
use App\Models\Notification;
use App\Models\UserFcmToken;
use App\Services\PushNotificationService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class SendQueuePushNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $details, $pushNotificationService;
    public $timeout = 7200;

    public function __construct($details)
    {
        $this->details = $details;
        $this->pushNotificationService = app()->make(PushNotificationService::class);
    }

    public function handle()
    {
        $campaign_id = $this->details['campaign_id'];
        $notificationDetails = $this->details['notification_details'];
        $conditionApply = $this->details['condition'];
        $userData = DB::select("SELECT users.id, users.first_name, UFT.fcm_token, courses.id as course_id, courses.course_type  FROM users
                            LEFT JOIN  course_user ON (course_user.user_id=users.id)
                            LEFT JOIN  courses ON (course_user.course_id=courses.id)
                            LEFT JOIN  user_fcm_tokens UFT ON (UFT.user_id=users.id)
                            WHERE course_user.user_id=users.id
                            AND UFT.user_id=users.id
                            " . $conditionApply . "  GROUP BY users.id");
        if ($notificationDetails['link_type'] == "specific_course") {
            $course = Course::where('id', $notificationDetails['course_link_id'])->select('id', 'course_type')->first();
            $app_dynamic_link = env('FCM_DYNAMIC_LINK_DOMAIN') . '?type=' . (($course->course_type == 4) ? 'webinar' : 'course') . '&id=' . $course->id;
        } elseif ($notificationDetails['link_type'] == "custom_link") {
            $app_dynamic_link = $notificationDetails['custom_link'];
        }
        foreach ($userData as $user) {
            if ($notificationDetails['link_type'] == "enroll_course") {
                $app_dynamic_link = env('FCM_DYNAMIC_LINK_DOMAIN') . '?type=' . (($user->course_type == 4) ? 'webinar' : 'course') . '&id=' . $user->id;
            }
            Notification::create([
                'user_id' => $user->id,
                'notification_type_id' => $campaign_id,
                'notification_type' => 'Campaign',
                'title' => $notificationDetails['title'],
                'description' => $notificationDetails['description'],
                'link' => $app_dynamic_link,
                'image' => $notificationDetails['image']
            ]);
            $addData = [
                'image' => isset($notificationDetails['image']) ? showNotificationImage($notificationDetails['image']) : "",
                'data' =>  ['app_dynamic_link' => $app_dynamic_link]
            ];
            $fcmTokens = UserFcmToken::where('user_id', $user->id)->pluck('fcm_token')->toArray();
            if (!empty($fcmTokens)) {
                $this->pushNotificationService->notify_push($fcmTokens, $notificationDetails['title'], $notificationDetails['description'], $addData);
            }
        }
    }
}
