<?php

namespace App\Jobs;

use App\Events\UpdateMauticDataEvent;
use App\Models\CourseUser;
use App\Models\User;
use App\Models\UserFcmToken;
use App\Models\UserResumeDetails;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class UpdateMarketLearnVernDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userDetail = $this->user;
        $user_id = $userDetail->id;
        $userInfoArray = [];
        $last_login_date = $userDetail->last_login_date ? Carbon::createFromFormat('Y-m-d H:i:s', $userDetail->last_login_date, 'Asia/Kolkata') : Carbon::now();
        $courseData = DB::select("select id,t1.progress from courses
                                      LEFT JOIN (SELECT course_id,ROUND(progress) as progress from course_user where user_id=$user_id)t1 ON courses.id = t1.course_id
                                      ");
        $userResumeDetail = UserResumeDetails::where('user_id', $user_id)->first();
        $name = explode(' ', $userDetail->first_name);
        $userInfoArray['points'] = 1;
        $userInfoArray['last_active'] = $last_login_date->setTimezone('UTC')->format('d-m-Y H:i');
        $userInfoArray['title'] = "";
        $userInfoArray['firstname'] = $name[0] ?? $userDetail->username;
        $userInfoArray['lastname'] = $name[1] ?? '';
        $userInfoArray['company'] = '';
        $userInfoArray['position'] = '';
        $userInfoArray['email'] = $userDetail->email;
        $userInfoArray['phone'] = '';
        $userInfoArray['mobile'] = $userDetail->mobile_number ?? '';
        $userInfoArray['address1'] = $userDetail->address ?? '';
        $userInfoArray['address2'] = '';
        $userInfoArray['city'] = $userDetail->city ?? '';
        $userInfoArray['state'] = $userDetail->state ? ucwords(strtolower($userDetail->state)) : '';
        $userInfoArray['zipcode'] = $userDetail->pin_code ?? '';
        $userInfoArray['timezone'] = '';
        $userInfoArray['country'] = $userDetail->country ? $userDetail->country : '';
        $userInfoArray['fax'] = '';
        $userInfoArray['preferred_locale'] = '';
        $userInfoArray['attribution_date'] = '';
        $userInfoArray['attribution'] = '';
        $userInfoArray['website'] = '';
        $userInfoArray['doi'] = '';
        $userInfoArray['gdpr_acceptance'] = '';
        //Add Course Column
        foreach ($courseData as $item) {
            $userInfoArray["f_" . $item->id] = !is_null($item->progress) ? $item->progress : "-1";
        }
        //End Course Column
        $userInfoArray['no_of_user_referred'] = $userDetail->referred_count ?? 0;
        $userInfoArray['own_referral_code'] = $userDetail->own_referral_code;
        $userInfoArray['sign_up_referral_code'] = $userDetail->referral ?? "";
        $userInfoArray['register_date'] = formatDate($userDetail->created_at, 'd-m-Y');
        $userInfoArray['course_enroll_date'] = '';
        $userInfoArray['last_login_date'] = $last_login_date->setTimezone('UTC')->format('d-m-Y');
        $userInfoArray['sign_up_from'] = $userDetail->signup_from ? User::SIGNUP_FROM[$userDetail->signup_from] : '';
        $userInfoArray['course_progress'] = '';
        $userInfoArray['referral_link'] = 'https://www.learnvern.com/r/' . $userDetail->own_referral_code;
        $userInfoArray['course_referral_link'] = '';
        $userInfoArray['nos_of_courses_enrolled'] = CourseUser::where('user_id', $user_id)->count();
        $userInfoArray['completed_courses'] = CourseUser::where('user_id', $user_id)->where('user_status_id', 4)->count();;
        $userInfoArray['current_occupation'] = $userDetail->current_occupation ?? '';
        $userInfoArray['nos_of_certificates_bough'] = CourseUser::where('user_id', $user_id)->where('paid_status', 1)->count();
        $userInfoArray['no_of_certificates_bought'] = $userInfoArray['nos_of_certificates_bough'];
        $userInfoArray['facebook'] = "";
        $userInfoArray['foursquare'] = "";
        $userInfoArray['instagram'] = $userResumeDetail->social_links['instagram'] ?? '';
        $userInfoArray['linkedin'] = $userResumeDetail->social_links['linked_in'] ?? '';
        $userInfoArray['skype'] = '';
        $userInfoArray['twitter'] = '';
        $userInfoArray['app_installed'] = "Yes";
        $fcmTokens = UserFcmToken::where('user_id', $user_id)->orderBy('id', 'DESC')->first();
        $userInfoArray['fcm_token'] = $fcmTokens->fcm_token ?? "";
        $siteurl = "market.learnvern.com";
        $loginname = 'apiadmin';
        $password = 'LearnVern2021';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://" . $loginname . ":" . $password . "@" . $siteurl . "/api/contacts/new",
            CURLOPT_USERAGENT => 'Mautic Connector',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $userInfoArray
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        if (isset(json_decode($response)->errors)) {
            $date = formatDate(Carbon::now(), 'd_m_Y');
            $logEbayURL = new Logger('Mautic Update Error');
            $logEbayURL->pushHandler(new StreamHandler(storage_path('logs/mautic_user_errors_' . $date . '.log')), Logger::ERROR);
            $logEbayURL->info('Updated User:', ['user_id' => $user_id, 'site_url' => $siteurl, 'response' => $response]);
        }
    }
}
