<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class KeywordSearchHistory extends Model {

    use HasFactory;

    protected $table = 'keyword_search_history';
    protected $connection = 'mongodb';
    protected $collection = 'keyword_search_history';
    protected $fillable = ['keyword', 'results', 'course_ids'];

}
