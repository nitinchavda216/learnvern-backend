<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';

    protected $primaryKey = 'id';

    public function countryDetail()
    {
        return $this->hasOne(Country::class, 'id','country_id');
    }
}
