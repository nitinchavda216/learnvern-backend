<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppUpdateDetails extends Model
{
    use SoftDeletes;

    protected $table = 'app_update_details';

    protected $fillable = ['title', 'version', 'platform', 'update_type', 'content'];


}
