<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseAttachment extends Model
{
    protected $table = 'course_attachments';

    protected $primaryKey = 'id';

    protected $fillable = ['curriculum_id', 'curriculum_type', 'title','attachment'];

    public function unitDetail(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Units::class, 'id', 'curriculum_id');
    }
    public function assignmentDetail(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Assignment::class, 'id', 'curriculum_id');
    }
}
