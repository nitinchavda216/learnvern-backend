<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WidgetsSettings extends Model
{
    const PAGE_SECTION = [
        "home_main_banner_header_text" => "Home Main Banner Header Text",
        "home_main_banner_subheader_text" => "Home Main Banner Sub-Header Text",
        "home_main_banner_block1" => "Home Main Banner Block 1",
        "home_main_banner_block2" => "Home Main Banner Block 2",
        "home_main_banner_block3" => "Home Main Banner Block 3",
        "nsdc_promotions" => "NSDC Promotions",
        "learnvern_promotions" => "Learnvern Promotions",
        "exclusive_features_section1" => "Exclusive Features Section 1",
        "exclusive_features_section2" => "Exclusive Features Section 2",
        "exclusive_features_section3" => "Exclusive Features Section 3",
        "app_promotions" => "App Promotions",

    ];
    protected $table = 'widgets_settings';

    protected $primaryKey = 'id';

    protected $fillable = [ 'name', 'page_type','contenttype', 'page_title','image','slug','content', 'created_at', 'updated_at'];

    public function scopeByActive($q)
    {
        $q->where('is_active', 1);
    }

    const IMG_URL = "storage/widgetssettings/";
    const IMG_PATH = "app/public/widgetssettings/";


}
