<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityLogs extends Model
{
    protected $table = 'activity_log';
    protected $primaryKey = 'id';

    protected $fillable = ['log_name', 'description', 'subject_type', 'subject_id ', 'causer_type ', 'causer_id','properties','created_at','updated_at'];

    public function userDetail()
    {
        return $this->hasOne(Admins::class, 'id', 'causer_id');
    }
}
