<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserResumeDetails extends Model
{
    protected $table = 'user_resume_details';

    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'name', 'title', 'email', 'phone_number', 'address', 'image', 'social_links', 'courses', 'summary', 'hobby_details', 'language_details',
        'education_details', 'experience_details', 'key_skill_details', 'technical_skill_details'];

    protected $casts = [
        'courses' => 'array',
        'summary' => 'array',
        'hobby_details' => 'array',
        'language_details' => 'array',
        'education_details' => 'array',
        'experience_details' => 'array',
        'key_skill_details' => 'array',
        'technical_skill_details' => 'array'
    ];

    /**
     * Biography
     */
    public function getBiographyAttribute($value)
    {
        return json_decode($value, true);
    }

    /**
     * Summary
     */
    public function getSummaryAttribute($value)
    {
        return json_decode($value, true);
    }

    /**
     * Courses
     */
    public function getCoursesAttribute($value)
    {
        return json_decode($value, true);
    }

    public function setCoursesAttribute($value)
    {
        $this->attributes['courses'] = json_encode($value);
    }

    /**
     * HOBBY DETAILS
     */
    public function getHobbyDetailsAttribute($value)
    {
        return json_decode($value, true);
    }

    /**
     * LANGUAGE DETAILS
     */
    public function getLanguageDetailsAttribute($value)
    {
        return json_decode($value, true);
    }

    /**
     * KEY DETAILS
     */
    public function getKeySkillDetailsAttribute($value)
    {
        return json_decode($value, true);
    }

    /**
     * TECHNICAL SKILLS
     */
    public function getTechnicalSkillDetailsAttribute($value)
    {
        return json_decode($value, true);
    }

    /**
     * EDUCATION DETAILS
     */
    public function getEducationDetailsAttribute($value)
    {
        return json_decode($value, true);
    }


    /**
     * PROFESSIONAL EXPERIENCE
     */
    public function getExperienceDetailsAttribute($value)
    {
        return json_decode($value, true);
    }
}
