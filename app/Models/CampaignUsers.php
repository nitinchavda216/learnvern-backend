<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignUsers extends Model
{
    protected $table = 'campaign_users';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'campaign_id', 'course_user_id', 'user_id', 'course_id','created_at'];


    public function courseUserDetail()
    {
        return $this->hasOne(CourseUser::class, 'id', 'course_user_id');
    }
    public function campaignDetail()
    {
        return $this->hasOne(Campaign::class, 'id', 'campaign_id');
    }

    public function campaignUserCalling()
    {
        return $this->hasOne(CampaignUserCallings::class, 'campaign_user_id', 'id');
    }
}
