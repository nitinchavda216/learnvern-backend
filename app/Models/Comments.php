<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12-02-2021
 * Time: 05:43 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comments extends Model
{
    use SoftDeletes;

    protected $table = 'comments';

    protected $primaryKey = 'id';

    protected $fillable = ['course_id', 'user_id', 'title', 'content', 'rating', 'author', 'is_default_testimonial', 'is_active'];

    public function courseDetail()
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    public function userDetail()
    {
        return $this->hasOne(User::class, 'id', 'section_id');
    }
}
