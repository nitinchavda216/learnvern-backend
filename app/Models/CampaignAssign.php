<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignAssign extends Model
{

    protected $table = 'campaign_assign';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'campaign_id', 'assign_to', 'number_of_user','created_at','updated_at'];
}
