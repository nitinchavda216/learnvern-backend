<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Contracts\Activity;

class QuestionOptions extends Model
{
    use LogsActivity;

    protected static $logName = 'QuestionOptions';
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;

    protected $table = 'quiz_question_options';

    protected $primaryKey = 'id';

    protected $fillable = ['quiz_id', 'question_id', 'option_id', 'content', 'is_active'];

    public function scopeByActive($q)
    {
        $q->where('is_active', 1);
    }

    public function tapActivity(Activity $activity)
    {
        $admin = Auth::guard('admins')->user();
        $activity->causer_id = isset($admin) ? $admin->id : null;
    }
}
