<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignUserCallings extends Model
{
    protected $table = 'campaign_users_calling';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'campaign_id', 'campaign_user_id', 'calling_action','assign_to','user_id','course_id','parent_id','action_taken_by','followup_date','notes','created_at', 'updated_at'];

    public function campaignDetail()
    {
        return $this->hasOne(Campaign::class, 'id', 'campaign_id');
    }
}
