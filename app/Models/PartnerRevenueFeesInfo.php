<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerRevenueFeesInfo extends Model
{
    protected $table = 'partner_revenue_fees_info';

    protected $fillable = ['partner_id', 'revenue_fees_type', 'from_value', 'to_value', 'revenue_share_value', 'revenue_share_type'];

}
