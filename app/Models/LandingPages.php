<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Contracts\Activity;

class LandingPages extends Model
{
    use LogsActivity;

    protected static $logName = 'LandingPages';
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;

    protected $table = 'landing_pages';

    protected $primaryKey = 'id';

    protected $fillable = ['course_id', 'title', 'slug', 'video_url', 'video_embed_url', 'description', 'what_you_learn',
        'what_you_get'];

    public function tapActivity(Activity $activity)
    {
        $admin = Auth::guard('admins')->user();
        $activity->causer_id = isset($admin) ? $admin->id : null;
    }

    public function courseDetail(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

}
