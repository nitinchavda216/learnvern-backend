<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersToken extends Model
{
    use HasFactory;

    protected $table = 'users_token';

    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'token'];
}
