<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailTemplates extends Model
{
    use HasFactory;
    const ATTACHMENT_URL = "storage/email_attachment/";
    const ATTACHMENT_PATH = "app/public/email_attachment/";

    const EMAIL_ACTION = [
        'reset_password' => 'Reset Password',
        'activation_link' => 'Activation Link',
        'admin_set_password_mail' => 'Admin Set Password Mail',
        'payment_conformation_mail' => 'Payment Conformation Mail',
        'contact_form_submission' => 'Contact Form Submission',
        'ambassador_process_email' => 'Ambassador Process Email',
        'assign_ambassador_badge' => 'Assign Ambassador Email',
        'payment_cancelled_alert' => 'Payment Cancelled Alert',
        'new_course_create_mail'=>'New Course Create Mail',
        'partner_request_submitted' => 'Partner Request Submitted',
        'partner_request_approved' => 'Partner Request Approved',
        'partner_request_rejected' => 'Partner Request Rejected',
        'partner_configuration_completed' => 'Partner Configuration Completed',
        'partner_payment_successful' => 'Partner Payment Successful',
        'partner_payment_successful_to_partner' => 'Partner Payment Successful to Partner',
        'exit_popup' => 'Admin Exit Popup Email',
        'daily_statistics' => 'Send Daily Statistics Email',
        'update_course_slug_mail' => 'Update Course Slug Mail',
        'update_unit_slug_mail' => 'Update Unit Slug Mail',
    ];
    protected $table = 'email_templates';

    protected $primaryKey = 'id';

    protected $fillable = ['partner_id', 'identifier', 'title', 'subject', 'content', 'attachment', 'is_active', 'created_at', 'updated_at'];

    public function scopeByPartnerId($query, $partner_id)
    {
        return $query->where('partner_id', $partner_id);
    }
}
