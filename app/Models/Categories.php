<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Contracts\Activity;

class Categories extends Model
{
    use LogsActivity;

    protected static $logName = 'Categories';
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;

    protected $table = 'categories';

    protected $primaryKey = 'id';

    const IMG_URL = "storage/category/";
    const IMG_PATH = "app/public/category/";

    const CAT_IMG_URL = "storage/category/images/";
    const CAT_IMG_PATH = "app/public/category/images/";

    protected $fillable = ['name', 'is_active', 'is_webinar_category', 'sort_order', 'image', 'category_icon', 'created_at', 'updated_at'];

    public function tapActivity(Activity $activity)
    {
        $admin = Auth::guard('admins')->user();
        $activity->causer_id = isset($admin) ? $admin->id : null;
    }

    public function scopeByActive($q)
    {
        $q->where('is_active', 1);
    }

    public function getCategoryIconAttribute($value)
    {
        return isset($value) ? showCategoryImageIcon($value) : "";
    }

    public function getImageAttribute($value)
    {
        return isset($value) ? showCategoryImage($value) : "";
    }


    public function courses()
    {
        return $this->hasMany(Course::class, 'category_id', 'id');
    }

    public function courseDetail()
    {
        return $this->hasMany(Course::class, 'category_id', 'id')->orderBy('sort_order');
    }

}
