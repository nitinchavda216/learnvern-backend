<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseUser extends Model
{
    use SoftDeletes;

    protected $table = 'course_user';

    protected $primaryKey = 'id';

    protected $fillable = ['course_id', 'partner_id', 'user_id', 'certificate_id', 'paid_status', 'is_from_nsdc', 'is_international_certificate',
        'nsdc_course_id', 'user_status_id', 'is_applicable_for_free_certificate', 'payment_completed_date',
        'progress', 'course_enroll_date', 'certificate_date', 'last_used_date', 'start_date', 'end_date', 'enroll_source'];


    const PAYMENT_METHOD_TYPE = [
        3 => "Payu Link",
        4 => "Google Pay",
        5 => "Phone Pay",
        6 => "Cheque",
        7 => "Cash",
    ];

    public function userDetail()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function paymentDetail()
    {
        return $this->hasOne(PaymentTransaction::class, 'course_id', 'course_id')->where('user_id', $this->user_id);
    }

    public function courseDetail()
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    public function userStatus()
    {
        return $this->hasOne(CourseUserStatus::class, "id", 'user_status_id');
    }
    public function partnerCourses()
    {
        return $this->hasMany(PartnerCourses::class, "course_id", 'course_id');
    }

    public function customPaymentRequest()
    {
        return $this->hasOne(CustomPaymentRequests::class, "course_user_id", 'id');
    }
}
