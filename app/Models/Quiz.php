<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Contracts\Activity;

class Quiz extends Model
{
    use LogsActivity;

    protected static $logName = 'Quiz';
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;

    protected $table = 'quiz';

    protected $primaryKey = 'id';

    protected $fillable = ['course_id', 'section_id', 'name','slug', 'sub_title', 'content', 'short_content', 'time', 'evaluate',
        'retake', 'is_master_quiz', 'is_active'];

    public function scopeByActive($q)
    {
        $q->where('is_active', 1);
    }

    public function courseDetail(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    public function sectionDetail(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Section::class, 'id', 'section_id');
    }

    public function QuizUser(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(QuizUser::class, 'id', 'quiz_id');
    }

    public function tapActivity(Activity $activity)
    {
        $admin = Auth::guard('admins')->user();
        $activity->causer_id = isset($admin) ? $admin->id : null;
    }
}
