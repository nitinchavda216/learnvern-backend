<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

class SiteConfiguration extends Model
{
    use LogsActivity;

    protected static $logName = 'SiteConfiguration';
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;

    protected $table = 'site_configuration';

    protected $fillable = ['partner_id', 'config_group_id', 'configuration_title', 'configuration_value', 'identifier', 'control_type', 'is_replicable_for_partner'];

    public function tapActivity(Activity $activity)
    {
        $admin = Auth::guard('admins')->user();
        $activity->causer_id = isset($admin) ? $admin->id : null;
    }
    /* SCOPE */
    public function scopeByIdentifier($query, $identifier)
    {
        return $query->where('identifier', $identifier);
    }
}
