<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class MissingSearchKeyword extends Model {

    use HasFactory;

    protected $table = 'missing_search_keyword';
    protected $connection = 'mongodb';
    protected $collection = 'missing_search_keyword';
    protected $fillable = ['keyword'];
    
    public static function deleteMissingKeywordWithHistory($id){
        MissingKeywordHistory::where('keyword_id', $id)->delete();
        MissingSearchKeyword::where('_id', $id)->delete();
        return true;
    }

}
