<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 16-02-2021
 * Time: 04:01 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CourseUserStatus extends Model
{
    protected $table = 'course_user_status';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'is_active', 'created_at', 'updated_at'];

}
