<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12-02-2021
 * Time: 05:43 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Curriculum extends Model
{
    use SoftDeletes;
    const TYPES = [1 => 'section', 2 => 'unit', 3 => 'quiz', 4 => 'assignment'];

    protected $table = 'curriculum';

    protected $primaryKey = 'id';

    protected $fillable = ['course_id','section_id','curriculum_list_id','name','type', 'sort_order', 'parent_section_id', 'is_active', 'hide_from_backend'];

    public function scopeByActive($q)
    {
        $q->where('is_active', 1);
    }

    public function courseDetail(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    public function getSectionChildData(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Curriculum::class, 'section_id', 'section_id')->whereIn('type', ['unit', 'quiz', 'assignment'])->where('is_active', 1)->orderBy('sort_order');
    }

    public function getMasterSectionChildData(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Curriculum::class, 'parent_section_id', 'section_id')->where('type', 'section')->where('is_active', 1)->orderBy('sort_order');
    }

    public function getParentSectionData(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Curriculum::class, 'curriculum_list_id', 'parent_section_id')->where('type', 'section');
    }

    public function sectionDetail(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Section::class, 'id', 'section_id');
    }

    public function unitDetail(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Units::class, 'id', 'curriculum_list_id');
    }

    public function assignmentDetail(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Assignment::class, 'id', 'curriculum_list_id');
    }

    public function quizDetail(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Quiz::class, 'id', 'curriculum_list_id');
    }

    public function relatedUnitAttachments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CourseAttachment::class, 'curriculum_id', 'curriculum_list_id')->where('curriculum_type', 'unit');
    }

    public function relatedAssignmentAttachments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CourseAttachment::class, 'curriculum_id', 'curriculum_list_id')->where('curriculum_type', 'assignment');
    }
}
