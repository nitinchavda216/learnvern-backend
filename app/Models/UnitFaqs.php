<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12-02-2021
 * Time: 05:43 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitFaqs extends Model
{
    use SoftDeletes;
    protected $table = 'unit_faqs';

    protected $primaryKey = 'id';

    protected $fillable = ['unit_id','content','answer','is_active'];

    public function scopeByActive($q)
    {
        $q->where('is_active', 1);
    }
}
