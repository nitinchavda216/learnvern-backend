<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Contracts\Activity;

class LocationPages extends Model
{
    use LogsActivity;

    protected static $logName = 'LocationPages';
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;

    protected $table = 'location_pages';

    protected $primaryKey = 'id';

    protected $fillable = ['course_id', 'name', 'slug', 'content', 'course_intro_video', 'course_intro_video_embed', 'meta_title', 'meta_description', 'meta_keywords', 'schema_script', 'canonical_url', 'created_at', 'updated_at', 'deleted_at'];

    public function scopeByActive($q)
    {
        $q->where('is_active', 1);
    }

    public function tapActivity(Activity $activity)
    {
        $admin = Auth::guard('admins')->user();
        $activity->causer_id = isset($admin) ? $admin->id : null;
    }

    public function CourseDetail()
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

}
