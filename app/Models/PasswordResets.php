<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15-02-2021
 * Time: 11:10 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PasswordResets extends Model
{

    protected $fillable = ['email', 'token', 'created_at'];

    protected $table = 'password_resets';

    public $timestamps = false;
}
