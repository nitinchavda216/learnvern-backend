<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class KeywordSearchAnalytics extends Model {

    use HasFactory;

    protected $table = 'keyword_search_analytics';
    protected $connection = 'mongodb';
    protected $collection = 'keyword_search_analytics';
    protected $fillable = ['keyword', 'num_of_hits', 'results', 'course_ids'];

    public function scopeWhereCourse($query, $search) {
        return $query->whereRaw(['$text' => ['$search' => "\"$search\""]]);
    }

}
