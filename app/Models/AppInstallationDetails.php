<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppInstallationDetails extends Model
{
    protected $table = 'app_installation_details';

    protected $primaryKey = 'id';

    protected $fillable = ['uuid', 'serial', 'manufacturer', 'version', 'platform', 'model', 'created_at', 'updated_at'];
}
