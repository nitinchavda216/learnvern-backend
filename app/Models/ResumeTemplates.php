<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResumeTemplates extends Model
{
    use SoftDeletes;
    const IMG_URL = "storage/resume_templates/";
    const IMG_PATH = "app/public/resume_templates/";

    protected $table = 'resume_templates';

    protected $primaryKey = 'id';

    protected $fillable = ['title', 'image'];

}
