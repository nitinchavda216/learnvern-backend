<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Colleges extends Model
{
    protected $table = 'colleges';
    protected $primaryKey = 'id';
    protected $fillable = ['title', 'state', 'district', 'city','created_at', 'updated_at'];
}
