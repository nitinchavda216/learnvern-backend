<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignBlockUsers extends Model
{
    protected $table = 'campaign_block_user';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'user_id'];


}
