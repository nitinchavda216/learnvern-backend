<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17-02-2021
 * Time: 03:51 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionAnswerUser extends Model
{
    use SoftDeletes;
    protected $table = 'question_answer_user';

    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'quiz_id', 'question_id', 'user_answer_id', 'answer', 'is_active', 'view_source'];
}
