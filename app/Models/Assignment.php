<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15-02-2021
 * Time: 09:40 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $table = 'assignments';

    protected $primaryKey = 'id';


    protected $fillable = ['course_id', 'section_id', 'name', 'slug', 'content', 'video', 'video_id', 'mark',
        'video_download_url_1', 'video_download_url_2', 'video_download_url_3', 'time', 'include_course_evaluation',
        'is_free', 'is_active', 'created_at', 'updated_at', 'deleted_at'];

    public function scopeByActive($q)
    {
        $q->where('is_active', 1);
    }

    public function courseDetail()
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    public function sectionDetail()
    {
        return $this->hasOne(Section::class, 'id', 'section_id');
    }

    public function courseUserDetail()
    {
        return $this->hasOne(CourseUser::class, 'course_id','course_id');
    }

    public function relatedAssignmentUser()
    {
        return $this->hasOne(AssignmentUser::class, 'assignment_id', 'id');
    }

    public function relatedAttachments()
    {
        return $this->hasMany(CourseAttachment::class, 'curriculum_id', 'id')->where('curriculum_type', 'assignment');
    }
}
