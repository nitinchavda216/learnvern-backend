<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnersSettings extends Model
{
    protected $table = 'partners_settings';
    protected $primaryKey = 'id';

    const IMG_URL = "storage/partners_documents/";
    const IMG_PATH = "app/public/partners_documents/";

    protected $fillable = ['third_party_partner_fees', 'content_partner_fees', 'third_party_agreement_document', 'content_partner_agreement_document',
        'content_partner_revenue_details', 'third_party_enrollment_fees_share_value', 'third_party_enrollment_fees_share_type'];
}
