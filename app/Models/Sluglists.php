<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sluglists extends Model
{
    use HasFactory;

    protected $table = 'sluglists';

    protected $primaryKey = 'id';

    protected $fillable = ['course_id','curriculum_id','curriculum_type','slug'];

    public function unitDetail()
    {
        return $this->hasOne(Units::class, 'id','curriculum_id')->select('id', 'course_id', 'slug');
    }
    public function assignmentDetail()
    {
        return $this->hasOne(Assignment::class, 'id','curriculum_id')->select('id', 'course_id', 'slug');
    }
    public function quizDetail()
    {
        return $this->hasOne(Quiz::class, 'id','curriculum_id')->select('id', 'course_id', 'slug');
    }
}
