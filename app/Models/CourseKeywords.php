<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class CourseKeywords extends Model {

    use HasFactory;

    protected $table = 'courses';
    protected $connection = 'mongodb';
    protected $collection = 'courses';
    protected $fillable = [
        'course_id', 'course_name', 'unit_id', 'keywords'
    ];
    
    const COURSES_JSON_FILE = 'courses_keywords.json';

}
