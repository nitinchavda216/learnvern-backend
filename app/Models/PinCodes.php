<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PinCodes extends Model
{
    protected $table = 'pincodes';

    protected $primaryKey = 'id';

    protected $fillable = ['pincode', 'division_name', 'region_name', 'circle_name', 'taluk', 'district_name', 'state_name'];

}
