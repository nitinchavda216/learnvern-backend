<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'state_id', 'name'];

    public function stateDetail()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }
}
