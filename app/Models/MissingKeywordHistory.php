<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MissingKeywordHistory extends Model {

    use HasFactory;
    
    protected $table = 'missing_keyword_history';
    protected $connection = 'mongodb';
    protected $collection = 'missing_keyword_history';
    protected $fillable = ['keyword_id', 'user_id', 'ip', 'country', 'state', 'city', 'pin_code', 'isd_code', 'city_id', 'state_id', 'country_id', 'attempts'];
    protected $primaryKey = '_id';

    public function userDetails() {
        return $this->belongsTo(User::class, 'user_id', 'id')->select('id', 'first_name', 'last_name', 'email', 'mobile_number', 'country', 'state', 'city', 'pin_code', 'isd_code');
    }

}
