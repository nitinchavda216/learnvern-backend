<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseBadge extends Model
{
    protected $table = 'course_badges';

    protected $primaryKey = 'id';

    protected $fillable = ['title'];
}
