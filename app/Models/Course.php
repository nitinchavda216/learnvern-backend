<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Contracts\Activity;

class Course extends Model
{
    use LogsActivity;

    protected static $logName = 'Course';
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;


    const COURSE_TYPES = [
        0 => "Free course with paid certificate",
        1 => "Paid Live Course",
        2 => "Free Live Course",
        3 => "Free course",
        4 => "Free course with free certificate",
        5 => "Paid course with paid certificate",
        6 => "Paid course with no certificate",
    ];

    const ICON_IMG_URL = "storage/course/icons/";
    const ICON_IMG_PATH = "app/public/course/icons/";

    const IMG_URL = "storage/course/";
    const IMG_PATH = "app/public/course/";

    const APP_IMG_URL = "storage/course/app_image/";
    const APP_IMG_PATH = "app/public/course/app_image/";

    const APP_ICON_URL = "storage/course/app_icon/";
    const APP_ICON_PATH = "app/public/course/app_icon/";


    protected $table = 'courses';

    protected $primaryKey = 'id';

    protected $fillable = ['category_id', 'badge_id', 'name', 'slug', 'course_certificate_name', 'course_language', 'price', 'content', 'who_can_take', 'pre_requisites',
        'average_rating', 'intro_video', 'intro_video_embed', 'intro_youtube_video_id', 'intro_youtube_video', 'intro_youtube_video_embed',
        'time', 'num_of_student', 'course_retake', 'what_you_learn', 'why_you_learn', 'how_can_learnvern_help', 'faq',
        'is_free', 'course_type', 'badge', 'unit_youtube_video_flag', 'start_date', 'complete_date', 'pre_course', 'related_course',
        'comment_count', 'is_active', 'meta_title', 'meta_description', 'meta_keywords', 'schema_script', 'canonical_url', 'course_icon',
        'brief_description', 'tiny_description', 'image', 'app_image', 'app_icon', 'color_code', 'app_dynamic_link','certificate_achievement_title', 'is_career_plan_course'];

    public function tapActivity(Activity $activity)
    {
        $admin = Auth::guard('admins')->user();
        $activity->causer_id = isset($admin) ? $admin->id : null;
    }

    public function getImageAttribute($value)
    {
        return isset($value) ? showCourseImage($value) : "";
    }

    public function getCourseIconAttribute($value)
    {
        return isset($value) ? showCourseIconImage($value) : "";
    }

    public function getAppIconAttribute($value)
    {
        return isset($value) ? showCourseAppIcon($value) : "";
    }

    public function getAppImageAttribute($value)
    {
        return isset($value) ? showCourseAppImage($value) : "";
    }

    public function scopeByActive($q)
    {
        $q->where('is_active', 1);
    }

    public function getPreCourseAttribute($value)
    {
        return explode(',', $value);
    }

    public function getRelatedCourseAttribute($value)
    {
        return explode(',', $value);
    }

    public function categoryDetail(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Categories::class, 'id', 'category_id');
    }

    public function relatedLandingPage(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(LandingPages::class, "course_id", 'id');
    }

    public function partnerCourses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PartnerCourses::class, "course_id", 'id');
    }

    public function relatedSection(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Curriculum::class, "course_id", 'id')->where('type', 'section')->where('is_active', 1);
    }

    public function relatedUnitCurriculum(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Curriculum::class, 'course_id', 'id')->where('type', 'unit')->where('is_active', 1);
    }

    public function relatedAssignmentCurriculum(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Curriculum::class, 'course_id', 'id')->where('type', 'assignment')->where('is_active', 1);
    }

    public function relatedQuizCurriculum(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Curriculum::class, 'course_id', 'id')->where('type', 'quiz')->where('is_active', 1);
    }

    public function relatedComments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Comments::class, 'course_id', 'id');
    }

    public function relatedFaqs(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Faqs::class, 'course_id', 'id')->where('is_active', 1);
    }
}
