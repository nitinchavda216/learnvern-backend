<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Partners extends Model
{
    protected $table = 'partners';

    protected $primaryKey = 'id';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    const TYPES = [
        1 => 'Third Party Partner',
        2 => 'Content Partner',
    ];

    protected $fillable = ['name', 'description', 'partner_type', 'website', 'logo_image', 'about_information',
        'is_master_franchisee', 'parent_franchisee_id', 'current_students_count', 'contact_person',
        'contact_person_email', 'contact_person_phone_number', 'address_line1', 'address_line2',
        'city', 'state', 'country', 'pin_code', 'branch_address_line1',
        'branch_address_line2', 'branch_city', 'branch_state', 'branch_country', 'branch_pin_code',
        'setup_fees', 'revenue_sharing_type',
        'use_own_certificate', 'certificate_image', 'partner_application_status', 'allow_course_enrollment',
        'allow_all_courses', 'allow_all_future_courses',
        'redirect_for_enrollment', 'show_webinars', 'mail_method', 'mail_host', 'mail_port', 'mail_username',
        'mail_password', 'mail_encryption', 'mail_from_email_address', 'mail_from_name', 'request_id',
        'default_admin_password', 'approve_website', 'setup_complete', 'is_other_payment_gateway'];

}
