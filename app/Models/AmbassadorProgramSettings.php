<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15-02-2021
 * Time: 09:40 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AmbassadorProgramSettings extends Model
{
    use SoftDeletes;

    const IMG_URL = "storage/ambassador_program_badge/";
    const IMG_PATH = "app/public/ambassador_program_badge/";

    protected $table = 'ambassador_program_settings';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'referrals_count', 'refer_from', 'title', 'content', 'app_content', 'image', 'is_active'];
}
