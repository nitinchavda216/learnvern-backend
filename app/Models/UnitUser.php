<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitUser extends Model
{
    use SoftDeletes;

    protected $table = 'unit_user';

    protected $primaryKey = 'id';

    protected $fillable = ['unit_id', 'user_id', 'video_progress', 'is_completed', 'is_manually_mark_completed', 'view_source', 'time', 'last_used_date', 'next_curriculum_id', 'next_curriculum_type'];

    public function scopeByActive($q)
    {
        $q->where('is_active', 1);
    }

    public function unitDetail()
    {
        return $this->hasOne(Units::class, 'id', 'unit_id');
    }

}
