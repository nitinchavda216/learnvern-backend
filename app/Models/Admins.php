<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admins extends Authenticatable
{
    use SoftDeletes;
    protected $table = 'admins';

    protected $primaryKey = 'id';

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $fillable = ['partner_id', 'name', 'email', 'password', 'status','role_id', 'activation_date', 'activation_token', 'remember_token',
        'created_at','updated_at','deleted_at'];

    protected $dates = ['activation_date'];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }
    public function relatedRole()
    {
        return $this->hasOne(AdminRole::class, "id", 'role_id');
    }
    public function partnerDetail()
    {
        return $this->hasOne(Partners::class, "id", 'partner_id');
    }
}
