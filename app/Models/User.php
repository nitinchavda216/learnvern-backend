<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\File;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'users';

    const IMG_URL = "storage/users/";
    const IMG_PATH = "app/public/users";

    const SIGNUP_FROM = [
        1 => 'facebook_web',
        2 => 'google+_web',
        3 => 'manual_signup_web',
        4 => 'manual_signup_android',
        5 => 'facebook_android',
        6 => 'google+_android',
        7 => 'manual_signup_iphone',
        8 => 'facebook_iphone',
        9 => 'google+_iphone',
        10 => 'NSDC_web',
        11 => 'LP_manual',
        12 => 'LP_google',
        13 => 'apple_id_android',
        14 => 'apple_id_iphone',
    ];
    const NSDC_ID_PROOF = [
        1 => 'Aadhar ID',
        2 => 'PAN Card',
        3 => 'Voter ID Card',
        4 => 'Driving License',
        5 => 'Passport',
    ];
    const COLUMN_LIST_NAME = [
        0 => "User Email",
        1 => "Full Name",
        2 => "Dial Code",
        3 => "Mobile No",
        4 => "Current Occupation",
        5 => "Sign Up From",
        6 => "Sign Up Referral",
        7 => "Register Date",
        8 => "Last Login Date",
        9 => "Enrolled Courses",
        10 => "Completed Courses",
        11 => "Total Refer",
        12 => "Paid Certificate",
        13 => "Free Certificate",
    ];
    const NSDC_EDUCATIONS = [
        1 => 'Basic Literacy/Below 5th',
        2 => '5th to 8th',
        3 => '9th to 10th',
        4 => '11th to 12th',
        5 => 'ITI',
        6 => 'Polytechnic',
        7 => 'Diploma',
        8 => 'Under Graduate',
        9 => 'Graduate',
        10 => 'Post Graduate',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['partner_id', 'resume_template_id', 'first_name', 'last_name', 'certificate_name', 'username', 'email', 'password', 'ci_password', 'gender',
        'isd_code', 'mobile_number', 'country_id', 'state_id', 'city_id', 'country', 'state', 'city', 'pin_code', 'salutation', 'birth_date',
        'address', 'father_name', 'cast_category', 'area_of_interest', 'current_occupation', 'id_proof', 'id_number', 'education',
        'candidate_id', 'referral', 'domain_signup', 'signup_from', 'is_social_signup', 'social_id', 'image', 'user_type_id', 'own_referral_code',
        'email_unsubscribe', 'ambassador_email_send_flag', 'ambassador_50_email_send_flag', 'is_active', 'activation_key', 'forgot_password_key',
        'device_token', 'last_login_ip', 'last_login_date', 'is_login', 'send_activation_email_date', 'login_count', 'is_from_nsdc', 'nsdc_user_id'
        , 'district', 'marital_status', 'mother_name', 'religion', 'disability', 'disability_type', 'previous_experience_sector', 'no_of_months_of_experience',
        'is_employed', 'employment_status', 'employment_details', 'heard_about_us', 'referred_count', 'nsdc_referred_count', 'is_complete_profile', 'ambassador_badge_id',
        'college_id', 'other_college_name', 'is_mobile_verify'];

    protected $appends = ['full_name'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = [];

    /**
     * Mutators
     */
    public function getImageAttribute($value)
    {
        if(is_null($value))
            return "";
        $fileExists = File::exists('storage/users/' . $value);
        return $fileExists ? asset("storage/users/$value") : "";
    }


    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    // Relationship
    public function myReferrals()
    {
        return $this->hasMany(User::class, "referral", 'own_referral_code');
    }

    public function myReferralsCount()
    {
        return $this->hasMany(User::class, "referral", 'own_referral_code')->count();
    }

    public function affiliate()
    {
        return $this->hasOne(User::class, "own_referral_code", 'referral');
    }

    public function userBadges()
    {
        return $this->hasMany(UserAmbassadorBadges::class, "user_id", 'id')->orderBy('id', 'DESC');
    }

    public function resumeDetails()
    {
        return $this->hasOne(UserResumeDetails::class, "user_id", 'id');
    }

    public function userNotifications()
    {
        return $this->hasMany(Notification::class, "user_id", 'id');
    }

    public function courses()
    {
        return $this->hasMany(CourseUser::class, "user_id", 'id')->with('courseDetail')->whereHas('courseDetail');
    }

    public function collegeDetail()
    {
        return $this->hasOne(Colleges::class, 'id', 'college_id');
    }

    public function quizUsers()
    {
        return $this->hasMany(QuizUser::class, "user_id", 'id');
    }

    public function courseStatus()
    {
        return $this->hasMany(CourseUserStatus::class, "user_id", 'id');
    }

    public function ambassadorBadgeDetail()
    {
        return $this->hasOne(AmbassadorProgramSettings::class, "id", 'ambassador_badge_id');
    }

    public function relatedPaymentTransaction(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PaymentTransaction::class, "user_id", 'id');
    }

    public function relatedAmbassadorBadge()
    {
        return $this->hasOne(AmbassadorProgramSettings::class, 'id', 'ambassador_badge_id');
    }
}
