<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15-02-2021
 * Time: 09:40 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApiLogs extends Model
{
    use SoftDeletes;
    protected $table = 'api_logs';

    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'course_id', 'api_name', 'parameters', 'response'];

}
