<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    const IMG_URL = "storage/notifications/";
    const IMG_PATH = "app/public/notifications/";

    protected $table = 'notifications';

    protected $fillable = ['user_id', 'notification_type_id', 'notification_type', 'title', 'description', 'link',
        'image', 'is_read'];
}
