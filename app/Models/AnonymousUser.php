<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnonymousUser extends Model
{
    use HasFactory;

    protected $table = 'anonymous_user';

    protected $primaryKey = 'id';

    protected $fillable = ['user_ip', 'phone_number', 'visited_page', 'city', 'state','country', 'user_id', 'created_at'];
}
