<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReferralVisitorsTracking extends Model
{
    protected $table = 'referral_visitors_tracking';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'referral_code', 'user_id', 'referral_link_visits', 'referral_registrations', 'tracking_date', 'last_tracked_time'];
    protected $dates = ['created_at', 'updated_at'];


}
