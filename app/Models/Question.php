<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Contracts\Activity;

class Question extends Model
{

    use LogsActivity;

    protected static $logName = 'Question';
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;

    const TYPES = ['single' => "Single"];
    protected $table = 'quiz_questions';

    protected $primaryKey = 'id';

    protected $fillable = ['quiz_id', 'title', 'content', 'answer', 'type', 'mark', 'is_active'];

    public function scopeByActive($q)
    {
        $q->where('is_active', 1);
    }

    public function tapActivity(Activity $activity)
    {
        $admin = Auth::guard('admins')->user();
        $activity->causer_id = isset($admin) ? $admin->id : null;
    }

    public function quizDetail()
    {
        return $this->hasOne(Quiz::class, 'id', 'quiz_id');
    }

    public function relatedOptions()
    {
        return $this->hasMany(QuestionOptions::class, 'question_id', 'id');
    }

    public function relatedQuestionAnswerUser()
    {
        return $this->hasMany(QuestionAnswerUser::class, 'question_id', 'id');
    }
}
