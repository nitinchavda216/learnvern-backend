<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignmentUser extends Model
{
    use SoftDeletes;

    protected $table = 'assignment_user';

    protected $primaryKey = 'id';

    protected $fillable = ['assignment_id', 'user_id', 'video_progress', 'is_completed', 'is_manually_mark_completed', 'view_source', 'last_used_date'
        , 'complete_time', 'created_at', 'updated_at', 'deleted_at'];
}
