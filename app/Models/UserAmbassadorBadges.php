<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15-02-2021
 * Time: 09:40 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAmbassadorBadges extends Model
{
    use SoftDeletes;

    protected $table = 'user_ambassador_badges';

    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'ambassador_badge_id'];


    public function ambassadorBadge()
    {
        return $this->hasOne(AmbassadorProgramSettings::class, 'id', 'ambassador_badge_id');
    }
    public function userDetails()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
