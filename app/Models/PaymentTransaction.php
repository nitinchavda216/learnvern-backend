<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentTransaction extends Model
{
    protected $table = 'payment_transactions';

    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'course_id', 'partner_id', 'course_user_id', 'amount', 'transaction_id', 'mihpayid', 'transaction_date',
    'payment_source', 'payment_method', 'payment_type'];


    const PAYMENT_METHOD_TYPE =[
        1 =>"System Web",
        2 =>"System App",
        3 => "Payu Link",
        4 => "Google Pay",
        5 => "Phone Pay",
        6 => "Cheque",
        7 => "Cash",
        10 => "Razorpay",
        11 => "App Razorpay",
    ];

    public function userDetail()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id')->withTrashed();
    }

    public function courseDetail()
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    public function courseUserDetails()
    {
        return $this->hasOne(CourseUser::class, 'id', 'course_user_id');
    }
}
