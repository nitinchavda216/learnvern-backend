<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12-02-2021
 * Time: 09:42 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Section extends Model
{
    use SoftDeletes;
    protected $table = 'sections';

    protected $primaryKey = 'id';

    protected $fillable = ['course_id','name','is_active','created_at', 'updated_at', 'deleted_at'];

    //Relationships
    public function courseDetail()
    {
        return $this->hasOne(Course::class, "id", 'course_id');
    }

    public function relatedCurriculum()
    {
        return $this->hasMany(Curriculum::class, 'section_id', 'id');
    }

    public function relatedUnits()
    {
        return $this->hasMany(Units::class, 'section_id', 'id');
    }

    public function relatedAssignments()
    {
        return $this->hasMany(Assignment::class, 'section_id', 'id');
    }

    public function relatedQuiz()
    {
        return $this->hasMany(Quiz::class, 'section_id', 'id');
    }
}





