<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoginHistory extends Model
{
    protected $table = 'login_history';

    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'ip', 'hostname', 'country_code', 'country_name', 'browser', 'browser_details', 'ip_details', 'login_from'];

}
