<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17-02-2021
 * Time: 03:51 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Contracts\Activity;

class QuizUser extends Model
{
    use SoftDeletes;

    protected $table = 'quiz_user';

    protected $primaryKey = 'id';

    protected $fillable = ['quiz_id', 'user_id', 'user_status', 'marks', 'is_completed', 'view_source'];

    //Relationship
    public function coursesUser(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CourseUser::class, "user_id", 'id');
    }

    public function quizDetails(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Quiz::class, "id", 'quiz_id');
    }

    public function curriculumDetails(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Curriculum::class, "curriculum_list_id", 'quiz_id');
    }

    public function relatedQuizQuestions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Question::class, 'quiz_id', 'quiz_id');
    }

}
