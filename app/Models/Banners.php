<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banners extends Model
{
    use HasFactory;

    const IMG_URL = "storage/banners/";
    const IMG_PATH = "app/public/banners/";

    protected $table = 'banners';

    protected $primaryKey = 'id';

    protected $fillable = ['title', 'image', 'user_auth_type','type','link_type','link','link_course_id'];

    const TYPE_BANNER = [
        "referral_banner" => "Global Referral Banner",
        "nsdc_banner" => "Global NSDC Banner",
        "promotion_banner" => "Promotion Banner",
        "referral_info_banner" => "Referral Info Banner",
    ];
    const LINK_TYPES = [
        "course_page" => "Course/Webinar Page Link",
        "referral_page" => "Referral Page Link",
        "my_courses_page" => "My Courses Page",
        "my_downloads_page" => "My Downloads Page",
    ];
    const USER_AUTH_TYPES = [
        "any" => "Any",
        "with_login" => "With Login",
        "without_login" => "Without Login",
    ];
}
