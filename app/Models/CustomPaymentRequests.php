<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomPaymentRequests extends Model
{
    use SoftDeletes;

    protected $table = 'custom_payment_requests';

    protected $fillable = ['user_id', 'course_id', 'course_user_id', 'razorpay_invoice_id', 'sent_request_content', 'status'];

    public function setSentRequestContentAttribute($value)
    {
        $this->attributes['sent_request_content'] = json_encode($value);
    }

    public function getSentRequestContentAttribute($value)
    {
        return json_decode($value, true);
    }

    public function userDetail()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function courseDetail()
    {
        return $this->hasOne(Course::class, 'id', 'course_id')->select('id', 'name', 'slug', 'course_type');
    }
}
