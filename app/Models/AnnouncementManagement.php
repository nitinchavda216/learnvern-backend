<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnnouncementManagement extends Model
{
    use HasFactory;
    protected $table = 'announcement_managment';

    protected $primaryKey = 'id';

    protected $fillable = ['title', 'description', 'start_date', 'end_date', 'is_title_display','is_active'];
}
