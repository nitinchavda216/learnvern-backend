<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AbandonedPaymentReports extends Model
{
    protected $table = 'abandoned_payment_reports';

    protected $primaryKey = 'id';

    public $progress = '';

    protected $fillable = ['user_id', 'course_id', 'trans_id', 'amount', 'created_at', 'updated_at','progress'];

    public function userDetail()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }


    public function courseUserDetail()
    {
        return $this->hasMany(CourseUser::class, 'user_id','user_id');
    }

    public function courseDetail()
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }
}
