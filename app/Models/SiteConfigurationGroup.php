<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiteConfigurationGroup extends Model
{

    protected $table = 'site_configuration_groups';

    protected $fillable = ['id', 'title'];

    /* RELATIONSHIPS */
    public function siteConfigurations()
    {
        return $this->hasMany('App\Models\SiteConfiguration', 'config_group_id', 'id');
    }
}
