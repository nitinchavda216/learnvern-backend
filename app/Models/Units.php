<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12-02-2021
 * Time: 05:43 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

class Units extends Model
{
    use LogsActivity;

    protected static $logName = 'Units';
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;

    use SoftDeletes;
    protected $table = 'units';

    const IMG_URL = "storage/unit/";
    const IMG_PATH = "app/public/unit/";

    protected $primaryKey = 'id';

    protected $fillable = ['course_id', 'section_id', 'name', 'slug', 'content', 'is_global_unit', 'global_unit_conditions',
        'short_content', 'time', 'is_free', 'video_id', 'video_url', 'video_download_url_1', 'video_download_url_2',
        'video_download_url_3', 'youtube_video_id', 'youtube_video_url', 'video_embed_url', 'meta_title', 'meta_description',
        'meta_keywords', 'schema_script', 'canonical_url', 'attachment', 'is_active', 'created_at', 'updated_at', 'deleted_at'];

    public function tapActivity(Activity $activity)
    {
        $admin = Auth::guard('admins')->user();
        $activity->causer_id = isset($admin) ? $admin->id : null;
    }

    public function scopeByActive($q)
    {
        $q->where('is_active', 1);
    }

    public function setGlobalUnitConditionsAttribute($value)
    {
        $this->attributes['global_unit_conditions'] = json_encode($value);
    }

    public function getGlobalUnitConditionsAttribute($value)
    {
        return json_decode($value, true);
    }

    public function courseDetail()
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    public function sectionDetail()
    {
        return $this->hasOne(Section::class, 'id', 'section_id');
    }

    public function courseUserDetail()
    {
        return $this->hasOne(CourseUser::class, 'course_id','course_id');
    }

    public function relatedUnitUser()
    {
        return $this->hasOne(UnitUser::class, 'unit_id', 'id');
    }

    public function relatedFaqTitles()
    {
        return $this->hasMany(UnitFaqs::class, 'unit_id', 'id');
    }

    public function relatedAttachments()
    {
        return $this->hasMany(CourseAttachment::class, 'curriculum_id', 'id')->where('curriculum_type', 'unit');
    }
}
