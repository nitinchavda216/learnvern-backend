<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerCourses extends Model
{
    public $timestamps = false;
    protected $table = 'partner_courses';
    protected $primaryKey = 'id';

    protected $fillable = ['partner_id', 'course_id', 'is_paid_course', 'course_fees'];
}
