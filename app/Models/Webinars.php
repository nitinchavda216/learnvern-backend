<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Webinars extends Model
{

    const IMG_URL = "storage/course/";
    const IMG_PATH = "app/public/course/";

    protected $table = 'webinars';

    protected $primaryKey = 'id';

    protected $fillable = [ 'name', 'slug', 'course_language', 'content',
        'average_rating', 'intro_video', 'intro_video_embed', 'intro_youtube_video_id', 'intro_youtube_video', 'intro_youtube_video_embed',
        'time', 'faq', 'image',
        'is_free', 'unit_youtube_video_flag', 'start_date', 'complete_date',
        'comment_count', 'is_active', 'num_of_student', 'meta_title', 'meta_description', 'meta_keywords', 'schema_script', 'canonical_url'];

    public function scopeByActive($q)
    {
        $q->where('is_active', 1);
    }


}
