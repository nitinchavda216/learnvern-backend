<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseLanguage extends Model
{
    use HasFactory;

    protected $table = 'course_languages';

    protected $primaryKey = 'id';

    protected $fillable=['course_id','language_id'];

    public function Languages()
    {
        return $this->belongsToMany(Language::class);
    }

    public function Courses()
    {
        return $this->belongsTo(Course::class);
    }
}
