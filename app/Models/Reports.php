<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reports extends Model
{
    protected $table = 'reports';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'name', 'fields', 'conditions', 'sql_query', 'created_at', 'updated_at', 'deleted_at'];
    protected $dates = ['created_at', 'updated_at'];

    const TABLE_LIST_NAME = [
        "course_user" => [
            "join" => "inner",
            "table" => "course_user",
            "joinable" => array("course_user_id"=>"id","course_id" => "course_id", "user_id" => "user_id"),
            "prereq" => array("courses","users")
        ],

        "course_progress_log" => [
            "join" => "left",
            "table" => "(SELECT course_user_id,MAX(created_at) AS last_date_of_progress FROM course_progress_log_by_day GROUP BY course_user_id)",
            "joinable" => array("course_user_id" => "course_user_id"),
            "prereq" => array("course_user")
        ],

        "users" => array(
            "table" => "users",
            "join" => "left",
            "joinable" => array("user_id" => "id"),
            "prereq" => array("course_user")
        ),
        "courses" => array(
            "table" => "courses",
            "join" => "left",
            "joinable" => array("course_id" => "id","category_id" => "category_id"),
            "prereq" => array("course_user")
        ),
        "categories" => array(
            "table" => "categories",
            "join" => "left",
            "joinable" => array("category_id" => "id"),
            "prereq" => array("courses")
        ),
        "colleges" => array(
            "table" => "(SELECT user_id,GROUP_CONCAT(DISTINCT(name)) as college_name FROM user_college_details GROUP BY user_id)",
            "join" => "left",
            "joinable" => array("user_id" => "user_id"),
            "prereq" => array("users")
        ),
        "key_skills" => array(
            "table" => "(SELECT user_id,GROUP_CONCAT(DISTINCT(title)) as key_skill_names FROM user_key_skills GROUP BY user_id)",
            "join" => "left",
            "joinable" => array("user_id" => "user_id"),
            "prereq" => array("users")
        ),
        "technical_skills" => array(
            "table" => "(SELECT user_id,GROUP_CONCAT(DISTINCT(title)) as technical_key_skill_names FROM user_technical_skills GROUP BY user_id)",
            "join" => "left",
            "joinable" => array("user_id" => "user_id"),
            "prereq" => array("users")
        ),
        "user_college_details" => array(
            "table" => "user_college_details",
            "join" => "left",
            "joinable" => array("user_id" => "user_id"),
        ),
        "user_key_skills" => array(
            "table" => "user_key_skills",
            "join" => "left",
            "joinable" => array("user_id" => "user_id"),
        ),
        "user_technical_skills" => array(
            "table" => "user_technical_skills",
            "join" => "left",
            "joinable" => array("user_id" => "user_id"),
        ),
        "ambassador_badges" => array(
            "table" => "(SELECT user_ambassador_badges.user_id,GROUP_CONCAT(DISTINCT(ambassador_program_settings.title)) as badge_name,MAX(user_ambassador_badges.created_at) as last_badge_date FROM user_ambassador_badges LEFT JOIN ambassador_program_settings ON user_ambassador_badges.ambassador_badge_id = ambassador_program_settings.id  GROUP BY user_ambassador_badges.user_id)",
            "join" => "left",
            "joinable" => array("user_id" => "user_id"),
            "prereq" => array("users")
        ),
        "abandoned_payment_reports" => array(
            "table" => "abandoned_payment_reports",
            "join" => "left",
            "joinable" => array("course_id" => "course_id","user_id" => "user_id"),
            "prereq" => array("course_user")
        ),
    ];

    const COLUMN_LIST_NAME = [
//        "course_user_id" => [
//            "table" => "course_user",
//            "label" => "ID",
//            "column" => "id",
//            "display_group" => "CourseUser",
//            "group_by" => true,
//            "format" => "course_user_id_link",
//        ],
//        "course_user_count" => [
//            "table" => "course_user",
//            "label" => "Course User Count",
//            "sql_column" => "COUNT(DISTINCT(`course_user`.id))",
//            "display_group" => "CourseUser",
//            "format" => "number"
//        ],
        "course_id" => [
            "table" => "course_user",
            "label" => "Course ID",
            "column" => "course_id",
            "group_by" => true,
            "display_group" => "Course"
        ],
//        "user_id" => [
//            "table" => "course_user",
//            "label" => "User ID",
//            "column" => "user_id",
//            "group_by" => true,
//            "display_group" => "CourseUser"
//        ],
        "certificate_id"=>[
            "table" => "course_user",
            "label" => "Certificate ID",
            "column" => "certificate_id",
            "group_by" => false,
            "display_group" => "CourseUser",
        ],
//        "paid_status"=>[
//            "table" => "course_user",
//            "label" => "Paid Status",
//            "column" => "paid_status",
//            "group_by" => true,
//            "display_group" => "CourseUser",
//        ],
//        "is_from_nsdc"=>[
//            "table" => "course_user",
//            "label" => "IS From NSDC?",
//            "column" => "is_from_nsdc",
//            "group_by" => true,
//            "display_group" => "CourseUser",
//        ],
//        "user_status_id"=>[
//            "table" => "course_user",
//            "label" => "User Status",
//            "column" => "user_status_id",
//            "group_by" => true,
//            "display_group" => "CourseUser",
//        ],
        "progress"=>[
            "table" => "course_user",
            "label" => "User Progress(%)",
            "sql_column" => "ROUND(progress)",
            "group_by" => false,
            "display_group" => "CourseUser",
        ],
        "course_enroll_date" => [
            "table" => "course_user",
            "label" => "Enroll Date",
            "column" => "course_enroll_date",
            'function' => 'DATE',
            "group_by" => false,
            "display_group" => "CourseUser",
            "format" => "date"
        ],
        "certificate_date" => [
            "table" => "course_user",
            "label" => "Certificate Date",
            "column" => "certificate_date",
            'function' => 'DATE',
            "group_by" => false,
            "display_group" => "CourseUser",
            "format" => "date"
        ],
        "last_used_date" => [
            "table" => "course_user",
            "label" => "Last Used Date",
            "column" => "last_used_date",
            'function' => 'DATE',
            "group_by" => false,
            "display_group" => "CourseUser",
            "format" => "date"
        ],

        "payment_completed_date" => [
            "table" => "course_user",
            "label" => "Payment Completed Date",
            "column" => "payment_completed_date",
            'function' => 'DATE',
            "group_by" => false,
            "display_group" => "CourseUser",
            "format" => "date"
        ],


        "full_name" => [
            "table" => "users",
            "label" => "Full Name",
            "sql_column" => "users.first_name",
            "group_by" => false,
            "display_group" => "User"
        ],

        "user_name" => [
            "table" => "users",
            "label" => "User Name",
            "sql_column" => "users.username",
            "group_by" => false,
            "display_group" => "User"
        ],
        "email" => [
            "table" => "users",
            "label" => "Email",
            "column" => "email",
            "group_by" => false,
            "display_group" => "User"
        ],
        "gender" => [
            "table" => "users",
            "label" => "Gender",
            "column" => "gender",
            "group_by" => false,
            "display_group" => "User"
        ],
        "mobile_number" => [
            "table" => "users",
            "label" => "Mobile Number",
            "column" => "mobile_number",
            "group_by" => false,
            "display_group" => "User"
        ],
        "state" => [
            "table" => "users",
            "label" => "State",
            "column" => "state",
            "group_by" => false,
            "display_group" => "User"
        ],
        "city" => [
            "table" => "users",
            "label" => "City",
            "column" => "city",
            "group_by" => false,
            "display_group" => "User"
        ],
        "own_referral_code" => [
            "table" => "users",
            "label" => "Referral Code",
            "column" => "own_referral_code",
            "group_by" => false,
            "display_group" => "User"
        ],

        "created_at" => [
            "table" => "users",
            "label" => "Registration Date",
            "column" => "created_at",
            "group_by" => false,
            "display_group" => "User"
        ],
        "last_login_date" => [
            "table" => "users",
            "label" => "Last Login Date",
            "column" => "last_login_date",
            "group_by" => false,
            "display_group" => "User"
        ],
        "course_name" => [
            "table" => "courses",
            "label" => "Course Name",
            "column" => "name",
            "group_by" => false,
            "display_group" => "Course"
        ],
        "course_link" => [
            "table" => "courses",
            "label" => "Course Link",
            "column" => "slug",
            "group_by" => false,
            "display_group" => "Course"
        ],
//        "course_number_of_user_enrollment" => [
//            "table" => "courses",
//            "label" => "Total User Enroll",
//            "column" => "num_of_student",
//            "group_by" => false,
//            "display_group" => "Course"
//        ],
        "category_id" => [
            "table" => "categories",
            "label" => "Course Category ID",
            "column" => "id",
            "group_by" => false,
            "display_group" => "Course"
        ],

        "category_name" => [
            "table" => "categories",
            "label" => "Category Name",
            "column" => "name",
            "group_by" => false,
            "display_group" => "Course"
        ],

        "college_name" => [
            "table" => "colleges",
            "label" => "College Name",
            "column" => "college_name",
            "group_by" => false,
            "display_group" => "User"
        ],
        "key_skill_names" => [
            "table" => "key_skills",
            "label" => "Key Skills",
            "column" => "key_skill_names",
            "group_by" => false,
            "display_group" => "User"
        ],
        "technical_key_skill_names" => [
            "table" => "technical_skills",
            "label" => "Technical Skills",
            "column" => "technical_key_skill_names",
            "group_by" => false,
            "display_group" => "User"
        ],
        "referred_count" => [
            "table" => "users",
            "label" => "Total Referral Count",
            "column" => "referred_count",
            "group_by" => false,
            "display_group" => "User"
        ],
//        "referred_count" => [
//            "table" => "users",
//            "label" => "Active Referral Count",
//            "column" => "referred_count",
//            "group_by" => false,
//            "display_group" => "User"
//        ],
        "current_occupation" => [
            "table" => "users",
            "label" => "Employment Status",
            "column" => "current_occupation",
            "group_by" => false,
            "display_group" => "User"
        ],
        "badge_name" => [
            "table" => "ambassador_badges",
            "label" => "Ambassador Badges Assign",
            "column" => "badge_name",
            "group_by" => false,
            "display_group" => "User"
        ],
        "last_badge_date" => [
            "table" => "ambassador_badges",
            "label" => "Ambassador Badges Date",
            "column" => "last_badge_date",
            "group_by" => false,
            "display_group" => "User"
        ],
        "abandoned_payment_date" => [
            "table" => "abandoned_payment_reports",
            "label" => "Abandoned Payment Date",
            "column" => "created_at",
            'function' => 'DATE',
            "group_by" => false,
            "display_group" => "CourseUser",
            "format" => "date"
        ],
    ];

    const DATE_TYPE_COLUMNS = ['course_enroll_date', 'certificate_date', 'last_used_date', 'payment_completed_date', 'created_at', 'last_login_date', 'last_badge_date', 'abandoned_payment_date'];

    const TIME_OPTIONS = [
        'Between' => 'Between',
        'today' => 'Today',
        'yesterday' => 'Yesterday',
        'l2day' => 'Last Two Days',
        'week' => 'This week',
        'lweek' => 'Last Week',
        'l2week' => 'Last 2 Week',
        'l7days' => 'Last 7 days',
        'month' => 'This Month',
        'lmonth' => 'Last Month',
        'l30days' => 'Last 30 days',
        'l90days' => 'Last 90 days',
        'year' => 'This Year',
        'lyear' => 'Last Year'
    ];





    const ELEMENT_TYPE = ['courses' => 'courses',  'enroll_type' => 'Certificate Purchase','from_nsdc'=>'From NSDC','current_occupation'=>'Employment Status','colleges'=>'colleges','key_skills'=>'Key Skills','technical_skills'=>'Technical Skills','qualification'=>'Qualification','webinars'=>'Webinars'];
    const IDENTIFIER = ['is' => 'is', 'is_one_of' => 'is_one_of', 'is_not' => 'is_not', 'is_not_one_of' => 'is_not_one_of'];
    const ATTRIBUTE_IDENTIFIER = ['is' => 'is', 'is_not' => 'is_not'];
    const COMPARISON_IDENTIFIER = ['is' => 'is', 'equal_to' => 'equal_to', 'not_equal_to' => 'not_equal_to', 'greater_than' => 'greater_than', 'less_than' => 'less_than'];
}
