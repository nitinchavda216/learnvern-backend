<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Contracts\Activity;

class Configuration extends Model
{
    use LogsActivity;

    protected static $logName = 'Configuration';
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;

    protected $table = 'configuration';

    protected $primaryKey = 'id';

    protected $fillable = [ 'email_address', 'contact_no', 'facebook_link','youtube_link', 'instagram_link', 'linkedin_link','twitter_link','google_link', 'created_at', 'updated_at', 'deleted_at'];


    public function tapActivity(Activity $activity)
    {
        $admin = Auth::guard('admins')->user();
        $activity->causer_id = isset($admin) ? $admin->id : null;
    }

    public function scopeByActive($q)
    {
        $q->where('is_active', 1);
    }
}
