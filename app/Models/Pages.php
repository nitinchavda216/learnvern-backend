<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Contracts\Activity;

class Pages extends Model
{
    use LogsActivity;

    protected static $logName = 'Pages';
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;

    const PAGE_SECTION = [
        "about" => "About",
        "certificate_verification" => "Certificate Verification",
        "forum" => "Forum",
        "ambassador" => "Ambassador",
        "career" => "Career",
        "contact" => "Contact",
        "employers" => "Employers",
        "privacy" => "Privacy",
        "sitemap" => "Sitemap",
        "about_referral_for_mobile" => "About Referral"
    ];

    protected $table = 'pages';

    protected $primaryKey = 'id';

    protected $fillable = ['partner_id', 'meta_title', 'page_type', 'page_title', 'slug', 'meta_description',
        'meta_keywords', 'schema_script', 'canonical_url', 'content', 'app_content', 'created_at', 'updated_at', 'deleted_at'];

    public function tapActivity(Activity $activity)
    {
        $admin = Auth::guard('admins')->user();
        $activity->causer_id = isset($admin) ? $admin->id : null;
    }

    public function scopeByActive($q)
    {
        $q->where('is_active', 1);
    }

    public function scopeByPartnerId($query, $partner_id)
    {
        return $query->where('partner_id', $partner_id);
    }
}
