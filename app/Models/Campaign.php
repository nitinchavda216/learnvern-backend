<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    const ACTIONS = ['sms' => 'SMS', 'email' => 'Email', 'push-notification' => 'Push Notifications', 'calling-user' => 'Calling Users'];

    protected $table = 'campaign';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'name', 'action', 'marketing_goal', 'fields', 'conditions', 'is_allow_duplicate', 'sql_query', 'push_notification_details', 'is_complete', 'complete_date', 'created_by', 'updated_at'];

    const CALLING_ACTIONS = ['move_to_followup' => 'MOVE TO FOLLOWUP', 'ringing' => 'RINGING', 'reject' => 'REJECT', 'switch_off' => 'SWITCH OFF', 'invalid_number' => 'INVALID NUMBER', 'recall' => 'RECALL', 'not_interest' => 'NOT INTEREST', 'already_followup' => 'ALREADY FOLLOWUP'];
    const SHOWING_ACTIONS = ['move_to_followup' => 'MOVE TO FOLLOWUP', 'not_interest' => 'NOT INTEREST', 'other' => 'OTHER'];

    public function setPushNotificationDetailsAttribute($value)
    {
        $this->attributes['push_notification_details'] = json_encode($value);
    }
    public function getPushNotificationDetailsAttribute($value)
    {
        return json_decode($value, true);
    }
}
