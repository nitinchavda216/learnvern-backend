<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Interfaces\PaymentRepositoryInterface;
use Carbon\Carbon;


class TotalIncome extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];
    protected $paymentRepository;
    public $reloadTimeout = 0; // Widget will reload every 5 mins, because unit is seconds here
    public $cacheTime = 0; // Data will be cached for 10 mins
    public $encryptParams = false;

    public function __construct(PaymentRepositoryInterface $paymentRepository)
    {
        $this->paymentRepository = $paymentRepository;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $startDate = Carbon::now(); //returns current day
        $current_day_current_month = $startDate->toDateString();
        $first_day_current_month = Carbon::now()->firstOfMonth()->toDateString();
        $current_day_last_month = $startDate->subMonths(1)->toDateString();
        $first_day_last_month = Carbon::now()->subMonths(1)->firstOfMonth()->toDateString();

        $current_month_income = $this->paymentRepository->getIncome($first_day_current_month, $current_day_current_month);
        $last_month_income = $this->paymentRepository->getIncome($first_day_last_month, $current_day_last_month);

        if ($current_month_income > 0) {
            $percentage_diff_income = round((($current_month_income - $last_month_income) / $current_month_income) * 100, 2);
        } else {
            $percentage_diff_income = 0;
        }

        return view('widgets.total_income', [
            'config' => $this->config,
            'first_day_current_month' => $first_day_current_month,
            'current_day_current_month' => $current_day_current_month,
            'first_day_last_month' => $first_day_last_month,
            'current_day_last_month' => $current_day_last_month,
            'current_month_income' => $current_month_income,
            'last_month_income' => $last_month_income,
            'percentage_diff_income' => $percentage_diff_income,

        ]);
    }

    public function placeholder()
    {
        return '<div class="col-md-4"><p class="text-center">Loading Data ....</p></div>';
    }
}
