<?php

namespace App\Widgets;

use App\Models\Assignment;
use App\Models\Course;
use App\Models\Quiz;
use App\Models\Units;
use Arrilot\Widgets\AbstractWidget;

class CatalogStatistics extends AbstractWidget
{
    public $reloadTimeout = 0; // Widget will reload every 5 mins, because unit is seconds here
    public $cacheTime = 0; // Data will be cached for 10 mins
    public $encryptParams = false;

    public function run()
    {
        $statistics['courses'] = Course::where('course_type', '!=', 4)->count();
        $statistics['webinars'] = Course::where('course_type', 4)->count();
        $statistics['units'] = Units::count();
        $statistics['assignments'] = Assignment::count();
        $statistics['quizzes'] = Quiz::count();
        return view('widgets.catalog_statistics', [
            'config' => $this->config,
            'statistics' => $statistics,
        ]);
    }
}
