<?php

namespace App\Widgets;

use App\Interfaces\PaymentRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;
use Arrilot\Widgets\AbstractWidget;

class StatisticsWidget extends AbstractWidget
{
    protected $config = [];

    protected $userRepository;
    protected $paymentRepository;

    public function __construct(UserRepositoryInterface $userRepository, PaymentRepositoryInterface $paymentRepository)
    {
        $this->userRepository = $userRepository;
        $this->paymentRepository = $paymentRepository;
    }

    public function run()
    {
        /*$data = $this->userRepository->getAllUser();
        $income = $this->paymentRepository->getIncome();
        return view('widgets.statistics_widget', [
            'config' => $this->config,
            'data' => $data,
            'income' => $income,
        ]);*/
        return view('widgets.statistics_widget', [
            'config' => $this->config,
        ]);
    }
}
