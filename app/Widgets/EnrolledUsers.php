<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Interfaces\CourseUserRepositoryInterface;
use Carbon\Carbon;


class EnrolledUsers extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];
    protected $courseUserRepository;
    public $reloadTimeout = 0; // Widget will reload every 5 mins, because unit is seconds here
    public $cacheTime = 0; // Data will be cached for 10 mins
    public $encryptParams = false;


    public function __construct(CourseUserRepositoryInterface $courseUserRepository)
    {
        $this->courseUserRepository = $courseUserRepository;
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $startDate = Carbon::now(); //returns current day
        $current_day_current_month = $startDate->toDateString();
        $first_day_current_month = Carbon::now()->firstOfMonth()->toDateString();
        $current_day_last_month = $startDate->subMonths(1)->toDateString();
        $first_day_last_month = Carbon::now()->subMonths(1)->firstOfMonth()->toDateString();
        $current_month_enrollments = $this->courseUserRepository->getCourseEnrollmentsByDate($first_day_current_month,$current_day_current_month);
        $last_month_enrollments = $this->courseUserRepository->getCourseEnrollmentsByDate($first_day_last_month,$current_day_last_month);

        if ($current_month_enrollments > 0) {
            $percentage_diff_value = round((($current_month_enrollments - $last_month_enrollments) / $current_month_enrollments) * 100, 2);
        } else {
            $percentage_diff_value = 0;
        }


        return view('widgets.enrolled_users', [
            'config' => $this->config,
            'first_day_current_month' => $first_day_current_month,
            'current_day_current_month' => $current_day_current_month,
            'first_day_last_month' => $first_day_last_month,
            'current_day_last_month' => $current_day_last_month,
            'current_month_enrollments' => $current_month_enrollments,
            'last_month_enrollments' => $last_month_enrollments,
            'percentage_diff_value' => $percentage_diff_value
        ]);
    }



    public function placeholder()
    {
        return '<div class="col-md-4"><p class="text-center">Loading Data ....</p></div>';
    }
}
