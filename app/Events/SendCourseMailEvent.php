<?php

namespace App\Events;

use App\Models\Course;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendCourseMailEvent implements ShouldQueue
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $course;

    public function __construct(Course $course)
    {

        $this->course = $course;
    }


}
