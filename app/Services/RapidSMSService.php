<?php

namespace App\Services;

use GuzzleHttp\Client;

class RapidSMSService
{
    public function __construct()
    {
    }

    public function sendSMS($data): \Psr\Http\Message\ResponseInterface
    {
        $url = "https://1.rapidsms.co.in/api/push";
        $client = new Client();
        $response = $client->request('GET', $url, ['query' => [
            'apikey' => env('RAPID_SMS_API_KEY'),
            'sender' => 'LERNVN',
            'mobileno' => $data['mobile_number'],
            'text' => $data['otp_code']." is your OTP to verify your mobile number on learnvern.
G3VqOa9Svjd"
        ]]);
        return $response;
    }
}
