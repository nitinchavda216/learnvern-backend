<?php
/**
 * Created by PhpStorm.
 * User: ombharti
 * Date: 1/2/2018
 * Time: 5:03 PM
 */

namespace App\Services;

use App\Models\EmailTemplates;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class EmailService
{
    protected $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendEmail($mail_params_array, $template = null, $dynamic_data = null)
    {
        $to = $mail_params_array['to'];
        $result = null;
        if (is_array($to)) {
            foreach ($mail_params_array['to'] as $user_email) {
                $result = self::sendEmailToUser($user_email, $template, $dynamic_data);
            }
        } else {
            $result = self::sendEmailToUser($to, $template, $dynamic_data);
        }
        return $result;
    }

    public function sendEmailToUser($to, $template = null, $dynamic_data = null)
    {
        if ($template == "new_course_create_mail") {
            $to = ['niral@learnvern.com', 'aditi.shah@learnvern.com'];
        }
        $from = env('MAIL_FROM_ADDRESS', 'info@learnvern.com');
        $from_name = env('MAIL_FROM_NAME', 'Learnvern Team');
        $template_variables = Config::get('emailvariables.' . $template);
        $replace_array = $key_array = [];
        if (!empty($template_variables)) {
            foreach ($template_variables as $key => $template_variable) {
                $key_array[] = "#" . $key . "#";
                $replace_array[] = (isset($dynamic_data[trim($key, '#')])) ? $dynamic_data[trim($key, '#')] : '';
            }
        }
        $email_data = null;
        if (!is_null($template)) {
            $email_data = EmailTemplates::where('identifier', $template)->first();
        }
        if (isset($email_data)) {
            $file_path = isset($email_data->attachment) ? showEmailAttachment($email_data->attachment) : null;
            $subject = str_replace($key_array, $replace_array, $email_data['subject']);
            $content = str_replace($key_array, $replace_array, $email_data->content);
            $content_data = ['email_content' => $content];
            try {
                Mail::send('emailsample', $content_data, function ($message) use ($to, $from, $from_name, $subject, $file_path) {
                    $message->from($from, $from_name)
                        ->to($to)
                        ->subject($subject);
                    if (isset($file_path)) {
                        $message->attach($file_path);
                    }
                    $headers = $message->getHeaders();
                    $headers->addTextHeader('X-MC-PreserveRecipients', 'false');
                });
            } catch (\Exception $e) {
                Log::error("Email not sent from the Emails. Returned with error: " . $e->getMessage());
                return $e->getMessage();
            }
        }
        return Mail::failures();
    }

    public function sendAmbassadorEmail($to, $template = null, $dynamic_data = null, $userData = null)
    {
        $from = env('MAIL_FROM_ADDRESS', 'info@learnvern.com');
        $from_name = env('MAIL_FROM_NAME', 'Learnvern Team');
        $template_variables = Config::get('emailvariables.' . $template);
        $replace_array = $key_array = [];
        if (!empty($template_variables)) {
            foreach ($template_variables as $key => $template_variable) {
                $key_array[] = "#" . $key . "#";
                $replace_array[] = (isset($dynamic_data[trim($key, '#')])) ? $dynamic_data[trim($key, '#')] : '';
            }
        }
        $email_data = null;
        if (!is_null($template)) {
            $email_data = EmailTemplates::where('identifier', $template)->first();
        }
        if (isset($email_data)) {
            $file_path = getAmbassadorPdf("Learnvern_Recommendation_Letterhead_" . $userData['id'] . "_" . $userData['referred_count'] . ".pdf");
            $subject = "Letter of Recommendation";
            $content = str_replace($key_array, $replace_array, $email_data->content);
            $content_data = ['email_content' => $content];
            try {
                Mail::send('emailsample', $content_data, function ($message) use ($to, $from, $from_name, $subject, $file_path) {
                    $message->from($from, $from_name)
                        ->to($to)
                        ->subject($subject)
                        ->attach($file_path);
                    $headers = $message->getHeaders();
                    $headers->addTextHeader('X-MC-PreserveRecipients', 'false');
                });
                @unlink(storage_path("app/public/ambassador_certificates/") . "Learnvern_Recommendation_Letterhead_" . $userData['id'] . "_" . $userData['referred_count'] . ".pdf");
            } catch (\Exception $e) {
                Log::error("Ambassador badge email not sent from the Emails. Returned with error: " . $e->getMessage());
                return $e->getMessage();
            }
        }
        return Mail::failures();
    }

    public function sendDailyStatisticsMail($template, $dynamic_data)
    {
        $from = env('MAIL_FROM_ADDRESS', 'info@learnvern.com');
        $from_name = env('MAIL_FROM_NAME', 'Learnvern Team');
        $template_variables = Config::get('emailvariables.' . $template);
        $replace_array = $key_array = [];
        if (!empty($template_variables)) {
            foreach ($template_variables as $key => $template_variable) {
                $key_array[] = "#" . $key . "#";
                $replace_array[] = (isset($dynamic_data[trim($key, '#')])) ? $dynamic_data[trim($key, '#')] : '';
            }
        }
        $email_data = null;
        if (!is_null($template)) {
            $email_data = EmailTemplates::where('identifier', $template)->first();
        }
        if (isset($email_data)) {
            $fileName = "LearnVern_Daily_Statistics_" . formatDate($dynamic_data['DATE'], 'd_m_Y') . ".pdf";
            $file_path = getDailyStatisticsPdf($fileName);
            $missingKeywordFileName = "LearnVern_Missing_Keyword_Statistics_" . formatDate($dynamic_data['DATE'], 'd_m_Y') . ".pdf";
            $missing_keyword_file_path = getDailyStatisticsPdf($missingKeywordFileName);
            $subject = str_replace($key_array, $replace_array, $email_data->subject);
            $content = str_replace($key_array, $replace_array, $email_data->content);
            $content_data = ['email_content' => $content];
            try {
                Mail::send('emailsample', $content_data, function ($message) use ($from, $from_name, $subject, $file_path, $missing_keyword_file_path) {
                    $message->from($from, $from_name)
                        ->to('info@learnvern.com')
                        ->cc(['niral@learnvern.com', 'meetmalay@gmail.com','alok.kumar@learnvern.com','shivang.learnvern@gmail.com','preetha.bhatt@learnvern.com','aditi.shah@learnvern.com','swati.jain@learnvern.com'])
                        ->subject($subject)
                        ->attach($file_path)
                        ->attach($missing_keyword_file_path);
                    $headers = $message->getHeaders();
                    $headers->addTextHeader('X-MC-PreserveRecipients', 'false');
                });
                @unlink(storage_path("app/public/daily_statistics/") . $fileName);
                @unlink(storage_path("app/public/daily_statistics/") . $missingKeywordFileName);
            } catch (\Exception $e) {
                Log::error("Daily Statistics not sent from the Emails. Returned with error: " . $e->getMessage());
                return $e->getMessage();
            }
        }
        return Mail::failures();
    }

    public function sendUnitUpdateLinkToAdmins($template, $dynamic_data)
    {
        $from = env('MAIL_FROM_ADDRESS', 'info@learnvern.com');
        $from_name = env('MAIL_FROM_NAME', 'Learnvern Team');
        $template_variables = Config::get('emailvariables.' . $template);
        $replace_array = $key_array = [];
        if (!empty($template_variables)) {
            foreach ($template_variables as $key => $template_variable) {
                $key_array[] = "#" . $key . "#";
                $replace_array[] = (isset($dynamic_data[trim($key, '#')])) ? $dynamic_data[trim($key, '#')] : '';
            }
        }
        $email_data = null;
        if (!is_null($template)) {
            $email_data = EmailTemplates::where('identifier', $template)->first();
        }
        if (isset($email_data)) {
            $subject = str_replace($key_array, $replace_array, $email_data->subject);
            $content = str_replace($key_array, $replace_array, $email_data->content);
            $content_data = ['email_content' => $content];
            try {
                Mail::send('emailsample', $content_data, function ($message) use ($from, $from_name, $subject) {
                    $message->from($from, $from_name)
                        ->to('niral@learnvern.com')
                        ->cc(['meetmalay@gmail.com','aditi.shah@learnvern.com'])
                        ->subject($subject);
                    $headers = $message->getHeaders();
                    $headers->addTextHeader('X-MC-PreserveRecipients', 'false');
                });
            } catch (\Exception $e) {
                Log::error("sendUpdateSlugMailToAdmins not sent from the Emails. Returned with error: " . $e->getMessage());
                return $e->getMessage();
            }
        }
        return Mail::failures();
    }
}
