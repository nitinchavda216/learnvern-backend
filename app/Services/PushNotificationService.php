<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 07-02-2019
 * Time: 10:42
 */

namespace App\Services;

use FCM;

class PushNotificationService
{
    public function notify_push($device_id, $title, $text, $data_array)
    {
        // prep the bundle
        $notification = array(
            'body' => $text,
            'title' => $title,
//            'smallIcon' => 'small_icon'
//            'largeIcon' => 'large_icon',
//            'click_action' => ".MainActivity",
            'sound' => 'default',
        );
        if($data_array['image']){
            $notification['image'] = $data_array['image'];
        }
        $fields_new = array(
            'registration_ids' => $device_id,
            'data' => $data_array['data'],
            'notification' => $notification,
        );
        $headers = array(
            'Content-Type: application/json',
            'Authorization: key='.env('FCM_SERVER_KEY'),
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields_new));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
