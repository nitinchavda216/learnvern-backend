<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 2/5/2018
 * Time: 11:26 AM
 */

namespace App\Services;

use App\Events\SendDailyReportMailEvent;
use App\Models\Admins;
use App\Models\AnonymousUser;
use App\Models\Course;
use App\Models\CourseUser;
use App\Models\PaymentTransaction;
use App\Models\User;
use Carbon\Carbon;
use PDF;
use Illuminate\Support\Facades\DB;
use App\Models\MissingSearchKeyword;

class SendDailyReportService
{
    public function sendReportMail()
    {
        $reportDate = Carbon::yesterday();
        $registerUsers = User::where('partner_id', 1)->select('id', 'signup_from')
            ->whereDate('created_at', $reportDate)->get()->groupBy('signup_from')->toArray();
        $pdfData['registered_users']['total'] = 0;
        $pdfData['registered_users']['NSDC_signups'] = isset($registerUsers[10]) ? count($registerUsers[10]) : 0;
        $pdfData['registered_users']['google+_web'] = isset($registerUsers[2]) ? count($registerUsers[2]) : 0;
        $pdfData['registered_users']['google+_android'] = isset($registerUsers[6]) ? count($registerUsers[6]) : 0;
        $pdfData['registered_users']['facebook_android'] = isset($registerUsers[5]) ? count($registerUsers[5]) : 0;
        $pdfData['registered_users']['manual_signup_web'] = isset($registerUsers[3]) ? count($registerUsers[3]) : 0;
        $pdfData['registered_users']['manual_signup_android'] = isset($registerUsers[4]) ? count($registerUsers[4]) : 0;
        $pdfData['registered_users']['total'] = $pdfData['registered_users']['NSDC_signups'] + $pdfData['registered_users']['google+_web'] +
            $pdfData['registered_users']['google+_android'] + $pdfData['registered_users']['manual_signup_web'] +
            $pdfData['registered_users']['manual_signup_android'] + $pdfData['registered_users']['facebook_android'];
        $certificateSales = [];
        $pdfData['total_certificate_sales'] = 0;
        $paymentTransactions = PaymentTransaction::where('partner_id', 1)->whereDate('created_at', $reportDate)->get()->groupBy('course_id');
        foreach ($paymentTransactions as $course_id => $paymentTransaction) {
            $certificateSales[$course_id] = count($paymentTransaction);
            $pdfData['total_certificate_sales'] += count($paymentTransaction);
        }
        $courseUsers = CourseUser::join('courses', 'course_user.course_id', '=', 'courses.id')
            ->where('partner_id', 1)
            ->whereDate('course_user.created_at', $reportDate)
            ->select('course_user.id', 'course_id', 'courses.name', 'course_type', DB::raw('count(*) as total'))
            ->groupBy('course_id')
            ->orderBy('total', 'DESC')
            ->get()->toArray();
        $pdfData['course_enrollments']['courses'] = $pdfData['course_enrollments']['webinars'] = [];
        foreach ($courseUsers as $item) {
            $arrayData = [
                'id' => $item['id'],
                'title' => $item['name'],
                'total_count' => $item['total'],
                'certificate_sales' => $certificateSales[$item['course_id']] ?? 0
            ];
            if ($item['course_type'] == 4){
                $pdfData['course_enrollments']['webinars'][] = $arrayData;
            }else{
                $pdfData['course_enrollments']['courses'][] = $arrayData;
            }
            unset($certificateSales[$item['course_id']]);
        }
        if (!empty($certificateSales)) {
            $courses = Course::whereIn('id', array_keys($certificateSales))->select('id','name', 'course_type')->get();
            foreach ($courses as $courseItem) {
                $courseArrayData = [
                    'id' => $courseItem['id'],
                    'title' => $courseItem['name'],
                    'total_count' => 0,
                    'certificate_sales' => $certificateSales[$courseItem['id']]
                ];
                if ($courseItem['course_type'] == 4){
                    $pdfData['course_enrollments']['webinars'][] = $courseArrayData;
                }else{
                    $pdfData['course_enrollments']['courses'][] = $courseArrayData;
                }
            }
        }
        $pdfData['anonymous_users'] = AnonymousUser::whereDate('created_at', $reportDate)->count();
        $referredUsers = User::where('partner_id', 1)
            ->select(DB::raw('count(*) as total'))
            ->whereDate('created_at', $reportDate)
            ->where('referral', '!=', null)
            ->groupBy('is_active')
            ->get()->toArray();
        $pdfData['referral']['active'] = isset($referredUsers[1]) ? $referredUsers[1]['total'] : 0;
        $pdfData['referral']['inactive'] = isset($referredUsers[0]) ? $referredUsers[0]['total'] : 0;
        $where = 'WHERE payment_transactions.partner_id =1';
        $where .= " AND (payment_transactions.transaction_date BETWEEN  '" . $reportDate . "' AND '" . $reportDate . "')";
        $countForCampaignOrOrganic = "SELECT COUNT(CASE WHEN calling.id IS NULL THEN 1 END) AS organic_sell,COUNT(CASE WHEN calling.id IS NOT NULL THEN 1 END) AS campaign_sell
            FROM payment_transactions
            LEFT JOIN
            (
                SELECT campaign_users_calling.id,campaign_users_calling.`course_id`,campaign_users_calling.`user_id`,campaign_users_calling.`first_call_date`
                FROM campaign_users_calling
                LEFT JOIN campaign ON campaign_users_calling.`campaign_id` = campaign.id
                WHERE campaign.marketing_goal = 'revenue' AND campaign_users_calling.calling_action IS NULL AND campaign_users_calling.parent_id IS NOT NULL
            ) calling
            ON payment_transactions.user_id = calling.user_id AND DATE(payment_transactions.transaction_date) >= DATE(calling.first_call_date) $where";
        $pdfData['sales_statistics'] = (array)DB::selectOne($countForCampaignOrOrganic);
        $crm_users = Admins::whereHas('relatedRole', function ($q){
            $q->where('user_type', 'crm');
        })->select('id', 'name', 'email', 'role_id')->get();
        $pdfData['crm_users_history'] = [];
        $revenueWhere = " AND (payment_transactions.transaction_date BETWEEN  '" . formatDate($reportDate, 'Y-m-d') . " 00:00:00' AND '" . formatDate($reportDate, 'Y-m-d') . " 23:59:59')";
        foreach ($crm_users as $crm_user){
            $revenueLeadQuery = "SELECT count(*) AS revenue_active_followup
FROM campaign_users_calling
LEFT JOIN admins ON campaign_users_calling.assign_to = admins.id
LEFT JOIN users ON campaign_users_calling.user_id = users.id
LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
LEFT JOIN payment_transactions ON payment_transactions.`user_id` = campaign_users_calling.`user_id`
LEFT JOIN courses ON payment_transactions.course_id = courses.id
WHERE campaign_users_calling.assign_to=$crm_user->id AND campaign.marketing_goal = 'revenue' AND campaign_users_calling.calling_action IS NULL AND campaign_users_calling.parent_id IS NOT NULL
$revenueWhere";
            
            $revenueLeadQueryResult = (array)DB::selectOne($revenueLeadQuery);
            if ($revenueLeadQueryResult['revenue_active_followup'] > 0){
                $pdfData['crm_users_history'][] = [
                    'name' => $crm_user->name,
                    'sales_count' => $revenueLeadQueryResult['revenue_active_followup'],
                ];
            }
        }
        $html = view('daily_statistics_pdf', compact('pdfData', 'reportDate'))->render();
        $dompdf = PDF::loadHtml($html);
        $fileName = "LearnVern_Daily_Statistics_" . formatDate($reportDate, 'd_m_Y') . ".pdf";
        $path = storage_path('app/public/daily_statistics/');
        $dompdf->save($path . '/' . $fileName);
        // Missing Keyword Attachment Section Starts from here.
        $get_missing_keyword_datas = MissingSearchKeyword::whereBetween('updated_at', [Carbon::yesterday()->startOfDay(), Carbon::yesterday()->endOfDay()])->orderBy('attempts','desc')->get();
        $get_missing_keyword_data_html = view('missing_keyword_statistics_pdf', compact('get_missing_keyword_datas', 'reportDate'))->render();
        $get_missing_keyword_data_pdf= PDF::loadHtml($get_missing_keyword_data_html);
        $missing_keyword_data_fileName = "LearnVern_Missing_Keyword_Statistics_" . formatDate($reportDate, 'd_m_Y') . ".pdf";
        $get_missing_keyword_data_pdf->save($path . '/' . $missing_keyword_data_fileName);
        // Missing Keyword Attachment Section Ends here.
        $emailData = ['date' => $reportDate];
        event(new SendDailyReportMailEvent($emailData));
        return true;
    }
}
