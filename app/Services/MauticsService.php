<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 2/5/2018
 * Time: 11:26 AM
 */

namespace App\Services;

use App\Jobs\UpdateMarketLearnVernDataJob;
use App\Jobs\UpdateMauticDataJob;
use App\Models\User;
use Illuminate\Queue\SerializesModels;

class MauticsService
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function storeUserData($user_id)
    {
        $userDetail = User::where('id', $user_id)->where('partner_id', 1)->first();
        if (env('FRIENDLY_MAUTICS_ENABLE')) {
            if ($userDetail) {
                UpdateMauticDataJob::dispatch($userDetail);
            }
        }
        if (env('LEARNVERN_MAUTICS_ENABLE')) {
            if ($userDetail) {
                UpdateMarketLearnVernDataJob::dispatch($userDetail);
            }
        }
    }
}
