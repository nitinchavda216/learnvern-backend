<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 2/5/2018
 * Time: 11:26 AM
 */

namespace App\Services;

use App\Models\CampaignBlockUsers;
use App\Models\LoginHistory;
use Jenssegers\Agent\Agent;
use Illuminate\Queue\SerializesModels;

class StoreLoginHistory
{
    use SerializesModels;

    public function store($ip_address, $user_id, $login_from)
    {
        $ip_address = $ip_address ?? '127.0.0.1';

        $agent = new Agent();
        $browser = $agent->browser();
        $platform = $agent->platform();
        $device = $agent->device();
        $version = $agent->version($browser);
        $browserDetails = request()->server('HTTP_USER_AGENT');

        $response = getUserIpDetails($ip_address);
        if (isset($response)) {
            $data = [
                'user_id' => $user_id,
                'ip' => $ip_address,
                'hostname' => $ip_address,
                'country_code' => !empty($response->country_code) ? $response->country_code : null,
                'country_name' => !empty($response->country_name) ? $response->country_name : null,
                'browser' => $browser,
                'browser_details' => $browserDetails,
                'login_from' => $login_from,
            ];
            $data['ip_details'] = json_encode([
                'continent_code' => !empty($response->continent_code) ? $response->continent_code : null,
                'continent_name' => !empty($response->continent_name) ? $response->continent_name : null,
                'region_code' => !empty($response->region_code) ? $response->region_code : null,
                'region_name' => !empty($response->region_name) ? $response->region_name : null,
                'city' => !empty($response->city) ? $response->city : null,
                'zip' => !empty($response->zip) ? $response->zip : null,
                'latitude' => !empty($response->latitude) ? $response->latitude : null,
                'longitude' => !empty($response->longitude) ? $response->longitude : null,
                'location' => !empty($response->location) ? json_encode($response->location) : null,
                'time_zone' => !empty($response->time_zone) ? json_encode($response->time_zone) : null,
                'currency' => !empty($response->currency) ? json_encode($response->currency) : null,
                'connection' => !empty($response->connection) ? json_encode($response->connection) : null,
                'security' => 'test',
                'device' => $device,
                'platform' => $platform,
                'version' => $version
            ]);
            LoginHistory::create($data);

            // Delete the user exists in the campaign_block_user table if the user exists
            CampaignBlockUsers::where('user_id', $user_id)->delete();
        }
    }
}
