<?php

namespace App\Services;

use App\Events\PaymentConfirmationEmailEvent;
use App\Models\Course;
use App\Models\CourseUser;
use App\Models\PaymentTransaction;
use App\Models\SiteConfiguration;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Razorpay\Api\Api;


class RazorpayService
{
    protected $api, $user, $courseDetail, $siteConfiguration, $currencyCode, $partnerId;

    public function __construct()
    {
        $this->api = new Api(config('services.razorpay.key'), config('services.razorpay.secret'));
    }

    public function sendPaymentLink($data)
    {
        try {
            $postData = [
                'amount' => $data['price'] * 100,
                'type' => "link",
                'currency' => "INR",
                'description' => "Payment Link for course ".$data['course_name'],
                'customer' => [
                    "name" => $data['name'],
                    "contact" => $data['mobile_number'],
                    "email" => $data['email']
                ],
                "callback_url" => route('payment_success'),
                "callback_method" => 'get',
            ];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://api.razorpay.com/v1/invoices/');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
            curl_setopt($ch, CURLOPT_USERPWD, env('RAZOR_KEY') . ':' . env('RAZOR_SECRET'));
            $headers = array();
            $headers[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $response = curl_exec($ch);
            curl_close($ch);
            $responseData = json_decode($response, true);
            if (isset($responseData['error'])) {
                return ['status' => false, 'message' => $responseData['error']['description']];
            }
            return ['status' => true, 'message' => 'Payment Link Sent Successfully', 'data' => json_decode($response, true)];
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function resendPaymentLink($invoiceId)
    {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://api.razorpay.com/v1/invoices/' . $invoiceId . '/notify/sms');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            $headers = array();
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $responseData = curl_exec($ch);
            curl_close($ch);
            if (isset($responseData['error'])) {
                return ['status' => false, 'message' => $responseData['error']['description']];
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://api.razorpay.com/v1/invoices/' . $invoiceId . '/notify/email');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            $headers = array();
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $responseData = curl_exec($ch);
            curl_close($ch);
            if (isset($responseData['error'])) {
                return ['status' => false, 'message' => $responseData['error']['description']];
            }
            return ['status' => true, 'message' => 'Payment Link Sent Successfully'];
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    /**
     * @param array $requestData = []
     * @return array
     */
    public function getOrderId(array $requestData): array
    {
        try {
            $this->setPartnerId($requestData['partner_id'] ?? 1);
            $this->setCourseDetail($requestData['course_id']);
            $this->setUser($requestData['user_id']);
            $this->setSiteConfiguration();
            $this->setCurrencyCode($requestData['currencyCode'] ?? 'INR');

            $orderData = [
                'receipt' => "Txn" . rand(10, 99) . date("YmdHis"),
                'amount' => $this->courseDetail->partnerCourses->where('partner_id', $this->partnerId)->first()->course_fees * 100,
                'currency' => $this->currencyCode,
                'payment_capture' => 1
            ];
            $razorpayOrder = $this->api->order->create($orderData);
            $razorpayOrderId = $razorpayOrder['id'];

            $data = [
                "key" => config('services.razorpay.key'),
                "amount" => $orderData['amount'],
                "name" => $this->courseDetail->name,
                "description" => $this->courseDetail->categoryDetail->name,
                "image" => $this->siteConfiguration->configuration_value ?? '',
                "prefill" => [
                    "name" => $this->user->first_name,
                    "email" => $this->user->email,
                    "contact" => $this->user->mobile_number ?? '',
                ],
                "notes" => [
                    "address" => "",
                    "merchant_order_id" => $orderData['receipt'],
                ],
                "theme" => [
                    "color" => config('services.bolt.modal_color')
                ],
                "order_id" => $razorpayOrderId,
            ];
            $response = ["status" => 200, 'message' => 'Payment Order Id Created Successfully.', "data" => $data];
        } catch (\Exception $e) {
            $response = ['status' => 100, 'message' => $e->getMessage(), 'data' => []];
        }
        return $response;
    }

    /**
     * @return mixed
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param $user_id
     */
    public function setUser($user_id): void
    {
        $this->user = User::query()->findOrFail($user_id);
    }

    /**
     * @return mixed
     */
    public function getCourseDetail(): Course
    {
        return $this->courseDetail;
    }

    /**
     * @param $course_id
     * @throws \Exception
     */
    public function setCourseDetail($course_id): void
    {
        $courseDetail = Course::with(['categoryDetail', 'partnerCourses'])->ByActive()->where('id', '=', $course_id)->first();
        if (is_null($courseDetail))
            throw new \Exception('Invalid Course Id');
        $this->courseDetail = $courseDetail;
    }

    /**
     * @return mixed
     */
    public function getSiteConfiguration()
    {
        return $this->siteConfiguration;
    }

    /**
     * @throws \Exception
     */
    public function setSiteConfiguration(): void
    {
        $configuration = SiteConfiguration::where('partner_id', $this->partnerId)->where('identifier', 'logo')->first();
        if (is_null($configuration))
            throw new \Exception('Invalid Site Configuration');
        $this->siteConfiguration = $configuration;
    }

    /**
     * @return mixed
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @param mixed $currencyCode
     */
    public function setCurrencyCode($currencyCode = 'INR'): void
    {
        $this->currencyCode = $currencyCode;
    }

    public function setPartnerId($partnerId = 1): void
    {
        $this->partnerId = $partnerId;
    }
}
