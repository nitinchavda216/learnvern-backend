<?php
/**
 * Created by PhpStorm.
 * User: ombharti
 * Date: 1/2/2018
 * Time: 5:03 PM
 */
namespace App\Services;

use App\Models\Course;
use Spatie\SchemaOrg\Schema;

class GenerateCourseSchema
{
    public function generateCourseSchema($course_id)
    {
        $course = Course::find($course_id);
        $schemaScript = "";
        $schemaScript .= Schema::webPage()
            ->name($course->name)
            ->url(env('FRONT_URL') . 'course/' . $course->slug)
            ->speakable(Schema::speakableSpecification()->xpath([
                "/html/head/title",
                "/html/head/meta[@name='description']/@content"
            ]));
        /*PRODUCT  SCHEMA*/
        $productSchema = Schema::course()
            ->name($course->name)
            ->description(strip_tags($course->brief_description))
            ->image($course->course_icon)
            ->aggregateRating(Schema::aggregateRating()
                ->ratingValue($course->average_rating)
                ->reviewCount($course->comment_count)
            );

        $reviewData = [];
        $reviews = $course->relatedComments->where('is_default_testimonial', 1);
        foreach ($reviews as $review) {
            $reviewData[] = Schema::review()
                ->reviewRating(Schema::rating()->ratingValue($review->rating))
                ->author(Schema::person()->name($review->author));
        }
        $schemaScript .= $productSchema->review($reviewData);
        /*PRODUCT  SCHEMA END*/

        /*FAQS SCHEMA*/
        if(count($course->relatedFaqs) > 0){
            $faqsSchema = Schema::fAQPage();
            foreach ($course->relatedFaqs as $faq) {
                $faqMinEntityData[] = Schema::question()->name($faq['question'])->acceptedAnswer(Schema::answer()->text($faq['answer']));
            }
            if (!empty($faqsSchema)) {
                $faqsSchema->mainEntity($faqMinEntityData);
            }
            $schemaScript .= $faqsSchema;
        }
        /*FAQS SCHEMA END*/

        $schemaScript .= Schema::videoObject()
            ->name($course->name)
            ->description($course->brief_description)
            ->contentUrl($course->intro_video)
            ->embedUrl($course->intro_video_embed);

        dd($schemaScript);
    }
}