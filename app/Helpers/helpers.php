<?php
function getCurrentAdmin()
{
    if (auth()->guard('admins')->check()) {
        return auth()->guard('admins')->user();
    }
}

function getUserNameInitials($first_name)
{
    $letters = explode(' ', $first_name);
    return substr($letters[0], 0, 1) . (isset($letters[1]) ? substr($letters[1], 0, 1) : "");
}

if (!function_exists('formatDate')) {
    function formatDate($date, $format = 'd-m-Y h:i A')
    {
        if ($date != null) {
            return Carbon\Carbon::parse($date)->format($format);
        }
        return null;
    }
}

if (!function_exists('convertAppHtml')) {
    function convertAppHtml($content)
    {
        if ($content != "") {
            $output = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $content);
            $output = strip_tags($output, '<h1><h2><h3><h4><h5><h6><p><u><b><strong><br><ul><ol><li><a>');
            return $output;
        }
        return null;
    }
}

if (!function_exists('getIsdCode')) {
    function getIsdCode($isd_code = null)
    {
        if (isset($isd_code) && $isd_code != "") {
            if (substr($isd_code, 0, 1) == "+") {
                return $isd_code;
            } else {
                return '+' . $isd_code;
            }
        }
        return null;
    }
}
if (!function_exists('getPartnerCourceId')) {
    function getPartnerCoursesIdArray()
    {
        return \App\Models\PartnerCourses::where('partner_id', 1)->pluck('course_id')->toArray();
    }
}
if (!function_exists('showBannerImage')) {
    function showBannerImage($image_name)
    {
        return url(\App\Models\Banners::IMG_URL . $image_name);
    }
}
function formatPrice($price = 0)
{
    return "₹ " . number_format(($price ? floatval($price) : 0), 2, ".", ",");
}

if (!function_exists('showCourseImage')) {
    function showCourseImage($image_name)
    {
        return url(App\Models\Course::IMG_URL . $image_name);
    }
}

if (!function_exists('showCourseIconImage')) {
    function showCourseIconImage($image_name)
    {
        return url(App\Models\Course::ICON_IMG_URL . $image_name);
    }
}

if (!function_exists('showCourseAppIcon')) {
    function showCourseAppIcon($image_name)
    {
        return url(App\Models\Course::APP_ICON_URL . $image_name);
    }
}

if (!function_exists('showCourseAppImage')) {
    function showCourseAppImage($image_name)
    {
        return url(App\Models\Course::APP_IMG_URL . $image_name);
    }
}

if (!function_exists('showResumeTemplateImage')) {
    function showResumeTemplateImage($image_name)
    {
        return url(App\Models\ResumeTemplates::IMG_URL . $image_name);
    }
}

if (!function_exists('showNotificationImage')) {
    function showNotificationImage($image_name)
    {
        return url(App\Models\Notification::IMG_URL . $image_name);
    }
}

if (!function_exists('showdocument')) {
    function showdocument($image_name)
    {
        return url(App\Models\PartnersSettings::IMG_URL . $image_name);
    }
}


if (!function_exists('getAmbassadorPdf')) {
    function getAmbassadorPdf($item)
    {
        return url('storage/ambassador_certificates/' . $item);
    }
}
if (!function_exists('getDailyStatisticsPdf')) {
    function getDailyStatisticsPdf($item)
    {
        return url('storage/daily_statistics/' . $item);
    }
}

if (!function_exists('shoWidgetsImage')) {
    function shoWidgetsImage($image_name)
    {
        return url(App\Models\WidgetsSettings::IMG_URL . $image_name);
    }
}
if (!function_exists('showEmailAttachment')) {
    function showEmailAttachment($file_name)
    {
        return url(App\Models\EmailTemplates::ATTACHMENT_URL . $file_name);
    }
}
if (!function_exists('showAmbassadorSettingImage')) {
    function showAmbassadorSettingImage($image_name)
    {
        return url(App\Models\AmbassadorProgramSettings::IMG_URL . $image_name);
    }
}
function getVimeoVideoEmbedUrl($url)
{
    $regs = array();
    $id = '';
    if (preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $url, $regs)) {
        $id = $regs[3];
    }
    return "https://player.vimeo.com/video/" . $id;
}

function getVimeoVideoDuration($video_url)
{
    $oembed_endpoint = 'http://vimeo.com/api/oembed';
    $json_url = $oembed_endpoint . '.json?url=' .
        rawurlencode($video_url) . '&width=640';
    $ch = curl_init($json_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $video_arr = curl_exec($ch);
    $video_arr = json_decode($video_arr, TRUE);
    $vid_duration = $video_arr['duration'];
    /*$video_length =
        str_pad(floor($vid_duration / 3600), 2, '0', STR_PAD_LEFT) . ':'
        . str_pad(floor($vid_duration / 60), 2, '0', STR_PAD_LEFT) . ':'
        . str_pad($vid_duration % 60, 2, '0', STR_PAD_LEFT);*/
    return gmdate("H:i:s", $vid_duration);
}

function getYoutubeDuration($videoDd)
{
    // video json data
    $ch = curl_init("https://www.googleapis.com/youtube/v3/videos?part=contentDetails&id=" . $videoDd . "&key='AIzaSyCt6UdASkotGKkEV0Pg05n2XQrnkK9d-Eo'");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $video_arr = curl_exec($ch);
    $video_arr = json_decode($video_arr, TRUE);
    $vTime = '';
    $H = 0;
    $M = 0;
    $S = 0;
    foreach ($video_arr['items'] as $vidTime) {
        $vTime = $vidTime['contentDetails']['duration'];
    }
    $vTime = substr($vTime, 2);
    $exp = explode("H", $vTime);
    //if explode executed
    if ($exp[0] != $vTime) {
        $H = $exp[0];
        $vTime = $exp[1];
    }
    $exp = explode("M", $vTime);
    if ($exp[0] != $vTime) {
        $M = $exp[0];
        $vTime = $exp[1];
    }
    $exp = explode("S", $vTime);
    $S = $exp[0];
    return $H . ':' . $M . ':' . $S;
}

function getUserIP()
{
    // Get real visitor IP behind CloudFlare network
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
        $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    $forward = @$_SERVER['HTTP_X_REAL_IP'];
    $remote = $_SERVER['REMOTE_ADDR'];

    $ip = (filter_var($forward, FILTER_VALIDATE_IP)) ? $forward : $remote;
    return $ip;
}

function getYouTubeIdFromURL($url)
{
    if (stristr($url, 'youtu.be/')) {
        preg_match('/(https:|http:|)(\/\/www\.|\/\/|)(.*?)\/(.{11})/i', $url, $final_ID);
        return $final_ID[4];
    } else {
        @preg_match('/(https:|http:|):(\/\/www\.|\/\/|)(.*?)\/(embed\/|watch.*?v=|)([a-z_A-Z0-9\-]{11})/i', $url, $IDD);
        return $IDD[5];
    }

}

if (!function_exists('showCategoryImageIcon')) {
    function showCategoryImageIcon($image_name)
    {
        return url(\App\Models\Categories::IMG_URL . $image_name);
    }
}
if (!function_exists('showCategoryImage')) {
    function showCategoryImage($image_name)
    {
        return url(\App\Models\Categories::CAT_IMG_URL . $image_name);
    }
}

if (!function_exists('showUserProfileImage')) {
    function showUserProfileImage($image_name)
    {
        return url(\App\Models\User::IMG_URL . $image_name);
    }
}
function showAttachment($attachment)
{
    return url('storage/attachments/' . $attachment);
}


function getLanguageList($course_language)
{
    return $course_language = \App\Models\Language::get('name') . $course_language;
}


if (!function_exists('checkPassword')) {
    function CheckPassword($password)
    {
        return sha1(md5(base64_encode(trim($password))));
    }
}

function getMonthList()
{
    return [
        1 => "January",
        2 => "February",
        3 => "March",
        4 => "April",
        5 => "May",
        6 => "June",
        7 => "July",
        8 => "August",
        9 => "September",
        10 => "October",
        11 => "November",
        12 => "December",
    ];
}


function getVimeoExternalUrl($video_id)
{
    $result = null;

    if (!empty($video_id)) {
        $url="https://api.vimeo.com/videos/".$video_id."?access_token=ad0361be4750edabb13703ebd423af48&client_id=e3d28eb42d4e1ee16df47af3e59ab41cacaff5ec&fields=files";
        //echo $url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0); //use this to suppress output
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);// tell cURL to graciously accept an SSL certificate
        $fetch_result= curl_exec($ch);
        curl_close($ch);
        $result=json_decode($fetch_result,true);
        $result=@$result['files'];
    }
    return $result;
}

function getVimeoProgressiveUrls($video_id)
{
    $result = null;
    if (!empty($video_id)) {
        $url = "https://player.vimeo.com/video/".$video_id."/config";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0); //use this to suppress output
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// tell cURL to graciously accept an SSL certificate
        $fetch_result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($fetch_result, true);
        $result = @$result['request']['files']['progressive'];
    }
    return $result;
}


function pagination($total, $per_page = 10, $page = 1, $url = '?')
{
    $queries = array();
    parse_str($_SERVER['QUERY_STRING'], $queries);

    if ($queries) {
        unset($queries['page']);
        if (count($queries) > 0) {
            $url = '?' . http_build_query($queries) . '&';
        }

    }

    $adjacents = "2";
    $prevlabel = "Previous";
    $nextlabel = "Next";
    $lastlabel = "Last &rsaquo;&rsaquo;";

    $page = ($page == 0 ? 1 : $page);
    $start = ($page - 1) * $per_page;

    $prev = $page - 1;
    $next = $page + 1;

    //$lastpage = ceil($total/$per_page);

    $lastpage = $total;

    $lpm1 = $lastpage - 1; // //last page minus 1

    $pagination = "";

    if ($lastpage > 1) {

        $pagination .= "<div class='row'><div class='col-sm-3'>Page {$page} of {$lastpage}</div><div class='col-sm-9'>";
        $pagination .= "<ul class='pagination'>";


        if ($page > 1) $pagination .= "<li class=\"page-item\"><a class=\"page-link previous-link\" href='{$url}page={$prev}'>{$prevlabel}</a></li>";

        if ($lastpage < 7 + ($adjacents * 2)) {
            for ($counter = 1; $counter <= $lastpage; $counter++) {
                if ($counter == $page)
                    $pagination .= "<li class=\"page-item active\"><a class='page-link'>{$counter}</a></li>";
                else
                    $pagination .= "<li class=\"page-item\"><a class='page-link' href='{$url}page={$counter}'>{$counter}</a></li>";
            }

        } elseif ($lastpage > 5 + ($adjacents * 2)) {

            if ($page < 1 + ($adjacents * 2)) {

                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                    if ($counter == $page)
                        $pagination .= "<li class=\"page-item\"><a class=\"page-link active\">{$counter}</a></li>";
                    else
                        $pagination .= "<li class=\"page-item\"><a class='page-link' href='{$url}page={$counter}'>{$counter}</a></li>";
                }
                $pagination .= "<li class='dot'>...</li>";
                $pagination .= "<li class=\"page-item\"><a class='page-link' href='{$url}page={$lpm1}'>{$lpm1}</a></li>";
                $pagination .= "<li class=\"page-item\"><a class='page-link' href='{$url}page={$lastpage}'>{$lastpage}</a></li>";

            } elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {

                $pagination .= "<li class=\"page-item\"><a class='page-link' href='{$url}page=1'>1</a></li>";
                $pagination .= "<li class=\"page-item\"><a class='page-link' href='{$url}page=2'>2</a></li>";
                $pagination .= "<li class='dot'>...</li>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                    if ($counter == $page)
                        $pagination .= "<li class=\"page-item active\"><a class='page-link'>{$counter}</a></li>";
                    else
                        $pagination .= "<li class=\"page-item\"><a class='page-link' href='{$url}page={$counter}'>{$counter}</a></li>";
                }
                $pagination .= "<li class='dot'>..</li>";
                $pagination .= "<li class=\"page-item\"><a class='page-link' href='{$url}page={$lpm1}'>{$lpm1}</a></li>";
                $pagination .= "<li class=\"page-item\"><a class='page-link' href='{$url}page={$lastpage}'>{$lastpage}</a></li>";

            } else {

                $pagination .= "<li class=\"page-item\"><a class='page-link' href='{$url}page=1'>1</a></li>";
                $pagination .= "<li class=\"page-item\"><a class='page-link' href='{$url}page=2'>2</a></li>";
                $pagination .= "<li class='dot'>..</li>";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination .= "<li class=\"page-item active\"><a class='page-link'>{$counter}</a></li>";
                    else
                        $pagination .= "<li class=\"page-item\"><a class='page-link' href='{$url}page={$counter}'>{$counter}</a></li>";
                }
            }
        }

        if ($page < $counter - 1) {
            $pagination .= "<li class=\"page-item\"><a class='page-link next-link' href='{$url}page={$next}'>{$nextlabel}</a></li>";
//            $pagination.= "<li class=\"page-item\"><a class='page-link' href='{$url}page=$lastpage'>{$lastlabel}</a></li>";
        }

        $pagination .= "</div></ul></div>";
    }

    return $pagination;
}

function signUpFormArray($value)
{
    switch ($value) {
        case '1':
            return "<a href=\"javascript:;\" class='showTooltip'  data-toggle=\"tooltip\" data-title=\"Facebook Signup from Website\"><i class=\"fab fa-facebook-f\" aria-hidden=\"true\"><sub>
                <i class=\"fas fa-desktop\"></i>
            </sub></i></a>";
            break;
        case '2':
            return "<a href=\"javascript:;\" class='showTooltip'  data-toggle=\"tooltip\" data-title=\"Google Signup from Website\"><i class=\"fab fa-google\" aria-hidden=\"true\"><sub>
                <i class=\"fas fa-desktop\"></i>
            </sub></i></a>";
            break;
        case '3':
            return "<a href=\"javascript:;\" class='showTooltip'  data-toggle=\"tooltip\" data-title=\"Manual Signup from Website\"><i class=\"fas fa-sign-in-alt\" aria-hidden=\"true\"><sub>
                <i class=\"fas fa-desktop\"></i>
            </sub></i></a>";
            break;
        case '4':
            return "<a href=\"javascript:;\" class='showTooltip'  data-toggle=\"tooltip\" data-title=\"Manual Signup from Mobile App\"><i class=\"fas fa-sign-in-alt\" aria-hidden=\"true\"><sub>
                <i class=\"fas fa-mobile-alt\" aria-hidden=\"true\"></i>
            </sub></i></a>";
            break;
        case '5':
            return "<a href=\"javascript:;\" class='showTooltip'  data-toggle=\"tooltip\" data-title=\"Facebook Signup from Mobile App\"><i class=\"fab fa-facebook-f\" aria-hidden=\"true\"><sub>
                <i class=\"fas fa-mobile-alt\" aria-hidden=\"true\"></i>
            </sub></i></a>";
            break;
        case '6':
            return "<a href=\"javascript:;\" class='showTooltip'  data-toggle=\"tooltip\" data-title=\"Google Signup from Mobile App\"><i class=\"fab fa-google\" aria-hidden=\"true\"><sub>
                <i class=\"fas fa-mobile-alt\" aria-hidden=\"true\"></i>
            </sub></i></a>";
            break;
        case '7':
            return "<a href=\"javascript:;\" class='showTooltip'  data-toggle=\"tooltip\" data-title=\"Manual Signup from iPhone\"><i class=\"fas fa-sign-in-alt\" aria-hidden=\"true\"><sub>
                <i class=\"fab fa-apple\" aria-hidden=\"true\">
            </sub></i></a>";
            break;
        case '8':
            return "<a href=\"javascript:;\" class='showTooltip'  data-toggle=\"tooltip\" data-title=\"Facebook Signup from iPhone\"><i class=\"fab fa-facebook-f\" aria-hidden=\"true\"><sub>
                <i class=\"fab fa-apple\" aria-hidden=\"true\">
            </sub></i></a>";
            break;
        case '9':
            return "<a href=\"javascript:;\" class='showTooltip'  data-toggle=\"tooltip\" data-title=\"Google Signup from iPhone\"><i class=\"fab fa-google\" aria-hidden=\"true\"><sub>
                <i class=\"fab fa-apple\" aria-hidden=\"true\">
            </sub></i></a>";
            break;
        case '10':
            return "<a href=\"javascript:;\" class='showTooltip'  data-toggle=\"tooltip\" data-title=\"NSDC Signup\"><i class=\"fas fa-registered\"></i></a>";
            break;
        case '11':
            return "<a href=\"javascript:;\" class='showTooltip'  data-toggle=\"tooltip\" data-title=\"Manual Signup from Landing Page\"><i class=\"fas fa-sign-in-alt\" aria-hidden=\"true\"><sub>
                <i class=\"fas fa-desktop\"></i>
            </sub></i></a>";
            break;
        case '12':
            return "<a href=\"javascript:;\" class='showTooltip'  data-toggle=\"tooltip\" data-title=\"Google Signup from Landing Page\"><i class=\"fab fa-google\" aria-hidden=\"true\"><sub>
                <i class=\"fas fa-desktop\"></i>
            </sub></i></a>";
            break;
        case '13':
            return "<a href=\"javascript:;\" class='showTooltip'  data-toggle=\"tooltip\" data-title=\"Apple ID Signup Using Android\"><i class=\"fab fa-apple\" aria-hidden=\"true\"><sub>
                <i class=\"fas fa-mobile-alt\"></i>
            </sub></i></a>";
            break;
        case '14':
            return "<a href=\"javascript:;\" class='showTooltip'  data-toggle=\"tooltip\" data-title=\"Apple ID Signup Using Iphone\"><i class=\"fab fa-apple\" aria-hidden=\"true\"><sub>
                <i class=\"fab fa-apple\"></i>
            </sub></i></a>";
            break;
    }
}

function formatBooleanStatus($status)
{
    return $status == 1 ? "Yes" : "No";
}


function formatBooleanActiveStatus($status)
{
    return $status == 1 ? "<i class='fas fa-check-square text-success'></i>" : "<i class='fas fa-times-circle text-warning'></i>";
}

function formatPartnerWebsite($domain)
{
    return "https://" . $domain . "." . env('FRONTEND_APP_URL');
}

function getReferralCode()
{
    $code = substr(md5(uniqid(mt_rand(), true)), 0, 6);
    $codeExist = \App\Models\User::select('id', 'own_referral_code')->where('own_referral_code', $code)->first();
    if (isset($codeExist)) {
        return getReferralCode();
    }
    return $code;
}

function getUsername($str)
{
    $userNameExist = \App\Models\User::select('id', 'username', 'first_name', 'email')->where('username', $str)->first();
    if (isset($userNameExist)) {
        $newStr = $str . '-' . time();
        return $newStr;
    }
    return $str;
}

if (!function_exists('getObjectToArray')) {
    function getObjectToArray($result)
    {
        $result = array_map(function ($value) {
            return (array)$value;
        }, $result);
        return $result;
    }
}

function getUserIpDetails($ip)
{
    $detailsExist = \Illuminate\Support\Facades\DB::table('ip_details')->where('ip', $ip)->first();
    if (isset($detailsExist)){
        return json_decode($detailsExist->details, true);
    }
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://api.ipstack.com/" . $ip . "?access_key=" . env('IPSTACK_KEY'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_TIMEOUT => 30000,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        return ["error" => true, "message" => $err];
    }
    $response = json_decode($response, true);
    if (isset($response['ip'])){
        \Illuminate\Support\Facades\DB::table('ip_details')->insert([
            'ip' => $ip,
            'details' => json_encode($response),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
    }
    return $response;
}

if (!function_exists('formatUnitTime')) {
    function formatUnitTime($unit_time)
    {
        $unit_time_arr = explode(":", $unit_time);

        $hours = isset($unit_time_arr[0]) && ($unit_time_arr[0] != "00") ? $unit_time_arr[0] : null;
        $mins = isset($unit_time_arr[1]) && ($unit_time_arr[1] != "00") ? $unit_time_arr[1] : null;
        $seconds = isset($unit_time_arr[2]) && ($unit_time_arr[2] != "00") ? $unit_time_arr[2] : null;

        $time_string = null;
        if (!is_null($hours)) {
            $time_string .= $hours . "h ";
        }
        if (!is_null($mins)) {
            $time_string .= ltrim($mins, '0') . "m ";
        }
        if (!is_null($seconds)) {
            $time_string .= ltrim($seconds, '0') . "s";
        }
        return $time_string;
    }
}


function getVimeoVideoId($url)
{
    $url_parts = parse_url($url);
    $url_path =explode("/",$url_parts['path']);
    return $url_path[1];
}

function isJSON($string)
{
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}

function array_iunique($array) {
    return array_intersect_key($array, array_unique(array_map("strtolower", $array)));
}
