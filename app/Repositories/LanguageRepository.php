<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18-02-2021
 * Time: 11:14 AM
 */

namespace App\Repositories;


use App\Interfaces\LanguageRepositoryInterface;
use App\Models\CourseLanguage;
use App\Models\Language;

class LanguageRepository implements LanguageRepositoryInterface
{
    public function getAllLanguage()
    {
        return Language::orderBy('id','DESC')->get();
    }

    public function storeLanguageData($request)
    {
        $requestData = $request->except('_token');
        Language::create($requestData);
        return true;
    }

    public function getLanguageDetails($id)
    {
        return Language::where('id', $id)->first();
    }

    public function updateLanguage($request, $id)
    {
        $requestData = $request->except('_token');
        $language = self::getLanguageDetails($id);
         $language->update($requestData);
        return true;
    }

    public function getLanguageTitles()
    {

        return Language::pluck('name', 'name')->toArray();
    }

}
