<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:35 PM
 */

namespace App\Repositories;

use App\Events\PartnerApplicationApproved;
use App\Interfaces\PartnerApplicationRepositoryInterface;
use App\Mail\SendMailable;
use App\Models\Admins;
use App\Models\Course;
use App\Models\EmailTemplates;
use App\Models\Pages;
use App\Models\PartnerCourses;
use App\Models\PartnerRevenueFeesInfo;
use App\Models\Partners;
use App\Models\PartnersSettings;
use App\Models\SiteConfiguration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\Facades\DataTables;
use App\Services\EmailService;
use Carbon\Carbon;

class PartnerApplicationRepository implements PartnerApplicationRepositoryInterface
{
    protected $email_service;

    public function __construct(EmailService $emailService)
    {
        $this->email_service = $emailService;
    }

    public function getPartnersName()
    {
        return Partners::where('approve_website', 1)->where('setup_complete', 1)->pluck('name', 'id')->toArray();
    }

    public function getApplicationRequestList($request)
    {
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = Partners::where('id', '>', 1);

        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id', "DESC")->skip($start)->take($pageSize);

        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('name', function ($data) {
            return isset($data->name) ? $data->name : "- - -";
        });
        $data_tables->EditColumn('created_at', function ($data) {
            return formatDate($data->created_at);
        });
        $data_tables->EditColumn('email', function ($data) {
            return $data->contact_person_email ?? "- - -";
        });
        $data_tables->EditColumn('setup_fees', function ($data) {
            $str = "₹" . $data->setup_fees;
            $str .= ($data->setup_fees_paid_status == 1) ? "<span class='badge bg-info text-white ml-2'>Paid</span>" : "<span class='badge bg-danger text-white ml-2'>Unpaid</span>";
            return $str;

        });
        $data_tables->EditColumn('setup_complete', function ($data) {
            $str = $data->setup_complete ? "Yes" : "No";
            return $str;
        });

        $data_tables->EditColumn('action', function ($data) {
            return view('partner_app_request.action', compact('data'))->render();
        });

        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['action', 'setup_fees']);
        return $data_tables->make(true);
    }

    public function updateApplicationStatus($request, $id, $status)
    {
        $partners = Partners::find($id);
        $partners->partner_application_status = $status;
        $partners->save();
        if ($status == 1) {
            $status = "Approve";
        } elseif ($status == 2) {
            $status = "Reject";
        }
        $array = array(
            'NAME' => $partners->name,
            'STATUS' => $status
        );
        $email_template = 'partner_request_approve';
        $this->email_service->sendEmailToUser($partners->contact_person_email, $email_template, $array);

        return true;
    }

    public function showApplicationStatus($id)
    {
        return Partners::findOrFail($id);
    }

    public function getPartnerRevenueFees($id)
    {
        return PartnerRevenueFeesInfo::where('partner_id', $id)->get();
    }

    public function getPartnerCoursesId($id)
    {
        return PartnerCourses::where('partner_id', $id)->pluck('course_id')->toArray();
    }

    public function storePartnerRevenueInfoAndAccessibility($partnerId, $request)
    {
        DB::beginTransaction();
        try {
            $requestData = $request->except('_token');
            $partner = Partners::where("id", $partnerId)->first();
            if (isset($requestData['approve_website']) || ($partner->approve_website == 1)) {
                if (isset($requestData['approve_website'])) {
                    DB::table('admins')->insert([
                        'role_id' => 4,
                        'partner_id' => $partner->id,
                        'name' => $partner->name,
                        'email' => $partner->contact_person_email,
                        'password' => $partner->default_admin_password,
                        'status' => 1,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]);
                }
                /*UPDATE PARTNER*/
                $partnerUpdateData = [
                    'revenue_sharing_type' => $requestData['revenueSharingOption'],
                    'allow_all_courses' => isset($requestData['allow_all_courses']) ? 1 : 0,
                    'allow_all_future_courses' => isset($requestData['allow_all_future_courses']) ? 1 : 0,
                    'show_webinars' => isset($requestData['show_webinars']) ? 1 : 0,
                    'approve_website' => 1,
                    'is_other_payment_gateway' => $requestData['is_other_payment_gateway'],
                ];
                $partner->update($partnerUpdateData);

                /*STORE PARTNER COURSES*/
                PartnerCourses::where('partner_id', $partnerId)->delete();
                $courses = Course::whereNotNull('id')->where('course_type', '!=', 4);
                if ($partnerUpdateData['allow_all_courses'] == 0) {
                    $courses = $courses->whereIn('id', $requestData['courses']);
                }
                $courses = $courses->select('id', 'price')->get();
                foreach ($courses as $course) {
                    PartnerCourses::create([
                        'partner_id' => $partner->id,
                        'course_id' => $course->id,
                        'is_paid_course' => 0,
                        'course_fees' => $course->price,
                    ]);
                }
                if ($partnerUpdateData['show_webinars'] == 1) {
                    $webinars = Course::where('course_type', 4)->select('id', 'price')->get();
                    foreach ($webinars as $webinar) {
                        PartnerCourses::create([
                            'partner_id' => $partner->id,
                            'course_id' => $webinar->id,
                            'is_paid_course' => 0,
                            'course_fees' => 0,
                        ]);
                    }
                }

                /*STORE PartnerRevenueFeesInfo*/
                PartnerRevenueFeesInfo::where('partner_id', $partnerId)->delete();
                if ($partnerUpdateData['revenue_sharing_type'] == "sharingByCount") {
                    foreach ($requestData['revenue_from'] as $key => $datum) {
                        PartnerRevenueFeesInfo::create([
                            'partner_id' => $partner->id,
                            'revenue_fees_type' => $partnerUpdateData['revenue_sharing_type'],
                            'from_value' => $datum,
                            'to_value' => $requestData['revenue_to'][$key],
                            'revenue_share_value' => $requestData['revenue_value'][$key],
                            'revenue_share_type' => $requestData['revenue_type'][$key],
                        ]);
                    }
                } else {
                    PartnerRevenueFeesInfo::create([
                        'partner_id' => $partner->id,
                        'revenue_fees_type' => $partnerUpdateData['revenue_sharing_type'],
                        'revenue_share_value' => $requestData['fixed_revenue_value'],
                        'revenue_share_type' => $requestData['fixed_revenue_type']
                    ]);
                }

                /*STORE SITE CONFIGURATIONS*/
                $existData = SiteConfiguration::where('partner_id', $partner->id)->first();
                if (is_null($existData)) {
                    $siteConfigurationArray = SiteConfiguration::where('partner_id', 1)->where('is_replicable_for_partner', 1)->get()->toArray();
                    foreach ($siteConfigurationArray as $item) {
                        $item['is_replicable_for_partner'] = 0;
                        $item['partner_id'] = $partnerId;
                        if ($item['identifier'] == "email_address") {
                            $item['configuration_value'] = $partner->contact_person_email;
                        } elseif (in_array($item['identifier'], ["web_site", "canonical_url"])) {
                            $item['configuration_value'] = 'https://' . $partner->domain . '.' . env('FRONTEND_APP_URL') . '/';
                        } elseif ($item['identifier'] == "contact_no") {
                            $item['configuration_value'] = $partner->contact_person_phone_number;
                        } elseif ($item['identifier'] == "logo") {
                            $item['configuration_value'] = $partner->logo_image;
                        } elseif ($item['identifier'] == "sample_certificate") {
                            $item['configuration_value'] = $partner->certificate_image;
                        }elseif ($item['identifier'] == "footer_content") {
                            $item['configuration_value'] = $partner->description ?? $item['configuration_value'];
                        }
                        SiteConfiguration::create($item);
                    }

                    $pages = Pages::where('partner_id', 1)->whereIn('slug', ['about', 'privacy-policy-facebook', 'contact'])->get();
                    foreach ($pages as $page) {
                        Pages::create([
                            'partner_id' => $partner->id,
                            'meta_title' => $page->meta_title ?? null,
                            'page_type' => $page->page_type ?? null,
                            'page_title' => $page->page_title ?? null,
                            'slug' => $page->slug ?? null,
                            'meta_description' => $page->meta_description ?? null,
                            'meta_keywords' => $page->meta_keywords ?? null,
                            'schema_script' => $page->schema_script ?? null,
                            'canonical_url' => $page->canonical_url ?? null,
                            'content' => $page->content ?? null,
                            'is_active' => 1 ?? null,
                        ]);
                    }

                    $emailTemplates = EmailTemplates::where('partner_id', 1)->whereIn('identifier', ['reset_password', 'activation_link', 'payment_conformation_mail', 'contact_form_submission'])->get();
                    foreach ($emailTemplates as $emailTemplate) {
                        EmailTemplates::create([
                            'partner_id' => $partner->id,
                            'identifier' => $emailTemplate->identifier ?? null,
                            'title' => $emailTemplate->title ?? null,
                            'subject' => $emailTemplate->subject ?? null,
                            'content' => $emailTemplate->content ?? null,
                            'attachment' => $emailTemplate->attachment ?? null,
                            'is_active' => 1,
                        ]);
                    }
                }
                DB::commit();
            }
            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function storePartnerType($request)
    {
        $requestData = $request->except('_token');
        $id = $request->partner_user_id;
        //$updateData['partner_type'] = (isset($requestData['revenueSharingOption']) && $requestData['revenueSharingOption'] == 'fixedFees') ? 1 : 2;
        $updateData['setup_fees'] = $requestData['setup_fees'];
        $updateData['setup_fees_paid_status'] = ($requestData['setup_fees'] == 0) ? 1 : 0;
        $updateData['partner_application_status'] = 1;
        $updateData['allow_all_courses'] = isset($requestData['allow_all_courses']) ? 1 : 0;
        $updateData['allow_all_future_courses'] = isset($requestData['allow_all_future_courses']) ? 1 : 0;
        $updateData['allow_course_enrollment'] = isset($requestData['allow_course_enrollment']) ? 1 : 0;
        $updateData['redirect_for_enrollment'] = isset($requestData['redirect_for_enrollment']) ? 1 : 0;
        $updateData['show_webinars'] = isset($requestData['show_webinars']) ? 1 : 0;
        $updateData['use_own_certificate'] = isset($requestData['use_own_certificate']) ? 1 : 0;
        $updateData['partner_application_status'] = 1;
        Partners::where("id", $id)->update($updateData);
    }
}
