<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 03:04 PM
 */

namespace App\Repositories;

use App\Interfaces\CertificateRepositoryInterface;
use Carbon\Carbon;

use Yajra\DataTables\Facades\DataTables;
use DB;
use App\Models\Course;
use App\Models\CourseUser;
use App\Models\PaymentTransaction;
use App\Models\User;
use PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class CertificateRepository implements CertificateRepositoryInterface
{
    public function getCertificate($userId)
    {
        return CourseUser::whereHas('userDetail', function ($q) use ($userId){
            $q->where('user_id', $userId);
        })->where(function ($q){
            $q->where('paid_status', 1)->orWhere('is_applicable_for_free_certificate', 1);
        })->get();
    }

    public function downloadCertificate($request): array
    {
        $requestData = $request->except('_token');
        $course_id = $requestData['hdn_course_id'];
        $user_id = $requestData['hdn_user_id'];
        $user = User::findOrFail($user_id);

        $certificate_details = CourseUser::with(['userDetail' => function ($q) {
            $q->select('id', 'first_name', 'certificate_name', 'username', 'email', 'mobile_number', 'candidate_id', 'created_at');
        }, 'courseDetail' => function ($q) {
            $q->select('id', 'name','course_certificate_name','course_type', 'start_date');
        }])->where(function ($q) {
            $q->where('paid_status', 1)->orWhere('is_applicable_for_free_certificate', 1);
        })->where('course_id', $course_id)->where('user_id', $user->id)->first();
        if (!isset($certificate_details)) {
            return ['status' => false, 'message' => "User should be not completed the course!"];
        }
        if (!isset($certificate_details->certificate_date) || $certificate_details->certificate_date == '0000-00-00') {
            $certificate_details->certificate_date = date('Y-m-d');
        }
        if (($certificate_details->is_international_certificate == 1) && ($user->candidate_id == null)){
            $user->candidate_id = 'CAN_'.rand(10000000,99999999);
            $user->save();
        }
        $course_name = $certificate_details->courseDetail->course_certificate_name;
        if ($certificate_details->courseDetail->course_type == 4) {
            if (is_null($certificate_details->certificate_id)){
                $certificate_details->certificate_id = 'LV'.$certificate_details->id;
            }
            $certificate_details->save();
            $html = view('user.certificate.learnvern_certificate', compact('certificate_details'))->render();
            $pdf = PDF::loadHtml($html);
            $pdf->setPaper('A4', 'landscape');
        } else {
            $candidate_id = $user->candidate_id;
            if (is_null($certificate_details->certificate_id)){
                $certificate_details->certificate_id = $candidate_id.$certificate_details->id;
            }
            $certificate_details->save();
            $codeContents = env('FRONT_URL') . 'certificate?candidate_id=' . $candidate_id . '&certificate_id=' . $certificate_details->certificate_id;
            $qr_code = QrCode::format('png')->size(100)->generate($codeContents);
            $html = view('user.certificate.nsdc_certificate', compact('qr_code', 'certificate_details', 'candidate_id', 'course_name'))->render();
            $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
            $pdf = PDF::loadHtml($html);
            $pdf->setPaper('A4', 'landscape');
        }
        $certificate_name = 'Certificate_' . $course_name;
        return ['status' => true, 'pdf' => $pdf, 'certificate_name' => $certificate_name . '.pdf'];
    }
}
