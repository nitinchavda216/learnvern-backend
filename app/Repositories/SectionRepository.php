<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:21 PM
 */

namespace App\Repositories;

use App\Events\UpdateCourseTimeEvent;
use App\Interfaces\SectionRepositoryInterface;
use App\Models\Curriculum;
use App\Models\Section;
use App\Models\Course;
use Yajra\DataTables\Facades\DataTables;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as Writer;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Illuminate\Support\Facades\DB;

class SectionRepository implements SectionRepositoryInterface
{
    public function getAjaxListData($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = Curriculum::where('type', 'section')->whereDoesntHave('getMasterSectionChildData')
            /*->where('hide_from_backend', $requestData['hide_from_backend'])*/;
        if (isset($requestData['course_id'])) {
            $dataInput = $dataInput->where('course_id', $requestData['course_id']);
        }
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id')->skip($start)->take($pageSize);
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('id', function ($section) {
            return isset($section->id) ? $section->id : "- - -";
        });
        $data_tables->EditColumn('course_name', function ($section) {
            return isset($section->courseDetail) ? $section->courseDetail->name : "- - -";
        });/*
        $data_tables->EditColumn('hide_status', function ($item) {
            return view('courses.partials.curriculum_hide_status', compact('item'))->render();
        });*/
        $data_tables->EditColumn('action', function ($section) {
            return view('section.partials.actions', compact('section'))->render();
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['action', 'hide_status']);
        return $data_tables->make(true);
    }

    public function storeSectionData($request): bool
    {
        $requestData = $request->except("_token");
        foreach ($requestData['sections'] as $section) {
            if (isset($section)) {
                $data = [
                    'course_id' => $requestData['course_id'],
                    'name' => $section,
                ];
                $sectionData = Section::create($data);
                Curriculum::create([
                    'course_id' => $requestData['course_id'],
                    'section_id' => $sectionData->id,
                    'curriculum_list_id' => $sectionData->id,
                    'name' => $sectionData->name,
                    'type' => 'section',
                    'parent_section_id' => $requestData['parent_section_id'] ?? null
                ]);
            }
        }
        return true;
    }

    public function getSectionDetail($id)
    {
        return Section::findOrFail($id);
    }

    public function updateSectionData($request, $id): bool
    {
        $requestData = $request->all();
        $section = self::getSectionDetail($id);
        $section->update($requestData);
        $existData = Curriculum::where('type', 'section')->where('curriculum_list_id', $id)->first();
        if (isset($existData)) {
            Curriculum::where('type', 'section')->where('curriculum_list_id', $id)->update(['name' => $section->name]);
        } else {
            Curriculum::create([
                'course_id' => $section->course_id,
                'section_id' => $section->id,
                'curriculum_list_id' => $section->id,
                'name' => $section->name,
                'type' => 'section',
                'parent_section_id' => $requestData['parent_section_id'] ?? null
            ]);
        }
        return true;
    }

    public function updateActiveStatus($request): bool
    {
        $curriculum = Curriculum::where('id', $request->id)->first();
        $section = Section::find($curriculum->section_id);
        $section->is_active = $request->is_active;
        $section->save();
        Curriculum::where('curriculum_list_id', $section->id)->where('type', 'section')->update(['is_active' => $request->is_active]);
        if ($request->is_active == 0){
            foreach ($section->relatedUnits as $unit) {
                $unit->update(['is_active' => 0]);
            }
            foreach ($section->relatedAssignments as $assignment) {
                $assignment->update(['is_active' => 0]);
            }
            foreach ($section->relatedQuiz as $quiz) {
                $quiz->update(['is_active' => 0]);
            }
            Curriculum::where('section_id', $section->id)->where('type', '!=', 'section')->update(['is_active' => 0]);
        }
        $courseList = Curriculum::where('curriculum_list_id', $section->id)->where('type', 'section')->pluck('course_id')->toArray();
        event(new UpdateCourseTimeEvent(['course_id' => $courseList]));
        return true;
    }

    public function getParentSections($request)
    {
        $requestData = $request->all();
        return Curriculum::join('courses as c', function ($join) {
            $join->on('c.id', '=', 'curriculum.course_id')->where('is_career_plan_course', 1);
        })->where('type', 'section')->whereNull('parent_section_id')
            ->where('course_id', $requestData['course_id'])->pluck('curriculum.name', 'section_id')->toArray();
    }

    public function getSectionTitles($course_id = null)
    {
        $sections = Section::select(DB::Raw('CONCAT(name," - ",IF(is_active =1,"(Active)","(Inactive)")) as title'), 'id')->orderBy('is_active', 'DESC');
        if (isset($course_id)) {
            $sections = $sections->where('course_id', $course_id);
        }
        $sections = $sections->get();
        return $sections->pluck('title', 'id')->toArray();
    }

    public function getSectionsByCourse($request): array
    {
        $requestData = $request->except("_token");
        $returnData = [];
        if (isset($requestData['courseId'])) {
            $courseDetails = Course::select('is_career_plan_course')->where('id', $requestData['courseId'])->first();
            $sections = Curriculum::where('course_id', $requestData['courseId'])->where('type', 'section');
            if (isset($requestData['searchTerm'])) {
                $sections = $sections->where('name', "Like", "%" . $requestData['searchTerm'] . "%");
            }
            if ($courseDetails->is_career_plan_course == 1){
                $sections = $sections->where('parent_section_id', "!=", null);
            }
            $sections = $sections->orderBy('is_active', 'DESC')->orderBy('sort_order')->get();
            foreach ($sections as $section) {
                $status =  ($section->is_active == 1) ? "Active" : "Inactive";
                $returnData[] = ['id' => $section->section_id, 'text' => $section->name.' - ('.$status.')'];
            }
        }
        return $returnData;
    }

    public function getAjaxExportData($request)
    {
        $requestData = $request->all();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $predefinedColumn = ['Course Id', 'Section'];
        $alpha = 'A';
        foreach ($predefinedColumn as $value) {
            $sheet->setCellValue($alpha . '1', $value);
            $sheet->getColumnDimension($alpha)->setAutoSize(true);
            $alpha++;
        }
        $sheet->setCellValue('A2', $requestData['course_id']);
        $writer = new Writer($spreadsheet);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Section_Sample.xlsx"');
        $writer->save("php://output");
    }

    public function importUpdateUnit($request)
    {
        DB::beginTransaction();
        try {
            $requestData = $request->all();
            $reader = new Xlsx();
            $spreadsheet = $reader->load($requestData['file']);
            $sheetData = $spreadsheet->getActiveSheet()->toArray();
            if (count($sheetData) > 0) {
                unset($sheetData[0]);
                foreach ($sheetData as $sheetDatum) {
                    if ($sheetDatum[0] != "") {
                        $courseExist = Course::where('id', $sheetDatum[0])->first();
                        if (isset($courseExist)) {
                            $sectionData = Section::create([
                                'course_id' => $sheetDatum[0],
                                'name' => $sheetDatum[1],
                                'is_active' => 0,
                            ]);
                            Curriculum::create([
                                'course_id' => $sheetDatum[0],
                                'section_id' => $sectionData->id,
                                'curriculum_list_id' => $sectionData->id,
                                'name' => $sectionData->name,
                                'type' => 'section'
                            ]);
                        }
                    }
                }
            }
            DB::commit();
            return ['status' => true, 'message' => 'Section imported successfully.'];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }
}
