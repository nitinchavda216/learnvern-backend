<?php

namespace App\Repositories;

use App\Interfaces\ReportBuilderRepositoryInterface;
use App\Models\Categories;
use App\Models\City;
use App\Models\Colleges;
use App\Models\Course;
use App\Models\Reports;
use App\Models\State;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class ReportBuilderRepository implements ReportBuilderRepositoryInterface
{
    var $all_where_fields = array();

    var $_tables;
    var $_fields;
    var $sort_by;
    var $sort_order;
    var $query = '';
    var $headers = array();
    var $_join = array();
    var $_whereTable = [];

    public function __construct()
    {
        $this->_tables = Reports::TABLE_LIST_NAME;
        $this->_fields = Reports::COLUMN_LIST_NAME;
    }

    public function validateReport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return ['status' => false, 'result' => $validator];
        }
        return [];
    }

    public function storeReports(Request $request)
    {
        $requestData = $request->all();
        $requestData['fields'] = json_encode($requestData['selectField']);
        $requestData['conditions'] = json_encode($requestData['conditions']);
        return Reports::create($requestData);
    }

    public function storeNewReports(Request $request)
    {
        $requestData = $request->all();
        return Reports::create($requestData);
    }

    public function updateReports(Request $request, $report_id)
    {
        $requestData = $request->all();
        $updateData['name'] = $requestData['name'];
        $updateData['fields'] = json_encode($requestData['selectField']);
        $updateData['conditions'] = json_encode($requestData['conditions']);
        Reports::where("id", $report_id)->update($updateData);
        return true;
    }

    public function getReportData($id)
    {
        $report = Reports::findOrFail($id);
        return $report;
    }

    public function deleteReport($id)
    {
        Reports::destroy($id);
        return true;
    }

    public function getAllReports()
    {
        $reports = Reports::select('id', 'name')->orderBy('id', 'DESC')->get();
        return $reports;
    }

    public function getConstantDataDetail($requestData = [])
    {
        $element = isset($requestData['element_type']) ? $requestData['element_type'] : null;
        $result = [];
        if ($element == 'courses' || !(isset($element))) {
            $result['course_list'] = Course::ByActive()->where('course_type', '!=', 4)->pluck('name', 'id')->toArray();
        }
        if ($element == 'webinars' || !(isset($element))) {
            $result['webinar_list'] = Course::ByActive()->where('course_type', 4)->pluck('name', 'id')->toArray();
        }
        if ($element == 'category' || !(isset($element))) {
            $result['category_list'] = Categories::ByActive()->pluck('name', 'id')->toArray();
        }
        if ($element == 'enroll_type' || !(isset($element))) {
            $result['enroll_type_list'] = ['1' => 'Yes', '0' => 'No'];
        }
        if ($element == 'from_nsdc' || !(isset($element))) {
            $result['from_nsdc_type_list'] = ['1' => 'Yes', '0' => 'No'];
        }
        if ($element == 'current_occupation' || !(isset($element))) {
            $result['current_occupation_list'] = ['Fresher' => 'Fresher', 'Experience' => 'Experience'];
        }
        if ($element == 'colleges' || !(isset($element))) {
            $result['colleges_list'] = Colleges::pluck('title', 'id')->toArray();
        }
        if ($element == 'key_skills' || !(isset($element))) {
            $result['key_skills_list'] = DB::table('user_key_skills')->groupBy('title')->pluck('title', 'title')->toArray();
        }
        if ($element == 'technical_skills' || !(isset($element))) {
            $result['technical_skills_list'] = DB::table('user_technical_skills')->groupBy('title')->pluck('title', 'title')->toArray();
        }
        if ($element == 'qualification' || !(isset($element))) {
            $result['qualification_list'] = config('info.education_list');
        }

        return $result;
    }

    public function getEditConstantDataDetail($requestData = [])
    {
        $result = [];
        $result['course_list'] = Course::ByActive()->where('course_type', '!=', 4)->pluck('name', 'id')->toArray();
        $result['webinar_list'] = Course::ByActive()->where('course_type', 4)->pluck('name', 'id')->toArray();
        $result['category_list'] = Categories::ByActive()->pluck('name', 'id')->toArray();
        $result['enroll_type_list'] = ['1' => 'Yes', '0' => 'No'];
        $result['from_nsdc_type_list'] = ['1' => 'Yes', '0' => 'No'];
        $result['current_occupation_list'] = ['Fresher' => 'Fresher', 'Experience' => 'Experience'];
        $result['colleges_list'] = Colleges::pluck('title', 'id')->toArray();
        $result['key_skills_list'] = DB::table('user_key_skills')->groupBy('title')->pluck('title', 'title')->toArray();
        $result['technical_skills_list'] = DB::table('user_technical_skills')->groupBy('title')->pluck('title', 'title')->toArray();
        $result['qualification_list'] = config('info.education_list');
        return $result;
    }

    public function getConstantData()
    {
        $columnList = $this->_fields;
        $fieldArray = array();
        foreach ($columnList as $key => $item) {
            $fieldArray[$item['display_group']][$key] = $item;
        }
        $reportsData['fieldArray'] = $fieldArray;
        $reportsData['timeArray'] = Reports::TIME_OPTIONS;
        $reportsData['element_type'] = Reports::ELEMENT_TYPE;
        $reportsData['identifier'] = Reports::IDENTIFIER;
        $reportsData['attribute_identifier'] = Reports::ATTRIBUTE_IDENTIFIER;
        $reportsData['comparison_identifier'] = Reports::COMPARISON_IDENTIFIER;
        return $reportsData;
    }


    public function getSqlColumn($field_name, $col_suffix = '')
    {
        $fieldList = Reports::COLUMN_LIST_NAME;
        $this_field_info = $fieldList[$field_name];
        if (isset($this_field_info['sql_column'])) {
            $sql_column = $this_field_info['sql_column'];
        }
        if ((!isset($sql_column) || empty($sql_column)) && $this_field_info['column'] == "slug") {
            $sql_column = "CONCAT('https://www.learnvern.com/',`courses`.slug,'?ref=',users.own_referral_code)";
        } else if (!isset($sql_column) || empty($sql_column)) {
            $sql_column = '`' . $this_field_info['table'] . '`.' . $this_field_info['column'];
        }
        $sql_column .= $col_suffix;
        if (!empty($this_field_info['function'])) {
            $sql_column = $this_field_info['function'] . '(' . $sql_column . ')';
        }
        return $sql_column;
    }


    public function date_condition($field_name, $operator, $from_date = "", $to_date = "")
    {
        switch ($operator) {
            case 'Between':
                $where = " (" . $field_name . " BETWEEN  '" . $from_date . " 00:00:00' AND '" . $to_date . " 23:59:59')";
                break;
            case 'today':
                $where = " (" . $field_name . " BETWEEN  '" . date("Y-m-d") . " 00:00:00' AND '" . date("Y-m-d") . " 23:59:59')";
                break;
            case 'yesterday':
                $yesterday = date("Y-m-d", strtotime('-1 days'));
                $where = " (" . $field_name . " BETWEEN  '" . $yesterday . " 00:00:00' AND '" . $yesterday . " 23:59:59')";
                break;
            case 'l2day':
                $day_start = date('Y-m-d 00:00:00', strtotime("-2 days"));
                $day_end = date('Y-m-d 00:00:00');
                $where = " (" . $field_name . " BETWEEN  '" . $day_start . "' AND '" . $day_end . "')";
                break;
            case 'week':
                $week_start = date("Y-m-d 00:00:00", strtotime(date('o-\\WW')));
                $week_end = date('Y-m-d 23:59:59', strtotime('+' . (6 - (date('w') - 1)) . ' days'));
                $where = " (" . $field_name . " BETWEEN  '" . $week_start . "' AND '" . $week_end . "')";
                break;
            case 'month':
                $month_start = date('Y-m-01 00:00:00');
                $month_end = date('Y-m') . "-" . cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y')) . " 23:59:59";
                $where = " (" . $field_name . " BETWEEN  '" . $month_start . "' AND '" . $month_end . "')";
                break;
            case 'year':
                $year_start = date('Y-01-01 00:00:00');
                $year_end = date('Y-12-31 23:59:59');
                $where = " (" . $field_name . " BETWEEN  '" . $year_start . "' AND '" . $year_end . "')";
                break;
            case 'lweek':
                $d = new \DateTime();
                $weekday = $d->format('w');
                $diff = 7 + ($weekday == 0 ? 6 : $weekday - 1); // Monday=0, Sunday=6
                $d->modify("-$diff day");
                $last_week_start = $d->format('Y-m-d');
                $d->modify('+6 day');
                $last_week_end = $d->format('Y-m-d');

                $where = " (" . $field_name . " BETWEEN  '" . $last_week_start . " 00:00:00' AND '" . $last_week_end . " 23:59:59')";
                break;
            case 'l2week':
                $d = new \DateTime();
                $weekday = $d->format('w');
                $diff = 14 + ($weekday == 0 ? 6 : $weekday - 1); // Monday=0, Sunday=6
                $d->modify("-$diff day");
                $last_week_start = $d->format('Y-m-d');
                $d->modify('+6 day');
                $last_week_end = $d->format('Y-m-d');
                $where = " (" . $field_name . " >= '" . $last_week_start . " 00:00:00')";
                break;

            case 'lmonth':
                $last_month_start = date('Y-m-01 00:00:00', strtotime("-1 month"));
                $last_month_end = date('Y-m', strtotime("-1 month")) . "-" . cal_days_in_month(CAL_GREGORIAN, date('m', strtotime("-1 month")), date('Y')) . " 23:59:59";
                $where = " (" . $field_name . " BETWEEN  '" . $last_month_start . "' AND '" . $last_month_end . "')";
                break;
            case 'l7days':
                $day_start = date('Y-m-d 00:00:00', strtotime("-7 days"));
                $where = " (" . $field_name . " >=  '" . $day_start . "')";
                break;
            case 'l30days':
                $day_start = date('Y-m-d 00:00:00', strtotime("-30 days"));
                $where = " (" . $field_name . " >=  '" . $day_start . "')";
                break;
            case 'l90days':
                $day_start = date('Y-m-d 00:00:00', strtotime("-90 days"));
                $where = " (" . $field_name . " >=  '" . $day_start . "')";
                break;
            case 'lyear':
                $last_year_start = date('Y-01-01 00:00:00', strtotime("-1 year"));
                $last_year_end = date('Y-12-31 23:59:59', strtotime("-1 year"));
                $where = " (" . $field_name . " BETWEEN  '" . $last_year_start . "' AND '" . $last_year_end . "')";
                break;
        }
        return " AND" . $where;
    }


    public function getWhere($conditionArray)
    {
        $where_str = "";

        $webinar_search_exist = $course_search_exist = false;
        foreach ($conditionArray as $cvalue) {
            if (isset($cvalue['element_type']) && ($cvalue['element_type'] == "courses")) {
                $course_search_exist = true;
                if (!in_array('courses', $this->_whereTable)) {
                    $this->_whereTable[] = "course_user";
                }

                if ($cvalue['identifier'] == "is" || $cvalue['identifier'] == "is_one_of") {
                    $where_str .= " AND course_user.course_id IN(" . implode(',', array_values($cvalue['values'])) . ")";
                }

                if ($cvalue['identifier'] == "is_not" || $cvalue['identifier'] == "is_not_one_of") {
                    $where_str .= " AND course_user.course_id NOT IN(" . implode(',', array_values($cvalue['values'])) . ")";
                }
            }
            if (isset($cvalue['element_type']) && ($cvalue['element_type'] == "webinars")) {
                $webinar_search_exist = true;
                if (!in_array('courses', $this->_whereTable)) {
                    $this->_whereTable[] = "course_user";
                }

                if ($cvalue['identifier'] == "is" || $cvalue['identifier'] == "is_one_of") {
                    $where_str .= " AND course_user.course_id IN(" . implode(',', array_values($cvalue['values'])) . ")";
                }

                if ($cvalue['identifier'] == "is_not" || $cvalue['identifier'] == "is_not_one_of") {
                    $where_str .= " AND course_user.course_id NOT IN(" . implode(',', array_values($cvalue['values'])) . ")";
                }
            }

            if (isset($cvalue['element_type']) && ($cvalue['element_type'] == "colleges")) {
                if (!in_array('user_college_details', $this->_whereTable)) {
                    $this->_whereTable[] = "user_college_details";
                }

                if ($cvalue['identifier'] == "is" || $cvalue['identifier'] == "is_one_of") {
                    $where_str .= " AND user_college_details.college_id IN(" . implode(',', array_values($cvalue['values'])) . ")";
                }

                if ($cvalue['identifier'] == "is_not" || $cvalue['identifier'] == "is_not_one_of") {
                    $where_str .= " AND user_college_details.college_id NOT IN(" . implode(',', array_values($cvalue['values'])) . ")";
                }
            }


            if (isset($cvalue['element_type']) && ($cvalue['element_type'] == "qualification")) {
                if (!in_array('user_college_details', $this->_whereTable)) {
                    $this->_whereTable[] = "user_college_details";
                }

                $qualification = '';
                foreach (array_values($cvalue['values']) as $item) {
                    $qualification .= ", '" . $item . "'";
                }
                $qualification = substr($qualification, 2);

                if ($cvalue['identifier'] == "is" || $cvalue['identifier'] == "is_one_of") {
                    $where_str .= " AND user_college_details.education_title IN(" . $qualification . ")";
                }

                if ($cvalue['identifier'] == "is_not" || $cvalue['identifier'] == "is_not_one_of") {
                    $where_str .= " AND user_college_details.education_title NOT IN(" . $qualification . ")";
                }
            }

            if (isset($cvalue['element_type']) && ($cvalue['element_type'] == "key_skills")) {
                if (!in_array('user_key_skills', $this->_whereTable)) {
                    $this->_whereTable[] = "user_key_skills";
                }

                $keyskill = '';
                foreach (array_values($cvalue['values']) as $item) {
                    $keyskill .= ", '" . $item . "'";
                }
                $keyskill = substr($keyskill, 2);

                if ($cvalue['identifier'] == "is" || $cvalue['identifier'] == "is_one_of") {
                    $where_str .= " AND user_key_skills.title IN(" . $keyskill . ")";
                }

                if ($cvalue['identifier'] == "is_not" || $cvalue['identifier'] == "is_not_one_of") {
                    $where_str .= " AND user_key_skills.title NOT IN(" . $keyskill . ")";
                }
            }

            if (isset($cvalue['element_type']) && ($cvalue['element_type'] == "technical_skills")) {
                if (!in_array('user_technical_skills', $this->_whereTable)) {
                    $this->_whereTable[] = "user_technical_skills";
                }

                $technical_keyskill = '';
                foreach (array_values($cvalue['values']) as $item) {
                    $technical_keyskill .= ", '" . $item . "'";
                }
                $technical_keyskill = substr($technical_keyskill, 2);

                if ($cvalue['identifier'] == "is" || $cvalue['identifier'] == "is_one_of") {
                    $where_str .= " AND user_technical_skills.title IN(" . $technical_keyskill . ")";
                }

                if ($cvalue['identifier'] == "is_not" || $cvalue['identifier'] == "is_not_one_of") {
                    $where_str .= " AND user_technical_skills.title NOT IN(" . $technical_keyskill . ")";
                }
            }


            if (isset($cvalue['element_type']) && ($cvalue['element_type'] == "enroll_type")) {
                if (!in_array('course_user', $this->_whereTable)) {
                    $this->_whereTable[] = "course_user";
                }
                $where_str .= " AND course_user.paid_status =" . $cvalue['values'][0];
            }

            if (isset($cvalue['element_type']) && ($cvalue['element_type'] == "from_nsdc")) {
                if (!in_array('course_user', $this->_whereTable)) {
                    $this->_whereTable[] = "course_user";
                }

                $where_str .= " AND course_user.is_from_nsdc =" . $cvalue['values'][0];
            }


            if (isset($cvalue['element_type']) && ($cvalue['element_type'] == "current_occupation")) {
                if (!in_array('users', $this->_whereTable)) {
                    $this->_whereTable[] = "users";
                }

                $where_str .= " AND users.current_occupation LIKE '%" . $cvalue['values'][0] . "%'";
            }


            if (isset($cvalue['element_type']) && ($cvalue['element_type'] == "category")) {
                if (!in_array('courses', $this->_whereTable)) {
                    $this->_whereTable[] = "courses";
                }
                if ($cvalue['identifier'] == "is" || $cvalue['identifier'] == "is_one_of") {
                    $where_str .= " AND courses.category_id IN(" . implode(',', array_values($cvalue['values'])) . ")";
                }
                if ($cvalue['identifier'] == "is_not" || $cvalue['identifier'] == "is_not_one_of") {
                    $where_str .= " AND courses.category_id NOT IN(" . implode(',', array_values($cvalue['values'])) . ")";
                }
            }
            if (isset($cvalue['timeLimit']) && $cvalue['timeLimit'] != "") {
                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d');
                $dateField = "course_user.course_enroll_date";
                if (($cvalue['timeLimit'] == "Between") && isset($cvalue['dateRange'])) {
                    $data_list = (explode(' - ', $cvalue['dateRange']));
                    $startDate = date('Y-m-d', strtotime($data_list[0]));
                    $endDate = date('Y-m-d', strtotime($data_list[1]));
                }
                $operatorTime = $cvalue['timeLimit'];
                $where_str .= $this->date_condition($dateField, $operatorTime, $startDate, $endDate);
            }
            if (isset($cvalue['registration_timeLimit']) && $cvalue['registration_timeLimit'] != "") {
                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d');
                $dateField = "DATE(users.created_at)";

                if (($cvalue['registration_timeLimit'] == "Between") && isset($cvalue['registration_dateRange'])) {
                    $data_list = (explode(' - ', $cvalue['registration_dateRange']));
                    $startDate = date('Y-m-d', strtotime($data_list[0]));
                    $endDate = date('Y-m-d', strtotime($data_list[1]));
                }
                $operatorTime = $cvalue['registration_timeLimit'];
                $where_str .= $this->date_condition($dateField, $operatorTime, $startDate, $endDate);
            }
            if (isset($cvalue['lastlogin_timeLimit']) && $cvalue['lastlogin_timeLimit'] != "") {
                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d');
                $dateField = "DATE(users.last_login_date)";

                if (($cvalue['lastlogin_timeLimit'] == "Between") && isset($cvalue['lastlogin_dateRange'])) {
                    $data_list = (explode(' - ', $cvalue['lastlogin_dateRange']));
                    $startDate = date('Y-m-d', strtotime($data_list[0]));
                    $endDate = date('Y-m-d', strtotime($data_list[1]));
                }
                $operatorTime = $cvalue['lastlogin_timeLimit'];
                $where_str .= $this->date_condition($dateField, $operatorTime, $startDate, $endDate);
            }


            if (isset($cvalue['payment_timeLimit']) && $cvalue['payment_timeLimit'] != "") {
                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d');
                $dateField = "course_user.payment_completed_date";
                if (($cvalue['payment_timeLimit'] == "Between") && isset($cvalue['payment_dateRange'])) {
                    $data_list = (explode(' - ', $cvalue['payment_dateRange']));
                    $startDate = date('Y-m-d', strtotime($data_list[0]));
                    $endDate = date('Y-m-d', strtotime($data_list[1]));
                }
                $operatorTime = $cvalue['payment_timeLimit'];
                $where_str .= $this->date_condition($dateField, $operatorTime, $startDate, $endDate);
            }

            if (isset($cvalue['progress_timeLimit']) && ($cvalue['progress_timeLimit'] != "")) {
                if (!in_array('course_progress_log', $this->_whereTable)) {
                    $this->_whereTable[] = "course_progress_log";
                }

                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d');
                $dateField = "DATE(course_progress_log.last_date_of_progress)";
                if (($cvalue['progress_timeLimit'] == "Between") && isset($cvalue['progress_dateRange'])) {
                    $data_list = (explode(' - ', $cvalue['progress_dateRange']));
                    $startDate = date('Y-m-d', strtotime($data_list[0]));
                    $endDate = date('Y-m-d', strtotime($data_list[1]));
                }
                $operatorTime = $cvalue['progress_timeLimit'];
                $where_str .= $this->date_condition($dateField, $operatorTime, $startDate, $endDate);
            }


            if (isset($cvalue['progress']) && $cvalue['progress'] != "0-100") {
                $data_list = explode('-', $cvalue['progress']);
                $minRange = $data_list[0];
                $maxRange = $data_list[1];
                $where_str .= " AND ROUND(progress) >= $minRange AND ROUND(progress) <=$maxRange";
            }
            if (isset($cvalue['referral_range']) && $cvalue['referral_range'] != "0-500") {

                if ($cvalue['referral_range'] == '0-0') {
                    $where_str .= " AND (users.referred_count IS NULL || users.referred_count < 1)";
                } else {
                    $data_list = explode('-', $cvalue['referral_range']);
                    $minRange = $data_list[0];
                    $maxRange = $data_list[1];
                    $where_str .= " AND users.referred_count >= $minRange AND users.referred_count <=$maxRange";
                }

            }
            if (isset($cvalue['state_id']) && $cvalue['state_id'] != "") {
                $state = State::where('id', $cvalue['state_id'])->first();
                $where_str .= " AND users.state LIKE '%" . $state['name'] . "%'";
            }
            if (isset($cvalue['city_id']) && $cvalue['city_id'] != "") {
                $city = City::where('id', $cvalue['city_id'])->first();
                $where_str .= " AND users.city LIKE '%" . $city['name'] . "%'";
            }

            if (isset($cvalue['ambassador_badge_id']) && $cvalue['ambassador_badge_id'] != '') {

                if (!in_array('ambassador_badges', $this->_whereTable)) {
                    $this->_whereTable[] = "ambassador_badges";
                }
                $where_str .= " AND ambassador_badges.badge_name LIKE '%" . $cvalue['ambassador_badge_id'] . "%'";
            }

            if (isset($cvalue['ambassador_timeLimit']) && ($cvalue['ambassador_timeLimit'] != "")) {
                if (!in_array('ambassador_badges', $this->_whereTable)) {
                    $this->_whereTable[] = "ambassador_badges";
                }

                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d');
                $dateField = "DATE(ambassador_badges.last_badge_date)";
                if (($cvalue['ambassador_timeLimit'] == "Between") && isset($cvalue['ambassador_dateRange'])) {
                    $data_list = (explode(' - ', $cvalue['ambassador_dateRange']));
                    $startDate = date('Y-m-d', strtotime($data_list[0]));
                    $endDate = date('Y-m-d', strtotime($data_list[1]));
                }
                $operatorTime = $cvalue['ambassador_timeLimit'];
                $where_str .= $this->date_condition($dateField, $operatorTime, $startDate, $endDate);
            }

            if (isset($cvalue['abandoned_timeLimit']) && ($cvalue['abandoned_timeLimit'] != "")) {
                if (!in_array('abandoned_payment_reports', $this->_whereTable)) {
                    $this->_whereTable[] = "abandoned_payment_reports";
                }

                $startDate = date('Y-m-d');
                $endDate = date('Y-m-d');
                $dateField = "DATE(abandoned_payment_reports.created_at)";
                if (($cvalue['abandoned_timeLimit'] == "Between") && isset($cvalue['abandoned_dateRange'])) {
                    $data_list = (explode(' - ', $cvalue['abandoned_dateRange']));
                    $startDate = date('Y-m-d', strtotime($data_list[0]));
                    $endDate = date('Y-m-d', strtotime($data_list[1]));
                }
                $operatorTime = $cvalue['abandoned_timeLimit'];
                $where_str .= $this->date_condition($dateField, $operatorTime, $startDate, $endDate);
            }
        }
        if ($webinar_search_exist && $course_search_exist) {
            $where_str .= "";
        } elseif ($webinar_search_exist) {
            $where_str .= " AND courses.course_type=4";
        } else {
            $where_str .= " AND courses.course_type!=4";
        }
        return $where_str;
    }


    public function campaignBuildQuery($requestData, $campaign_id, $conditionApply)
    {

        $conditionArray = $requestData['conditions'];
        $selected_fields = $requestData['fields'];
        $tables = array("course_user", "courses");
        $sql_columns = array();
        $group_by = array('course_user.user_id');
        $total_columns = array();
        $where_str = "1=1";
        if ($conditionApply) {
            $where_str .= $conditionApply;
        }

        foreach ($selected_fields as $field_name) {
            $this_field_info = $this->_fields[$field_name];
            if (!isset($this_field_info)) {
                continue;
            }
            $sql_column = $this->getSqlColumn($field_name);
            $sql_columns[$field_name] = $sql_column;
            if (isset($this_field_info['group_by']) && $this_field_info['group_by']) {
                $group_by[] = '`' . $field_name . '`';
            } else {
                $total_columns[$field_name] = $sql_column;
            }
            $tables[] = $this_field_info['table'];
        }
        /**
         * add prereq tables first pass
         */
        foreach ($tables as $table_name) {
            if (isset($this->_tables[$table_name]['prereq'])) {
                $tables = array_merge($tables, (array)$this->_tables[$table_name]['prereq']);
            }
        }
        if ($conditionArray) {
            $this->_whereTable = $tables;
            $where_str .= $this->getWhere($conditionArray);
            $tables = $this->_whereTable;
        }
        $tables = array_unique($tables);
        $joinable_colums_by_table = array();
        /**
         * add prereq tables again
         */
        foreach ($tables as $table_name) {
            if (isset($this->_tables[$table_name]['prereq'])) {
                $tables = array_merge($tables, (array)$this->_tables[$table_name]['prereq']);
            }
        }
        $tables = array_unique($tables);
        $priority_order = array_keys($this->_tables);
        $prioritized = array();
        foreach ($priority_order as $table_name) {
            if (in_array($table_name, $tables)) {
                $prioritized[] = $table_name;
            }
        }


        foreach ($tables as $table) {
            foreach ($this->_tables[$table]['joinable'] as $joinable_alias => $joinable_col) {
                if (!isset($joinable_colums_by_table[$joinable_alias])) {
                    $joinable_colums_by_table[$joinable_alias] = array("table" => $table, "column" => $joinable_col);
                }
            }
        }
        $joins = array();
        foreach ($tables as $table_name) {
            $this_table_info = $this->_tables[$table_name];

            if (empty($joins)) {
                $joins[] = 'FROM `' . $this_table_info['table'] . '` AS `' . $table_name . '`';
            } else {

                $this_join = $this_table_info['join'] . ' JOIN ' . $this_table_info['table'] . ' AS `' . $table_name . '` ON ';
                $on = array('1=1');

                foreach ($this_table_info['joinable'] as $joinable_alias => $joinable_col) {
                    if (isset($joinable_colums_by_table[$joinable_alias]) && $joinable_colums_by_table[$joinable_alias]['table'] != $table_name) {
                        $on[] = '`' . $joinable_colums_by_table[$joinable_alias]['table'] . '`.' . $joinable_colums_by_table[$joinable_alias]['column'] . ' = `' . $table_name . '`.' . $joinable_col;
                    }
                }
                if (isset($this_table_info['join_condition'])) {
                    $on[] = $this_table_info['join_condition'];
                }
                $this_join .= implode(" AND ", $on);
                $joins[] = $this_join;

                if ($table_name == 'order_item_attribute' && $this->has_attribute == 'false') {
                    unset($joins[array_search($this_join, $joins)]);
                }
            }
        }
        if (!empty($this->_join))
            $joins = array_merge($joins, $this->_join);

        $columns = array();
        foreach ($sql_columns as $alias => $column) {
            $columns[] = $column . ' AS `' . $alias . '`';
        }

        if (isset($requestData['action']) && $requestData['action'] == "calling-user") {

            $where_str .= " AND users.mobile_number IS NOT NULL";

            $day_start = date('Y-m-d', strtotime("-6 months"));
            $marketing_goal = $requestData['marketing_goal'];

            $blockQuery = "left JOIN (select user_id,created_at as block_date from campaign_block_user WHERE DATE(created_at) >='$day_start' GROUP BY user_id) blocktable ON course_user.user_id = blocktable.user_id";
            array_push($joins, $blockQuery);
            $where_str .= " AND blocktable.block_date IS NULL";

            if ($requestData['is_allow_duplicate'] == 0) {

                if ($marketing_goal == "referral") {
                    $checkCallingUserFilterQuery = "left JOIN (select campaign_users_calling.user_id,count(campaign_users_calling.id) as alrady_assign_calling from campaign_users_calling LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id WHERE calling_action IS NULL GROUP BY campaign_users_calling.user_id) callingtable ON course_user.user_id = callingtable.user_id";
                } else {
                    $checkCallingUserFilterQuery = "left JOIN (select campaign_users_calling.user_id,count(campaign_users_calling.id) as alrady_assign_calling from campaign_users_calling LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id WHERE calling_action IS NULL AND campaign.marketing_goal ='$marketing_goal' GROUP BY campaign_users_calling.user_id) callingtable ON course_user.user_id = callingtable.user_id";
                }
                array_push($joins, $checkCallingUserFilterQuery);
                $where_str .= " AND alrady_assign_calling IS NULL";
            }
        }

        if ($campaign_id != '') {
            $base_query = "SELECT $campaign_id as campaign_id,course_user.id as course_user_id,course_user.user_id,course_user.course_id,now() as created_at " . implode("
		", $joins) . "
		WHERE $where_str";
            if (count($group_by) > 0) {
                $base_query .= " GROUP BY " . implode(", ", $group_by);
            }

            return $base_query;
        } else {


            $count_query = "SELECT COUNT(*) as totalRecord FROM (
            SELECT COUNT(*) as unique_id " . implode("
		", $joins) . "
		WHERE $where_str";
            if (count($group_by) > 0) {
                $count_query .= " GROUP BY " . implode(", ", $group_by);
            }
            $count_query .= ")t1";


            $countResult = (array)DB::selectOne($count_query);

            return $countResult['totalRecord'];
        }

    }


    public function buildQuery($requestData, $pageSize = null, $start = null)
    {

        $conditionArray = $requestData['conditions'];

        $selected_fields = $requestData['fields'];
        $tables = array("course_user", "courses");
        $sql_columns = array();
        $group_by = array('course_user.id');
        $total_columns = array();
        $where_str = "1=1";
        foreach ($selected_fields as $field_name) {
            $this_field_info = $this->_fields[$field_name];
            if (!isset($this_field_info)) {
                continue;
            }
            $sql_column = $this->getSqlColumn($field_name);
            $sql_columns[$field_name] = $sql_column;
            if (isset($this_field_info['group_by']) && $this_field_info['group_by']) {
                $group_by[] = '`' . $field_name . '`';
            } else {
                $total_columns[$field_name] = $sql_column;
            }
            $tables[] = $this_field_info['table'];
        }
        /**
         * add prereq tables first pass
         */
        foreach ($tables as $table_name) {
            if (isset($this->_tables[$table_name]['prereq'])) {
                $tables = array_merge($tables, (array)$this->_tables[$table_name]['prereq']);
            }
        }
        if ($conditionArray) {
            $this->_whereTable = $tables;
            $where_str .= $this->getWhere($conditionArray);
            $tables = $this->_whereTable;
        }
        $tables = array_unique($tables);
        $joinable_colums_by_table = array();
        /**
         * add prereq tables again
         */
        foreach ($tables as $table_name) {
            if (isset($this->_tables[$table_name]['prereq'])) {
                $tables = array_merge($tables, (array)$this->_tables[$table_name]['prereq']);
            }
        }
        $tables = array_unique($tables);
        $priority_order = array_keys($this->_tables);
        $prioritized = array();
        foreach ($priority_order as $table_name) {
            if (in_array($table_name, $tables)) {
                $prioritized[] = $table_name;
            }
        }


        foreach ($tables as $table) {
            foreach ($this->_tables[$table]['joinable'] as $joinable_alias => $joinable_col) {
                if (!isset($joinable_colums_by_table[$joinable_alias])) {
                    $joinable_colums_by_table[$joinable_alias] = array("table" => $table, "column" => $joinable_col);
                }
            }
        }
        $joins = array();
        foreach ($tables as $table_name) {
            $this_table_info = $this->_tables[$table_name];

            if (empty($joins)) {
                $joins[] = 'FROM `' . $this_table_info['table'] . '` AS `' . $table_name . '`';
            } else {

                $this_join = $this_table_info['join'] . ' JOIN ' . $this_table_info['table'] . ' AS `' . $table_name . '` ON ';
                $on = array('1=1');

                foreach ($this_table_info['joinable'] as $joinable_alias => $joinable_col) {
                    if (isset($joinable_colums_by_table[$joinable_alias]) && $joinable_colums_by_table[$joinable_alias]['table'] != $table_name) {
                        $on[] = '`' . $joinable_colums_by_table[$joinable_alias]['table'] . '`.' . $joinable_colums_by_table[$joinable_alias]['column'] . ' = `' . $table_name . '`.' . $joinable_col;
                    }
                }
                if (isset($this_table_info['join_condition'])) {
                    $on[] = $this_table_info['join_condition'];
                }
                $this_join .= implode(" AND ", $on);
                $joins[] = $this_join;

                if ($table_name == 'order_item_attribute' && $this->has_attribute == 'false') {
                    unset($joins[array_search($this_join, $joins)]);
                }
            }
        }
        if (!empty($this->_join))
            $joins = array_merge($joins, $this->_join);

        $columns = array();
        foreach ($sql_columns as $alias => $column) {
            $columns[] = $column . ' AS `' . $alias . '`';
        }


        $orderBy = " ORDER BY " . $requestData['order']['field'] . " " . $requestData['order']['dir'];


        $base_query = "SELECT ";
        if (isset($pageSize)) {
            $base_query = "SELECT course_user.id,";
        }
        $base_query .= implode(",
		", $columns) . "
		" . implode("
		", $joins) . "
		WHERE $where_str";
        if (count($group_by) > 0) {
            $base_query .= " GROUP BY " . implode(", ", $group_by);
        }

        $base_query .= $orderBy;
        if (isset($pageSize)) {
            $base_query .= " LIMIT " . $start . ',' . $pageSize;
        }

        $countQuery = "SELECT COUNT(*) as totalRecord " . implode(" ", $joins) . " WHERE $where_str";
        if (count($group_by) > 0) {
            $countQuery .= " GROUP BY " . implode(", ", $group_by);
            $countQuerySub = "SELECT COUNT(*) as totalRecord FROM ($countQuery) temp";
            $countQuery = $countQuerySub;
        }

        $countResult = (array)DB::selectOne($countQuery);

        $result['base_query'] = $base_query;
        $result['count_query'] = $countQuery;
        $result['data'] = DB::select($base_query);
        $result['count_total'] = $countResult['totalRecord'];
        return $result;
    }

}
