<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 23-02-2021
 * Time: 03:42 PM
 */

namespace App\Repositories;

use App\Interfaces\ReviewRepositoryInterface;
use App\Models\Comments;
use App\Models\Course;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class ReviewRepository implements ReviewRepositoryInterface
{
    public function getAjaxListData($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = Comments::whereNotNull('id');
        if (isset($requestData['course_id'])) {
            $dataInput = $dataInput->where('course_id', $requestData['course_id']);
        }
        if (isset($requestData['is_default_testimonial'])) {
            $dataInput = $dataInput->where('is_default_testimonial', $requestData['is_default_testimonial']);
        }
        if (isset($requestData['date_range'])) {
            $data_list = (explode(' - ', $requestData['date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
        }
        if (isset($requestData['search_input'])) {
            $searchTerm = $requestData['search_input'];
            $dataInput = $dataInput->where(function ($q) use ($searchTerm) {
                $q->where('content', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('rating', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('author', 'LIKE', "%{$searchTerm}%");
            })->orWhereHas('courseDetail', function ($q) use ($searchTerm) {
                $q->where('name', 'LIKE', "%{$searchTerm}%");
            });

        }
        if (isset($requestData['rating'])) {
            $dataInput = $dataInput->Where('rating', 'LIKE', $requestData['rating']);
        }
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id', "DESC")->skip($start)->take($pageSize);
        $data_tables = DataTables::of($dataInput);

        $data_tables->EditColumn('content', function ($review) {
            return $review->content ?? "- - -";
        });
        $data_tables->EditColumn('rating', function ($review) {
            return $review->rating ?? "- - -";
        });
        $data_tables->EditColumn('author', function ($review) {
            return $review->author ?? "- - -";
        });
        $data_tables->EditColumn('course_name', function ($review) {
            return $review->courseDetail->name ?? "- - -";
        });
        $data_tables->EditColumn('is_default_testimonial', function ($review) {
            return ($review->is_default_testimonial == 1) ? 'Yes' : "No";
        });
        $data_tables->EditColumn('date', function ($review) {
            return formatDate($review->created_at) ?? "- - -";
        });

        $data_tables->EditColumn('action', function ($review) {
            return view('review.action', compact('review'))->render();
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);

        $data_tables->rawColumns(['action']);
        return $data_tables->make(true);
    }

    public function updateActiveStatus($request)
    {
        $quiz = Comments::find($request->id);
        $quiz->is_active = $request->is_active;
        $quiz->save();
        return true;
    }

    public function getReviewDetails($id)
    {
        return Comments::findOrFail($id);
    }

    public function storeDefaultReviews($request)
    {
        $requestData = $request->except('_token');
        $requestData['is_default_testimonial'] = 1;
        Comments::create($requestData);

        $data = Comments::where('course_id', $requestData['course_id'])->where('is_active', 1)->select(DB::raw('avg(rating) as average_ratings'), DB::raw('count(*) as comment_count'))->first();
        Course::where('id', $requestData['course_id'])->update(['average_rating' => round($data['average_ratings'], 1), 'comment_count' => $data['comment_count']]);
        return true;
    }

    public function updateReviews($request, $id)
    {
        $requestData = $request->except('_token');
        unset($requestData['course']);
        $review = self::getReviewDetails($id);
        $requestData['is_default_testimonial'] = isset($requestData['is_default_testimonial']) ? 1 : 0;
        $review->update($requestData);
        $data = Comments::where('course_id', $review['course_id'])->where('is_active', 1)->select(DB::raw('avg(rating) as average_ratings'), DB::raw('count(*) as comment_count'))->first();
        Course::where('id', $review['course_id'])->update(['average_rating' => round($data['average_ratings'], 1), 'comment_count' => $data['comment_count']]);
        return true;
    }

    public function getReview()
    {
        return Comments::all();
    }

    public function getCountActive()
    {
        $count = [
            'active' => Comments::where('is_active', 1)->count(),
            'total' => Comments::count(),
        ];
        return $count;
    }

    public function createReview($request)
    {
        $requestData = $request->all();
        $user = User::select('id', 'first_name')->where('id', $requestData['user_id'])->first();
        $requestData['author'] = $user->first_name ?? null;
        $comment = Comments::query()->updateOrCreate([
            'course_id' => $requestData['course_id'],
            'user_id' => $requestData['user_id'],
        ], $requestData);

        $data = Comments::where('course_id', $requestData['course_id'])->where('is_active', 1)->select(DB::raw('avg(rating) as average_ratings'), DB::raw('count(*) as comment_count'))->first();
        Course::where('id', $requestData['course_id'])->update(['average_rating' => round($data['average_ratings'], 1), 'comment_count' => $data['comment_count']]);
        return $comment;
    }
}
