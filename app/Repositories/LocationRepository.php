<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 09-02-2021
 * Time: 02:14 PM
 */

namespace App\Repositories;

use App\Interfaces\LocationRepositoryInterface;
use App\Models\City;
use App\Models\Country;
use App\Models\State;
use Illuminate\Support\Facades\DB;

class LocationRepository implements LocationRepositoryInterface
{
    public function getCountries()
    {
        return Country::pluck('name', 'id')->toArray();
    }

    public function getCountriesForCompleteProfile()
    {
        return Country::where('country_code', 'IN')->pluck('name', 'id')->toArray();
    }

    public function getStates($country_id)
    {
        return State::where('country_id', $country_id)->pluck('name', 'id')->toArray();
    }

    public function getCities($state_id)
    {
        return City::where('state_id', $state_id)->pluck('name', 'id')->toArray();
    }

    public function getDistrict($state_id)
    {
        return DB::table('districts')->where('state_id', $state_id)->pluck('name', 'name')->toArray();
    }

    public function getCityDetailByName($name)
    {
        return City::where('name', $name)->first();
    }

    public function getCityDetailById($cityId)
    {
        return City::where('id', $cityId)->first();
    }

    public function getStatesByCountry($request)
    {
        $requestData = $request->except("_token");
        $returnData = [];
        if (isset($requestData['countryId'])) {
            $data = State::where('country_id', $requestData['countryId']);
            if (isset($requestData['searchTerm'])) {
                $data = $data->where('name', "Like", "%" . $requestData['searchTerm'] . "%");
            }
            $data = $data->get();
            foreach ($data as $datum) {
                $returnData[] = ['id' => $datum->id, 'text' => $datum->name];
            }
        }
        return $returnData;
    }

    public function getCitiesByState($request)
    {
        $requestData = $request->except("_token");
        $returnData = [];
        if (isset($requestData['stateId'])) {
            $data = City::where('state_id', $requestData['stateId']);
            if (isset($requestData['searchTerm'])) {
                $data = $data->where('name', "Like", "%" . $requestData['searchTerm'] . "%");
            }
            $data = $data->get();
            foreach ($data as $datum) {
                $returnData[] = ['id' => $datum->id, 'text' => $datum->name];
            }
        }
        return $returnData;
    }

    public function getDistrictByState($request)
    {
        $requestData = $request->except("_token");
        $returnData = [];
        if (isset($requestData['stateId'])) {
            $data = DB::table('districts')->where('state_id', $requestData['stateId']);
            if (isset($requestData['searchTerm'])) {
                $data = $data->where('name', "Like", "%" . $requestData['searchTerm'] . "%");
            }
            $data = $data->get();
            foreach ($data as $datum) {
                $returnData[] = ['id' => $datum->name, 'text' => $datum->name];
            }
        }
        return $returnData;
    }


}
