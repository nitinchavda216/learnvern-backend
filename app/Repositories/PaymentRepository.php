<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:35 PM
 */

namespace App\Repositories;

use App\Events\PaymentConfirmationEmailEvent;
use App\Interfaces\PaymentRepositoryInterface;
use App\Models\CourseUser;
use App\Models\User;
use App\Models\CustomPaymentRequests;
use App\Models\PaymentTransaction;
use App\Services\RazorpayService;
use Illuminate\Support\Facades\DB;
use Razorpay\Api\Api;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;

class PaymentRepository implements PaymentRepositoryInterface
{
    protected $razorpayService;

    public function __construct(RazorpayService $razorpayService)
    {
        $this->razorpayService = $razorpayService;
    }

    public function getPaymentHistoryList($request)
    {
//        dd($request['payment_method']);

        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = PaymentTransaction::where('payment_type', 'nsdc_sales');
        if (isset($requestData['partner_user_id'])) {
            $dataInput = $dataInput->where('partner_id', $requestData['partner_user_id']);
            $where = 'WHERE payment_transactions.partner_id ='.$requestData['partner_user_id'];
        } else {
            $dataInput = $dataInput->where('partner_id', session('partner_id'));
            $where = 'WHERE payment_transactions.partner_id ='.session('partner_id');
        }
        if (isset($requestData['transaction_date'])) {
            $data_list = (explode(' - ', $requestData['transaction_date']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereBetween('transaction_date', [$startDate, $endDate]);
            $where .= " AND (payment_transactions.transaction_date BETWEEN  '" . $startDate . "' AND '" . $endDate . "')";
        }


        $countForCampaingOrOrganic = "SELECT COUNT(CASE WHEN calling.id IS NULL THEN 1 END) AS organic_sell,COUNT(CASE WHEN calling.id IS NOT NULL THEN 1 END) AS campaign_sell
FROM payment_transactions
LEFT JOIN
(
	SELECT campaign_users_calling.id,campaign_users_calling.`course_id`,campaign_users_calling.`user_id`,campaign_users_calling.`first_call_date`
	FROM campaign_users_calling
	LEFT JOIN campaign ON campaign_users_calling.`campaign_id` = campaign.id
	WHERE campaign.marketing_goal = 'revenue' AND campaign_users_calling.calling_action IS NULL AND campaign_users_calling.parent_id IS NOT NULL
) calling
ON payment_transactions.user_id = calling.user_id AND DATE(payment_transactions.transaction_date) >= DATE(calling.first_call_date)
$where";


        $countForCampaingOrOrganicResult = (array)DB::selectOne($countForCampaingOrOrganic);
        if (isset($requestData['search'])) {
            $searchTerm = $requestData['search'];
            $dataInput = $dataInput->where(function ($qq) use ($searchTerm) {
                $qq->whereHas('userDetail', function ($q) use ($searchTerm) {

                        $q->where('first_name', 'LIKE', "%{$searchTerm}%")
                            ->orWhere('email', 'LIKE', "%{$searchTerm}%")
                            ->orWhere('mobile_number', 'LIKE', "%{$searchTerm}%")
                            ->orWhere('referral', 'LIKE', "%{$searchTerm}%")
                            ->orWhere('own_referral_code', 'LIKE', "%{$searchTerm}%");
                    })->orWhereHas('courseDetail', function ($q) use ($searchTerm) {
                        $q->where('name', 'LIKE', "%{$searchTerm}%");
                });
            });
        }

        if ($requestData['user_type'] == '1') {
            $dataInput = $dataInput->where(function ($qq) {
                $qq->whereHas('userDetail', function ($q) {
                    $q->where('country', '!=', 'India');
                    $q->where('country', '!=', null);
                });
            });
        } else if ($requestData['user_type'] == '0') {
            $dataInput = $dataInput->where(function ($qq) {
                $qq->whereHas('userDetail', function ($q) {
                    $q->where('country', '=', 'India')->orWhere('country', null);
                });
            });
        }


        if (isset($requestData['payment_method'])){
                  $dataInput = $dataInput->where('payment_method', $requestData['payment_method']);
        }


        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id', "DESC")->skip($start)->take($pageSize);

        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('name', function ($data) {
            return $data->userDetail->first_name ?? "- - -";
        });
        $data_tables->EditColumn('email', function ($data) {
            return $data->userDetail->email ?? "- - -";
        });
        $data_tables->EditColumn('mobile_number', function ($data) {
            return $data->userDetail->mobile_number ?? "- - -";
        });
        $data_tables->EditColumn('referral_code', function ($data) {
            return $data->userDetail->own_referral_code ?? "- - -";
        });
        $data_tables->EditColumn('affiliate_code', function ($data) {
            return $data->userDetail->referral ?? "- - -";
        });
        $data_tables->EditColumn('transaction_date', function ($data) {
            return formatDate($data->transaction_date, 'Y-m-d');
        });
        $data_tables->EditColumn('amount', function ($data) {
            return formatPrice($data->amount);
        });
        $data_tables->EditColumn('course', function ($data) {
            return $data->courseDetail->name ?? "- - -";
        });
        $data_tables->EditColumn('referrence_id', function ($data) {
            return isset($data->mihpayid) && ($data->mihpayid != "") ? $data->mihpayid : "- - -";
        });
        $data_tables->EditColumn('payment_method', function ($data) {
//            $payment_source = isset($data->payment_source) && ($data->payment_source != "") ? '('.$data->payment_source.')' : null;
            $payment_source = null;
            if ($data->payment_method) {
                return PaymentTransaction::PAYMENT_METHOD_TYPE[$data->payment_method]."".(isset($payment_source) ? $payment_source : "");
            } else {
                return isset($payment_source) ? $payment_source : "- - -";
            }
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
            "countForCampaingOrOrganicResult" =>$countForCampaingOrOrganicResult
        ]);
        return $data_tables->make(true);
    }

    public function getIncome($start_date, $end_date)
    {
        return PaymentTransaction::where('partner_id', session('partner_id'))
            ->whereIn('payment_type', ['nsdc_sales', 'international_sales'])
            ->whereBetween('transaction_date', [$start_date . " 00:00:00", $end_date . " 23:59:59"])
            ->sum('amount');
    }

    public function getPaymentRecord()
    {
        $PaymentData = PaymentTransaction::selectRaw('date(transaction_date)as date, count(id) data')
            ->whereYear('transaction_date', Carbon::now()->year)
            ->whereMonth('transaction_date', Carbon::now()->month)
            ->groupBy('Date')
            ->orderBy('Date', 'asc')
            ->get();
        $userCounts = [];
        foreach ($PaymentData as $data) {
            $userCounts[] = $data['data'];
        }
        $returnData['userCounts'] = json_encode($userCounts);
        return $returnData;
    }

    public function getCertificatePurchaseUsersAjaxData($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = PaymentTransaction::where('payment_type', 'nsdc_sales');
        if (isset($requestData['partner_user_id'])) {
            $dataInput = $dataInput->where('partner_id', $requestData['partner_user_id']);
        } else {
            $dataInput = $dataInput->where('partner_id', session('partner_id'));
        }
        if (isset($requestData['course_id'])) {
            $dataInput = $dataInput->where('course_id', $requestData['course_id']);
        }
        if (isset($requestData['enroll_date_range'])) {
            $data_list = (explode(' - ', $requestData['enroll_date_range']));
            $startEnrollDate = date('Y-m-d', strtotime($data_list[0]));
            $endEnrollDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereHas('courseUserDetails', function ($q) use ($startEnrollDate, $endEnrollDate) {
                $q->whereBetween('course_enroll_date', [$startEnrollDate . ' 00:00:00', $endEnrollDate . ' 23:59:59']);
            });
        }
        if (isset($requestData['transaction_date'])) {
            $data_list = (explode(' - ', $requestData['transaction_date']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereBetween('transaction_date', [$startDate, $endDate]);
        }
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id', "DESC")->skip($start)->take($pageSize);

        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('name', function ($data) {
            return $data->userDetail->first_name ?? "- - -";
        });
        $data_tables->EditColumn('email', function ($data) {
            return $data->userDetail->email ?? "- - -";
        });
        $data_tables->EditColumn('mobile_number', function ($data) {
            return $data->userDetail->mobile_number ?? "- - -";
        });
        $data_tables->EditColumn('referral_code', function ($data) {
            return $data->userDetail->own_referral_code ?? "- - -";
        });
        $data_tables->EditColumn('transaction_date', function ($data) {
            return formatDate($data->transaction_date, 'Y-m-d');
        });
        $data_tables->EditColumn('amount', function ($data) {
            return formatPrice($data->amount);
        });
        $data_tables->EditColumn('course', function ($data) {
            return $data->courseDetail->name ?? "- - -";
        });
        $data_tables->EditColumn('payment_source', function ($data) {
            return $data->payment_source ?? "- - -";
        });
        $data_tables->EditColumn('payment_method', function ($data) {
            if ($data->payment_method) {
                return PaymentTransaction::PAYMENT_METHOD_TYPE[$data->payment_method];
            } else {
                return "- - -";
            }
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        return $data_tables->make(true);
    }

    public function sendPaymentLink($request)
    {
        DB::beginTransaction();
        try {
            $requestData = $request->all();
            $courseUser = CourseUser::where('id', $requestData['course_user_id'])->first();
            $existData = CustomPaymentRequests::where('course_user_id', $courseUser->id)->first();
            if (isset($existData)) {
                $response = $this->razorpayService->resendPaymentLink($existData->razorpay_invoice_id);
                return $response;
            } else {
                $price = 1;
                foreach ($courseUser->partnerCourses as $partnerCourse) {
                    if ($courseUser->partner_id == $partnerCourse->partner_id) {
                        $price = $partnerCourse->course_fees;
                        break;
                    }
                }
                $paymentLinkData = [
                    'name' => $courseUser->userDetail->first_name,
                    'mobile_number' => $courseUser->userDetail->mobile_number,
                    'email' => $courseUser->userDetail->email,
                    'course_name' => $courseUser->courseDetail->name,
                    'price' => $price,
                ];
                $response = $this->razorpayService->sendPaymentLink($paymentLinkData);
                if ($response['status']) {
                    CustomPaymentRequests::create([
                        'user_id' => $courseUser->user_id,
                        'course_id' => $courseUser->course_id,
                        'course_user_id' => $courseUser->id,
                        'razorpay_invoice_id' => $response['data']['id'],
                        'sent_request_content' => $response['data']
                    ]);
                    DB::commit();
                    return ['status' => true, 'message' => "Payment Link Sent Successfully."];
                } else {
                    return $response;
                }
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function paymentSuccess($request)
    {
        DB::beginTransaction();
        try {
            $requestData = $request->all();
            $customPaymentRequest = CustomPaymentRequests::where('razorpay_invoice_id', $requestData['razorpay_invoice_id'])->first();
            $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
            if (isset($customPaymentRequest)) {
                $payment = $api->payment->fetch($requestData['razorpay_payment_id']);
                if ($payment) {
                    $course_detail = $customPaymentRequest->courseDetail;
                    $amount = ($payment->amount / 100);
                    if ($course_detail) {
                        $courseUserDetails = CourseUser::where('course_id', $course_detail->id)->where('user_id', $customPaymentRequest->user_id)->first();
                        if (isset($courseUserDetails)) {
                            $courseUserDetails->update(['paid_status' => 1, 'updated_at' => Carbon::now(), 'payment_completed_date' => Carbon::now()]);
                        } else {
                            $user_id = $customPaymentRequest->user_id;
                            $course_id = $course_detail->id;
                            $all_count_user = (array)DB::selectOne("select (select count(id) from curriculum where course_id=$course_id and type!='section' and is_active=1) as total ,
                                (select count(uu.id) from curriculum c join unit_user uu on c.curriculum_list_id=uu.unit_id and c.type='unit' and uu.user_id=$user_id and uu.is_completed=1 where c.course_id=$course_id and c.is_active=1) +
                                (select count(distinct(qu.quiz_id)) from curriculum c join quiz_user qu on c.curriculum_list_id=qu.quiz_id and c.type='quiz' and qu.user_id=$user_id and is_completed=1 where c.course_id=$course_id and c.is_active=1) +
                                (select count(distinct(au.id)) from curriculum c join assignment_user au on c.curriculum_list_id=au.assignment_id and c.type='assignment' and au.user_id=$user_id and is_completed=1 where c.course_id=$course_id and c.is_active=1) as user_total
                                ");
                            $course_avg = ($all_count_user['user_total'] / $all_count_user['total']) * 100;
                            $insert_course_user_data = array(
                                "course_id" => $course_id,
                                "user_id" => $user_id,
                                "paid_status" => 1,
                                "user_status_id" => 2,
                                "progress" => $course_avg,
                                "partner_id" => $customPaymentRequest->userDetail->partner_id,
                                "payment_completed_date" => Carbon::now(),
                                "last_used_date" => Carbon::now(),
                                "enroll_source" => 1,
                            );
                            $courseUserDetails = CourseUser::create($insert_course_user_data);
                        }
                        $existPaymentTransaction = PaymentTransaction::where('course_user_id', $courseUserDetails->id)->first();
                        if (is_null($existPaymentTransaction)) {
                            $ins_trans = array(
                                "user_id" => $customPaymentRequest->user_id,
                                "course_id" => $course_detail->id,
                                "course_user_id" => $customPaymentRequest->course_user_id,
                                "amount" => $amount,
                                "transaction_id" => $payment->order_id,
                                "mihpayid" => $payment->id,
                                "payment_source" => $payment->method,
                                "partner_id" => $customPaymentRequest->userDetail->partner_id,
                                "payment_method" => 10,
                                "transaction_date" => date("Y-m-d"),
                            );
                            PaymentTransaction::create($ins_trans);
                            $ins_trans['status'] = 'success';
                            $ins_trans['server_response'] = json_encode($payment);
                            unset($ins_trans['course_user_id']);
                            $ins_trans['created_at'] = date("Y-m-d H:i:s");
                            DB::table('all_transactions')->insert($ins_trans);
                        }
                        $certificate_name = $customPaymentRequest->userDetail->certificate_name;
                        if (is_null($certificate_name) && empty($certificate_name) && $certificate_name == "") {
                            $certificate_name = $customPaymentRequest->userDetail->first_name . ' ' . $customPaymentRequest->userDetail->last_name;
                        }
                        $user_data = ['email' => $customPaymentRequest->userDetail->email, 'first_name' => $customPaymentRequest->userDetail->first_name, 'course_name' => $course_detail->name, 'amount' => $amount, 'certificate_name' => $certificate_name];
                        $referral = $customPaymentRequest->userDetail->referral;
                        if (isset($referral)) {
                            User::where('own_referral_code', $referral)->increment('nsdc_referred_count');
                        }

                        $customPaymentRequest->update(['status' => 1]);
                        event(new PaymentConfirmationEmailEvent($user_data));
                        $url = env('FRONT_URL') . 'payment/thank-you/'.$course_detail->slug.'?utm_campaign=PaymentDone';
                        DB::commit();
                        return ['status' => true, 'url' => $url];
                    }
                }
            }
            DB::rollBack();
            return ['status' => false, 'message' => "Something went wrong"];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }
}
