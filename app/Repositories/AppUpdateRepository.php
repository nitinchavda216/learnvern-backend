<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08-03-2021
 * Time: 11:06 AM
 */

namespace App\Repositories;

use App\Interfaces\AppUpdateRepositoryInterface;
use App\Models\AppUpdateDetails;

class AppUpdateRepository implements AppUpdateRepositoryInterface
{
    public function getAppUpdateList()
    {
        return AppUpdateDetails::get();
    }

    public function getLastVersionDetails()
    {
        return AppUpdateDetails::orderBy('id', 'DESC')->select('id', 'title', 'version', 'update_type', 'content')->first();
    }

    public function getAppLatestVersionDetails($request)
    {
        $requestData = $request->all();
        $platform = $requestData['platform'] ?? 'android';
        return AppUpdateDetails::orderBy('id', 'DESC')->where('platform', $platform)->select('id', 'title', 'version', 'update_type', 'platform', 'content')->first();
    }

    public function getVersionDetails($id)
    {
        return AppUpdateDetails::findOrFail($id);
    }

    public function storeAppUpdate($request): bool
    {
        $requestData = $request->except('_token');
        AppUpdateDetails::create($requestData);
        return true;
    }
}
