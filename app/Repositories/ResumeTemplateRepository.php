<?php

namespace App\Repositories;

use App\Interfaces\ResumeTemplateRepositoryInterface;
use App\Models\ResumeTemplates;

class ResumeTemplateRepository implements ResumeTemplateRepositoryInterface
{
    public function getResumeTemplates()
    {
        return ResumeTemplates::orderBy('id', 'DESC')->get();
    }

    public function storeTemplate($request)
    {
        $requestData = $request->except("_token");
        if (isset($requestData['image'])) {
            $requestData['image'] = self::uploadImage($requestData['image']);
        }
        ResumeTemplates::create($requestData);
        return true;
    }

    public function uploadImage($file, $template = NULL)
    {
        $uploadPath = storage_path(ResumeTemplates::IMG_PATH);
        if (isset($template)) {
            $imagePath = $uploadPath . $template->image;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function getTemplateDetail($id)
    {
        return ResumeTemplates::findOrFail($id);
    }

    public function updateTemplate($request, $id)
    {
        $requestData = $request->except("_token");
        $template = self::getTemplateDetail($id);
        if (isset($template)) {
            if (isset($requestData['image'])) {
                $requestData['image'] = self::uploadImage($requestData['image'], $template);
            }
            $template->update($requestData);
        }
        return true;
    }

    public function deleteTemplate($id)
    {
        return ResumeTemplates::destroy($id);
    }

}
