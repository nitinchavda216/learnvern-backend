<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:39 PM
 */

namespace App\Repositories;

use App\Exports\AmbassadorReportExport;
use App\Exports\UsersExport;
use App\Interfaces\ReferralPointRepositoryInterface;
use App\Models\User;
use App\Models\UserAmbassadorBadges;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Yajra\DataTables\Facades\DataTables;

class ReferralPointRepository implements ReferralPointRepositoryInterface
{

    public static function getEloquentSqlWithBindings($query)
    {
        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }


    public function getAjaxListData($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 10;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = User::whereNotNull('id')->where('referred_count', '>', 0);
        $campaignInput = User::whereNotNull('id')->where('referred_count', '>', 0);
        if (isset($requestData['partner_user_id'])) {
            $dataInput = $dataInput->where('partner_id', $requestData['partner_user_id']);
            $campaignInput = $campaignInput->where('partner_id', $requestData['partner_user_id']);
        } else {
            $dataInput = $dataInput->where('partner_id', session('partner_id'));
            $campaignInput = $campaignInput->where('partner_id', session('partner_id'));
        }
        if (isset($requestData['searchData'])) {
            $searchTerm = $requestData['searchData'];
            $dataInput = $dataInput->where(function ($q) use ($searchTerm) {
                $q->where('first_name', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('username', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('own_referral_code', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('email', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('mobile_number', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('isd_code', 'LIKE', "%{$searchTerm}%");
            });

            $campaignInput = $campaignInput->where(function ($q) use ($searchTerm) {
                    $q->where('first_name', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('username', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('own_referral_code', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('email', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('mobile_number', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('isd_code', 'LIKE', "%{$searchTerm}%");
            });

        }

        $campaignInput = $campaignInput->where('ambassador_badge_id', 2);

        if (isset($requestData['ambassador_badge_id']) && ($requestData['ambassador_badge_id'] == 1)) {
            $dataInput = $dataInput->whereNull('ambassador_badge_id')->orWhere('ambassador_badge_id', $requestData['ambassador_badge_id']);
        } elseif (isset($requestData['ambassador_badge_id'])) {
            $dataInput = $dataInput->where('ambassador_badge_id', $requestData['ambassador_badge_id']);
        }
        $where = "";
        if (isset($requestData['ambassador_badge_date'])) {
            $data_list = (explode(' - ', $requestData['ambassador_badge_date']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereHas('userBadges', function ($q) use ($startDate, $endDate) {
                $q->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            });

            $campaignInput = $campaignInput->whereHas('userBadges', function ($q) use ($startDate, $endDate) {
                $q->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            });


            $where = "AND (user_ambassador_badges.created_at BETWEEN  '" . $startDate ." 00:00:00 ' AND '". $endDate ." 23:59:59 ')";
        }


        $queryBinder =  $this->getEloquentSqlWithBindings($campaignInput);


            $countForCampaignOrOrganic = "SELECT SUM(CASE
             WHEN id IS NULL THEN 1
             ELSE 0
           END) AS organic_ambassador,
       SUM(CASE
             WHEN id IS NOT NULL THEN 1
             ELSE 0
           END) AS campaign_ambassador

                FROM (
                        SELECT campaign_users_calling.id
                        FROM (".$queryBinder.") t1
                        LEFT JOIN `campaign_users_calling` ON t1.`id` = campaign_users_calling.`user_id` AND campaign_users_calling.`parent_id` IS NOT NULL
                        GROUP BY t1.id
                )t2
                ";


        $countForCampaignOrOrganicResult = (array)DB::selectOne($countForCampaignOrOrganic);
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id', "DESC")->skip($start)->take($pageSize);
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('user_details', function ($user) {
            $first_name = $user->first_name ?? null;
            $email = $user->email ?? "- - -";
            $mobile_number = isset($user->mobile_number) ? $user->mobile_number : null;
            $isd_code = isset($user->isd_code) ? getIsdCode($user->isd_code) . ' ' : "";
            $str = "<i class=\"fas fa-user\"></i>" . "&nbsp;&nbsp;&nbsp;" . $first_name . "<br>";
            $str .= "<i class=\"far fa-envelope\"></i>" . "&nbsp;&nbsp;" . $email . "<br>";
            if (isset($mobile_number)) {
                $str .= "<i class=\"fa fa-phone-square\" aria-hidden=\"true\"></i>" . "&nbsp;&nbsp;&nbsp;" . $isd_code . $mobile_number;
            }
            return $str;
        });
        $data_tables->EditColumn('own_referral_code', function ($user) {
            return isset($user->own_referral_code) ? $user->own_referral_code : "- - -";
        });
        $data_tables->EditColumn('referral', function ($user) {
            $total = count($user->myReferrals);
            $active = $user->referred_count;
            $inactive = $total - $active;
            $msg = 'Total: ' . $total . ' &nbsp;Active: ' . $active . ' &nbsp;InActive: ' . $inactive . '&nbsp;&nbsp';
            $str = '<a href="javascript:;" class="showTooltip text-dark" data-toggle="tooltip" data-title="' . $msg . '"><span class="badge badge-success badge-pill">' . $user->referred_count . '</span></a>';
            return $str;
        });
        $data_tables->EditColumn('nsdc', function ($user) {
            $str = $user->nsdc_referred_count;
            return $str;
        });

        $data_tables->EditColumn('level_name', function ($user) {
            return $user->ambassadorBadgeDetail->title ?? "Volunteer";
        });

        $data_tables->EditColumn('location', function ($user) {
            $str = "<b>Country : </b>" . $user->country ?? "- -";
            $str .= "<br><b>State : </b>" . $user->state ?? "- -";
            $str .= "<br><b>City : </b>" . $user->city ?? "- -";
            return $str;
        });
        $data_tables->EditColumn('education', function ($user) {
            return $user->education ?? "- - -";
        });

        $data_tables->EditColumn('college', function ($user) {
            return isset($user->other_college_name) ? $user->other_college_name : (isset($user->collegeDetail) ? $user->collegeDetail->title : "- - -");
        });

        $data_tables->EditColumn('last_referral_date', function ($user) {
            if (count($user->userBadges) > 0) {
                $last_ref_date = $user->userBadges[0];
            } else {
                $last_ref_date = User::where('referral', $user->own_referral_code)->latest()->first();
            }
            return isset($last_ref_date->created_at) ? formatDate($last_ref_date->created_at, "d-m-Y") : "- - -";
        });
        $data_tables->EditColumn('actions', function ($user) {
            return view('referral-point.partials.actions', compact('user'))->render();
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
            "countForCampaignOrOrganicResult" => $countForCampaignOrOrganicResult

        ]);
        $data_tables->rawColumns(['user_details', 'location', 'no_of_sign_up', 'last_referral_date', 'referral', 'nsdc', 'actions']);
        return $data_tables->make(true);
    }

    public function exportListData($request)
    {
        $requestData = $request->all();
        $dataInput = User::whereNotNull('id')->where('referred_count', '>', 0);
        if (isset($requestData['partner_user_id'])) {
            $dataInput = $dataInput->where('partner_id', $requestData['partner_user_id']);
        } else {
            $dataInput = $dataInput->where('partner_id', session('partner_id'));
        }
        if (isset($requestData['searchData'])) {
            $searchTerm = $requestData['searchData'];
            $dataInput = $dataInput->where(function ($q) use ($searchTerm) {
                $q->where('first_name', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('username', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('email', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('mobile_number', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('isd_code', 'LIKE', "%{$searchTerm}%");
            });
        }
        if (isset($requestData['ambassador_badge_date'])) {
            $data_list = (explode(' - ', $requestData['ambassador_badge_date']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereHas('userBadges', function ($q) use ($startDate, $endDate) {
                $q->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            });
        }
        if (isset($requestData['ambassador_badge_id']) && ($requestData['ambassador_badge_id'] == 1)) {
            $dataInput = $dataInput->whereNull('ambassador_badge_id')->orWhere('ambassador_badge_id', $requestData['ambassador_badge_id']);
        } elseif (isset($requestData['ambassador_badge_id'])) {
            $dataInput = $dataInput->where('ambassador_badge_id', $requestData['ambassador_badge_id']);
        }
        $dataInput = $dataInput->orderBy('id', "DESC")->get();
        if (count($dataInput) == 0) {
            return ['status' => false];
        }
        $alldata = [];
        foreach ($dataInput as $user) {

            if (count($user->userBadges) > 0) {
                $last_ref_date = $user->userBadges[0];
            } else {
                $last_ref_date = User::where('referral', $user->own_referral_code)->latest()->first();
            }

            $courses = [];
            foreach ($user->courses as $courseUser){
                $courses[] = $courseUser->courseDetail->name;
            }
            $alldata[] = [
                'User Name' => $user->first_name ?? "",
                'User Email' => $user->email,
                'Mobile Number' => isset($user->mobile_number) ? $user->mobile_number : "",
                'Own Referral Code' => $user->own_referral_code,
                'Total Referrals' => $user->referred_count,
                'Nsdc Users' => $user->nsdc_referred_count,
                'Level' => $user->ambassadorBadgeDetail->title ?? "Volunteer",
                'Country' => $user->country,
                'State' => $user->state,
                'City' => $user->city,
                'Education' => $user->education ?? "",
                'College' => isset($user->other_college_name) ? $user->other_college_name : (isset($user->collegeDetail) ? $user->collegeDetail->title : "- - -"),
                'Last Referral Date' => isset($last_ref_date->created_at) ? formatDate($last_ref_date->created_at, "d-m-Y") : "- - -",
                'Courses' => implode(',', $courses),
            ];
        }
        $list = array_keys($alldata[0]);
        $data = new AmbassadorReportExport($alldata, $list);
        return ['status' => true, 'data' => $data];
    }
}
