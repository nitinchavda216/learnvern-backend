<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:26 PM
 */

namespace App\Repositories;

use App\Interfaces\QuizRepositoryInterface;
use App\Models\Course;
use App\Models\Curriculum;
use App\Models\Question;
use App\Models\QuestionOptions;
use App\Models\Quiz;
use App\Models\QuizUser;
use App\Models\Section;
use App\Models\Sluglists;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as Writer;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;

class QuizRepository implements QuizRepositoryInterface
{
    public function getAjaxListData($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = Curriculum::where('type', 'quiz')/*->where('hide_from_backend', $requestData['hide_from_backend'])*/;
        if (isset($requestData['course_id'])) {
            $dataInput = $dataInput->where('course_id', $requestData['course_id']);
            if (isset($requestData['section_id'])) {
                $dataInput = $dataInput->where('section_id', $requestData['section_id']);
            }
        }
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id')->skip($start)->take($pageSize);
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('course_name', function ($quiz) {
            return isset($quiz->courseDetail) ? html_entity_decode($quiz->courseDetail->name) : "- - -";
        });
        $data_tables->EditColumn('section_name', function ($quiz) {
            return isset($quiz->sectionDetail) ? html_entity_decode($quiz->sectionDetail->name) : "- - -";
        });/*
        $data_tables->EditColumn('hide_status', function ($item) {
            return view('courses.partials.curriculum_hide_status', compact('item'))->render();
        });*/
        $data_tables->EditColumn('action', function ($quiz) {
            return view('quiz.partials.actions', compact('quiz'))->render();
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['action', 'hide_status']);
        return $data_tables->make(true);
    }

    public function storeQuiz($request): bool
    {
        $requestData = $request->except('_token');
        $requestData['evaluate'] = isset($requestData['evaluate']) ? 1 : 0;
        $requestData['is_master_quiz'] = isset($requestData['is_master_quiz']) ? 1 : 0;
        $quiz = Quiz::create($requestData);
        Curriculum::create([
            'course_id' => $quiz->course_id,
            'section_id' => $quiz->section_id,
            'curriculum_list_id' => $quiz->id,
            'name' => $quiz->name,
            'type' => 'quiz'
        ]);
        Sluglists::create([
            'course_id' => $quiz->course_id,
            'curriculum_id' => $quiz->id,
            'curriculum_type' => 'quiz',
            'slug' => $quiz->slug,
        ]);
        return true;
    }

    public function getQuizDetails($id)
    {
        return Quiz::findOrFail($id);
    }

    public function updateQuiz($request, $id): bool
    {
        $requestData = $request->except('_token');
        $quiz = self::getQuizDetails($id);
        unset($requestData['slug']);
        $requestData['evaluate'] = isset($requestData['evaluate']) ? 1 : 0;
        $requestData['is_master_quiz'] = isset($requestData['is_master_quiz']) ? 1 : 0;
        $quiz->update($requestData);
        $existData = Curriculum::where('curriculum_list_id', $quiz->id)->where('type', 'quiz')->first();
        if (isset($existData)) {
            $existData->update(['name' => $quiz->name, 'course_id' => $quiz->course_id, 'section_id' => $quiz->section_id]);
            Curriculum::where('curriculum_list_id', $quiz->id)->where('type', 'quiz')
                ->where('id', '!=', $existData->id)->update([
                    'name' => $quiz->name
                ]);
        } else {
            Curriculum::create([
                'course_id' => $quiz->course_id,
                'section_id' => $quiz->section_id,
                'curriculum_list_id' => $quiz->id,
                'name' => $quiz->name,
                'type' => 'quiz'
            ]);
        }
        return true;
    }

    public function updateActiveStatus($request): bool
    {
        $quiz = Quiz::find($request->id);
        $quiz->is_active = $request->is_active;
        $quiz->save();

        Curriculum::where('curriculum_list_id', $quiz->id)->where('type', 'quiz')->update(['is_active' => $request->is_active]);
        return true;
    }

    public function getQuizUser($id)
    {
        return QuizUser::findOrFail($id);
    }

    public function getQuestionListByQuiz($id)
    {
        return Question::where('quiz_id', $id)->get();
    }

    public function getQuizQuestionDetails($id)
    {
        return Question::findOrFail($id);
    }

    public function storeQuizQuestion($request): bool
    {
        $requestData = $request->all();
        $question = Question::create($requestData);
        foreach ($requestData['options'] as $option_key => $option_value) {
            if (isset($option_value)) {
                QuestionOptions::create([
                    'quiz_id' => $requestData['quiz_id'],
                    'question_id' => $question->id,
                    'option_id' => $option_key,
                    'content' => $option_value,
                ]);
            }
        }
        return true;
    }

    public function updateQuizQuestion($request, $id): bool
    {
        $requestData = $request->all();
        $question = self::getQuizQuestionDetails($id);
        $question->update($requestData);
        foreach ($requestData['options'] as $option_key => $option_value) {
            if (isset($option_value)) {
                QuestionOptions::where('quiz_id', $requestData['quiz_id'])->where('question_id', $id)
                    ->where('option_id', $option_key)->update([
                        'content' => $option_value
                    ]);
            }
        }
        return true;
    }

    public function updateQuestionStatus($request): bool
    {
        $question = Question::find($request->id);
        $question->is_active = $request->is_active;
        $question->save();
        return true;
    }

    public function getAjaxExportData($request)
    {
        $requestData = $request->all();
        $sections = Section::where('course_id', $requestData['course_id'])->pluck('name')->toarray();
        $spreadsheet = new Spreadsheet();
        $dummySeat = $spreadsheet->createSheet();
        $dummySeat->setTitle('Worksheet2');
        $a = 'A';
        $i = 1;
        foreach ($sections as $key => $value) {
            $dummySeat->setCellValue($a . $i, $value);
            $i++;
        }
        $sheet = $spreadsheet->getActiveSheet();
        $predefinedColumn = ['Course Id', 'Section', 'Name', 'Slug', 'Sub Title', 'Time', 'Content', 'Short Content'];
        $alpha = 'A';
        foreach ($predefinedColumn as $value) {
            $sheet->setCellValue($alpha . '1', $value);
            $sheet->getColumnDimension($alpha)->setAutoSize(true);
            $alpha++;
        }
        $sheet->setCellValue('A2', $requestData['course_id']);
        for ($i = 2; $i <= 500; $i++) {
            $validation = $spreadsheet->getActiveSheet()->getCell('B' . $i)->getDataValidation();
            $validation->setType(DataValidation::TYPE_LIST);
            $validation->setErrorStyle(DataValidation::STYLE_INFORMATION);
            $validation->setAllowBlank(false);
            $validation->setShowInputMessage(true);
            $validation->setShowErrorMessage(true);
            $validation->setShowDropDown(true);
            $validation->setErrorTitle('Input error');
            $validation->setError('Value is not in list.');
            $validation->setPromptTitle('Pick from list');
            $validation->setPrompt('Please pick a value from the drop-down list.');
            $validation->setFormula1('Worksheet2!A1:A200');
        }
        $writer = new Writer($spreadsheet);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Quiz_Sample.xlsx"');
        $writer->save("php://output");
    }

    public function importQuiz($request): array
    {
        DB::beginTransaction();
        try {
            $requestData = $request->all();
            $reader = new Xlsx();
            $spreadsheet = $reader->load($requestData['file']);
            $sheetData = $spreadsheet->getActiveSheet()->toArray();
            if (count($sheetData) > 0) {
                unset($sheetData[0]);
                foreach ($sheetData as $sheetDatum) {
                    if ($sheetDatum[0] != "") {
                        $courseExist = Course::where('id', $sheetDatum[0])->first();
                        if (isset($courseExist)) {
                            $section = Section::where('course_id', $sheetDatum[0])->where('name', $sheetDatum[1])->first();
                            if (isset($section)) {
                                $slug = $sheetDatum[3];
                                $slugExist = Sluglists::where('slug', $slug)->first();
                                if (isset($slugExist)) {
                                    $slug = $sheetDatum[3] . '_' . time();
                                }
                                $quiz = Quiz::create([
                                    'course_id' => $sheetDatum[0],
                                    'section_id' => $section->id,
                                    'name' => $sheetDatum[2],
                                    'slug' => $slug,
                                    'sub_title' => $sheetDatum[4],
                                    'time' => $sheetDatum[5],
                                    'content' => ($sheetDatum[6] != "") ? $sheetDatum[6] : null,
                                    'short_content' => ($sheetDatum[7] != "") ? $sheetDatum[7] : null,
                                    'is_active' => 0
                                ]);
                                Curriculum::create([
                                    'course_id' => $quiz->course_id,
                                    'section_id' => $quiz->section_id,
                                    'curriculum_list_id' => $quiz->id,
                                    'name' => $quiz->name,
                                    'type' => 'quiz',
                                    'is_active' => 0
                                ]);
                                Sluglists::create([
                                    'course_id' => $quiz->course_id,
                                    'curriculum_id' => $quiz->id,
                                    'curriculum_type' => 'quiz',
                                    'slug' => $slug
                                ]);
                            }
                        }
                    }
                }
            }
            DB::commit();
            return ['status' => true, 'message' => 'Quiz imported successfully.'];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function downloadQuestionSampleFile($id)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $predefinedColumn = ['Quiz ID', 'Backend Question Title', 'Frontend Question Title', 'Option 1', 'Option 2', 'Option 3', 'Option 4', 'Question Right Answer', 'Question Mark'];
        $optionListString = "\"" . implode(',', [1, 2, 3, 4]) . "\"";
        $sheet->setCellValue('A2', $id);
        $alpha = 'A';
        foreach ($predefinedColumn as $value) {
            $sheet->setCellValue($alpha . '1', $value);
            $sheet->getColumnDimension($alpha)->setAutoSize(true);
            $alpha++;
        }
        for ($i = 2; $i <= 500; $i++) {
            $validation = $spreadsheet->getActiveSheet()->getCell('H' . $i)->getDataValidation();
            $validation->setType(DataValidation::TYPE_LIST);
            $validation->setErrorStyle(DataValidation::STYLE_INFORMATION);
            $validation->setAllowBlank(false);
            $validation->setShowInputMessage(true);
            $validation->setShowErrorMessage(true);
            $validation->setShowDropDown(true);
            $validation->setErrorTitle('Input error');
            $validation->setError('Value is not in list.');
            $validation->setPromptTitle('Pick from list');
            $validation->setPrompt('Please pick a value from the drop-down list.');
            $validation->setFormula1($optionListString);
        }
        $writer = new Writer($spreadsheet);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Questions_Sample.xlsx"');
        $writer->save("php://output");
    }

    public function importQuestions($request)
    {
        DB::beginTransaction();
        try {
            $requestData = $request->all();
            $reader = new Xlsx();
            $spreadsheet = $reader->load($requestData['file']);
            $sheetData = $spreadsheet->getActiveSheet()->toArray();
            if (count($sheetData) > 0) {
                unset($sheetData[0]);
                foreach ($sheetData as $sheetDatum) {
                    if ($sheetDatum[0] != "") {
                        $quizExist = Quiz::where('id', $sheetDatum[0])->first();
                        if (isset($quizExist)) {
                            $question = Question::create([
                                'quiz_id' => $sheetDatum[0],
                                'title' => $sheetDatum[1],
                                'content' => $sheetDatum[2],
                                'answer' => $sheetDatum[7],
                                'type' => 'single',
                                'mark' => $sheetDatum[8],
                                'is_active' => 0,
                            ]);
                            $options = array_slice($sheetDatum, 3, 4);
                            foreach ($options as $key => $option) {
                                if (isset($option)) {
                                    QuestionOptions::create([
                                        'quiz_id' => $sheetDatum[0],
                                        'question_id' => $question->id,
                                        'option_id' => $key + 1,
                                        'content' => $option,
                                        'is_active' => 1,
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
            DB::commit();
            return ['status' => true, 'message' => 'Questions Imported successfully.'];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

}
