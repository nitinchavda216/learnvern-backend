<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:43 PM
 */

namespace App\Repositories;


use App\Interfaces\WidgetsSettingsRepositoryInterface;
use App\Models\WidgetsSettings;
class WidgetsSettingsRepository implements WidgetsSettingsRepositoryInterface
{
    public function getAllWidgets()
    {
        return WidgetsSettings::all();
    }

    /**
     * @param $request
     * @return bool
     */
    public function storewidgetssettings($request)
    {
        $requestData = $request->all();
        if($requestData['content1'] != null && $requestData['contenttype'] == 'html'){
            $requestData['content'] = $requestData['content1'];
        }elseif($requestData['content2'] != null && $requestData['contenttype'] == 'text'){
            $requestData['content'] = $requestData['content2'];

        }
        if (isset($requestData['image'])) {
            $requestData['image'] = self::uploadImage($requestData['image']);
        }
        WidgetsSettings::create($requestData);
        return true;
    }

    public function uploadImage($file, $WidgetsSettings = NULL)
    {
        $uploadPath = storage_path(WidgetsSettings::IMG_PATH);
        if (isset($WidgetsSettings)) {
            $imagePath = $uploadPath . $WidgetsSettings->image;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function updateActiveStatus($request)
    {
        $pages = WidgetsSettings::find($request->id);
        $pages->is_active = $request->is_active;
        $pages->save();
        return true;
    }

    public function updateWidgetsSettings($request, $id)
    {
        $requestData = $request->all();
        if($requestData['content1'] != null && $requestData['contenttype'] == 'html'){
            $requestData['content'] = $requestData['content1'];
        }elseif($requestData['content2'] != null && $requestData['contenttype'] == 'text'){
            $requestData['content'] = $requestData['content2'];

        }
//        dd($requestData);
        $WidgetsSettings = self::getWidgetsSettingsDetails($id);
        if (isset($requestData['image'])) {
            $requestData['image'] = self::uploadImage($requestData['image'], $WidgetsSettings);
        }
        $WidgetsSettings->update($requestData);
        return true;
    }

    public function getWidgetsSettingsDetails($id)
    {
        return WidgetsSettings::findOrFail($id);
    }
}
