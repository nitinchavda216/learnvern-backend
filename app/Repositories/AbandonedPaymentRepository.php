<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 03:04 PM
 */

namespace App\Repositories;

use App\Exports\AbandonedPaymentReportExport;
use App\Interfaces\AbandonedPaymentRepositoryInterface;
use App\Models\AbandonedPaymentReports;
use App\Models\CourseUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Yajra\DataTables\Facades\DataTables;
use App\Exports\UsersExport;

class AbandonedPaymentRepository implements AbandonedPaymentRepositoryInterface
{
    public function getallabandonedpayment()
    {
        return AbandonedPaymentReports::select('user_id', 'course_id', DB::raw('COUNT(id) as countid'), DB::raw('max(created_at) as paymentcreated_date'))->with('UserDetail', 'CourseDetail')->groupBy('user_id', 'course_id')->orderBy('created_at', 'ASC')->get();
    }

    public function getAjaxListData($request)
    {
        $requestData = $request->all();
        $pageSize = $request['length'] ?? 50;
        $start = $request['start'] ?? 0;
        $where = 'WHERE 1=1';
        if (isset($requestData['partner_user_id'])) {
            $where .= ' AND abandoned_payment_reports.partner_id = ' . $requestData['partner_user_id'];
        } else {
            $where .= ' AND abandoned_payment_reports.partner_id = ' . session('partner_id');
        }


        if (isset($requestData['date_range'])) {
            $data_list = (explode(' - ', $requestData['date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $where .= " AND (abandoned_payment_reports.created_at BETWEEN  '" . $startDate . " 00:00:00' AND '" . $endDate . " 23:59:59')";
        }

        if (isset($requestData['search'])) {
            $search = trim($requestData['search']);
            $where .= " AND (users.first_name LIKE '%$search%' OR users.email LIKE '%$search%' OR users.mobile_number LIKE '%$search%' OR users.username LIKE '%$search%')";
        }

        $countQuery = "SELECT count(id) AS totalRecord FROM (
                  Select abandoned_payment_reports.id
                   FROM  abandoned_payment_reports
                   LEFT JOIN course_user ON course_user.user_id = abandoned_payment_reports.user_id AND course_user.course_id = abandoned_payment_reports.course_id
                   LEFT JOIN users ON users.id = abandoned_payment_reports.user_id
                   LEFT JOIN courses ON courses.id = abandoned_payment_reports.course_id
                   $where GROUP BY abandoned_payment_reports.user_id,abandoned_payment_reports.course_id ) temp";

        $countResult = (array)DB::selectOne($countQuery);

        $query = "Select abandoned_payment_reports.user_id,abandoned_payment_reports.course_id,COUNT(abandoned_payment_reports.id) as countid,max(abandoned_payment_reports.created_at) as paymentcreated_date,ROUND(course_user.progress) as progress,users.first_name,users.username,users.email,users.mobile_number,courses.name as course_name
                   FROM  abandoned_payment_reports
                   LEFT JOIN course_user ON course_user.user_id = abandoned_payment_reports.user_id AND course_user.course_id = abandoned_payment_reports.course_id
                   LEFT JOIN users ON users.id = abandoned_payment_reports.user_id
                   LEFT JOIN courses ON courses.id = abandoned_payment_reports.course_id
                   $where GROUP BY abandoned_payment_reports.user_id,abandoned_payment_reports.course_id ORDER BY abandoned_payment_reports.created_at DESC LIMIT $start,$pageSize"; //  OFFSET $pageSize"; //  . $start . ", OFFSET " . $pageSize;


        $resultSet = DB::select($query);

        $resultSet = collect($resultSet)->map(function ($x) {
            $data = (array)$x;
            $data['first_name'] = $data['first_name'] ?: '--';
            $data['email'] = $data['email'] ?: '-';
            $data['mobile_number'] = $data['mobile_number'] ?: '--';
            $data['course_name'] = $data['course_name'] ?: '--';
            $data['progress'] = $data['progress'] ?: 0;
            $data['countid'] = $data['countid'] ?: '-';
            $data['paymentcreated_date'] = $data['paymentcreated_date'] ? formatDate($data['paymentcreated_date']) : '--';
            return $data;
        })->toArray();


        $count_filter = $countResult['totalRecord'];
        $count_total = $countResult['totalRecord'];

        $data_tables = [
            "draw" => $requestData['draw'],
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
            "data" => $resultSet
        ];

        return response()->json($data_tables);
    }


    public function getAjaxExportData($request)
    {
        $requestData = $request->all();

        $where = 'WHERE 1=1';
        if (isset($requestData['partner_user_id'])) {
            $where .= ' AND abandoned_payment_reports.partner_id = ' . $requestData['partner_user_id'];
        } else {
            $where .= ' AND abandoned_payment_reports.partner_id = ' . session('partner_id');
        }


        if (isset($requestData['date_range'])) {
            $data_list = (explode(' - ', $requestData['date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $where .= " AND (abandoned_payment_reports.created_at BETWEEN  '" . $startDate . " 00:00:00' AND '" . $endDate . " 23:59:59')";
        }


        if (isset($requestData['search'])) {
            $search = trim($requestData['search']);
            $where .= " AND (users.first_name LIKE '%$search%' OR users.email LIKE '%$search%' OR users.mobile_number LIKE '%$search%' OR users.username LIKE '%$search%')";
        }


        $query = "Select abandoned_payment_reports.user_id,abandoned_payment_reports.course_id,COUNT(abandoned_payment_reports.id) as countid,max(abandoned_payment_reports.created_at) as paymentcreated_date,ROUND(course_user.progress) as progress,users.first_name,users.username,users.email,users.mobile_number,courses.name as course_name
                   FROM  abandoned_payment_reports
                   LEFT JOIN course_user ON course_user.user_id = abandoned_payment_reports.user_id AND course_user.course_id = abandoned_payment_reports.course_id
                   LEFT JOIN users ON users.id = abandoned_payment_reports.user_id
                   LEFT JOIN courses ON courses.id = abandoned_payment_reports.course_id
                   $where GROUP BY abandoned_payment_reports.user_id,abandoned_payment_reports.course_id ORDER BY abandoned_payment_reports.created_at DESC";

        $resultSet = DB::select($query);


        if (count($resultSet) == 0) {
            return ['status' => false];
        }

        $alldata = [];
        $i = 0;

        foreach ($resultSet as $data) {
            $payment_created_date = isset($data->paymentcreated_date) ? formatDate($data->paymentcreated_date, 'd-m-Y') : null;
            $alldata[$i] = [
                'User Name' => $data->first_name ?? $data->username,
                'User Email' => $data->email,
                'Mobile Number' => $data->mobile_number,
                'Course Name' => $data->course_name,
                'Course Progress (%)' => $data->progress ? $data->progress : 0,
                'Attempt' => $data->countid,
                'Date' => isset($payment_created_date) ? Date::dateTimeToExcel(Carbon::parse($payment_created_date)) : "",
            ];
            $i++;
        }


        $list = array_keys($alldata[0]);
        $data = new AbandonedPaymentReportExport($alldata, $list);
        return ['status' => true, 'data' => $data];

    }

}
