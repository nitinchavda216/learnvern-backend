<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:23 PM
 */

namespace App\Repositories;

use App\Events\UpdateCourseTimeEvent;
use App\Events\UpdateUnitSlugMailEvent;
use App\Interfaces\UnitRepositoryInterface;
use App\Models\Course;
use App\Models\Curriculum;
use App\Models\Section;
use App\Models\UnitFaqs;
use App\Models\Units;
use App\Models\CourseAttachment;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as Writer;
use Spatie\SchemaOrg\Schema;
use Spatie\SchemaOrg\VideoObject;
use Yajra\DataTables\Facades\DataTables;
use App\Exports\UsersExport;
use App\Models\Sluglists;

class UnitRepository implements UnitRepositoryInterface
{
    public function getAjaxListData($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = Curriculum::where('type', 'unit')/*->where('hide_from_backend', $requestData['hide_from_backend'])*/
        ;
        if (isset($requestData['course_id'])) {
            $dataInput = $dataInput->where('course_id', $requestData['course_id']);
            if (isset($requestData['section_id'])) {
                $dataInput = $dataInput->where('section_id', $requestData['section_id']);
            }
        }
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id')->skip($start)->take($pageSize);

        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('course_name', function ($item) {
            return isset($item->courseDetail) ? html_entity_decode($item->courseDetail->name) : "- - -";
        });
        $data_tables->EditColumn('section_name', function ($item) {
            return isset($item->sectionDetail) ? html_entity_decode($item->sectionDetail->name) : "- - -";
        });/*
        $data_tables->EditColumn('hide_status', function ($item) {
            return view('courses.partials.curriculum_hide_status', compact('item'))->render();
        });*/
        $data_tables->EditColumn('action', function ($item) {
            return view('units.partials.actions', compact('item'))->render();
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['action', 'hide_status']);
        return $data_tables->make(true);
    }

    public function getUnitDetails($id)
    {
        return Units::findOrFail($id);
    }

    public function getOldUnitSlugList($id)
    {
        return Sluglists::where(['curriculum_id' => $id, 'curriculum_type' => 'unit'])->orderBy('id', 'DESC')->pluck('slug');
    }

    public function getUnitAttachment($id)
    {
        return CourseAttachment::where('curriculum_type', '=', 'unit')->where('curriculum_id', $id)->get();
    }

    public function storeUnit($request): array
    {
        DB::beginTransaction();
        try {
            $requestData = $request->except('_token');
            $courseDetail = Course::findOrFail($requestData['course_id']);
            if (isset($requestData['video_id'])) {
                $requestData['video_url'] = "https://player.vimeo.com/video/" . $requestData['video_id'];
                $requestData['time'] = getVimeoVideoDuration($requestData['video_url']);
                $external_urls = getVimeoExternalUrl($requestData['video_id']);
                if (!empty($external_urls) && count($external_urls) > 0) {
                    $requestData['video_download_url_1'] = isset($external_urls[0]['link']) ? $external_urls[0]['link'] : NULL;
                    $requestData['video_download_url_2'] = isset($external_urls[1]['link']) ? $external_urls[1]['link'] : NULL;
                    $requestData['video_download_url_3'] = isset($external_urls[2]['link']) ? $external_urls[2]['link'] : NULL;
                }
            }
            if (isset($requestData['youtube_video_url'])) {
                $requestData['youtube_video_id'] = getYouTubeIdFromURL($requestData['youtube_video_url']);
                $requestData['video_embed_url'] = "https://www.youtube.com/embed/" . $requestData['youtube_video_id'];
            }
            $requestData['is_free'] = isset($requestData['is_free']) ? 1 : 0;
            $requestData['is_active'] = 1;
            $requestData['is_global_unit'] = isset($requestData['is_global_unit']) ? 1 : 0;
            $requestData['canonical_url'] = $requestData['canonical_url'] ?? env('FRONT_URL') . $courseDetail['slug'] . '/' . $requestData['slug'];
            $unit = Units::create($requestData);
            foreach ($requestData['question'] as $key => $question) {
                if (isset($question)) {
                    $data = [
                        'unit_id' => $unit['id'],
                        'content' => $question,
                        'answer' => $requestData['answer'][$key] ?? null,
                        'is_active' => '1'
                    ];
                    UnitFaqs::create($data);
                }
            }
            if (isset($requestData['title_attachment'])) {
                foreach ($requestData['title_attachment'] as $key => $value) {
                    $files = isset($requestData['attachment']) ? $requestData['attachment'][$key] : '';
                    if ($files) {
                        $file = self::uploadAttachment($files, $unit, $value);
                    }
                    CourseAttachment::create([
                        'curriculum_id' => $unit->id,
                        'curriculum_type' => 'unit',
                        'title' => $value,
                        'attachment' => isset($file) ? $file : '',
                    ]);
                }
            }
            self::storeSchemaScript($unit->id);
            Curriculum::create([
                'course_id' => $unit->course_id,
                'section_id' => $unit->section_id,
                'curriculum_list_id' => $unit->id,
                'name' => $unit->name,
                'type' => 'unit'
            ]);
            Sluglists::create([
                'course_id' => $unit->course_id,
                'curriculum_id' => $unit->id,
                'curriculum_type' => 'unit',
                'slug' => $unit->slug,
            ]);
            event(new UpdateCourseTimeEvent(['course_id' => [$unit->course_id]]));
            DB::commit();
            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function storeSchemaScript($unit_id)
    {
        $unit = Units::find($unit_id);
        $webPageSchema = Schema::webPage()
            ->name($unit->name)
            ->url(env('FRONT_URL') . '' . $unit->courseDetail->slug . '/' . $unit->slug)
            ->speakable(Schema::speakableSpecification()->xpath([
                "/html/head/title",
                "/html/head/meta[@name='description']/@content"
            ]))->toScript();

        /*VIDEO OBJECT SCHEMA*/
        $videoObjectSchema = Schema::videoObject()
            ->name($unit->name)
            ->description($unit->meta_description ?? $unit->name)
            ->uploadDate(isset($unit->created_at) ? formatDate($unit->created_at, 'Y-m-d h:i:s') : formatDate($unit->updated_at, 'Y-m-d h:i:s'))
            ->thumbnailUrl($unit->courseDetail->course_icon)
            ->if(isset($unit->video_url), function (VideoObject $schema) use ($unit) {
                $schema->contentUrl(env('FRONT_URL') . '' . $unit->courseDetail->slug . '/' . $unit->slug)
                    ->embedUrl($unit->video_url);
            })->if(isset($unit->youtube_video_url), function (VideoObject $schema) use ($unit) {
                $schema->contentUrl(env('FRONT_URL') . '' . $unit->courseDetail->slug . '/' . $unit->slug)
                    ->embedUrl($unit->video_embed_url);
            })->toScript();
        /*VIDEO OBJECT SCHEMA END*/

        /*FAQS SCHEMA*/
        $faqsSchema = "";
        if (count($unit->relatedFaqTitles) > 0) {
            $faqMinEntityData = [];
            foreach ($unit->relatedFaqTitles as $faq) {
                if (isset($faq['answer'])) {
                    $faqMinEntityData[] = Schema::question()->name($faq['content'])->acceptedAnswer(Schema::answer()->text($faq['answer']));
                }
            }
            if (!empty($faqMinEntityData)) {
                $faqsSchema = Schema::fAQPage();
                $faqsSchema->mainEntity($faqMinEntityData);
                $faqsSchema = $faqsSchema->toScript();
            }
        }
        /*FAQS SCHEMA END*/
        /*BREADCRUMBS SCHEMA*/
        $breadcrumbsSchema = Schema::breadcrumbList()
            ->itemListElement([
                Schema::listItem()->position(1)->item(Schema::thing()->identifier(env('FRONT_URL') . 'course/' . $unit->courseDetail->slug)->name($unit->courseDetail->name))
            ])->toScript();
        $schemaScript = $webPageSchema . '
        ' . $videoObjectSchema . '
        ' . $faqsSchema . '
        ' . $breadcrumbsSchema;
        $unit->update(['schema_script' => $schemaScript]);
    }

    public function uploadFile($file, $unit = NULL): string
    {
        $uploadPath = storage_path('app/public/attachments/');
        if (isset($unit)) {
            $imagePath = $uploadPath . $unit->attachment;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();

        $fileName = rand(11111, 99999) . '_unit.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function uploadAttachment($file, $unit = NULL, $title = NULL): string
    {
        $uploadPath = storage_path('app/public/attachments/');
        if (isset($unit)) {
            $imagePath = $uploadPath . $unit->attachment;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $name = str_replace(' ', '_', $title);
        $fileName = $name . '.' . $extension;
        if (is_file($uploadPath . $fileName)) {
            $fileName = $name . '_' . rand(11111, 99999) . '.' . $extension;
        }
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function updateUnit($request, $id): array
    {
        DB::beginTransaction();
        try {
            $requestData = $request->except('_token');
            $unit = self::getUnitDetails($id);
            if (isset($requestData['video_id'])) {
                $requestData['video_url'] = "https://player.vimeo.com/video/" . $requestData['video_id'];
                $requestData['time'] = getVimeoVideoDuration($requestData['video_url']);
                $external_urls = getVimeoExternalUrl($requestData['video_id']);
                if (!empty($external_urls) && count($external_urls) > 0) {
                    $requestData['video_download_url_1'] = isset($external_urls[0]['link']) ? $external_urls[0]['link'] : NULL;
                    $requestData['video_download_url_2'] = isset($external_urls[1]['link']) ? $external_urls[1]['link'] : NULL;
                    $requestData['video_download_url_3'] = isset($external_urls[2]['link']) ? $external_urls[2]['link'] : NULL;
                } /*else {
                    return ['status' => false, 'message' => "Couldn't Add Unit. Problem in vimeo api."];
                }*/
            } else {
                $requestData['video_id'] = $requestData['video_url'] = $requestData['video_download_url_1'] = $requestData['video_download_url_2'] = $requestData['video_download_url_3'] = NULL;
            }
            if (isset($requestData['youtube_video_url'])) {
                $requestData['youtube_video_id'] = getYouTubeIdFromURL($requestData['youtube_video_url']);
                $requestData['video_embed_url'] = "https://www.youtube.com/embed/" . $requestData['youtube_video_id'];
                $requestData['time'] = "00:00:00";
            } else {
                $requestData['youtube_video_url'] = $requestData['youtube_video_id'] = $requestData['video_embed_url'] = NULL;
            }
            if (isset($requestData['title_attachment'])) {
                CourseAttachment::where('curriculum_id', $id)->delete();
                foreach ($requestData['title_attachment'] as $key => $value) {
                    if (isset($requestData['attachment'][$key])) {
                        $files = $requestData['attachment'][$key];
                        $file = self::uploadAttachment($files, $unit, $value);
                    } else {
                        if (isset($requestData['hidden_attachment' . $key])) {
                            $file = $requestData['hidden_attachment' . $key];
                        } else {
                            $file = '';
                        }
                    }
                    CourseAttachment::create([
                        'curriculum_id' => $id,
                        'curriculum_type' => 'unit',
                        'title' => $value,
                        'attachment' => isset($file) ? $file : '',
                    ]);
                }
            }
            $requestData['is_free'] = isset($requestData['is_free']) ? 1 : 0;
            $requestData['is_global_unit'] = isset($requestData['is_global_unit']) ? 1 : 0;
            $requestData['global_unit_conditions'] = ($requestData['is_global_unit'] == 1) ? $requestData['conditions'] : [];
            unset($requestData['conditions'], $requestData['slug']);
            $unit->update($requestData);
            self::storeSchemaScript($unit->id);
            UnitFaqs::where('unit_id', $unit['id'])->forceDelete();
            foreach ($requestData['question'] as $key => $question) {
                if (isset($question)) {
                    $data = [
                        'unit_id' => $unit['id'],
                        'content' => $question,
                        'answer' => $requestData['answer'][$key] ?? null,
                        'is_active' => '1'
                    ];
                    UnitFaqs::create($data);
                }
            }
            $existData = Curriculum::where('curriculum_list_id', $unit->id)->where('type', 'unit')->first();
            if (isset($existData)) {
                $existData->update(['name' => $unit->name, 'course_id' => $unit->course_id, 'section_id' => $unit->section_id]);
                Curriculum::where('curriculum_list_id', $unit->id)->where('type', 'unit')
                    ->where('id', '!=', $existData->id)->update([
                        'name' => $unit->name
                    ]);
            } else {
                Curriculum::create([
                    'course_id' => $unit->course_id,
                    'section_id' => $unit->section_id,
                    'curriculum_list_id' => $unit->id,
                    'name' => $unit->name,
                    'type' => 'unit'
                ]);
            }
            $courseList = Curriculum::where('curriculum_list_id', $unit->id)->where('type', 'unit')->pluck('course_id')->toArray();
            event(new UpdateCourseTimeEvent(['course_id' => $courseList]));
            DB::commit();
            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function updateSlug($request): array
    {
        $requestData = $request->except('_token');
        $exitSlug = Sluglists::where('slug', $requestData['slug'])->first();
        if (is_null($exitSlug)) {
            $unit = Units::where('id', $requestData['unit_id'])->first();
            $emailData = [
                'COURSE_NAME' => $unit->courseDetail->name,
                'UNIT_NAME' => $unit->name,
                'OLD_SLUG_URL' => env('FRONT_URL').$unit->courseDetail->slug.'/'.$unit->slug,
                'NEW_SLUG_URL' => env('FRONT_URL').$unit->courseDetail->slug.'/'.$requestData['slug'],
            ];
            $unit->update(['slug' => $requestData['slug'], 'canonical_url' => $emailData['NEW_SLUG_URL']]);
            self::storeSchemaScript($requestData['unit_id']);
            Sluglists::create([
                'course_id' => $unit->course_id,
                'curriculum_id' => $requestData['unit_id'],
                'curriculum_type' => 'unit',
                'slug' => $requestData['slug'],
            ]);
            event(new UpdateUnitSlugMailEvent($emailData));
            return ['status' => true];
        }
        return ['status' => false, 'message' => 'Slug Already Exist!'];
    }

    public function updateActiveStatus($request): bool
    {
        $unit = Units::find($request->id);
        $unit->is_active = $request->is_active;
        $unit->save();

        Curriculum::where('curriculum_list_id', $unit->id)
            ->where('type', 'unit')->update(['is_active' => $request->is_active]);
        $courseList = Curriculum::where('curriculum_list_id', $unit->id)->where('type', 'unit')->pluck('course_id')->toArray();
        event(new UpdateCourseTimeEvent(['course_id' => $courseList]));
        return true;
    }

    public function getAjaxExportData($request): array
    {
        $requestData = $request->all();
        $dataInput = Units::byActive()->with('relatedFaqTitles')->whereNotNull('id');
        if (isset($requestData['course_id'])) {
            $dataInput = $dataInput->where('course_id', $requestData['course_id']);
        }
        $dataInput = $dataInput->orderBy('id', "DESC")->get();
        if (count($dataInput) == 0) {
            return ['status' => false];
        }
        $alldata = [];
        $i = 0;
        foreach ($dataInput as $data) {
            $alldata[] = [
                'Unit ID' => isset($data->id) ? $data->id : "",
                'Course ID' => $data->course_id,
                'Unit Name' => isset($data->name) ? $data->name : "",
                'Unit Slug' => isset($data->slug) ? $data->slug : "",
                'Unit Video Url' => isset($data->video_url) ? $data->video_url : "",
                'Unit Short Content' => isset($data->short_content) ? $data->short_content : "",
                'Meta Title' => isset($data->meta_title) ? $data->meta_title : "",
                'Meta Description' => isset($data->meta_description) ? $data->meta_description : "",
                'Keyword' => isset($data->meta_keywords) ? $data->meta_keywords : "",
                'Schema' => isset($data->schema_script) ? $data->schema_script : "",
                'Canonical Url' => isset($data->canonical_url) ? $data->canonical_url : "",
                'Faq Question 1' => isset($data->relatedFaqTitles[0]) ? $data->relatedFaqTitles[0]->content : "",
                'Faq Answer 1' => isset($data->relatedFaqTitles[0]) ? $data->relatedFaqTitles[0]->answer : "",
                'Faq Question 2' => isset($data->relatedFaqTitles[1]) ? $data->relatedFaqTitles[1]->content : "",
                'Faq Answer 2' => isset($data->relatedFaqTitles[1]) ? $data->relatedFaqTitles[1]->answer : "",
                'Faq Question 3' => isset($data->relatedFaqTitles[2]) ? $data->relatedFaqTitles[2]->content : "",
                'Faq Answer 3' => isset($data->relatedFaqTitles[2]) ? $data->relatedFaqTitles[2]->answer : "",
                'Faq Question 4' => isset($data->relatedFaqTitles[3]) ? $data->relatedFaqTitles[3]->content : "",
                'Faq Answer 4' => isset($data->relatedFaqTitles[3]) ? $data->relatedFaqTitles[3]->answer : "",
                'Faq Question 5' => isset($data->relatedFaqTitles[4]) ? $data->relatedFaqTitles[4]->content : "",
                'Faq Answer 5' => isset($data->relatedFaqTitles[4]) ? $data->relatedFaqTitles[4]->answer : "",
            ];
            $i++;
        }
        $list = array_keys($alldata[0]);
        $data = new UsersExport($alldata, $list);
        $name = isset($requestData['course_id']) ? $dataInput[0]->courseDetail->name . ' Units.xlsx' : "All Units.xlsx";
        return ['status' => true, 'data' => $data, 'name' => $name];
    }

    public function importUpdateUnit($request): bool
    {
        $requestData = $request->all();
        $reader = new Xlsx();
        $spreadsheet = $reader->load($requestData['file']);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        if (count($sheetData) > 0) {
            unset($sheetData[0]);
            foreach ($sheetData as $sheetDatum) {
                $unitData = Units::where('id', $sheetDatum[0])->first();
                if (isset($unitData)) {
                    $unitData->update([
                        'short_content' => $sheetDatum[5],
                        'meta_title' => $sheetDatum[6],
                        'meta_description' => $sheetDatum[7],
                        'meta_keywords' => $sheetDatum[8],
                        'schema_script' => $sheetDatum[9],
                        'canonical_url' => $sheetDatum[10],
                    ]);
                    UnitFaqs::where('unit_id', $sheetDatum[0])->forceDelete();
                    $faqs = array_slice($sheetDatum, 11, 10);
                    $questions = $answers = [];
                    foreach ($faqs as $key => $faq) {
                        if ($key % 2 == 0) {
                            $questions[] = $faq;
                        } else {
                            $answers[] = $faq;
                        }
                    }
                    foreach ($questions as $key => $question) {
                        if (isset($question)) {
                            UnitFaqs::create([
                                'unit_id' => $sheetDatum[0],
                                'content' => $question,
                                'answer' => $answers[$key] ?? null,
                                'is_active' => 1]);
                        }
                    }
                    self::storeSchemaScript($unitData->id);
                }
            }
        }
        return true;
    }

    public function exportSampleUnitExcel($request)
    {
        $requestData = $request->all();
        $sections = Section::where('course_id', $requestData['course_id'])->where('is_active', 1)->pluck('name', 'id')->toArray();
        $spreadsheet = new Spreadsheet();
        $dummySeat = $spreadsheet->createSheet();
        $dummySeat->setTitle('Worksheet2');
        $a = 'A';
        $i = 1;
        foreach ($sections as $key => $value) {
            $dummySeat->setCellValue($a . $i, $value);
            $i++;
        }
        $sheet = $spreadsheet->getActiveSheet();
        $predefinedColumn = ['Course Id', 'Section', 'Name', 'Slug', 'Video Id', 'Content', 'Short Content'];
        $alpha = 'A';
        foreach ($predefinedColumn as $value) {
            $sheet->setCellValue($alpha . '1', $value);
            $sheet->getColumnDimension($alpha)->setAutoSize(true);
            $alpha++;
        }
        $sheet->setCellValue('A2', $requestData['course_id']);
        for ($i = 2; $i <= 500; $i++) {
            $validation = $spreadsheet->getActiveSheet()->getCell('B' . $i)->getDataValidation();
            $validation->setType(DataValidation::TYPE_LIST);
            $validation->setErrorStyle(DataValidation::STYLE_INFORMATION);
            $validation->setAllowBlank(false);
            $validation->setShowInputMessage(true);
            $validation->setShowErrorMessage(true);
            $validation->setShowDropDown(true);
            $validation->setErrorTitle('Input error');
            $validation->setError('Value is not in list.');
            $validation->setPromptTitle('Pick from list');
            $validation->setPrompt('Please pick a value from the drop-down list.');
            $validation->setFormula1('Worksheet2!A1:A200');
        }
        $writer = new Writer($spreadsheet);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Units Sample.xlsx"');
        $writer->save("php://output");
        return true;
    }

    public function importNewUnits($request): bool
    {
        $requestData = $request->all();
        $reader = new Xlsx();
        $spreadsheet = $reader->load($requestData['file']);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        if (count($sheetData) > 0) {
            unset($sheetData[0]);
            foreach ($sheetData as $sheetDatum) {
                if ($sheetDatum[0] != "") {
                    $courseExist = Course::where('id', $sheetDatum[0])->first();
                    if (isset($courseExist)) {
                        $section = Section::where('course_id', $sheetDatum[0])->where('name', $sheetDatum[1])->first();
                        if (isset($section)) {
                            $slug = $sheetDatum[3];
                            $slugExist = Sluglists::where('slug', $slug)->first();
                            if (isset($slugExist)) {
                                $slug = $sheetDatum[3] . '_' . time();
                            }
                            $video_id = $sheetDatum[4];
                            if (isset($video_id)) {
                                $video = "https://player.vimeo.com/video/" . $video_id;
                                $time = getVimeoVideoDuration($video);
                                $external_urls = getVimeoExternalUrl($video_id);
                                if (!empty($external_urls) && count($external_urls) > 0) {
                                    $video_download_url_1 = isset($external_urls[0]['link']) ? $external_urls[0]['link'] : null;
                                    $video_download_url_2 = isset($external_urls[1]['link']) ? $external_urls[1]['link'] : null;
                                    $video_download_url_3 = isset($external_urls[2]['link']) ? $external_urls[2]['link'] : null;
                                }
                            }
                            $unit = Units::create([
                                'course_id' => $sheetDatum[0],
                                'section_id' => $section->id,
                                'name' => $sheetDatum[2],
                                'slug' => $slug,
                                'content' => $sheetDatum[5],
                                'short_content' => $sheetDatum[5],
                                'video_id' => ($sheetDatum[4] != "") ? $sheetDatum[4] : null,
                                'video_url' => $video ?? null,
                                'video_download_url_1' => $video_download_url_1 ?? null,
                                'video_download_url_2' => $video_download_url_2 ?? null,
                                'video_download_url_3' => $video_download_url_3 ?? null,
                                'time' => $time ?? null,
                                'is_active' => 0
                            ]);
                            Curriculum::create([
                                'course_id' => $unit->course_id,
                                'section_id' => $unit->section_id,
                                'curriculum_list_id' => $unit->id,
                                'name' => $unit->name,
                                'type' => 'unit',
                                'is_active' => 0
                            ]);
                            Sluglists::create([
                                'course_id' => $unit->course_id,
                                'curriculum_id' => $unit->id,
                                'curriculum_type' => 'unit',
                                'slug' => $slug,
                            ]);
                            self::storeSchemaScript($unit->id);
                        }
                    }
                }
            }
        }
        return true;
    }

    public function CheckSlugExists($request): array
    {
        $UnitSlugCheck = Sluglists::where('slug', '=', $request['slug'])->first();
        if ($UnitSlugCheck) {
            return ['status' => true, 'message' => "Slug Already Exists!"];
        } else {
            return ['status' => false];
        }
    }

}
