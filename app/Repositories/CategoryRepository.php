<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08-02-2021
 * Time: 02:19 PM
 */

namespace App\Repositories;

use App\Interfaces\CategoryRepositoryInterface;
use App\Models\Categories;
use App\Models\Course;

class CategoryRepository implements CategoryRepositoryInterface
{
    public function getAllCategory()
    {
        return Categories::orderBy('sort_order', 'ASC')->get();
    }

    public function getWebinarCategoriesTitles()
    {
        return Categories::where('is_webinar_category', 1)->pluck('name', 'id')->toArray();
    }

    public function getCourseCategoriesTitles()
    {
        return Categories::where('is_webinar_category', 0)->pluck('name', 'id')->toArray();
    }

    public function getCategoriesTitles()
    {
        return Categories::pluck('name', 'id')->toArray();
    }

    public function storeCategoryData($request)
    {
        $requestData = $request->all();
        $requestData['sort_order'] = Categories::count();
        $requestData['is_active'] = 1;
        $requestData['is_webinar_category'] = isset($requestData['is_webinar_category']) ? 1 : 0;
        if (isset($requestData['category_icon'])) {
            $requestData['category_icon'] = self::uploadIconImage($requestData);
        }
        if (isset($requestData['image'])) {
            $requestData['image'] = self::uploadImage($requestData);
        }
        Categories::create($requestData);
        return true;
    }

    public function uploadImage($requestData, $category = null)
    {
        $file = $requestData['image'];
        $uploadPath = storage_path(Categories::CAT_IMG_PATH);
        if (isset($category)) {
            $imagePath = $uploadPath . $category->image;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function uploadIconImage($requestData, $category = null)
    {
        $file = $requestData['category_icon'];
        $uploadPath = storage_path(Categories::IMG_PATH);
        if (isset($category)) {
            $imagePath = $uploadPath . $category->category_icon;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function getCategoryDetails($id)
    {
        return Categories::where('id', $id)->first();
    }

    public function updateCategoryData($request, $id)
    {
        $requestData = $request->all();
        $requestData['is_webinar_category'] = isset($requestData['is_webinar_category']) ? 1 : 0;
        $category = self::getCategoryDetails($id);
        if (isset($requestData['category_icon'])) {
            $requestData['category_icon'] = self::uploadIconImage($requestData, $category);
        }
        if (isset($requestData['image'])) {
            $requestData['image'] = self::uploadImage($requestData, $category);
        }
        $category->update($requestData);
        return true;
    }

    public function updateActiveStatus($request)
    {
        $category = Categories::find($request->id);
        $category->is_active = $request->is_active;
        $category->save();
        return true;
    }

    public function getSortOrderData()
    {
        return Categories::orderBy('sort_order', 'asc')->get();
    }

    public function updateSortOrder($request)
    {
        try {
            $requestData = $request->all();
            foreach ($requestData['ids'] as $sortOrder => $id) {
                $curriculum = Categories::where('id', $id)->first();
                if (isset($curriculum)) {
                    $curriculum->update(['sort_order' => $sortOrder]);
                }
            }
            return ['status' => true, 'message' => "Sort Order Updated Successfully!"];
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function getCoursesByCategory()
    {
        return Categories::with(['courses' => function($q){
            $q->where('course_type', '!=', 4);
        }])->whereHas('courses', function($q){
            $q->where('course_type', '!=', 4);
        })->where('is_webinar_category', 0)->get();

    }
}
