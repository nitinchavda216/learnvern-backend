<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:21 PM
 */

namespace App\Repositories;

use App\Interfaces\FaqsRepositoryInterface;
use App\Models\Faqs;
use Yajra\DataTables\Facades\DataTables;

class FaqsRepository implements FaqsRepositoryInterface
{
    public function getAjaxListData($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = Faqs::whereNotNull('id');
        if (isset($requestData['course_id'])) {
            $dataInput = $dataInput->where('course_id', $requestData['course_id']);
        }
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id', "DESC")->skip($start)->take($pageSize);
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('course_name', function ($item) {
            return $item->courseDetail->name ?? "- - -";
        });
        $data_tables->EditColumn('action', function ($item) {
            return view('faqs.partials.actions', compact('item'))->render();
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['action']);
        return $data_tables->make(true);
    }

    public function storeFaqData($request)
    {
        $requestData = $request->except("_token");
        foreach ($requestData['question'] as $key => $question) {
            if (isset($question)) {
                $data = [
                    'course_id' => $requestData['course_id'],
                    'question' => $requestData['question'][$key],
                    'answer' => $requestData['answer'][$key],
                    'is_active' => '1'
                ];
                Faqs::create($data);
            }
        }
        return true;
    }

    public function getFaqDetail($id)
    {
        return Faqs::findOrFail($id);
    }

    public function updateFaqData($request, $id)
    {
        $requestData = $request->all();
        $faq = self::getFaqDetail($id);
        $faq->update($requestData);
        return true;
    }

    public function updateActiveStatus($request)
    {
        $faq = self::getFaqDetail($request->id);
        $faq->is_active = $request->is_active;
        $faq->save();
        return $faq->course_id;
    }

    public function getFaqsByCourse($course_id)
    {
        $returnData = [];
        if (isset($course_id)) {
            $faqs = Faqs::where('course_id', $course_id)->get();
            foreach ($faqs as $faq) {
                $returnData[] = ['id' => $faq->id, 'question' => $faq->question];
            }
        }
        return $returnData;
    }
}
