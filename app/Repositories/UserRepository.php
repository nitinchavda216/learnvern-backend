<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:29 PM
 */

namespace App\Repositories;

use App\Exports\NSDCReportExport;
use App\Interfaces\LocationRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;
use App\Models\CampaignAssign;
use App\Models\CampaignUsers;
use App\Models\Course;
use App\Models\CourseUser;
use App\Models\PaymentTransaction;
use App\Models\Quiz;
use App\Models\QuizUser;
use App\Models\State;
use App\Models\User;

use PDF;
use Carbon\Carbon;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use App\Exports\UsersExport;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Events\SendAmbassadorProcessEmailEvent;


class UserRepository implements UserRepositoryInterface
{
    protected $locationRepository;

    public function __construct(LocationRepositoryInterface $locationRepository)
    {
        $this->locationRepository = $locationRepository;
    }

    public function downloadAmbassadorCertificate($user_id, $referral_count)
    {
        $user = User::where('id', $user_id)->first();
        $html = view('ambassador_pdf_view', compact('user', 'referral_count'))->render();
        $dompdf = PDF::loadHtml($html);
        return ['status' => true, 'data' => $dompdf, 'name' => 'Learnvern_Recommendation_Letterhead_' . $user_id . '_' . $referral_count . '.pdf'];
    }

    public function getNSDCCertificate($request)
    {
        $input = $request->all();
        $course_id = $input['hdn_course_id'];
        $user_id = $input['hdn_user_id'];
        $certificate_details = CourseUser::with(['userDetail' => function ($q) {
            $q->select('id', 'first_name', 'last_name', 'username', 'email', 'mobile_number', 'candidate_id', 'created_at');
        }, 'courseDetail' => function ($q) {
            $q->select('id', 'name', 'course_type');
        }])->where('paid_status', 1)->where('course_id', $course_id)->where('user_id', $user_id)->first();
        if (!isset($certificate_details)) {
            return ['status' => false, 'message' => "User should be not completed the course!"];
        }
        $get_certificate_course_name = DB::select("SELECT certificate_name,candidate_id,(select c.course_certificate_name from courses c where id=$course_id) as course_name,(select user_id from course_user where id=$course_id and user_id=$user_id order by user_id DESC limit 1 ) as certificate_no from users where id=$user_id");
        $certificate_id = $get_certificate_course_name[0]->candidate_id . $certificate_details->id;
        $certificate_details->certificate_id = $certificate_id;
        $certificate_details->save();
        $candidate_id = $get_certificate_course_name[0]->candidate_id;
        $codeContents = env('FRONT_URL') . 'certificate?candidate_id=' . $candidate_id . '&certificate_id=' . $certificate_id;
        $qr_code = QrCode::format('png')->size(100)->generate($codeContents);
        $html = view('nsdc-candidate.certificate_gen', compact('qr_code', 'get_certificate_course_name', 'certificate_details', 'certificate_id'))->render();
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        $pdf = PDF::loadHtml($html);
        return ['status' => true, 'data' => $pdf];
    }

    public function getNSDCCandidatesList($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = CourseUser::with(['userDetail' => function ($q) {
            $q->select('id', 'first_name', 'username', 'email', 'mobile_number', 'candidate_id', 'created_at', 'isd_code');
        }, 'courseDetail' => function ($q) {
            $q->select('id', 'name', 'course_type');
        }])->where('paid_status', 1)->where('is_international_certificate', 0);
        if (isset($requestData['certificate_type']) && $requestData['certificate_type'] == "pending") {
            $dataInput = $dataInput->whereHas('userDetail', function ($q) {
                $q->whereNull('candidate_id');
            });
        } /*else {
            $dataInput = $dataInput->whereHas('userDetail', function ($q) {
                $q->where('country', 'India');
            });
        }*/
        if (isset($requestData['partner_user_id'])) {
            $dataInput = $dataInput->where('partner_id', $requestData['partner_user_id']);
        } else {
            $dataInput = $dataInput->where('partner_id', session('partner_id'));
        }
        if (isset($requestData['date_range'])) {
            $data_list = (explode(' - ', $requestData['date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereHas('userDetail', function ($q) use ($startDate, $endDate) {
                $q->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            });
        }
        /*if (isset($requestData['payment_date_range'])) {
            $data_list = (explode(' - ', $requestData['payment_date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereHas('paymentDetail', function ($q) use ($startDate, $endDate) {
                $q->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            });
        }*/
        if (isset($requestData['search'])) {
            $searchTerm = $requestData['search'];
            $dataInput = $dataInput->where(function ($qq) use ($searchTerm) {
                $qq->whereHas('userDetail', function ($q) use ($searchTerm) {
                    $q->where('first_name', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('username', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('email', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('candidate_id', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('mobile_number', 'LIKE', "%{$searchTerm}%");
                })->orWhereHas('courseDetail', function ($q) use ($searchTerm) {
                    $q->where('name', 'LIKE', "%{$searchTerm}%");
                });
            });
        }
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id', "DESC")->skip($start)->take($pageSize);
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('userDetails', function ($data) {
            $date = " " . "<i class=\"fas fa-star\"></i>";
            if ($data->start_date && $data->end_date) {
                $first_name = $data->userDetail->first_name . ' ' . $date;
            } else {
                $first_name = $data->userDetail->first_name;
            }
            $email = $data->userDetail->email ?? "- - -";
            $mobile_number = isset($data->userDetail->mobile_number) ? $data->userDetail->mobile_number : null;
            $isd_code = isset($data->userDetail->isd_code) ? getIsdCode($data->userDetail->isd_code) . ' ' : "";
            $str = "<i class=\"fas fa-user\"></i>" . "&nbsp;&nbsp;&nbsp;" . $first_name . "<br>";
            $str .= "<i class=\"far fa-envelope\"></i>" . "&nbsp;&nbsp;" . $email . "<br>";
            if (isset($mobile_number)) {
                $str .= "<i class=\"fa fa-phone-square\" aria-hidden=\"true\"></i>" . "&nbsp;&nbsp;&nbsp;" . $isd_code . $mobile_number;
            }
            return $str;
        });

        $data_tables->EditColumn('candidate_id', function ($data) {
            return $data->userDetail->candidate_id ?? "- - -";
        });
        $data_tables->EditColumn('certificate_id', function ($data) {
            return $data->certificate_id ?? "- - -";
        });

        $data_tables->EditColumn('courseDetails', function ($data) {
            $course_name = $data->courseDetail->name ?? "- - -";
            $course_type = Course::COURSE_TYPES[$data->courseDetail->course_type] ?? "- - -";
            $str = "<b>" . $course_name . "</b><br>";
            $str .= "<b>Type :</b>  " . $course_type . "<br>";
            $str .= "<b>Progress :</b>  " . $data->progress . " %";
            return $str;
        });
        $data_tables->EditColumn('payment_date', function ($data) {
            return isset($data->paymentDetail) ? formatDate($data->paymentDetail->created_at) : "- - -";
        });
        $data_tables->EditColumn('action', function ($data) {
            if (isset($data->userDetail->candidate_id) && ($data->progress > 99)) {
                return view('nsdc-candidate.action', compact('data'))->render();
            } else {
                return "- - -";
            }
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['download_certificate', 'action', 'courseDetails', 'userDetails']);

        return $data_tables->make(true);
    }

    public function getAjaxListData($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = User::whereNotNull('id');
        if (isset($requestData['partner_user_id'])) {
            $dataInput = $dataInput->where('partner_id', $requestData['partner_user_id']);
        } else {
            $dataInput = $dataInput->where('partner_id', session('partner_id'));
        }
        if (isset($requestData['date_range'])) {
            $data_list = (explode(' - ', $requestData['date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
        }
        if (isset($requestData['search'])) {
            $searchTerm = $requestData['search'];
            $dataInput = $dataInput->where(function ($q) use ($searchTerm) {
                $q->where('first_name', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('username', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('email', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('mobile_number', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('own_referral_code', 'LIKE', "%{$searchTerm}%");
            });
        }
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->skip($start)->take($pageSize);
        $dataInput = $dataInput->orderBy('id', "DESC")->skip($start)->take($pageSize);
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('first_name', function ($user) {
            return $user->first_name;
        });
        $data_tables->EditColumn('username', function ($user) {
            return isset($user->username) ? $user->username : "- - -";
        });
        $data_tables->EditColumn('email', function ($user) {
            return isset($user->email) ? $user->email : "- - -";
        });
        $data_tables->EditColumn('mobile_number', function ($user) {
            $isd_code = (isset($user->isd_code) ? getIsdCode($user->isd_code) . ' ' : "");
            return isset($user->mobile_number) ? $isd_code . '' . $user->mobile_number : "- - -";
        });
        $data_tables->EditColumn('email_verify', function ($user) {
            if ($user->is_active == 1) {
                $verifyicon = "<i class=\"fa fa-check-circle\" aria-hidden=\"true\"
                                                                   style=\"color: green;\"> Active</i>";
            } else {
                $verifyicon = "<i class=\"fas fa-times-circle\" aria-hidden=\"true\"
                                                                   style=\"color: red;\"> Inactive</i>";
            }
            return $verifyicon;
        });
        $data_tables->EditColumn('action', function ($user) {
            return view('user.action', compact('user'))->render();
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['action', 'email_verify']);
        return $data_tables->make(true);
    }

    public function updateBlockStatus($request)
    {
        $user = User::find($request->id);
        $user->is_blocked = $request->is_active;
        $user->save();
        return true;
    }

    public function getUserByRegister($request)
    {
        $requestData = $request->except("_token");
        $returnData = [];
        if (isset($requestData['created_at'])) {
            $user = User::where('created_at', $requestData['created_at']);
            if (isset($requestData['searchTerm'])) {
                $user = $user->where('username', "Like", "%" . $requestData['searchTerm'] . "%");
            }
            $user = $user->get();
            foreach ($user as $users) {
                $returnData[] = ['id' => $users->id, 'text' => $users->name];
            }
        }
        return $returnData;
    }

    public function getUserDetails($id)
    {
        return User::findOrFail($id);
    }


    public function updateUser($request, $id)
    {
        $requestData = $request->all();
        $user = self::getUserDetails($id);
        if (isset($user)) {
            $cityDetail = $this->locationRepository->getCityDetailById($requestData['city_id']);
            $requestData['birth_date'] = Carbon::parse($requestData['birth_date'])->toDateString();
            $requestData['country'] = $cityDetail->stateDetail->countryDetail->name;
            $requestData['state'] = $cityDetail->stateDetail->name;
            $requestData['city'] = $cityDetail->name;
            $requestData['isd_code'] = '+' . $cityDetail->stateDetail->countryDetail->isd_code;
            $user->update($requestData);
        }
        return true;
    }

    public function getCourses($request)
    {
        $requestData = $request->except("_token");
        $returnData = [];
        if (isset($requestData['course_id'])) {
            $course = CourseUser::where('course_id', $requestData['course_id']);
            if (isset($requestData['searchTerm'])) {
                $course = $course->where('name', "Like", "%" . $requestData['searchTerm'] . "%");
            }
            $course = $course->get();
            foreach ($course as $courses) {
                $returnData[] = ['id' => $courses->id, 'text' => $courses->name];
            }
        }
        return $returnData;
    }

    public function getAllUser()
    {
        $last_month = User::where('partner_id', session('partner_id'))->whereYear('created_at', Carbon::now()->year)
            ->whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->count();
        $current_month = User::where('partner_id', session('partner_id'))->whereYear('created_at', Carbon::now()->year)
            ->whereMonth('created_at', Carbon::now()->month)->count();

        if ($current_month > 0) {
            $result = round((($current_month - $last_month) / $current_month) * 100, 2);
        } else {
            $result = 100;
        }

        $last_month_course = CourseUser::where('partner_id', session('partner_id'))->whereYear('course_enroll_date', Carbon::now()->year)
            ->whereMonth('course_enroll_date', '=', Carbon::now()->subMonth()->month)->count();


        $current_month_course = CourseUser::where('partner_id', session('partner_id'))->whereYear('course_enroll_date', Carbon::now()->year)
            ->whereMonth('course_enroll_date', Carbon::now()->month)->count();

        if ($current_month_course > 0) {
            $res = round((($current_month_course - $last_month_course) / $current_month_course) * 100, 2);
        } else {
            $res = 100;
        }


        $data = [
            'total_user' => User::where('partner_id', session('partner_id'))->whereYear('created_at', Carbon::now()->year)
                ->whereMonth('created_at', Carbon::now()->month)->count(),

            'course_enroll' => CourseUser::where('partner_id', session('partner_id'))->whereYear('course_enroll_date', Carbon::now()->year)
                ->whereMonth('course_enroll_date', Carbon::now()->month)->count(),
            'percentage' => $result,
            'course_percentage' => $res
        ];
        return $data;
    }


    public function getCourseSearchListData($request)
    {
        $query = $request->get('term', '');
        $data = User::select('id', 'own_referral_code', 'first_name', 'last_name', 'email', 'username', 'mobile_number', 'partner_id')
            ->where("is_active", 1)
            ->where("partner_id", 1)
            ->where(function ($q) use ($query) {
                $q->where("own_referral_code", "LIKE", "%{$query}%")
                    ->orwhere("first_name", "LIKE", "%{$query}%")
                    ->orwhere("last_name", "LIKE", "%{$query}%")
                    ->orwhere("email", "LIKE", "%{$query}%")
                    ->orwhere("username", "LIKE", "%{$query}%")
                    ->orwhere("mobile_number", "LIKE", "%{$query}%");
            })
            ->take(20)->get();
        $returnData = array();
        foreach ($data as $datum) {
            $returnData[] = array(
                'value' => $datum->first_name ?? '-',
                'id' => $datum->id,
                'email' => $datum->email ?? '-',
                'mobilenumber' => $datum->mobile_number ?? '-',
            );
        }
        if (count($returnData))
            return $returnData;
        else
            return ['value' => 'No Result Found', 'id' => ''];

    }


    /**
     * @param $request
     * @param null $adminUserId
     * @return array|string[]
     */
    public function getCampaignUsersSearchListData($request, $adminUserId = null): array
    {

        $campaignsId = CampaignAssign::query()->where(['assign_to' => $adminUserId])->pluck('campaign_id')->toArray();
        $usersId = CampaignUsers::query()->whereIn('campaign_id', $campaignsId)->pluck('user_id')->toArray();

        $query = $request->get('term', '');
        $data = User::query()->select('id', 'own_referral_code', 'first_name', 'last_name', 'email', 'username', 'mobile_number', 'partner_id')
            ->where("is_active", 1)
            ->where("partner_id", 1);
        $data->whereIn('id', $usersId);
        $data = $data->where(function ($q) use ($query) {
            $q->where("own_referral_code", "LIKE", "%{$query}%")
                ->orwhere("first_name", "LIKE", "%{$query}%")
                ->orwhere("last_name", "LIKE", "%{$query}%")
                ->orwhere("email", "LIKE", "%{$query}%")
                ->orwhere("username", "LIKE", "%{$query}%")
                ->orwhere("mobile_number", "LIKE", "%{$query}%");
        })->take(50)->get();

        $returnData = array();
        foreach ($data as $datum) {
            $returnData[] = array(
                'value' => $datum->first_name,
                'id' => $datum->id,
                'email' => $datum->email,
                'mobilenumber' => $datum->mobile_number,
            );
        }
        if (count($returnData))
            return $returnData;
        else
            return ['value' => 'No Result Found', 'id' => ''];
    }

    public function getActiveUser()
    {
        $data = User::where('is_active', 1)
            ->selectRaw('count(id) data')
            ->get();
        return $data;

    }

    public function getRevenue()
    {
        $userData = User::selectRaw('year(created_at) as year, monthname(created_at) as month, count(id) data')
            ->whereyear('created_at', date('Y'))
            ->groupBy('year', 'month')
            ->orderBy('year', 'desc')
            ->get();

        $courseData = CourseUser::selectRaw('year(course_enroll_date) as year, monthname(course_enroll_date) as month, count(user_id) course')
            ->whereyear('course_enroll_date', date('Y'))
            ->groupBy('year', 'month')
            ->orderBy('year', 'desc')
            ->get();

        $revenue = PaymentTransaction::selectRaw('count(user_id) data')
            ->whereyear('transaction_date', date('Y'))
            ->sum('amount');

        $arraydata['month'] = [];
        $arraydata['data'] = [];
        $arraydata['course'] = [];
        $arraydata['course-month'] = [];
        $arraydata['revenue'] = $revenue;
        foreach ($userData as $datas) {
            array_push($arraydata['month'], $datas['month']);
            array_push($arraydata['data'], $datas['data']);
        }

        foreach ($courseData as $datas) {
            array_push($arraydata['course'], $datas['course']);
            array_push($arraydata['course-month'], $datas['month']);

        }

        return $arraydata;
    }


    public function getMonthRecord()
    {
        $data = [
            'last_month' => User::whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->count(),
            'current_month' => User::whereMonth('created_at', Carbon::now()->month)->count(),
        ];
        return $data;
    }

    public function getNSDCExportData($request)
    {
        $requestData = $request->all();
        $dataInput = CourseUser::with(['courseDetail' => function ($q) {
            $q->select('id', 'name', 'course_type');
        }])->where('paid_status', 1)->where('is_international_certificate', 0);
        if (isset($requestData['partner_user_id'])) {
            $dataInput = $dataInput->where('partner_id', $requestData['partner_user_id']);
        } else {
            $dataInput = $dataInput->where('partner_id', session('partner_id'));
        }
        if (isset($requestData['certificate_type']) && $requestData['certificate_type'] == "pending") {
            $dataInput = $dataInput->whereHas('userDetail', function ($q) {
                $q->whereNull('candidate_id')->where('is_international_certificate', 0);
            });
        } /*else {
            $dataInput = $dataInput->whereHas('userDetail', function ($q) {
                $q->where('country', 'India');
            });
        }*/
        /*if (isset($requestData['payment_date_range'])) {
            $data_list = (explode(' - ', $requestData['payment_date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereHas('paymentDetail', function ($q) use ($startDate, $endDate) {
                $q->whereBetween('transaction_date', [$startDate, $endDate]);
            });
        }*/
        if (isset($requestData['date_range'])) {
            $data_list = (explode(' - ', $requestData['date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereHas('userDetail', function ($q) use ($startDate, $endDate) {
                $q->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            });
        }
        if (isset($requestData['search'])) {
            $searchTerm = $requestData['search'];
            $dataInput = $dataInput->where(function ($qq) use ($searchTerm) {
                $qq->whereHas('userDetail', function ($q) use ($searchTerm) {
                    $q->where('first_name', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('username', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('email', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('candidate_id', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('mobile_number', 'LIKE', "%{$searchTerm}%");
                })->orWhereHas('courseDetail', function ($q) use ($searchTerm) {
                    $q->where('name', 'LIKE', "%{$searchTerm}%");
                });
            });
        }
        $dataInput = $dataInput->orderBy('id', "DESC")->get();
        if (count($dataInput) == 0) {
            return ['status' => false];
        }
        $alldata = [];
        $i = 0;
        foreach ($dataInput as $data) {
            $constituency = "";
            $state_id = null;
            if (isset($data->userDetail->state_id)) {
                $state_id = $data->userDetail->state_id;
            } elseif (isset($data->userDetail->state)) {
                $stateDetail = State::where('name', 'Like', '%' . $data->userDetail->state . '%')->first();
                if (isset($stateDetail)) {
                    $state_id = $stateDetail->id;
                }
            }
            if (isset($state_id) && isset(config('info.constituency')[$state_id])) {
                $constituencies = config('info.constituency')[$state_id];
                if (in_array($state_id, [1, 3, 6, 8, 9, 10, 11, 14, 15, 20, 23, 24, 25, 26, 31, 34, 37, 39])) {
                    $constituency = $constituencies[0];
                } else {
                    $matched_values = preg_grep('/^' . $data->userDetail->city . '\s.*/', $constituencies);
                    if (!empty($matched_values)) {
                        $matched_values = array_values($matched_values);
                        $constituency = $matched_values[0];
                    } else {
                        $constituency = $constituencies[0];
                    }
                }
            }
            $occupation = $data->userDetail->current_occupation ?? "Fresher";
            $birth_date = isset($data->userDetail->birth_date) ? formatDate($data->userDetail->birth_date, 'Y-m-d') . " 00:00:00" : NULL;
            $alldata[$i] = [
                'Salutation' => $data->userDetail->salutation,
                'FullName' => ucfirst($data->userDetail->first_name),
                'Gender' => $data->userDetail->gender,
                'DateofBirth' => (!is_null($birth_date)) ? formatDate($birth_date, 'd/m/Y') : null,
                'EmailId' => $data->userDetail->email,
                'MaritalStatus' => $data->userDetail->marital_status ?? "Single/Unmarried",
                'FatherName' => preg_replace('/[^a-zA-Z0-9_ -]/s', '', ucfirst($data->userDetail->father_name)),
                'MotherName' => preg_replace('/[^a-zA-Z0-9_ -]/s', '', ucfirst($data->userDetail->mother_name)),
                'Religion' => $data->userDetail->religion,
                'category' => $data->userDetail->cast_category,
                'Disability' => ($data->userDetail->disability == 1) ? "Yes" : "No",
                'TypeofDisability' => ($data->userdetail->disability == 1) ? $data->userDetail->disability_type : '',
                'DomicileState' => strtoupper($data->userDetail->state),
                'DomicileDistrict' => strtoupper($data->userDetail->district),
                'IDType' => "Aadhar ID",
                'TypeofAlternateID' => "",
                'AdharReferenceNo' => ($data->userDetail->id_proof == "Aadhar ID") ? $data->userDetail->id_number : '',
                'IDNo' => "",
                'CountryCode' => "INDIA",
                'MobileNumber' => $data->userDetail->mobile_number,
                'EducationLevel' => $data->userDetail->education,
                'PermanentAddressAddress' => $data->userDetail->address,
                'PermanentAddressState' => strtoupper($data->userDetail->state),
                'PermanentAddressDistrict' => strtoupper($data->userDetail->district),
                'PermanentAddressPINCode' => $data->userDetail->pin_code,
                'PermanentAddressCity' => strtoupper($data->userDetail->city),
                'PermanentAddressTehsil' => "",
                'PermanentAddressConstituency' => $constituency,
                'CommunicationSameasPermanentAddress' => 'Yes',
                'CommunicationAddressState' => strtoupper($data->userDetail->state),
                'CommunicationAddressDistrict' => strtoupper($data->userDetail->district),
                'CommunicationAddressPINCode' => $data->userDetail->pin_code,
                'CommunicationAddressCity' => strtoupper($data->userDetail->city),
                'CommunicationAddressTehsil' => "",
                'CommunicationAddressPermanentConstituency' => $constituency,
                'TrainingStatus' => $occupation,
                'PreviousExperienceSector' => ($occupation != "Fresher") ? $data->userDetail->previous_experience_sector : ' ',
                'Noofmonthsofpreviousexperience' => ($occupation != "Fresher") ? $data->userDetail->no_of_months_of_experience : '',
                'Employed' => (($occupation != "Fresher") && ($data->userDetail->is_employed == 1)) ? 'Yes' : 'No',
                'EmploymentStatus' => ($occupation != "Fresher") ? $data->userDetail->employment_status : "",
                'EmploymentDetails' => ($occupation != "Fresher") ? $data->userDetail->employment_details : "",
                'HeardAboutUs' => $data->userDetail->heard_about_us,
                'CandidateID' => $data->userDetail->candidate_id,
            ];
            $i++;
        }
        $list = array_keys($alldata[0]);
        $data = new NSDCReportExport($alldata, $list);
        return ['status' => true, 'data' => $data];
    }

    public function getNSDCUploadData($request)
    {
        $requestData = $request->except('_token');
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load($requestData['file']);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        if (count($sheetData) > 0) {
            unset($sheetData[0]);
            foreach ($sheetData as $sheetDatum) {
                $user = User::where('email', $sheetDatum[0])->select('id', 'email', 'candidate_id')->first();
                if (isset($user)) {
                    $user->update(['candidate_id' => $sheetDatum[1]]);
                    $course_users = CourseUser::where('user_id', $user->id)->whereNull('certificate_date')->where('paid_status', 1)->get();
                    foreach ($course_users as $course_user) {
                        $updateData['certificate_id'] = $sheetDatum[1] . $course_user->id;
                        $updateData['certificate_date'] = Carbon::now()->toDateString();
                        $course_user->update($updateData);
                    }
                }
            }
            return ['status' => true, 'message' => "File has been uploaded successfully."];
        } else {
            return ['status' => false, 'message' => "Uploaded file is empty."];
        }
    }

    public function getTopReferralsUsers($request)
    {
        $date = \Carbon\Carbon::today()->subDays(7);
        if ($request->search == "week") {
            return User::where('created_at', '>=', $date)->select('id', 'own_referral_code', 'referred_count')
                ->orderBy('referred_count', 'DESC')->take(10)->get()->toArray();
        } else if ($request->search == "month") {

            return User::whereMonth('created_at', Carbon::now()->month)->select('id', 'own_referral_code', 'referred_count')
                ->orderBy('referred_count', 'DESC')->take(10)->get()->toArray();
        } else {
            return User::whereDate('created_at', Carbon::today())
                ->select('id', 'own_referral_code', 'referred_count')
                ->orderBy('referred_count', 'DESC')->take(10)->get()->toArray();
        }

    }


    public function getDayWiseData($request)
    {
        $requestData = $request->all();
        $totalUser = User::selectRaw('date(created_at)as date, count(id) userData')
            ->whereYear('created_at', Carbon::now()->year)
            ->whereMonth('created_at', Carbon::now()->month)
            ->groupBy('Date')
            ->orderBy('Date', 'asc')
            ->get();

        $unEnrolled = User::selectRaw('date(created_at)as date, count(id) unEnrolledUser')
            ->whereYear('created_at', Carbon::now()->year)
            ->whereMonth('created_at', Carbon::now()->month)
            ->groupBy('Date')
            ->orderBy('Date', 'asc')
            ->doesntHave('courses')
            ->get();

        $date = [];
        $userCounts = [];
        $unEnrolledUser = [];
        foreach ($totalUser as $data) {
            $date[] = $data['date'];
            $userCounts [] = $data['userData'];
            $unenrolled_total = 0;
            foreach ($unEnrolled as $item) {
                if ($item['date'] == $data['date']) {
                    $unenrolled_total = $item['unEnrolledUser'];
                }

            }
            $unEnrolledUser[] = $unenrolled_total;
        }
        $returnData['date'] = json_encode($date);
        $returnData['totalUser'] = json_encode($userCounts);
        $returnData['unEnrolledUser'] = json_encode($unEnrolledUser);

        return $returnData;

    }

    public function getMonthWiseData()
    {
        $totalUser = User::selectRaw('Month(created_at)as month, count(id) total')
            ->whereYear('created_at', Carbon::now()->year)
            ->groupBy('Month')
            ->get();

        $unEnrolledUser = User::selectRaw('Month(created_at)as month, count(id) uneEnrolled')
            ->whereYear('created_at', Carbon::now()->year)
            ->groupBy('month')
            ->doesntHave('courses')
            ->get();
        $month_names = getMonthList();
        $months = $counts = $referrals = [];

        foreach ($totalUser as $data) {
            $months[] = $month_names[$data['month']];
            $total[] = $data['total'];
            $unenrolled_total = 0;
            foreach ($unEnrolledUser as $item) {
                if ($item['month'] == $data['month']) {
                    $unenrolled_total = $item['uneEnrolled'];
                }
            }
            $unEnrolled[] = $unenrolled_total;
        }

        $returnData['months'] = json_encode($months);
        $returnData['totalUser'] = json_encode($total);
        $returnData['unEnrolled'] = json_encode($unEnrolled);

        return $returnData;

    }

    public function getRecord($request)
    {
        $requestData = $request->all();
        if (isset($requestData['date'])) {
            $day = $requestData['date'];
        } else {
            $year = date('Y-m-d');
        }
        $userData = User::selectRaw('date(created_at)as date, count(id) userData')
            ->whereYear('created_at', Carbon::now()->year)
            ->whereMonth('created_at', Carbon::now()->month)
            ->groupBy('Date')
            ->orderBy('Date', 'asc')
            ->get();

        $courseUserData = CourseUser::selectRaw('date(course_enroll_date)as date, count(id) courseData')
            ->whereYear('course_enroll_date', Carbon::now()->year)
            ->whereMonth('course_enroll_date', Carbon::now()->month)
            ->groupBy('Date')
            ->orderBy('Date', 'asc')
            ->get();

        foreach ($userData as $data) {
            $date[] = $data['date'];
            $userCounts[] = $data['userData'];
        }
        foreach ($courseUserData as $data) {
            $courseUserCount[] = $data['courseData'];
        }
        $returnData['date'] = isset($date) && !empty($date) ? json_encode($date) : [];
        $returnData['userCounts'] = isset($userCounts) && !empty($userCounts) ? json_encode($userCounts) : [];
        $returnData['courseUserCount'] = isset($courseUserCount) && !empty($courseUserCount) ? json_encode($courseUserCount) : [];
        return $returnData;
    }

    public function getTotalReferral($own_referral_code)
    {
        return User::where('referral', $own_referral_code)->get();
    }


    public function updateNSDCCertificateDate($request)
    {
        $requestData = $request->except('_token', 'course_user_id');
        $id = $request->course_user_id;
        CourseUser::where("id", $id)->update($requestData);
        return true;
    }

    public function getInternationalCandidatesList($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = CourseUser::with(['userDetail' => function ($q) {
            $q->select('id', 'first_name', 'username', 'email', 'mobile_number', 'candidate_id', 'created_at', 'isd_code');
        }, 'courseDetail' => function ($q) {
            $q->select('id', 'name', 'course_type');
        }])->where('paid_status', 1)->where('is_international_certificate', 1);
        if (isset($requestData['partner_user_id'])) {
            $dataInput = $dataInput->where('partner_id', $requestData['partner_user_id']);
        } else {
            $dataInput = $dataInput->where('partner_id', session('partner_id'));
        }
        if (isset($requestData['date_range'])) {
            $data_list = (explode(' - ', $requestData['date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereHas('userDetail', function ($q) use ($startDate, $endDate) {
                $q->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            });
        }
        if (isset($requestData['search'])) {
            $searchTerm = $requestData['search'];
            $dataInput = $dataInput->where(function ($qq) use ($searchTerm) {
                $qq->whereHas('userDetail', function ($q) use ($searchTerm) {
                    $q->where('first_name', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('username', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('email', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('candidate_id', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('mobile_number', 'LIKE', "%{$searchTerm}%");
                })->orWhereHas('courseDetail', function ($q) use ($searchTerm) {
                    $q->where('name', 'LIKE', "%{$searchTerm}%");
                });
            });
        }
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id', "DESC")->skip($start)->take($pageSize);
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('userDetails', function ($data) {
            $date = " " . "<i class=\"fas fa-star\"></i>";
            if ($data->start_date && $data->end_date) {
                $first_name = $data->userDetail->first_name . ' ' . $date;
            } else {
                $first_name = $data->userDetail->first_name;
            }
            $email = $data->userDetail->email ?? "- - -";
            $mobile_number = isset($data->userDetail->mobile_number) ? $data->userDetail->mobile_number : null;
            $isd_code = isset($data->userDetail->isd_code) ? getIsdCode($data->userDetail->isd_code) . ' ' : "";
            $str = "<i class=\"fas fa-user\"></i>" . "&nbsp;&nbsp;&nbsp;" . $first_name . "<br>";
            $str .= "<i class=\"far fa-envelope\"></i>" . "&nbsp;&nbsp;" . $email . "<br>";
            if (isset($mobile_number)) {
                $str .= "<i class=\"fa fa-phone-square\" aria-hidden=\"true\"></i>" . "&nbsp;&nbsp;&nbsp;" . $isd_code . $mobile_number;
            }
            return $str;
        });
        $data_tables->EditColumn('courseDetails', function ($data) {
            $course_name = $data->courseDetail->name ?? "- - -";
            $course_type = Course::COURSE_TYPES[$data->courseDetail->course_type] ?? "- - -";
            $str = "<b>" . $course_name . "</b><br>";
            $str .= "<b>Type :  </b>" . $course_type . "<br>";
            $str .= "<b>Progress : </b>" . $data->progress . " %";
            return $str;
        });
        $data_tables->EditColumn('action', function ($data) {
            return view('international_candidate.action', compact('data'))->render();
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['download_certificate', 'action', 'courseDetails', 'userDetails']);
        return $data_tables->make(true);
    }

    public function getInternationalExportData($request)
    {
        $requestData = $request->all();
        $dataInput = CourseUser::with(['courseDetail' => function ($q) {
            $q->select('id', 'name', 'course_type');
        }])->where('paid_status', 1)->where('is_international_certificate', 1);
        if (isset($requestData['partner_user_id'])) {
            $dataInput = $dataInput->where('partner_id', $requestData['partner_user_id']);
        } else {
            $dataInput = $dataInput->where('partner_id', session('partner_id'));
        }
        if (isset($requestData['date_range'])) {
            $data_list = (explode(' - ', $requestData['date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereHas('userDetail', function ($q) use ($startDate, $endDate) {
                $q->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            });
        }
        if (isset($requestData['search'])) {
            $searchTerm = $requestData['search'];
            $dataInput = $dataInput->where(function ($qq) use ($searchTerm) {
                $qq->whereHas('userDetail', function ($q) use ($searchTerm) {
                    $q->where('first_name', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('email', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('mobile_number', 'LIKE', "%{$searchTerm}%");
                })->orWhereHas('courseDetail', function ($q) use ($searchTerm) {
                    $q->where('name', 'LIKE', "%{$searchTerm}%");
                });
            });
        }
        $dataInput = $dataInput->orderBy('id', "DESC")->get();
        if (count($dataInput) == 0) {
            return ['status' => false];
        }
        $alldata = [];
        $i = 0;
        foreach ($dataInput as $data) {
            $alldata[$i] = [
                'Full Name' => ucfirst($data->userDetail->first_name),
                'Email' => $data->userDetail->email,
                'Mobile Number' => $data->userDetail->mobile_number,
                'Country' => strtoupper($data->userDetail->country),
                'State' => strtoupper($data->userDetail->state),
                'City' => strtoupper($data->userDetail->city),
                'PIN Code' => $data->userDetail->pin_code,
                'Certificate ID' => $data->certificate_id,
                'Course Name' => $data->courseDetail->name,
                'Course Type' => $data->courseDetail->course_type,
                'Progress' => $data->progress
            ];
            $i++;
        }
        $list = array_keys($alldata[0]);
        $data = new NSDCReportExport($alldata, $list);
        return ['status' => true, 'data' => $data];
    }

    public function activateUser($activation_key)
    {
        $user = User::select('id', 'first_name', 'email', 'activation_key', 'last_login_ip', 'last_login_date', 'login_count', 'is_login', 'is_active', 'referral', 'referred_count', 'country')->where('activation_key', $activation_key)->first();
        if (isset($user)) {
            $user->update([
                'activation_key' => null,
                'last_login_ip' => getUserIP(),
                'last_login_date' => Carbon::now(),
                'login_count' => 1,
                'is_login' => 1,
                'is_active' => 1,
            ]);
            event(new SendAmbassadorProcessEmailEvent($user));
            return true;
        }
    }

    public function getRegisteredDataByDate($start_date, $end_date)
    {
        return User::where('partner_id', session('partner_id'))
            ->whereBetween('created_at', [$start_date . " 00:00:00", $end_date . " 23:59:59"])
            ->count();
    }

    public function getUserQuizResults($user_id)
    {
        $quiz_result_ds = [];
        $get_user_course = CourseUser::where('user_id', $user_id)->select('c.id as course_id', 'name')
            ->join("courses AS c", function ($join) use ($user_id) {
                $join->on("c.id", "=", "course_user.course_id")->where('course_user.user_id', '=', $user_id);
            })->where('c.is_active', 1)->orderBy('c.sort_order')->get()->toArray();
        foreach ($get_user_course as $v) {
            $user_quiz = Quiz::where('is_active', 1)->where('course_id', $v['course_id'])->get();
            if (count($user_quiz) > 0) {
                $v['quiz'] = [];
                foreach ($user_quiz as $quiz) {
                    $user_quiz_marks = QuizUser::where('user_id', $user_id)->where('quiz_id', $quiz['id'])->orderBy('id', 'desc')->first();
                    /*$user_quiz_marks=$this->general_model->select_row("select *,(select sum(que_mark) from lms_question que where is_active=1 and quiz_id=".$quiz['quiz_id'].") as total_mark from lms_quiz_user qu
                    join lms_quiz q on qu.quiz_id=q.quiz_id where qu.quiz_id=".$quiz['quiz_id']." and qu.user_id=$user_id order by qu.quiz_user_id desc limit 1");*/
                    if (isset($user_quiz_marks)) {
                        $user_quiz_marks->total_mark = $user_quiz_marks->relatedQuizQuestions->where('is_active', 1)->sum('mark');
                        $user_quiz_marks->quiz_name = $user_quiz_marks->quizDetails->name;
                        unset($user_quiz_marks->relatedQuizQuestions, $user_quiz_marks->quizDetails);
                        if ($user_quiz_marks->quizDetails->is_master_quiz == 1) {
                            $v['master_quiz'] = $user_quiz_marks;
                        }
                        $v['quiz'][] = $user_quiz_marks;
                    }
                }
                if (!empty($v['quiz'])) {
                    $quiz_result_ds[] = $v;
                }
            }
        }
        return $quiz_result_ds;
    }
}
