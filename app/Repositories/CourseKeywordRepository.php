<?php

namespace App\Repositories;

use MongoDB\BSON\ObjectId;
use App\Exports\KeywordExport;
use App\Models\CourseKeywords;
use Illuminate\Support\Facades\DB;
use App\Models\MissingSearchKeyword;
use App\Models\KeywordSearchHistory;
use App\Models\MissingKeywordHistory;
use App\Models\KeywordSearchAnalytics;
use Yajra\DataTables\Facades\DataTables;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use App\Exports\KeywordSearchHistoryExport;
use App\Interfaces\CourseKeywordRepositoryInterface;

class CourseKeywordRepository implements CourseKeywordRepositoryInterface {

    public function getListData() {
        return CourseKeywords::orderBy('course_id', 'desc')->get();
    }

    public function getCourseKeywordDetails($id) {
        return CourseKeywords::findOrFail($id);
    }

    public function courseKeywordsImport($request): bool {
        $requestData = $request->all();
        $reader = new Xlsx();
        $spreadsheet = $reader->load($requestData['file']);
        $sheetDatas = $spreadsheet->getActiveSheet()->toArray();
        $sheetDatas = array_column($sheetDatas, 0); // Get value from multidimensional array's 0th key.
        if (count($sheetDatas) > 0 && isset($request->id)) {
            unset($sheetDatas[0]);
            $existingData = CourseKeywords::findOrFail($request->id);
            $keywords = (isJSON($existingData->keywords)) ? json_decode($existingData->keywords) : [];
            $keywords = json_encode(array_iunique(array_filter(array_merge($keywords, $sheetDatas))));
            $existingData->keywords = $keywords;
            $existingData->save();
        }
        return true;
    }

    public function upsertcourseKeywordsData($request, $id) {
        $existingData = CourseKeywords::findOrFail($id);
        $existingData->keywords = json_encode(array_iunique(array_filter(explode(',', $request->search_keywords))));
        $existingData->save();
        return true;
    }

    public function getMissingCourseKeywordsListData($request) {
        $requestData = $request->all();
        $dataInput = MissingSearchKeyword::query();
        if (isset($requestData['search']) && !empty($requestData['search'])) {
            $dataInput = $dataInput->where(function ($query) use ($requestData) {
                $query->where('keyword', 'like', '%' . $requestData['search'] . '%');
                $query->orWhere('attempts', (int) $requestData['search']);
            });
        }
        $dataInput = $dataInput->orderBy('updated_at', 'desc')->get();
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('keyword', function ($item) {
            return $item->keyword ?? "- - -";
        });
        $data_tables->EditColumn('attempts', function ($item) {
            return $item->attempts ?? "1";
        });
        $data_tables->EditColumn('updated_at', function ($item) {
            return $item->updated_at ? date('d-m-Y', strtotime($item->updated_at)) : "- - -";
        });
        $data_tables->EditColumn('actions', function ($item) {
            return view('missing-course-keywords.partials.actions', compact('item'))->render();
        });
        $data_tables->rawColumns(['keyword', 'attempts', 'actions', 'updated_at']);
        return $data_tables->make(true);
    }

    public function missingCourseKeywordsupdate($request): bool {
        $course_ids = array_map('intval', $request->course_id);
        $existingDatas = CourseKeywords::whereIn('course_id', $course_ids)->get();
        foreach ($existingDatas as $existingData) {
            $existing_keywords = json_decode($existingData->keywords, true);
            $existing_keywords[] = trim($request->missingKeyword);
            $existing_keywords = json_encode(array_iunique(array_filter($existing_keywords)));
            $upd_data = CourseKeywords::find($existingData->id);
            $upd_data->keywords = $existing_keywords;
            $upd_data->save();
        }
        MissingSearchKeyword::deleteMissingKeywordWithHistory($request->missingKeywordId);
        return true;
    }

    public function missingCourseKeywordsdestroy($request, $id) {
        MissingSearchKeyword::deleteMissingKeywordWithHistory($id);
        return true;
    }

    public function keywordSearchAnalyticsIndex($request) {
        //
    }

    public function keywordSearchAnalyticsList($request) {
        $requestData = $request->all();
        $dataInput = KeywordSearchAnalytics::query()->orderBy('_id', "DESC");
        if (isset($requestData['course_id']) && !empty($requestData['course_id'])) {
            $dataInput = $dataInput->whereCourse($requestData['course_id']);
        }
        $dataInput = $dataInput->get();
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('keyword', function ($item) {
            return $item->keyword ?? "- - -";
        });
        $data_tables->EditColumn('course_names', function ($item) {
            $course_names = (isset($item->course_ids) && !empty($item->course_ids) && isJSON($item->course_ids)) ? implode(',<br>', self::getCoursesNames($item->course_ids)) : "";
            return $course_names;
        });
        $data_tables->rawColumns(['course_names']);
        return $data_tables->make(true);
    }

    public function getCoursesNames($course_ids) {
        return CourseKeywords::whereIn('course_id', json_decode($course_ids, true))->pluck('course_name')->toArray();
    }

    public function getAjaxExportData($request) {
        $requestData = $request->all();
        $dataInput = KeywordSearchHistory::whereNotNull('_id');
        if (isset($requestData['date_range'])) {
            $data_list = (explode(' - ', $requestData['date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $startDate = new \MongoDB\BSON\UTCDateTime(strtotime($startDate . ' 00:00:00') * 1000);
            $endDate = new \MongoDB\BSON\UTCDateTime(strtotime($endDate . ' 00:00:00') * 1000);
            $dataInput = $dataInput->whereBetween('created_at', [$startDate, $endDate]);
        }
        $dataInput = $dataInput->orderBy('_id', "desc")->get();
        if (count($dataInput) == 0) {
            return ['status' => false];
        }
        $alldata = [];
        $i = 0;
        foreach ($dataInput as $data) {
            $created_at = isset($data->created_at) ? formatDate($data->created_at, 'Y-m-d') . ' 00:00:00' : null;
            $alldata[] = [
                'Keyword' => isset($data->keyword) ? $data->keyword : "",
                'Results' => $data->results,
                'Courses' => (isset($data->course_ids) && !empty($data->course_ids) && isJSON($data->course_ids)) ? implode(PHP_EOL, self::getCoursesNames($data->course_ids)) : "",
            ];
            $i++;
        }
        $list = array_keys($alldata[0]);
        $data = new KeywordSearchHistoryExport($alldata, $list);
        return ['status' => true, 'data' => $data];
    }

    public function getKeywordExportData($request) {
        $requestData = $request->all();
        if (!isset($requestData['course_id']) || empty($requestData['course_id'])) {
            return ['status' => false];
        }
        $dataInput = CourseKeywords::where('_id', $requestData['course_id'])->first();
        if (empty($dataInput)) {
            return ['status' => false];
        }
        $alldata = [];
        if (isset($dataInput->keywords) && isJSON($dataInput->keywords) && !empty($dataInput->keywords)) {
            $keywords = json_decode($dataInput->keywords, true);
            if (count($keywords) == 0) {
                $keywords['keywords'] = null;
            }
        }
        foreach ($keywords as $keyword) {
            $alldata[] = [
                'Keywords' => isset($keyword) ? $keyword : "",
            ];
        }
        $list = array_keys($alldata[0]);
        $data = new KeywordExport($alldata, $list);
        return ['status' => true, 'data' => $data];
    }

    public function missingKeywordHistoryList($id) {
        return MissingSearchKeyword::select('keyword')->where('_id', $id)->first();
    }

    public function missingKeywordHistoryListData($request) {
        $requestData = $request->all();
        $dataInput = MissingKeywordHistory::with('userDetails');
        if (isset($requestData['keyword_id']) && !empty($requestData['keyword_id'])) {
            $dataInput = $dataInput->where('keyword_id', $requestData['keyword_id']);
        }
        $dataInput = $dataInput->orderBy('_id', "DESC")->get();
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('full_name', function ($item) {
            return $item->userDetails->first_name ?? "- - -";
        });
        $data_tables->EditColumn('city', function ($item) {
            $city = $item->userDetails->city ?? $item->city;
            return !empty($city) ? $city : "- - -";
        });
        $data_tables->EditColumn('state', function ($item) {
            $state = $item->userDetails->state ?? $item->state;
            return !empty($state) ? $state : "- - -";
        });
        $data_tables->EditColumn('country', function ($item) {
            $country = $item->userDetails->country ?? $item->country;
            return !empty($country) ? $country : "- - -";
        });
        $data_tables->EditColumn('isd_code', function ($item) {
            return $item->userDetails->isd_code ?? "- - -";
        });
        $data_tables->EditColumn('mobile_number', function ($item) {
            return $item->userDetails->mobile_number ?? "- - -";
        });
        $data_tables->EditColumn('email', function ($item) {
            return $item->userDetails->email ?? "- - -";
        });
        $data_tables->EditColumn('pin_code', function ($item) {
            $pin_code = $item->userDetails->pin_code ?? $item->pin_code;
            return !empty($pin_code) ? $pin_code : "- - -";
        });
        $data_tables->EditColumn('last_attempt', function ($item) {
            return $item->updated_at ?? "- - -";
        });
        $data_tables->rawColumns(['full_name', 'city', 'state', 'country', 'isd_code', 'mobile_number', 'email', 'pin_code', 'last_attempt']);
        return $data_tables->make(true);
    }

}
