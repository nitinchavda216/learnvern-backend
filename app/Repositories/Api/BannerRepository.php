<?php
/**
 * Created by PhpStorm.
 * User: -
 * Date: 10-07-19
 * Time: 03:24 PM
 */

namespace App\Repositories\Api;

use App\Interfaces\Api\BannerRepositoryInterface;
use App\Models\Banners;
use App\Models\Course;

class BannerRepository implements BannerRepositoryInterface
{
    public function getBannersListByType()
    {
        $banners = Banners::select('title', 'image')->whereIn('type', ['promotion_banner']);

        $banners = $banners->get();
        $banners = $banners->filter(function ($item) {
            $item->image = showBannerImage($item->image);
            return $item;
        });
        return $banners->toArray();
    }

    public function getPromotionBanners()
    {
        $returnData['with_login_banners'] = $returnData['without_login_banners'] = [];
        $banners = Banners::select('title', 'image', 'link', 'user_auth_type')->where('type', 'promotion_banner')->get();
        foreach ($banners as $banner) {
            $arrayData = [
                'title' => $banner->title,
                'image' => isset($banner->image) ? showBannerImage($banner->image) : "",
                'link' => $banner->link ?? "",
            ];
            if (($banner->user_auth_type == "any") || ($banner->user_auth_type == "with_login")){
                $returnData['with_login_banners'][] = $arrayData;
            }
            if (($banner->user_auth_type == "any") || ($banner->user_auth_type == "without_login")){
                $returnData['without_login_banners'][] = $arrayData;
            }
        }
        return $returnData;
    }

    public function getConfigurationBanners()
    {
        $banners = Banners::select('image', 'type')->whereIn('type', ['referral_banner', 'nsdc_banner'])->get();
        $returnData = [
            'referral_banner' => "",
            'nsdc_banner' => "",
        ];
        foreach ($banners as $banner) {
            if ($banner->type == 'referral_banner') {
                $returnData['referral_banner'] = showBannerImage($banner->image);
            } else {
                $returnData['nsdc_banner'] = showBannerImage($banner->image);
            }
        }
        return $returnData;
    }

    public function getBannerImageByType($type)
    {
        $banner = Banners::select('image', 'type')->where('type', $type)->first();
        return isset($banner->image) ? showBannerImage($banner->image) : "";
    }

    public function getBannerDetails($id)
    {
        return Banners::findOrFail($id);
    }

    public function getAllBanners()
    {
        return Banners::get();
    }

    public function storeBannerData($request)
    {
        $requestData = $request->except('_token');
        $requestData['link_course_id'] = ($requestData['link_type'] == 'course_page') ? $requestData['link_course_id'] : null;
        if ($requestData['link_type'] == "course_page") {
            $course = Course::where('id', $requestData['link_course_id'])->select('id', 'course_type')->first();
            $requestData['link'] = env('FCM_DYNAMIC_LINK_DOMAIN') . '?type=' . (($course->course_type == 4) ? 'webinar' : 'course') . '&id=' . $course->id;
        } elseif ($requestData['link_type'] == "referral_page") {
            $requestData['link'] = env('FCM_DYNAMIC_LINK_DOMAIN') . '?type=referral';
        } elseif ($requestData['link_type'] == "my_courses_page") {
            $requestData['link'] = env('FCM_DYNAMIC_LINK_DOMAIN') . '?type=my_courses';
        } elseif ($requestData['link_type'] == "my_downloads_page") {
            $requestData['link'] = env('FCM_DYNAMIC_LINK_DOMAIN') . '?type=my_downloads';
        }
        if (isset($requestData['image'])) {
            $requestData['image'] = self::uploadFile($requestData['image']);
        }
        Banners::create($requestData);
        return true;
    }

    public function updateBannerData($request, $id)
    {
        $requestData = $request->except('_token');
        $banners = Banners::find($id);
        if ($banners) {
            if ($requestData['type'] == 'promotion_banner') {
                $requestData['link_course_id'] = ($requestData['link_type'] == 'course_page') ? $requestData['link_course_id'] : null;
                if ($requestData['link_type'] == "course_page") {
                    $course = Course::where('id', $requestData['link_course_id'])->select('id', 'course_type')->first();
                    $requestData['link'] = env('FCM_DYNAMIC_LINK_DOMAIN') . '?type=' . (($course->course_type == 4) ? 'webinar' : 'course') . '&id=' . $course->id;
                } elseif ($requestData['link_type'] == "referral_page") {
                    $requestData['link'] = env('FCM_DYNAMIC_LINK_DOMAIN') . '?type=referral';
                } elseif ($requestData['link_type'] == "my_courses_page") {
                    $requestData['link'] = env('FCM_DYNAMIC_LINK_DOMAIN') . '?type=my_courses';
                } elseif ($requestData['link_type'] == "my_downloads_page") {
                    $requestData['link'] = env('FCM_DYNAMIC_LINK_DOMAIN') . '?type=my_downloads';
                }
            } else {
                $requestData['link_type'] = $requestData['link'] = $requestData['link_course_id'] = $requestData['user_auth_type'] = null;
            }
            if (isset($requestData['image'])) {
                $requestData['image'] = self::uploadFile($requestData['image'], $banners);
            }
            $banners->update($requestData);
        }
        return true;
    }

    public function deleteBanner($id)
    {
        $banners = Banners::find($id);
        if ($banners) {

            $banners->delete();
        }
        return true;
    }

    public function uploadFile($file, $banner = NULL)
    {
        $uploadPath = storage_path(Banners::IMG_PATH);

        if (isset($banner)) {
            $imagePath = $uploadPath . $banner->attachment;
            @unlink($imagePath);
        }

        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '_banner.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

}
