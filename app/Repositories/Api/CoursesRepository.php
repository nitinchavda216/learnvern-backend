<?php
/**
 * Created by PhpStorm.
 * User: -
 * Date: 10-07-19
 * Time: 03:24 PM
 */

namespace App\Repositories\Api;

use App\Interfaces\Api\CoursesRepositoryInterface;
use App\Interfaces\SiteConfigurationsRepositoryInterface;
use App\Models\Assignment;
use App\Models\AssignmentUser;
use App\Models\Categories;
use App\Models\Comments;
use App\Models\Course;
use App\Models\CourseUser;
use App\Models\Curriculum;
use App\Models\Faqs;
use App\Models\SiteConfiguration;
use App\Models\Units;
use App\Models\UnitUser;
use App\Models\User;
use App\Models\UserInterests;
use App\Models\UserResumeDetails;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CoursesRepository implements CoursesRepositoryInterface
{
    protected $siteConfigurationsRepository;

    public function __construct()
    {
        $this->siteConfigurationsRepository = app()->make(SiteConfigurationsRepositoryInterface::class);
    }

    public function allCourseList()
    {
        $returnData['webinars'] = $returnData['courses'] = [];
        $courseList = Course::select('id', 'name', 'price', 'course_language', 'image', 'app_icon', 'app_image', 'course_type', 'comment_count', 'average_rating', 'num_of_student')
            ->byActive()->orderBy('sort_order', 'ASC')->get()->toArray();
        foreach ($courseList as $course) {
            $course['average_rating'] = $course['average_rating'] ?? 0;
            $course['comment_count'] = $course['comment_count'] ?? 0;
            if ($course['course_type'] == 4) {
                $returnData['webinars'][] = $course;
            } else {
                $returnData['courses'][] = $course;
            }
        }
        return $returnData;
    }

    public function getCoursesByCategory($category_id)
    {
        if (isset($category_id)) {
            $category = Categories::find($category_id);
            $returnData['id'] = $category_id;
            $returnData['name'] = $category->name;
            if ($category->is_webinar_category == 1) {
                $returnData['upcoming_webinars'] = self::upcomingWebinarList(null, $category_id);
                $returnData['completed_webinars'] = self::completedWebinarList($category_id);
            } else {
                $returnData['courses'] = Course::select('id', 'name', 'price', 'course_language', 'image', 'app_image', 'app_icon', 'course_type', 'comment_count', 'average_rating', 'num_of_student')
                    ->where('is_active', 1)->where('category_id', $category_id)->orderBy('sort_order', 'ASC')->get()->toArray();
            }
            return ['status' => 200, 'message' => 'Category wise courses list.', 'data' => $returnData];
        }
        return ['status' => 100, 'message' => 'Category_id is empty.', 'data' => null];
    }

    public function popularCourseList()
    {
        $popularCourses = Course::select('id', 'name', 'price', 'course_language', 'image', 'app_image', 'app_icon', 'course_type', 'comment_count', 'average_rating', 'num_of_student')
            ->ByActive()->orderBy('num_of_student', 'DESC')->take(8)->get()->toArray();
        return $popularCourses;
    }

    public function upcomingWebinarList($webinar_id = null, $category_id = null)
    {
        $webinars = Course::byActive()->select('id', 'name', 'image', 'app_image', 'app_icon', 'start_date', 'complete_date')
            ->where('course_type', 4)->where('complete_date', '>=', Carbon::now());
        if (isset($webinar_id)) {
            $webinars = $webinars->where('id', '!=', $webinar_id);
        }
        if (isset($category_id)) {
            $webinars = $webinars->where('category_id', $category_id);
        }
        return $webinars->orderBy('start_date', 'ASC')->get()->filter(function ($webinar) {
            $webinar->is_started = ((Carbon::now() >= Carbon::parse($webinar->start_date)) && (Carbon::now() <= Carbon::parse($webinar->complete_date)));
            $webinar->is_completed = !is_null($webinar->complete_date) && Carbon::parse($webinar->complete_date)->lessThan(now());
            $webinar->duration = (!$webinar->is_started || !$webinar->is_completed) ? Carbon::parse($webinar->start_date)->diffInSeconds(Carbon::now()) : 0;
            return $webinar;
        })->toArray();
    }

    public function completedWebinarList($category_id = null)
    {
        $webinars = Course::byActive()->select('id', 'name', 'image', 'app_image', 'app_icon')->where('course_type', 4)->where('complete_date', '<', Carbon::now());
        if (isset($category_id)) {
            $webinars = $webinars->where('category_id', $category_id);
        }
        $webinars = $webinars->orderBy('sort_order', 'ASC')->get()->toArray();
        return $webinars;
    }

    public function getUserCoursesList($user_id): array
    {
        $data = CourseUser::where('user_id', $user_id)->select('course_user.id as course_user_id', 'c.id', 'name', 'c.price', 'progress', 'paid_status', 'user_status_id', 'course_type', 'slug', 'color_code', 'image', 'app_image', 'app_icon', 'pc.course_fees', 'course_language', 'comment_count', 'average_rating', 'num_of_student')
            ->join("courses AS c", function ($join) use ($user_id) {
                $join->on("c.id", "=", "course_user.course_id");
            })
            ->join("partner_courses AS pc", function ($join) use ($user_id) {
                $join->on("course_user.course_id", "=", "pc.course_id")->where('pc.partner_id', '=', 1);
            })
            ->where('c.is_active', 1)->where('c.course_type', '!=', 4)->orderBy('course_user_id', 'DESC')->get();
        $returnData = [];
        foreach ($data as $courseUser) {
            $arrayData = $courseUser;
            $arrayData['progress'] = round($arrayData['progress']);
            $arrayData['average_rating'] = $arrayData['average_rating'] ?? 0;
            $arrayData['comment_count'] = $arrayData['comment_count'] ?? 0;
            $arrayData['image'] = isset($arrayData['image']) ? showCourseImage($arrayData['image']) : "";
            $arrayData['app_image'] = isset($arrayData['app_image']) ? showCourseAppImage($arrayData['app_image']) : "";
            $arrayData['app_icon'] = isset($arrayData['app_icon']) ? showCourseAppIcon($arrayData['app_icon']) : "";
            $arrayData['paid_status'] = ($arrayData['paid_status'] == 1);
            $redirectUnitData = $this->getNextContinueUnitDetail($courseUser['id'], $user_id);
            $arrayData['next_curriculum_details'] = $redirectUnitData;
            $returnData[] = $arrayData;
        }
        return $returnData;
    }

    public function getUserContinueCoursesList($user_id): array
    {
        $courseUsers = CourseUser::where('user_id', $user_id)->select('c.id', 'name', 'c.price', 'course_language', 'progress', 'image', 'app_image', 'app_icon', 'average_rating', 'comment_count', 'num_of_student')
            ->join("courses AS c", function ($join) use ($user_id) {
                $join->on("c.id", "=", "course_user.course_id");
            })
            ->where('c.is_active', 1)->where('c.course_type', '!=', 4)->orderBy('course_user.last_used_date', 'DESC')->get()->toArray();
        $returnData = [];
        foreach ($courseUsers as $courseUser) {
            $arrayData = $courseUser;
            $arrayData['progress'] = round($arrayData['progress']);
            $arrayData['average_rating'] = $arrayData['average_rating'] ?? 0;
            $arrayData['comment_count'] = $arrayData['comment_count'] ?? 0;
            $arrayData['image'] = isset($arrayData['image']) ? showCourseImage($arrayData['image']) : "";
            $arrayData['app_image'] = isset($arrayData['app_image']) ? showCourseAppImage($arrayData['app_image']) : "";
            $arrayData['app_icon'] = isset($arrayData['app_icon']) ? showCourseAppIcon($arrayData['app_icon']) : "";
            $redirectUnitData = $this->getNextContinueUnitDetail($courseUser['id'], $user_id);
            $arrayData['next_curriculum_details'] = $redirectUnitData;
            $returnData[] = $arrayData;
        }
        return $returnData;
    }

    public function getNextContinueUnitDetail($course_id, $user_id): array
    {
        $curriculumSectionList = Curriculum::whereHas('getSectionChildData')->where('course_id', $course_id)->select('section_id', 'name', 'type', 'parent_section_id')
            ->where('type', 'section')->where('is_active', 1)->orderBy('sort_order')->get()->toArray();
        $lastWatched = [];
        foreach ($curriculumSectionList as $key => $cs) {
            $sectionList = DB::select("select c.id,c.name,c.curriculum_list_id,c.type,c.section_id,
                if(c.type='unit',
                    if(uu.is_completed,uu.is_completed,0),
                        if(c.type='quiz',
                            if(qu.is_completed!=0,qu.is_completed,0),
                                if(c.type='assignment',
                                    if(au.is_completed,au.is_completed,0),
                                0)
                            )
                ) 'finish' from curriculum c
                left join unit_user uu on c.curriculum_list_id=uu.unit_id and uu.user_id=$user_id
                left join units u on u.id=c.curriculum_list_id and c.type='unit'
                left join quiz q on q.id=c.curriculum_list_id and c.type='quiz'
                left join assignments a on a.id=c.curriculum_list_id and c.type='assignment'
                left join (select is_completed,quiz_id from quiz_user where user_id=$user_id group by quiz_id  order by user_id ) as qu on c.curriculum_list_id=qu.quiz_id
                left join assignment_user au on c.curriculum_list_id=au.assignment_id and au.user_id=$user_id where c.section_id=" . $cs['section_id'] . " and c.course_id=" . $course_id . " and c.type!='section' and c.is_active=1  order by c.sort_order");
            $sectionList = getObjectToArray($sectionList);
            foreach ($sectionList as $uq) {
                if ($key == 0) {
                    $unit_quiz = $uq;
                }
                if ($uq['finish'] == 0) {
                    $lastWatched = [
                        'curriculum_id' => $uq['id'],
                        'curriculum_type' => $uq['type'],
                    ];
                    break;
                }
            }
            if (!empty($lastWatched)) {
                break;
            }
        }
        if (empty($lastWatched)) {
            $lastWatched = [
                'curriculum_id' => $unit_quiz['id'],
                'curriculum_type' => $unit_quiz['type'],
            ];
        }
        return $lastWatched;
    }

    public function getNextCurriculumDetails($course_id, $user_id, $current_curriculum_id, $current_curriculum_type): array
    {
        $curriculumSectionList = Curriculum::whereHas('getSectionChildData')->where('course_id', $course_id)->select('section_id', 'name', 'type', 'parent_section_id')
            ->where('type', 'section')->where('is_active', 1)->orderBy('sort_order')->get()->toArray();
        $nextCurriculum = [];
        $current_unit_match = false;
        foreach ($curriculumSectionList as $key => $cs) {
            $sectionsList = DB::select("select c.id,c.name,c.curriculum_list_id,c.type,c.section_id,
                if(c.type='unit',
                    if(uu.is_completed,uu.is_completed,0),
                        if(c.type='quiz',
                            if(qu.is_completed!=0,qu.is_completed,0),
                                if(c.type='assignment',
                                    if(au.is_completed,au.is_completed,0),
                                0)
                            )
                ) 'finish' from curriculum c
                left join unit_user uu on c.curriculum_list_id=uu.unit_id and uu.user_id=$user_id
                left join units u on u.id=c.curriculum_list_id and c.type='unit'
                left join quiz q on q.id=c.curriculum_list_id and c.type='quiz'
                left join assignments a on a.id=c.curriculum_list_id and c.type='assignment'
                left join (select is_completed,quiz_id from quiz_user where user_id=$user_id group by quiz_id  order by user_id ) as qu on c.curriculum_list_id=qu.quiz_id
                left join assignment_user au on c.curriculum_list_id=au.assignment_id and au.user_id=$user_id where c.section_id=" . $cs['section_id'] . " and c.course_id=" . $course_id . " and c.type!='section' and c.is_active=1  order by c.sort_order");
            $sectionsList = getObjectToArray($sectionsList);
            foreach ($sectionsList as $uq) {
                if ($current_unit_match) {
                    $nextCurriculum = [
                        'curriculum_id' => $uq['id'],
                        'curriculum_type' => $uq['type'],
                    ];
                    break;
                }
                if (($uq['id'] == $current_curriculum_id) && ($uq['type'] == $current_curriculum_type)) {
                    $current_unit_match = true;
                }
            }
            if (!empty($nextCurriculum)) {
                break;
            }
        }
        if (empty($nextCurriculum)) {
            $nextCurriculum = self::getNextContinueUnitDetail($course_id, $user_id);
        }
        return $nextCurriculum;
    }

    public function getTopCourseWithCategoryWise()
    {
        return Categories::with(['courses' => function ($q) {
            $q->byActive()->select('id', 'category_id', 'name', 'price', 'course_language', 'image', 'app_image', 'app_icon', 'comment_count', 'average_rating', 'num_of_student');
            $q->orderBY('num_of_student', 'desc');
        }])->select('id', 'name')->byActive()->where('is_webinar_category', 0)->get()->map(function ($item) {
            $item->setRelation('courses', $item->courses->take(8));
            return $item;
        })->toArray();
    }

    public function getTopSearchCourseList()
    {
        return Course::byActive()->orderBy('num_of_student', 'DESC')->select('id', 'name')->get()->take(10)->toArray();
    }

    public function getRecommendedCourses($userId, $courseId = null)
    {
        $relatedCourse = $enrollCourses = [];
        if (isset($courseId)) {
            $courseDetail = Course::byActive()->where('id', $courseId)->select('related_course')->first();
            if (isset($courseDetail->related_course) && !empty($courseDetail->related_course)) {
                $relatedCourse = $courseDetail->related_course;
            }
            return Course::where('is_active', 1)->where('course_type', '!=', 4)->whereIn('id', $relatedCourse)
                ->select('id', 'name', 'price', 'course_language', 'image', 'app_image', 'app_icon', 'comment_count', 'average_rating', 'num_of_student')
                ->orderBy('sort_order')->get()->toArray();
        } else {
            if (isset($userId)) {
                $interestCategories = UserInterests::where('user_id', $userId)->pluck('category_id')->toArray();
                if (empty($interestCategories)) {
                    $enrollCourses = CourseUser::where('user_id', $userId)->pluck('course_id')->toArray();
                    $courses = Course::byActive()->whereIn('id', $enrollCourses)->whereNotNull('related_course')->where('course_type', '!=', 4)->select('related_course')->get()->toArray();
                    foreach ($courses as $course) {
                        foreach ($course['related_course'] as $courseId) {
                            if (!in_array($courseId, $relatedCourse)) {
                                $relatedCourse[] = $courseId;
                            }
                        }
                    }
                } else {
                    $relatedCourse = Course::whereIn('category_id', $interestCategories)->pluck('id')->toArray();
                }
            }
            return Course::where('is_active', 1)->where('course_type', '!=', 4)->whereIn('id', $relatedCourse)->whereNotIn('id', $enrollCourses)
                ->select('id', 'name', 'price', 'course_language', 'image', 'app_image', 'app_icon', 'comment_count', 'average_rating', 'num_of_student')
                ->orderBy('sort_order')->get()->toArray();
        }
    }

    public function getViewPageCourseDetails($courseDetail, $course_id, $user_id = null): array
    {
        if (isset($user_id)) {
            $courseUser = CourseUser::where('course_id', $course_id)->where('user_id', $user_id)->first();
            $userDetails = User::select('id', 'isd_code', 'country')->where('id', $user_id)->first();
        }
        $siteConfigurations = $this->siteConfigurationsRepository->getConfigurationByKeys(['course_share_message', 'show_app_review_button_course_progress','webinar_share_message']);
        $returnData = [
            'id' => $courseDetail->id,
            'name' => html_entity_decode($courseDetail->name),
            'course_certificate_name' => $courseDetail->course_certificate_name,
            'course_language' => $courseDetail->course_language,
            'course_type' => $courseDetail->course_type,
            'price' => $courseDetail->price,
            'intro_video' => $courseDetail->intro_video,
            'intro_youtube_video' => $courseDetail->intro_youtube_video,
            'intro_youtube_video_id' => $courseDetail->intro_youtube_video_id,
            'intro_video_id' => getVimeoVideoId($courseDetail->intro_video),
            'content' => convertAppHtml($courseDetail->content),
            'tiny_description' => $courseDetail->tiny_description,
            'brief_description' => $courseDetail->brief_description,
            'pre_requisites' => convertAppHtml($courseDetail->pre_requisites),
            'how_can_learnvern_help' => convertAppHtml($courseDetail->how_can_learnvern_help),
            'average_rating' => $courseDetail->average_rating ?? 0,
            'num_of_student' => $courseDetail->num_of_student ?? 0,
            'comment_count' => $courseDetail->comment_count ?? 0,
            'image' => $courseDetail->image,
            'app_image' => $courseDetail->app_image,
            'app_icon' => $courseDetail->app_icon,
            'is_enrolled' => isset($courseUser),
            'paid_status' => isset($courseUser) && ($courseUser->paid_status == 1),
            'is_certified' => false,
            'enable_rating_button' => isset($courseUser) && (($courseUser->progress > $siteConfigurations['show_app_review_button_course_progress']) || ($courseDetail->course_type == 4)),
            'user_review' => [],
            'share_url' => env('FRONT_URL') . 'course/' . $courseDetail->slug,
            'course_progress' => isset($courseUser->progress) ? round($courseUser->progress) : 0,
            'total_videos_count' => count($courseDetail->relatedUnitCurriculum) + count($courseDetail->relatedAssignmentCurriculum),
            'certificate_achievement_title' => $courseDetail->certificate_achievement_title ?? null
        ];
        if (isset($courseUser) && ($courseUser->paid_status == 1) && ($courseUser->progress > 99) && (isset($courseUser->userDetail->candidate_id) || ($courseUser->is_international_certificate == 1))) {
            $returnData['is_certified'] = true;
        }
        if ($returnData['enable_rating_button']) {
            $reviewExist = Comments::where(['course_id' => $course_id, 'user_id' => $user_id])->first();
            if (isset($reviewExist)) {
                $returnData['user_review_details'] = [
                    'rating' => $reviewExist->rating,
                    'content' => $reviewExist->content,
                ];
            }
        }
        if ($courseDetail->course_type == 4) {
            $returnData['share_message'] = isset($siteConfigurations['webinar_share_message']) ? str_replace(['#COURSE_NAME#'], [$returnData['name']], $siteConfigurations['webinar_share_message']) : "Webinar Share Message";
            $returnData['is_started'] = ((Carbon::now() >= Carbon::parse($courseDetail->start_date)) && (Carbon::now() <= Carbon::parse($courseDetail->complete_date)));
            $returnData['is_completed'] = !is_null($courseDetail->complete_date) && Carbon::parse($courseDetail->complete_date)->lessThan(now());
            $returnData['duration'] = (!$returnData['is_started'] || !$returnData['is_completed']) ? Carbon::parse($courseDetail->start_date)->diffInSeconds(Carbon::now()) : 0;
            $returnData['is_applicable_for_join'] = (Carbon::now() > Carbon::parse($courseDetail->start_date)->subMinutes(15));
            $returnData['join_now_button_duration'] = ((!$returnData['is_started'] || !$returnData['is_completed']) && (Carbon::parse($courseDetail->start_date)->diffInMinutes(Carbon::now()) >= 15)) ? Carbon::parse($courseDetail->start_date)->subMinutes(15)->diffInSeconds(Carbon::now()) : 0;
            $returnData['start_date'] = formatDate($courseDetail->start_date, 'd M, Y');
            $returnData['start_date_time'] = $courseDetail->start_date;
            $returnData['complete_date'] = formatDate($courseDetail->complete_date, 'd M, Y');
            $returnData['upcoming_webinars'] = $this->upcomingWebinarList($course_id);
        } else {
            $returnData['share_message'] = isset($siteConfigurations['course_share_message']) ? str_replace(['#COURSE_NAME#'], [$returnData['name']], $siteConfigurations['course_share_message']) : "Course Share Message";
            $downloadable_resources_count = 0;
            foreach ($courseDetail->relatedAssignmentCurriculum as $relatedAssignment) {
                $downloadable_resources_count += count($relatedAssignment->relatedAssignmentAttachments);
            }
            foreach ($courseDetail->relatedUnitCurriculum as $relatedUnit) {
                $downloadable_resources_count += count($relatedUnit->relatedUnitAttachments);
            }
            $timeArray = explode(':', $courseDetail->time);
            $returnData['statistics'] = [
                [
                    'icon' => url('images/api_icons/video.png'),
                    'label' => ($timeArray[0] < 1) ? 'minutes video' : 'hours video',
                    'value' => ($timeArray[0] < 1) ? $timeArray[1] . '+' : $timeArray[0]
                ], [
                    'icon' => url('images/api_icons/article.png'),
                    'label' => 'articles',
                    'value' => count($courseDetail->relatedUnitCurriculum)
                ], [
                    'icon' => url('images/api_icons/download.png'),
                    'label' => 'downloads',
                    'value' => $downloadable_resources_count
                ], [
                    'icon' => url('images/api_icons/exercise.png'),
                    'label' => 'exercises',
                    'value' => count($courseDetail->relatedQuizCurriculum)
                ], [
                    'icon' => url('images/api_icons/life-time-access.png'),
                    'label' => 'Lifetime Access',
                    'value' => ""
                ], [
                    'icon' => url('images/api_icons/certificate.png'),
                    'label' => 'Certificate (Optional)',
                    'value' => ""
                ]
            ];
            $curriculumData = $this->getCurriculumList($user_id, $course_id);
            $returnData['curriculum'] = $curriculumData['curriculum'];
            $returnData['next_curriculum_details'] = [];
            if (isset($courseUser)) {
                $redirectUnitData = $this->getNextContinueUnitDetail($course_id, $user_id);
                $returnData['next_curriculum_details'] = $redirectUnitData;
            }
            $is_international_user = (isset($userDetails) && !in_array($userDetails->isd_code, ['91', '+91']));
            $returnData['static_data'] = $this->getStaticTextDetails($is_international_user, $returnData['is_enrolled'], isset($userDetails));
        }
        return $returnData;
    }

    public function getStaticTextDetails($is_international_user, $is_enrolled, $user_exist): array
    {
        $certificate_tab_text = $is_international_user ? "Enroll Course for a Certificate and be eligible for job internships" : "Enroll Course for a globally recognized NSDC Skill India Certificate and be eligible for job internships";
        if ($is_enrolled) {
            $certificate_tab_text = $is_international_user ? "To Apply for Certificate you must upgrade your course" : "To Apply for NSDC Skill India Certificate you must upgrade your course";
        } elseif (!$user_exist) {
            $certificate_tab_text = $is_international_user ? "You can download your certificate here once you will sign up for the course with Certificate" : "You can download your certificate here once you will sign up for the course with NSDC Certificate";
        }
        return [
            'certificate_image' => $is_international_user ? env('FRONT_URL') . 'images/International_Certificate_View.png' : env('FRONT_URL') . 'images/sample_certificate.jpg',
            'enroll_modal' => [
                [
                    'title' => "Full Course, No Certificate",
                    'description' => "You Have Complete Access to All The Course Materials",
                    'button_text' => "Start Course"
                ], [
                    'title' => $is_international_user ? "Enroll for Free Course, with Certificate" : "Enroll for Free Course, with NSDC Certificate",
                    'description' => $is_international_user ? "Get Certificate Which is Helpful to Demonstrate Your Learning and Skill Anywhere, Everywhere" : "Get NSDC Certificate Which is Helpful to Demonstrate Your Learning and Skill Anywhere, Everywhere",
                    'button_text' => "Make Payment"
                ]
            ],
            'certificate_tab' => $certificate_tab_text
        ];
    }

    public function getCourseDetails($course_id)
    {
        return Course::byActive()->where('id', $course_id)->first();
    }

    public function getTestimonials($course_id = null)
    {
        if (isset($course_id)) {
            $comments = Comments::where('course_id', $course_id)->where('rating', '>=', 4)->where('is_active', 1)
                ->select('author', 'rating', 'content')->orderBy('is_default_testimonial', 'DESC')->orderBy('created_at', 'DESC')->take(16)->get()->toArray();
        } else {
            $comments = DB::select("select c.title,c.content,c.rating,c.author,t.name as course_name,t.course_comment_count from comments c
LEFT JOIN (SELECT count(*) as course_comment_count,course_id,courses.name FROM `comments` LEFT JOIN courses on courses.id = comments.course_id WHERE rating >=4 GROUP BY course_id) t ON c.course_id = t.course_id where c.rating >= 4 AND c.is_active = 1 ORDER BY c.is_default_testimonial DESC, c.created_at DESC LIMIT 16");
        }
        return $comments;
    }

    public function getCurriculumList($userId, $courseId, $curriculum_id = null, $curriculum_type = null): array
    {
        $curriculumSectionList = Curriculum::whereHas('getSectionChildData')->where('course_id', $courseId)->select('section_id', 'name', 'type', 'parent_section_id')
            ->where('type', 'section')->where('is_active', 1)->orderBy('parent_section_id')->orderBy('sort_order')->get()->toArray();
        if (isset($userId)) {
            $curriculumAllList = $this->getUnitQuizAssignmentBySection($curriculumSectionList, $userId, $courseId);
        } else {
            $curriculumAllList = $this->getUnitQuizAssignmentBySectionForGuest($curriculumSectionList, $courseId);
        }
        $returnData['all_units'] = $returnData['curriculum'] = [];
        $returnData['sort_order'] = 1;
        $sort_order = 1;
        $index = 1;
        foreach ($curriculumAllList as $item) {
            $sectionData = [
                'index' => "",
                'id' => $item['section_id'],
                'name' => html_entity_decode($item['name']),
                'type' => $item['type'],
                'list' => []
            ];
            foreach ($item['list'] as $unitDatum) {
                $time = ($unitDatum->type == 'unit') ? $unitDatum->unit_duration : (($unitDatum->type == 'assignment') ? $unitDatum->assignment_duration : $unitDatum->quiz_duration);
                $unitData = [
                    'id' => $unitDatum->id,
                    'name' => html_entity_decode($unitDatum->name),
                    'type' => $unitDatum->type,
                    'duration' => formatUnitTime($time),
                    'is_completed' => ($unitDatum->finish == 0) ? false : true,
                    'video_download_url_1' => $unitDatum->video_download_url_1 ?? null,
                    'video_download_url_2' => $unitDatum->video_download_url_2 ?? null,
                    'video_download_url_3' => $unitDatum->video_download_url_3 ?? null,
                    'video_id' => $unitDatum->video_id ?? 0,
                    'video_progress' => $unitDatum->video_progress ?? 0,
                    'video_url' => $unitDatum->video_url ?? 0,
                    'sort_order' => $sort_order,
                ];
                $returnData['all_units'][] = [
                    'id' => $unitData['id'],
                    'type' => $unitData['type'],
                ];
                if (isset($curriculum_id) && ($curriculum_id == $unitData['id']) && ($curriculum_type == $unitData['type'])) {
                    $unitData['current_state'] = 1;
                    $returnData['sort_order'] = $sort_order;
                } elseif ($unitData['is_completed']) {
                    $unitData['current_state'] = 2;
                } else {
                    $unitData['current_state'] = 0;
                }
                $sectionData['list'][] = $unitData;
                $sort_order++;
            }
            if (!empty($sectionData['list'])) {
                $sectionData['index'] = $index;
                $index++;
            }
            $returnData['curriculum'][] = $sectionData;
        }
        return $returnData;
    }

    public function getUnitQuizAssignmentBySection($curriculum_section_get, $user_id, $courseId): array
    {
        $curriculum = $masterSections = [];
        foreach ($curriculum_section_get as $key => $cs) {
            if (isset($cs['parent_section_id']) && !in_array($cs['parent_section_id'], $masterSections)) {
                $masterSections[] = $cs['parent_section_id'];
                $masterSection = Curriculum::select('section_id', 'name', 'type', 'parent_section_id')->where('course_id', $courseId)->where('section_id', $cs['parent_section_id'])->where('type', 'section')->first()->toArray();
                $masterSection['list'] = [];
                $curriculum[] = $masterSection;
            }
            $curriculum_section = $cs;
            $sectionLists = DB::select("select c.id,c.name,c.curriculum_list_id,c.type,c.section_id,
                if(c.type='unit',u.slug,
                  if(c.type='assignment',a.slug,
                    if(c.type ='quiz',q.slug,'')
                  )
                ) as slug,
                if(c.type='unit',u.video_download_url_1,
                  if(c.type='assignment',a.video_download_url_1,
                    if(c.type ='quiz','','')
                  )
                ) as video_download_url_1,
                if(c.type='unit',u.video_download_url_2,
                  if(c.type='assignment',a.video_download_url_2,
                    if(c.type ='quiz','','')
                  )
                ) as video_download_url_2,
                if(c.type='unit',u.video_download_url_3,
                  if(c.type='assignment',a.video_download_url_3,
                    if(c.type ='quiz','','')
                  )
                ) as video_download_url_3,
                if(c.type='unit',u.video_id,
                  if(c.type='assignment',a.video_id,
                    if(c.type ='quiz','','')
                  )
                ) as video_id,
                if(c.type='unit',u.video_url,
                  if(c.type='assignment',a.video,
                    if(c.type ='quiz','','')
                  )
                ) as video_url,
                if(c.type='quiz',q.time,'') as quiz_duration,
                if(c.type='unit',u.time,'') as unit_duration,
                if(c.type='assignment',a.time,'') as assignment_duration,
                if(c.type='quiz',q.is_master_quiz,0) as is_master_quiz,
                if(c.type='unit',
                    if(uu.video_progress,uu.video_progress,0),
                        if(c.type='assignment',
                            if(au.video_progress,au.video_progress,0),
                                if(c.type='quiz',
                                    0,
                                0)
                            )
                ) 'video_progress',
                if(c.type='unit',
                    if(uu.is_completed,uu.is_completed,0),
                        if(c.type='quiz',
                            if(qu.is_completed!=0,qu.is_completed,0),
                                if(c.type='assignment',
                                    if(au.is_completed,au.is_completed,0),
                                0)
                            )
                ) 'finish' from curriculum c
                left join unit_user uu on c.curriculum_list_id=uu.unit_id and uu.user_id=$user_id
                left join units u on u.id=c.curriculum_list_id and c.type='unit'
                left join quiz q on q.id=c.curriculum_list_id and c.type='quiz'
                left join assignments a on a.id=c.curriculum_list_id and c.type='assignment'
                left join (select is_completed,quiz_id from quiz_user where user_id=$user_id group by quiz_id  order by user_id ) as qu on c.curriculum_list_id=qu.quiz_id
                left join assignment_user au on c.curriculum_list_id=au.assignment_id and au.user_id=$user_id where c.section_id=" . $cs['section_id'] . " and c.course_id=" . $courseId . " and c.type!='section' and c.is_active=1  order by c.sort_order");

            if (!empty($sectionLists)) {
                $curriculum_section['list'] = $sectionLists;
                $curriculum[] = $curriculum_section;
            }
        }
        return $curriculum;
    }

    public function getUnitQuizAssignmentBySectionForGuest($curriculum_section_get, $courseId): array
    {
        $curriculum = $masterSections = [];
        foreach ($curriculum_section_get as $key => $cs) {
            if (isset($cs['parent_section_id']) && !in_array($cs['parent_section_id'], $masterSections)) {
                $masterSections[] = $cs['parent_section_id'];
                $masterSection = Curriculum::select('section_id', 'name', 'type', 'parent_section_id')->where('course_id', $courseId)->where('section_id', $cs['parent_section_id'])->where('type', 'section')->first()->toArray();
                $masterSection['list'] = [];
                $curriculum[] = $masterSection;
            }
            $curriculum_section = $cs;
            $sectionLists = DB::select("select c.id,c.name,c.curriculum_list_id,c.type,c.section_id,
                if(c.type='unit',u.slug,
                  if(c.type='assignment',a.slug,
                    if(c.type ='quiz',q.slug,'')
                  )
                ) as slug,
                if(c.type='unit',u.video_download_url_1,
                  if(c.type='assignment',a.video_download_url_1,
                    if(c.type ='quiz','','')
                  )
                ) as video_download_url_1,
                if(c.type='unit',u.video_download_url_2,
                  if(c.type='assignment',a.video_download_url_2,
                    if(c.type ='quiz','','')
                  )
                ) as video_download_url_2,
                if(c.type='unit',u.video_download_url_3,
                  if(c.type='assignment',a.video_download_url_3,
                    if(c.type ='quiz','','')
                  )
                ) as video_download_url_3,
                if(c.type='quiz',q.time,'') as quiz_duration,
                if(c.type='unit',u.time,'') as unit_duration,
                if(c.type='assignment',a.time,'') as assignment_duration,
                if(c.type='quiz',q.is_master_quiz,0) as is_master_quiz,
                '0' as 'finish' from curriculum c

                left join units u on u.id=c.curriculum_list_id and c.type='unit'
                left join quiz q on q.id=c.curriculum_list_id and c.type='quiz'
                left join assignments a on a.id=c.curriculum_list_id and c.type='assignment'
                where c.section_id=" . $cs['section_id'] . " and c.course_id=" . $courseId . " and c.type NOT IN('section') and c.is_active=1  order by c.sort_order");
            if (!empty($sectionLists)) {
                $curriculum_section['list'] = $sectionLists;
                $curriculum[] = $curriculum_section;
            }
        }
        return $curriculum;
    }

    public function getFaqsCourse($courseId)
    {
        $faqs = Faqs::where('course_id', $courseId)->select('question', 'answer')->where('is_active', 1)->get();
        $faqs = $faqs->filter(function ($item) {
            $item->answer = convertAppHtml($item->answer);
            return $item;
        });
        return $faqs->toArray();
    }

    public function getUnitDetails($curriculum_id, $curriculum_type, $user_id = null): array
    {
        if (isset($curriculum_id) && isset($curriculum_type)) {
            if (isset($user_id)) {
                $userDetails = User::select('id', 'isd_code', 'country')->where('id', $user_id)->first();
            }
            $curriculumDetails = Curriculum::byActive()->where('id', $curriculum_id)->select('id', 'course_id', 'section_id', 'name', 'curriculum_list_id', 'type')->first();
            if (isset($curriculumDetails)) {
                $course_id = $curriculumDetails->course_id;
                $courseUser = CourseUser::where('user_id', $user_id)->where('course_id', $course_id)->first();
                $comments = Comments::where('user_id', $user_id)->where('course_id', $course_id)->first();
                $siteConfigurations = $this->siteConfigurationsRepository->getConfigurationByKeys(['course_share_message', 'show_app_review_button_course_progress']);
                if ($curriculum_type == 'unit') {
                    $unit = Units::with(['relatedUnitUser' => function ($q) use ($user_id) {
                        $q->where('user_id', $user_id);
                    }])->where('id', $curriculumDetails->curriculum_list_id)->first();
                    $returnData = [
                        "id" => $curriculumDetails->id,
                        "course_id" => $course_id,
                        "section_id" => $curriculumDetails->section_id,
                        "section_name" => html_entity_decode($curriculumDetails->sectionDetail->name),
                        "name" => html_entity_decode($curriculumDetails->name),
                        "course_name" => html_entity_decode($curriculumDetails->courseDetail->name),
                        "type" => 'unit',
                        "short_content" => convertAppHtml($unit->short_content) ?? null,
                        "video_id" => $unit->video_id ?? null,
                        "video_url" => $unit->video_url ?? null,
                        "duration" => $unit->time ?? null,
                        "video_download_url_1" => $unit->video_download_url_1 ?? null,
                        "video_download_url_2" => $unit->video_download_url_2 ?? null,
                        "video_download_url_3" => $unit->video_download_url_3 ?? null,
                        'is_enrolled' => isset($courseUser),
                        "video_progress" => (int)isset($unit->relatedUnitUser) ? ($unit->relatedUnitUser->video_progress ?? 0) : 0,
                        "is_completed" => (isset($unit->relatedUnitUser) && $unit->relatedUnitUser->is_completed == 1),
                        'paid_status' => (isset($courseUser) && ($courseUser->paid_status == 1)),
                        'enable_rating_button' => isset($courseUser) && ($courseUser->progress > $siteConfigurations['show_app_review_button_course_progress']) && empty($comments),
                        'is_certified' => false,
                        'share_url' => env('FRONT_URL') . $curriculumDetails->courseDetail->slug . '/' . $unit->slug,
                        "attachments" => [],
                        'course_progress' => isset($courseUser) ? round($courseUser->progress) : 0.0,
                        'total_videos_count' => count($curriculumDetails->courseDetail->relatedUnitCurriculum) + count($curriculumDetails->courseDetail->relatedAssignmentCurriculum),
                        'certificate_achievement_title' => $curriculumDetails->courseDetail->certificate_achievement_title ?? null
                    ];
                    foreach ($unit->relatedAttachments as $relatedAttachment) {
                        $returnData['attachments'][] = [
                            'id' => $relatedAttachment->id,
                            'title' => $relatedAttachment->title,
                            'attachment' => showAttachment($relatedAttachment->attachment),
                        ];
                    }
                } elseif ($curriculum_type == 'assignment') {
                    $assignment = Assignment::with(['relatedAssignmentUser' => function ($q) use ($user_id) {
                        $q->where('user_id', $user_id);
                    }])->where('id', $curriculumDetails->curriculum_list_id)->first();
                    $returnData = [
                        "id" => $curriculumDetails->id,
                        "course_id" => $course_id,
                        "section_id" => $curriculumDetails->section_id,
                        "section_name" => html_entity_decode($curriculumDetails->sectionDetail->name),
                        "name" => html_entity_decode($curriculumDetails->name),
                        "course_name" => html_entity_decode($curriculumDetails->courseDetail->name),
                        "type" => 'assignment',
                        "short_content" => convertAppHtml($assignment->short_content) ?? null,
                        "video_id" => $assignment->video_id ?? null,
                        "video_url" => $assignment->video ?? null,
                        "duration" => $assignment->time ?? null,
                        "video_download_url_1" => $assignment->video_download_url_1 ?? null,
                        "video_download_url_2" => $assignment->video_download_url_2 ?? null,
                        "video_download_url_3" => $assignment->video_download_url_3 ?? null,
                        'is_enrolled' => isset($courseUser),
                        "is_completed" => (isset($assignment->relatedAssignmentUser) && $assignment->relatedAssignmentUser->is_completed == 1),
                        'paid_status' => (isset($courseUser) && ($courseUser->paid_status == 1)),
                        'enable_rating_button' => isset($courseUser) && ($courseUser->progress > $siteConfigurations['show_app_review_button_course_progress']) && empty($comments),
                        'is_certified' => false,
                        'share_url' => env('FRONT_URL') . $curriculumDetails->courseDetail->slug . '/' . $assignment->slug,
                        "attachments" => [],
                        "video_progress" => (int)isset($assignment->relatedAssignmentUser) ? ($assignment->relatedAssignmentUser->video_progress ?? 0) : 0,
                        "course_progress" => isset($courseUser) ? $courseUser->progress : 0.0,
                        'total_videos_count' => count($curriculumDetails->courseDetail->relatedUnitCurriculum) + count($curriculumDetails->courseDetail->relatedAssignmentCurriculum),
                        'certificate_achievement_title' => $curriculumDetails->courseDetail->certificate_achievement_title ?? null
                    ];
                    foreach ($assignment->relatedAttachments as $relatedAttachment) {
                        $returnData['attachments'][] = [
                            'title' => $relatedAttachment->title,
                            'attachment' => showAttachment($relatedAttachment->attachment),
                        ];
                    }
                }
                $returnData['share_message'] = isset($siteConfigurations['course_share_message']) ? str_replace(['#COURSE_NAME#'], [$curriculumDetails->courseDetail->name], $siteConfigurations['course_share_message']) : "Course Share Message";
                if ($returnData['enable_rating_button']) {
                    if (isset($comments)) {
                        $returnData['user_review_details'] = [
                            'rating' => $comments->rating,
                            'content' => $comments->content,
                        ];
                    }
                }
                if (isset($courseUser) && ($courseUser->paid_status == 1) && ($courseUser->progress > 99) && (isset($courseUser->userDetail->candidate_id) || ($courseUser->is_international_certificate == 1))) {
                    $returnData['is_certified'] = true;
                }
                $curriculumData = $this->getCurriculumList($user_id, $course_id, $curriculum_id, $curriculum_type);
                $returnData['curriculum'] = $curriculumData['curriculum'];
                $returnData['all_units'] = $curriculumData['all_units'];
                $returnData['recommended_course'] = $this->getRecommendedCourses($user_id, $course_id);
                $returnData['sort_order'] = $curriculumData['sort_order'];
                // Condition for Checking Referral Popup.
                $returnData['show_referral_popup'] = false;
                $is_international_user = (isset($userDetails) && !in_array($userDetails->isd_code, ['91', '+91']));
                $returnData['static_data'] = $this->getStaticTextDetails($is_international_user, $returnData['is_enrolled'], isset($userDetails));
                return ['status' => 200, 'message' => "Curriculum details.", 'data' => $returnData];
            } else {
                return ['status' => 100, 'message' => "Can not find Curriculum details.", 'data' => null];
            }
        } else {
            return ['status' => 100, 'message' => 'Unit_id/Curriculum_type is empty.', 'data' => null];
        }
    }

    public function markUnitAsCompleted($request): array
    {
        $requestData = $request->all();
        $user_id = $requestData['user_id'] ?? null;
        $curriculum_id = $requestData['curriculum_id'] ?? null;
        $curriculum_type = $requestData['curriculum_type'] ?? null;
        $course_id = $requestData['course_id'];
        $is_manually_mark_completed = isset($requestData['is_manually_mark_completed']) ? 1 : 0;
        $platform = isset($requestData['platform']) ? $requestData['platform'] : 'android';
        if (!empty($curriculum_type) && !empty($curriculum_id) && !empty($user_id) && !empty($course_id)) {
            $curriculumDetail = Curriculum::where('id', $curriculum_id)->first();
            $courseUser = CourseUser::where('user_id', $user_id)->where('course_id', $course_id)->first();
            if (isset($courseUser)) {
                $updateData = ["is_completed" => 1, "last_used_date" => Carbon::now(), 'video_progress' => 0,
                    'is_manually_mark_completed' => $is_manually_mark_completed, 'view_source' => ($platform == 'android') ? 2 : 3];
                if ($curriculum_type == 'unit') {
                    $existUnitUser = UnitUser::where('unit_id', $curriculumDetail->curriculum_list_id)->where('user_id', $user_id)->first();
                    if (isset($existUnitUser)) {
                        $existUnitUser->update($updateData);
                    } else {
                        $updateData['unit_id'] = $curriculumDetail->curriculum_list_id;
                        $updateData['user_id'] = $user_id;
                        UnitUser::create($updateData);
                    }
                } elseif ($curriculum_type == 'assignment') {
                    $existAssignmentUser = AssignmentUser::where('assignment_id', $curriculumDetail->curriculum_list_id)->where('user_id', $user_id)->first();
                    if (isset($existAssignmentUser)) {
                        $existAssignmentUser->update($updateData);
                    } else {
                        $updateData['assignment_id'] = $curriculumDetail->curriculum_list_id;
                        $updateData['user_id'] = $user_id;
                        AssignmentUser::create($updateData);
                    }
                }
                $this->storeOtherRelatedUserCourses($curriculumDetail->curriculum_list_id, $curriculum_type, $course_id, $user_id);
                $all_count_user = (array)DB::selectOne("select (select count(id) from curriculum where course_id=$course_id and type!='section' and is_active=1) as total ,
                        (select count(uu.id) from curriculum c join unit_user uu on c.curriculum_list_id=uu.unit_id and c.type='unit' and uu.user_id=$user_id and uu.is_completed=1 where c.course_id=$course_id and c.is_active=1) +
                        (select count(distinct(qu.quiz_id)) from curriculum c join quiz_user qu on c.curriculum_list_id=qu.quiz_id and c.type='quiz' and qu.user_id=$user_id and is_completed=1 where c.course_id=$course_id and c.is_active=1) +
                        (select count(distinct(au.id)) from curriculum c join assignment_user au on c.curriculum_list_id=au.assignment_id and c.type='assignment' and au.user_id=$user_id and is_completed=1 where c.course_id=$course_id and c.is_active=1) as user_total
                        ");
                $otherData['show_course_complete_screen'] = false;
                $otherData['show_referral_popup'] = false;
                $course_avg = round(($all_count_user['user_total'] / $all_count_user['total']) * 100);
                if (100 == (int)$course_avg) {
                    $updateCourseUserData = ["progress" => $course_avg, "user_status_id" => 4, "last_used_date" => Carbon::now()];
                    if ($courseUser->user_status_id != 4) {
                        $otherData['show_course_complete_screen'] = true;
                        $otherData['paid_status'] = ($courseUser->paid_status == 1);
                        $otherData['course_id'] = $course_id;
                        $otherData['recommended_course'] = $this->getRecommendedCourses($user_id, $course_id);
                    }
                } else {
                    $updateCourseUserData = ["progress" => $course_avg, "last_used_date" => Carbon::now()];
                }
                if (($course_avg >= 80) && ($courseUser->progress < 80)) {
                    $existResumeDetails = UserResumeDetails::where('user_id', $courseUser->user_id)->first();
                    if (isset($existResumeDetails)) {
                        $userCourses = $existResumeDetails->courses;
                        $userCourses['values'][] = [
                            'id' => $courseUser->courseDetail->id,
                            'title' => $courseUser->courseDetail->name,
                            'source' => 'LearnVern',
                            'link' => env('FRONT_URL') . 'course/' . $courseUser->courseDetail->slug,
                        ];
                        $existResumeDetails->update(['courses' => $userCourses]);
                    }
                }
                // Get the referral popup value based on user progress
                $configuration_value = SiteConfiguration::where('identifier', 'show_app_referral_popup_based_on_user_progress')->first();
                $referral_popup_progress_value = explode(",", $configuration_value['configuration_value']);
                if (($course_avg != $courseUser->progress) && (in_array($course_avg, $referral_popup_progress_value))) {
                    $otherData['show_referral_popup'] = true;
                }
                $courseUser->update($updateCourseUserData);
                $data = array("course_user_id" => $courseUser->id, "course_id" => $course_id, "user_id" => $user_id, "progress" => $course_avg, "type" => $curriculum_type, 'curriculum_list_id' => $curriculum_id, 'created_at' => Carbon::now());
                DB::table('course_progress_log_by_day')->insert($data);
                $next_curriculum_details = self::getNextCurriculumDetails($course_id, $user_id, $curriculum_id, $curriculum_type);
                return ['status' => 200, 'message' => "Mark this completed successfully.", 'data' => $next_curriculum_details, 'otherData' => $otherData];
            }
            return ['status' => 100, 'message' => "You have not enrolled this course.", 'data' => null];
        } else {
            return ['status' => 100, 'message' => "Curriculum_type or Unit_id or course_id is empty.", 'data' => null];
        }
    }

    public function storeOtherRelatedUserCourses($curriculum_id, $curriculum_type, $course_id, $user_id)
    {
        $otherRelatedCoursesIdArray = Curriculum::where(['type' => $curriculum_type, 'curriculum_list_id' => $curriculum_id])->where('course_id', '!=', $course_id)->pluck('course_id');
        $otherRelatedUserCourses = CourseUser::whereIn('course_id', $otherRelatedCoursesIdArray)->where('user_id', $user_id)->get();
        foreach ($otherRelatedUserCourses as $item) {
            $course_avg_count = (array)DB::selectOne("select (select count(id) from curriculum where course_id=$item->course_id and type!='section' and is_active=1) as total ,
                        (select count(uu.id) from curriculum c join unit_user uu on c.curriculum_list_id=uu.unit_id and c.type='unit' and uu.user_id=$user_id and uu.is_completed=1 where c.course_id=$item->course_id and c.is_active=1) +
                        (select count(distinct(qu.quiz_id)) from curriculum c join quiz_user qu on c.curriculum_list_id=qu.quiz_id and c.type='quiz' and qu.user_id=$user_id and is_completed=1 where c.course_id=$item->course_id and c.is_active=1) +
                        (select count(distinct(au.id)) from curriculum c join assignment_user au on c.curriculum_list_id=au.assignment_id and c.type='assignment' and au.user_id=$user_id and is_completed=1 where c.course_id=$item->course_id and c.is_active=1) as user_total
                        ");
            if ($course_avg_count['total'] != 0) {
                $other_course_avg = round(($course_avg_count['user_total'] / $course_avg_count['total']) * 100);
                $update_data = ["progress" => $other_course_avg, "last_used_date" => date("Y-m-d H:i:s")];
                if (100 == (int)$other_course_avg) {
                    $update_data['user_status_id'] = 4;
                }
                $item->update($update_data);
                DB::table('course_progress_log_by_day')->insert([
                    "course_user_id" => $item->id,
                    "course_id" => $item->course_id,
                    "user_id" => $user_id,
                    "progress" => $other_course_avg,
                    "type" => $curriculum_type,
                    'curriculum_list_id' => $curriculum_id,
                    'created_at' => date('Y-m-d H:i:s')]);
            }
        }
    }

    public function storeOfflineProgressData($request)
    {
        $requestData = $request->all();
        $user_id = $requestData['user_id'] ?? null;
        if (isset($requestData['offline_progress_data'])) {
            foreach ($requestData['offline_progress_data'] as $progressData) {
                $progressData['progress'] = round($progressData['progress']);
                $progressData['user_id'] = $requestData['user_id'];
                if ($progressData['curriculum_type'] == 'assignment') {
                    $this->setAssignmentProgress($progressData);
                } elseif ($progressData['curriculum_type'] == 'unit') {
                    $this->saveUnitProgress($progressData);
                }
            }
        }
        if (isset($requestData['mark_as_completed_data'])) {
            foreach ($requestData['mark_as_completed_data'] as $data) {
                $curriculum_id = $data['curriculum_id'] ?? null;
                $curriculum_type = $data['curriculum_type'] ?? null;
                $course_id = $data['course_id'];
                $platform = isset($data['platform']) ? $data['platform'] : 'android';
                $is_manually_mark_completed = isset($data['is_manually_mark_completed']) ? 1 : 0;
                if (!empty($curriculum_type) && !empty($curriculum_id) && !empty($user_id) && !empty($course_id)) {
                    $curriculumDetail = Curriculum::where('id', $curriculum_id)->first();
                    $courseUser = CourseUser::where('user_id', $user_id)->where('course_id', $course_id)->first();
                    if (isset($courseUser)) {
                        $updateData = ["is_completed" => 1, "last_used_date" => Carbon::now(), 'video_progress' => 0,
                            'is_manually_mark_completed' => $is_manually_mark_completed, 'view_source' => ($platform == 'android') ? 2 : 3];
                        if ($curriculum_type == 'unit') {
                            $existUnitUser = UnitUser::where('unit_id', $curriculumDetail->curriculum_list_id)->where('user_id', $user_id)->first();
                            if (isset($existUnitUser)) {
                                $existUnitUser->update($updateData);
                            } else {
                                $updateData['unit_id'] = $curriculumDetail->curriculum_list_id;
                                $updateData['user_id'] = $user_id;
                                UnitUser::create($updateData);
                            }
                        } elseif ($curriculum_type == 'assignment') {
                            $existAssignmentUser = AssignmentUser::where('assignment_id', $curriculumDetail->curriculum_list_id)->where('user_id', $user_id)->first();
                            if (isset($existAssignmentUser)) {
                                $existAssignmentUser->update($updateData);
                            } else {
                                $updateData['assignment_id'] = $curriculumDetail->curriculum_list_id;
                                $updateData['user_id'] = $user_id;
                                AssignmentUser::create($updateData);
                            }
                        }
                        $this->storeOtherRelatedUserCourses($curriculumDetail->curriculum_list_id, $curriculum_type, $course_id, $user_id);
                        $all_count_user = (array)DB::selectOne("select (select count(id) from curriculum where course_id=$course_id and type!='section' and is_active=1) as total ,
                            (select count(uu.id) from curriculum c join unit_user uu on c.curriculum_list_id=uu.unit_id and c.type='unit' and uu.user_id=$user_id and uu.is_completed=1 where c.course_id=$course_id and c.is_active=1) +
                            (select count(distinct(qu.quiz_id)) from curriculum c join quiz_user qu on c.curriculum_list_id=qu.quiz_id and c.type='quiz' and qu.user_id=$user_id and is_completed=1 where c.course_id=$course_id and c.is_active=1) +
                            (select count(distinct(au.id)) from curriculum c join assignment_user au on c.curriculum_list_id=au.assignment_id and c.type='assignment' and au.user_id=$user_id and is_completed=1 where c.course_id=$course_id and c.is_active=1) as user_total
                        ");
                        $otherData['show_course_complete_screen'] = false;
                        $otherData['show_referral_popup'] = false;
                        $course_avg = round(($all_count_user['user_total'] / $all_count_user['total']) * 100);
                        if (100 == (int)$course_avg) {
                            $updateCourseUserData = ["progress" => $course_avg, "user_status_id" => 4, "last_used_date" => Carbon::now()];
                            if ($courseUser->user_status_id != 4) {
                                $otherData['show_course_complete_screen'] = true;
                                $otherData['paid_status'] = ($courseUser->paid_status == 1);
                                $otherData['course_id'] = $course_id;
                                $otherData['recommended_course'] = $this->getRecommendedCourses($user_id, $course_id);
                            }
                        } else {
                            $updateCourseUserData = ["progress" => $course_avg, "last_used_date" => Carbon::now()];
                        }
                        if (($course_avg >= 80) && ($courseUser->progress < 80)) {
                            $existResumeDetails = UserResumeDetails::where('user_id', $courseUser->user_id)->first();
                            if (isset($existResumeDetails)) {
                                $userCourses = $existResumeDetails->courses;
                                $userCourses['values'][] = [
                                    'id' => $courseUser->courseDetail->id,
                                    'title' => $courseUser->courseDetail->name,
                                    'source' => 'LearnVern',
                                    'link' => env('FRONT_URL') . 'course/' . $courseUser->courseDetail->slug,
                                ];
                                $existResumeDetails->update(['courses' => $userCourses]);
                            }
                        }
                        // Get the referral popup value based on user progress
                        $configuration_value = SiteConfiguration::where('identifier', 'show_app_referral_popup_based_on_user_progress')->first();
                        $referral_popup_progress_value = explode(",", $configuration_value['configuration_value']);
                        if (($course_avg != $courseUser->progress) && (in_array($course_avg, $referral_popup_progress_value))) {
                            $otherData['show_referral_popup'] = true;
                        }
                        $courseUser->update($updateCourseUserData);
                        $data = array("course_user_id" => $courseUser->id, "course_id" => $course_id, "user_id" => $user_id, "progress" => $course_avg, "type" => $curriculum_type, 'curriculum_list_id' => $curriculum_id, 'created_at' => Carbon::now());
                        DB::table('course_progress_log_by_day')->insert($data);
                    }
                }
            }
        }
    }

    public function courseEnroll($request): array
    {
        $requestData = $request->all();
        $course_id = $requestData['course_id'] ?? null;
        $user_id = $requestData['user_id'] ?? null;
        $platform = isset($requestData['platform']) ? $requestData['platform'] : 'android';
        if (isset($course_id) && isset($user_id)) {
            $courseDetail = $this->getCourseDetails($course_id);
            $courseUserDetail = CourseUser::where('course_id', $course_id)->where('user_id', $user_id)->first();
            if (is_null($courseUserDetail)) {
                $userDetails = User::select('id', 'country')->where('id', $user_id)->first();
                $is_applicable_for_free_certificate = 0;
                if ($courseDetail->course_type == 4) {
                    if (new \DateTime() > new \DateTime(date('Y-m-d H:i:s', strtotime('-15 minutes', strtotime($courseDetail->start_date)))) && new \DateTime() < new \DateTime(date('Y-m-d H:i:s', strtotime($courseDetail->complete_date)))) {
                        $is_applicable_for_free_certificate = 1;
                    }
                }
                $course_id = $courseDetail->id;
                $all_count_user = (array)DB::selectOne("select (select count(id) from curriculum where course_id=$course_id and type!='section' and is_active=1) as total ,
                        (select count(uu.id) from curriculum c join unit_user uu on c.curriculum_list_id=uu.unit_id and c.type='unit' and uu.user_id=$user_id and uu.is_completed=1 where c.course_id=$course_id and c.is_active=1) +
                        (select count(distinct(qu.quiz_id)) from curriculum c join quiz_user qu on c.curriculum_list_id=qu.quiz_id and c.type='quiz' and qu.user_id=$user_id and is_completed=1 where c.course_id=$course_id and c.is_active=1) +
                        (select count(distinct(au.id)) from curriculum c join assignment_user au on c.curriculum_list_id=au.assignment_id and c.type='assignment' and au.user_id=$user_id and is_completed=1 where c.course_id=$course_id and c.is_active=1) as user_total
                        ");
                $course_avg = ($all_count_user['user_total'] / $all_count_user['total']) * 100;
                CourseUser::create([
                    'course_id' => $course_id,
                    'partner_id' => 1,
                    'user_id' => $user_id,
                    'certificate_id' => null,
                    'paid_status' => 0,
                    'is_from_nsdc' => 0,
                    'nsdc_course_id' => null,
                    'progress' => ($is_applicable_for_free_certificate == 1) ? 100 : $course_avg,
                    'user_status_id' => 2,
                    'is_international_certificate' => (isset($userDetails->country) && $userDetails->country == "India") ? 0 : 1,
                    'course_enroll_date' => Carbon::now(),
                    'last_used_date' => Carbon::now(),
                    'is_applicable_for_free_certificate' => $is_applicable_for_free_certificate,
                    'enroll_source' => ($platform == 'android') ? 2 : 3
                ]);
                $courseDetail->num_of_student = $courseDetail->num_of_student + 1;
                $courseDetail->save();
            } else {
                if ($courseDetail->course_type == 4) {
                    if ($courseUserDetail->is_applicable_for_free_certificate == 0 && new \DateTime() >= new \DateTime(date('Y-m-d H:i:s', strtotime('-15 minutes', strtotime($courseDetail->start_date)))) && new \DateTime() < new \DateTime(date('Y-m-d H:i:s', strtotime($courseDetail->complete_date)))) {
                        $courseUserDetail->is_applicable_for_free_certificate = 1;
                        $courseUserDetail->progress = 100;
                    }
                }
                $courseUserDetail->last_used_date = Carbon::now();
                $courseUserDetail->save();
            }
            $getCourseFirstCurriculumDetails = null;
            if ($courseDetail->course_type != 4) {
                $getCourseFirstCurriculumDetails = $this->getCourseFirstCurriculumDetails($course_id, $user_id);
            }
            return ['status' => 200, 'message' => "You have enrolled successfully.", 'data' => $getCourseFirstCurriculumDetails];
        } else {
            return ['status' => 100, 'message' => "Empty course id or user status.", 'data' => null];
        }
    }

    public function getCourseFirstCurriculumDetails($course_id, $user_id): array
    {
        $firstSection = Curriculum::whereHas('getSectionChildData')->where('course_id', $course_id)->where('type', 'section')->where('is_active', 1)->orderBy('sort_order')->first();
        $firstCurriculum = $firstSection->getSectionChildData[0];
        $curriculum_id = $firstCurriculum->id;
        $curriculum_type = $firstCurriculum->type;
        $response = $this->getUnitDetails($curriculum_id, $curriculum_type, $user_id);
        return $response['data'];
    }

    public function saveUnitProgress($requestData): bool
    {
        $platform = isset($requestData['platform']) ? $requestData['platform'] : 'android';
        $curriculumDetails = Curriculum::where('id', $requestData['curriculum_id'])->first();
        $unitUser = UnitUser::where([
            'unit_id' => $curriculumDetails['curriculum_list_id'],
            'user_id' => $requestData['user_id']
        ])->first();
        if (isset($unitUser)) {
            $unitUser->update(['video_progress' => $requestData['progress'], 'view_source' => ($platform == 'android') ? 2 : 3]);
        } else {
            UnitUser::create([
                'unit_id' => $curriculumDetails['curriculum_list_id'],
                'user_id' => $requestData['user_id'],
                'course_id' => $curriculumDetails['course_id'],
                'video_progress' => $requestData['progress'],
                'last_used_date' => Carbon::now(),
                'view_source' => ($platform == 'android') ? 2 : 3
            ]);
        }
        return true;
    }

    public function setAssignmentProgress($requestData): bool
    {
        $platform = isset($requestData['platform']) ? $requestData['platform'] : 'android';
        $curriculumDetails = Curriculum::where('id', $requestData['curriculum_id'])->first();
        $assignmentUser = AssignmentUser::where([
            'assignment_id' => $curriculumDetails['curriculum_list_id'],
            'user_id' => $requestData['user_id']
        ])->first();
        if (isset($assignmentUser)) {
            $assignmentUser->update(['video_progress' => $requestData['progress'], 'view_source' => ($platform == 'android') ? 2 : 3]);
        } else {
            AssignmentUser::create([
                'assignment_id' => $curriculumDetails['curriculum_list_id'],
                'user_id' => $requestData['user_id'],
                'video_progress' => $requestData['progress'],
                'last_used_date' => Carbon::now(),
                'view_source' => ($platform == 'android') ? 2 : 3
            ]);
        }
        return true;
    }
}
