<?php
/**
 * Created by PhpStorm.
 * User: -
 * Date: 10-07-19
 * Time: 03:24 PM
 */

namespace App\Repositories\Api;

use App\Interfaces\Api\NotificationRepositoryInterface;
use App\Models\Notification;
use Carbon\Carbon;

class NotificationRepository implements NotificationRepositoryInterface
{
    public function getUserNotificationList($user_id): array
    {
        $returnData = $todayData = $earlierData = [];
        $unread_count = 0;
        $notifications = Notification::where('user_id', $user_id)->orderBy('id', "DESC")->get();
        foreach ($notifications as $notification) {
            if ($notification->is_read == 0){
                $unread_count++;
            }
            $notificationData = [
                'id' => $notification->id,
                'title' => $notification->title,
                'description' => $notification->description,
                'link' => $notification->link,
                'image' => isset($notification->image) ? showNotificationImage($notification->image) : "",
                'is_read' => ($notification->is_read == 1),
            ];
            $notification_date_string = Carbon::parse($notification['created_at'])->toDateString();
            if (Carbon::today()->toDateString() == $notification_date_string) {
                $todayData[] = $notificationData;
            } else {
                $earlierData[formatDate($notification['created_at'], "d M, Y")][] = $notificationData;
            }
        }
        if (!empty($todayData)) {
            $returnData[] = ['title' => 'Today', 'data' => $todayData];
        }
        if (!empty($earlierData)) {
            foreach ($earlierData as $date => $details) {
                $returnData[] = ['title' => $date, 'data' => $details];
            }
        }
        return ['data' => $returnData, 'unread_count' => $unread_count];
    }

    public function readNotification($request): bool
    {
        $requestData = $request->all();
        Notification::where('id', $requestData['id'])->update(['is_read' => 1]);
        return true;
    }

    public function deleteNotification($request): bool
    {
        $requestData = $request->all();
        Notification::where('id', $requestData['id'])->delete();
        return true;
    }
}
