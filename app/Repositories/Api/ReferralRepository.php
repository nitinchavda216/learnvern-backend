<?php


namespace App\Repositories\Api;


use App\Interfaces\Api\ReferralInterface;
use App\Models\AmbassadorProgramSettings;
use App\Models\ReferralLog;
use App\Models\Pages;
use App\Models\User;
use App\Models\UserAmbassadorBadges;
use Illuminate\Support\Facades\DB;

class ReferralRepository implements ReferralInterface
{
    public function getAmbassadorSettings()
    {
        $data = AmbassadorProgramSettings::select('id', 'title', 'referrals_count', 'app_content as content', 'image')->get();
        $data = $data->filter(function ($q) {
            $q->image = isset($q->image) ? showAmbassadorSettingImage($q->image) : "";
            return $q;
        });
        return $data->toArray();
    }

    public function myReferrals($ownReferralCode)
    {
        $myReferrals = User::where('referral', $ownReferralCode)
            ->select('id', 'first_name as name', 'is_active', 'referred_count', 'nsdc_referred_count', 'created_at')
            ->orderBy('id', 'Desc')
            ->get();
        $myReferrals = $myReferrals->filter(function ($item) {
            $item->is_active = $item->is_active == 1;
            $item->created_at = formatDate($item->created_at);
            return $item;
        });
        return $myReferrals->toArray();
    }

    public function getAffiliateUser($userId)
    {
        $user = User::select('id', 'first_name', 'username', 'referral', 'own_referral_code', 'referred_count', 'nsdc_referred_count', 'ambassador_badge_id')
            ->where('id', $userId)->first();
        $user['current_badge'] = $user->ambassadorBadgeDetail->title ?? 'Volunteer';
        $user['current_badge_image'] = isset($user->ambassadorBadgeDetail->image) ? showAmbassadorSettingImage($user->ambassadorBadgeDetail->image) : "";
        $user['ambassador_badge_id'] = $user['ambassador_badge_id'] ?? 1;
        $user['total_referred_count'] = count($user->myReferrals);
        unset($user->myReferrals, $user->ambassadorBadgeDetail);
        return $user->toArray();
    }

    public function getRecentAmbassadors($count = 10)
    {
        $returnData = [];
        $recentAmbassadors = UserAmbassadorBadges::where('ambassador_badge_id', '!=', 1)->orderByDesc('created_at')->take($count)->get();
        foreach ($recentAmbassadors as $recentAmbassador) {
            $returnData[] = [
                'name' => $recentAmbassador->userDetails->first_name ?? null,
                'title' => $recentAmbassador->ambassadorBadge->title ?? null,
                'referred_count' => $recentAmbassador->userDetails->referred_count ?? 0,
                'nsdc_referred_count' => $recentAmbassador->userDetails->nsdc_referred_count ?? 0,
            ];
        }
        return $returnData;
    }

    public function userAmbassadorBadges($userId)
    {
        return UserAmbassadorBadges::query()->with('ambassadorBadge')->where('user_id', $userId)->get()->filter(function ($item) {
            $item['badge_title'] = $item->ambassadorBadge->title;
            $item['badge_image'] = $item->ambassadorBadge->image;
            unset($item['ambassadorBadge']);
            return $item;
        });
    }

    public function getFaqs()
    {
        return Pages::query()->where(['slug' => 'mobile-referral-faq', 'is_active' => 1])->get();
    }

    public function getUserAmbassadorSettings($user_id)
    {
        $user = User::query()->findOrFail($user_id);
        $data = AmbassadorProgramSettings::query()->select('id', 'title', 'referrals_count', 'app_content as content', 'image')->get();
        $data = $data->filter(function ($q) use ($user) {
            $q->image = isset($q->image) ? showAmbassadorSettingImage($q->image) : "";
            /*if ($q->referrals_count <= $user->referred_count) {
                $q->status = 'completed';
            }*/
            if ($q->referrals_count <= $user->referred_count) {
                $q->status = 1;
            } else {
                $q->status = 0;
            }
            return $q;
        });
        return $data->toArray();
    }
}
