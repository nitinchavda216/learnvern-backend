<?php


namespace App\Repositories\Api;


use App\Models\Colleges;

class CollegeRepository implements \App\Interfaces\Api\CollegeInterface
{

    public function getById($id)
    {
        return Colleges::query()->find($id);
    }
}
