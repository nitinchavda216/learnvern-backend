<?php


namespace App\Repositories\Api;


use App\Interfaces\Api\ResumeBuilderInterface;
use App\Models\UserResumeDetails;

class ResumeBuilderRepository implements ResumeBuilderInterface
{
    protected $experiences;

    public function updateResumeBuilder($requestData)
    {

    }

    public function updateExperience($experiences, $user_id)
    {

    }

    /**
     * @return mixed
     */
    public function getExperiences()
    {
        if (is_null($this->experiences))
            $this->setExperience();
        return $this->experiences;
    }

    /**
     * @param mixed $experience
     */
    public function setExperience($experience = []): void
    {
        if (!empty($experience))
            $experienceDetails['values'][] = $experience;
        $this->experiences = $experienceDetails ?? ["is_enable" => "true", "values" => []];
    }
}
