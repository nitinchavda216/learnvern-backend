<?php

namespace App\Repositories\Api;

use App\Interfaces\Api\CoursesRepositoryInterface;
use App\Interfaces\Api\UserRepositoryInterface;
use App\Interfaces\SiteConfigurationsRepositoryInterface;
use App\Models\City;
use App\Models\Country;
use App\Models\CourseUser;
use App\Models\PaymentTransaction;
use App\Models\State;
use App\Models\User;
use App\Models\UserInterests;
use App\Services\MauticsService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use PDF;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class UserRepository implements UserRepositoryInterface
{
    protected $user;
    protected $siteConfigurationsRepository, $coursesRepository, $mauticsService;

    public function __construct(SiteConfigurationsRepositoryInterface $siteConfigurationsRepository, CoursesRepositoryInterface $coursesRepository,
    MauticsService $mauticsService)
    {
        $this->siteConfigurationsRepository = $siteConfigurationsRepository;
        $this->coursesRepository = $coursesRepository;
        $this->mauticsService = $mauticsService;
    }

    public function validateUpdateRequest($request): array
    {
        $this->user = User::findOrFail($request['user_id']);
        $isd_code = $request['isd_code'] ?? 91;
        $existNumberUser = User::where('partner_id', 1)->whereIn('isd_code', [$isd_code, '+' . $isd_code])
            ->where('mobile_number', $request->mobile_number)->where('id', '!=', $request['user_id'])->first();
        $email_string = "";
        if ($existNumberUser) {
            $email = $existNumberUser->email;
            $string_parts = explode("@", $email);
            $blurred_email_string = "xxxxxxxx";
            if (isset($string_parts[0])) {
                $replacement_string = str_repeat("x", ((strlen($string_parts[0]) - 4)));
                $blurred_email_string = substr_replace($string_parts[0], $replacement_string, 2, (strlen($string_parts[0]) - 4));
            }
            $email_string = isset($string_parts[1]) ? $blurred_email_string .'@'. $string_parts[1] : $blurred_email_string . "@gmail.com";
        }
        $validator = Validator::make($request->all(), [
            'mobile_number' => ['string', 'min:10', Rule::unique('users')->where(function ($query) use ($isd_code) {
                return $query->whereIn('isd_code', [$isd_code, '+'.$isd_code])->whereNull('deleted_at')->where('partner_id', 1);
            })->ignore($this->user)]
        ], [
            'mobile_number.unique' => "This Mobile Number is already Linked With ".$email_string
        ]);
        if ($validator->fails()) {
            return ['status' => false, 'result' => $validator];
        }
        return [];
    }

    public function validateMobileNumber($request): array
    {
        $this->user = User::findOrFail($request['user_id']);
        $isd_code = $request['isd_code'] ?? 91;
        $email_string = "";
        $existNumberUser = User::where('partner_id', 1)->whereIn('isd_code', [$isd_code, '+' . $isd_code])
            ->where('mobile_number', $request->mobile_number)->where('id', '!=', $request['user_id'])->first();
        if ($existNumberUser) {
            $email = $existNumberUser->email;
            $string_parts = explode("@", $email);
            $blurred_email_string = "xxxxxxxx";
            if (isset($string_parts[0])) {
                $replacement_string = str_repeat("x", ((strlen($string_parts[0]) - 4)));
                $blurred_email_string = substr_replace($string_parts[0], $replacement_string, 2, (strlen($string_parts[0]) - 4));
            }
            $email_string = isset($string_parts[1]) ? $blurred_email_string .'@'. $string_parts[1] : $blurred_email_string . "@gmail.com";
        }
        $validator = Validator::make($request->all(), [
            'mobile_number' => ['string', 'size:10', Rule::unique('users')->where(function ($query) use ($isd_code) {
                return $query->whereIn('isd_code', [$isd_code, '+'.$isd_code])->whereNull('deleted_at')->where('partner_id', 1);
            })->ignore($this->user)]
        ], [
            'mobile_number.unique' => "This Mobile Number is already Linked With ".$email_string
        ]);
        if ($validator->fails()) {
            return ['status' => false, 'result' => $validator];
        }
        return [];
    }

    public function updateUser($request): array
    {
        $requestData = $request->all();
        if (isset($requestData['birth_date'])) {
            $requestData['birth_date'] = Carbon::parse(str_replace('/', '-', $requestData['birth_date']))->toDateString();
            $todayDate = Carbon::now();
            $age = $todayDate->diffInYears($requestData['birth_date']);
            if ($age <= 10) {
                return ['status' => false, 'message' => config('app_messages.update_profile.birth_year_error.description'), 'header_message' => config('app_messages.update_profile.birth_year_error.title'), 'data' => null];
            }
        }
        $user = User::query()->findOrFail($request->user_id);
        if (is_null($user->username) && isset($requestData['first_name'])) {
            $requestData['username'] = getUsername($requestData['first_name']);
        }
        if (isset($requestData['user_interests'])) {
            $requestData['certificate_name'] = $requestData['first_name'];
            UserInterests::where('user_id', $request->user_id)->delete();
            foreach ($requestData['user_interests'] as $interest_category_id) {
                UserInterests::create([
                    'user_id' => $request->user_id,
                    'category_id' => $interest_category_id,
                ]);
            }
            $cityExist = City::where('name', $requestData['city'])->first();
            if (isset($cityExist)){
                $requestData['city_id'] = $cityExist->id;
                $requestData['state_id'] = $cityExist->stateDetail->id ?? null;
                $requestData['state'] = $cityExist->stateDetail->name ?? null;
                $requestData['country_id'] = $cityExist->stateDetail->countryDetail->id ?? null;
                $requestData['country'] = $cityExist->stateDetail->countryDetail->name ?? null;
            }else{
                $country = Country::where('isd_code', $requestData['isd_code'])->first();
                if (isset($country)){
                    $requestData['country_id'] = $country->id;
                    $requestData['country'] = $country->name;
                }
            }
        }else{
            if ($user->mobile_number != $requestData['mobile_number']){
                $requestData['is_mobile_verify'] = 0;
            }
            $requestData['country_id'] = ($requestData['country_id'] != "") ? $requestData['country_id'] : null;
            $requestData['country'] = isset($requestData['country_id']) ? Country::find($requestData['country_id'])->name : null;
            $requestData['state_id'] = ($requestData['state_id'] != "") ? $requestData['state_id'] : null;
            $requestData['state'] = isset($requestData['state_id']) ? State::find($requestData['state_id'])->name : null;
            $requestData['city_id'] = ($requestData['city_id'] != "") ? $requestData['city_id'] : null;
            $requestData['city'] = isset($requestData['city_id']) ? City::find($requestData['city_id'])->name : null;
            if (isset($requestData['is_complete_profile'])){
                $requestData['disability_type'] = ($requestData['disability'] == 1) ? $requestData['disability_type'] : null;
                if(isset($requestData['current_occupation']) && ($requestData['current_occupation'] == 'Fresher')){
                    $requestData['employment_details'] = $requestData['employment_status'] = $requestData['previous_experience_sector'] = $requestData['no_of_months_of_experience'] = null;
                }
                $requestData['is_complete_profile'] = 1;
                CourseUser::where('user_id', $user->id)->where('paid_status', 1)->update(['is_international_certificate' => ($requestData['country'] == "India") ? 0 : 1]);
            }else{
                $requestData['other_college_name'] = $requestData['other_college_name'] ?? null;
                $requestData['college_id'] = $requestData['college_id'] ?? null;
                if (isset($request['image']) && !is_null($request['image'])) {
                    $requestData['image'] = $this->updateProfileImage($request['image'], $user->getRawOriginal('image') ?? null);
                }
            }
        }
        $user->update($requestData);
        $this->mauticsService->storeUserData($user->id);
        return ['status' => true];
    }

    public function updateProfileImage($image, $oldFile = null): string
    {
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random() . '.' . 'png';
        File::put(storage_path() . '/app/public/users/' . $imageName, base64_decode($image));
        if (Storage::exists("public/users/$oldFile")) {
            Storage::delete("public/users/$oldFile");
        }
        return $imageName;
    }

    public function getUserCertificatesList($request): array
    {
        $requestData = $request->all();
        $userId = $requestData['user_id'] ?? null;
        if (isset($userId)) {
            $returnData = [];
            $courseUsers = CourseUser::where('user_id', $userId)->select('c.id as course_id', 'c.id as course_id', 'name', 'c.image', 'app_icon', 'app_image', 'progress', 'paid_status', 'is_international_certificate', 'user_status_id', 'course_type', 'u.candidate_id', 'c.start_date as start_date', 'c.complete_date as complete_date')
                ->join("courses AS c", function ($join) {
                    $join->on("c.id", "=", "course_user.course_id");
                })->join("users AS u", function ($join) {
                    $join->on("u.id", "=", "course_user.user_id");
                })->where(function ($q) {
                    $q->where('paid_status', 1)->orWhere('is_applicable_for_free_certificate', 1);
                })->orderBy('c.sort_order')->get()->toArray();
            foreach ($courseUsers as $courseUser) {
                $courseUser['progress'] = round($courseUser['progress']);
                $courseUser['image'] = isset($courseUser['icon']) ? showCourseImage($courseUser['image']) : "";
                $courseUser['app_icon'] = isset($courseUser['app_icon']) ? showCourseAppIcon($courseUser['app_icon']) : "";
                $courseUser['app_image'] = isset($courseUser['app_image']) ? showCourseAppImage($courseUser['app_image']) : "";
                $download_enable = false;
                $button_text = null;
                if ($courseUser['course_type'] != 4) {
                    if ((isset($courseUser['candidate_id']) || ($courseUser['is_international_certificate'] == 1)) && ($courseUser['progress'] > 99)) {
                        $download_enable = true;
                    } else {
                        $button_text = ($courseUser['progress'] <= 99) ? "Course Progress " : "Certification In Progress";
                    }
                } else {
                    if ((Carbon::now() < Carbon::parse($courseUser['complete_date'])) && ($courseUser['progress'] < 100)) {
                        $button_text = "Webinar Progress Pending";
                    } else {
                        $download_enable = true;
                    }
                }
                $courseUser['download_enable'] = $download_enable;
                $courseUser['button_text'] = $button_text;
                $siteConfigurations = $this->siteConfigurationsRepository->getConfigurationByKeys(['certificate_share_message']);
                $courseUser['share_message'] = isset($siteConfigurations['certificate_share_message']) ? str_replace(['#COURSE_NAME#'], [$courseUser['name']], $siteConfigurations['certificate_share_message']) : "Certificate Share Message";
                if ($courseUser['course_type'] != 4) {
                    $courseUser['next_curriculum_details'] = $this->coursesRepository->getNextContinueUnitDetail($courseUser['course_id'], $userId);
                } else {
                    $courseUser['next_curriculum_details'] = [];
                }
                $returnData[] = $courseUser;
            }
            return ['status' => 200, 'message' => 'Certificates List.', 'data' => $returnData];
        } else {
            return ['status' => 100, 'message' => 'User_id is empty.', 'data' => null];
        }
    }

    public function downloadCertificate($request)
    {
        $requestData = $request->all();
        $course_id = $requestData['course_id'] ?? null;
        $user_id = $requestData['user_id'] ?? null;
        if (isset($course_id) && isset($user_id)) {
            //check if course paid and completed 100%
            $certificate_details = CourseUser::where('course_id', $course_id)->where('user_id', $user_id)->where(function ($q) {
                $q->where('paid_status', 1)->orWhere('is_applicable_for_free_certificate', 1);
            })->where('progress', 100)->first();
            if (!isset($certificate_details)) {
                return ['status' => 100, 'message' => "You can not download certificate.", "data" => null];
            }
            if (!isset($certificate_details['certificate_date']) || $certificate_details['certificate_date'] == '0000-00-00') {
                $certificate_details->certificate_date = date('Y-m-d');
            }
            $user = User::find($user_id);
            if (($certificate_details->is_international_certificate == 1) && ($user->candidate_id == null)) {
                $user->candidate_id = 'CIN_' . rand(10000000, 99999999);
                $user->save();
            }
            $course_name = $certificate_details->courseDetail->course_certificate_name;
            if ($certificate_details->courseDetail->course_type == 4) {
                if (is_null($certificate_details->certificate_id)) {
                    $certificate_details->certificate_id = 'LV' . $certificate_details->id;
                }
                $certificate_details->save();
                $html = view('user.certificate.learnvern_certificate', compact('certificate_details'))->render();
                $pdf = PDF::loadHtml($html);
                $pdf->setPaper('A4', 'landscape');
            } else {
                $candidate_id = $user->candidate_id;
                if (is_null($certificate_details->certificate_id)) {
                    $certificate_details->certificate_id = $candidate_id . $certificate_details->id;
                }
                $certificate_details->save();
                $codeContents = env('FRONT_URL') . 'certificate?candidate_id=' . $candidate_id . '&certificate_id=' . $certificate_details->certificate_id;
                $qr_code = QrCode::format('png')->size(100)->generate($codeContents);
                $html = view('user.certificate.nsdc_certificate', compact('qr_code', 'certificate_details', 'course_name', 'candidate_id'))->render();
                $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
                $pdf = PDF::loadHtml($html);
                $pdf->setPaper('A4', 'landscape');
            }
            $file_name = 'certificate-' . $certificate_details->id;
            $pdf_path = storage_path('app/public/certificates/') . $file_name . '.pdf';
            $pdf->save($pdf_path);
            $pdf_url = url('storage/certificates/' . $file_name . '.pdf');

            $newPDF = new \Spatie\PdfToImage\Pdf($pdf_path);
            $filePath = storage_path('app/public/certificates/images/') . $file_name . '.jpg';
            $imageData = $newPDF->getImageData($filePath);
            file_put_contents($filePath, $imageData);
            $share_image_url = url('storage/certificates/images/' . $file_name . '.jpg');

            $siteConfigurations = $this->siteConfigurationsRepository->getConfigurationByKeys(['certificate_share_message']);
            $share_message = isset($siteConfigurations['certificate_share_message']) ? str_replace(['#COURSE_NAME#'], [$course_name], $siteConfigurations['certificate_share_message']) : "Certificate Share Message";
            return ['status' => 200, 'message' => "Certificate download successfully.", 'data' => ['download_link' => $pdf_url, 'share_image_url' => $share_image_url, 'share_message' => $share_message]];
        }
        return ['status' => 100, 'message' => "Missing parameters", 'data' => null];
    }

    public function verifyCertificate($request)
    {
        $requestData = $request->all();
        $candidate_id = $requestData['candidate_id'] ?? null;
        $certificate_id = $requestData['certificate_id'] ?? null;
        if (isset($candidate_id) && isset($certificate_id)) {
            $certificate = DB::selectOne("select u.certificate_name,
            c.name,u.candidate_id,u.first_name,cu.certificate_id,cu.certificate_date
            from course_user cu
            join users u on cu.user_id=u.id
            join courses c on c.id=cu.course_id
            where cu.certificate_id='$certificate_id' and u.candidate_id='$candidate_id' limit 1");
            $certificateDetails = (array)$certificate;
            if (empty($certificateDetails)) {
                return ['status' => 100, 'message' => config('certificate_verification.empty_record.description'), 'header_message' => null, 'data' => null];
            }
            $certificateDetails['certificate_date'] = isset($certificateDetails['certificate_date']) ? formatDate($certificateDetails['certificate_date'], 'd/m/Y') : null;
            return ['status' => 200, 'message' => "Certificate details.", 'header_message' => null, 'data' => $certificateDetails];
        }
        return ['status' => 100, 'message' => "Missing parameters", 'header_message' => null, 'data' => null];
    }

    public function getUserById($userId)
    {
        $user = User::findOrFail($userId);
        $user->birth_date = isset($user->birth_date) ? formatDate($user->birth_date, 'd-m-Y') : null;
        $user->user_interests = UserInterests::where('user_id', $userId)->pluck('category_id')->toArray();
        $user->unread_notifications_count = $user->userNotifications->where('is_read', 0)->count();
        $user->is_international_user = isset($user->country) && ($user->country != "India");
        $user->show_nsdc_complete_profile_page = (count($user->relatedPaymentTransaction) > 0) && ($user->is_complete_profile == 0);
        unset($user->userNotifications, $user->relatedPaymentTransaction);
        return $user;
    }

    public function updateVerifiedMobileNumber($request): bool
    {
        $requestData = $request->all();
        User::where('id', $requestData['user_id'])->update(['isd_code' => '+91', 'mobile_number' => $requestData['mobile_number'], 'is_mobile_verify' => 1]);
        $this->mauticsService->storeUserData($requestData['user_id']);
        return true;
    }
}
