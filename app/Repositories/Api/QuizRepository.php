<?php
/**
 * Created by PhpStorm.
 * User: -
 * Date: 10-07-19
 * Time: 03:24 PM
 */

namespace App\Repositories\Api;

use App\Interfaces\Api\CoursesRepositoryInterface;
use App\Interfaces\Api\QuizRepositoryInterface;
use App\Models\Course;
use App\Models\CourseUser;
use App\Models\Curriculum;
use App\Models\Question;
use App\Models\QuestionAnswerUser;
use App\Models\QuestionOptions;
use App\Models\Quiz;
use App\Models\QuizUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class QuizRepository implements QuizRepositoryInterface
{
    protected $coursesRepository;

    public function __construct()
    {
        $this->coursesRepository = app()->make(CoursesRepositoryInterface::class);
    }

    public function quizDetails($quiz_id, $user_id): array
    {
        if (isset($quiz_id)) {
            $curriculumDetails = Curriculum::where('id', $quiz_id)->select('id', 'course_id', 'curriculum_list_id', 'type')->first();
            $quiz = Quiz::where('id', $curriculumDetails->curriculum_list_id)->select('id', 'name', 'sub_title', 'content', 'time', 'retake', 'is_master_quiz')
                ->where('is_active', 1)->first();
            $quiz->course_id = $curriculumDetails->course_id;
            $quiz->is_master_quiz = ($quiz->is_master_quiz == 1);
            $returnData = $quiz->toArray();
            $returnData['duration'] = strtotime($returnData['time']) - strtotime('TODAY');
            $returnData['questions'] = [];
            if (isset($quiz)) {
                $returnData['questions'] = Question::with(['relatedOptions' => function ($q) {
                    $q->select('question_id', 'option_id', 'content');
                }, 'relatedQuestionAnswerUser' => function ($q) use ($user_id) {
                    $q->where('user_id', $user_id)->orderBy('id', 'DESC');
                }])->where('quiz_id', $quiz->id)->select('id', 'content', 'answer', 'mark')->where('is_active', 1)->get();
                $returnData['questions'] = $returnData['questions']->filter(function ($item) {
                    $selected_answer = (count($item->relatedQuestionAnswerUser) > 0) ? $item->relatedQuestionAnswerUser[0]->user_answer_id : 0;
                    $item->content = html_entity_decode(preg_replace('/(<[^>]+) style=".*?"/i', '$1', $item->content));
                    $item->related_options = $item->relatedOptions->filter(function ($subItem) use ($selected_answer) {
                        $subItem->content = html_entity_decode($subItem->content);
                        $subItem->isSelected = ($selected_answer == $subItem->option_id);
                        return $subItem;
                    });
                    unset($item->relatedOptions, $item->relatedQuestionAnswerUser);
                    return $item;
                })->toArray();
            }
            $returnData['id'] = $curriculumDetails->id;
            return ['status' => 200, 'message' => "Quiz details", 'data' => $returnData];
        }
        return ['status' => 100, 'message' => "Quiz_id is null.", 'data' => null];
    }

    public function userQuizResultCourseList($user_id): array
    {
        $quizUsers = QuizUser::join('curriculum as c', function ($join) {
            $join->on('c.curriculum_list_id', '=', 'quiz_user.quiz_id')->where('type', 'quiz');
        })->join('courses', function ($join) {
            $join->on('courses.id', '=', 'c.course_id')->where('courses.is_active', 1)->where('course_type', '!=', 4);
        })->where('user_id', $user_id)->select('courses.id', 'courses.name', 'courses.app_icon')->groupBy('c.course_id')->get();
        $quizUsers = $quizUsers->filter(function ($item){
            $item->app_icon = isset($item->app_icon) ? showCourseAppIcon($item->app_icon) : "";
            return $item;
        })->toArray();
        if (!empty($quizUsers)) {
            return ['status' => 200, 'message' => "My quiz result sent successfully.", 'data' => $quizUsers];
        } else {
            return ['status' => 200, 'message' => "You have not completed any quiz.", 'data' => []];
        }
    }

    public function userQuizResultsByCourse($user_id, $course_id): array
    {
        if (isset($user_id) && isset($course_id)) {
            $course = Course::select('id', 'name', 'app_icon')->where('id', $course_id)->first()->toArray();
            $course['quiz_list'] = DB::select("SELECT quiz_user.id as quiz_user_id, curriculum.id as quiz_id, curriculum.name as quiz_name,quiz_user.marks as quiz_marks,SUM(quiz_questions.mark) as total_mark FROM curriculum JOIN quiz_user ON curriculum.curriculum_list_id = quiz_user.quiz_id
                        JOIN quiz_questions ON curriculum.curriculum_list_id = quiz_questions.quiz_id and quiz_questions.is_active=1 where quiz_user.id IN ( SELECT max(quiz_user.id) FROM quiz_user where quiz_user.user_id ='" . $user_id . "'  GROUP BY quiz_id ) AND curriculum.course_id ='" . $course_id . "' GROUP BY curriculum.curriculum_list_id");
            return ['status' => 200, 'message' => "My quiz result sent successfully.", 'data' => $course];
        } else {
            return ['status' => 100, 'message' => "Course_id/User_id is empty.", 'data' => null];
        }
    }

    public function getSingleQuizResults($user_id, $curriculum_id): array
    {
        if (isset($curriculum_id)) {
            $curriculumDetail = Curriculum::where('id', $curriculum_id)->first();
            $get_que_answer = DB::select("select quiz.name as quiz_name,q.id as que_id,q.content as que_content, quiz.course_id, quiz.is_master_quiz, q.mark as que_mark, qo.content as co_answer,if(qau.answer is not null, qau.answer,'') as user_answer,if(q.answer=qau.user_answer_id,1,0) as result, if(q.answer=qau.user_answer_id,q.mark,0) as get_mark  from quiz_questions q  left join question_answer_user qau on q.id=qau.question_id and qau.quiz_id=$curriculumDetail->curriculum_list_id and qau.user_id=$user_id left join quiz_question_options qo on q.id=qo.question_id and q.answer=qo.option_id join quiz on q.quiz_id=quiz.id where q.quiz_id=$curriculumDetail->curriculum_list_id and q.is_active=1 order by qau.id");
            $get_mark = 0;
            $total_mark = 0;
            if (count($get_que_answer) > 0) {
                $returnData['id'] = $curriculum_id;
                $returnData['course_id'] = $curriculumDetail->course_id;
                $returnData['title'] = html_entity_decode($get_que_answer[0]->quiz_name);
                $returnData['is_master_quiz'] = ($get_que_answer[0]->is_master_quiz == 1);
                foreach ($get_que_answer as $key => $v) {
                    $v = (array)$v;
                    if ($v['result'] == 1) {
                        $get_mark += $v['que_mark'];
                    }
                    $total_mark += $v['que_mark'];
                    $returnData['que_answer'][] = [
                        'id' => $v['que_id'],
                        'content' => html_entity_decode(preg_replace('/(<[^>]+) style=".*?"/i', '$1', $v['que_content'])),
                        'mark' => $v['que_mark'],
                        'answer' => html_entity_decode($v['co_answer']),
                        'user_answer' => html_entity_decode($v['user_answer']),
                        'result' => $v['result'],
                    ];
                }
                $returnData['get_mark'] = $get_mark;
                $returnData['total_marks'] = $total_mark;
                return ['status' => 200, 'message' => "Success.", 'data' => $returnData];
            } else {
                return ['status' => 100, 'message' => "No question answer found.", 'data' => null];
            }
        } else {
            return ['status' => 100, 'message' => "Quiz_id is empty", 'data' => null];
        }
    }

    public function submitQuiz($request): array
    {
        $requestData = $request->all();
        $user_id = $requestData['user_id'];
        $curriculum_id = $requestData['quiz_id'] ?? null;
        $course_id = $requestData['course_id'] ?? null;
        $que_answers = json_decode($requestData['answers'], true);
        $platform = isset($requestData['platform']) ? $requestData['platform'] : 'android';
        if (isset($curriculum_id) && isset($course_id)) {
            $curriculumDetail = Curriculum::where('id', $curriculum_id)->first();
            $quiz_id = $curriculumDetail->curriculum_list_id;
            if (!empty($que_answers)) {
                foreach ($que_answers as $que_answer_array) {
                    $current_que_id = $que_answer_array['que_id'];
                    $ans_value_id = $que_answer_array['answer_id'];

                    $answerDetail = QuestionOptions::where('question_id', $current_que_id)->where('option_id', $ans_value_id)->first();
                    $check_ans_flag = QuestionAnswerUser::where('question_id', $current_que_id)->where('user_id', $user_id)->where('quiz_id', $quiz_id)->first();
                    if (isset($check_ans_flag)) {
                        $check_ans_flag->update(['answer' => $answerDetail['content'], 'user_answer_id' => $ans_value_id, 'view_source' => ($platform == 'android') ? 2 : 3]);
                    } else {
                        QuestionAnswerUser::create([
                            'question_id' => $current_que_id,
                            'quiz_id' => $quiz_id,
                            'user_id' => $user_id,
                            'answer' => $answerDetail['content'],
                            'user_answer_id' => $ans_value_id,
                            'view_source' => ($platform == 'android') ? 2 : 3
                        ]);
                    }
                }
                $get_que_answer = DB::select("select q.mark as que_mark,if(q.answer=qau.user_answer_id,1,0) as result  from quiz_questions q  left join question_answer_user qau on q.id=qau.question_id and qau.quiz_id=$quiz_id and qau.user_id=$user_id left join quiz_question_options qo on q.id=qo.question_id and q.answer=qo.option_id where  q.quiz_id=$quiz_id and q.is_active=1 order by qau.id ");
                $get_mark = 0;
                $total_mark = 0;
                if (count($get_que_answer) > 0) {
                    foreach ($get_que_answer as $key => $v) {
                        $v = (array)$v;
                        if ($v['result'] == 1) {
                            $get_mark += $v['que_mark'];
                        }
                        $total_mark += $v['que_mark'];
                    }
                }
                QuizUser::create(["quiz_id" => $quiz_id, "user_id" => $user_id, "marks" => $get_mark, "is_completed" => 1, 'view_source' => ($platform == 'android') ? 2 : 3]);
                $this->storeOtherRelatedUserCourses($quiz_id, 'quiz', $course_id, $user_id);
                $all_count_user = (array)DB::selectOne("select (select count(id) from curriculum where course_id=$course_id and type!='section' and is_active=1) as total ,
                        (select count(uu.id) from curriculum c join unit_user uu on c.curriculum_list_id=uu.unit_id and c.type='unit' and uu.user_id=$user_id and uu.is_completed=1 where c.course_id=$course_id and c.is_active=1) +
                        (select count(distinct(qu.quiz_id)) from curriculum c join quiz_user qu on c.curriculum_list_id=qu.quiz_id and c.type='quiz' and qu.user_id=$user_id and is_completed=1 where c.course_id=$course_id and c.is_active=1) +
                        (select count(distinct(au.id)) from curriculum c join assignment_user au on c.curriculum_list_id=au.assignment_id and c.type='assignment' and au.user_id=$user_id and is_completed=1 where c.course_id=$course_id and c.is_active=1) as user_total
                        ");
                $returnData['show_course_complete_screen'] = false;
                $courseUserDetail = CourseUser::where('course_id', $course_id)->where('user_id', $user_id)->first();
                $course_avg = ($all_count_user['user_total'] / $all_count_user['total']) * 100;
                if (100 == (int)$course_avg) {
                    $updateCourseUserData = ["progress" => $course_avg, "user_status_id" => 4, "last_used_date" => Carbon::now()];
                    if ($courseUserDetail->user_status_id != 4) {
                        $returnData['show_course_complete_screen'] = true;
                        $returnData['paid_status'] = ($courseUserDetail->paid_status == 1);
                        $returnData['course_id'] = $course_id;
                        $returnData['recommended_course'] = $this->coursesRepository->getRecommendedCourses($user_id, $course_id);
                    }
                } else {
                    $updateCourseUserData = ["progress" => $course_avg, "last_used_date" => Carbon::now()];
                }
                $courseUserDetail->update($updateCourseUserData);
                $data = array("course_user_id" => $courseUserDetail->id, "course_id" => $course_id, "user_id" => $user_id, "progress" => $course_avg, "type" => 'quiz', 'curriculum_list_id' => $quiz_id, 'created_at' => Carbon::now());
                DB::table('course_progress_log_by_day')->insert($data);
                return ['status' => 200, 'message' => 'Quiz Submit Successfully', 'data' => $returnData];
            } else {
                return ['status' => 100, 'message' => 'Question_answer array is empty.', 'data' => null];
            }
        } else {
            return ['status' => 100, 'message' => 'Quiz_id or course_id is empty.', 'data' => null];
        }
    }

    public function storeOtherRelatedUserCourses($curriculum_id, $curriculum_type, $course_id, $user_id)
    {
        $otherRelatedCoursesIdArray = Curriculum::where(['type' => $curriculum_type, 'curriculum_list_id' => $curriculum_id])->where('course_id', '!=', $course_id)->pluck('course_id');
        $otherRelatedUserCourses = CourseUser::whereIn('course_id', $otherRelatedCoursesIdArray)->where('user_id', $user_id)->get();
        foreach ($otherRelatedUserCourses as $item) {
            $course_avg_count = (array)DB::selectOne("select (select count(id) from curriculum where course_id=$item->course_id and type!='section' and is_active=1) as total ,
                        (select count(uu.id) from curriculum c join unit_user uu on c.curriculum_list_id=uu.unit_id and c.type='unit' and uu.user_id=$user_id and uu.is_completed=1 where c.course_id=$item->course_id and c.is_active=1) +
                        (select count(distinct(qu.quiz_id)) from curriculum c join quiz_user qu on c.curriculum_list_id=qu.quiz_id and c.type='quiz' and qu.user_id=$user_id and is_completed=1 where c.course_id=$item->course_id and c.is_active=1) +
                        (select count(distinct(au.id)) from curriculum c join assignment_user au on c.curriculum_list_id=au.assignment_id and c.type='assignment' and au.user_id=$user_id and is_completed=1 where c.course_id=$item->course_id and c.is_active=1) as user_total
                        ");
            if ($course_avg_count['total'] != 0) {
                $other_course_avg = round(($course_avg_count['user_total'] / $course_avg_count['total']) * 100);
                $update_data = ["progress" => $other_course_avg, "last_used_date" => date("Y-m-d H:i:s")];
                if (100 == (int)$other_course_avg) {
                    $update_data['user_status_id'] = 4;
                }
                $item->update($update_data);
                DB::table('course_progress_log_by_day')->insert([
                    "course_user_id" => $item->id,
                    "course_id" => $item->course_id,
                    "user_id" => $user_id,
                    "progress" => $other_course_avg,
                    "type" => $curriculum_type,
                    'curriculum_list_id' => $curriculum_id,
                    'created_at' => date('Y-m-d H:i:s')]);
            }
        }
    }
}
