<?php


namespace App\Repositories\Api;


use App\Events\PaymentConfirmationEmailEvent;
use App\Interfaces\Api\CoursesRepositoryInterface;
use App\Interfaces\Api\PaymentInterface;
use App\Models\AbandonedPaymentReports;
use App\Models\Course;
use App\Models\CourseUser;
use App\Models\PaymentTransaction;
use App\Models\User;
use App\Services\MauticsService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Razorpay\Api\Api;

class PaymentRepository implements PaymentInterface
{
    protected $coursesRepository, $mauticsService;

    public function __construct(CoursesRepositoryInterface $coursesRepository, MauticsService $mauticsService)
    {
        $this->coursesRepository = $coursesRepository;
        $this->mauticsService = $mauticsService;
    }

    public function doPayment($requestData)
    {
        DB::beginTransaction();
        try {
            $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
            $payment = $api->payment->fetch($requestData['razorpay_payment_id']);
            $platform = isset($requestData['platform']) ? $requestData['platform'] : 'android';
            $user = User::query()->findOrFail($requestData['user_id']);
            if ($payment) {
                $courseDetail = Course::query()->findOrFail($requestData['course_id']);
                $amount = ($payment->amount / 100);
                if ($courseDetail) {
                    $courseUserDetails = CourseUser::query()
                        ->where('course_id', $courseDetail->id)
                        ->where('user_id', $requestData['user_id'])->first();
                    $isInternationalCertificate = ($user->country != "India") ? 1 : 0;
                    if (isset($courseUserDetails)) {
                        $courseUserDetails->update([
                            'paid_status' => 1,
                            'updated_at' => Carbon::now(),
                            'payment_completed_date' => Carbon::now(),
                            'is_international_certificate' => $isInternationalCertificate
                        ]);
                    } else {
                        $course_id = $courseDetail->id;
                        $user_id = $requestData['user_id'];
                        $all_count_user = (array)DB::selectOne("select (select count(id) from curriculum where course_id=$course_id and type!='section' and is_active=1) as total ,
                        (select count(uu.id) from curriculum c join unit_user uu on c.curriculum_list_id=uu.unit_id and c.type='unit' and uu.user_id=$user_id and uu.is_completed=1 where c.course_id=$course_id and c.is_active=1) +
                        (select count(distinct(qu.quiz_id)) from curriculum c join quiz_user qu on c.curriculum_list_id=qu.quiz_id and c.type='quiz' and qu.user_id=$user_id and is_completed=1 where c.course_id=$course_id and c.is_active=1) +
                        (select count(distinct(au.id)) from curriculum c join assignment_user au on c.curriculum_list_id=au.assignment_id and c.type='assignment' and au.user_id=$user_id and is_completed=1 where c.course_id=$course_id and c.is_active=1) as user_total
                        ");
                        $course_avg = ($all_count_user['user_total'] / $all_count_user['total']) * 100;
                        $insert_course_user_data = array(
                            "course_id" => $course_id,
                            "user_id" => $user_id,
                            "paid_status" => 1,
                            "user_status_id" => 2,
                            "progress" => $course_avg,
                            "partner_id" => $user->partner_id,
                            "payment_completed_date" => Carbon::now(),
                            "last_used_date" => Carbon::now(),
                            'is_international_certificate' => $isInternationalCertificate,
                            'enroll_source' => ($platform == 'android') ? 2 : 3
                        );
                        $courseUserDetails = CourseUser::create($insert_course_user_data);
                    }
                    $existPaymentTransaction = PaymentTransaction::where('course_user_id', $courseUserDetails->id)->first();
                    if (is_null($existPaymentTransaction)) {
                        $ins_trans = array(
                            "user_id" => $user->id,
                            "course_id" => $courseDetail->id,
                            "course_user_id" => $courseUserDetails->id,
                            "amount" => $amount,
                            "transaction_id" => $payment->order_id,
                            "mihpayid" => $payment->id,
                            "payment_source" => $payment->method,
                            "partner_id" => $user->partner_id,
                            "payment_method" => 11,
                            "transaction_date" => date("Y-m-d"),
                        );
                        PaymentTransaction::create($ins_trans);
                        $ins_trans['status'] = 'success';
                        $ins_trans['server_response'] = json_encode($payment);
                        unset($ins_trans['course_user_id']);
                        $ins_trans['created_at'] = date("Y-m-d H:i:s");
                        DB::table('all_transactions')->insert($ins_trans);
                    }
                    $certificate_name = $user->certificate_name;
                    if (is_null($certificate_name) && empty($certificate_name) && $certificate_name == "") {
                        $certificate_name = $user->first_name . ' ' . $user->last_name;
                    }
                    $user_data = [
                        'email' => $user->email,
                        'first_name' => $user->first_name,
                        'course_name' => $courseDetail->name,
                        'amount' => $amount,
                        'certificate_name' => $certificate_name,
                    ];
                    $referral = $user->referral;
                    if (isset($referral)) {
                        User::query()->where('own_referral_code', $referral)->increment('nsdc_referred_count');
                    }
                    event(new PaymentConfirmationEmailEvent($user_data));
                    DB::commit();
                    $this->mauticsService->storeUserData($requestData['user_id']);
                    $returnData['is_complete_profile'] = $user->is_complete_profile;
                    $returnData['is_international_user'] = isset($user->country) && ($user->country != "India");
                    $returnData['course_id'] = $courseDetail->id;
                    $returnData['course_name'] = $courseDetail->name;
                    $returnData['app_icon'] = $courseDetail->app_icon;
                    $returnData['app_image'] = $courseDetail->app_image;
                    $returnData['price'] = $courseDetail->price;
                    $returnData['recommended_courses'] = $this->coursesRepository->getRecommendedCourses($user->id, $courseDetail->id);
                    $returnData['next_curriculum_details'] = $this->coursesRepository->getNextContinueUnitDetail($courseDetail->id, $user->id);
                    $returnData['my_courses'] = $this->coursesRepository->getUserCoursesList($user->id);
                    $returnData['continue_course'] = $this->coursesRepository->getUserContinueCoursesList($user->id);
                    return ['status' => true, 'message' => 'Course Payment Details Saved.', 'data' => $returnData];
                }
            }
            DB::rollBack();
            return ['status' => false, 'message' => "Something went wrong"];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function abandonedPayment($requestData)
    {
        $user = User::query()->findOrFail($requestData['user_id']);
        $model = AbandonedPaymentReports::query();
        $model->create([
            "course_id" => $requestData['course_id'],
            "user_id" => $user->id,
            "trans_id" => $requestData['trans_id'],
            "amount" => $requestData['amount'],
            "partner_id" => $user->partner_id,
        ]);
        return ['status' => 200, 'message' => 'Abandoned Payment Entry Created Successfully.'];
    }
}
