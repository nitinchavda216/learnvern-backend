<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08-02-2021
 * Time: 02:19 PM
 */

namespace App\Repositories\Decorators;

use App\Interfaces\Api\CoursesRepositoryInterface;
use Illuminate\Contracts\Cache\Repository as Cache;

class CoursesRepository implements CoursesRepositoryInterface
{
    protected $repository;
    protected $cache;
    const SHORT_TIME = 60;
    const MID_TIME = 360;
    const LONG_TIME = 1440;

    public function __construct(CoursesRepositoryInterface $repository, Cache $cache)
    {
        $this->repository = $repository;
        $this->cache = $cache;
    }

    public function getTopCourseWithCategoryWise()
    {
        return $this->cache->tags('api_category_wise_courses')->remember('api_category_wise_courses', self::SHORT_TIME, function ()  {
            return $this->repository->getTopCourseWithCategoryWise();
        });
    }

    public function allCourseList(){
        return $this->cache->tags('api_all_courses')->remember('api_all_courses', self::SHORT_TIME, function ()  {
            return $this->repository->allCourseList();
        });
    }

    public function getCoursesByCategory($category_id){
        return $this->cache->tags('api_courses_by_category_'.$category_id)->remember('api_courses_by_category_'.$category_id, self::SHORT_TIME, function () use ($category_id) {
            return $this->repository->getCoursesByCategory($category_id);
        });
    }

    public function popularCourseList(){
        return $this->cache->tags('api_popular_courses')->remember('api_popular_courses', self::SHORT_TIME, function ()  {
            return $this->repository->popularCourseList();
        });
    }

    public function upcomingWebinarList($webinar_id = null){
        return $this->repository->upcomingWebinarList($webinar_id = null);
    }

    public function getUserCoursesList($userId){
        return $this->cache->tags('user_courses_'.$userId)->remember('user_courses_'.$userId, self::SHORT_TIME, function () use ($userId) {
            return $this->repository->getUserCoursesList($userId);
        });
    }

    public function getUserContinueCoursesList($userId){
        return $this->repository->getUserContinueCoursesList($userId);
    }

    public function getNextContinueUnitDetail($course_id, $user_id){
        return $this->repository->getNextContinueUnitDetail($course_id, $user_id);
    }

    public function getNextCurriculumDetails($course_id, $user_id, $current_curriculum_id, $current_curriculum_type){
        return $this->repository->getNextCurriculumDetails($course_id, $user_id, $current_curriculum_id, $current_curriculum_type);
    }

    public function getTopSearchCourseList(){
        return $this->cache->tags('api_top_search_courses')->remember('api_top_search_courses', self::SHORT_TIME, function ()  {
            return $this->repository->getTopSearchCourseList();
        });
    }

    public function getRecommendedCourses($userId, $courseId = null){
        return $this->repository->getRecommendedCourses($userId, $courseId);
    }

    public function getViewPageCourseDetails($courseDetail, $course_id, $user_id=null){
        return $this->repository->getViewPageCourseDetails($courseDetail, $course_id, $user_id);
    }

    public function getCourseDetails($course_id){
        return $this->cache->tags('api_course_details_'.$course_id)->remember('api_course_details_'.$course_id, self::SHORT_TIME, function () use ($course_id) {
            return $this->repository->getCourseDetails($course_id);
        });
    }

    public function getUnitDetails($curriculum_id, $curriculum_type, $user_id=null){
        return $this->repository->getUnitDetails($curriculum_id, $curriculum_type, $user_id);
    }

    public function markUnitAsCompleted($request){
        return $this->repository->markUnitAsCompleted($request);
    }

    public function storeOfflineProgressData($request){
        return $this->repository->storeOfflineProgressData($request);
    }

    public function courseEnroll($request){
        return $this->repository->courseEnroll($request);
    }

    public function getFaqsCourse($courseId){
        return $this->cache->tags('api_course_faqs_'.$courseId)->remember('api_course_faqs_'.$courseId, self::LONG_TIME, function () use ($courseId) {
            return $this->repository->getFaqsCourse($courseId);
        });
    }

    public function getCurriculumList($userId, $courseId, $curriculum_id = null, $curriculum_type = null){
        return $this->repository->getCurriculumList($userId, $courseId, $curriculum_id, $curriculum_type);
    }

    public function getTestimonials($courseId){
        return $this->cache->tags('api_course_testimonials_'.$courseId)->remember('api_course_testimonials_'.$courseId, self::LONG_TIME, function () use ($courseId) {
            return $this->repository->getTestimonials($courseId);
        });
    }

    public function saveUnitProgress($requestData){
        return $this->repository->saveUnitProgress($requestData);
    }

    public function setAssignmentProgress($requestData){
        return $this->repository->setAssignmentProgress($requestData);
    }
}
