<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08-02-2021
 * Time: 02:19 PM
 */

namespace App\Repositories\Decorators;

use App\Interfaces\Api\QuizRepositoryInterface;
use Illuminate\Contracts\Cache\Repository as Cache;

class QuizRepository implements QuizRepositoryInterface
{
    protected $repository;
    protected $cache;
    const SHORT_TIME = 60;
    const MID_TIME = 360;
    const LONG_TIME = 1440;

    public function __construct(QuizRepositoryInterface $repository, Cache $cache)
    {
        $this->repository = $repository;
        $this->cache = $cache;
    }

    public function quizDetails($quiz_id, $user_id)
    {
        return $this->repository->quizDetails($quiz_id, $user_id);
        /*return $this->cache->tags('api_quiz_details_'.$quiz_id)->remember('api_quiz_details_'.$quiz_id, self::SHORT_TIME, function () use ($quiz_id) {

        });*/
    }

    public function userQuizResultCourseList($user_id): array
    {
        return $this->cache->tags('api_user_quiz_courses_'.$user_id)->remember('api_user_quiz_courses_'.$user_id, self::LONG_TIME, function () use ($user_id) {
            return $this->repository->userQuizResultCourseList($user_id);
        });
    }

    public function userQuizResultsByCourse($user_id, $course_id): array
    {
        return $this->cache->tags('api_quiz_result_'.$user_id.'_'.$course_id)->remember('api_quiz_result_'.$user_id.'_'.$course_id, self::LONG_TIME, function () use ($user_id, $course_id) {
            return $this->repository->userQuizResultsByCourse($user_id, $course_id);
        });
    }

    public function getSingleQuizResults($user_id, $quiz_id): array
    {
        return $this->cache->tags('api_single_quiz_result_'.$user_id.'_'.$quiz_id)->remember('api_single_quiz_result_'.$user_id.'_'.$quiz_id, self::LONG_TIME, function () use ($user_id, $quiz_id) {
            return $this->repository->getSingleQuizResults($user_id, $quiz_id);
        });
    }

    public function submitQuiz($request)
    {
        return $this->repository->submitQuiz($request);
    }
}
