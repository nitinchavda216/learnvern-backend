<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08-02-2021
 * Time: 02:19 PM
 */

namespace App\Repositories\Decorators;

use App\Interfaces\SiteConfigurationsRepositoryInterface;
use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Http\Request;

class SiteConfigurationsRepository implements SiteConfigurationsRepositoryInterface
{
    protected $repository;
    protected $cache;
    const SHORT_TIME = 60;
    const MID_TIME = 360;
    const LONG_TIME = 1440;

    public function __construct(SiteConfigurationsRepositoryInterface $repository, Cache $cache)
    {
        $this->repository = $repository;
        $this->cache = $cache;
    }

    public function getAllConfigurationGroups($request)
    {
        return $this->repository->getAllConfigurationGroups($request);
    }

    public function updateConfigurations(Request $data)
    {
        return $this->repository->updateConfigurations($data);
    }

    public function getConfigurationForApp()
    {
        return $this->cache->tags('api_configurations')->remember('api_configurations', self::LONG_TIME, function () {
            return $this->repository->getConfigurationForApp();
        });
    }

    public function getConfigurationByKeys($keys = [])
    {
        return $this->repository->getConfigurationByKeys($keys);
    }

    public function getBlockedUsersDomain()
    {
        return $this->repository->getBlockedUsersDomain();
    }

    public function getCollegeList($college_search = null)
    {
        return $this->repository->getCollegeList($college_search);
    }

    public function getCountry()
    {
        return $this->cache->tags('api_countries_list')->remember('api_countries_list', self::LONG_TIME, function () {
            return $this->repository->getCountry();
        });
    }

    public function getIsdCode()
    {
        return $this->cache->tags('api_isd_code_lists')->remember('api_isd_code_lists', self::LONG_TIME, function () {
            return $this->repository->getIsdCode();
        });
    }

    public function getPageType()
    {
        return $this->cache->tags('api_page_types')->remember('api_page_types', self::LONG_TIME, function () {
            return $this->repository->getPageType();
        });
    }

    public function getCategoriesList()
    {
        return $this->cache->tags('api_category_list')->remember('api_category_list', self::LONG_TIME, function () {
            return $this->repository->getCategoriesList();
        });
    }

    public function getPageUrl($page_type)
    {
        return $this->cache->tags('api_page_details_'.$page_type)->remember('api_page_details_'.$page_type, self::LONG_TIME, function () use ($page_type) {
            return $this->repository->getPageUrl($page_type);
        });
    }

    public function getState($country_id)
    {
        return $this->cache->tags('api_state_list_'.$country_id)->remember('api_state_list_'.$country_id, self::LONG_TIME, function () use ($country_id) {
            return $this->repository->getState($country_id);
        });
    }

    public function getCity($state_id)
    {
        return $this->cache->tags('api_city_list_'.$state_id)->remember('api_city_list_'.$state_id, self::LONG_TIME, function () use ($state_id) {
            return $this->repository->getCity($state_id);
        });
    }

    public function getDistrict($state_id)
    {
        return $this->cache->tags('api_district_list_'.$state_id)->remember('api_district_list_'.$state_id, self::LONG_TIME, function () use ($state_id) {
            return $this->repository->getDistrict($state_id);
        });
    }
}
