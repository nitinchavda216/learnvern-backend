<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13-04-2021
 * Time: 09:59 AM
 */

namespace App\Repositories;

use App\Interfaces\AdminUserRoleRepositoryInterface;
use App\Models\AdminModules;
use App\Models\AdminRole;
use App\Models\RoleWiseModuleAccess;

class AdminUserRoleRepository implements AdminUserRoleRepositoryInterface
{
    public function getAdminParentModule()
    {
        return AdminModules::with('child')->where('parent_module', 0)->get();
    }

    public function getAdminModulesByType($type)
    {
        return AdminModules::with('child')->where('parent_module', 0)->whereIn('accessible_for_user_type', [$type, 'both'])->get();
    }

    public function storeAllData($request)
    {
        $requestData = $request->except('_token');

        $requestData['is_crm_manager'] = ($requestData['user_type'] == 'crm' && isset($requestData['is_crm_manager']) == 'on') ? 1 : 0;
        $data = AdminRole::create($requestData);
        $role_id = $data['id'];
        if ($requestData['user_type'] != 'super_admin') {
            self::RoleWiseAccess($request, $role_id);
        }
        return true;
    }

    public function RoleWiseAccess($request, $role_id)
    {
        $module_key = $request['user_type'] == 'lms' ? $request['lms_modules'] : $request['crm_modules'];
        if (isset($module_key)) {
            foreach ($module_key as $data) {
                RoleWiseModuleAccess::create([
                    'role_id' => $role_id,
                    'module_key' => $data]);
            }
        }
    }

    public function getAllUserRole()
    {
        return AdminRole::orderBy('id', 'DESC')->get();
    }

    public function getAdminRoleDetails($id)
    {
        return AdminRole::where('id', $id)->first();
    }

    public function getSelectedModule($id)
    {
        return RoleWiseModuleAccess::where('role_id', $id)->pluck('module_key')->toArray();
    }

    public function updateAdminModule($request, $id)
    {
        $requestData = $request->except('_token', 'admin_modules');
        $requestData['is_crm_manager'] = ($requestData['user_type'] == 'crm' && isset($requestData['is_crm_manager']) == 'on') ? 1 : 0;
        $data = self::getAdminRoleDetails($id);

        // If the current role settings is not as super_admin and now it is super_admin, then delete all the current features.
        if ($data->user_type !='super_admin' && $requestData['user_type'] =='super_admin') {
            RoleWiseModuleAccess::where('role_id', $id)->delete();
        }

        $data->update($requestData);

        if ($requestData['user_type'] != 'super_admin') {
            RoleWiseModuleAccess::where('role_id', $id)->delete();
            self::RoleWiseAccess($request, $id);
        }
        return true;
    }

    public function getRoleTitles()
    {
        return AdminRole::pluck('name', 'id')->toArray();
    }
}
