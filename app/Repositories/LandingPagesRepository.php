<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08-02-2021
 * Time: 02:19 PM
 */

namespace App\Repositories;

use App\Interfaces\LandingPagesRepositoryInterface;
use App\Models\Course;
use App\Models\LandingPages;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class LandingPagesRepository implements LandingPagesRepositoryInterface
{
    public function getAllLandingPages($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = LandingPages::where('id', "!=", null);
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id')->skip($start)->take($pageSize);

        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('course_title', function ($item) {
            return $item->courseDetail->name ?? "- - -";
        });
        $data_tables->EditColumn('created_at', function ($item) {
            return formatDate($item->created_at);
        });
        $data_tables->EditColumn('actions', function ($item) {
            return view('landing-pages.partials.actions_col', compact('item'))->render();
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['actions']);
        return $data_tables->make(true);
    }

    public function getCourseTitles($id =null)
    {
        $existCourseIds = LandingPages::where('id', '!=', null);
        if (isset($id)){
            $existCourseIds = $existCourseIds->where('id', '!=', $id);
        }
        $existCourseIds = $existCourseIds->pluck('course_id')->toArray();
        $courses = Course::select(DB::Raw('CONCAT(name," - ",IF(is_active =1,"(Active)","(Inactive)")) as title'), 'id')->where('course_type', '!=', 4)->whereNotIn('id', $existCourseIds)->orderBy('is_active', 'DESC')->get();
        return $courses->pluck('title', 'id')->toArray();
    }

    public function getCourseDetails($id)
    {
        return Course::select('id', 'name', 'slug', 'intro_video')->where('id', $id)->first()->toArray();
    }

    public function storeLandingPage($request): bool
    {
        $requestData = $request->except('_token');
        $requestData['video_embed_url'] = getVimeoVideoEmbedUrl($requestData['video_url']);
        LandingPages::create($requestData);
        return true;
    }

    public function updateLandingPage($request, $id): bool
    {
        $requestData = $request->all();
        $locationPage = self::getLandingPagesDetails($id);
        $requestData['video_embed_url'] = getVimeoVideoEmbedUrl($requestData['video_url']);
        $locationPage->update($requestData);
        return true;
    }

    public function getLandingPagesDetails($id)
    {
        return LandingPages::findOrFail($id);
    }

    public function deleteLandingPage($id): int
    {
        return LandingPages::destroy($id);
    }
}
