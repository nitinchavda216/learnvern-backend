<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:53 PM
 */

namespace App\Repositories;

use App\Interfaces\EmailTemplatesRepositoryInterface;
use App\Models\EmailTemplates;

class EmailTemplatesRepository implements EmailTemplatesRepositoryInterface
{
    public function getAllEmailTemplates($request)
    {
        return EmailTemplates::byPartnerId($request->partner_id)->get();
    }

    public function getEmailIdentifierTitles($request,$id = null)
    {
        $titles = [];
        $exist_record_actions = EmailTemplates::byPartnerId($request->partner_id)->whereNotNull('id');
        if (isset($id)) {
            $exist_record_actions = $exist_record_actions->where('id', '!=', $id);
        }
        $exist_record_actions = $exist_record_actions->pluck('identifier')->toArray();
        $actions = EmailTemplates::EMAIL_ACTION;
        foreach ($actions as $key => $value) {
            if (!in_array($key, $exist_record_actions)) {
                $titles[$key] = $value;
            }
        }
        return $titles;
    }

    public function storeEmailTemplate($request)
    {
        $requestData = $request->all();
        if (isset($requestData['attachment'])) {
            $requestData['attachment'] = self::uploadAttachment($requestData['attachment']);
        }
        EmailTemplates::create($requestData);
        return true;
    }

    public function getEmailTemplateDetail($id)
    {
        return EmailTemplates::byPartnerId(session('partner_id'))->where('id', $id)->first();
    }

    public function updateEmailTemplate($request, $id)
    {
        $requestData = $request->all();
        $email_template = self::getEmailTemplateDetail($id);
        if (isset($email_template)) {
            if (isset($requestData['attachment'])) {
                $requestData['attachment'] = self::uploadAttachment($requestData['attachment']);
            } elseif (!isset($requestData['image']) && ($requestData['remove_attachment'] == "true") && isset($email_template->attchment)) {
                $requestData['attachment'] = null;
            }
            $email_template->update($requestData);
        }
        return true;
    }

    public function uploadAttachment($file)
    {
        $uploadPath = storage_path(EmailTemplates::ATTACHMENT_PATH);
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }


}
