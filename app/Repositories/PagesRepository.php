<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:43 PM
 */

namespace App\Repositories;

use App\Interfaces\PagesRepositoryInterface;
use App\Models\Pages;

class PagesRepository implements PagesRepositoryInterface
{
    public function getAllPages()
    {
        return Pages::byPartnerId(session('partner_id'))->get();
    }

    public function storePage($request)
    {
        $requestData = $request->all();
        $requestData['canonical_url'] = $requestData['canonical_url'] ?? env('FRONT_URL').$requestData['slug'];
        Pages::create($requestData);
        return true;
    }

    public function updateActiveStatus($request)
    {
        $pages = Pages::find($request->id);
        $pages->is_active = $request->is_active;
        $pages->save();
        return true;
    }

    public function updatePage($request, $id)
    {
        $requestData = $request->all();
        $page = self::getPagesDetails($id);
        $page->update($requestData);
        return true;
    }

    public function getPagesDetails($id)
    {
        return Pages::byPartnerId(session('partner_id'))->findOrFail($id);
    }
}
