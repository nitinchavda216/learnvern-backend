<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08-03-2021
 * Time: 11:06 AM
 */

namespace App\Repositories;

use App\Events\AdminSetPasswordEvent;
use App\Interfaces\AdminUserRepositoryInterface;
use App\Models\Admins;
use Illuminate\Support\Str;

class AdminUserRepository implements AdminUserRepositoryInterface
{
    public function updateProfile($request)
    {
        $requestData = $request->all();
        $currentAdmin = getCurrentAdmin();
        if (!isset($requestData['change_password'])) {
            /* unset($requestData['password'], $requestData['password_confirmation']);*/
        }
        $currentAdmin->update($requestData);
        return true;
    }

    public function getAllAdminNames()
    {
        return Admins::whereHas('relatedRole', function ($q) {
            $q->where('is_super_admin', 0);
        })->pluck('name', 'id')->toArray();
    }


    public function getAllCRMUser()
    {
        return Admins::whereHas('relatedRole', function ($q) {
            $q->where('is_super_admin', 0);
            $q->where('user_type', 'crm');
        })->where('partner_id', 1)->pluck('name', 'id')->toArray();
    }

    public function getAllAdmins($request)
    {
        return Admins::with('relatedRole')->where('partner_id',1)->get();
    }

    public function storeAdmin($request)
    {
        $requestData = $request->except('_token');
        $requestData['activation_token'] = Str::random(10);
        $requestData['status'] = 0;
        $admin = Admins::create($requestData);
        event(new AdminSetPasswordEvent($admin));
        return true;
    }

    public function getAdminDetail($id)
    {
        return Admins::where('id', $id)->first();
    }

    public function updateAdmin($request, $id)
    {
        $requestData = $request->all();
        $admins = self::getAdminDetail($id);
        if (isset($admins)) {
            $admins->update($requestData);
        }
        return true;
    }

    public function getByActivationToken($token)
    {
        return Admins::where('activation_token', $token)->first();
    }

}
