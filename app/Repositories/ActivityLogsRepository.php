<?php

namespace App\Repositories;

use App\Models\ActivityLogs;

use App\Interfaces\ActivityLogsRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class ActivityLogsRepository implements ActivityLogsRepositoryInterface
{
    public function getActivityLogsChangesData($input)
    {
        $lastActivity = ActivityLogs::where('id', $input['id'])->first();
        $data = array();
        if (!empty($lastActivity->properties)) {
            $var = json_decode($lastActivity->properties);
            $var = (array)$var;
            if (isset($var['attributes'])) {
                $data['attributes'] = json_encode($var['attributes']);
                $data['attributes'] = json_decode($data['attributes']);
                $data['attributes'] = (array)$data['attributes'];
            } else {
                $data['attributes'] = "";
            }
            if (isset($var['old'])) {
                $data['old'] = json_encode($var['old']);
                $data['old'] = json_decode($data['old']);
                $data['old'] = (array)$data['old'];
            } else {
                $data['old'] = "";
            }
        } else {
            $data['attributes'] = '';
            $data['old'] = '';
        }
        return $data;
    }

    public function getActivityLogsData($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = ActivityLogs::whereNotNull('id');
        if (isset($requestData['log_action'])) {
            $dataInput = $dataInput->where('description', $requestData['log_action']);
        }
        if (isset($requestData['search'])) {
            $search = $requestData['search'];
            $dataInput = $dataInput->where('log_name', 'Like', '%' . $search . '%')
                ->orWhereHas('userDetail', function ($q) use ($search){
                    $q->where('name', 'Like', '%' . $search . '%');
                });
        }
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id', "DESC")->skip($start)->take($pageSize);
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('name', function ($item) {
            return $item->userDetail['name'] ?? '- - -';
        });
        $data_tables->EditColumn('description', function ($item) {
            return ucfirst($item->description);
        });
        $data_tables->EditColumn('action', function ($item) {
            return view('activity-logs.actions', compact('item'))->render();
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['action']);
        return $data_tables->make(true);
    }

    public function getActivityDataByModel($id, $log_name)
    {
        return ActivityLogs::where('log_name', $log_name)->where('subject_id', $id)->orderBy('id', 'DESC')->get();
    }
}
