<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:14 PM
 */

namespace App\Repositories;

use App\Interfaces\WebinarsRepositoryInterface;
use App\Models\Admins;
use App\Models\Course;
use App\Models\PartnerCourses;
use Carbon\Carbon;
use Spatie\SchemaOrg\Schema;
use Spatie\SchemaOrg\VideoObject;
use Yajra\DataTables\Facades\DataTables;

class WebinarsRepository implements WebinarsRepositoryInterface
{
    public function getWebinarCourseList($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = Course::select('id', 'category_id', 'name', 'is_active', 'slug', 'time')->where('course_type', 4);
        if (isset($requestData['category_id'])) {
            $dataInput = $dataInput->where('category_id', $requestData['category_id']);
        }
        if (isset($requestData['search'])) {
            $dataInput = $dataInput->where('name', 'LIKE', '%' . $requestData['search'] . '%');
        }
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id', "DESC")->skip($start)->take($pageSize);

        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('category_name', function ($item) {
            return $item->categoryDetail->name ?? "- - -";
        });
        $data_tables->EditColumn('actions', function ($item) {
            return view('webinars.partials.actions_col', compact('item'))->render();
        });
        $data_tables->EditColumn('status', function ($item) {
            return view('webinars.partials.status_col', compact('item'))->render();
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['actions', 'status']);
        return $data_tables->make(true);
    }

    public function getCourseTitles()
    {
        return Course::where('course_type', 4)->pluck('name', 'id')->toArray();
    }

    public function getCourseDetails($id)
    {
        return Course::findOrFail($id);
    }

    public function storeCourse($request)
    {
        $requestData = $request->all();
        if (isset($requestData['intro_video'])) {
            $requestData['intro_video_embed'] = getVimeoVideoEmbedUrl($requestData['intro_video']);
        }
        if (isset($requestData['intro_youtube_video'])) {
            $requestData['intro_youtube_video_id'] = getYouTubeIdFromURL($requestData['intro_youtube_video']);
            $requestData['intro_youtube_video_embed'] = "https://www.youtube.com/embed/" . $requestData['intro_youtube_video_id'];
        }
        $requestData['course_type'] = 4;
        $requestData['start_date'] = Carbon::parse($requestData['start_date'])->toDateTimeString();
        $requestData['complete_date'] = Carbon::parse($requestData['complete_date'])->toDateTimeString();
        $requestData['is_free'] = 1;
        $requestData['unit_youtube_video_flag'] = 1;
        if (isset($requestData['course_icon'])) {
            $requestData['course_icon'] = self::uploadIconImage($requestData['course_icon']);
        }
        if (isset($requestData['image'])) {
            $requestData['image'] = self::uploadImage($requestData['image']);
        }
        if (isset($requestData['app_image'])) {
            $requestData['app_image'] = self::uploadAppImage($requestData['app_image']);
        }
        if (isset($requestData['app_icon'])) {
            $requestData['app_icon'] = self::uploadAppIcon($requestData['app_icon']);
        }
        $requestData['canonical_url'] = $requestData['canonical_url'] ?? env('FRONT_URL').'course/'.$requestData['slug'];
        $course = Course::create($requestData);
        PartnerCourses::create([
            'partner_id' => 1,
            'course_id' => $course->id,
            'is_paid_course' => 0,
            'course_fees' => 0,
        ]);
        $admins = Admins::whereHas('partnerDetail', function ($q) {
            $q->where('show_webinars', 1);
        })->where('partner_id', '!=', 1)->where('status', 1)->get();
        foreach ($admins as $admin) {
            PartnerCourses::create([
                'partner_id' => $admin->partner_id,
                'course_id' => $course->id,
                'is_paid_course' => 0,
                'course_fees' => $course->price,
            ]);
        }
        self::storeSchemaScript($course->id);
        return true;
    }

    public function updateCourse($request, $id)
    {
        $requestData = $request->all();
        $requestData['course_type'] = 4;
        $course = self::getCourseDetails($id);
        if (isset($course)) {
            if (isset($requestData['intro_video'])) {
                $requestData['intro_video_embed'] = getVimeoVideoEmbedUrl($requestData['intro_video']);
            } else {
                $requestData['intro_video'] = $requestData['intro_video_embed'] = NULL;
            }
            if (isset($requestData['intro_youtube_video'])) {
                $requestData['intro_youtube_video_id'] = getYouTubeIdFromURL($requestData['intro_youtube_video']);
                $requestData['intro_youtube_video_embed'] = "https://www.youtube.com/embed/" . $requestData['intro_youtube_video_id'];
            } else {
                $requestData['intro_youtube_video'] = $requestData['intro_youtube_video_id'] = $requestData['intro_youtube_video_embed'] = NULL;
            }
            $requestData['start_date'] = Carbon::parse($requestData['start_date'])->toDateTimeString();
            $requestData['complete_date'] = Carbon::parse($requestData['complete_date'])->toDateTimeString();
            if (isset($requestData['course_icon'])) {
                $requestData['course_icon'] = self::uploadIconImage($requestData['course_icon'], $course);
            }
            if (isset($requestData['image'])) {
                $requestData['image'] = self::uploadImage($requestData['image'], $course);
            }
            if (isset($requestData['app_image'])) {
                $requestData['app_image'] = self::uploadAppImage($requestData['app_image'], $course);
            }
            if (isset($requestData['app_icon'])) {
                $requestData['app_icon'] = self::uploadAppIcon($requestData['app_icon'], $course);
            }
            unset($requestData['slug']);
            $course->update($requestData);
        }
        self::storeSchemaScript($id);
        return true;
    }

    public function storeSchemaScript($course_id)
    {
        $course = Course::find($course_id);
        $webPageSchema = Schema::webPage()
            ->name($course->name)
            ->url(env('FRONT_URL') . 'course/' . $course->slug)
            ->speakable(Schema::speakableSpecification()->xpath([
                "/html/head/title",
                "/html/head/meta[@name='description']/@content"
            ]))->toScript();

        /*COURSE SCHEMA*/
        $productSchema = Schema::educationEvent()
            ->name($course->name)
            ->description($course->brief_description ?? $course->name)
            ->image($course->course_icon)
            ->eventAttendanceMode('https://schema.org/OnlineEventAttendanceMode')
            ->eventStatus("http://schema.org/EventScheduled")
            ->startDate(formatDate($course->start_date, 'Y-m-d'))
            ->endDate(formatDate($course->start_date, 'Y-m-d'))
            ->url(env('FRONT_URL') . 'course/' . $course->slug)
            ->performer(Schema::thing()->name('Learnvern'))
            ->organizer(Schema::thing()->name('Learnvern')->url(env('FRONT_URL')))
            ->location(Schema::virtualLocation()->url(env('FRONT_URL') . '/course/' . $course->slug))
            ->toScript();
        /*COURSE SCHEMA END*/

        /*FAQS SCHEMA*/
        $faqsSchema = "";
        if (count($course->relatedFaqs) > 0) {
            $faqsSchema = Schema::fAQPage();
            foreach ($course->relatedFaqs as $faq) {
                $faqMinEntityData[] = Schema::question()->name($faq['question'])->acceptedAnswer(Schema::answer()->text($faq['answer']));
            }
            if (!empty($faqsSchema)) {
                $faqsSchema->mainEntity($faqMinEntityData);
            }
            $faqsSchema = $faqsSchema->toScript();
        }
        /*FAQS SCHEMA END*/

        /*VIDEO OBJECT SCHEMA*/
        $videoObjectSchema = Schema::videoObject()
            ->name($course->name)
            ->description($course->brief_description ?? $course->name)
            ->thumbnailUrl($course->course_icon)
            ->uploadDate(isset($course->created_at) ? formatDate($course->created_at, 'Y-m-d h:i:s') : formatDate($course->updated_at, 'Y-m-d h:i:s'))
            ->if(isset($course->intro_video), function (VideoObject $schema) use ($course) {
                $schema->contentUrl(env('FRONT_URL') . 'course/' . $course->slug)
                    ->embedUrl($course->intro_video_embed);
            })->if(isset($course->intro_youtube_video), function (VideoObject $schema) use ($course) {
                $schema->contentUrl(env('FRONT_URL') . 'course/' . $course->slug)
                    ->embedUrl($course->intro_youtube_video_embed);
            })->toScript();
        /*VIDEO OBJECT SCHEMA END*/

        /*BREADCRUMBS SCHEMA*/
        $breadcrumbsSchema = Schema::breadcrumbList()
            ->itemListElement([
                Schema::listItem()->position(1)->item(Schema::thing()->identifier(env('FRONT_URL'))->name('Home')),
                Schema::listItem()->position(2)->item(Schema::thing()->identifier(env('FRONT_URL') . $course->slug)->name($course->name))
            ])->toScript();
        $schemaScript = $webPageSchema . '
        ' . $productSchema . '
        ' . $faqsSchema . '
        ' . $videoObjectSchema . '
        ' . $breadcrumbsSchema;
        $course->update(['schema_script' => $schemaScript]);
    }

    public function uploadImage($file, $course = NULL)
    {
        $uploadPath = storage_path(Course::IMG_PATH);
        if (isset($course)) {
            $imagePath = $uploadPath . $course->image;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function uploadIconImage($file, $course = NULL)
    {
        $uploadPath = storage_path(Course::ICON_IMG_PATH);
        if (isset($course)) {
            $imagePath = $uploadPath . $course->course_icon;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function uploadAppImage($file, $course = NULL)
    {
        $uploadPath = storage_path(Course::APP_IMG_PATH);
        if (isset($course)) {
            $imagePath = $uploadPath . $course->app_image;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function uploadAppIcon($file, $course = NULL)
    {
        $uploadPath = storage_path(Course::APP_ICON_PATH);
        if (isset($course)) {
            $imagePath = $uploadPath . $course->app_icon;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function updateActiveStatus($request)
    {
        $course = Course::find($request->id);
        $course->is_active = $request->is_active;
        $course->save();
        return true;
    }
}
