<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 03:04 PM
 */

namespace App\Repositories;


use App\Interfaces\TenancyInterface;
use App\Models\PartnersSettings;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;
use DB;

class TenancysettingRepository implements TenancyInterface
{
    public function storeSettings($request)
    {
        if ($request->id != null) {
            $PartnersSettings = PartnersSettings::find($request->id);
            $PartnersSettings->third_party_partner_fees = $request->third_party_partner_fees;
            $PartnersSettings->content_partner_fees = $request->content_partner_fees;
            if (isset($request->third_party_agreement_document)) {
                $doc_third_path = self::third_party_uploadImage($request->third_party_agreement_document);
                $PartnersSettings->third_party_agreement_document = isset($doc_third_path) ? $doc_third_path : '';
            }
            if (isset($request->content_partner_agreement_document)) {
                $doc_content = self::content_uploadImage($request->content_partner_agreement_document);
                $PartnersSettings->content_partner_agreement_document = isset($doc_content) ? $doc_content : '';
            }
            $PartnersSettings->save();
        } else {
            $PartnersSettings = new PartnersSettings();
            $PartnersSettings->third_party_partner_fees = $request->third_party_partner_fees;
            $PartnersSettings->content_partner_fees = $request->content_partner_fees;
            if (isset($request->third_party_agreement_document)) {
                $doc_third_path = self::third_party_uploadImage($request->third_party_agreement_document);
            }
            if (isset($request->content_partner_agreement_document)) {
                $doc_content = self::content_uploadImage($request->content_partner_agreement_document);
            }
            $PartnersSettings->third_party_agreement_document = isset($doc_third_path) ? $doc_third_path : '';
            $PartnersSettings->content_partner_agreement_document = isset($doc_content) ? $doc_content : '';
            $PartnersSettings->save();
        }
    }

    public function third_party_uploadImage($requestData, $PartnersSettings = null)
    {

        $uploadPath = storage_path(PartnersSettings::IMG_PATH);
        if (isset($PartnersSettings)) {
            $imagePath = $uploadPath . $PartnersSettings->third_party_agreement_document;
            @unlink($imagePath);
        }
        $extension = $requestData->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $requestData->move($uploadPath, $fileName);
        return $fileName;
    }

    public function content_uploadImage($requestData, $PartnersSettings = null)
    {
        $uploadPath = storage_path(PartnersSettings::IMG_PATH);
        if (isset($PartnersSettings)) {
            $imagePath = $uploadPath . $PartnersSettings->content_partner_agreement_document;
            @unlink($imagePath);
        }
        $extension = $requestData->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $requestData->move($uploadPath, $fileName);
        return $fileName;
    }
}
