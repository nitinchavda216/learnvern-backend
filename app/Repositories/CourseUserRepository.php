<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:35 PM
 */

namespace App\Repositories;
use App\Interfaces\CourseUserRepositoryInterface;
use App\Models\CourseUser;

class CourseUserRepository implements CourseUserRepositoryInterface
{
    public function __construct()
    {

    }

    public function getCourseEnrollmentsByDate($start_date,$end_date){
        return CourseUser::where('partner_id', session('partner_id'))
            ->whereBetween('created_at', [$start_date." 00:00:00", $end_date." 23:59:59"])->count();
    }

}
