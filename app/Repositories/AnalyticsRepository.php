<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 03:07 PM
 */

namespace App\Repositories;


use App\Interfaces\AnalyticsRepositoryInterface;
use App\Models\User;
use Illuminate\Http\Request;
use Analytics;
use Spatie\Analytics\Period;
use App\Models\ReferralVisitorsTracking;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class AnalyticsRepository implements AnalyticsRepositoryInterface
{

    public function getAnalyticsReferralDataByDate($request)
    {

        $requestData = $request->all();
        if (isset($requestData['date_range']) && $requestData['date_range']) {
            $data_list = (explode(' - ', $requestData['date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $startDate = new \DateTime($startDate);
            $endDate = new \DateTime($endDate);
        } else {
            $startDate = new \DateTime(date('Y-m-d', strtotime('-7 day')));
            $endDate = new \DateTime(date('Y-m-d'));
        }

        $analyticsData = Analytics::performQuery(
            Period::create($startDate, $endDate),
            'ga:sessions',
            [
                'metrics' => 'ga:uniquePageviews',
                'dimensions' => 'ga:pagePath',
                'filters' => 'ga:pagePath=~ref=.*',
                'sort' => '-ga:uniquePageviews'
            ]
        );

        $analyticsData = collect($analyticsData['rows'] ?? [])->map(function (array $pageRow) use ($startDate, $endDate) {
            $url_components = parse_url($pageRow[0]);
            parse_str($url_components['query'], $params);
            if (isset($params['ref']) && $params['ref']) {

                $referalOwnerDetail = User::where('own_referral_code', $params['ref'])->select('first_name', 'email', 'mobile_number')->first();
                $referalActiveDetail = User::where('referral', $params['ref'])->whereBetween('created_at', [$startDate->format('Y-m-d 00:00:00'), $endDate->format('Y-m-d 23:59:59')])->count();

                return [
                    'referral_code' => $params['ref'],
                    'page_views' => (int)$pageRow[1],
                    'owner_name' => $referalOwnerDetail ? $referalOwnerDetail->first_name : '-',
                    'owner_email' => $referalOwnerDetail ? $referalOwnerDetail->email : '-',
                    'owner_phone' => $referalOwnerDetail ? $referalOwnerDetail->mobile_number : '-',
                    'active_referral' => $referalActiveDetail ? $referalActiveDetail : 0,
                ];
            }
        })->toArray();

        return $analyticsData;
    }

    public function getHistoricalAnalyticsReferralData()
    {
        $start_date = date('Y-m-d',strtotime('-1 days'));
        $end_date = date('Y-m-d',strtotime('-1 days'));
        while (strtotime($start_date) <= strtotime($end_date)) {
            $timestamp = strtotime($start_date);
            $day = date('D', $timestamp);

            $start_day = new \DateTime(date('Y-m-d', strtotime($start_date)));

            $analyticsData = Analytics::performQuery(Period::create($start_day, $start_day), 'ga:sessions', [
                    'metrics' => 'ga:uniquePageviews',
                    'dimensions' => 'ga:pagePath',
                    'filters' => 'ga:pagePath=~ref=.*,ga:pagePath=~%5E/r/.*',
                    'sort' => '-ga:uniquePageviews'
                ]
            );

            $analyticsData = collect($analyticsData['rows'])->map(function (array $pageRow) use ($start_date) {
                $url_components = parse_url($pageRow[0]);
                if (!isset($url_components['query'])) {
                    $ref_code = str_replace("/r/", "", $url_components['path']);
                } else {
                    parse_str($url_components['query'], $params);
                    $ref_code = isset($params['ref']) ? $params['ref'] : null;
                }

                if (isset($ref_code)) {
                    $referalOwnerDetail = User::where('own_referral_code', $ref_code)->select('id', 'own_referral_code')->first();
                    $referalActiveDetail = User::where('referral', $ref_code)->whereBetween('created_at', [$start_date . ' 00:00:00', $start_date . ' 23:59:59'])->count();

                    if ($referalOwnerDetail) {
                        $check_referral_visitors_tracking = ReferralVisitorsTracking::where('user_id', $referalOwnerDetail['id'])->where('tracking_date', $start_date)->first();

                        if (!empty($check_referral_visitors_tracking)) {
                            $check_referral_visitors_tracking->referral_link_visits = $check_referral_visitors_tracking->referral_link_visits + (int)$pageRow[1];
                            $check_referral_visitors_tracking->last_tracked_time = date("Y-m-d H:i:s");
                            $check_referral_visitors_tracking->save();
                        } else {
                            ReferralVisitorsTracking::create(
                                [
                                    'referral_code' => $ref_code,
                                    'user_id' => $referalOwnerDetail['id'],
                                    'referral_link_visits' => (int)$pageRow[1],
                                    'referral_registrations' => $referalActiveDetail ? $referalActiveDetail : 0,
                                    'tracking_date' => $start_date,
                                    'last_tracked_time' => date("Y-m-d H:i:s")
                                ]
                            );
                        }
                    } else {
                        $log = ['Referral-Code' => $ref_code, 'description' => 'No Such Referral Code Found : ' . $ref_code];
                        //first parameter passed to Monolog\Logger sets the logging channel name
                        $orderLog = new Logger('referral_visitor_tracking');
                        $orderLog->pushHandler(new StreamHandler(storage_path('logs/referral_visitor_tracking.log')), Logger::INFO);
                        $orderLog->info('ReferralVisitorTracking', $log);
                    }
                } else {
                    $log = ['Referral-Code' => $ref_code, 'description' => 'Unparseable Referral Code. Data Not Found for the Referral Code: ' . $ref_code];
                    //first parameter passed to Monolog\Logger sets the logging channel name
                    $orderLog = new Logger('referral_visitor_tracking');
                    $orderLog->pushHandler(new StreamHandler(storage_path('logs/referral_visitor_tracking.log')), Logger::INFO);
                    $orderLog->info('ReferralVisitorTracking', $log);
                }
            });


            $start_date = date("Y-m-d", strtotime("+1 days", strtotime($start_date)));
        }

        return true;
    }
}
