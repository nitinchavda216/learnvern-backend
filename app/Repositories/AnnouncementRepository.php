<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 03:04 PM
 */

namespace App\Repositories;


use App\Interfaces\AnnouncementInterface;
use App\Models\AnnouncementManagement;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;
use DB;

class AnnouncementRepository implements AnnouncementInterface
{
    public function getAjaxListData($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = AnnouncementManagement::whereNotNull('id');
        if (isset($requestData['course_id'])) {
            $dataInput = $dataInput->where('course_id', $requestData['course_id']);
        }
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id', "DESC")->skip($start)->take($pageSize);
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('title', function ($item) {
            return $item->title ?? "- - -";
        });
        $data_tables->EditColumn('start_date', function ($item) {
            return formatDate($item->start_date) ?? "- - -";
        });
        $data_tables->EditColumn('end_date', function ($item) {
            return formatDate($item->end_date) ?? "- - -";
        });

        $data_tables->EditColumn('action', function ($item) {
            return view('announcement_managment.partials.actions', compact('item'))->render();
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['action']);
        return $data_tables->make(true);
    }

    public function store($request)
    {
        $requestData = $request->except("_token");
        $requestData['start_date'] = Carbon::parse($requestData['start_date'])->toDateTimeString();
        $requestData['end_date'] = Carbon::parse($requestData['end_date'])->toDateTimeString();
        $requestData['is_title_display'] = isset($requestData['is_title_display']) ? 1 : 0;
        AnnouncementManagement::create($requestData);

        return true;
    }

    public function getAnnouncementDetail($id)
    {
        return AnnouncementManagement::findOrFail($id);
    }

    public function updateAnnouncementData($request, $id)
    {
        $requestData = $request->all();
        $requestData['is_title_display'] = isset($requestData['is_title_display']) ? 1 : 0;
        $requestData['start_date']=Carbon::parse($requestData['start_date'])->toDateTimeString();
        $requestData['end_date']=Carbon::parse($requestData['end_date'])->toDateTimeString();
        $faq = self::getAnnouncementDetail($id);
        $faq->update($requestData);

        return true;
    }

    public function updateActiveStatus($request)
    {
        $faq = self::getAnnouncementDetail($request->id);
        $faq->is_active = $request->is_active;
        $faq->save();
        return $faq->course_id;
    }

    public function getFaqsByCourse($course_id)
    {
        $returnData = [];
        if (isset($course_id)) {
            $faqs = Faqs::where('course_id', $course_id)->get();
            foreach ($faqs as $faq) {
                $returnData[] = ['id' => $faq->id, 'question' => $faq->question];
            }
        }
        return $returnData;
    }
}
