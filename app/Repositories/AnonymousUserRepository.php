<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 03:04 PM
 */

namespace App\Repositories;


use App\Interfaces\AnonymousUserInterface;
use App\Models\AnonymousUser;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;
use DB;

class AnonymousUserRepository implements AnonymousUserInterface
{
    public function getAjaxListData($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = AnonymousUser::whereNotNull('id');

        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id', "DESC")->skip($start)->take($pageSize);

        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('city', function (AnonymousUser $user) {
            return isset($user->city) ? $user->city : "- - -";
        });
        $data_tables->EditColumn('state', function (AnonymousUser $user) {
            return isset($user->state) ? $user->state : "- - -";
        });
        $data_tables->EditColumn('country', function (AnonymousUser $user) {
            return isset($user->country) ? $user->country : "- - -";
        });
        $data_tables->EditColumn('created_at', function (AnonymousUser $user) {
            return isset($user->created_at) ? date('d-m-Y', strtotime($user->created_at)): "- - -";
        });

        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        //$data_tables->rawColumns(['action']);
        return $data_tables->make(true);
    }

    public function store($request)
    {
        //AnonymousUser::create($requestData);
        return true;
    }






}
