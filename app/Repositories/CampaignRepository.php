<?php

namespace App\Repositories;

use App\Exports\CampaignExport;
use App\Exports\UsersExport;
use App\Interfaces\CampaignRepositoryInterface;
use App\Interfaces\ReportBuilderRepositoryInterface;
use App\Jobs\SendQueuePushNotification;
use App\Models\Campaign;
use App\Models\CampaignAssign;
use App\Models\CampaignUserCallings;
use App\Models\CampaignUsers;
use App\Models\CourseUser;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\File;
use App\Services\PushNotificationService;

class CampaignRepository implements CampaignRepositoryInterface
{
    protected $reportBuilderRepository;
    protected $pushNotificationService;

    public function __construct(ReportBuilderRepositoryInterface $reportBuilderRepository, PushNotificationService $pushNotificationService)
    {
        $this->reportBuilderRepository = $reportBuilderRepository;
        $this->pushNotificationService = $pushNotificationService;
    }

    public function getCampaignDetails($id)
    {
        return Campaign::where('id', $id)->first();
    }

    public function getAllCampaign($request)
    {

        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $where = "WHERE 1=1";

        if (isset($requestData['accessible_to'])) {
            $where .= " AND campaign_assign.assign_to=" . $requestData['accessible_to'];
        }

        if (isset($requestData['action'])) {
            $where .= " AND campaign.action ='" . $requestData['action'] . "'";
        }
        if (isset($requestData['marketing_goal'])) {
            $where .= " AND campaign.marketing_goal = '" . $requestData['marketing_goal'] . "'";
        }


        $countQuery = "SELECT count(*) AS totalRecord FROM (
                            SELECT campaign.id
                              FROM `campaign`
                              LEFT JOIN campaign_assign on campaign.id = campaign_assign.campaign_id
                              LEFT JOIN admins on campaign.created_by = admins.id
                              LEFT JOIN (SELECT id,name FROM `admins`) ta on ta.id = campaign_assign.assign_to
                       $where GROUP BY campaign.id) temp";


        $countResult = (array)DB::selectOne($countQuery);


        $query = "SELECT campaign.id,campaign.name,campaign.action,campaign.marketing_goal,campaign.created_at,campaign.is_fixed_campaign,admins.name as created_by_name,GROUP_CONCAT(DISTINCT ta.name) as assign_to_user_name
                  FROM `campaign`
                  LEFT JOIN campaign_assign on campaign.id = campaign_assign.campaign_id
                  LEFT JOIN admins on campaign.created_by = admins.id
                  LEFT JOIN (SELECT id,name FROM `admins`) ta on ta.id = campaign_assign.assign_to
                  $where GROUP BY campaign.id ORDER BY campaign.id DESC LIMIT " . $start . "," . $pageSize;


        $resultSet = DB::select($query);
        $resultSet = collect($resultSet)->map(function ($x) {
            $data = (array)$x;
            $data['id'] = $data['id'];
            $data['name'] = $data['name'] ? $data['name'] : '--';
            $data['action'] = $data['action'] ? Campaign::ACTIONS[$data['action']] : '-';
            $data['marketing_goal'] = $data['marketing_goal'] ? ucfirst($data['marketing_goal']) : '-';
            $data['created_by_name'] = $data['created_by_name'] ? $data['created_by_name'] : '-';
            $data['assign_to_user_name'] = $data['assign_to_user_name'] ? $data['assign_to_user_name'] : '-';
            if ($data['action'] == "Calling Users") {
                $data['number_of_user_in_campaign'] = CampaignUserCallings::where('campaign_id', $data['id'])->whereNULL('parent_id')->count();
            } else {
                $data['number_of_user_in_campaign'] = CampaignUsers::where('campaign_id', $data['id'])->count();
            }

            $data['created_at'] = formatDate($data['created_at'], 'd-m-Y');


            $data['actions_menu'] = '<a href="' . route('campaign.viewReport', $data['id']) . '" class="btn btn-secondary btn-xs" title="View Report Data"><i class="fa fa-chart-bar" aria-hidden="true"></i> Statistics</a>';
            $data['actions_menu'] .= '<a href="' . route('campaign.export', $data['id']) . '" class="btn btn-secondary btn-xs ml-1" title="Export"><i class="fa fa-download" aria-hidden="true"></i> Export</a>';

            if ($data['is_fixed_campaign'] == 0) {
                if ($data['action'] != 'Push Notifications'){
                    $data['actions_menu'] .= '<a href="' . route('campaign.edit', $data['id']) . '" class="btn btn-primary btn-xs ml-1"><i class="fas fa-edit"></i></a>';
                }
                $data['actions_menu'] .= '<a href="javascript:void(0);" onclick="confirmDelete(\'' . route('campaign.destroy', $data['id']) . '\')" class="btn btn-danger btn-xs ml-1" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>';
            }

            return $data;
        })->toArray();


        $count_filter = $countResult['totalRecord'];
        $count_total = $countResult['totalRecord'];


        $data_tables = [
            "draw" => $requestData['draw'],
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
            "data" => $resultSet
        ];

        return response()->json($data_tables);
    }


    public function getCampaignReportForCrm($request)
    {


        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $where = "WHERE 1=1";


        if (isset($requestData['search']) && trim($requestData['search'])) {

            $search = trim($requestData['search']);
            $where .= " AND (campaign.marketing_goal LIKE '%$search%' OR campaign.action LIKE '%$search%' OR campaign.name LIKE '%$search%' OR admins.name LIKE '%$search%' OR ta.name LIKE '%$search%')";
        }


        if (isset($requestData['date_range'])) {
            $data_list = (explode(' - ', $requestData['date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $where .= " AND (campaign.created_at BETWEEN  '" . $startDate . " 00:00:00' AND '" . $endDate . " 23:59:59')";
        }


        $countQuery = "SELECT count(campaign_assign.id) AS totalRecord
                              FROM `campaign_assign`
                              LEFT JOIN campaign on campaign_assign.campaign_id = campaign.id
                              LEFT JOIN admins on campaign.created_by = admins.id
                              LEFT JOIN (SELECT id,name FROM `admins`) ta on ta.id = campaign_assign.assign_to
                       $where";


        $countResult = (array)DB::selectOne($countQuery);


        $query = "SELECT campaign.id,campaign.name,campaign.action,campaign.marketing_goal,campaign.created_at,admins.name as created_by_name,ta.name as assign_to_user_name,campaign_assign.assign_to,campaign_assign.number_of_user as count_user,calling_statistics.move_to_followup_count as move_to_followup_count,calling_statistics.not_interest_count as not_interest_count,calling_statistics.other_count as other_count
                  FROM `campaign_assign`
                  LEFT JOIN campaign on campaign_assign.campaign_id = campaign.id
                  LEFT JOIN (

        SELECT campaign_id,assign_to, COUNT(CASE WHEN calling_action = 'move_to_followup' THEN 1 END) AS move_to_followup_count
                      		,COUNT(CASE WHEN calling_action = 'not_interest' THEN 1 END) AS not_interest_count
      						,COUNT(CASE WHEN calling_action = 'other' THEN 1 END) AS other_count
    				  FROM campaign_users_calling where parent_id IS NULL AND calling_action IS NOT NULL GROUP BY assign_to,campaign_id

                  ) calling_statistics ON campaign_assign.campaign_id = calling_statistics.campaign_id AND campaign_assign.assign_to =  calling_statistics.assign_to
                  LEFT JOIN admins on campaign.created_by = admins.id
                  LEFT JOIN (SELECT id,name FROM `admins`) ta on ta.id = campaign_assign.assign_to
                  $where ORDER BY campaign.id DESC LIMIT " . $start . "," . $pageSize;


        $resultSet = DB::select($query);
        $resultSet = collect($resultSet)->map(function ($x) {
            $data = (array)$x;
            $data['id'] = $data['id'];
            $data['name'] = $data['name'] ? $data['name'] : '--';
            $data['marketing_goal'] = $data['marketing_goal'] ? ucfirst($data['marketing_goal']) : '-';
            $data['action'] = $data['action'] ? Campaign::ACTIONS[$data['action']] : '-';

            $data['created_by_name'] = $data['created_by_name'] ? $data['created_by_name'] : '-';
            $data['assign_to_user_name'] = $data['assign_to_user_name'] ? $data['assign_to_user_name'] : '-';
            $data['count_user'] = $data['count_user'] ? $data['count_user'] : 0;
            $data['created_at'] = formatDate($data['created_at'], 'd-m-Y');

            $data['move_to_followup_count'] = $data['move_to_followup_count'] ?? 0;
            $data['not_interest_count'] = $data['not_interest_count'] ?? 0;
            $data['other_count'] = $data['other_count'] ?? '0';
            $data['actions_menu'] = '';

            if ($data['action'] == "Calling Users") {

                if ($data['count_user'] == ($data['move_to_followup_count'] + $data['not_interest_count'] + $data['other_count'])) {
                    $data['actions_menu'] .= '<span class="text-success"><i class="fas fa-check" aria-hidden="true"></i> &nbsp;Completed</span>';

                    if ($data['other_count'] > 0) {
                        $data['actions_menu'] .= '<a href="' . route('crm.calling', $data['id'] . '/' . $data['assign_to'] . '?status=other') . '" class="btn btn-primary btn-xs ml-1 mb-2" title=""><i class="fa fa-phone" aria-hidden="true"></i> Recall Other Status</a>';
                    }

                } else {
                    $data['actions_menu'] .= '<a href="' . route('crm.calling', $data['id'] . '/' . $data['assign_to']) . '" class="btn btn-success btn-xs" title="View Report Data"><i class="fa fa-phone" aria-hidden="true"></i> Start Calling</a>';
                }


                if ($data['move_to_followup_count'] > 0) {
                    $routeUrl = route('crm.followUpStatisticsRevenue', ['campaign' => $data['id']]);
                    if ($data['marketing_goal'] == "Referral") {
                        $routeUrl = route('crm.followUpStatisticsReferral', ['campaign' => $data['id']]);
                    }
                    $data['actions_menu'] .= '<a href="' . $routeUrl . '" class="btn btn-primary btn-xs ml-1" title="View Report Data"><i class="fas fa-chart-bar" aria-hidden="true"></i> Follow Up Statistics</a>';
                }

            }

            $data['actions_menu'] = $data['actions_menu'] ? $data['actions_menu'] : '--';
            return $data;
        })->toArray();


        $count_filter = $countResult['totalRecord'];
        $count_total = $countResult['totalRecord'];


        $data_tables = [
            "draw" => $requestData['draw'],
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
            "data" => $resultSet
        ];

        return response()->json($data_tables);
    }

    public function getAllCampaignForCrm($request)
    {


        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $where = "WHERE 1=1";
        $where .= " AND campaign_assign.assign_to=" . getCurrentAdmin()->id;
        $countQuery = "SELECT count(*) AS totalRecord FROM (
                            SELECT campaign.id
                              FROM `campaign`
                              LEFT JOIN campaign_assign on campaign.id = campaign_assign.campaign_id
                              LEFT JOIN admins on campaign.created_by = admins.id
                              LEFT JOIN (SELECT id,name FROM `admins`) ta on ta.id = campaign_assign.assign_to
                       $where GROUP BY campaign_assign.campaign_id,campaign_assign.assign_to) temp";


        $countResult = (array)DB::selectOne($countQuery);


        $query = "SELECT campaign.id,campaign.name,campaign.action,campaign.marketing_goal,campaign.created_at,admins.name as created_by_name,ta.name as assign_to_user_name,SUM(campaign_assign.number_of_user) as count_user,SUM(calling_statistics.move_to_followup_count) as move_to_followup_count,SUM(calling_statistics.not_interest_count) as not_interest_count,SUM(calling_statistics.other_count) as other_count
                  FROM `campaign`
                  LEFT JOIN campaign_assign on campaign.id = campaign_assign.campaign_id
                  LEFT JOIN (

        SELECT campaign_id, COUNT(CASE WHEN calling_action = 'move_to_followup' THEN 1 END) AS move_to_followup_count
                      		,COUNT(CASE WHEN calling_action = 'not_interest' THEN 1 END) AS not_interest_count
      						,COUNT(CASE WHEN calling_action = 'other' THEN 1 END) AS other_count
    				  FROM campaign_users_calling where assign_to = " . getCurrentAdmin()->id . " AND parent_id IS NULL AND calling_action IS NOT NULL GROUP BY campaign_id

                  ) calling_statistics ON campaign.id = calling_statistics.campaign_id
                  LEFT JOIN admins on campaign.created_by = admins.id
                  LEFT JOIN (SELECT id,name FROM `admins`) ta on ta.id = campaign_assign.assign_to
                  $where GROUP BY campaign_assign.campaign_id,campaign_assign.assign_to ORDER BY campaign.id DESC LIMIT " . $start . "," . $pageSize;


        $resultSet = DB::select($query);
        $resultSet = collect($resultSet)->map(function ($x) {
            $data = (array)$x;
            $data['id'] = $data['id'];
            $data['name'] = $data['name'] ? $data['name'] : '--';
            $data['action'] = $data['action'] ? Campaign::ACTIONS[$data['action']] : '-';
            $data['marketing_goal'] = $data['marketing_goal'] ? ucfirst($data['marketing_goal']) : '-';
            $data['created_by_name'] = $data['created_by_name'] ? $data['created_by_name'] : '-';
            $data['assign_to_user_name'] = $data['assign_to_user_name'] ? $data['assign_to_user_name'] : '-';
            $data['count_user'] = $data['count_user'] ? $data['count_user'] : 0;
            $data['created_at'] = formatDate($data['created_at'], 'd-m-Y');

            $data['move_to_followup_count'] = $data['move_to_followup_count'] ?? 0;
            $data['not_interest_count'] = $data['not_interest_count'] ?? 0;
            $data['other_count'] = $data['other_count'] ?? '0';
            $data['actions_menu'] = '';


            if ($data['action'] == 'Calling Users') {
                $data['actions_menu'] = '<a href="' . route('crm.calling', $data['id']) . '" class="btn btn-success btn-xs" title="View Report Data"><i class="fa fa-phone" aria-hidden="true"></i> Start Calling</a>';
                if ($data['count_user'] == ($data['move_to_followup_count'] + $data['not_interest_count'] + $data['other_count'])) {
                    $data['actions_menu'] = '<span class="text-success">Completed</span>';
                    if ($data['other_count'] > 0) {
                        $data['actions_menu'] .= '<a href="' . route('crm.calling', $data['id'] . '?status=other') . '" class="btn btn-primary btn-xs" title=""><i class="fa fa-phone" aria-hidden="true"></i> ReCall Other Status</a>';
                    }


                }
            } else {
                $data['actions_menu'] = $data['actions_menu'] ? $data['actions_menu'] : '--';
            }


            return $data;
        })->toArray();


        $count_filter = $countResult['totalRecord'];
        $count_total = $countResult['totalRecord'];


        $data_tables = [
            "draw" => $requestData['draw'],
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
            "data" => $resultSet
        ];
        return response()->json($data_tables);
    }

    public function storeCampaign($request)
    {
        try {
            $requestData = $request->all();
            DB::beginTransaction();
            $campaignInsertData['name'] = $requestData['name'];
            $campaignInsertData['action'] = $requestData['action'];
            $campaignInsertData['marketing_goal'] = $requestData['marketing_goal'];
            $campaignInsertData['fields'] = $requestData['fields'];
            $campaignInsertData['conditions'] = $requestData['conditions'];
            $campaignInsertData['is_allow_duplicate'] = $requestData['is_allow_duplicate'] ?? null;
            $campaignInsertData['created_by'] = getCurrentAdmin()->id;
            if ($requestData['action'] == "push-notification") {
                $campaignInsertData['push_notification_details'] = $requestData['push_notification_details'];
                if (isset($campaignInsertData['push_notification_details']['image'])) {
                    $uploadPath = storage_path(Notification::IMG_PATH);
                    $image = $campaignInsertData['push_notification_details']['image'];
                    $imageName = strtotime(date('Y-m-d H:i:s')) . '.' . $image->getClientOriginalExtension();
                    $image->move($uploadPath, $imageName);
                    $campaignInsertData['push_notification_details']['image'] = $imageName;
                }else{
                    $campaignInsertData['push_notification_details']['image'] = null;
                }
                $campaignInsertData['push_notification_details']['course_link_id'] = ($campaignInsertData['push_notification_details']['link_type'] == 'specific_course') ? $campaignInsertData['push_notification_details']['course_link_id'] : null;
                $campaignInsertData['push_notification_details']['custom_link'] = ($campaignInsertData['push_notification_details']['link_type'] == 'custom_link') ? $campaignInsertData['push_notification_details']['custom_link'] : null;
            }
            $campaign = Campaign::create($campaignInsertData);
            $campaign_id = $campaign->id;
            if ($requestData['action'] == "calling-user" && $requestData['chunk_type'] == 1) {
                foreach ($requestData['chunk_distribute']['name'] as $key => $value) {
                    if ($value && $requestData['chunk_distribute']['qty'][$key]) {
                        $campaignAssignData = [];
                        $campaignAssignData['campaign_id'] = $campaign_id;
                        $campaignAssignData['assign_to'] = $value;
                        $campaignAssignData['number_of_user'] = $requestData['chunk_distribute']['qty'][$key];
                        CampaignAssign::create($campaignAssignData);
                    }
                }
            } else {
                $campaignAssignData = [];
                $campaignAssignData['campaign_id'] = $campaign_id;
                $campaignAssignData['assign_to'] = $requestData['assign_to'];
                $campaignAssignData['number_of_user'] = $requestData['number_of_user_campaign'];
                CampaignAssign::create($campaignAssignData);
            }
            $selectedData['report_id'] = $requestData['report_id'];
            $selectedData['fields'] = json_decode($requestData['fields']);
            $selectedData['conditions'] = json_decode($requestData['conditions'], true);
            $selectedData['action'] = $requestData['action'];
            $selectedData['marketing_goal'] = $requestData['marketing_goal'];
            $selectedData['is_allow_duplicate'] = $requestData['is_allow_duplicate'] ?? 0;
            if ($requestData['checkAllUsers'] == "true") {
                $extractID = isset($requestData['unselect_course_user_ids']) ? json_decode($requestData['unselect_course_user_ids'], true) : [];
                $conditionApply = '';
                if ($extractID) {
                    $conditionApply = " AND course_user.id NOT IN (" . implode(',', $extractID) . ")";
                }
                $requestData['sql_query'] = $this->reportBuilderRepository->campaignBuildQuery($selectedData, $campaign_id, $conditionApply);
            } else {
                $importIDS = json_decode($requestData['course_user_ids'], true);
                $conditionApply = '';
                if ($importIDS) {
                    $conditionApply = " AND course_user.id IN (" . implode(',', $importIDS) . ")";
                }
                $requestData['sql_query'] = $this->reportBuilderRepository->campaignBuildQuery($selectedData, $campaign_id, $conditionApply);
            }
            DB::insert("INSERT INTO campaign_users (campaign_id ,course_user_id,user_id,course_id,created_at) " . $requestData['sql_query']);
            if ($campaign->action == "calling-user") {
                if ($requestData['chunk_type'] == 1) {
                    $start = 0;
                    foreach ($requestData['chunk_distribute']['name'] as $key => $value) {
                        if ($value && $requestData['chunk_distribute']['qty'][$key]) {
                            $offset = $requestData['chunk_distribute']['qty'][$key];
                            DB::insert("INSERT INTO campaign_users_calling (campaign_id ,campaign_user_id,user_id,course_id,assign_to,created_at) select campaign_id,id,user_id,course_id, " . $value . " as assign_to,created_at FROM campaign_users WHERE campaign_id=$campaign->id LIMIT $start,$offset");
                            $start = $start + $offset;
                        }
                    }
                } else {
                    DB::insert("INSERT INTO campaign_users_calling (campaign_id ,campaign_user_id,user_id,course_id,assign_to,created_at) select campaign_id,id,user_id,course_id, " . $requestData['assign_to'] . " as assign_to,created_at FROM campaign_users WHERE campaign_id=$campaign->id");
                }
                if ($selectedData['marketing_goal'] == "revenue") {
                    $deleteOtherRecord = DB::select("SELECT campaign_users_calling.*
                                            FROM campaign_users_calling
                                            LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
                                            WHERE parent_id IS NULL AND calling_action IS NULL AND user_id IN(select user_id from campaign_users_calling where campaign_id=$campaign->id) AND campaign.marketing_goal !='revenue'
                                            ");
                    if ($deleteOtherRecord) {
                        foreach ($deleteOtherRecord as $item) {
                            DB::table('campaign_assign')->where('campaign_id', '=', $item->campaign_id)->where('assign_to', '=', $item->assign_to)->decrement('number_of_user');
                            DB::table('campaign_users')->where('id', '=', $item->campaign_user_id)->delete();
                            DB::table('campaign_users_calling')->where('id', '=', $item->id)->delete();
                        }
                    }
                }
            }
            if ($requestData['action'] == "push-notification") {
                $jobData['campaign_id'] = $campaign->id;
                $jobData['notification_details'] = $campaignInsertData['push_notification_details'];
                $jobData['condition'] = $conditionApply;
                $job = (new SendQueuePushNotification($jobData))
                    ->delay(now()->addSeconds(2));
                dispatch($job);
            }
            DB::commit();
            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function storeCampaignInCampaign($request)
    {
        try {
            DB::beginTransaction();
            $requestData = $request->except('_token');
            $selectedData['report_id'] = $requestData['report_id'];
            $selectedData['fields'] = '';
            $selectedData['conditions'] = '';
            $requestData['sql_query']['base_query'] = '';
            $requestData['sql_query']['count_query'] = '';
            $campaign = Campaign::create($requestData);
            $campaign_id = $campaign->id;


            $where = "WHERE 1=1";

            if ($requestData['checkAllUsers'] == "true") {
                $extractID = isset($requestData['unselect_course_user_ids']) ? json_decode($requestData['unselect_course_user_ids'], true) : [];
                if ($extractID) {
                    $where .= " AND campaign_users.id NOT IN (" . implode(',', $extractID) . ")";
                }
            } else {
                $importIDS = json_decode($requestData['course_user_ids'], true);
                if ($importIDS) {
                    $where .= " AND campaign_users.id IN (" . implode(',', $importIDS) . ")";
                }
            }


            $where .= " AND campaign_users.campaign_id = " . $requestData['report_id'];

            if ($requestData['marketing_goal'] == "revenue") {
                if (isset($requestData['payment_status']) && $requestData['payment_status'] != "") {

                    $where .= " AND course_user.paid_status =" . $requestData['payment_status'];
                }

                if (isset($requestData['progress']) && $requestData['progress']) {
                    $progressArray = explode('-', $requestData['progress']);
                    $where .= " AND ROUND(course_user.progress) >=" . $progressArray[0] . " AND ROUND(course_user.progress)<=" . $progressArray[1];
                }

                $query = "SELECT $campaign->id as campaign_id,campaign_users.course_user_id,campaign_users.user_id,campaign_users.course_id,now() as created_at
                          FROM `campaign_users`
                          LEFT JOIN course_user ON campaign_users.course_user_id = course_user.id
                          $where";

            }

            if ($requestData['marketing_goal'] == "growth") {

                $campaignDetail = Campaign::where('id', $requestData['report_id'])->first();


                $campaign_date = date('Y-m-d', strtotime($campaignDetail->created_at));

                if (isset($requestData['progress']) && $requestData['progress']) {

                    $progressArray = explode('-', $requestData['progress']);
                    if ($progressArray[0] == 0) {
                        $where .= " AND (course_progress_table.progress IS NULL OR ROUND(course_progress_table.progress)<=" . $progressArray[1] . ")";
                    } else {
                        $where .= " AND ROUND(course_progress_table.progress) >=" . $progressArray[0] . " AND ROUND(course_progress_table.progress)<=" . $progressArray[1];
                    }

                }

                $query = "SELECT $campaign->id as campaign_id,campaign_users.course_user_id,campaign_users.user_id,campaign_users.course_id,now() as created_at
                         FROM `campaign_users`
                         LEFT JOIN (SELECT course_user_id,MAX(progress) as progress,MAX(created_at)as created_at FROM `course_progress_log_by_day` WHERE DATE(created_at) >= '$campaign_date' GROUP BY course_user_id ORDER BY progress DESC) course_progress_table ON campaign_users.course_user_id = course_progress_table.course_user_id
                         $where";
            }

            if ($requestData['marketing_goal'] == "referral") {

                $campaignDetail = Campaign::where('id', $requestData['report_id'])->first();

                $campaign_date = date('Y-m-d', strtotime($campaignDetail->created_at));

                if (isset($requestData['progress']) && $requestData['progress']) {

                    $progressArray = explode('-', $requestData['progress']);
                    $where .= " AND high_progress >=" . $progressArray[0] . " AND high_progress<=" . $progressArray[1];
                }

                $query = "SELECT $campaign->id as campaign_id,campaign_users.course_user_id,campaign_users.user_id,campaign_users.course_id,now() as created_at
                          FROM `campaign_users`
                          LEFT JOIN (SELECT count(user_id) as no_of_course_enroll,user_id,Round(MAX(progress)) as high_progress,course_id FROM `course_user` GROUP by user_id) course_progress on course_progress.user_id = campaign_users.user_id
                          $where GROUP BY campaign_users.user_id";
            }

            DB::insert("INSERT INTO campaign_users (campaign_id ,course_user_id,user_id,course_id,created_at) " . $query);
            if ($campaign->action == "calling-user") {
                DB::insert("INSERT INTO campaign_users_calling (campaign_id ,campaign_user_id,user_id,course_id,created_at) select campaign_id,id,user_id,course_id,created_at FROM campaign_users WHERE campaign_id=$campaign->id");
            }
            DB::commit();
            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function getReportData($id)
    {
        return Campaign::findOrFail($id);
    }

    public function exportData($id)
    {
        $campaignDetail = Campaign::where('id', $id)->select('name')->first();

        $userData = DB::select("select users.first_name,users.email,users.mobile_number,CONCAT(users.own_referral_code,' '),CONCAT('https://www.learnvern.com/r/',users.own_referral_code) as own_referral_link,courses.name
              FROM campaign_users
              LEFT JOIN users on campaign_users.user_id = users.id
              LEFT JOIN courses on campaign_users.course_id = courses.id
              WHERE campaign_users.campaign_id=" . $id);
        if (count($userData) == 0) {
            return ['status' => false];
        }

        return Excel::download(new CampaignExport($userData, (array)$userData[0]), $campaignDetail->name . '.xlsx');
    }

    public function storeCampaignUser($campaign_id, $course_user_id)
    {
        $array = explode('-', $course_user_id);
        $existData = CampaignUsers::where('campaign_id', $campaign_id)->where('course_user_id', $array[0])
            ->where('user_id', $array[1])->where('course_id', $array[2])->first();
        if (is_null($existData)) {
            CampaignUsers::create([
                'campaign_id' => $campaign_id,
                'course_user_id' => $array[0],
                'user_id' => $array[1],
                'course_id' => $array[2],
            ]);
        }
    }

    public function viewReportAjaxDataReferral($request, $campaignDetail)
    {

        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $campaign_date = date('Y-m-d', strtotime($campaignDetail->created_at));
        $where = "WHERE campaign_id = " . $requestData['report_id'];

        if (isset($requestData['progress']) && $requestData['progress']) {

            $progressArray = explode('-', $requestData['progress']);
            $where .= " AND high_progress >=" . $progressArray[0] . " AND high_progress<=" . $progressArray[1];
        }

        $countQuery = "SELECT count(user_id) AS totalRecord FROM (
        SELECT campaign_users.user_id
      FROM `campaign_users`
      LEFT JOIN users on campaign_users.user_id = users.id
      LEFT JOIN (SELECT count(user_id) as no_of_course_enroll,user_id,MAX(progress) as high_progress,course_id FROM `course_user` GROUP by user_id) course_progress on course_progress.user_id = campaign_users.user_id
      LEFT JOIN courses on course_progress.course_id = courses.id
      LEFT JOIN (SELECT COUNT(*) as total_referral_before_campaign,referral,temp_user.user_id
                    FROM `users`
                    LEFT JOIN (select id as user_id,own_referral_code from users) temp_user on temp_user.own_referral_code=users.referral
                    WHERE referral !='' AND DATE(created_at) < '$campaign_date'  GROUP BY referral) temp_ref on temp_ref.user_id = campaign_users.user_id
      LEFT JOIN (SELECT COUNT(*) as total_referral_after_campaign,referral,temp_after_user.user_id
                    FROM `users`
                    LEFT JOIN (select id as user_id,own_referral_code from users) temp_after_user on temp_after_user.own_referral_code=users.referral
                    WHERE referral !='' AND DATE(created_at) >= '$campaign_date'  GROUP BY referral) temp_ref_after on temp_ref_after.user_id = campaign_users.user_id
      $where GROUP BY campaign_users.user_id) temp";


        $countResult = (array)DB::selectOne($countQuery);


        $query = "
      SELECT campaign_users.*,users.first_name,users.email,users.mobile_number,users.own_referral_code,course_progress.no_of_course_enroll,course_progress.high_progress,courses.name as course_name,temp_ref.total_referral_before_campaign,temp_ref_after.total_referral_after_campaign
      FROM `campaign_users`
      LEFT JOIN users on campaign_users.user_id = users.id
      LEFT JOIN (SELECT count(user_id) as no_of_course_enroll,user_id,Round(MAX(progress)) as high_progress,course_id FROM `course_user` GROUP by user_id) course_progress on course_progress.user_id = campaign_users.user_id
      LEFT JOIN courses on course_progress.course_id = courses.id
      LEFT JOIN (SELECT COUNT(*) as total_referral_before_campaign,referral,temp_user.user_id
                    FROM `users`
                    LEFT JOIN (select id as user_id,own_referral_code from users) temp_user on temp_user.own_referral_code=users.referral
                    WHERE referral !='' AND DATE(created_at) < '$campaign_date'  GROUP BY referral) temp_ref on temp_ref.user_id = campaign_users.user_id
      LEFT JOIN (SELECT COUNT(*) as total_referral_after_campaign,referral,temp_after_user.user_id
                    FROM `users`
                    LEFT JOIN (select id as user_id,own_referral_code from users) temp_after_user on temp_after_user.own_referral_code=users.referral
                    WHERE referral !='' AND DATE(created_at) >= '$campaign_date'  GROUP BY referral) temp_ref_after on temp_ref_after.user_id = campaign_users.user_id
      $where GROUP BY campaign_users.user_id LIMIT " . $start . "," . $pageSize;


        $resultSet = DB::select($query);


        $resultSet = collect($resultSet)->map(function ($x) {
            $data = (array)$x;
            $checkboxString = $data['id'];
            $data['first_name'] = $data['first_name'] ? $data['first_name'] : '--';
            $data['email'] = $data['email'] ? $data['email'] : '-';
            $data['own_referral_code'] = $data['own_referral_code'] ? $data['own_referral_code'] : '-';
            $data['own_referral_link'] = $data['own_referral_code'] ? "https://www.learnvern.com/r/" . $data['own_referral_code'] : '-';
            $data['mobile_number'] = $data['mobile_number'] ? $data['mobile_number'] : '--';
            $data['no_of_course_enroll'] = $data['no_of_course_enroll'] ? $data['no_of_course_enroll'] : '0';
            $data['course_name'] = $data['course_name'] ? $data['course_name'] : '--';
            $data['high_progress'] = $data['high_progress'] ? $data['high_progress'] : '0';
            $data['total_referral_before_campaign'] = $data['total_referral_before_campaign'] ? $data['total_referral_before_campaign'] : '0';
            $data['total_referral_after_campaign'] = $data['total_referral_after_campaign'] ? $data['total_referral_after_campaign'] : '0';
            $data['checkbox'] = '<input type="checkbox" class="user_id" id="' . $checkboxString . '" name="campaign_select[]" value="' . $checkboxString . '">';
            return $data;
        })->toArray();


        $count_filter = $countResult['totalRecord'];
        $count_total = $countResult['totalRecord'];


        $data_tables = [
            "draw" => $requestData['draw'],
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
            "data" => $resultSet
        ];

        return response()->json($data_tables);

    }


    public function viewReportAjaxDataRevenue($request, $campaignDetail)
    {

        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;


        $where = "WHERE campaign_id = " . $requestData['report_id'];


        if (isset($requestData['payment_status']) && $requestData['payment_status'] != "") {

            $where .= " AND course_user.paid_status =" . $requestData['payment_status'];
        }


        if (isset($requestData['progress']) && $requestData['progress']) {

            $progressArray = explode('-', $requestData['progress']);
            $where .= " AND ROUND(course_user.progress) >=" . $progressArray[0] . " AND ROUND(course_user.progress)<=" . $progressArray[1];
        }


        $orderBy = " ORDER BY high_progress ASC";
        if (isset($requestData['order'][0]['column'])) {

            if ($requestData['order'][0]['column'] == 7) {
                $orderBy = " ORDER BY high_progress " . $requestData['order'][0]['dir'];
            } else if ($requestData['order'][0]['column'] == 8) {
                $orderBy = " ORDER BY course_user.paid_status " . $requestData['order'][0]['dir'];
            } else if ($requestData['order'][0]['column'] == 9) {
                $orderBy = " ORDER BY course_user.last_used_date " . $requestData['order'][0]['dir'];
            }

        }

        $countQuery = "SELECT count(campaign_users.user_id) as totalRecord FROM `campaign_users`
                      LEFT JOIN users ON campaign_users.user_id = users.id
                      LEFT JOIN course_user ON campaign_users.course_user_id = course_user.id
                      LEFT JOIN courses ON campaign_users.course_id = courses.id $where";

        $countResult = (array)DB::selectOne($countQuery);


        $query = "
      SELECT campaign_users.*,users.first_name,users.email,users.mobile_number,users.own_referral_code,ROUND(course_user.progress) AS high_progress,courses.name AS course_name,course_user.`paid_status`,course_user.`last_used_date`
      FROM `campaign_users`
      LEFT JOIN users ON campaign_users.user_id = users.id
      LEFT JOIN course_user ON campaign_users.course_user_id = course_user.id
      LEFT JOIN courses ON campaign_users.course_id = courses.id
      $where $orderBy LIMIT " . $start . "," . $pageSize;

        $resultSet = DB::select($query);


        $resultSet = collect($resultSet)->map(function ($x) {
            $data = (array)$x;
            $checkboxString = $data['id'];
            $data['first_name'] = $data['first_name'] ? $data['first_name'] : '--';
            $data['email'] = $data['email'] ? $data['email'] : '-';
            $data['mobile_number'] = $data['mobile_number'] ? $data['mobile_number'] : '--';
            $data['own_referral_code'] = $data['own_referral_code'] ? $data['own_referral_code'] : '--';
            $data['own_referral_link'] = $data['own_referral_code'] ? "https://www.learnvern.com/r/" . $data['own_referral_code'] : '--';
            $data['course_name'] = $data['course_name'] ? $data['course_name'] : '--';
            $data['high_progress'] = $data['high_progress'] ? $data['high_progress'] : '0';
            $data['paid_status'] = $data['paid_status'] == 1 ? 'Yes' : 'No';
            $data['last_used_date'] = $data['last_used_date'] ? formatDate($data['last_used_date'], 'd-m-Y') : '--';
            $data['checkbox'] = '<input type="checkbox" class="user_id" id="' . $checkboxString . '" name="campaign_select[]" value="' . $checkboxString . '">';
            return $data;
        })->toArray();


        $count_filter = $countResult['totalRecord'];
        $count_total = $countResult['totalRecord'];


        $data_tables = [
            "draw" => $requestData['draw'],
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
            "data" => $resultSet
        ];

        return response()->json($data_tables);

    }


    public function viewReportAjaxDataGrowth($request, $campaignDetail)
    {

        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;

        $campaign_date = date('Y-m-d', strtotime($campaignDetail->created_at));


        $where = "WHERE campaign_id = " . $requestData['report_id'];
        if (isset($requestData['progress']) && $requestData['progress']) {

            $progressArray = explode('-', $requestData['progress']);
            if ($progressArray[0] == 0) {
                $where .= " AND (course_progress_table.progress IS NULL OR ROUND(course_progress_table.progress)<=" . $progressArray[1] . ")";
            } else {
                $where .= " AND ROUND(course_progress_table.progress) >=" . $progressArray[0] . " AND ROUND(course_progress_table.progress)<=" . $progressArray[1];
            }


        }


        $orderBy = " ORDER BY campaign_after_progress ASC";
        if (isset($requestData['order'][0]['column']) && $requestData['order'][0]['column'] == 7) {
            $orderBy = " ORDER BY campaign_after_progress " . $requestData['order'][0]['dir'];
        }


        $countQuery = "SELECT count(campaign_users.user_id) as totalRecord FROM `campaign_users`
                      LEFT JOIN users ON campaign_users.user_id = users.id
                      LEFT JOIN course_user ON campaign_users.course_user_id = course_user.id
                      LEFT JOIN (SELECT course_user_id,MAX(progress) as progress,MAX(created_at)as created_at FROM `course_progress_log_by_day` WHERE DATE(created_at) >= '$campaign_date' GROUP BY course_user_id ORDER BY progress DESC) course_progress_table ON campaign_users.course_user_id = course_progress_table.course_user_id
                      LEFT JOIN courses ON campaign_users.course_id = courses.id
                      $where";

        $countResult = (array)DB::selectOne($countQuery);


        $query = "
      SELECT campaign_users.*,users.first_name,users.email,users.mobile_number,users.own_referral_code,ROUND(course_user.progress) AS course_user_progress,courses.name AS course_name,ROUND(course_progress_table.progress) as campaign_after_progress
      FROM `campaign_users`
      LEFT JOIN users ON campaign_users.user_id = users.id
      LEFT JOIN course_user ON campaign_users.course_user_id = course_user.id
      LEFT JOIN (SELECT course_user_id,MAX(progress) as progress,MAX(created_at)as created_at FROM `course_progress_log_by_day` WHERE DATE(created_at) >= '$campaign_date' GROUP BY course_user_id ORDER BY progress DESC) course_progress_table ON campaign_users.course_user_id = course_progress_table.course_user_id
      LEFT JOIN courses ON campaign_users.course_id = courses.id
      $where $orderBy LIMIT " . $start . "," . $pageSize;

        $resultSet = DB::select($query);


        $resultSet = collect($resultSet)->map(function ($x) {
            $data = (array)$x;
            $checkboxString = $data['id'];
            $data['first_name'] = $data['first_name'] ? $data['first_name'] : '--';
            $data['email'] = $data['email'] ? $data['email'] : '-';
            $data['own_referral_code'] = $data['own_referral_code'] ? $data['own_referral_code'] : '--';
            $data['own_referral_link'] = $data['own_referral_code'] ? "https://www.learnvern.com/r/" . $data['own_referral_code'] : '--';
            $data['mobile_number'] = $data['mobile_number'] ? $data['mobile_number'] : '--';
            $data['course_name'] = $data['course_name'] ? $data['course_name'] : '--';
            $data['course_user_progress'] = $data['course_user_progress'] ? $data['course_user_progress'] : '0';
            $data['campaign_after_progress'] = $data['campaign_after_progress'] ? $data['campaign_after_progress'] : '0';
            $data['checkbox'] = '<input type="checkbox" class="user_id" id="' . $checkboxString . '" name="campaign_select[]" value="' . $checkboxString . '">';
            return $data;
        })->toArray();


        $count_filter = $countResult['totalRecord'];
        $count_total = $countResult['totalRecord'];


        $data_tables = [
            "draw" => $requestData['draw'],
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
            "data" => $resultSet
        ];

        return response()->json($data_tables);

    }


    public function viewCallingUserData($request)
    {

        $requestData = $request->all();
        $pageSize = $request->length ?? 50;
        $start = $request->start ?? 0;

        $campaignDetail = (array)DB::selectOne("select marketing_goal from campaign where id=" . $requestData['campaign_id']);

        /*if (isset($requestData['assignTo']) && $requestData['assignTo']) {
            $where = "WHERE campaign_users_calling.campaign_id = " . $requestData['campaign_id'] . " AND campaign_users_calling.calling_action IS NULL AND campaign_users_calling.parent_id IS NULL AND campaign_users_calling.assign_to=" . $requestData['assignTo'];
        } else {
            $where = "WHERE campaign_users_calling.campaign_id = " . $requestData['campaign_id'] . " AND campaign_users_calling.calling_action IS NULL AND campaign_users_calling.parent_id IS NULL AND campaign_users_calling.assign_to=" . getCurrentAdmin()->id;
        }*/


        if (isset($requestData['assignTo']) && $requestData['assignTo']) {
            $where = "WHERE campaign_users_calling.campaign_id = " . $requestData['campaign_id'] . " AND campaign_users_calling.assign_to=" . $requestData['assignTo'];
        } else {
            $where = "WHERE campaign_users_calling.campaign_id = " . $requestData['campaign_id'] . " AND campaign_users_calling.assign_to=" . getCurrentAdmin()->id;
        }

        if (isset($requestData['status']) && $requestData['status'] == "other") {
            $where .= " AND campaign_users_calling.calling_action = '" . $requestData['status'] . "' AND campaign_users_calling.parent_id IS NULL";
        } else {
            $where .= " AND campaign_users_calling.calling_action IS NULL AND campaign_users_calling.parent_id IS NULL";
        }


        if (isset($requestData['action']) && $requestData['action']) {
            $where .= " AND campaign_users_calling.calling_action = '" . $requestData['action'] . "'";
        }

        if (isset($requestData['search']) && trim($requestData['search'])) {
            $search = trim($requestData['search']);
            $where .= " AND (users.first_name LIKE '%$search%' OR users.email LIKE '%$search%' OR users.mobile_number LIKE '%$search%' OR courses.name LIKE '%$search%')";
        }

        $countQuery = "SELECT count(campaign_users_calling.id) as totalRecord FROM `campaign_users_calling`
                      LEFT JOIN users ON campaign_users_calling.user_id = users.id
                      LEFT JOIN courses ON campaign_users_calling.course_id = courses.id
                      $where";
        $countResult = (array)DB::selectOne($countQuery);


        $query = "
          SELECT campaign_users_calling.id,campaign_users_calling.user_id,users.first_name,users.own_referral_code,users.email,users.isd_code,users.mobile_number,users.education,users.current_occupation,users.referred_count,users.referral,users.last_login_date,courses.name AS course_name,campaign_users_calling.calling_action,campaign_users_calling.followup_date,campaign_users_calling.notes,dtable.countforduplicate,course_info.courses_selected,course_info.number_of_course_enroll,course_info.total_bought_certificate
          FROM `campaign_users_calling`
          LEFT JOIN users ON campaign_users_calling.user_id = users.id
          LEFT JOIN (select course_user.user_id,GROUP_CONCAT(CONCAT(courses.name,' (',ROUND(course_user.progress),'%)',' - ',IF(course_user.paid_status<1, 'Free', 'Paid'))) as courses_selected,count(course_user.id) as number_of_course_enroll,SUM(course_user.paid_status) as total_bought_certificate from course_user LEFT JOIN courses ON course_user.course_id = courses.id GROUP BY course_user.user_id) course_info ON campaign_users_calling.user_id = course_info.user_id
          LEFT JOIN (select user_id,count(campaign_users_calling.id) as countforduplicate FROM campaign_users_calling LEFT JOIN campaign on campaign_users_calling.campaign_id = campaign.id WHERE campaign_users_calling.calling_action IS NULL AND campaign.marketing_goal='" . $campaignDetail['marketing_goal'] . "' group by user_id) dtable ON campaign_users_calling.user_id = dtable.user_id
          LEFT JOIN courses ON campaign_users_calling.course_id = courses.id
          $where ORDER BY campaign_users_calling.id LIMIT " . $start . "," . $pageSize;


        $resultSet = DB::select($query);

        $resultSet = collect($resultSet)
            ->map(function ($x) use ($campaignDetail) {
                $data = (array)$x;
                $data['mobile_number'] = $data['mobile_number'];
                if ($data['mobile_number']) {
                    $isdCode = 91;
                    if ($data['isd_code']) {
                        $isdCode = $data['isd_code'];
                    }
                    $data['mobile_number'] = '<a target="_blank" href="https://wa.me/' . $isdCode . $data['mobile_number'] . '?text=Hi">' . $data['mobile_number'] . '</a>';
                } else {
                    $data['mobile_number'] = '--';
                }

                if (empty($data['first_name'])) {
                    $data['first_name'] = '--';
                }

                if ($data['countforduplicate'] > 1) {

                    $alreadyAssignDetail = (array)DB::selectOne("select campaign.name as campaign_name,admins.name as assign_to_name,campaign_users_calling.followup_date,campaign_users_calling.created_at
                                        FROM campaign_users_calling
                                        LEFT JOIN admins ON campaign_users_calling.assign_to = admins.id
                                        LEFT JOIN campaign on campaign_users_calling.campaign_id = campaign.id WHERE campaign_users_calling.calling_action IS NULL AND campaign.marketing_goal='" . $campaignDetail['marketing_goal'] . "' AND campaign_users_calling.id !=" . $data['id']);
                    $html = '';
                    if ($alreadyAssignDetail) {

                        if ($alreadyAssignDetail['followup_date']) {
                            $html .= '<div><div><h6 class=\'text-center\'>Last Called</h6></div><div><h6>Date: ' . formatDate($alreadyAssignDetail['created_at'], 'd-m-Y') . '</h6></div><div><h6>Status: Follow UP</h6></div></div>';
                        }
                        $html .= '<div><h6>Campaign: ' . $alreadyAssignDetail['campaign_name'] . '</h6></div><div><h6>Assign To: ' . $alreadyAssignDetail['assign_to_name'] . '</h6></div>';
                    }
                    $data['first_name'] = '<a href="javascript:;" class="showTooltip text-dark" data-toggle="tooltip" data-title="' . $html . '"><span class="text-danger" style="text-decoration: underline;text-decoration-style:dashed;">' . $data['first_name'] . '</span></a>';
                }


                if (empty($data['email'])) {
                    $data['email'] = '--';
                }

                $data['course_name'] = $data['course_name'] ? $data['course_name'] : '--';
                $data['move_to_followup'] = '<div><label class="rdiobox"><input id="move_to_followup' . $data['id'] . '" class="actionPerform" type="radio" name="calling_action_' . $data['id'] . '" value="move_to_followup"/><span></span></label></div>';
                $data['other'] = '<div><label class="rdiobox"><input id="other' . $data['id'] . '" class="actionPerform" type="radio" name="calling_action_' . $data['id'] . '" value="other"/><span></span></label></div>';
                $data['not_interest'] = '<div><label class="rdiobox"><input id="not_interest' . $data['id'] . '" class="actionPerform" type="radio" name="calling_action_' . $data['id'] . '" value="not_interest"/><span></span></label></div>';
                $data['name'] = $data['name'] ?? '--';
                $data['referred_count'] = $data['referred_count'] ?? '0';
                $data['referral'] = $data['referral'] ?? '--';
                $data['own_referral_link'] = $data['own_referral_code'] ? "https://www.learnvern.com/r/" . $data['own_referral_code'] : '--';
                $data['last_login_date'] = $data['last_login_date'] ? formatDate($data['last_login_date'], 'd-m-Y') : '--';

                return $data;
            })->toArray();


        $count_filter = $countResult['totalRecord'];
        $count_total = $countResult['totalRecord'];


        $data_tables = [
            "draw" => $requestData['draw'],
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
            "data" => $resultSet
        ];

        return response()->json($data_tables);

    }


    public function deleteCampaign($id)
    {
        try {
            DB::beginTransaction();
            DB::table('campaign')->where('id', $id)->delete();
            DB::table('campaign_assign')->where('campaign_id', $id)->delete();
            DB::table('campaign_users')->where('campaign_id', $id)->delete();
            DB::table('campaign_users_calling')->where('campaign_id', $id)->delete();

            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'message' => $e->getMessage()]);
        }

        return true;
    }

    public function markAsCompleteCampaign($id)
    {
        $campaign = Campaign::where('id', $id)->first();
        $campaign->update(['is_complete' => 1, 'complete_date' => date('Y-m-d')]);
        return true;
    }

    public function getCallingStatisticsData($campaignId)
    {

        $defaultResult = [];
        foreach (Campaign::CALLING_ACTIONS as $key => $value) {
            $defaultResult[$key] = 0;
        }
        $callingResult = DB::select("SELECT calling_action,count(*) as total FROM `campaign_users_calling` WHERE campaign_id=" . $campaignId . " AND calling_action IS NOT NULL group by calling_action");
        if ($callingResult) {
            foreach ($callingResult as $calling) {
                $defaultResult[$calling->calling_action] = $calling->total;
            }
        }
        $defaultResult['numberOfTotalRecord'] = CampaignUserCallings::where('campaign_id', $campaignId)->whereNULL('parent_id')->count();
        $defaultResult['callingCompleted'] = CampaignUserCallings::where('campaign_id', $campaignId)->whereNotNull('calling_action')->count();
        return $defaultResult;
    }

    public function saveCallingDetail($request)
    {
        $requestData = $request->all();
        try {
            DB::beginTransaction();
            $campaignUserCallingDetail = CampaignUserCallings::where('id', $requestData['id'])->first();
            if ($campaignUserCallingDetail) {
                if ($requestData['action'] == "move_to_followup") {

                    DB::table('campaign_users_calling')->insert([
                        'campaign_id' => $campaignUserCallingDetail->campaign_id,
                        'campaign_user_id' => $campaignUserCallingDetail->campaign_user_id,
                        'user_id' => $campaignUserCallingDetail->user_id,
                        'course_id' => $campaignUserCallingDetail->course_id,
                        'parent_id' => $campaignUserCallingDetail->id,
                        'assign_to' => $campaignUserCallingDetail->assign_to,
                        'followup_date' => (isset($requestData['followup_date']) && $requestData['followup_date']) ? $requestData['followup_date'] : null,
                        'notes' => (isset($requestData['notes']) && $requestData['notes']) ? $requestData['notes'] : null,
                        'created_at' => date('Y-m-d H:i:s'),
                        'top_parent_id' => $campaignUserCallingDetail->top_parent_id ? $campaignUserCallingDetail->top_parent_id : $campaignUserCallingDetail->id,
                        'first_call_date' => $campaignUserCallingDetail->first_call_date ? $campaignUserCallingDetail->first_call_date : date('Y-m-d H:i:s')
                    ]);

                    if (isset($requestData['assignOtherCampaign']) && $requestData['assignOtherCampaign']) {

                        $getMarketingGoal = Campaign::where('id', $campaignUserCallingDetail->campaign_id)->first();
                        $getCourseUserDetail = CourseUser::where('course_id', $campaignUserCallingDetail->course_id)->where('user_id', $campaignUserCallingDetail->user_id)->first();

                        $assignToCampaignData = [];
                        if ($getMarketingGoal->marketing_goal == "revenue") {
                            $referralCampaign = Campaign::where('marketing_goal', 'referral')->where('is_fixed_campaign', 1)->first();
                            $assignToCampaignData['campaign_id'] = $referralCampaign->id;
                        } else {
                            $revenueCampaign = Campaign::where('marketing_goal', 'revenue')->where('is_fixed_campaign', 1)->first();
                            $assignToCampaignData['campaign_id'] = $revenueCampaign->id;
                        }
                        $assignToCampaignData['course_user_id'] = $getCourseUserDetail->id;
                        $assignToCampaignData['action_taken_by'] = getCurrentAdmin()['id'];
                        $assignToCampaignData['calling_action'] = $requestData['action'];
                        $assignToCampaignData['action'] = $requestData['action'];
                        $assignToCampaignData['assign_to'] = getCurrentAdmin()['id'];
                        $assignToCampaignData['followup_date'] = (isset($requestData['followup_date']) && $requestData['followup_date']) ? $requestData['followup_date'] : null;
                        $assignToCampaignData['notes'] = (isset($requestData['notes']) && $requestData['notes']) ? $requestData['notes'] : null;
                        $res = $this->assignCampaignToUser($assignToCampaignData, true);

                    }

                }

                $campaignUserCallingDetail->update([
                    'calling_action' => $requestData['action'],
                    'action_taken_by' => getCurrentAdmin()['id'],
                    'followup_date' => (isset($requestData['followup_date']) && $requestData['followup_date']) ? $requestData['followup_date'] : null,
                    'notes' => (isset($requestData['notes']) && $requestData['notes']) ? $requestData['notes'] : null,
                ]);


                if ($requestData['action'] != "move_to_followup") {
                    $userDetail = (array)DB::selectOne("SELECT DATE(last_login_date) as last_login_date FROM users WHERE id=$campaignUserCallingDetail->user_id");
                    $where = "WHERE user_id=$campaignUserCallingDetail->user_id AND (calling_action LIKE '%not_interest%' OR calling_action LIKE '%other%')";
                    if ($userDetail['last_login_date']) {
                        $where .= " AND DATE(created_at) >= '" . $userDetail['last_login_date'] . "'";
                    }
                    $campaignNotInterestAndOtherCount = (array)DB::selectOne("SELECT COUNT(*) as count FROM campaign_users_calling $where");
                    if ($campaignNotInterestAndOtherCount['count'] >= 3) {
                        DB::table('campaign_block_user')->insert([
                            'user_id' => $campaignUserCallingDetail->user_id,
                            'created_at' => date('Y-m-d H:i:s')
                        ]);
                    }
                }
            }
            DB::commit();
            return response()->json(['status' => true]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => false, 'message' => $e->getMessage()]);
        }

    }


    public function getCrmDashboardData($request)
    {


        $whereStr = '';

        if (getCurrentAdmin()->relatedRole->is_super_admin == 0 && getCurrentAdmin()->relatedRole->is_crm_manager == 0) {
            $whereStr .= " AND campaign_users_calling.assign_to = " . getCurrentAdmin()->id;
        }


        $pendingReferralQuery = "SELECT COUNT(CASE WHEN referral_count > 0 THEN 1 END) AS active_followup
      ,COUNT(CASE WHEN referral_count = 0 THEN 1 END) AS passive_followup
FROM (
SELECT count(referral_logs.id) as referral_count FROM `campaign_users_calling`
LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
LEFT JOIN referral_logs ON referral_logs.affiliate_user_id=campaign_users_calling.user_id AND is_active =1 AND DATE(referral_logs.created_at) >= DATE(campaign_users_calling.first_call_date)
where campaign.marketing_goal = 'referral' $whereStr AND campaign_users_calling.calling_action IS NULL AND campaign_users_calling.parent_id IS NOT NULL
GROUP BY campaign_users_calling.campaign_id,campaign_users_calling.user_id ) t1";
        $pendingReferralResult = (array)DB::selectOne($pendingReferralQuery);

        $pendingRevenueQuery = "SELECT COUNT(CASE WHEN payment_count > 0 THEN 1 END) AS active_followup
      ,COUNT(CASE WHEN COALESCE(payment_count,0) = 0 THEN 1 END) AS passive_followup
        FROM (
            SELECT payment.id as payment_count FROM `campaign_users_calling`
            LEFT JOIN (select id,user_id,max(created_at) as payment_date FROM payment_transactions GROUP BY user_id) payment ON campaign_users_calling.user_id = payment.user_id AND DATE(payment.payment_date) >= DATE(campaign_users_calling.first_call_date)
            LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
            where campaign.marketing_goal = 'revenue' $whereStr AND campaign_users_calling.calling_action IS NULL AND campaign_users_calling.parent_id IS NOT NULL GROUP by campaign_users_calling.campaign_id,campaign_users_calling.user_id
        ) t1";


        $pendingRevenueResult = (array)DB::selectOne($pendingRevenueQuery);

        $data['pending_revenue_statistics'] = $pendingRevenueResult;
        $data['pending_refferal_statistics'] = $pendingReferralResult;
        $data['today_followup'] = CampaignUserCallings::whereNotNull('calling_action')->where('assign_to', getCurrentAdmin()->id)->where('is_call_consider', 1)->whereDate('updated_at', Carbon::today())->count();
        return $data;
    }

    public function pendingCallingUserData($request)
    {

        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;

        $where = "WHERE campaign_users_calling.calling_action IS NULL AND campaign_users_calling.parent_id IS NOT NULL";

        if (getCurrentAdmin()->relatedRole->is_super_admin == 0 && getCurrentAdmin()->relatedRole->is_crm_manager == 0) {
            $where .= " AND campaign_users_calling.assign_to = " . getCurrentAdmin()->id;
        }


        if (isset($requestData['userId']) && trim($requestData['userId'])) {
            $userId = trim($requestData['userId']);
            $where .= " AND campaign_users_calling.`assign_to` =" . $userId;
        }


        if (isset($requestData['date_range'])) {
            $data_list = (explode(' - ', $requestData['date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $where .= " AND (campaign_users_calling.followup_date BETWEEN  '" . $startDate . "' AND '" . $endDate . "')";
        }

        if (isset($requestData['search']) && trim($requestData['search'])) {

            $search = trim($requestData['search']);
            $where .= " AND (users.first_name LIKE '%$search%' OR users.email LIKE '%$search%' OR users.mobile_number LIKE '%$search%' OR campaign.marketing_goal LIKE '%$search%' OR campaign.name LIKE '%$search%')";
        }

        $countQuery = "SELECT count(campaign_users_calling.id) as totalRecord FROM `campaign_users_calling`
                      LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
                      LEFT JOIN users ON campaign_users_calling.user_id = users.id
                      $where";
        $countResult = (array)DB::selectOne($countQuery);


        $query = "
          SELECT campaign_users_calling.id,users.first_name,users.own_referral_code,users.email,users.isd_code,users.mobile_number,users.education,users.current_occupation,users.referred_count,users.referral,users.last_login_date,campaign_users_calling.calling_action,campaign_users_calling.followup_date,campaign_users_calling.notes,campaign.name,campaign.marketing_goal,course_info.courses_selected,course_info.number_of_course_enroll,course_info.total_bought_certificate
          FROM `campaign_users_calling`
          LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
          LEFT JOIN users ON campaign_users_calling.user_id = users.id
          LEFT JOIN (select course_user.user_id,GROUP_CONCAT(CONCAT(courses.name,' (',ROUND(course_user.progress),'%)',' - ',IF(IFNULL(course_user.paid_status,0)<1, 'Free', 'Paid'))) as courses_selected,count(course_user.id) as number_of_course_enroll,SUM(course_user.paid_status) as total_bought_certificate from course_user LEFT JOIN courses ON course_user.course_id = courses.id GROUP BY course_user.user_id) course_info ON campaign_users_calling.user_id = course_info.user_id
          $where ORDER BY campaign_users_calling.id LIMIT " . $start . "," . $pageSize;


        $resultSet = DB::select($query);


        $resultSet = collect($resultSet)->map(function ($x) {
            $data = (array)$x;
            if ($data['mobile_number']) {
                $isdCode = 91;
                if ($data['isd_code']) {
                    $isdCode = $data['isd_code'];
                }
                $data['mobile_number'] = '<a target="_blank" href="https://wa.me/' . $isdCode . $data['mobile_number'] . '?text=Hi">' . $data['mobile_number'] . '</a>';
            } else {
                $data['mobile_number'] = '--';
            }


            if (empty($data['first_name'])) {
                $data['first_name'] = '-';
            }

            if (empty($data['email'])) {
                $data['email'] = '-';
            }

            $data['marketing_goal'] = $data['marketing_goal'] ? ucfirst($data['marketing_goal']) : '--';
            $data['move_to_followup'] = '<div><label class="rdiobox"><input id="move_to_followup' . $data['id'] . '" class="actionPerform" type="radio" name="calling_action_' . $data['id'] . '" value="move_to_followup"/><span></span></label></div>';
            $data['other'] = '<div><label class="rdiobox"><input id="other' . $data['id'] . '" class="actionPerform" type="radio" name="calling_action_' . $data['id'] . '" value="other"/><span></span></label></div>';
            $data['not_interest'] = '<div><label class="rdiobox"><input id="not_interest' . $data['id'] . '" class="actionPerform" type="radio" name="calling_action_' . $data['id'] . '" value="not_interest"/><span></span></label></div>';
            $data['name'] = $data['name'] ?? '--';
            $data['followup_date'] = formatDate($data['followup_date'], 'd-m-Y') ?? '--';
            $data['referred_count'] = $data['referred_count'] ?? '0';
            $data['referral'] = $data['referral'] ?? '--';
            $data['own_referral_link'] = $data['own_referral_code'] ? "https://www.learnvern.com/r/" . $data['own_referral_code'] : '--';
            $data['last_login_date'] = $data['last_login_date'] ? formatDate($data['last_login_date'], 'd-m-Y') : '--';
            return $data;
        })->toArray();


        $count_filter = $countResult['totalRecord'];
        $count_total = $countResult['totalRecord'];


        $data_tables = [
            "draw" => $requestData['draw'],
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
            "data" => $resultSet
        ];

        return response()->json($data_tables);

    }

    public function getFollowUpStatisticsReferral($request)
    {

        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;


        $having = '';
        $where = "WHERE campaign.marketing_goal = 'referral' AND campaign_users_calling.calling_action IS NULL AND campaign_users_calling.parent_id IS NOT NULL";

        if (getCurrentAdmin()->relatedRole->is_super_admin == 0 && getCurrentAdmin()->relatedRole->is_crm_manager == 0) {
            $where .= " AND campaign_users_calling.assign_to = " . getCurrentAdmin()->id;
        }


        if (isset($requestData['userId']) && trim($requestData['userId'])) {
            $userId = trim($requestData['userId']);
            $where .= " AND campaign_users_calling.`assign_to` =" . $userId;
        }


        if (isset($requestData['search']) && trim($requestData['search'])) {

            $search = trim($requestData['search']);
            $where .= " AND (users.first_name LIKE '%$search%' OR users.email LIKE '%$search%' OR users.mobile_number LIKE '%$search%' OR campaign.marketing_goal LIKE '%$search%' OR campaign.name LIKE '%$search%' OR admins.name LIKE '%$search%')";
        }


        if (isset($requestData['date_range'])) {
            $data_list = (explode(' - ', $requestData['date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $where .= " AND (campaign_users_calling.followup_date BETWEEN  '" . $startDate . " 00:00:00' AND '" . $endDate . " 23:59:59')";
        }


        if (isset($requestData['filter_date_range'])) {

            $data_list = (explode(' - ', $requestData['filter_date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $where .= " AND (campaign_users_calling.created_at BETWEEN  '" . $startDate . " 00:00:00' AND '" . $endDate . " 23:59:59')";
        }


        if (isset($requestData['type']) && trim($requestData['type'])) {

            $type = trim($requestData['type']);

            if ($type == "active") {
                $where .= " AND referral_logs.id IS NOT NULL";
            }
            if ($type == "passive") {
                $where .= " AND referral_logs.id IS NULL";
            }
        }

        if (isset($requestData['campaign']) && trim($requestData['campaign'])) {
            $campaignId = trim($requestData['campaign']);
            $where .= " AND campaign_users_calling.`campaign_id` =" . $campaignId;
        }

        if (isset($requestData['referral_count']) && trim($requestData['referral_count'])) {
            if ($requestData['referral_count'] == "0-5") {
                $having = "HAVING total_referral_after_campaign < 5";
            }
            if ($requestData['referral_count'] == "5-10") {
                $having = "HAVING (total_referral_after_campaign BETWEEN 5 AND 10)";
            }
            if ($requestData['referral_count'] == "11-25") {
                $having = "HAVING (total_referral_after_campaign BETWEEN 11 AND 25)";
            }
            if ($requestData['referral_count'] == "25+") {
                $having = "HAVING total_referral_after_campaign > 25";
            }

        }


        $orderBy = " ORDER BY total_referral_after_campaign DESC";
        if (isset($requestData['order'][0]['column'])) {
            $orderColumn = ["8" => "course_progress.high_progress", "9" => "course_progress.no_of_course_enroll", "10" => "total_referral_before_campaign", "11" => "total_referral_after_campaign", "12" => "campaign_users_calling.created_at", "13" => "campaign_users_calling.followup_date"];
            $orderBy = " ORDER BY " . $orderColumn[$requestData['order'][0]['column']] . ' ' . $requestData['order'][0]['dir'];
        }


        $referralCountQuery = "SELECT count(*) AS totalRecord FROM (
SELECT campaign_users_calling.id,COUNT(DISTINCT (referral_logs.id)) as total_referral_after_campaign
FROM `campaign_users_calling`
LEFT JOIN admins on campaign_users_calling.assign_to = admins.id
LEFT JOIN users on campaign_users_calling.user_id = users.id
LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
LEFT JOIN referral_logs ON referral_logs.affiliate_user_id=campaign_users_calling.user_id AND referral_logs.is_active =1 AND DATE(referral_logs.created_at) >= DATE(campaign_users_calling.first_call_date)
$where
GROUP BY campaign_users_calling.campaign_id,campaign_users_calling.user_id $having) temp";


        $countResult = (array)DB::selectOne($referralCountQuery);


        $referralQuery = "SELECT campaign_users_calling.*,users.first_name,users.email,users.mobile_number,users.own_referral_code,users.referral,users.last_login_date,users.referred_count,campaign.name as campaign_name,admins.name as assign_to_name,COUNT(DISTINCT (referral_logs.id)) AS total_referral_after_campaign,COUNT(DISTINCT (referral_visitors_tracking.id)) AS total_visit
FROM `campaign_users_calling`
LEFT JOIN admins on campaign_users_calling.assign_to = admins.id
LEFT JOIN users on campaign_users_calling.user_id = users.id
LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
LEFT JOIN referral_logs ON referral_logs.affiliate_user_id=campaign_users_calling.user_id AND referral_logs.is_active =1 AND DATE(referral_logs.created_at) >= DATE(campaign_users_calling.first_call_date)
LEFT JOIN referral_visitors_tracking ON referral_visitors_tracking.user_id=campaign_users_calling.user_id AND DATE(referral_visitors_tracking.tracking_date) >= DATE(campaign_users_calling.first_call_date)
$where
GROUP BY campaign_users_calling.campaign_id,campaign_users_calling.user_id $having $orderBy LIMIT " . $start . "," . $pageSize;


        $resultSet = DB::select($referralQuery);


        $resultSet = collect($resultSet)->map(function ($x) {
            $data = (array)$x;
            if ($data['mobile_number']) {
                $data['mobile_number'] = '<a href="tel:+' . $data['mobile_number'] . '">' . $data['mobile_number'] . '</a>';
            } else {
                $data['mobile_number'] = '--';
            }

            $data['first_name'] = $data['first_name'] ? $data['first_name'] : '--';
            $data['email'] = $data['email'] ? $data['email'] : '-';
            $data['own_referral_code'] = $data['own_referral_code'] ? $data['own_referral_code'] : '-';
            $data['own_referral_link'] = $data['own_referral_code'] ? "https://www.learnvern.com/r/" . $data['own_referral_code'] : '-';
            $data['total_referral_after_campaign'] = $data['total_referral_after_campaign'] ?? 0;
            $data['last_calling_date'] = formatDate($data['created_at'], 'd-m-Y');
            $data['followup_date'] = formatDate($data['followup_date'], 'd-m-Y');
            $data['assign_to_name'] = $data['assign_to_name'] ?? '--';
            $data['referral'] = $data['referral'] ?? '--';
            $data['referred_count'] = $data['referred_count'] ?? '0';
            $data['total_visit'] = $data['total_visit'] ?? '0';
            $data['last_login_date'] = $data['last_login_date'] ? formatDate($data['last_login_date'], 'd-m-Y') : '--';
            $data['0-5'] = '--';
            $data['5-10'] = '--';
            $data['11-25'] = '--';
            $data['25+'] = '--';
            if ($data['total_referral_after_campaign'] > 25) {
                $data['25+'] = $data['total_referral_after_campaign'];
            } elseif ($data['total_referral_after_campaign'] >= 11 && $data['total_referral_after_campaign'] <= 25) {
                $data['11-25'] = $data['total_referral_after_campaign'];
            } elseif ($data['total_referral_after_campaign'] >= 5 && $data['total_referral_after_campaign'] <= 10) {
                $data['5-10'] = $data['total_referral_after_campaign'];
            } elseif ($data['total_referral_after_campaign'] >= 0 && $data['total_referral_after_campaign'] < 5) {
                $data['0-5'] = $data['total_referral_after_campaign'];
            }
            return $data;
        })->toArray();


        $count_filter = $countResult['totalRecord'];
        $count_total = $countResult['totalRecord'];


        $data_tables = [
            "draw" => $requestData['draw'],
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
            "data" => $resultSet
        ];

        return response()->json($data_tables);

    }


    public function getFollowUpStatisticsRevenue($request)
    {

        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;


        $where = "WHERE campaign.marketing_goal = 'revenue' AND campaign_users_calling.calling_action IS NULL AND campaign_users_calling.parent_id IS NOT NULL";

        if (getCurrentAdmin()->relatedRole->is_super_admin == 0 && getCurrentAdmin()->relatedRole->is_crm_manager == 0) {
            $where .= " AND campaign_users_calling.assign_to = " . getCurrentAdmin()->id;
        }

        if (isset($requestData['search']) && trim($requestData['search'])) {

            $search = trim($requestData['search']);
            $where .= " AND (users.first_name LIKE '%$search%' OR users.email LIKE '%$search%' OR users.mobile_number LIKE '%$search%' OR campaign.marketing_goal LIKE '%$search%' OR campaign.name LIKE '%$search%' OR courses.name LIKE '%$search%' OR admins.name LIKE '%$search%')";
        }

        if (isset($requestData['userId']) && trim($requestData['userId'])) {
            $userId = trim($requestData['userId']);
            $where .= " AND campaign_users_calling.`assign_to` =" . $userId;
        }

        if (isset($requestData['date_range'])) {
            $data_list = (explode(' - ', $requestData['date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $where .= " AND (campaign_users_calling.followup_date BETWEEN  '" . $startDate . " 00:00:00' AND '" . $endDate . " 23:59:59')";
        }


        if (isset($requestData['payment_date_range'])) {
            $data_list = (explode(' - ', $requestData['payment_date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $where .= " AND (payment_transactions.transaction_date BETWEEN  '" . $startDate . " 00:00:00' AND '" . $endDate . " 23:59:59')";
        }


        if (isset($requestData['last_date_range'])) {
            $data_list = (explode(' - ', $requestData['last_date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $where .= " AND (campaign_users_calling.created_at BETWEEN  '" . $startDate . " 00:00:00' AND '" . $endDate . " 23:59:59')";
        }


        if (isset($requestData['type']) && trim($requestData['type'])) {
            $type = trim($requestData['type']);
            if ($type == "active") {
                $where .= " AND payment_transactions.`id` IS NOT NULL";
            }
            if ($type == "passive") {
                $where .= " AND payment_transactions.`id` IS NULL";
            }
        }

        if (isset($requestData['payment_status'])) {

            if ($requestData['payment_status'] == 1) {
                $where .= " AND payment_transactions.`id` IS NOT NULL";
            } else {
                $where .= " AND payment_transactions.`id` IS NULL";
            }

        }


        if (isset($requestData['campaign']) && trim($requestData['campaign'])) {
            $campaignId = trim($requestData['campaign']);
            $where .= " AND campaign_users_calling.`campaign_id` =" . $campaignId;
        }

        $orderBy = "";
        if (isset($requestData['order'][0]['column'])) {
            $orderColumn = ["8" => "payment.payment_id", "9" => "payment.payment_date", "10" => "campaign_users_calling.created_at", "11" => "campaign_users_calling.followup_date"];
            $orderBy = " ORDER BY " . $orderColumn[$requestData['order'][0]['column']] . ' ' . $requestData['order'][0]['dir'];
        }

        /*$revenueCountQuery = "SELECT count(*) AS totalRecord FROM (
SELECT campaign_users_calling.id FROM `campaign_users_calling`
LEFT JOIN admins on campaign_users_calling.assign_to = admins.id
LEFT JOIN users on campaign_users_calling.user_id = users.id
LEFT JOIN (select id as payment_id,user_id,course_id,max(created_at) as payment_date FROM payment_transactions GROUP BY user_id order by id desc) payment ON campaign_users_calling.user_id = payment.user_id AND DATE(payment.payment_date) >= DATE(campaign_users_calling.first_call_date)
LEFT JOIN courses on payment.course_id = courses.id
LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
$where
GROUP BY campaign_users_calling.user_id,campaign_users_calling.campaign_id) temp"; // ,*/

        $revenueCountQuery = "SELECT count(*) AS totalRecord
FROM campaign_users_calling
LEFT JOIN admins ON campaign_users_calling.assign_to = admins.id
LEFT JOIN users ON campaign_users_calling.user_id = users.id
LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
LEFT JOIN payment_transactions ON payment_transactions.`user_id` = campaign_users_calling.`user_id`
LEFT JOIN courses ON payment_transactions.course_id = courses.id
$where";
        $countResult = (array)DB::selectOne($revenueCountQuery);


        /*$revenueQuery = "SELECT campaign_users_calling.*,users.first_name,users.email,users.mobile_number,users.own_referral_code,users.referral,users.last_login_date,courses.name as course_name,campaign.name as campaign_name,admins.name as assign_to_name,payment.payment_id as paid_status,payment.payment_date as payment_completed_date
FROM `campaign_users_calling`
LEFT JOIN admins on campaign_users_calling.assign_to = admins.id
LEFT JOIN users on campaign_users_calling.user_id = users.id
LEFT JOIN (select id as payment_id,user_id,course_id,max(created_at) as payment_date FROM payment_transactions GROUP BY user_id order by id desc) payment ON campaign_users_calling.user_id = payment.user_id AND DATE(payment.payment_date) >= DATE(campaign_users_calling.first_call_date)
LEFT JOIN courses on payment.course_id = courses.id
LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
$where
GROUP BY campaign_users_calling.user_id,campaign_users_calling.campaign_id $orderBy LIMIT " . $start . "," . $pageSize; //*/


        $revenueQuery = "SELECT campaign_users_calling.*,users.first_name,users.email,users.mobile_number,users.own_referral_code,users.referral,users.last_login_date,courses.name AS course_name,
campaign.name AS campaign_name,admins.name AS assign_to_name,payment_transactions.id AS paid_status,payment_transactions.created_at AS payment_completed_date
FROM campaign_users_calling
LEFT JOIN admins ON campaign_users_calling.assign_to = admins.id
LEFT JOIN users ON campaign_users_calling.user_id = users.id
LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
LEFT JOIN payment_transactions ON payment_transactions.`user_id` = campaign_users_calling.`user_id`
LEFT JOIN courses ON payment_transactions.course_id = courses.id
$where $orderBy LIMIT " . $start . "," . $pageSize;


        $resultSet = DB::select($revenueQuery);


        $resultSet = collect($resultSet)->map(function ($x) {
            $data = (array)$x;
            if ($data['mobile_number']) {
                $data['mobile_number'] = '<a href="tel:+' . $data['mobile_number'] . '">' . $data['mobile_number'] . '</a>';
            } else {
                $data['mobile_number'] = '--';
            }
            $data['first_name'] = $data['first_name'] ? $data['first_name'] : '--';
            $data['email'] = $data['email'] ? $data['email'] : '-';
            $data['own_referral_code'] = $data['own_referral_code'] ? $data['own_referral_code'] : '-';
            $data['own_referral_link'] = $data['own_referral_code'] ? "https://www.learnvern.com/r/" . $data['own_referral_code'] : '-';
            $data['referral'] = $data['referral'] ?? '--';
            $data['last_login_date'] = $data['last_login_date'] ? formatDate($data['last_login_date'], 'd-m-Y') : '--';
            $data['course_name'] = $data['course_name'] ? $data['course_name'] : '--';
            $data['paid_status'] = $data['paid_status'] ? 'Yes' : 'No';
            $data['last_calling_date'] = formatDate($data['created_at'], 'd-m-Y');
            $data['followup_date'] = formatDate($data['followup_date'], 'd-m-Y');
            $data['assign_to_name'] = $data['assign_to_name'] ?? '--';
            $data['payment_completed_date'] = $data['payment_completed_date'] ? formatDate($data['payment_completed_date'], 'd-m-Y') : '--';
            return $data;
        })->toArray();


        $count_filter = $countResult['totalRecord'];
        $count_total = $countResult['totalRecord'];


        $data_tables = [
            "draw" => $requestData['draw'],
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
            "data" => $resultSet
        ];

        return response()->json($data_tables);

    }


    public function getCrmUserList($request)
    {

        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;


        $where = "WHERE deleted_at IS NULL";

        if (isset($requestData['search']) && trim($requestData['search'])) {

            $search = trim($requestData['search']);
            $where .= " AND admins.name LIKE '%$search%'";
        }

        $orderBy = " ORDER BY name ASC";
        if (isset($requestData['order'][0]['column'])) {
//            $orderColumn = ["8"=>"course_progress.high_progress","9"=>"course_progress.no_of_course_enroll","10"=>"total_referral_before_campaign","11"=>"total_referral_after_campaign","12"=>"campaign_users_calling.created_at","13"=>"campaign_users_calling.followup_date"];
//            $orderBy = " ORDER BY ".$orderColumn[$requestData['order'][0]['column']].' '.$requestData['order'][0]['dir'];
        }

        $countQuery = "SELECT count(*) AS totalRecord FROM (SELECT campaign_users_calling.assign_to FROM `campaign_users_calling` GROUP BY assign_to) t1
                             LEFT JOIN admins ON t1.assign_to = admins.id
                             $where";

        $countResult = (array)DB::selectOne($countQuery);


        $query = "SELECT admins.id,admins.name
FROM (SELECT campaign_users_calling.assign_to FROM `campaign_users_calling` GROUP BY assign_to) t1
LEFT JOIN admins ON t1.assign_to = admins.id
$where $orderBy LIMIT " . $start . "," . $pageSize;


        $resultSet = DB::select($query);


        $resultSet = collect($resultSet)->map(function ($x) {
            $data = (array)$x;
            $data['name'] = $data['name'] ?? '--';
            $data['actions_menu'] = '<a href="' . route('crm.userPerformance', $data['id'] . '?date_range=' . date('d-m-Y') . ' - ' . date('d-m-Y')) . '" class="btn btn-primary btn-xs" title="View Report Data">View Report</a>';
            return $data;
        })->toArray();


        $count_filter = $countResult['totalRecord'];
        $count_total = $countResult['totalRecord'];


        $data_tables = [
            "draw" => $requestData['draw'],
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
            "data" => $resultSet
        ];

        return response()->json($data_tables);

    }

    public function timeRecordArraySetup($timeSlot, $timeStatistics, $data)
    {

        if ($data['calling_action'] == "other") {
            $timeStatistics[$timeSlot]['otherStatus'] += 1;
        } else {
            $timeStatistics[$timeSlot]['totalCalls'] += 1;
        }

        if ($data['parent_id']) {
            if ($data['calling_action'] != "other")
                $timeStatistics[$timeSlot]['followUpCalls'] += 1;
        } else {
            if ($data['calling_action'] != "other")
                $timeStatistics[$timeSlot]['campaignCalls'] += 1;
        }
        $timeStatistics[$timeSlot]['count'] += 1;
        $timeStatistics[$timeSlot]['details'][] = $data;

        return $timeStatistics;

    }

    public function userPerformance($id, $request)
    {
        $requestData = $request->all();
        $date = $requestData['date_range'] ?? '';
        $commonWhere = "WHERE campaign_users_calling.assign_to=$id";
        $referralWhere = "";
        $ambassodorWhere = "";
        $revenueWhere = "";

        $fxArray = [
            "totalCalls" => 0,
            "campaignCalls" => 0,
            "followUpCalls" => 0,
            "otherStatus" => 0,
            "details" => [],
            "count" => 0,
        ];

        $entryExistsOrNot = 0;
        $timeStatistics = ["before_10" => $fxArray, "10-12" => $fxArray, "12-14" => $fxArray, "14-16" => $fxArray, "16-18" => $fxArray, "18-20" => $fxArray, "after_8" => $fxArray];
        if ($date) {
            $data_list = (explode(' - ', $requestData['date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $commonWhere .= " AND (campaign_users_calling.updated_at BETWEEN  '" . $startDate . " 00:00:00' AND '" . $endDate . " 23:59:59')";
            $referralWhere .= " AND (campaign_users_calling.created_at BETWEEN  '" . $startDate . " 00:00:00' AND '" . $endDate . " 23:59:59')";
            $ambassodorWhere .= " AND (user_ambassador_badges.created_at BETWEEN  '" . $startDate . " 00:00:00' AND '" . $endDate . " 23:59:59')";
            $revenueWhere .= " AND (payment_transactions.transaction_date BETWEEN  '" . $startDate . " 00:00:00' AND '" . $endDate . " 23:59:59')";
//            $revenueWhere .= " AND (payment.payment_date BETWEEN  '" . $startDate . " 00:00:00' AND '" . $endDate . " 23:59:59')";

            $daySpecificQuery = "SELECT campaign_users_calling.id,campaign_users_calling.calling_action,campaign_users_calling.updated_at,campaign_users_calling.parent_id,users.first_name,users.mobile_number,users.email,campaign.name,campaign.marketing_goal
                                FROM campaign_users_calling
                                LEFT JOIN users ON campaign_users_calling.user_id = users.id
                                LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
                                $commonWhere AND is_call_consider=1 GROUP BY DATE(campaign_users_calling.updated_at),campaign_users_calling.user_id";
            $daySpecificResult = DB::select($daySpecificQuery);

            if ($daySpecificResult) {
                $entryExistsOrNot = 1;
                foreach ($daySpecificResult as $x) {
                    $data = (array)$x;
                    $updateAt = strtotime($data['updated_at']);
                    if ($updateAt >= strtotime(date('Y-m-d', strtotime($data['updated_at'])) . ' 00:00:00') && $updateAt < strtotime(date('Y-m-d', strtotime($data['updated_at'])) . ' 10:00:00')) {
                        $timeStatistics = $this->timeRecordArraySetup('before_10', $timeStatistics, $data);
                    } else if ($updateAt >= strtotime(date('Y-m-d', strtotime($data['updated_at'])) . ' 10:00:00') && $updateAt < strtotime(date('Y-m-d', strtotime($data['updated_at'])) . ' 12:00:00')) {
                        $timeStatistics = $this->timeRecordArraySetup('10-12', $timeStatistics, $data);
                    } else if ($updateAt >= strtotime(date('Y-m-d', strtotime($data['updated_at'])) . ' 12:00:00') && $updateAt < strtotime(date('Y-m-d', strtotime($data['updated_at'])) . ' 14:00:00')) {
                        $timeStatistics = $this->timeRecordArraySetup('12-14', $timeStatistics, $data);
                    } else if ($updateAt >= strtotime(date('Y-m-d', strtotime($data['updated_at'])) . ' 14:00:00') && $updateAt < strtotime(date('Y-m-d', strtotime($data['updated_at'])) . ' 16:00:00')) {
                        $timeStatistics = $this->timeRecordArraySetup('14-16', $timeStatistics, $data);
                    } else if ($updateAt >= strtotime(date('Y-m-d', strtotime($data['updated_at'])) . ' 16:00:00') && $updateAt < strtotime(date('Y-m-d', strtotime($data['updated_at'])) . ' 18:00:00')) {
                        $timeStatistics = $this->timeRecordArraySetup('16-18', $timeStatistics, $data);
                    } else if ($updateAt >= strtotime(date('Y-m-d', strtotime($data['updated_at'])) . ' 18:00:00') && $updateAt < strtotime(date('Y-m-d', strtotime($data['updated_at'])) . ' 20:00:00')) {
                        $timeStatistics = $this->timeRecordArraySetup('18-20', $timeStatistics, $data);
                    } else if ($updateAt >= strtotime(date('Y-m-d', strtotime($data['updated_at'])) . ' 20:00:00') && $updateAt < strtotime(date('Y-m-d', strtotime($data['updated_at'])) . ' 23:59:59')) {
                        $timeStatistics = $this->timeRecordArraySetup('after_8', $timeStatistics, $data);
                    }
                }
            }
        }

        $callingDayStatusQuery = "SELECT COUNT(CASE WHEN calling_action = 'move_to_followup' THEN 1 END) AS moveToFollowUpCalls,COUNT(CASE WHEN calling_action = 'not_interest' THEN 1 END) AS notInterestedCalls ,COUNT(CASE WHEN calling_action = 'other' THEN 1 END) AS otherCalls FROM campaign_users_calling $commonWhere AND is_call_consider=1";
        $callingDayStatusResult = (array)DB::selectOne($callingDayStatusQuery);


        $freshFollowupQuery = "SELECT count(id) AS freshFollowUpCall FROM campaign_users_calling $commonWhere AND calling_action ='move_to_followup' AND parent_id IS NULL AND is_call_consider=1";
        $freshFollowupResult = (array)DB::selectOne($freshFollowupQuery);

        $pendingFollowupQuery = "SELECT count(id) AS pendingFollowUpCall FROM campaign_users_calling $commonWhere AND calling_action ='move_to_followup' AND parent_id IS NOT NULL";
        $pendingFollowupResult = (array)DB::selectOne($pendingFollowupQuery);


        $totalFollowUpResult['freshFollowUpCall'] = $freshFollowupResult['freshFollowUpCall'];
        $totalFollowUpResult['pendingFollowUpCall'] = $pendingFollowupResult['pendingFollowUpCall'];


        $referralLeadQuery = "SELECT COUNT(CASE WHEN referral_count > 0 THEN 1 END) AS referral_active_followup
FROM (
SELECT count(referral_logs.id) as referral_count FROM `campaign_users_calling`
LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
LEFT JOIN referral_logs ON referral_logs.affiliate_user_id=campaign_users_calling.user_id AND is_active =1 AND DATE(referral_logs.created_at) >= DATE(campaign_users_calling.first_call_date)
WHERE campaign_users_calling.assign_to=$id AND campaign.marketing_goal = 'referral' AND campaign_users_calling.calling_action IS NULL AND campaign_users_calling.parent_id IS NOT NULL
 $referralWhere GROUP BY campaign_users_calling.campaign_id,campaign_users_calling.user_id ) t1";


        $referralLeadQueryResult = (array)DB::selectOne($referralLeadQuery);


        $ambassodorQuery = "SELECT COUNT(CASE WHEN badge_id = 2 THEN 1 END) AS ambassador,COUNT(CASE WHEN badge_id = 3 THEN 1 END) AS ambassador_plus,COUNT(CASE WHEN badge_id = 4 THEN 1 END) AS ambassador_pro
                            FROM  (
                                    SELECT MAX(user_ambassador_badges.ambassador_badge_id) AS badge_id FROM `campaign_users_calling`
                                    LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
                                    LEFT JOIN user_ambassador_badges ON user_ambassador_badges.user_id=campaign_users_calling.user_id AND DATE(user_ambassador_badges.created_at) >= DATE(campaign_users_calling.first_call_date)
                                    WHERE campaign_users_calling.assign_to=$id AND campaign.marketing_goal = 'referral' AND campaign_users_calling.calling_action IS NULL
                                    AND campaign_users_calling.parent_id IS NOT NULL AND user_ambassador_badges.`ambassador_badge_id` IS NOT NULL
                                    $ambassodorWhere
                                    GROUP BY campaign_users_calling.campaign_id,campaign_users_calling.user_id
                                )t1";

        $ambassodorStatResult = (array)DB::selectOne($ambassodorQuery);


        $revenueLeadQuery = "SELECT count(*) AS revenue_active_followup
FROM campaign_users_calling
LEFT JOIN admins ON campaign_users_calling.assign_to = admins.id
LEFT JOIN users ON campaign_users_calling.user_id = users.id
LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
LEFT JOIN payment_transactions ON payment_transactions.`user_id` = campaign_users_calling.`user_id`
LEFT JOIN courses ON payment_transactions.course_id = courses.id
WHERE campaign_users_calling.assign_to=$id AND campaign.marketing_goal = 'revenue' AND campaign_users_calling.calling_action IS NULL AND campaign_users_calling.parent_id IS NOT NULL
$revenueWhere";

        /*$revenueLeadQuery = "SELECT COUNT(CASE WHEN payment_count > 0 THEN 1 END) AS revenue_active_followup
        FROM (
            SELECT payment.id as payment_count FROM `campaign_users_calling`
            LEFT JOIN (select id,user_id,max(created_at) as payment_date FROM payment_transactions GROUP BY user_id) payment ON campaign_users_calling.user_id = payment.user_id AND DATE(payment.payment_date) >= DATE(campaign_users_calling.first_call_date)
            LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
            WHERE campaign_users_calling.assign_to=$id AND campaign.marketing_goal = 'revenue' AND campaign_users_calling.calling_action IS NULL AND campaign_users_calling.parent_id IS NOT NULL
            $revenueWhere GROUP by campaign_users_calling.campaign_id,campaign_users_calling.user_id
        ) t1";*/

        $revenueLeadQueryResult = (array)DB::selectOne($revenueLeadQuery);

        $datePending = date('Y-m-d');
//        $dayPendingNotDoneQuery = "SELECT count(id) AS pendingFollowUpCallOnDay FROM campaign_users_calling WHERE campaign_users_calling.assign_to=$id AND DATE(followup_date)='" . $datePending . "'  AND calling_action IS NULL";
//        $dayPendingNotDoneResult = (array)DB::selectOne($dayPendingNotDoneQuery);



        $campaignPendingCallActiveReferralQuery = "SELECT COUNT(CASE WHEN referral_count > 0 THEN 1 END) AS campaign_pending_call_on_day_for_referral
FROM (
SELECT count(referral_logs.id) as referral_count FROM `campaign_users_calling`
LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
LEFT JOIN referral_logs ON referral_logs.affiliate_user_id=campaign_users_calling.user_id AND is_active =1 AND DATE(referral_logs.created_at) >= DATE(campaign_users_calling.first_call_date)
WHERE campaign_users_calling.assign_to=$id AND campaign.marketing_goal = 'referral' AND DATE(campaign_users_calling.followup_date)='" . $datePending . "' AND campaign_users_calling.calling_action IS NULL
GROUP BY campaign_users_calling.campaign_id,campaign_users_calling.user_id ) t1";

        $campaignPendingCallActiveReferralResult = (array)DB::selectOne($campaignPendingCallActiveReferralQuery);


        $campaignPendingCallActiveRevenueQuery = "SELECT count(*) AS campaign_pending_call_on_day_for_revenue
FROM campaign_users_calling
LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
LEFT JOIN payment_transactions ON payment_transactions.`user_id` = campaign_users_calling.`user_id`
WHERE campaign_users_calling.assign_to=$id AND campaign.marketing_goal = 'revenue' AND DATE(campaign_users_calling.followup_date)='" . $datePending . "' AND campaign_users_calling.calling_action IS NULL AND payment_transactions.id IS NOT NULL";

        $campaignPendingCallActiveRevenueResult = (array)DB::selectOne($campaignPendingCallActiveRevenueQuery);

        $dayPendingNotDoneResult['campaign_pending_call_on_day_for_referral'] = $campaignPendingCallActiveReferralResult['campaign_pending_call_on_day_for_referral'];
        $dayPendingNotDoneResult['campaign_pending_call_on_day_for_revenue'] = $campaignPendingCallActiveRevenueResult['campaign_pending_call_on_day_for_revenue'];



        $dayCampaignPendingQuery = "SELECT count(id) AS campaignPendingCallOnDay FROM campaign_users_calling WHERE campaign_users_calling.assign_to=$id AND DATE(created_at)='" . $datePending . "'  AND calling_action IS NULL AND parent_id IS NULL";
        $dayCampaignPendingResult = (array)DB::selectOne($dayCampaignPendingQuery);

        $userStatisticsResult = ["callingDayStatusResult" => $callingDayStatusResult, "totalFollowUp" => $totalFollowUpResult, "referral_active_followup" => $referralLeadQueryResult['referral_active_followup'], "revenue_active_followup" => $revenueLeadQueryResult['revenue_active_followup'], "timeStatistics" => $timeStatistics, "entryExistsOrNot" => $entryExistsOrNot, "dayPendingNotDoneResult" => $dayPendingNotDoneResult, 'dayCampaignPendingResult' => $dayCampaignPendingResult['campaignPendingCallOnDay'], 'ambassodorStatResult' => $ambassodorStatResult];
        return $userStatisticsResult;
    }


    public function getUserAssociateWithCampaign($request)
    {

        $requestData = $request->all();
        $userId = $requestData['userId'];
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;

        $where = "WHERE campaign_users_calling.user_id = $userId AND (campaign_users_calling.calling_action IS NULL OR campaign_users_calling.calling_action != 'move_to_followup')";

//        if (getCurrentAdmin()->relatedRole->is_super_admin == 0 && getCurrentAdmin()->relatedRole->is_crm_manager == 0) {
//            $where .= " AND campaign_users_calling.assign_to = " . getCurrentAdmin()->id;
//        }

        if (isset($requestData['date_range']) && trim($requestData['date_range'])) {

            $followdateFilter = trim($requestData['date_range']);
            $where .= " AND campaign_users_calling.followup_date='$followdateFilter'";
        }

        if (isset($requestData['search']) && trim($requestData['search'])) {

            $search = trim($requestData['search']);
            $where .= " AND (campaign.marketing_goal LIKE '%$search%' OR campaign.name LIKE '%$search%')";
        }

        $countQuery = "SELECT count(campaign_users_calling.id) as totalRecord FROM `campaign_users_calling`
                      LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
                      $where";
        $countResult = (array)DB::selectOne($countQuery);


        $query = "
          SELECT campaign_users_calling.id,campaign_users_calling.calling_action,campaign_users_calling.followup_date,campaign_users_calling.notes,campaign.name,campaign.marketing_goal,admins.name as assign_to_name,campaign_users_calling.created_at,campaign_users_calling.parent_id,campaign_users_calling.first_call_date
          FROM `campaign_users_calling`
          LEFT JOIN admins ON campaign_users_calling.assign_to = admins.id
          LEFT JOIN campaign ON campaign_users_calling.campaign_id = campaign.id
          $where ORDER BY campaign_users_calling.id LIMIT " . $start . "," . $pageSize;


        $resultSet = DB::select($query);


        $resultSet = collect($resultSet)->map(function ($x) {
            $data = (array)$x;
            $moveToCheckedText = $data['calling_action'] == "move_to_followup" ? "checked" : "";
            $notInterestCheckedText = $data['calling_action'] == "not_interest" ? "checked" : "";
            $otherCheckedText = $data['calling_action'] == "other" ? "checked" : "";

            if ($data['calling_action'] == '' && $data['parent_id']) {
                $moveToCheckedText = "checked";
            }

            $data['marketing_goal'] = $data['marketing_goal'] ? ucfirst($data['marketing_goal']) : '--';
            $data['move_to_followup'] = '<div><label class="rdiobox"><input id="move_to_followup' . $data['id'] . '" class="actionPerform" type="radio" name="calling_action_' . $data['id'] . '" value="move_to_followup" ' . $moveToCheckedText . '/><span></span></label></div>';
            $data['other'] = '<div><label class="rdiobox"><input id="other' . $data['id'] . '" class="actionPerform" type="radio" name="calling_action_' . $data['id'] . '" value="other" ' . $otherCheckedText . '/><span></span></label></div>';
            $data['not_interest'] = '<div><label class="rdiobox"><input id="not_interest' . $data['id'] . '" class="actionPerform" type="radio" name="calling_action_' . $data['id'] . '" value="not_interest" ' . $notInterestCheckedText . ' /><span></span></label></div>';
            $data['name'] = $data['name'] ?? '--';
            $data['followup_date'] = formatDate($data['followup_date'], 'd-m-Y') ?? '--';
            $data['assign_to_name'] = $data['assign_to_name'] ?? '--';
            $data['notes'] = $data['notes'] ?? '--';
            $data['last_calling_date'] = $data['parent_id'] ? formatDate($data['created_at'], 'd-m-Y') : '--';
            $data['first_call_date'] = $data['first_call_date'] ? formatDate($data['first_call_date'], 'd-m-Y') : '--';

            return $data;
        })->toArray();


        $count_filter = $countResult['totalRecord'];
        $count_total = $countResult['totalRecord'];


        $data_tables = [
            "draw" => $requestData['draw'],
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
            "data" => $resultSet
        ];

        return response()->json($data_tables);

    }


    public function assignCampaignToUser($requestData, $isFollowUP)
    {
        try {
            DB::beginTransaction();

            $courseUserDetail = CourseUser::where('id', $requestData['course_user_id'])->first();
            if (empty($courseUserDetail)) {
                return ['status' => false, 'message' => 'No course found.'];
            }

            $campaignAssociate = (array)DB::selectOne("Select * FROM campaign_users_calling WHERE campaign_id=" . $requestData['campaign_id'] . " AND user_id=" . $courseUserDetail->user_id);


            if (count($campaignAssociate) > 0) {

                return ['status' => false, 'message' => 'Already assign this campaign.'];
            }

            $assignAlready = CampaignAssign::where('campaign_id', $requestData['campaign_id'])->where('assign_to', $requestData['assign_to'])->first();

            if ($assignAlready) {
                CampaignAssign::find($assignAlready->id)->increment('number_of_user');
            } else {
                $campaignAssignData = [];
                $campaignAssignData['campaign_id'] = $requestData['campaign_id'];
                $campaignAssignData['assign_to'] = $requestData['assign_to'];
                $campaignAssignData['number_of_user'] = 1;
                CampaignAssign::create($campaignAssignData);
            }

            $campaignUserData = [];
            $campaignUserData['campaign_id'] = $requestData['campaign_id'];
            $campaignUserData['course_user_id'] = $courseUserDetail->id;
            $campaignUserData['user_id'] = $courseUserDetail->user_id;
            $campaignUserData['course_id'] = $courseUserDetail->course_id;
            $campaignUserData['created_at'] = date('Y-m-d H:i:s');
            $campaignUserDetailId = DB::table('campaign_users')->insertGetId($campaignUserData);


            if ($isFollowUP) {

                $campaignUserCallingData = [];
                $campaignUserCallingData['campaign_id'] = $requestData['campaign_id'];
                $campaignUserCallingData['campaign_user_id'] = $campaignUserDetailId;
                $campaignUserCallingData['user_id'] = $courseUserDetail->user_id;
                $campaignUserCallingData['course_id'] = $courseUserDetail->course_id;
                $campaignUserCallingData['calling_action'] = $requestData['action'];
                $campaignUserCallingData['assign_to'] = $requestData['assign_to'];
                $campaignUserCallingData['action_taken_by'] = $requestData['action_taken_by'];
                $campaignUserCallingData['followup_date'] = $requestData['followup_date'];
                $campaignUserCallingData['notes'] = $requestData['notes'];
                $campaignUserCallingData['created_at'] = date('Y-m-d H:i:s');
                $campaignUserCallingData['updated_at'] = date('Y-m-d H:i:s');
                $campaignUserCallingData['is_call_consider'] = 0;
                $campaignUserCallingDetailId = DB::table('campaign_users_calling')->insertGetId($campaignUserCallingData);


                DB::table('campaign_users_calling')->insert([
                    'campaign_id' => $requestData['campaign_id'],
                    'campaign_user_id' => $campaignUserDetailId,
                    'user_id' => $courseUserDetail->user_id,
                    'course_id' => $courseUserDetail->course_id,
                    'parent_id' => $campaignUserCallingDetailId,
                    'assign_to' => $requestData['assign_to'],
                    'followup_date' => $requestData['followup_date'],
                    'notes' => $requestData['notes'],
                    'top_parent_id' => $campaignUserCallingDetailId,
                    'first_call_date' => $campaignUserCallingData['updated_at'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'is_call_consider' => 0
                ]);

            } else {
                $campaignUserCallingData = [];
                $campaignUserCallingData['campaign_id'] = $requestData['campaign_id'];
                $campaignUserCallingData['campaign_user_id'] = $campaignUserDetailId;
                $campaignUserCallingData['user_id'] = $courseUserDetail->user_id;
                $campaignUserCallingData['course_id'] = $courseUserDetail->course_id;
                $campaignUserCallingData['assign_to'] = $requestData['assign_to'];
                $campaignUserCallingData['created_at'] = date('Y-m-d H:i:s');
                $campaignUserCallingDetailId = DB::table('campaign_users_calling')->insertGetId($campaignUserCallingData);

            }
            DB::commit();
            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

}
