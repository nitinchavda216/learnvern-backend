<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:46 PM
 */

namespace App\Repositories;


use App\Interfaces\ConfigurationRepositoryInterface;
use App\Models\Configuration;
class ConfigurationRepository implements ConfigurationRepositoryInterface
{
    public function getdetails()
    {
        return Configuration::first();
    }

    public function update($request)
    {
        $requestData = $request->all();

        $Configuration = Configuration::first();
        if(empty($Configuration)){
            Configuration::create($requestData);
            return true;
        }else{
            $page = self::getdetails();
            $page->update($requestData);
            return true;
        }
    }
}
