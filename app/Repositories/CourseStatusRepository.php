<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26-02-2021
 * Time: 09:25 AM
 */

namespace App\Repositories;

use App\Exports\UsersExport;
use App\Interfaces\CourseStatusRepositoryInterface;
use App\Models\Course;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class CourseStatusRepository implements CourseStatusRepositoryInterface
{
    public function getAjaxListData($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $partner_id = isset($requestData['partner_user_id']) ? $requestData['partner_user_id'] : session('partner_id');

        $where = "WHERE course_user.partner_id = $partner_id AND courses.course_type !=4";
        if (isset($requestData['date_range'])) {
            $data_list = (explode(' - ', $requestData['date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $where .= " AND (course_user.course_enroll_date BETWEEN  '" . $startDate . " 00:00:00' AND '" . $endDate . " 23:59:59')";
        }

        if (isset($requestData['course_id'])) {
            $where .= " AND course_user.course_id=".$requestData['course_id'];
        }

        $orderBy = " ORDER BY total_user_count DESC";

        $courseCountQuery = "SELECT COUNT(*) as totalRecord FROM 
                             (
                                 SELECT courses.id FROM course_user
                                 LEFT JOIN courses ON course_user.course_id = courses.id
                                 $where GROUP BY course_user.course_id
                             ) temp";


        $countResult = (array)DB::selectOne($courseCountQuery);


        $courseQuery = "SELECT courses.id,courses.name as course_name,count(course_user.id) as total_user_count,COUNT(CASE WHEN ROUND(progress)<=20 THEN 1 END) as twenty,COUNT(CASE WHEN ROUND(progress)>=21 AND ROUND(progress)<=40 THEN 1 END) as fourty,COUNT(CASE WHEN ROUND(progress)>=41 AND ROUND(progress)<=60 THEN 1 END) as sixty,COUNT(CASE WHEN ROUND(progress)>=61 AND ROUND(progress)<=80 THEN 1 END) as eighty,COUNT(CASE WHEN ROUND(progress)>=81 AND ROUND(progress)<=99 THEN 1 END) as ninty,COUNT(CASE WHEN ROUND(progress)>=100 THEN 1 END) as hundred,SUM(paid_status) as total_sell  FROM course_user
                             LEFT JOIN courses ON course_user.course_id = courses.id
                             $where GROUP BY course_user.course_id $orderBy LIMIT $start,$pageSize";

        $resultSet = DB::select($courseQuery);

        $resultSet = collect($resultSet)->map(function ($x) use ($partner_id,$requestData) {

            $data = (array)$x;
            $data['0-20'] = $data['twenty'] ?? 0;
            $data['21-40'] = $data['fourty'] ?? 0;
            $data['41-60'] = $data['sixty'] ?? 0;
            $data['61-80'] = $data['eighty'] ?? 0;
            $data['81-99'] = $data['ninty'] ?? 0;
            $data['completed'] = $data['hundred'] ?? 0;
            $data['total_sell'] = $data['total_sell'] ?? 0;
            $data['total_user_count'] = $data['total_user_count'] ?? 0;
            $url = route('course-status-report.getCertificatePurchaseUsers',['partner_user_id'=>$partner_id,'enroll_date_range'=> $requestData['date_range'] ?? '','course_id'=>$data['id']]);
            if($data['total_sell'] > 0)
            {
                $data['total_sell'] = '<a href='.$url.' class="btn btn-primary btn-xs" target="_blank"><i class="fas fa-eye"></i> View Sell ('.$data['total_sell'].')</a>';
            }
            return $data;
        })->toArray();

        $data_tables = [
            "draw" => $requestData['draw'],
            "recordsTotal" => $countResult['totalRecord'],
            "recordsFiltered" => $countResult['totalRecord'],
            "data" => $resultSet
        ];
        return response()->json($data_tables);
    }

    public function exportListData($request)
    {
        $requestData = $request->all();
        $partner_id = isset($requestData['partner_user_id']) ? $requestData['partner_user_id'] : session('partner_id');
        $where = "WHERE course_user.partner_id = $partner_id AND courses.course_type !=4";
        if (isset($requestData['date_range'])) {
            $data_list = (explode(' - ', $requestData['date_range']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $where .= " AND (course_user.course_enroll_date BETWEEN  '" . $startDate . " 00:00:00' AND '" . $endDate . " 23:59:59')";
        }

        if (isset($requestData['course_id'])) {
            $where .= " AND course_user.course_id=".$requestData['course_id'];
        }

        $orderBy = " ORDER BY total_user_count DESC";
        $courseQuery = "SELECT courses.id,courses.name as course_name,count(course_user.id) as total_user_count,COUNT(CASE WHEN ROUND(progress)<=20 THEN 1 END) as twenty,COUNT(CASE WHEN ROUND(progress)>=21 AND ROUND(progress)<=40 THEN 1 END) as fourty,COUNT(CASE WHEN ROUND(progress)>=41 AND ROUND(progress)<=60 THEN 1 END) as sixty,COUNT(CASE WHEN ROUND(progress)>=61 AND ROUND(progress)<=80 THEN 1 END) as eighty,COUNT(CASE WHEN ROUND(progress)>=81 AND ROUND(progress)<=99 THEN 1 END) as ninty,COUNT(CASE WHEN ROUND(progress)>=100 THEN 1 END) as hundred,SUM(paid_status) as total_sell  FROM course_user
                             LEFT JOIN courses ON course_user.course_id = courses.id
                             $where GROUP BY course_user.course_id $orderBy";

        $resultSet = DB::select($courseQuery);
        $alldata = [];
        foreach ($resultSet as $item) {
            $alldata[] = [
                'Course Name'=>$item->course_name,
                '0-20%' => $item->twenty,
                '21-40%' => $item->fourty,
                '41-60%' => $item->sixty,
                '61-80%' => $item->eighty,
                '81-99%' => $item->ninty,
                'Completed 100%' => $item->hundred,
                'Total User Count' => $item->total_user_count,
                'Certificate Purchased Count' => $item->total_sell,
            ];
        }
        $list = array_keys($alldata[0]);
        $data = new UsersExport($alldata, $list);
        return ['status' => true, 'data' => $data];
    }
}
