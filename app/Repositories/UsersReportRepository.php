<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 03:02 PM
 */

namespace App\Repositories;

use App\Exports\ReferralReportExport;
use App\Exports\UserReportExport;
use App\Interfaces\UsersReportRepositoryInterface;
use App\Models\User;
use App\Models\CourseUser;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Yajra\DataTables\Facades\DataTables;
use DB;

class UsersReportRepository implements UsersReportRepositoryInterface
{
    public function getAjaxListData($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = User::whereNotNull('id');

        if (isset($requestData['partner_user_id'])) {
            $dataInput = $dataInput->where('partner_id', $requestData['partner_user_id']);
        } else {
            $dataInput = $dataInput->where('partner_id', session('partner_id'));
        }
        if (isset($requestData['register_date'])) {
            $data_list = (explode(' - ', $requestData['register_date']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
        }
        if (isset($requestData['last_login_date'])) {
            $data_list = (explode(' - ', $requestData['last_login_date']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereBetween('last_login_date', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
        }
        if (isset($requestData['searchData'])) {
            $searchTerm = $requestData['searchData'];
            $dataInput = $dataInput->where(function ($q) use ($searchTerm) {
                $q->where('first_name', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('username', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('email', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('mobile_number', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('isd_code', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('current_occupation', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('referral', 'LIKE', "%{$searchTerm}%");
            });
        }
        if (isset($requestData['sign_up'])) {
            $dataInput = $dataInput->where('signup_from', $requestData['sign_up']);
        }
        if ($requestData['user_type'] == '2') {
            $dataInput = $dataInput->where('country', '!=', 'India')->where('country', '!=', null); //
        } else if ($requestData['user_type'] == '1') {
            $dataInput = $dataInput->where('country', 'India');
        }

        if (isset($requestData['is_active']) && $requestData['is_active'] != "") {
            $dataInput = $dataInput->where('is_active', $requestData['is_active']);
        }

        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->skip($start)->take($pageSize);
        if (!isset($requestData['order'])) {
            $dataInput = $dataInput->orderBy('id', "DESC");
        }
        $dataInput = $dataInput->skip($start)->take($pageSize);

        $data_tables = DataTables::of($dataInput);

        $data_tables->EditColumn('email', function ($user) {
            $first_name = $user->first_name ?? null;
            $email = $user->email ?? "- - -";
            $mobile_number = isset($user->mobile_number) ? $user->mobile_number : null;
            $isd_code = isset($user->isd_code) ? getIsdCode($user->isd_code) . ' ' : "";
            $str = "<i class=\"fas fa-user\"></i>" . "&nbsp;&nbsp;&nbsp;" . $first_name . "<br>";
            $str .= "<i class=\"far fa-envelope\"></i>" . "&nbsp;&nbsp;" . $email . "<br>";
            if (isset($mobile_number)) {
                $str .= "<i class=\"fa fa-phone-square\" aria-hidden=\"true\"></i>" . "&nbsp;&nbsp;&nbsp;" . $isd_code . $mobile_number;
            }
            if (isset($user->current_occupation)) {
                $str .= '<br>' . $user->current_occupation;
            }
            return $str;
        });

        $data_tables->EditColumn('current_occupation', function ($user) {
            return $user->current_occupation ?? '- - -';
        });
        $data_tables->EditColumn('sign_up_from', function ($user) {
            return isset($user->signup_from) ? signUpFormArray($user->signup_from) : "- - -";
        });

        $data_tables->EditColumn('is_active', function ($user) {
            return formatBooleanActiveStatus($user->is_active);
        });
        $data_tables->EditColumn('referral', function ($user) {
            return isset($user->referral) ? $user->referral : "- - -";
        });
        $data_tables->EditColumn('registration_date', function ($user) {
            return isset($user->created_at) ? formatDate($user->created_at, "d-m-Y") : "- - -";
        });
        $data_tables->EditColumn('last_login_date', function ($user) {
            return isset($user->last_login_date) ? formatDate($user->last_login_date) : "- - -";
        });
        $data_tables->EditColumn('enrolled_courses', function ($user) {
            return $user->courses->count();
        });
        $data_tables->EditColumn('completed_courses', function ($user) {
            return $user->courses->where('user_status_id', 4)->count();
        });
        $data_tables->EditColumn('total_refer', function ($user) {
            return $user->referred_count;
        });
        $data_tables->EditColumn('paid_certificate', function ($user) {
            return $user->courses->where('paid_status', 1)->count();
        });
        $data_tables->EditColumn('free_certificate', function ($user) {
            return $user->courses->where('paid_status' < 0)->count();
        });
        $data_tables->EditColumn('course', function ($user) {
            $str = [];
            foreach ($user->courses as $value) {
                $enroll_date = isset($value->course_enroll_date) ? formatDate($value->course_enroll_date) : '-';
                $last_date = isset($value->last_used_date) ? formatDate($value->last_used_date) : '-';
                $str[] = '<br><b>Course Name :</b> ' . $value->courseDetail->name . '&nbsp;&nbsp;<b>Course Enrolled Date :</b>&nbsp;&nbsp;' . $enroll_date . ' <b>&nbsp;&nbsp;Progress :</b>&nbsp;&nbsp;' . $value->progress . '%&nbsp;&nbsp;<b> Last Course View Date :</b>&nbsp;&nbsp;' . $last_date;
            }
            if (empty($str)) {
                $str = 'This user have not any enrolled courses.';
            } else {
                $str = implode("<br>", $str);
            }
            return $str;
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);

        $data_tables->rawColumns(['action', 'sign_up_from', 'email', 'course', 'is_active']);
        return $data_tables->make(true);
    }

    public function getReferralReportAjaxListData($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = User::where('referral', '!=', "");
        if (isset($requestData['partner_user_id'])) {
            $dataInput = $dataInput->where('partner_id', $requestData['partner_user_id']);
        } else {
            $dataInput = $dataInput->where('partner_id', session('partner_id'));
        }
        if (isset($requestData['register_date'])) {
            $data_list = (explode(' - ', $requestData['register_date']));
            $startDate = date('Y-m-d', strtotime($data_list[0])) . " 00:00:01";
            $endDate = date('Y-m-d', strtotime($data_list[1])) . " 23:59:59";
            $dataInput = $dataInput->whereBetween('created_at', [$startDate, $endDate]);
        }
        if (isset($requestData['signup_from'])) {
            $dataInput = $dataInput->where('signup_from', $requestData['signup_from']);
        }
        if (isset($requestData['users_type'])) {
            $dataInput = $dataInput->where('is_active', $requestData['users_type']);
        }
        if (isset($requestData['search'])) {
            $dataInput = $dataInput->where(function ($q) use ($requestData) {
                $q->where('email', 'Like', '%' . $requestData['search'] . '%')
                    ->orWhere('first_name', 'Like', '%' . $requestData['search'] . '%')
                    ->orWhere('referral', 'Like', '%' . $requestData['search'] . '%');
            })->orWhereHas('affiliate', function ($q) use ($requestData) {
                $q->where('email', 'Like', '%' . $requestData['search'] . '%');
            });
        }
        $count_total = $count_filter = $dataInput->count();

        $dataInput = $dataInput->skip($start)->take($pageSize);
        $dataInput = $dataInput->orderBy('id', "DESC")->skip($start)->take($pageSize);
        $data_tables = DataTables::of($dataInput);

        $data_tables->EditColumn('user_details', function ($user) {
            $first_name = $user->first_name ?? null;
            $email = $user->email ?? "- - -";
            $mobile_number = isset($user->mobile_number) ? $user->mobile_number : null;
            $isd_code = isset($user->isd_code) ? getIsdCode($user->isd_code) . ' ' : "";
            $register_date = isset($user->created_at) ? formatDate($user->created_at) : "";
            $str = "<i class=\"fas fa-user\"></i>" . "&nbsp;&nbsp;&nbsp;" . $first_name . "<br>";
            $str .= "<i class=\"far fa-envelope\"></i>" . "&nbsp;&nbsp;" . $email . "<br>";
            if (isset($mobile_number)) {
                $str .= "<i class=\"fa fa-phone-square\" aria-hidden=\"true\"></i>" . "&nbsp;&nbsp;&nbsp;" . $isd_code . $mobile_number . "<br>";
            }
            $str .= "<i class=\"fas fa-calendar\"></i>" . "&nbsp;&nbsp;&nbsp;" . $register_date . "<br>";

            return $str;
        });
        $data_tables->EditColumn('current_occupation', function ($user) {
            return isset($user->current_occupation) && ($user->current_occupation != "") ? $user->current_occupation : "- - -";
        });
        $data_tables->EditColumn('sign_up_from', function ($user) {
            return '<span class="btn btn-light btn-rounded">' . signUpFormArray(isset($user->signup_from) ? $user->signup_from : "- - -") . '</span>';

        });
        $data_tables->EditColumn('affiliate', function ($user) {
            return isset($user->affiliate) ? $user->affiliate->email : "- - -";
        });
        $data_tables->EditColumn('referral', function ($user) {
            return isset($user->referral) && ($user->referral != "") ? $user->referral : "- - -";
        });

        $data_tables->EditColumn('last_login_date', function ($user) {
            return isset($user->last_login_date) ? formatDate($user->last_login_date) : "- - -";
        });

        $data_tables->EditColumn('email_verify', function ($user) {
            if ($user->is_active == 1) {
                $verifyicon = "<i class=\"fa fa-check-circle\" aria-hidden=\"true\"
                                                                   style=\"color: green;\"> Active</i>";
            } else {
                $verifyicon = "<i class=\"fas fa-times-circle\" aria-hidden=\"true\"
                                                                   style=\"color: red;\"> Inactive</i>";
            }
            return $verifyicon;
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['user_details', 'sign_up_from', 'email_verify', 'action']);
        return $data_tables->make(true);
    }

    public function exportReferralReportData($request)
    {
        $requestData = $request->all();
        $dataInput = User::where('referral', '!=', "");
        if (isset($requestData['partner_user_id'])) {
            $dataInput = $dataInput->where('partner_id', $requestData['partner_user_id']);
        } else {
            $dataInput = $dataInput->where('partner_id', session('partner_id'));
        }
        if (isset($requestData['register_date'])) {
            $data_list = (explode(' - ', $requestData['register_date']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
        }
        if (isset($requestData['signup_from'])) {
            $dataInput = $dataInput->where('signup_from', $requestData['signup_from']);
        }
        if (isset($requestData['search'])) {
            $dataInput = $dataInput->where('email', 'Like', '%' . $requestData['search'] . '%')
                ->orWhere('first_name', 'Like', '%' . $requestData['first_name'] . '%')
                ->orWhere('referral', 'Like', '%' . $requestData['first_name'] . '%');
        }
        if (isset($requestData['usertype'])) {

            $dataInput = $dataInput->where('is_active', $requestData['usertype']);


        }
        $dataInput = $dataInput->orderBy('id', "DESC")->get();
        if (count($dataInput) == 0) {
            return ['status' => false];
        }
        $alldata = [];
        foreach ($dataInput as $data) {
            $created_at = isset($data->created_at) ? formatDate($data->created_at, 'Y-m-d') . " 00:00:00" : null;
            $last_login_date = isset($data->last_login_date) ? formatDate($data->last_login_date, 'Y-m-d') . " 00:00:00" : null;
            $alldata[] = [
                'User Email' => isset($data->email) ? $data->email : "",
                'Full Name' => $data->first_name,
                'Mobile No' => isset($data->mobile_number) ? $data->mobile_number : "",
                'Current Occupation' => isset($data->current_occupation) ? $data->current_occupation : "",
                'Affiliate Referral' => $data->referral,
                'Affiliate User' => isset($data->affiliate) ? $data->affiliate->email : "",
                'Sign up From' => isset($data->signup_from) ? User::SIGNUP_FROM[$data->signup_from] : "",
                'Register Date' => isset($created_at) ? Date::dateTimeToExcel(Carbon::parse($created_at)) : "",
                'Last Login Date' => isset($last_login_date) ? Date::dateTimeToExcel(Carbon::parse($last_login_date)) : "",
                'Email Verify' => ($data->is_active == 1) ? "Verified" : "Not Verified",
            ];
        }
        $list = array_keys($alldata[0]);
        $data = new ReferralReportExport($alldata, $list);
        return ['status' => true, 'data' => $data];
    }


    public function getAjaxExportData($request)
    {
        $requestData = $request->all();
        $dataInput = User::whereNotNull('id');
        if (isset($requestData['partner_user_id'])) {
            $dataInput = $dataInput->where('partner_id', $requestData['partner_user_id']);
        } else {
            $dataInput = $dataInput->where('partner_id', session('partner_id'));
        }
        if (isset($requestData['register_date'])) {
            $data_list = (explode(' - ', $requestData['register_date']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
        }
        if (isset($requestData['last_login_date'])) {
            $data_list = (explode(' - ', $requestData['last_login_date']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereBetween('last_login_date', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
        }
        if (isset($requestData['searchData'])) {
            $searchTerm = $requestData['searchData'];
            $dataInput = $dataInput->where(function ($q) use ($searchTerm) {
                $q->where('first_name', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('username', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('email', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('mobile_number', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('isd_code', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('signup_from', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('current_occupation', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('referral', 'LIKE', "%{$searchTerm}%");
            });
        }
        if (isset($requestData['sign_up'])) {
            $dataInput = $dataInput->where('signup_from', $requestData['sign_up']);
        }
        if ($requestData['user_type'] == '1') {
            $dataInput = $dataInput->where('country', '!=', 'India')->where('country', '!=', null);
        } else if ($requestData['user_type'] == '0') {
            $dataInput = $dataInput->where('country', 'India');
        }
        $dataInput = $dataInput->orderBy('id', "DESC")->get();
        if (count($dataInput) == 0) {
            return ['status' => false];
        }
        $alldata = [];
        $i = 0;
        foreach ($dataInput as $data) {
            $created_at = isset($data->created_at) ? formatDate($data->created_at, 'Y-m-d') . ' 00:00:00' : null;
            $last_login_date = isset($data->last_login_date) ? formatDate($data->last_login_date, 'Y-m-d') . ' 00:00:00' : null;
            $courseuser = CourseUser::where('user_id', $data->id)->latest()->first();
            $enroll_date = isset($courseuser->course_enroll_date) ? formatDate($courseuser->course_enroll_date) : '-';
            $last_date = isset($courseuser->last_used_date) ? formatDate($courseuser->last_used_date) : '-';
            $alldata[] = [
                'Email' => isset($data->email) ? $data->email : "",
                'Full Name' => $data->first_name,
                'Mobile Number' => isset($data->mobile_number) ? $data->mobile_number : "",
                'Current Occupation' => isset($data->current_occupation) ? $data->current_occupation : "",
                'Sign_up_from' => isset($data->signup_from) ? User::SIGNUP_FROM[$data->signup_from] : "",
                'User Status' => isset($data->is_active) && $data->is_active ==1 ? "Active" : "Inactive",
                'Referral Code' => $data->own_referral_code ?? "",
                'Affiliate Code' => isset($data->referral) ? $data->referral : "",
                'Registration Date' => isset($created_at) ? Date::dateTimeToExcel(Carbon::parse($created_at)) : "",
                'Last Login Date' => isset($last_login_date) ? Date::dateTimeToExcel(Carbon::parse($last_login_date)) : "",
                'Enrolled Courses' => $data->courses->count(),
                'Completed Courses' => $data->courses->where('user_status_id', 4)->count(),
                'Total Refer' => $data->where('referral', $data->own_referral_code)->count(),
                'Paid Certificate' => $data->courses->where('paid_status', 1)->count(),
                'Free Certificate' => $data->courses->where('paid_status' < 0)->count(),
                'Last Viewed Course Name' => $courseuser->courseDetail->name ?? '',
                'Course Enrolled Date' => isset($enroll_date) ? $enroll_date : '-',
                'Course Progress' => isset($courseuser) ? $courseuser->progress . '%' : '-',
                'Last Course View Date' => isset($last_date) ? $last_date : '-',
            ];
            $i++;
        }
        $list = array_keys($alldata[0]);
        $data = new UserReportExport($alldata, $list);
        return ['status' => true, 'data' => $data];
    }

    public function getUnUnrolledUsersAjaxList($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = User::doesntHave('courses');
        // dd($requestData);
        if (isset($requestData['partner_user_id'])) {
            $dataInput = $dataInput->where('partner_id', $requestData['partner_user_id']);
        } else {
            $dataInput = $dataInput->where('partner_id', session('partner_id'));
        }
        if (isset($requestData['register_date'])) {
            $data_list = (explode(' - ', $requestData['register_date']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
        }
        if (isset($requestData['searchData'])) {
            $searchTerm = $requestData['searchData'];
            $dataInput = $dataInput->where(function ($q) use ($searchTerm) {
                $q->where('first_name', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('email', 'LIKE', "%{$searchTerm}%");
            });
        }
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->skip($start)->take($pageSize);
        $dataInput = $dataInput->orderBy('id', "DESC")->skip($start)->take($pageSize);
        $data_tables = DataTables::of($dataInput);


        $data_tables->EditColumn('user_details', function ($user) {
            $first_name = $user->first_name ?? null;
            $email = $user->email ?? "- - -";
            $mobile_number = isset($user->mobile_number) ? $user->mobile_number : null;
            $isd_code = isset($user->isd_code) ? getIsdCode($user->isd_code) . ' ' : "";
            $str = "<i class=\"fas fa-user\"></i>" . "&nbsp;&nbsp;&nbsp;" . $first_name . "<br>";
            $str .= "<i class=\"far fa-envelope\"></i>" . "&nbsp;&nbsp;" . $email . "<br>";
            if (isset($mobile_number)) {
                $str .= "<i class=\"fa fa-phone-square\" aria-hidden=\"true\"></i>" . "&nbsp;&nbsp;&nbsp;" . $isd_code . $mobile_number . "<br>";
            }


            return $str;
        });

        $data_tables->EditColumn('current_occupation', function ($user) {
            return $user->current_occupation ?? "- - -";
        });
        $data_tables->EditColumn('sign_up_from', function ($user) {
            return isset($user->signup_from) ? User::SIGNUP_FROM[$user->signup_from] : "- - -";
        });
        $data_tables->EditColumn('referral', function ($user) {
            return $user->referral ?? "- - -";
        });
        $data_tables->EditColumn('registration_date', function ($user) {
            return isset($user->created_at) ? formatDate($user->created_at) : "- - -";
        });
        $data_tables->EditColumn('last_login_date', function ($user) {
            return isset($user->last_login_date) ? formatDate($user->last_login_date) : "- - -";
        });
        $data_tables->EditColumn('email_verify', function ($user) {
            if ($user->is_active == 1) {
                $verifyicon = "<i class=\"fa fa-check-circle\" aria-hidden=\"true\"
                                                                   style=\"color: green;\"> Active</i>";
            } else {
                $verifyicon = "<i class=\"fas fa-times-circle\" aria-hidden=\"true\"
                                                                   style=\"color: red;\"> Inactive</i>";
            }
            return $verifyicon;
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['user_details', 'sign_up_from', 'email_verify']);
        return $data_tables->make(true);
    }

    public function exportUnEnrolledUsersList($request)
    {
        $requestData = $request->all();
        $dataInput = User::doesntHave('courses');
        if (isset($requestData['partner_user_id'])) {
            $dataInput = $dataInput->where('partner_id', $requestData['partner_user_id']);
        } else {
            $dataInput = $dataInput->where('partner_id', session('partner_id'));
        }
        if (isset($requestData['register_date'])) {
            $data_list = (explode(' - ', $requestData['register_date']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
        }
        if (isset($requestData['searchData'])) {
            $searchTerm = $requestData['searchData'];
            $dataInput = $dataInput->where(function ($q) use ($searchTerm) {
                $q->where('first_name', 'LIKE', "%{$searchTerm}%")
                    ->orWhere('email', 'LIKE', "%{$searchTerm}%");
            });
        }
        $dataInput = $dataInput->orderBy('id', "DESC")->get();
        if (count($dataInput) == 0) {
            return ['status' => false];
        }
        $alldata = [];
        $i = 0;
        foreach ($dataInput as $data) {
            $created_at = isset($data->created_at) ? formatDate($data->created_at, 'Y-m-d') . " 00:00:00" : null;
            $last_login_date = isset($data->last_login_date) ? formatDate($data->last_login_date, 'Y-m-d') . " 00:00:00" : null;
            $alldata[] = [
                'Email' => $data->email ?? "",
                'Full Name' => $data->first_name ?? "",
                'Mobile Number' => isset($data->mobile_number) ? $data->mobile_number : "",
                'Current Occupation' => $data->current_occupation ?? "",
                'Signup From' => isset($data->signup_from) ? User::SIGNUP_FROM[$data->signup_from] : "",
                'Affiliate Code' => $data->referral ?? "",
                'Register Date' => isset($created_at) ? Date::dateTimeToExcel(Carbon::parse($created_at)) : "",
                'Last Login Date' => isset($last_login_date) ? Date::dateTimeToExcel(Carbon::parse($last_login_date)) : "",
                'Email Verify' => ($data->is_active == 1) ? "Verified" : "Not Verified",
            ];
            $i++;
        }
        $list = array_keys($alldata[0]);
        $data = new UserReportExport($alldata, $list);
        return ['status' => true, 'data' => $data];
    }

    public function getMonthlyData($request)
    {
        $requestData = $request->all();
        if (isset($requestData['year'])) {
            $year = $requestData['year'];
        } else {
            $year = date('Y');
        }
        $userData = User::where('partner_id', session('partner_id'))->selectRaw('year(created_at) as year, MONTH(created_at) month, count(id) total')
            ->whereyear('created_at', $year)
            ->groupBy('year', 'month')
            ->orderBy('month')
            ->get()->toArray();
        $referralData = User::where('partner_id', session('partner_id'))->selectRaw('year(created_at) as year, MONTH(created_at) month, count(id) total')
            ->where('referral', '!=', "")
            ->whereyear('created_at', $year)
            ->groupBy('year', 'month')
            ->orderBy('month')
            ->get()->toArray();
        $month_names = getMonthList();
        $months = $counts = $referrals = [];
        foreach ($userData as $data) {
            $months[] = $month_names[$data['month']];
            $counts[] = $data['total'];
            $referral_total = 0;
            foreach ($referralData as $referralDatum) {
                if ($referralDatum['month'] == $data['month']) {
                    $referral_total = $referralDatum['total'];
                }
            }
            $referrals[] = $referral_total;
        }
        $returnData['months'] = json_encode($months);
        $returnData['counts'] = json_encode($counts);
        $returnData['referral_users'] = json_encode($referrals);
        return $returnData;

    }

}
