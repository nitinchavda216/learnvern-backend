<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:14 PM
 */

namespace App\Repositories;

use App\Events\UpdateCourseSlugMailEvent;
use App\Events\UpdateCourseTimeEvent;
use App\Exports\CourseReportExport;
use App\Interfaces\CoursesRepositoryInterface;
use App\Interfaces\UnitRepositoryInterface;
use App\Models\Admins;
use App\Models\Course;
use App\Models\CourseBadge;
use App\Models\CourseLanguage;
use App\Models\CourseSkills;
use App\Models\CourseUser;
use App\Models\Curriculum;
use App\Models\Language;
use App\Models\PartnerCourses;
use App\Models\Section;
use App\Models\Sluglists;
use App\Models\Units;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Spatie\SchemaOrg\Schema;
use Spatie\SchemaOrg\VideoObject;
use Yajra\DataTables\Facades\DataTables;
use App\Events\SendCourseMailEvent;
use App\Models\CourseKeywords;

class CoursesRepository implements CoursesRepositoryInterface
{
    protected $unitRepository;

    public function __construct(UnitRepositoryInterface $unitRepository)
    {
        $this->unitRepository = $unitRepository;
    }

    public function getCourseList($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = Course::with(['partnerCourses' => function ($q) {
            $q->where('partner_id', session('partner_id'));
        }])->select('id', 'category_id', 'name', 'is_active', 'slug', 'time', 'price', 'is_career_plan_course')->where('course_type', "!=", 4);
        if (session('partner_id') != 1) {
            $dataInput = $dataInput->whereHas('partnerCourses', function ($q) {
                $q->where('partner_id', session('partner_id'));
            });
        }
        if (isset($requestData['category_id'])) {
            $dataInput = $dataInput->where('category_id', $requestData['category_id']);
        }
        if (isset($requestData['search'])) {
            $dataInput = $dataInput->where('name', 'LIKE', '%' . $requestData['search'] . '%');
        }
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id')->skip($start)->take($pageSize);

        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('category_name', function ($item) {
            return $item->categoryDetail->name ?? "- - -";
        });
        $data_tables->EditColumn('statistics', function ($item) {
            return view('courses.partials.statistics_col', compact('item'))->render();
        });
        $data_tables->EditColumn('default_testimonial', function ($item) {
            return view('courses.partials.default_testimonial_col', compact('item'))->render();
        });
        $data_tables->EditColumn('actions', function ($item) {
            return view('courses.partials.actions_col', compact('item'))->render();
        });
        $data_tables->EditColumn('status', function ($item) {
            if (session('partner_id') == 1) {
                return view('courses.partials.status_col', compact('item'))->render();
            } else {
                if ($item->is_active == 1) {
                    $statusIcon = "<i class=\"fa fa-check-circle\" aria-hidden=\"true\"
                                                                   style=\"color: green;\"> Active</i>";
                } else {
                    $statusIcon = "<i class=\"fas fa-times-circle\" aria-hidden=\"true\"
                                                                   style=\"color: red;\"> Inactive</i>";
                }
                return $statusIcon;
            }
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['statistics', 'default_testimonial', 'actions', 'status']);
        return $data_tables->make(true);
    }

    public function getLatestCourses($count)
    {
        return Course::where('course_type', '!=', 4)->orderBy('id', 'DESC')->take($count)->get();
    }

    public function getCourseTitles()
    {
        $courses = Course::select(DB::Raw('CONCAT(name," - ",IF(is_active =1,"(Active)","(Inactive)")) as title'), 'id')->orderBy('is_active', 'DESC')->get();
        return $courses->pluck('title', 'id')->toArray();
    }

    public function getCourseBadges()
    {
        $badges = CourseBadge::pluck('title', 'id')->toArray();
        return $badges;
    }

    public function getCourseSkills($id)
    {
        return CourseSkills::where('course_id', $id)->get();
    }

    public function getActiveCoursesListExceptCareerPlanCourses()
    {
        return Course::where('is_career_plan_course', 0)->where('course_type', '!=', 4)->where('is_active', 1)->pluck('name', 'id')->toArray();
    }

    public function getAllActiveCourseTitles()
    {
        return Course::byActive()->pluck('name', 'id')->toArray();
    }

    public function getCourseDetails($id)
    {
        return Course::findOrFail($id);
    }

    public function storeCourse($request): array
    {
        DB::beginTransaction();
        try {
            $requestData = $request->except('_token');
            if (isset($requestData['intro_video'])) {
                $requestData['intro_video_embed'] = getVimeoVideoEmbedUrl($requestData['intro_video']);
            }
            if (isset($requestData['intro_youtube_video'])) {
                $requestData['intro_youtube_video_id'] = getYouTubeIdFromURL($requestData['intro_youtube_video']);
                $requestData['intro_youtube_video_embed'] = "https://www.youtube.com/embed/" . $requestData['intro_youtube_video_id'];
            }
            $requestData['is_free'] = isset($requestData['is_free']) ? 1 : 0;
            $requestData['is_career_plan_course'] = isset($requestData['is_career_plan_course']) ? 1 : 0;
            $requestData['unit_youtube_video_flag'] = isset($requestData['unit_youtube_video_flag']) ? 1 : 0;
            if (isset($requestData['course_icon'])) {
                $requestData['course_icon'] = self::uploadIconImage($requestData['course_icon']);
            }
            if (isset($requestData['image'])) {
                $requestData['image'] = self::uploadImage($requestData['image']);
            }
            if (isset($requestData['app_image'])) {
                $requestData['app_image'] = self::uploadAppImage($requestData['app_image']);
            }
            if (isset($requestData['app_icon'])) {
                $requestData['app_icon'] = self::uploadAppIcon($requestData['app_icon']);
            }
            $requestData['pre_course'] = isset($requestData['pre_course']) ? implode(',', $requestData['pre_course']) : null;
            $requestData['related_course'] = isset($requestData['related_course']) ? implode(',', $requestData['related_course']) : null;
            $requestData['num_of_student'] = 1;
            $requestData['time'] = "00:00:00";
            $requestData['canonical_url'] = $requestData['canonical_url'] ?? env('FRONT_URL') . 'course/' . $requestData['slug'];
            $course = Course::create($requestData);
            self::upsertSearchKeywords($course->id, $course->name, []);
            event(new SendCourseMailEvent($course));
            self::storeSchemaScript($course->id);
            PartnerCourses::create([
                'partner_id' => 1,
                'course_id' => $course->id,
                'is_paid_course' => 0,
                'course_fees' => $course->price,
            ]);
            $admins = Admins::whereHas('partnerDetail', function ($q) {
                $q->where('allow_all_future_courses', 1);
            })->where('partner_id', '!=', 1)->where('status', 1)->get();
            foreach ($admins as $admin) {
                PartnerCourses::create([
                    'partner_id' => $admin->partner_id,
                    'course_id' => $course->id,
                    'is_paid_course' => 0,
                    'course_fees' => $course->price,
                ]);
            }
            foreach ($requestData['course_skills'] as $course_skill) {
                if (isset($course_skill)) {
                    CourseSkills::create([
                        'course_id' => $course->id,
                        'title' => $course_skill,
                    ]);
                }
            }
            if (env('LEARNVERN_MAUTICS_ENABLE')) {
                Artisan::call("create:new-custom-field-in-mautic", ['--course_id' => $course->id]);
            }
            DB::commit();
            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function getSearchKeywords($course_id)
    {
        $data = CourseKeywords::where('course_id', (int)$course_id)->first();
        $keywords = (isset($data->keywords) && !empty($data->keywords)) ? json_decode($data->keywords, true) : [];
        return implode(',', $keywords);
    }

    public function upsertSearchKeywords($course_id, $course_name, $keywords = [])
    {
        $data = [
            'course_id' => $course_id,
            'course_name' => $course_name,
            'unit_id' => null,
            'keywords' => json_encode($keywords),
        ];
        $course = CourseKeywords::where('course_id', (int)$course_id)->update($data, ['upsert' => true]);
        return true;
    }

    public function storeLanguage($id, $requestData)
    {
        foreach ($requestData as $language) {
            $data = Language::Where('name', $language)->first();
            CourseLanguage::create([
                'course_id' => $id,
                'language_id' => $data['id'],
            ]);
        }
        return true;
    }

    public function updateCourse($request, $id): bool
    {
        $requestData = $request->all();
        $course = self::getCourseDetails($id);
        if (isset($course)) {
            if (isset($requestData['intro_video'])) {
                $requestData['intro_video_embed'] = getVimeoVideoEmbedUrl($requestData['intro_video']);
            } else {
                $requestData['intro_video'] = $requestData['intro_video_embed'] = null;
            }
            if (isset($requestData['intro_youtube_video'])) {
                $requestData['intro_youtube_video_id'] = getYouTubeIdFromURL($requestData['intro_youtube_video']);
                $requestData['intro_youtube_video_embed'] = "https://www.youtube.com/embed/" . $requestData['intro_youtube_video_id'];
            } else {
                $requestData['intro_youtube_video'] = $requestData['intro_youtube_video_id'] = $requestData['intro_youtube_video_embed'] = null;
            }
            $requestData['is_free'] = isset($requestData['is_free']) ? 1 : 0;
            $requestData['is_career_plan_course'] = isset($requestData['is_career_plan_course']) ? 1 : 0;
            $requestData['unit_youtube_video_flag'] = isset($requestData['unit_youtube_video_flag']) ? 1 : 0;
            if (isset($requestData['course_icon'])) {
                $requestData['course_icon'] = self::uploadIconImage($requestData['course_icon'], $course);
            }
            if (isset($requestData['image'])) {
                $requestData['image'] = self::uploadImage($requestData['image'], $course);
            }
            if (isset($requestData['app_image'])) {
                $requestData['app_image'] = self::uploadAppImage($requestData['app_image'], $course);
            }
            if (isset($requestData['app_icon'])) {
                $requestData['app_icon'] = self::uploadAppIcon($requestData['app_icon'], $course);
            }
            unset($requestData['slug']);
            $requestData['pre_course'] = isset($requestData['pre_course']) ? implode(',', $requestData['pre_course']) : null;
            $requestData['related_course'] = isset($requestData['related_course']) ? implode(',', $requestData['related_course']) : null;
            $course->update($requestData);
            self::upsertSearchKeywords($course->id, $course->name, []);
            CourseSkills::where('course_id', $id)->delete();
            foreach ($requestData['course_skills'] as $course_skill) {
                if (isset($course_skill)) {
                    CourseSkills::create([
                        'course_id' => $id,
                        'title' => $course_skill,
                    ]);
                }
            }
        }
        $admin = getCurrentAdmin();
        PartnerCourses::where('partner_id', $admin->partner_id)->where('course_id', $course->id)->update(['course_fees' => $course->price]);
        self::storeSchemaScript($id);
        return true;
    }

    public function updateCourseSlug($request): array
    {
        DB::beginTransaction();
        try{
            $requestData = $request->all();
            $slugExist = Sluglists::where(['slug' => $requestData['slug'], 'curriculum_type' => 'course'])->first();
            if (isset($slugExist)) {
                DB::rollBack();
                return ['status' => false, 'message' => "Slug Already Exists! Please try another slug"];
            }
            $course = self::getCourseDetails($requestData['course_id']);
            $emailData = [
                'COURSE_NAME' => $course->name,
                'OLD_SLUG_URL' => env('FRONT_URL').'course/'.$course->slug,
                'NEW_SLUG_URL' => env('FRONT_URL').'course/'.$requestData['slug'],
            ];
            $course->update([
                'slug' => $requestData['slug'],
                'canonical_url' => env('FRONT_URL') . 'course/' . $requestData['slug']
            ]);
            Sluglists::create([
                'course_id' => $course->id, 'curriculum_id' => $course->id, 'slug' => $requestData['slug'], 'curriculum_type' => 'course'
            ]);
            self::storeSchemaScript($requestData['course_id']);
            $courseUnits = Units::select('id', 'slug', 'canonical_url')->where('course_id', $course->id)->get();
            foreach ($courseUnits as $courseUnit) {
                $courseUnit->update(['canonical_url' => env('FRONT_URL') . $requestData['slug'] . '/' . $courseUnit->slug]);
                $this->unitRepository->storeSchemaScript($courseUnit->id);
            }
            event(new UpdateCourseSlugMailEvent($emailData));
            DB::commit();
            return ['status' => true, 'message' => "Slug Updated Successfully!", 'slug' => $requestData['slug']];
        }catch (\Exception $e){
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function updateCoursePrice($request): bool
    {
        $requestData = $request->all();
        PartnerCourses::where('partner_id', $requestData['partner_id'])->where('course_id', $requestData['course_id'])->update(['course_fees' => $requestData['price']]);
        return true;
    }

    public function storeSchemaScript($course_id)
    {
        $course = Course::find($course_id);
        $webPageSchema = Schema::webPage()
            ->name($course->name)
            ->url(env('FRONT_URL') . 'course/' . $course->slug)
            ->speakable(Schema::speakableSpecification()->xpath([
                "/html/head/title",
                "/html/head/meta[@name='description']/@content"
            ]))->toScript();

        /*COURSE SCHEMA*/
        $productSchema = Schema::course()
            ->name($course->name)
            ->description($course->brief_description ?? $course->name)
            ->image($course->course_icon)
            ->url(env('FRONT_URL') . 'course/' . $course->slug)
            ->provider(Schema::organization()->name("LearnVern")->url(env('FRONT_URL')));
        if ((isset($course->comment_count) && ($course->comment_count > 0))) {
            $productSchema = $productSchema->aggregateRating(Schema::aggregateRating()
                ->ratingValue($course->average_rating)
                ->reviewCount($course->comment_count)
            );
        }
        $reviews = $course->relatedComments->where('is_default_testimonial', 1)->take(16);
        if (count($reviews) > 0) {
            foreach ($reviews as $review) {
                $reviewData[] = Schema::review()
                    ->reviewRating(Schema::rating()->ratingValue($review->rating))
                    ->reviewBody($review->content)
                    ->author(Schema::person()->name($review->author));
            }
            $productSchema = $productSchema->review($reviewData);
        }
        $productSchema = $productSchema->toScript();
        /*COURSE SCHEMA END*/

        /*FAQS SCHEMA*/
        $faqsSchema = "";
        if (count($course->relatedFaqs) > 0) {
            $faqsSchema = Schema::fAQPage();
            foreach ($course->relatedFaqs as $faq) {
                $faqMinEntityData[] = Schema::question()->name($faq['question'])->acceptedAnswer(Schema::answer()->text($faq['answer']));
            }
            if (!empty($faqMinEntityData)) {
                $faqsSchema->mainEntity($faqMinEntityData);
            }
            $faqsSchema = $faqsSchema->toScript();
        }
        /*FAQS SCHEMA END*/

        /*VIDEO OBJECT SCHEMA*/
        $videoObjectSchema = Schema::videoObject()
            ->name($course->name)
            ->description($course->brief_description ?? $course->name)
            ->thumbnailUrl($course->course_icon)
            ->uploadDate(isset($course->created_at) ? formatDate($course->created_at, 'Y-m-d h:i:s') : formatDate($course->updated_at, 'Y-m-d h:i:s'))
            ->if(isset($course->intro_video), function (VideoObject $schema) use ($course) {
                $schema->contentUrl(env('FRONT_URL') . 'course/' . $course->slug)
                    ->embedUrl($course->intro_video_embed);
            })->if(isset($course->intro_youtube_video), function (VideoObject $schema) use ($course) {
                $schema->contentUrl(env('FRONT_URL') . 'course/' . $course->slug)
                    ->embedUrl($course->intro_youtube_video_embed);
            })->toScript();
        /*VIDEO OBJECT SCHEMA END*/

        /*BREADCRUMBS SCHEMA*/
        $breadcrumbsSchema = Schema::breadcrumbList()
            ->itemListElement([
                Schema::listItem()->position(1)->item(Schema::thing()->identifier(env('FRONT_URL'))->name('Home')),
                Schema::listItem()->position(2)->item(Schema::thing()->identifier(env('FRONT_URL') . 'course/' . $course->slug)->name($course->name))
            ])->toScript();
        $schemaScript = $webPageSchema . '
        ' . $productSchema . '
        ' . $faqsSchema . '
        ' . $videoObjectSchema . '
        ' . $breadcrumbsSchema;
        $course->update(['schema_script' => $schemaScript]);
    }

    public function uploadIconImage($file, $course = NULL): string
    {
        $uploadPath = storage_path(Course::ICON_IMG_PATH);
        if (isset($course)) {
            $imagePath = $uploadPath . $course->course_icon;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function uploadImage($file, $course = NULL): string
    {
        $uploadPath = storage_path(Course::IMG_PATH);
        if (isset($course)) {
            $imagePath = $uploadPath . $course->image;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function uploadAppImage($file, $course = NULL): string
    {
        $uploadPath = storage_path(Course::APP_IMG_PATH);
        if (isset($course)) {
            $imagePath = $uploadPath . $course->app_image;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function uploadAppIcon($file, $course = NULL): string
    {
        $uploadPath = storage_path(Course::APP_ICON_PATH);
        if (isset($course)) {
            $imagePath = $uploadPath . $course->app_icon;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function updateActiveStatus($request): bool
    {
        $course = Course::find($request->id);
        $course->is_active = $request->is_active;
        $course->save();
        return true;
    }

    public function getCurriculumData($course_id)
    {
        return Curriculum::with(['getSectionChildData' => function ($q) use ($course_id) {
            $q->where('course_id', $course_id);
        }])->whereHas('getSectionChildData', function ($q) use ($course_id) {
            $q->where('course_id', $course_id);
        })->where('course_id', $course_id)->where('type', 'section')->where('is_active', 1)->orderBy('sort_order')->get();
    }

    public function getCurriculumDetail($id)
    {
        return Curriculum::where('id', $id)->first();
    }

    public function getCurrentCurriculumRelatedCourses($course_id, $id, $type)
    {
        return Curriculum::join('courses', 'courses.id', '=', 'curriculum.course_id')
            ->where('course_id', '!=', $course_id)->where('curriculum_list_id', $id)
            ->where('type', $type)->groupBy('courses.id')->pluck('courses.name')->toArray();
    }

    public function getCareerPlanCurriculumData($course_id)
    {
        return Curriculum::with(['getMasterSectionChildData.getSectionChildData' => function ($q) use ($course_id) {
            $q->where('course_id', $course_id);
        }])->where('course_id', $course_id)->where('type', 'section')->where('is_active', 1)->where('parent_section_id', null)->get();
    }

    public function storeCareerPlanCurriculum($request): bool
    {
        $requestData = $request->except('_token');
        if (strpos($requestData['section_id'], 'newTag-') !== false) {
            $section = Section::create([
                'course_id' => $requestData['course_id'],
                'name' => str_replace(['newTag-'], [''], $requestData['section_id'])
            ]);
            $section_id = $section->id;
            Curriculum::create([
                'course_id' => $requestData['course_id'],
                'section_id' => $section_id,
                'curriculum_list_id' => $section_id,
                'name' => $section->name,
                'type' => 'section',
                'is_active' => 1,
            ]);
        } else {
            $section_id = $requestData['section_id'];
        }
        $section_sort_order = 1;
        foreach ($requestData['curriculum_ids'] as $section_curriculum_id => $curriculum_ids) {
            $sectionCurriculum = Curriculum::find($section_curriculum_id);
            $sectionExist = Curriculum::where(['course_id' => $requestData['course_id'],
                'section_id' => $sectionCurriculum->section_id,
                'parent_section_id' => $section_id, 'type' => $sectionCurriculum->type])->first();
            if (is_null($sectionExist)) {
                Curriculum::create([
                    'course_id' => $requestData['course_id'],
                    'section_id' => $sectionCurriculum->section_id,
                    'parent_section_id' => $section_id,
                    'curriculum_list_id' => $sectionCurriculum->curriculum_list_id,
                    'name' => $sectionCurriculum->name,
                    'type' => $sectionCurriculum->type,
                    'is_active' => $sectionCurriculum->is_active,
                    'sort_order' => $section_sort_order,
                ]);
            }
            $curriculum_sort_order = 1;
            foreach ($curriculum_ids as $curriculum_id) {
                $curriculum = Curriculum::find($curriculum_id);
                Curriculum::create([
                    'course_id' => $requestData['course_id'],
                    'section_id' => $curriculum->section_id,
                    'curriculum_list_id' => $curriculum->curriculum_list_id,
                    'parent_section_id' => $section_id,
                    'name' => $curriculum->name,
                    'type' => $curriculum->type,
                    'is_active' => $curriculum->is_active,
                    'sort_order' => $curriculum_sort_order,
                ]);
                $curriculum_sort_order++;
            }
            $section_sort_order++;
        }
        event(new UpdateCourseTimeEvent(['course_id' => [$requestData['course_id']]]));
        return true;
    }

    public function updateCurriculum($request): array
    {
        try {
            $requestData = $request->all();
            foreach ($requestData['sortSectionArray'] as $sortSectionOrder => $sectionId) {
                if (isset($sectionId)) {
                    Curriculum::where('id', $sectionId)->update(['sort_order' => $sortSectionOrder]);
                    foreach ($requestData['sortUnitsArray'][$sectionId] as $sortUnitOrder => $unitId) {
                        Curriculum::where('id', $unitId)->update(['sort_order' => $sortUnitOrder]);
                    }
                }
            }
            return ['status' => true, 'message' => "Sort Order Updated Successfully!"];
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function deleteCurriculum($request): array
    {
        try {
            $requestData = $request->all();
            $curriculum = Curriculum::where('id', $requestData['id'])->first();
            if ($curriculum->type == 'section') {
                Curriculum::where('course_id', $requestData['course_id'])->where('section_id', $curriculum->section_id)->forceDelete();
            }
            $curriculum->forceDelete();
            event(new UpdateCourseTimeEvent(['course_id' => [$requestData['course_id']]]));
            return ['status' => true, 'message' => "Curriculum Deleted Successfully!"];
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function editMasterSection($request): array
    {
        try {
            $requestData = $request->except('_token');
            $curriculum = Curriculum::where('id', $requestData['curriculum_id'])->first();
            $curriculum->update(['name' => $requestData['name']]);
            Section::where('id', $curriculum['curriculum_list_id'])->update($requestData);
            return ['status' => true, 'message' => "Master Section Updated Successfully!"];
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function updateSortOrder($request): array
    {
        try {
            $requestData = $request->all();
            foreach ($requestData['ids'] as $sortOrder => $id) {
                $curriculum = Course::where('id', $id)->first();
                if (isset($curriculum)) {
                    $curriculum->update(['sort_order' => $sortOrder]);
                }
            }
            return ['status' => true, 'message' => "Sort Order Updated Successfully!"];
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function updateHideFromBackendStatus($request): bool
    {
        $curriculum = Curriculum::where('id', $request->id)->first();
        $curriculum->hide_from_backend = $request->hide_from_backend;
        $curriculum->save();
        return true;
    }

    public function getCourseAjaxListData($request)
    {
        $requestData = $request->all();
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = CourseUser::whereNotNull('id')->whereHas('courseDetail')->whereHas('userDetail');
        if (isset($requestData['partner_user_id'])) {
            $dataInput = $dataInput->where('partner_id', $requestData['partner_user_id']);
        } else {
            $dataInput = $dataInput->where('partner_id', session('partner_id'));
        }
        if (isset($requestData['register_date'])) {
            $data_list = (explode(' - ', $requestData['register_date']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereHas('userDetail', function ($q) use ($startDate, $endDate) {
                $q->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            });
        }
        if (isset($requestData['search'])) {
            $searchTerm = $requestData['search'];
            $dataInput = $dataInput->where(function ($qq) use ($searchTerm) {
                $qq->whereHas('userDetail', function ($q) use ($searchTerm) {
                    $q->where('first_name', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('username', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('email', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('candidate_id', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('isd_code', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('mobile_number', 'LIKE', "%{$searchTerm}%");
                })->orWhereHas('userStatus', function ($q) use ($searchTerm) {
                    $q->where('name', 'LIKE', "%{$searchTerm}%");
                });
            });
        }
        if (isset($requestData['course_id'])) {
            $dataInput = $dataInput->where('course_id', $requestData['course_id']);
        }
        if (isset($requestData['course_status'])) {
            if ($requestData['course_status'] == 100) {
                $dataInput = $dataInput->where('progress', 100);
            } else {
                $data = (explode('-', $requestData['course_status']));
                $dataInput = $dataInput->whereBetween('progress', $data);
            }
        }
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->skip($start)->take($pageSize);
        $dataInput = $dataInput->orderBy('id', "DESC")->skip($start)->take($pageSize);
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('course_name', function ($user) {
            return $user->courseDetail->name ?? "- - -";
        });
        $data_tables->EditColumn('user_details', function ($user) {
            $first_name = $user->userDetail->first_name ?? null;
            $email = $user->userDetail->email ?? "- - -";
            $mobile_number = isset($user->userDetail->mobile_number) ? $user->userDetail->mobile_number : null;
            $isd_code = isset($user->userDetail->isd_code) ? getIsdCode($user->userDetail->isd_code) . ' ' : "";
            $str = "<i class=\"fas fa-user\"></i>" . "&nbsp;&nbsp;&nbsp;" . $first_name . "<br>";
            $str .= "<i class=\"far fa-envelope\"></i>" . "&nbsp;&nbsp;" . $email . "<br>";
            if (isset($mobile_number)) {
                $str .= "<i class=\"fa fa-phone-square\" aria-hidden=\"true\"></i>" . "&nbsp;&nbsp;&nbsp;" . $isd_code . $mobile_number;
            }
            return $str;
        });
        $data_tables->EditColumn('course_status', function ($user) {
            return isset($user->progress) ? $user->progress . "%" : "- - -";
        });
        $data_tables->EditColumn('certificate_type', function ($user) {
            return ($user->courseDetail->course_type == 4) ? "LearnVern" : (($user->is_international_certificate == 1) ? 'International' : 'NSDC');
        });
        $data_tables->EditColumn('certificate_date', function ($user) {
            return isset($user->certificate_date) ? formatDate($user->certificate_date) : "- - -";
        });
        $data_tables->EditColumn('last_used_date', function ($user) {
            return isset($user->last_used_date) ? formatDate($user->last_used_date) : "- - -";
        });
        $data_tables->EditColumn('course_enroll_date', function ($user) {
            return isset($user->created_at) ? formatDate($user->created_at) : "- - -";
        });
        $data_tables->EditColumn('actions', function ($courseUser) {
            return view('reports.partials.course_report_actions', compact('courseUser'))->render();
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['user_details', 'actions']);
        return $data_tables->make(true);
    }

    public function getAjaxExportData($request)
    {
        $requestData = $request->all();
        $dataInput = CourseUser::whereNotNull('id')->whereHas('courseDetail')->whereHas('userDetail');
        if (isset($requestData['partner_user_id'])) {
            $dataInput = $dataInput->where('partner_id', $requestData['partner_user_id']);
        } else {
            $dataInput = $dataInput->where('partner_id', session('partner_id'));
        }
        if (isset($requestData['register_date'])) {
            $data_list = (explode(' - ', $requestData['register_date']));
            $startDate = date('Y-m-d', strtotime($data_list[0]));
            $endDate = date('Y-m-d', strtotime($data_list[1]));
            $dataInput = $dataInput->whereHas('userDetail', function ($q) use ($startDate, $endDate) {
                $q->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            });
        }
        if (isset($requestData['search'])) {
            $searchTerm = $requestData['search'];
            $dataInput = $dataInput->where(function ($qq) use ($searchTerm) {
                $qq->whereHas('userDetail', function ($q) use ($searchTerm) {
                    $q->where('first_name', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('username', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('email', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('candidate_id', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('isd_code', 'LIKE', "%{$searchTerm}%")
                        ->orWhere('mobile_number', 'LIKE', "%{$searchTerm}%");
                })->orWhereHas('userStatus', function ($q) use ($searchTerm) {
                    $q->where('name', 'LIKE', "%{$searchTerm}%");
                });
            });
        }
        if (isset($requestData['course_id'])) {
            $dataInput = $dataInput->where('course_id', $requestData['course_id']);
        }
        if (isset($requestData['course_status'])) {
            if ($requestData['course_status'] == 100) {
                $dataInput = $dataInput->where('progress', 100);
            } else {
                $data = (explode('-', $requestData['course_status']));
                $dataInput = $dataInput->whereBetween('progress', $data);
            }
        }
        $dataInput = $dataInput->orderBy('id', "DESC")->get();
        if (count($dataInput) == 0) {
            return ['status' => false];
        }
        $alldata = [];
        $i = 0;
        foreach ($dataInput as $data) {
            $created_at = isset($data->userDetail->created_at) ? formatDate($data->userDetail->created_at, 'Y-m-d') . " 00:00:00" : null;
            $last_login_date = isset($data->userDetail->last_login_date) ? formatDate($data->userDetail->last_login_date, 'Y-m-d') . " 00:00:00" : null;
            $course_enroll_date = isset($data->course_enroll_date) ? formatDate($data->course_enroll_date, 'Y-m-d') . " 00:00:00" : null;
            $alldata[] = [
                'User Email' => $data->userDetail->email ?? "",
                'User Name' => $data->userDetail->first_name ?? "",
                'Course Name' => $data->courseDetail->name ?? "",
                'Course Progress' => $data->progress,
                'Mobile No' => isset($data->userDetail->mobile_number) ? $data->userDetail->mobile_number : "",
                'Own Referral Code' => $data->userDetail->own_referral_code ?? "",
                'Referral Link' => env('FRONT_URL') . 'r/' . $data->userDetail->own_referral_code,
                'Course Referral Link' => env('FRONT_URL') . 'course/' . $data->courseDetail->slug . '?ref=' . $data->userDetail->own_referral_code,
                'Sign up Referral' => $data->userDetail->referral ?? "",
                'Register Date' => isset($created_at) ? Date::dateTimeToExcel(Carbon::parse($created_at)) : "",
                'Last Login Date' => isset($last_login_date) ? Date::dateTimeToExcel(Carbon::parse($last_login_date)) : "",
                'Current Occupation' => $data->userDetail->current_occupation ?? "",
                'Sign up From' => isset($data->userDetail->signup_from) ? User::SIGNUP_FROM[$data->userDetail->signup_from] : "",
                'Course Enroll Date' => isset($created_at) ? Date::dateTimeToExcel(Carbon::parse($course_enroll_date)) : "",
//                'Email Verify' => ($data->userDetail->is_active == 1) ? "Verified" : "Not Verified",
            ];
            $i++;
        }
        $list = array_keys($alldata[0]);
        $data = new CourseReportExport($alldata, $list);
        return ['status' => true, 'data' => $data];
    }

    public function convertInternationalToNSDC($request)
    {
        $requestData = $request->all();
        $courseUser = CourseUser::find($requestData['id']);
        if (isset($courseUser)) {
            $update_candidate_id = false;
            if (strpos($courseUser->userDetail->candidate_id, 'CIN_') !== false) {
                $update_candidate_id = true;
                User::where('id', $courseUser->user_id)->update(['candidate_id' => null]);
            }
            $courseUser->is_international_certificate = 0;
            if ($update_candidate_id) {
                $courseUser->certificate_id = null;
            }
            $courseUser->save();
        }
        return true;
    }
}
