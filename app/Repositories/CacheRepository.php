<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 03:04 PM
 */

namespace App\Repositories;

use App\Interfaces\CacheRepositoryInterface;
use Illuminate\Contracts\Cache\Repository as Cache;

class CacheRepository implements CacheRepositoryInterface
{
    protected $cache;

    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    public function flushAllCache()
    {
        $this->cache->flush();
        return true;
    }

    public function removeCacheByTag($tags)
    {
        $this->cache->tags($tags)->flush();
        return true;
    }
}
