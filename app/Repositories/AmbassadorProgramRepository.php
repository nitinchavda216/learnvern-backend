<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:37 PM
 */

namespace App\Repositories;

use App\Interfaces\AmbassadorProgramRepositoryInterface;
use App\Models\AmbassadorProgramSettings;

class AmbassadorProgramRepository implements AmbassadorProgramRepositoryInterface
{
    public function getAllSettings()
    {
        return AmbassadorProgramSettings::get();
    }

    public function storeSettings($request)
    {
        $requestData = $request->except('_token');
        if (isset($requestData['image'])) {
            $requestData['image'] = self::uploadImage($requestData['image']);
        }
        AmbassadorProgramSettings::create($requestData);
        return true;
    }

    public function uploadImage($file, $program_setting = NULL)
    {
        $uploadPath = storage_path(AmbassadorProgramSettings::IMG_PATH);
        if (isset($program_setting)) {
            $imagePath = $uploadPath . $program_setting->image;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function getSettingDetails($id)
    {
        return AmbassadorProgramSettings::findOrFail($id);
    }

    public function updateSettings($request, $id)
    {
        $program_setting = self::getSettingDetails($id);
        $requestData = $request->except('_token');
        if (isset($requestData['image'])) {
            $requestData['image'] = self::uploadImage($requestData['image']);
        } elseif (!isset($requestData['image']) && ($requestData['remove-image'] == "true") && isset($program_setting->image)) {
            $uploadPath = storage_path(AmbassadorProgramSettings::IMG_PATH);
            $imagePath = $uploadPath . $program_setting->image;
            @unlink($imagePath);
            $requestData['image'] = null;
        }
        $program_setting->update($requestData);
        return true;
    }

    public function updateActiveStatus($request)
    {
        $program_setting = AmbassadorProgramSettings::find($request->id);
        $program_setting->is_active = $request->is_active;
        $program_setting->save();
        return true;
    }

    public function getAmbassadorLevelTitles()
    {
        return AmbassadorProgramSettings::where('refer_from', 'other')->pluck('title', 'id')->toArray();
    }
}
