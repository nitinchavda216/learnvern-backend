<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09-02-2021
 * Time: 02:28 PM
 */

namespace App\Repositories;

use App\Events\UpdateCourseTimeEvent;
use App\Events\UpdateUnitSlugMailEvent;
use App\Interfaces\AssignmentRepositoryInterface;
use App\Models\Assignment;
use App\Models\Course;
use App\Models\Curriculum;
use App\Models\Section;
use App\Models\Sluglists;
use App\Models\CourseAttachment;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as Writer;
use Spatie\SchemaOrg\Schema;
use Spatie\SchemaOrg\VideoObject;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class AssignmentRepository implements AssignmentRepositoryInterface
{
    public function getAjaxListData($request)
    {
        $requestData = $request->all();
        // dd($requestData);
        $pageSize = ($request->length) ? $request->length : 50;
        $start = ($request->start) ? $request->start : 0;
        $dataInput = Curriculum::where('type', 'assignment')/*->where('hide_from_backend', $requestData['hide_from_backend'])*/
        ;
        if (isset($requestData['course_id'])) {
            $dataInput = $dataInput->where('course_id', $requestData['course_id']);
            if (isset($requestData['section_id'])) {
                $dataInput = $dataInput->where('section_id', $requestData['section_id']);
            }
        }
        $count_total = $count_filter = $dataInput->count();
        $dataInput = $dataInput->orderBy('id')->skip($start)->take($pageSize);
        $data_tables = DataTables::of($dataInput);
        $data_tables->EditColumn('course_name', function ($item) {
            return isset($item->courseDetail) ? html_entity_decode($item->courseDetail->name) : "- - -";
        });
        $data_tables->EditColumn('section_name', function ($item) {
            return isset($item->sectionDetail) ? html_entity_decode($item->sectionDetail->name) : "- - -";
        });/*
        $data_tables->EditColumn('hide_status', function ($item) {
            return view('courses.partials.curriculum_hide_status', compact('item'))->render();
        });*/
        $data_tables->EditColumn('action', function ($item) {
            return view('assignment.partials.action', compact('item'))->render();
        });
        $data_tables->with([
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filter,
        ]);
        $data_tables->rawColumns(['action', 'hide_status']);
        return $data_tables->make(true);
    }

    public function storeAssignment($request): array
    {
        DB::beginTransaction();
        try {
            $requestData = $request->except('_token');
            $requestData['is_free'] = isset($requestData['is_free']) ? 1 : 0;
            $requestData['include_courses_evaluation'] = isset($requestData['include_courses_evaluation']) ? 1 : 0;
            $requestData['video'] = "https://player.vimeo.com/video/" . $requestData['video_id'];
            $requestData['time'] = getVimeoVideoDuration($requestData['video']);
            $external_urls = getVimeoExternalUrl($requestData['video_id']);
            if (!empty($external_urls) && count($external_urls) > 0) {
                $requestData['video_download_url_1'] = isset($external_urls[0]['link']) ? $external_urls[0]['link'] : null;
                $requestData['video_download_url_2'] = isset($external_urls[1]['link']) ? $external_urls[1]['link'] : null;
                $requestData['video_download_url_3'] = isset($external_urls[2]['link']) ? $external_urls[2]['link'] : null;
            }
            $assignment = Assignment::create($requestData);
            if (isset($requestData['title_attachment'])) {
                foreach ($requestData['title_attachment'] as $key => $value) {
                    $files = isset($requestData['attachment']) ? $requestData['attachment'][$key] : '';
                    if ($files) {
                        $file = self::uploadAttachment($files, $assignment, $value);
                        CourseAttachment::create([
                            'curriculum_id' => $assignment->id,
                            'curriculum_type' => 'assignment',
                            'title' => $value,
                            'attachment' => isset($file) ? $file : '',
                        ]);
                    }
                }
            }
            Curriculum::create([
                'course_id' => $assignment->course_id,
                'section_id' => $assignment->section_id,
                'curriculum_list_id' => $assignment->id,
                'name' => $assignment->name,
                'type' => 'assignment'
            ]);
            Sluglists::create([
                'course_id' => $assignment->course_id,
                'curriculum_id' => $assignment->id,
                'curriculum_type' => 'assignment',
                'slug' => $assignment->slug,
            ]);
            event(new UpdateCourseTimeEvent(['course_id' => [$assignment->course_id]]));
            DB::commit();
            return ['status' => true];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function updateSlug($request): array
    {
        $requestData = $request->except('_token');
        $exitSlug = Sluglists::where('slug', $requestData['slug'])->first();
        if (is_null($exitSlug)) {
            $assignment = Assignment::where('id', $requestData['unit_id'])->first();
            $emailData = [
                'COURSE_NAME' => $assignment->courseDetail->name,
                'UNIT_NAME' => $assignment->name,
                'OLD_SLUG_URL' => env('FRONT_URL').$assignment->courseDetail->slug.'/'.$assignment->slug,
                'NEW_SLUG_URL' => env('FRONT_URL').$assignment->courseDetail->slug.'/'.$requestData['slug'],
            ];
            $assignment->update(['slug' => $requestData['slug']]);
            Sluglists::create([
                'course_id' => $assignment->course_id,
                'curriculum_id' => $requestData['unit_id'],
                'curriculum_type' => 'assignment',
                'slug' => $requestData['slug'],
            ]);
            event(new UpdateUnitSlugMailEvent($emailData));
            return ['status' => true];
        }
        return ['status' => false, 'message' => 'Slug Already Exist!'];
    }

    public function updateActiveStatus($request): bool
    {
        $assignment = Assignment::find($request->id);
        $assignment->is_active = $request->is_active;
        $assignment->save();

        Curriculum::where('curriculum_list_id', $assignment->id)
            ->where('type', 'assignment')->update(['is_active' => $request->is_active]);
        $courseList = Curriculum::where('curriculum_list_id', $assignment->id)->where('type', 'assignment')->pluck('course_id')->toArray();
        event(new UpdateCourseTimeEvent(['course_id' => $courseList]));
        return true;
    }

    public function getAssignmentDetails($id)
    {
        return Assignment::findOrFail($id);
    }

    public function getAssignmentAttachment($id)
    {
        return CourseAttachment::where('curriculum_type', '=', 'assignment')->where('curriculum_id', $id)->get();
    }

    public function updateAssignment($request, $id): bool
    {
        $requestData = $request->except('_token');
        $assignment = self::getAssignmentDetails($id);
        unset($requestData['slug']);
        $requestData['is_free'] = isset($requestData['is_free']) ? 1 : 0;
        $requestData['include_courses_evaluation'] = isset($requestData['include_courses_evaluation']) ? 1 : 0;
        $requestData['video'] = "https://player.vimeo.com/video/" . $requestData['video_id'];
        $requestData['time'] = getVimeoVideoDuration($requestData['video']);
        $external_urls = getVimeoExternalUrl($requestData['video_id']);
        if (!empty($external_urls) && count($external_urls) > 0) {
            $requestData['video_download_url_1'] = isset($external_urls[0]['link']) ? $external_urls[0]['link'] : null;
            $requestData['video_download_url_2'] = isset($external_urls[1]['link']) ? $external_urls[1]['link'] : null;
            $requestData['video_download_url_3'] = isset($external_urls[2]['link']) ? $external_urls[2]['link'] : null;
        }

        if (isset($requestData['title_attachment'])) {
            CourseAttachment::where('curriculum_id', $id)->delete();
            foreach ($requestData['title_attachment'] as $key => $value) {
                if (isset($requestData['attachment'][$key])) {
                    $files = $requestData['attachment'][$key];
                    $file = self::uploadAttachment($files, $assignment, $value);
                } else {
                    if (isset($requestData['hidden_attachment' . $key])) {
                        $file = $requestData['hidden_attachment' . $key];
                    } else {
                        $file = '';
                    }
                }
                CourseAttachment::create([
                    'curriculum_id' => $id,
                    'curriculum_type' => 'assignment',
                    'title' => $value,
                    'attachment' => isset($file) ? $file : '',
                ]);
                unset($file);
            }
        }
        $assignment->update($requestData);

        $existData = Curriculum::where('curriculum_list_id', $assignment->id)->where('type', 'assignment')->first();
        if (isset($existData)) {
            $existData->update(['name' => $assignment->name, 'course_id' => $assignment->course_id, 'section_id' => $assignment->section_id]);
            Curriculum::where('curriculum_list_id', $assignment->id)->where('type', 'assignment')
                ->where('id', '!=', $existData->id)->update([
                    'name' => $assignment->name
                ]);
        } else {
            Curriculum::create([
                'course_id' => $assignment->course_id,
                'section_id' => $assignment->section_id,
                'curriculum_list_id' => $assignment->id,
                'name' => $assignment->name,
                'type' => 'assignment'
            ]);
        }
        $courseList = Curriculum::where('curriculum_list_id', $assignment->id)->where('type', 'assignment')->pluck('course_id')->toArray();
        event(new UpdateCourseTimeEvent(['course_id' => $courseList]));
        return true;
    }

    public function uploadFile($file, $assignment = NULL): string
    {
        $uploadPath = storage_path("app/public/attachments/");

        if (isset($assignment)) {
            $imagePath = $uploadPath . $assignment->attachment;
            @unlink($imagePath);
        }

        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '_assignment.' . $extension;
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function uploadAttachment($file, $unit = NULL, $title = NULL): string
    {
        $uploadPath = storage_path('app/public/attachments/');
        if (isset($unit)) {
            $imagePath = $uploadPath . $unit->attachment;
            @unlink($imagePath);
        }
        $extension = $file->getClientOriginalExtension();
        $name = str_replace(' ', '_', $title);
        $fileName = $name . '.' . $extension;
        if (is_file($uploadPath . $fileName)) {
            $fileName = $name . '_' . rand(11111, 99999) . '.' . $extension;
        }
        $file->move($uploadPath, $fileName);
        return $fileName;
    }

    public function getAjaxExportData($request)
    {
        $requestData = $request->all();
        $sections = Section::where('course_id', $requestData['course_id'])->pluck('name')->toarray();
        $spreadsheet = new Spreadsheet();
        $dummySeat = $spreadsheet->createSheet();
        $dummySeat->setTitle('Worksheet2');
        $test = 1;
        $lastCell = null;
        foreach ($sections as $key => $value) {
            $dummySeat->setCellValue('A' . $test, $value);
            $lastCell = 'A' . $test;
            $test++;
        }
        $sheet = $spreadsheet->getActiveSheet();
        $predefinedColumn = ['Course_id', 'Section', 'Name', 'Slug', 'Video Id', 'Marks'];
        $alpha = 'A';
        foreach ($predefinedColumn as $value) {
            $sheet->setCellValue($alpha . '1', $value);
            $sheet->getColumnDimension($alpha)->setAutoSize(true);
            $alpha++;
        }
        $sheet->setCellValue('A2', $requestData['course_id']);
        for ($i = 2; $i <= 500; $i++) {
            $validation = $spreadsheet->getActiveSheet()->getCell('B' . $i)->getDataValidation();
            $validation->setType(DataValidation::TYPE_LIST);
            $validation->setErrorStyle(DataValidation::STYLE_INFORMATION);
            $validation->setAllowBlank(false);
            $validation->setShowInputMessage(true);
            $validation->setShowErrorMessage(true);
            $validation->setShowDropDown(true);
            $validation->setErrorTitle('Input error');
            $validation->setError('Value is not in list.');
            $validation->setPromptTitle('Pick from list');
            $validation->setPrompt('Please pick a value from the drop-down list.');
            $validation->setFormula1('Worksheet2!A1:'.$lastCell);
        }
        $writer = new Writer($spreadsheet);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Assignment Sample.xlsx"');
        $writer->save("php://output");
        return true;
    }

    public function importUpdateAssignment($request): array
    {
        DB::beginTransaction();
        try {
            $requestData = $request->all();
            $reader = new Xlsx();
            $spreadsheet = $reader->load($requestData['file']);
            $sheetData = $spreadsheet->getActiveSheet()->toArray();
            if (count($sheetData) > 0) {
                unset($sheetData[0]);
                foreach ($sheetData as $sheetDatum) {
                    if ($sheetDatum[0] != "") {
                        $courseExist = Course::where('id', $sheetDatum[0])->first();
                        if (isset($courseExist)) {
                            $section = Section::where('course_id', $sheetDatum[0])->where('name', $sheetDatum[1])->first();
                            if (isset($section)) {
                                $slug = $sheetDatum[3];
                                $slugExist = Sluglists::where('slug', $slug)->first();
                                if (isset($slugExist)) {
                                    $slug = $sheetDatum[3] . '_' . time();
                                }
                                $video_id = $sheetDatum[4];
                                if (isset($video_id)) {
                                    $video = "https://player.vimeo.com/video/" . $video_id;
                                    $time = getVimeoVideoDuration($video);
                                    $external_urls = getVimeoExternalUrl($video_id);
                                    if (!empty($external_urls) && count($external_urls) > 0) {
                                        $video_download_url_1 = isset($external_urls[0]['link']) ? $external_urls[0]['link'] : null;
                                        $video_download_url_2 = isset($external_urls[1]['link']) ? $external_urls[1]['link'] : null;
                                        $video_download_url_3 = isset($external_urls[2]['link']) ? $external_urls[2]['link'] : null;
                                    }
                                }
                                $assignment = Assignment::create([
                                    'course_id' => $sheetDatum[0],
                                    'section_id' => $section->id,
                                    'name' => $sheetDatum[2],
                                    'slug' => $slug,
                                    'video' => $video ?? null,
                                    'video_id' => ($sheetDatum[4] != "") ? $sheetDatum[4] : null,
                                    'mark' => $sheetDatum[5],
                                    'video_download_url_1' => $video_download_url_1 ?? null,
                                    'video_download_url_2' => $video_download_url_2 ?? null,
                                    'video_download_url_3' => $video_download_url_3 ?? null,
                                    'time' => $time ?? null,
                                    'is_active' => 0
                                ]);
                                Curriculum::create([
                                    'course_id' => $assignment->course_id,
                                    'section_id' => $assignment->section_id,
                                    'curriculum_list_id' => $assignment->id,
                                    'name' => $assignment->name,
                                    'type' => 'assignment',
                                    'is_active' => 0
                                ]);
                                Sluglists::create([
                                    'course_id' => $assignment->course_id,
                                    'curriculum_id' => $assignment->id,
                                    'curriculum_type' => 'assignment',
                                    'slug' => $slug,
                                ]);
                            }
                        }
                    }
                }
            }
            DB::commit();
            return ['status' => true, 'message' => 'Assignment imported successfully.'];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function storeSchemaScript($unit_id)
    {
        $unit = Assignment::find($unit_id);
        $webPageSchema = Schema::webPage()
            ->name($unit->name)
            ->url(env('FRONT_URL') . '' . $unit->courseDetail->slug . '/' . $unit->slug)
            ->speakable(Schema::speakableSpecification()->xpath([
                "/html/head/title",
                "/html/head/meta[@name='description']/@content"
            ]))->toScript();

        /*VIDEO OBJECT SCHEMA*/
        $videoObjectSchema = Schema::videoObject()
            ->name($unit->name)
            ->description($unit->content ?? $unit->name)
            ->uploadDate(isset($unit->created_at) ? formatDate($unit->created_at, 'Y-m-d h:i:s') : formatDate($unit->updated_at, 'Y-m-d h:i:s'))
            ->thumbnailUrl($unit->courseDetail->course_icon)
            ->if(isset($unit->video_url), function (VideoObject $schema) use ($unit) {
                $schema->contentUrl(env('FRONT_URL') . '' . $unit->courseDetail->slug . '/' . $unit->slug)
                    ->embedUrl($unit->video_url);
            })->if(isset($unit->youtube_video_url), function (VideoObject $schema) use ($unit) {
                $schema->contentUrl(env('FRONT_URL') . '' . $unit->courseDetail->slug . '/' . $unit->slug)
                    ->embedUrl($unit->video_embed_url);
            })->toScript();
        /*VIDEO OBJECT SCHEMA END*/

        /*FAQS SCHEMA*/
        $faqsSchema = "";
        if (count($unit->relatedFaqTitles) > 0) {
            $faqsSchema = Schema::fAQPage();
            $faqMinEntityData = [];
            foreach ($unit->relatedFaqTitles as $faq) {
                if (isset($faq['answer'])) {
                    $faqMinEntityData[] = Schema::question()->name($faq['content'])->acceptedAnswer(Schema::answer()->text($faq['answer']));
                }
            }
            if (!empty($faqMinEntityData)) {
                $faqsSchema->mainEntity($faqMinEntityData);
            }
            $faqsSchema = $faqsSchema->toScript();
        }
        /*FAQS SCHEMA END*/
        /*BREADCRUMBS SCHEMA*/
        $breadcrumbsSchema = Schema::breadcrumbList()
            ->itemListElement([
                Schema::listItem()->position(1)->item(Schema::thing()->identifier(env('FRONT_URL') . 'course/' . $unit->courseDetail->slug)->name($unit->courseDetail->name)),
                Schema::listItem()->position(2)->item(Schema::thing()->identifier(env('FRONT_URL') . '' . $unit->courseDetail->slug . '/' . $unit->slug)->name($unit->name))
            ])->toScript();
        $schemaScript = $webPageSchema . '
        ' . $videoObjectSchema . '
        ' . $faqsSchema . '
        ' . $breadcrumbsSchema;
        $unit->update(['schema_script' => $schemaScript]);
    }
}
