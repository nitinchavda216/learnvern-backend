<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08-02-2021
 * Time: 02:19 PM
 */

namespace App\Repositories;

use App\Interfaces\LocationPagesRepositoryInterface;
use App\Models\Course;
use App\Models\LocationPages;
use Spatie\SchemaOrg\AggregateRating;
use Spatie\SchemaOrg\Schema;

class LocationPagesRepository implements LocationPagesRepositoryInterface
{
    public function getAllLocationPages()
    {
        return LocationPages::orderBy('id', 'DESC')->get();
    }

    public function storeLocationPage($request)
    {
        $requestData = $request->all();
        if (isset($requestData['course_intro_video'])) {
            $requestData['course_intro_video_embed'] = getVimeoVideoEmbedUrl($requestData['course_intro_video']);
        }
        $requestData['canonical_url'] = $requestData['canonical_url'] ?? env('FRONT_URL').'location/'.$requestData['slug'];
        $locationPage = LocationPages::create($requestData);
        self::storeSchemaScript($locationPage, $locationPage->course_id);
        return true;
    }

    public function updateLocationPage($request, $id)
    {
        $requestData = $request->all();
        $locationPage = self::getLocationPagesDetails($id);
        if (isset($requestData['course_intro_video'])) {
            $requestData['course_intro_video_embed'] = getVimeoVideoEmbedUrl($requestData['course_intro_video']);
        }
        $locationPage->update($requestData);
        self::storeSchemaScript($locationPage, $locationPage->course_id);
        return true;
    }

    public function storeSchemaScript($locationPage, $course_id)
    {
        $course = Course::find($course_id);
        $webPageSchema = Schema::webPage()
            ->name($locationPage->name)
            ->url(env('FRONT_URL') . 'location/' . $locationPage->slug)
            ->speakable(Schema::speakableSpecification()->xpath([
                "/html/head/title",
                "/html/head/meta[@name='description']/@content"
            ]))->toScript();

        /*COURSE SCHEMA*/
        $productSchema = Schema::course()
            ->name($locationPage->name)
            ->description(isset($locationPage->content) ? strip_tags($locationPage->content) : $locationPage->name)
            ->image($course->course_icon)
            ->url(env('FRONT_URL') . 'location/' . $locationPage->slug)
            ->provider(Schema::organization()->name("LearnVern")->url(env('FRONT_URL')));
        if ((isset($course->comment_count) && ($course->comment_count > 0))) {
            $productSchema = $productSchema->aggregateRating(Schema::aggregateRating()
                ->ratingValue($course->average_rating)
                ->reviewCount($course->comment_count)
            );
        }
        $reviews = $course->relatedComments->where('is_default_testimonial', 1)->take(16);
        if (count($reviews) > 0) {
            foreach ($reviews as $review) {
                $reviewData[] = Schema::review()
                    ->reviewRating(Schema::rating()->ratingValue($review->rating))
                    ->reviewBody($review->content)
                    ->author(Schema::person()->name($review->author));
            }
            $productSchema = $productSchema->review($reviewData);
        }
        $productSchema = $productSchema->toScript();
        /*COURSE SCHEMA END*/

        /*FAQS SCHEMA*/
        $faqsSchema = "";
        if (count($course->relatedFaqs) > 0) {
            $faqsSchema = Schema::fAQPage();
            foreach ($course->relatedFaqs as $faq) {
                $faqMinEntityData[] = Schema::question()->name($faq['question'])->acceptedAnswer(Schema::answer()->text($faq['answer']));
            }
            if (!empty($faqsSchema)) {
                $faqsSchema->mainEntity($faqMinEntityData);
            }
            $faqsSchema = $faqsSchema->toScript();
        }
        /*FAQS SCHEMA END*/

        /*VIDEO OBJECT SCHEMA*/
        $videoObjectSchema = Schema::videoObject()
            ->name($locationPage->name)
            ->description($course->brief_description ?? $locationPage->name)
            ->thumbnailUrl($course->course_icon)
            ->uploadDate(isset($locationPage->created_at) ? formatDate($locationPage->created_at, 'Y-m-d h:i:s') : formatDate($locationPage->updated_at, 'Y-m-d h:i:s'))
            ->video(Schema::videoObject()->contentUrl(env('FRONT_URL') . 'course/' . $course->slug)
                ->embedUrl($locationPage->course_intro_video_embed))->toScript();
        /*VIDEO OBJECT SCHEMA END*/

        /*BREADCRUMBS SCHEMA*/
        $breadcrumbsSchema = Schema::breadcrumbList()
            ->itemListElement([
                Schema::listItem()->position(1)->item(Schema::thing()->identifier(env('FRONT_URL'))->name('Home')),
                Schema::listItem()->position(2)->item(Schema::thing()->identifier(env('FRONT_URL') . 'course/' . $course->slug)->name($course->name))
            ])->toScript();
        $schemaScript = $webPageSchema . '
        ' . $productSchema . '
        ' . $faqsSchema . '
        ' . $videoObjectSchema . '
        ' . $breadcrumbsSchema;
        $locationPage->update(['schema_script' => $schemaScript]);
    }

    public function updateActiveStatus($request)
    {
        $LocationPages = LocationPages::find($request->id);
        $LocationPages->is_active = $request->is_active;
        $LocationPages->save();
        return true;
    }

    public function getLocationPagesDetails($id)
    {
        return LocationPages::findOrFail($id);
    }
}
