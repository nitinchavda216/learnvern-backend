<?php

namespace App\Repositories;

use App\Interfaces\SiteConfigurationsRepositoryInterface;
use App\Models\Colleges;
use App\Models\SiteConfiguration;
use App\Models\SiteConfigurationGroup;
use App\Models\Country;
use App\Models\Pages;
use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SiteConfigurationsRepository implements SiteConfigurationsRepositoryInterface
{
    public function getAllConfigurationGroups($request)
    {
        return SiteConfigurationGroup::with(['siteConfigurations' => function ($q) use ($request) {
            $q->where('partner_id', $request->partner_id);
        }])->get();
    }

    public function updateConfigurations(Request $request)
    {
        foreach ($request->configuration_value as $key => $value) {

            $configuration = SiteConfiguration::where('partner_id', session('partner_id'))->where('identifier', $key)->first();
            if ($key == 'enable_cash_on_delivery') {
                $configuration->configuration_value = 1;
            } elseif ($key == 'show_cancellation_option_status_list') {
                $configuration->configuration_value = implode(',', $value);
            } else {
                $configuration->configuration_value = $value;
            }
            $configuration->save();
        }
        return true;
    }

    public function getConfigurationForApp()
    {
        $data = SiteConfiguration::where('partner_id', 1)
            ->whereIn('identifier', [
                'email_address',
                'web_site',
                'logo',
                'sample_certificate',
                'contact_no',
                'facebook_link',
                'youtube_link',
                'instagram_link',
                'linkedin_link',
                'twitter_link',
                'telegram_link',
                'footer_content',
                'footer_copyrighttext',
                'guest_user_video_time_limit',
                'partner_application_receiver_email',
                'general_share_message',
                'refer_share_message',
            ])->pluck('configuration_value', 'identifier')->toArray();
        return $data;
    }

    public function getConfigurationByKeys($keys = [])
    {
        $data = SiteConfiguration::where('partner_id', 1)
            ->whereIn('identifier', $keys)->pluck('configuration_value', 'identifier')->toArray();
        return $data;
    }

    public function getBlockedUsersDomain()
    {
        $configuration = SiteConfiguration::where('identifier', 'blocked_user_domains')->select('configuration_value')->first();
        return explode(',', $configuration->configuration_value);
    }

    public function getCollegeList($college_search = null)
    {
        $result = Colleges::query();
        if (!is_null($college_search)) {
            $result->where('title', 'LIKE', "%{$college_search}%");
            $result->orWhere('state', 'LIKE', "%{$college_search}%");
            $result->orWhere('district', 'LIKE', "%{$college_search}%");
            $result->orWhere('city', 'LIKE', "%{$college_search}%");
        }
        $result->select('id', 'title');
        return $result->get();
    }

    public function getCountry()
    {
        $countries = Country::select('id', 'name as title')->get()->toArray();;
        $indiaCountry = $countries[100];
        unset($countries[100]);
        $countries = array_values($countries);
        array_unshift($countries,$indiaCountry);
        return $countries;
    }

    public function getIsdCode()
    {
        $isdCodes = Country::orderBy('isd_code')->pluck('name', 'isd_code')->toArray();
        unset($isdCodes[91]);
        $returnData[] = ['id' => 91, 'isd_code' => "India"];
        foreach ($isdCodes as $index =>$isdCode){
            $returnData[] = ['id' => $index, 'isd_code' => $isdCode];
        }

        return $returnData;
    }

    public function getPageType()
    {
        $data = Pages::select('slug as page_type', 'page_title')->where('partner_id', 1)->pluck('page_title', 'page_type')->toArray();
        return $data;
    }

    public function getPageUrl($page_type)
    {
        return Pages::select('page_title', 'slug', 'app_content')->where('partner_id', 1)->where('slug', $page_type)->first();
    }

    public function getCategoriesList()
    {
        $categories = Categories::whereHas('courses', function ($q){
            $q->where('is_active', 1);
        })->where('is_active', 1)->select('id', 'name', 'category_icon', 'image', 'is_webinar_category')->withCount('courses')->orderBy('sort_order')->get();
        $categories = $categories->filter(function ($item) {
            $item->is_webinar_category = ($item->is_webinar_category == 1) ? true : false;
            return $item;
        });
        return $categories;
    }

    public function getState($country_id)
    {
        $states = DB::table('states')
            ->select('*')
            ->where('country_id', $country_id)
            ->get();
        return $states;
    }

    public function getCity($state_id)
    {

        $cities = DB::table('cities')
            ->select('*')
            ->where('state_id', $state_id)
            ->get();
        return $cities;
    }

    public function getDistrict($state_id)
    {
        $districts = DB::table('districts')
            ->select('*')
            ->where('state_id', $state_id)
            ->get();
        return $districts;
    }

}
