<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
class CampaignExport implements WithHeadings, FromArray
{
    private $column;
    private $result;
    public function __construct($resultSet,$column)
    {
        $this->result = $resultSet;
        $this->column = $column;
    }

    public function headings(): array
    {
        return ['Name','Email','Mobile Number','Referral Code','Referral Code Link','Course'];
    }


    public function array(): array
    {
        return $this->result;
    }
}