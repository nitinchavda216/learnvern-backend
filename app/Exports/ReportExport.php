<?php

namespace App\Exports;

use App\Models\Reports;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ReportExport implements FromArray, WithColumnFormatting
{
    private $column;
    private $result;

    public function __construct($resultSet, $column)
    {
        $this->result = $resultSet;
        $this->column = $column;

    }

    public function array(): array
    {
        $fieldArray = Reports::COLUMN_LIST_NAME;
        $fieldName = [];
        foreach ($this->column as $value) {
            $fieldName[] = $fieldArray[$value]['label'];
        }
        array_unshift($this->result, $fieldName);
        return $this->result;
    }

    public function columnFormats(): array
    {
        $dateCols = [];
        $col = 'A';
        foreach ($this->column as $value) {
            if (in_array($value, Reports::DATE_TYPE_COLUMNS)) {
                $dateCols[$col] = NumberFormat::FORMAT_DATE_DDMMYYYY;
            }
            $col++;
        }
        return $dateCols;
    }

}
