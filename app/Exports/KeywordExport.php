<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class KeywordExport implements WithHeadings, FromArray, ShouldAutoSize, WithColumnFormatting, WithEvents {

    protected $alldata, $list;

    public function __construct($alldata, $list) {
        $this->alldata = $alldata;
        $this->list = $list;
    }

    public function array(): array {
        $published_goals = $this->alldata;
        return $published_goals;
    }

    public function headings(): array {
        $fieldArray = $this->list;
        $fieldName = [];
        foreach ($fieldArray as $value) {
            $fieldName[] = $value;
        }
        return $fieldName;
    }

    public function registerEvents(): array {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $cellRange = 'A1:X1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }

    public function columnFormats(): array {
        return [
        ];
    }

}
