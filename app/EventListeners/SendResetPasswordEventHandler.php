<?php

namespace App\EventListeners;

use App\Events\SendResetPasswordMailEvent;
use App\Services\EmailService;

class SendResetPasswordEventHandler
{
    protected $email_service;

    public function __construct(EmailService $emailService)
    {
        $this->email_service = $emailService;
    }

    public function handle(SendResetPasswordMailEvent $event)
    {
        $user = $event->user;
        $token = $user->forgot_password_key;
        $link = env('FRONT_URL').'password/'.$token;
        $content_var_values = ['NAME' => $user->first_name, 'RESET_URL' => $link,'DATE_TIME'=>date("Y-m-d h:i A")];
        $mail_params_array = ['to' => $user->email];
        $email_template = 'reset_password';
        $this->email_service->sendEmail($mail_params_array, $email_template, $content_var_values);
    }
}