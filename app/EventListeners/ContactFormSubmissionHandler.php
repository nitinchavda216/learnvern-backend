<?php

namespace App\EventListeners;

use App\Events\ContactFormSubmissionEvent;
use App\Models\SiteConfiguration;
use App\Services\EmailService;

class ContactFormSubmissionHandler
{
    protected $email_service;

    public function __construct(EmailService $emailService)
    {
        $this->email_service = $emailService;
    }

    public function handle(ContactFormSubmissionEvent $event)
    {
        $siteConfigurations = SiteConfiguration::where('identifier', 'email_address')->where('partner_id', 1)->first();
        $emailAddress = $siteConfigurations->configuration_value ?? "info@learnvern.com";
        $data = $event->data;
        $content_var_values = ['NAME' => $data['name'] ?? "Not Available", 'EMAIL' => $data['email'], 'PHONE' => $data['mobile_number'] ?? "Not Available", 'MESSAGE' => $data['message']];
        $mail_params_array = ['to' => $emailAddress];
        $email_template = 'contact_form_submission';
        $this->email_service->sendEmail($mail_params_array, $email_template, $content_var_values);
    }
}