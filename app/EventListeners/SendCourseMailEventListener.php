<?php

namespace App\EventListeners;

use App\Events\SendCourseMailEvent;
use App\Models\Course;
use App\Services\EmailService;

class SendCourseMailEventListener
{
    protected $email_service;

    public function __construct(EmailService $emailService)
    {
        $this->email_service = $emailService;
    }

    public function handle(SendCourseMailEvent $event)
    {
        $course = $event->course;
        $emptyMessage = "<b style='color:red;'>Value not entered</b>";
        $courses = Course::select('name')->whereIn('id', $course->pre_course)->get();
        $preCourses = array();
        foreach ($courses as $value) {
            $preCourses[] = $value->name;
        }
        $preCourses = implode(',', $preCourses);
        $relatedCourses = Course::select('name')->whereIn('id', $course->related_course)->get();
        $related_course = array();
        foreach ($relatedCourses as $relatedCourse) {
            $related_course[] = $relatedCourse->name;
        }
        $related_course = implode(',', $related_course);
        $content_var_values = [
            'COURSE_NAME' => isset($course->name) ? $course->name : $emptyMessage,
            'CATEGORY' => isset($course->category_id) ? $course->categoryDetail->name : $emptyMessage,
            'LANGUAGE' => isset($course->course_language) ? $course->course_language : $emptyMessage,
            'PRICE' => isset($course->price) ? $course->price : $emptyMessage,
            'COURSE_TYPE' => Course::COURSE_TYPES[$course->course_type],
//            'COURSE_ICON' => '<img src="https://admin.learnvern.com/storage/course/icons/90371.svg" alt="Learnvern" class="img-responsive">',
//            'COURSE_IMAGE' => '<img src="https://admin.learnvern.com/storage/course/99374.png" alt="Learnvern" height="100px" class="img-responsive">',
            'TINY_DESCRIPTION' => isset($course->tiny_description) ? $course->tiny_description : $emptyMessage,
            'BRIEF_DESCRIPTION' => isset($course->brief_description) ? $course->brief_description : $emptyMessage,
            'VIMEO_VIDEOLINK' => isset($course->intro_video) ? $course->intro_video : $emptyMessage,
            'COURSE_CONTENT' => isset($course->content) ? $course->content : $emptyMessage,
            'PRE_REQUISITES' => isset($course->pre_requisites) ? $course->pre_requisites : $emptyMessage,
            'HOW_CAN_LEARNVERN_HELP' => isset($course->how_can_learnvern_help) ? $course->how_can_learnvern_help : $emptyMessage,
            'PRE_COURSES' => isset($preCourses) ? $preCourses : $emptyMessage,
            'RELATES_COURSES' => isset($related_course) ? $related_course : $emptyMessage
        ];
        $email_template = 'new_course_create_mail';
        $mail_params_array['to'] = 'kishan.urvam@gmail.com';
        $this->email_service->sendEmail($mail_params_array, $email_template, $content_var_values);
    }
}