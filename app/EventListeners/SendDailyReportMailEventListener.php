<?php

namespace App\EventListeners;

use App\Events\SendDailyReportMailEvent;
use App\Services\EmailService;

class SendDailyReportMailEventListener
{
    protected $email_service;

    public function __construct(EmailService $emailService)
    {
        $this->email_service = $emailService;
    }

    public function handle(SendDailyReportMailEvent $event)
    {
        $emailData = $event->data;
        $content_var_values = ['DATE' => formatDate($emailData['date'], 'd-m-Y')];
        $email_template = 'daily_statistics';
        $this->email_service->sendDailyStatisticsMail($email_template, $content_var_values);
    }
}
