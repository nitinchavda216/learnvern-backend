<?php

namespace App\EventListeners;

use App\Events\SendAmbassadorProcessEmailEvent;
use App\Models\AmbassadorProgramSettings;
use App\Models\UserAmbassadorBadges;
use App\Services\EmailService;

class SendAmbassadorProcessEmailEventListener
{
    protected $email_service;

    public function __construct(EmailService $emailService)
    {
        $this->email_service = $emailService;
    }

    public function handle(SendAmbassadorProcessEmailEvent $event)
    {
        $user = $event->user;
        $badgeDetails = AmbassadorProgramSettings::where('referrals_count', 0)->first();
        if (isset($badgeDetails)){
            UserAmbassadorBadges::create([
                'user_id' => $user->id,
                'ambassador_badge_id' => $badgeDetails->id,
            ]);
            $user->ambassador_badge_id = $badgeDetails->id;
            $user->save();
        }
        /*$content_var_values = ['NAME' => $user->first_name];
        $mail_params_array = ['to' => $user->email];
        $email_template = 'ambassador_process_email';
        $this->email_service->sendEmail($mail_params_array, $email_template, $content_var_values);*/
    }
}