<?php

namespace App\EventListeners;

use App\Events\UpdateCourseTimeEvent;
use App\Models\Course;

class UpdateCourseTimeEventListener
{

    public function handle(UpdateCourseTimeEvent $event)
    {
        $event = $event->data;
        $courses = Course::whereIn('id', $event['course_id'])->get();
        foreach ($courses as $course){
            $seconds = 0;
            foreach ($course->relatedUnitCurriculum as $unit) {
                if (isset($unit->unitDetail->time)){
                    $seconds += strtotime($unit->unitDetail->time) - strtotime('TODAY');
                }
            }
            foreach ($course->relatedAssignmentCurriculum as $assignment) {
                if (isset($assignment->assignmentDetail->time)){
                    $seconds += strtotime($assignment->assignmentDetail->time) - strtotime('TODAY');
                }
            }
            $hours = floor($seconds / 3600);
            $minutes = floor(($seconds / 60) % 60);
            $seconds = $seconds % 60;
            $time = $hours.':'.$minutes.':'.$seconds;
            $course->time = $time;
            $course->save();
        }
        return true;
    }
}
