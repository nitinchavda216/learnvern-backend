<?php

namespace App\EventListeners;

use App\Events\PartnerApplicationApproved;
use App\Services\EmailService;

class PartnerApplicationApprovedEventListener
{
    protected $email_service;

    public function __construct(EmailService $emailService)
    {
        $this->email_service = $emailService;
    }

    public function handle(PartnerApplicationApproved $event)
    {
        $partner = $event->data;

        $content_var_values = [
            'NAME' => $partner['name'],
            'ACTIVATION_LINK' => env('FRONT_URL') . "partner/accepted/" . $partner['request_id'],
        ];
        $mail_params_array = ['to' => $partner['contact_person_email']];
        $email_template = 'partner_request_approved';
        $this->email_service->sendEmail($mail_params_array, $email_template, $content_var_values);
        return true;
    }
}