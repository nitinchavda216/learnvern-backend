<?php

namespace App\EventListeners;

use App\Events\UpdateCourseSlugMailEvent;
use App\Services\EmailService;

class UpdateCourseSlugMailEventListener
{
    protected $email_service;

    public function __construct(EmailService $emailService)
    {
        $this->email_service = $emailService;
    }

    public function handle(UpdateCourseSlugMailEvent $event)
    {
        $admin = getCurrentAdmin();
        $event = $event->data;
        $content_var_values = ['ADMIN_NAME' => $admin['name'], 'COURSE_NAME' => $event['COURSE_NAME'], 'OLD_SLUG_URL' => $event['OLD_SLUG_URL'], 'NEW_SLUG_URL' => $event['NEW_SLUG_URL']];
        $email_template = 'update_course_slug_mail';
        $this->email_service->sendUnitUpdateLinkToAdmins($email_template, $content_var_values);
    }
}
