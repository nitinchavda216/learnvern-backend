<?php

namespace App\EventListeners;

use App\Events\UpdateUnitSlugMailEvent;
use App\Services\EmailService;

class UpdateUnitSlugMailEventListener
{
    protected $email_service;

    public function __construct(EmailService $emailService)
    {
        $this->email_service = $emailService;
    }

    public function handle(UpdateUnitSlugMailEvent $event)
    {
        $admin = getCurrentAdmin();
        $event = $event->data;
        $content_var_values = ['ADMIN_NAME' => $admin['name'], 'COURSE_NAME' => $event['COURSE_NAME'], 'UNIT_NAME' => $event['UNIT_NAME'], 'OLD_SLUG_URL' => $event['OLD_SLUG_URL'], 'NEW_SLUG_URL' => $event['NEW_SLUG_URL']];
        $email_template = 'update_unit_slug_mail';
        $this->email_service->sendUnitUpdateLinkToAdmins($email_template, $content_var_values);
    }
}
