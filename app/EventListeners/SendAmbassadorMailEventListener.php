<?php

namespace App\EventListeners;

use App\Events\SendAmbassadorMailEvent;
use App\Services\EmailService;

class SendAmbassadorMailEventListener
{
    protected $email_service;

    public function __construct(EmailService $emailService)
    {
        $this->email_service = $emailService;
    }

    public function handle(SendAmbassadorMailEvent $event)
    {
        $user = $event->data;
        $content_var_values = ['NAME' => $user['first_name'], 'REFERRED_COUNT' => $user['referred_count'], 'BADGE_TITLE' => $user['badge_title']];
        $email_template = 'assign_ambassador_badge';
        $this->email_service->sendAmbassadorEmai($user['email'], $email_template, $content_var_values, $user);
    }
}