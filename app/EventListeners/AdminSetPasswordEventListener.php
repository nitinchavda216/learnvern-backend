<?php

namespace App\EventListeners;

use App\Events\AdminSetPasswordEvent;
use App\Services\EmailService;

class AdminSetPasswordEventListener
{
    protected $email_service;

    public function __construct(EmailService $emailService)
    {
        $this->email_service = $emailService;
    }

    public function handle(AdminSetPasswordEvent $event)
    {
        $user = $event->admin;
        $link = route('admin.set_password', $user['activation_token']);
        $content_var_values = ['NAME' => $user['name'], 'PASSWORD_LINK' => $link];
        $mail_params_array = ['to' => $user['email']];
        $email_template = 'admin_set_password_mail';
        $this->email_service->sendEmail($mail_params_array, $email_template, $content_var_values);
    }
}