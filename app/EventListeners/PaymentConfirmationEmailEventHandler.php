<?php

namespace App\EventListeners;

use App\Events\PaymentConfirmationEmailEvent;
use App\Services\EmailService;

class PaymentConfirmationEmailEventHandler
{
    protected $email_service;

    public function __construct(EmailService $emailService)
    {
        $this->email_service = $emailService;
    }

    public function handle(PaymentConfirmationEmailEvent $event)
    {
        $user_data = $event->data;
        $content_var_values = ['NAME' => $user_data['first_name'], 'COURSE_NAME' => $user_data['course_name'], 'AMOUNT' => $user_data['amount'],'CERTIFICATE_NAME'=>$user_data['certificate_name']];
        $mail_params_array = ['to' => $user_data['email']];
        $email_template = 'payment_conformation_mail';
        $this->email_service->sendEmail($mail_params_array, $email_template, $content_var_values);
    }
}