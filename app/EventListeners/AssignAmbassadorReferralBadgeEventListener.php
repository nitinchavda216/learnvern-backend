<?php

namespace App\EventListeners;

use App\Events\AssignAmbassadorReferralBadgeEvent;
use App\Models\AmbassadorProgramSettings;
use App\Models\UserAmbassadorBadges;
use App\Services\EmailService;
use PDF;

class AssignAmbassadorReferralBadgeEventListener
{
    protected $emailService;

    public function __construct(EmailService $emailService)
    {
        $this->emailService = $emailService;
    }

    public function handle(AssignAmbassadorReferralBadgeEvent $event)
    {
        $affiliate = $event->user;
        $referral_count = $affiliate->referred_count;
        $badge_details = AmbassadorProgramSettings::where('refer_from', 'other')->where('referrals_count', $referral_count)->first();
        if (isset($badge_details)) {
            UserAmbassadorBadges::create([
                'user_id' => $affiliate->id,
                'ambassador_badge_id' => $badge_details->id,
            ]);
            $updateData['ambassador_badge_id'] = $badge_details->id;
            if ($referral_count == 25) {
                $updateData['ambassador_email_send_flag'] = 1;
            }
            if ($referral_count == 50) {
                $updateData['ambassador_50_email_send_flag'] = 1;
            }
            $affiliate->update($updateData);
            $user = [
                'id' => $affiliate->id,
                'first_name' => $affiliate->first_name,
                'referred_count' => $referral_count,
                'created_at' => $affiliate->created_at,
                'email' => $affiliate->email,
            ];
            $path = storage_path('app/public/ambassador_certificates/');
            $fileName = "Learnvern_Recommendation_Letterhead_" . $affiliate->id . "_" . $referral_count . ".pdf";
            $html = view('ambassador_pdf_view', compact('user', 'referral_count'))->render();
            $dompdf = PDF::loadHtml($html);
            $dompdf->save($path . '/' . $fileName);

            $content_var_values = ['NAME' => $user['first_name'], 'REFERRED_COUNT' => $referral_count, 'BADGE_TITLE' => $badge_details['title']];
            $email_template = 'assign_ambassador_badge';
            $this->emailService->sendAmbassadorEmail($user['email'], $email_template, $content_var_values, $user);
        }
    }
}
