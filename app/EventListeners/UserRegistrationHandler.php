<?php

namespace App\EventListeners;

use App\Events\UserRegistration;
use App\Services\EmailService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class UserRegistrationHandler
{
    protected $email_service;

    public function __construct(EmailService $emailService)
    {
        $this->email_service = $emailService;
    }

    public function handle(UserRegistration $event)
    {
        $user = $event->user;
        $token = $user->activation_key;
         $link = env('FRONT_URL').'user/activation/'.$token;

        /*$link=route('activation-process',$token);*/
//        Log::alert("SIGNup Verification Testing Url: ".$link);
        $content_var_values = ['NAME' => $user->first_name, 'ACTIVATION_URL' => $link,'DATE_TIME'=>Carbon::now()->format("d-m-Y h:i A")];
        $mail_params_array = ['to' => $user->email];
        $email_template = 'activation_link';
        $this->email_service->sendEmail($mail_params_array, $email_template, $content_var_values);
    }
}
