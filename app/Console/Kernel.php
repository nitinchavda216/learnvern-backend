<?php

namespace App\Console;

use App\Console\Commands\CheckExpiredAnnouncement;
use App\Console\Commands\DailyDeletedUsers;
use App\Console\Commands\SendDailyReportCommand;
use App\Console\Commands\VisitorTracking;
use App\Console\Commands\MongoDBBackup;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CheckExpiredAnnouncement::class,
        VisitorTracking::class,
        MongoDBBackup::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->command(CheckExpiredAnnouncement::class)->hourly();
        $schedule->command(VisitorTracking::class)->daily();
        $schedule->command(SendDailyReportCommand::class)->daily();
        $schedule->command(DailyDeletedUsers::class)->daily();
        $schedule->command(MongoDBBackup::class)->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
