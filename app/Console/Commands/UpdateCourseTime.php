<?php

namespace App\Console\Commands;

use App\Models\Course;
use Illuminate\Console\Command;

class UpdateCourseTime extends Command
{
    protected $signature = 'update:course_time';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $courses = Course::where('is_active', 1)->get();
        foreach ($courses as $course){
            $seconds = 0;
            foreach ($course->relatedUnitCurriculum as $unit) {
                if (isset($unit->unitDetail->time)){
                    $seconds += strtotime($unit->unitDetail->time) - strtotime('TODAY');
                }
            }
            foreach ($course->relatedAssignmentCurriculum as $assignment) {
                if (isset($assignment->assignmentDetail->time)){
                    $seconds += strtotime($assignment->assignmentDetail->time) - strtotime('TODAY');
                }
            }
            $hours = floor($seconds / 3600);
            $minutes = floor(($seconds / 60) % 60);
            $seconds = $seconds % 60;
            $time = $hours.':'.$minutes.':'.$seconds;
            $course->update(['time' => $time]);
            $this->info("Course ".$course->id." Time Updates.");
        }
        $this->info("All Course Updated successfully.");
    }
}
