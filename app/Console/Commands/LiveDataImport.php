<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class LiveDataImport extends Command
{

    protected $signature = 'import:start';
    protected $apiLogTableDataArray = [
        'insertTable' => 'learnvern.api_logs',
        'fromTable' => 'db_learnvern_lms.lms_api_logs o',
        'columnMapping' => [
            'id' => 'o.id',
            'user_id' => 'o.user_id',
            'course_id' => 'o.course_id',
            'api_name'=>'o.api_name',
            'parameters'=>'o.parameters',
            'response'=>'o.response',
            'created_at'=>'o.created_date',
        ]
    ];
    protected $categoryTableDataArray = [
        'insertTable' => 'learnvern.categories',
        'fromTable' => 'db_learnvern_lms.lms_category o',
        'columnMapping' => [
            'id' => 'o.category_id ',
            'name' => 'o.category_name',
            'is_active' => 'o.is_active',
            'sort_order'=>'o.sort_order',
            'created_at'=>'o.created_date',
            'updated_at'=>'o.modified_date',
        ]
    ];
    protected $userTableDataArray = [
      'insertTable' => 'learnvern.users',
      'fromTable' => 'db_learnvern_lms.lms_user o INNER JOIN db_learnvern_lms.lms_user_login ul ON o.user_id = ul.user_id',
      'columnMapping' => [
          'id' => 'o.user_id',
          'first_name' => 'o.first_name',
          'last_name' => 'o.last_name',
          'certificate_name'=>'o.certificate_name',
          'username'=>'ul.user_name',
          'email'=>'ul.user_email',
          'ci_password'=>'ul.user_password',
          'gender'=>'o.gender',
          'isd_code'=>'o.country_dial_code',
          'mobile_number'=>'o.mobile_no',
          'country_id'=>'o.country_id',
          'state_id'=>'o.state_id',
          'country'=>'o.country',
          'state'=>'o.state',
          'city'=>'o.city',
          'pin_code'=>'o.pincode',
          'salutation'=>'o.salutation',
          'birth_date'=>'o.birth_date',
          'address'=>'o.address',
          'father_name'=>'o.father_name',
          'cast_category'=>'o.cast_category',
          'area_of_interest'=>'o.area_of_interest',
          'current_occupation'=>'o.current_occupation',
          'id_proof'=>'o.id_proof',
          'id_number'=>'o.id_number',
          'education'=>'o.education',
          'candidate_id'=>'o.candidate_id',
          'referral'=>'o.referral',
          'domain_signup'=>'o.domin_signup',
          'signup_from'=>'o.signup_from',
          'is_social_signup'=>'o.social_signup',
          'social_id'=>'o.social_id',
          'image'=>'o.image_path',
          'own_referral_code'=>'o.own_referral_code',
          'email_unsubscribe'=>"IF(email_unsubscribe='Y', 1, 0)",
          'ambassador_email_send_flag'=>"IF(ambassador_email_send_flag='Y', 1, 0)",
          'ambassador_50_email_send_flag'=>"IF(ambassador_50_email_send_flag='Y', 1, 0)",
          'is_active'=>'o.is_active',
          'activation_key'=>'ul.activaction_key',
          'device_token'=>'ul.device_token',
          'last_login_ip'=>'ul.last_login_ip',
          'last_login_date'=>'ul.last_login_date',
          'is_login'=>'ul.is_login',
          'send_activation_email_date'=>'ul.send_activation_email_date',
          'login_count'=>'ul.login_count',
          'nsdc_user_id'=>'o.nsdc_user_id',
          'is_from_nsdc'=>'o.is_from_nsdc',
          'created_at'=>"IF(CAST(o.created_date AS CHAR(20))='0000-00-00 00:00:00', NULL, o.created_date)",
          'updated_at'=>"IF(CAST(o.modified_date AS CHAR(20))='0000-00-00 00:00:00', NULL, o.modified_date)",
      ]
    ];
    protected $courseTableDataArray = [
        'insertTable' => 'learnvern.courses',
        'fromTable' => 'db_learnvern_lms.lms_course c LEFT JOIN db_learnvern_lms.lms_seo_details s ON s.curriculum_type = \'course\' AND c.course_id = s.curriculum_id WHERE c.category_id != \'\'',
        'columnMapping' => [
            'id' => 'c.course_id',
            'category_id' => 'c.category_id',
            'name' => 'c.course_name',
            'slug'=>'c.course_slug',
            'course_language'=>'c.course_language',
            'content'=>'c.course_content',
            'who_can_take'=>'c.who_can_take',
            'pre_requisites'=>'c.pre_requisites',
            'average_rating'=>'c.average_rating',
            'intro_video'=>'c.course_intro_video',
            'intro_video_embed'=>'c.course_intro_video_embed',
            'intro_youtube_video_id'=>'c.course_intro_youtube_video_id',
            'intro_youtube_video'=>'c.course_intro_youtube_video',
            'intro_youtube_video_embed'=>'c.course_intro_youtube_video_embed',
            'time'=>'c.course_time',
            'num_of_student'=>'c.num_of_student',
            'course_retake'=>'c.course_retake',
            'what_you_learn'=>'c.what_you_learn',
            'why_you_learn'=>'c.why_you_learn',
            'how_can_learnvern_help'=>'c.how_can_learnvern_help',
            'faq'=>'c.faq',
            'image'=>'c.course_image',
            'is_free'=>'c.is_free',
            'course_type'=>'c.course_type',
            'unit_youtube_video_flag'=>'c.course_unit_youtube_video_flag',
            'start_date'=>"IF(CAST(c.course_start_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.course_start_date)",
            'complete_date'=>"IF(CAST(c.course_complete_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.course_complete_date)",
            'pre_course'=>'c.pre_course',
            'related_course'=>'c.related_course',
            'comment_count'=>'c.comment_count',
            'is_active'=>'c.is_active',
            'meta_title'=>'s.title',
            'meta_description'=>'s.description',
            'meta_keywords'=>'s.keyword',
            'schema_script'=>'s.schema',
            'canonical_url'=>'s.canonical',
            'created_at'=>"IF(CAST(c.created_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.created_date)",
            'updated_at'=>"IF(CAST(c.modified_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.modified_date)",
        ]
    ];

    protected $courseUserTableDataArray = [
        'insertTable' => 'learnvern.course_user',
        'fromTable' => 'db_learnvern_lms.lms_course_user c',
        'columnMapping' => [
            'id' => 'c.course_user_id',
            'course_id' => 'c.course_id',
            'user_id' => 'c.user_id',
            'certificate_id'=>'c.certificate_id',
            'paid_status'=>'c.paid_status',
            'user_status_id'=>'c.course_user_status_id',
            'progress'=>'c.course_progress',
            'is_from_nsdc'=>'c.is_from_nsdc',
            'nsdc_course_id'=>'c.nsdc_course_id',
            'course_enroll_date'=>"IF(CAST(c.course_enroll_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.course_enroll_date)",
            'certificate_date'=>"IF(CAST(c.certificate_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.certificate_date)",
            'last_used_date'=>"IF(CAST(c.last_used_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.last_used_date)",
        ]
    ];


    protected $courseUserStatusTableDataArray = [
        'insertTable' => 'learnvern.course_user_status',
        'fromTable' => 'db_learnvern_lms.lms_course_user_status c',
        'columnMapping' => [
            'id' => 'c.course_user_status_id',
            'name' => 'c.course_user_status_name',
            'is_active' => 'c.is_active',
        ]
    ];


    protected $sectionTableDataArray = [
        'insertTable' => 'learnvern.sections',
        'fromTable' => 'db_learnvern_lms.lms_section c',
        'columnMapping' => [
            'id' => 'c.section_id',
            'course_id' => 'c.course_id',
            'name' => 'c.section_name',
            'is_active'=>'c.is_active',
            'created_at'=>"IF(CAST(c.created_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.created_date)",
            'updated_at'=>"IF(CAST(c.modified_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.modified_date)",
        ]
    ];


    protected $unitTableDataArray = [
        'insertTable' => 'learnvern.units',
        'fromTable' => 'db_learnvern_lms.lms_unit c LEFT JOIN db_learnvern_lms.lms_seo_details s ON s.curriculum_type = \'2\' AND c.unit_id = s.curriculum_id',
        'columnMapping' => [
            'id' => 'DISTINCT(c.unit_id)',
            'course_id' => 'c.course_id',
            'section_id' => 'c.section_id',
            'name'=>'c.unit_name',
            'slug'=>'c.unit_slug',
            'content'=>'c.unit_content',
            'short_content'=>'c.unit_short_content',
            'time'=>'c.unit_time',
            'is_free'=>'IF(CAST(c.is_free AS CHAR(20))=\'\', 0, c.is_free)',
            'video_id'=>'c.unit_video_id',
            'video_url'=>'c.unit_video_url',
            'video_download_url_1'=>'c.unit_video_download_url_1',
            'video_download_url_2'=>'c.unit_video_download_url_2',
            'video_download_url_3'=>'c.unit_video_download_url_3',
            'youtube_video_id'=>'c.unit_youtube_video_id',
            'youtube_video_url'=>'c.unit_youtube_video_url',
            'video_embed_url'=>'c.unit_youtube_video_embed_url',
            'is_active'=>'c.is_active',
            'meta_title'=>'s.title',
            'meta_description'=>'s.description',
            'meta_keywords'=>'s.keyword',
            'schema_script'=>'s.schema',
            'canonical_url'=>'s.canonical',
            'created_at'=>"IF(CAST(c.created_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.created_date)",
            'updated_at'=>"IF(CAST(c.modified_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.modified_date)",
        ]
    ];



    protected $unitFAQTableDataArray = [
        'insertTable' => 'learnvern.unit_faqs',
        'fromTable' => 'db_learnvern_lms.lms_unit_faq c',
        'columnMapping' => [
            'id' => 'c.faq_id',
            'unit_id' => 'c.unit_id',
            'content'=>'c.faq_content',
            'is_active'=>'c.is_active',
            'created_at'=>"IF(CAST(c.created_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.created_date)",
            'updated_at'=>"IF(CAST(c.modified_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.modified_date)",
        ]
    ];

    protected $unitUserTableDataArray = [
        'insertTable' => 'learnvern.unit_user',
        'fromTable' => 'db_learnvern_lms.lms_unit_user c',
        'columnMapping' => [
            'id' => 'c.unit_user_id',
            'unit_id' => 'c.unit_id',
            'user_id'=>'c.user_id',
            'is_completed'=>'c.unit_finish',
            'time'=>'c.unit_time',
            'last_used_date'=>"IF(CAST(c.last_used_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.last_used_date)",
            'next_curriculum_id'=>'c.next_curriculum_id',
            'next_curriculum_type'=>"IF(c.next_curriculum_type = '1','section',IF(c.next_curriculum_type = '2','unit',IF(c.next_curriculum_type = '3','quiz',IF(c.next_curriculum_type = '4','assignment',c.next_curriculum_type))))",
        ]
    ];

    protected $assignmentTableDataArray = [
        'insertTable' => 'learnvern.assignments',
        'fromTable' => 'db_learnvern_lms.lms_assignment c',
        'columnMapping' => [
            'id' => 'c.assignment_id',
            'course_id' => 'c.course_id',
            'section_id'=>'c.section_id',
            'name'=>'c.assignment_name',
            'content'=>'c.assignment_content',
            'video'=>'c.assignment_video',
            'video_id'=>'c.assignment_video_id',
            'mark'=>'c.assignment_mark',
            'video_download_url_1'=>'c.assi_video_download_url_1',
            'video_download_url_2'=>'c.assi_video_download_url_2',
            'video_download_url_3'=>'c.assi_video_download_url_3',
            'time'=>'c.assignment_time',
            'include_course_evaluation'=>'c.inc_course_evaluation',
            'is_free'=>'c.is_free',
            'is_active'=>'c.is_active',
            'created_at'=>"IF(CAST(c.created_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.created_date)",
            'updated_at'=>"IF(CAST(c.modified_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.modified_date)",
        ]
    ];

    protected $assignmentUserTableDataArray = [
        'insertTable' => 'learnvern.assignment_user',
        'fromTable' => 'db_learnvern_lms.lms_assignment_user c',
        'columnMapping' => [
            'id' => 'c.assi_user_id',
            'assignment_id' => 'c.assi_id',
            'user_id'=>'c.user_id',
            'course_id'=>'c.course_id',
            'is_completed'=>'c.assi_finish',
            'complete_time'=>'c.assi_complete_time',
            'last_used_date'=>'c.last_used_date',
        ]
    ];

    protected $citiesTableDataArray = [
        'insertTable' => 'learnvern.cities',
        'fromTable' => 'db_learnvern_lms.lms_cities c',
        'columnMapping' => [
            'id' => 'c.id',
            'state_id' => 'c.state_id',
            'name'=>'c.name',
        ]
    ];

    protected $commentTableDataArray = [
        'insertTable' => 'learnvern.comments',
        'fromTable' => 'db_learnvern_lms.lms_comment c',
        'columnMapping' => [
            'id' => 'c.comment_id',
            'course_id' => 'c.course_id',
            'user_id'=>'c.user_id',
            'title'=>'c.comment_title',
            'content'=>'c.comment_content',
            'rating'=>'c.comment_rating',
            'author'=>'c.comment_author',
            'is_active'=>'c.is_active',
            'created_at'=>"IF(CAST(c.created_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.created_date)",
            'updated_at'=>"IF(CAST(c.modified_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.modified_date)",
        ]
    ];

    protected $countryTableDataArray = [
        'insertTable' => 'learnvern.countries',
        'fromTable' => 'db_learnvern_lms.lms_countries c',
        'columnMapping' => [
            'id' => 'c.id',
            'country_code' => 'c.sortname',
            'name'=>'c.name',
            'isd_code'=>'c.phonecode',
        ]
    ];


    protected $cronTableDataArray = [
        'insertTable' => 'learnvern.cron_history',
        'fromTable' => 'db_learnvern_lms.lms_cron_history c',
        'columnMapping' => [
            'id' => 'c.id',
            'name' => 'c.cron_name',
            'start_time'=>'c.start_time',
            'end_time'=>'c.end_time',
            'sent_email_count'=>'c.sent_email_count',
            'created_at'=>"IF(CAST(c.last_modified AS CHAR(20))='0000-00-00 00:00:00', NULL, c.last_modified)",
            'updated_at'=>"IF(CAST(c.last_modified AS CHAR(20))='0000-00-00 00:00:00', NULL, c.last_modified)",
        ]
    ];

    protected $curriculumTableDataArray = [
        'insertTable' => 'learnvern.curriculum',
        'fromTable' => 'db_learnvern_lms.lms_curriculum c',
        'columnMapping' => [
            'id' => 'c.curriculum_id',
            'course_id' => 'c.course_id',
            'section_id'=>'c.section_id',
            'curriculum_list_id'=>'c.curriculum_list_id',
            'name'=>'c.curriculum_list_name',
            'type'=>"IF(c.curriculum_type = '1','section',IF(c.curriculum_type = '2','unit',IF(c.curriculum_type = '3','quiz',IF(c.curriculum_type = '4','assignment',c.curriculum_type))))",
            'sort_order'=>'c.sort_order',
            'is_active'=>'c.is_active',
            'created_at'=>"IF(CAST(c.created_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.created_date)",
            'updated_at'=>"IF(CAST(c.modified_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.modified_date)",
        ]
    ];


    protected $domainTableDataArray = [
        'insertTable' => 'learnvern.domains',
        'fromTable' => 'db_learnvern_lms.lms_domin c',
        'columnMapping' => [
            'id' => 'c.id',
            'name' => 'c.domin_name',
            'url'=>'c.domin_url',
            'logo'=>'c.domin_logo',
            'is_active'=>'c.is_active',
            'created_at'=>"IF(CAST(c.created_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.created_date)",
            'updated_at'=>"IF(CAST(c.modified_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.modified_date)",
        ]
    ];

    protected $locationPagesTableDataArray = [
        'insertTable' => 'learnvern.location_pages',
        'fromTable' => 'db_learnvern_lms.lms_location_pages c LEFT JOIN db_learnvern_lms.lms_seo_details s ON s.curriculum_type = \'location_page\' AND c.location_page_id = s.curriculum_id',
        'columnMapping' => [
            'id' => 'c.location_page_id',
            'course_id' => 'c.course_id',
            'name' => 'c.page_name',
            'slug'=>'c.page_slug',
            'content'=>'c.page_content',
            'course_intro_video'=>'c.course_intro_video',
            'course_intro_video_embed'=>'c.course_intro_video_embed',
            'is_active'=>'c.is_active',
            'meta_title'=>'s.title',
            'meta_description'=>'s.description',
            'meta_keywords'=>'s.keyword',
            'schema_script'=>'s.schema',
            'canonical_url'=>'s.canonical',
            'created_at'=>"IF(CAST(c.created_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.created_date)",
            'updated_at'=>"IF(CAST(c.modified_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.modified_date)",
        ]
    ];

    protected $paymentTransactionTableDataArray = [
        'insertTable' => 'learnvern.payment_transactions',
        'fromTable' => 'db_learnvern_lms.lms_payment_transaction c',
        'columnMapping' => [
            'id' => 'c.id',
            'user_id' => 'c.user_id',
            'course_id' => 'c.course_id',
            'amount'=>'c.amount',
            'transaction_id'=>'c.transaction_id',
            'mihpayid'=>'c.mihpayid',
            'transaction_date'=>'c.transaction_date',
            'created_at'=>"IF(CAST(c.created_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.created_date)",
        ]
    ];


    protected $quizTableDataArray = [
        'insertTable' => 'learnvern.quiz',
        'fromTable' => 'db_learnvern_lms.lms_quiz c',
        'columnMapping' => [
            'id' => 'c.quiz_id',
            'course_id' => 'c.course_id',
            'section_id' => 'c.section_id',
            'name'=>'c.quiz_name',
            'sub_title'=>"c.quiz_sub_title",
            'content'=>'c.quiz_content',
            'short_content'=>'c.quiz_short_content',
            'time'=>'c.quiz_time',
            'evaluate'=>'c.quiz_evaluate',
            'retake'=>'c.quiz_retake',
            'is_master_quiz'=>'c.is_master_quiz',
            'is_active'=>'c.is_active',
            'created_at'=>"IF(CAST(c.created_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.created_date)",
            'updated_at'=>"IF(CAST(c.modified_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.modified_date)",
        ]
    ];

    protected $questionAnswerUserTableDataArray = [
        'insertTable' => 'learnvern.question_answer_user',
        'fromTable' => 'db_learnvern_lms.lms_question_answer_user c',
        'columnMapping' => [
            'id' => 'c.que_ans_user_id',
            'user_id' => 'c.user_id',
            'quiz_id' => 'c.quiz_id',
            'question_id'=>'c.que_id',
            'user_answer_id'=>"c.user_answer_id",
            'answer'=>'c.user_answer',
            'is_active'=>'c.is_active',
            'created_at'=>"IF(CAST(c.created_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.created_date)",
            'updated_at'=>"IF(CAST(c.modified_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.modified_date)",
        ]
    ];



    protected $quizQuestionTableDataArray = [
        'insertTable' => 'learnvern.quiz_questions',
        'fromTable' => 'db_learnvern_lms.lms_question c',
        'columnMapping' => [
            'id' => 'c.que_id',
            'quiz_id' => 'c.quiz_id',
            'title' => 'c.que_title',
            'content'=>'c.que_content',
            'answer'=>"c.que_answer",
            'type'=>'c.que_type',
            'mark'=>'c.que_mark',
            'is_active'=>'c.is_active',
            'created_at'=>"IF(CAST(c.created_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.created_date)",
            'updated_at'=>"IF(CAST(c.modified_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.modified_date)",
        ]
    ];



    protected $quizQuestionOptionTableDataArray = [
        'insertTable' => 'learnvern.quiz_question_options',
        'fromTable' => 'db_learnvern_lms.lms_que_option c',
        'columnMapping' => [
            'id' => 'c.que_option_id',
            'quiz_id' => 'c.quiz_id',
            'question_id' => 'c.que_id',
            'option_id'=>'c.option_id',
            'content'=>"c.option_content",
            'is_active'=>'c.is_active',
            'created_at'=>"IF(CAST(c.created_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.created_date)",
            'updated_at'=>"IF(CAST(c.modified_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.modified_date)",
        ]
    ];



    protected $quizUserTableDataArray = [
        'insertTable' => 'learnvern.quiz_user',
        'fromTable' => 'db_learnvern_lms.lms_quiz_user c',
        'columnMapping' => [
            'id' => 'c.quiz_user_id',
            'quiz_id' => 'c.quiz_id',
            'user_id' => 'c.user_id',
            'user_status'=>'c.quiz_user_status',
            'marks'=>"c.quiz_marks",
            'is_completed'=>'c.quiz_finish',
            'created_at'=>"IF(CAST(c.quiz_time AS CHAR(20))='0000-00-00 00:00:00', NULL, c.quiz_time)",
        ]
    ];



    protected $shareVideoTrackingTableDataArray = [
        'insertTable' => 'learnvern.share_video_tracking',
        'fromTable' => 'db_learnvern_lms.share_video_tracking c',
        'columnMapping' => [
            'id' => 'c.share_video_tracking_id',
            'unit_id' => 'c.unit_id',
            'course_id' => 'c.course_id',
            'send_user_id'=>'c.send_user_id',
            'receiver_user_id'=>"c.receiver_user_id",
            'receiver_video_open'=>'c.receiver_video_open',
            'share_time'=>"IF(CAST(c.share_time AS CHAR(20))='0000-00-00 00:00:00', NULL, c.share_time)",
            'created_at'=>"IF(CAST(c.share_time AS CHAR(20))='0000-00-00 00:00:00', NULL, c.share_time)",
        ]
    ];


    protected $stateTableDataArray = [
        'insertTable' => 'learnvern.states',
        'fromTable' => 'db_learnvern_lms.lms_states c',
        'columnMapping' => [
            'id' => 'c.id',
            'country_id' => 'c.country_id',
            'name' => 'c.name',
        ]
    ];

    protected $abandonedPaymentReportsTableDataArray = [
        'insertTable' => 'learnvern.abandoned_payment_reports',
        'fromTable' => 'db_learnvern_lms.abandoned_payment_reports c',
        'columnMapping' => [
            'id' => 'c.id',
            'user_id' => 'c.user_id',
            'course_id' => 'c.course_id',
            'trans_id'=>'c.trans_id',
            'amount'=>'c.amount',
            'created_at'=>"IF(CAST(c.created_date AS CHAR(20))='0000-00-00 00:00:00', NULL, c.created_date)",
        ]
    ];

    protected $description = 'Command description';


    public function __construct()
    {
        parent::__construct();
    }


    protected function buildQueryForInsert($userTableDataArray) {

        $table = $userTableDataArray['insertTable'];
        $from = $userTableDataArray['fromTable'];
        $key = array_keys($userTableDataArray['columnMapping']);
        $val = array_values($userTableDataArray['columnMapping']);
        $sql = "INSERT INTO $table (" . implode(', ', $key) . ") ". "SELECT ".implode(', ', $val)." FROM $from";
        return($sql);
    }


    public function handle()
    {

        try {
            DB::beginTransaction();


            $query = $this->buildQueryForInsert($this->abandonedPaymentReportsTableDataArray);
            DB::statement($query);
            $this->info("Abandone Payment Report Import Successfully.");

            $query = $this->buildQueryForInsert($this->userTableDataArray);
            DB::statement($query);
            $this->info("Users Import Successfully.");

            $query = $this->buildQueryForInsert($this->apiLogTableDataArray);
            DB::statement($query);
            $this->info("Api Log Import Successfully.");

            $query = $this->buildQueryForInsert($this->categoryTableDataArray);
            DB::statement($query);
            $this->info("Category Import Successfully.");

            $query = $this->buildQueryForInsert($this->courseTableDataArray);
            DB::statement($query);
            $this->info("Course Import Successfully.");

            $query = $this->buildQueryForInsert($this->courseUserTableDataArray);
            DB::statement($query);
            $this->info("Course User Import Successfully.");

            $query = $this->buildQueryForInsert($this->courseUserStatusTableDataArray);
            DB::statement($query);
            $this->info("Course User Status Import Successfully.");

            $query = $this->buildQueryForInsert($this->sectionTableDataArray);
            DB::statement($query);
            $this->info("Section Import Successfully.");

            $query = $this->buildQueryForInsert($this->unitTableDataArray);
            DB::statement($query);
            $this->info("Unit Import Successfully.");

            $query = $this->buildQueryForInsert($this->unitFAQTableDataArray);
            DB::statement($query);
            $this->info("Unit FAQ Import Successfully.");

            $query = $this->buildQueryForInsert($this->unitUserTableDataArray);
            DB::statement($query);
            $this->info("Unit User Import Successfully.");


            $query = $this->buildQueryForInsert($this->assignmentTableDataArray);
            DB::statement($query);
            $this->info("Assignment Import Successfully.");


            $query = $this->buildQueryForInsert($this->assignmentUserTableDataArray);
            DB::statement($query);
            $this->info("Assignment User Import Successfully.");

            $query = $this->buildQueryForInsert($this->citiesTableDataArray);
            DB::statement($query);
            $this->info("Cities Import Successfully.");

            $query = $this->buildQueryForInsert($this->commentTableDataArray);
            DB::statement($query);
            $this->info("Comments Import Successfully.");

            $query = $this->buildQueryForInsert($this->countryTableDataArray);
            DB::statement($query);
            $this->info("Country Import Successfully.");

            $query = $this->buildQueryForInsert($this->cronTableDataArray);
            DB::statement($query);
            $this->info("Cron Import Successfully.");

            $query = $this->buildQueryForInsert($this->curriculumTableDataArray);
            DB::statement($query);
            $this->info("Curriculam Import Successfully.");

            $query = $this->buildQueryForInsert($this->domainTableDataArray);
            DB::statement($query);
            $this->info("Domain Import Successfully.");

            $query = $this->buildQueryForInsert($this->locationPagesTableDataArray);
            DB::statement($query);
            $this->info("Location Pages Import Successfully.");

            $query = $this->buildQueryForInsert($this->paymentTransactionTableDataArray);
            DB::statement($query);
            $this->info("Payment Transaction Import Successfully.");

            $query = $this->buildQueryForInsert($this->quizTableDataArray);
            DB::statement($query);
            $this->info("Quiz Import Successfully.");

            $query = $this->buildQueryForInsert($this->questionAnswerUserTableDataArray);
            DB::statement($query);
            $this->info("Quiz Answer User Import Successfully.");


            $query = $this->buildQueryForInsert($this->quizQuestionTableDataArray);
            DB::statement($query);
            $this->info("Quiz Question Import Successfully.");

            $query = $this->buildQueryForInsert($this->quizQuestionOptionTableDataArray);
            DB::statement($query);
            $this->info("Quiz Question Options Import Successfully.");

            $query = $this->buildQueryForInsert($this->quizUserTableDataArray);
            DB::statement($query);
            $this->info("Quiz User Import Successfully.");


            $query = $this->buildQueryForInsert($this->shareVideoTrackingTableDataArray);
            DB::statement($query);
            $this->info("Share Video Tracking Import Successfully.");


            $query = $this->buildQueryForInsert($this->stateTableDataArray);
            DB::statement($query);
            $this->info("States Import Successfully.");
            DB::commit();
            $this->info("All Data Import Successfully.");
        } catch (\Exception $e) {
            DB::rollBack();
            $this->error("Error: ".$e->getMessage());
        }

    }
}
