<?php

namespace App\Console\Commands;

use App\Models\Course;
use App\Models\CourseUser;
use App\Models\Units;
use Illuminate\Console\Command;
use Spatie\SchemaOrg\AggregateRating;
use Spatie\SchemaOrg\Schema;
use Spatie\SchemaOrg\VideoObject;

class UpdateInternationalCertificateFlag extends Command
{
    protected $signature = 'update:international_certificate_flag';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        CourseUser::whereHas('userDetail', function ($q){
            $q->where('country', '!=', null)->where('country', '!=', 'India');
        })->where('paid_status', 1)->update(['is_international_certificate' => 1]);
        $this->info("International Certificate Flag Updated Successfully.");
    }
}
