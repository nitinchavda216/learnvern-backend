<?php

namespace App\Console\Commands;

use App\Models\Course;
use Illuminate\Console\Command;
use App\Models\Sluglists;

class StoreCourseSlugIntoSlugListsTable extends Command
{
    protected $signature = 'store:course-slug-list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will store course table slug into slug_lists table';


    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $courses = Course::where('course_type', '!=', 4)->select('id', 'slug')->get();
        foreach ($courses as $course){
            $existData = Sluglists::where(['course_id' => $course->id, 'slug' => $course->slug, 'curriculum_type' => 'course'])->first();
            if (is_null($existData)){
                Sluglists::create([
                    'course_id' => $course->id, 'curriculum_id' => $course->id, 'slug' => $course->slug, 'curriculum_type' => 'course'
                ]);
            }
        }
        $this->info("Slug List Updated Successfully With Course Slug!");
    }
}
