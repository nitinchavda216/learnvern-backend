<?php

namespace App\Console\Commands;

use App\Models\CourseUser;
use App\Models\PaymentTransaction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Models\Assignment;
use App\Models\Quiz;
use App\Models\Units;
use App\Models\Sluglists;

class UpdatePaymentCourseUserId extends Command
{
    protected $signature = 'update:payment_course_user_id';

    protected $description = 'This command will update course_user_id in payment_transactions table';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        try{
            $allTransaction = PaymentTransaction::where('payment_type', 'nsdc_sales')->get();
            foreach ($allTransaction as $item){
                $courseUserDetail = CourseUser::select('id')->where('partner_id',$item->partner_id)->where('course_id',$item->course_id)->where('user_id',$item->user_id)->first();
                if(isset($courseUserDetail)){
                    $item->course_user_id = $courseUserDetail->id;
                    $item->save();
                }
            }
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            echo $e->getMessage();
        }
        $this->info("Payment transactions table course_user_id updated successfully.");
    }
}
