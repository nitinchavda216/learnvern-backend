<?php

namespace App\Console\Commands;

use App\Models\AnnouncementManagement;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckExpiredAnnouncement extends Command
{
    protected $signature = 'disable:expire_announcement';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        AnnouncementManagement::whereDate('end_date', '<', Carbon::now())->where('is_active', 1)->update(['is_active' => 0]);
        $this->info("Expired Announcements Disabled Successfully.");
    }
}
