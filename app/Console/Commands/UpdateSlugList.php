<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Models\Assignment;
use App\Models\Quiz;
use App\Models\Units;
use App\Models\Sluglists;

class UpdateSlugList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */


    protected $signature = 'update:slug-list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will create the directories needed in the application and also create the symbolic link between storage and public/storage directory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $slugLists = Sluglists::get();
        foreach ($slugLists as $slugList){
            if (($slugList->curriculum_type == 'unit') && isset($slugList->unitDetail)){
                $slugList->update(['course_id' => $slugList->unitDetail->course_id]);
            }elseif (($slugList->curriculum_type == 'assignment') && isset($slugList->assignmentDetail)){
                $slugList->update(['course_id' => $slugList->assignmentDetail->course_id]);
            }elseif (($slugList->curriculum_type == 'quiz') && isset($slugList->quizDetail)){
                $slugList->update(['course_id' => $slugList->quizDetail->course_id]);
            }
        }
        $this->info("Slug List Updated Successfully!");
    }
}
