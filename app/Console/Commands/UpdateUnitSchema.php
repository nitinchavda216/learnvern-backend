<?php

namespace App\Console\Commands;

use App\Models\Units;
use Illuminate\Console\Command;
use Spatie\SchemaOrg\Schema;
use Spatie\SchemaOrg\VideoObject;

class UpdateUnitSchema extends Command
{
    protected $signature = 'update:unit_schema';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $units = Units::whereHas('courseDetail')->where('course_id', 10712)->where('is_active', 1)->get();
        foreach ($units as $unit){
            $webPageSchema = Schema::webPage()
                ->name($unit->name)
                ->url(env('FRONT_URL') . '' . $unit->courseDetail->slug . '/' . $unit->slug)
                ->speakable(Schema::speakableSpecification()->xpath([
                    "/html/head/title",
                    "/html/head/meta[@name='description']/@content"
                ]))->toScript();

            /*VIDEO OBJECT SCHEMA*/
            $videoObjectSchema = Schema::videoObject()
                ->name($unit->name)
                ->description($unit->meta_description ?? $unit->name)
                ->uploadDate(isset($unit->created_at) ? formatDate($unit->created_at, 'Y-m-d h:i:s') : formatDate($unit->updated_at, 'Y-m-d h:i:s'))
                ->thumbnailUrl($unit->courseDetail->course_icon)
                ->if(isset($unit->video_url), function (VideoObject $schema) use ($unit) {
                    $schema->contentUrl(env('FRONT_URL') . '' . $unit->courseDetail->slug . '/' . $unit->slug)
                        ->embedUrl($unit->video_url);
                })->if(isset($unit->youtube_video_url), function (VideoObject $schema) use ($unit) {
                    $schema->contentUrl(env('FRONT_URL') . '' . $unit->courseDetail->slug . '/' . $unit->slug)
                        ->embedUrl($unit->video_embed_url);
                })->toScript();
            /*VIDEO OBJECT SCHEMA END*/
            /*FAQS SCHEMA*/
            $faqsSchema = "";
            if (count($unit->relatedFaqTitles) > 0) {
                $faqMinEntityData = [];
                foreach ($unit->relatedFaqTitles as $faq) {
                    if (isset($faq['answer'])) {
                        $faqMinEntityData[] = Schema::question()->name($faq['content'])->acceptedAnswer(Schema::answer()->text($faq['answer']));
                    }
                }
                if (!empty($faqMinEntityData)) {
                    $faqsSchema = Schema::fAQPage();
                    $faqsSchema->mainEntity($faqMinEntityData);
                    $faqsSchema = $faqsSchema->toScript();
                }
            }
            /*FAQS SCHEMA END*/
            /*BREADCRUMBS SCHEMA*/
            $breadcrumbsSchema = Schema::breadcrumbList()
                ->itemListElement([
                    Schema::listItem()->position(1)->item(Schema::thing()->identifier(env('FRONT_URL') . 'course/' . $unit->courseDetail->slug)->name($unit->courseDetail->name)),
                ])->toScript();
            $schemaScript = $webPageSchema . '
        ' . $videoObjectSchema . '
        ' . $faqsSchema . '
        ' . $breadcrumbsSchema;
            $unit->update(['schema_script' => $schemaScript]);
        }
        $this->info("Unit Schema Updated successfully.");
    }
}
