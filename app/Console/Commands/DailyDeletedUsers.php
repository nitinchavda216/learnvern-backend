<?php

namespace App\Console\Commands;

use App\Models\AssignmentUser;
use App\Models\CampaignBlockUsers;
use App\Models\CampaignUserCallings;
use App\Models\CampaignUsers;
use App\Models\CourseUser;
use App\Models\QuestionAnswerUser;
use App\Models\QuizUser;
use App\Models\SiteConfiguration;
use App\Models\UnitUser;
use App\Models\User;
use App\Models\UserFcmToken;
use App\Models\UsersToken;
use Illuminate\Console\Command;

class DailyDeletedUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:daily-users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $siteConfiguration = SiteConfiguration::where('identifier', 'daily_deleted_users_email')->select('configuration_value')->first();
        if (isset($siteConfiguration) && isset($siteConfiguration['configuration_value'])) {
//            $siteConfiguration['configuration_value'] = "g.nimesh118@gmail.com,aditya.modi@tops-int.com";
            $emailArray = explode(",", $siteConfiguration['configuration_value']);
            if (!empty($emailArray)){
                $user_id_array = User::whereIn('email', $emailArray)->pluck('id')->toArray();
                CourseUser::whereIn('user_id', $user_id_array)->forceDelete();
                AssignmentUser::whereIn('user_id', $user_id_array)->forceDelete();
                UnitUser::whereIn('user_id', $user_id_array)->forceDelete();
                QuizUser::whereIn('user_id', $user_id_array)->forceDelete();
                QuestionAnswerUser::whereIn('user_id', $user_id_array)->forceDelete();
                CampaignBlockUsers::whereIn('user_id', $user_id_array)->delete();
                CampaignUsers::whereIn('user_id', $user_id_array)->delete();
                CampaignUserCallings::whereIn('user_id', $user_id_array)->delete();
                UsersToken::whereIn('user_id', $user_id_array)->delete();
                UserFcmToken::whereIn('user_id', $user_id_array)->delete();
                User::whereIn('id', $user_id_array)->forceDelete();
            }
        }
    }
}
