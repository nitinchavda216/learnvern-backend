<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\UserAmbassadorBadges;
use Illuminate\Console\Command;

class StoreLiveUserAmbassadorBadge extends Command
{
    protected $signature = 'store:live_ambassador_badge';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $ambassador_users = User::where('referred_count', '>=', 25)->get();
        foreach ($ambassador_users as $ambassador_user){
            if ($ambassador_user->referred_count >= 250){
                $ambassador_user->update(['ambassador_badge_id' => 4]);
            }elseif ($ambassador_user->referred_count >= 50){
                $ambassador_user->update(['ambassador_badge_id' => 3]);
            }else{
                $ambassador_user->update(['ambassador_badge_id' => 2]);
            }
        }
    }
}
