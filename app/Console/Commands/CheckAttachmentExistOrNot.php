<?php

namespace App\Console\Commands;

use App\Models\CourseAttachment;
use Illuminate\Console\Command;

class CheckAttachmentExistOrNot extends Command
{
    protected $signature = 'get:not_exist_attachment';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $courseAttachments = CourseAttachment::get();
        $array = [];
        foreach ($courseAttachments as $courseAttachment) {
            if (!file_exists(public_path().'/storage/attachments/'.$courseAttachment->attachment)) {
                if ($courseAttachment->curriculum_type == 'unit'){
                    $array[] = [
                        'id' => $courseAttachment->curriculum_id,
                        'unit' => $courseAttachment->unitDetail->name ?? "",
                        'unit_slug' => $courseAttachment->unitDetail->slug ?? "",
                        'course' => $courseAttachment->unitDetail->courseDetail->name ?? "",
                        'file' => $courseAttachment->attachment
                    ];
                }else{
                    $array[] = [
                        'id' => $courseAttachment->curriculum_id,
                        'assignment' => $courseAttachment->assignmentDetail->name ?? "",
                        'assignment_slug' => $courseAttachment->assignmentDetail->slug ?? "",
                        'course' => $courseAttachment->unitDetail->courseDetail->name ?? "",
                        'file' => $courseAttachment->attachment
                    ];
                }
            }
        }
        dd($array);
        $this->info("Exist Attachments.");
    }
}
