<?php

namespace App\Console\Commands;

use App\Models\CourseUser;
use App\Models\PaymentTransaction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdatePaymentDateCourseUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */


    protected $signature = 'update:payment-date';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will update payment date in course_user table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        try{

            $allTransaction = PaymentTransaction::get();
            foreach ($allTransaction as $item){
                $courseUserDetail = CourseUser::where('course_id',$item->course_id)->where('user_id',$item->user_id)->where('paid_status',1)->whereNull('payment_completed_date')->first();
                if($courseUserDetail){
                    $courseUserDetail->payment_completed_date = $item->created_at ? $item->created_at : $item->transaction_date;
                    $courseUserDetail->save();
                }

            }
            DB::commit();

        }catch (\Exception $e){

            DB::rollBack();
            echo $e->getMessage();
        }

    }
}
