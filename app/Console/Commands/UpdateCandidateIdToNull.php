<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\UserAmbassadorBadges;
use Illuminate\Console\Command;

class UpdateCandidateIdToNull extends Command
{
    protected $signature = 'update:candidate_id';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $users = User::whereDoesntHave('courses', function ($q) {
            $q->where('paid_status', 1)->where('progress', '>', 99);
        })->where('candidate_id', '>', 5)->count();
        dd($users);
    }
}
