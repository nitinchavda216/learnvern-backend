<?php

namespace App\Console\Commands;

use App\Models\Assignment;
use App\Models\Units;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class StoreVideoDetails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store:downloadable-videos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $units = Units::where('id', '>=', 12272)->get();
        foreach ($units as $unit) {
            if (isset($unit->video_id) && ($unit->video_id != "")) {
                $external_urls = getVimeoExternalUrl($unit->video_id);
                if (!empty($external_urls) && is_array($external_urls)) {
                    $unit->update(
                        [
                            'video_download_url_1' => isset($external_urls[0]['link']) ? $external_urls[0]['link'] : null,
                            'video_download_url_2' => isset($external_urls[1]['link']) ? $external_urls[1]['link'] : null,
                            'video_download_url_3' => isset($external_urls[2]['link']) ? $external_urls[2]['link'] : null
                        ]
                    );
                }
            }
        }

        $assignments = Assignment::where('id', '>=', 138)->get();
        foreach ($assignments as $assignment) {
            if (isset($assignment->video_id) && $assignment->video_id != "") {
                $external_urls = getVimeoExternalUrl($assignment->video_id);
                if (!empty($external_urls) && is_array($external_urls)) {
                    $assignment->update(
                        [
                            'video_download_url_1' => isset($external_urls[0]['link']) ? $external_urls[0]['link'] : null,
                            'video_download_url_2' => isset($external_urls[1]['link']) ? $external_urls[1]['link'] : null,
                            'video_download_url_3' => isset($external_urls[2]['link']) ? $external_urls[2]['link'] : null
                        ]
                    );
                }
            }
        }
    }
}
