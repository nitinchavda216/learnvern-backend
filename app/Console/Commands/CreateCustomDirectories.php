<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class CreateCustomDirectories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    const DIRECTORY_PATH = [
        'app/public/course' => 'course',
        'app/public/qr_codes' => 'qr_codes',
        'app/public/category' => 'category',
        'app/public/course/' => 'course',
        'app/public/attachments/' => 'attachments',
        'app/public/course/icons/' => 'course_icon',
        'app/public/ambassador_program_badge/' => 'ambassador_program_badge',
        'app/public/email_attachment/' => 'email_attachment',
        'app/public/ambassador_certificates/' => 'ambassador_certificates',
        'app/public/resume_templates/' => 'resume_templates',
        'app/public/partners_documents/' => 'partners_documents',
        'app/public/certificates/' => 'certificates',
        'app/public/certificates/images' => 'certificates/images',
        'app/public/users/' => 'users',
        'app/public/daily_statistics/' => 'daily_statistics',
        'app/public/category/images' => 'category/images',
        'app/public/course/app_image' => 'course/app_image',
        'app/public/course/app_icon' => 'course/app_icon',
        'app/public/notifications' => 'notifications',
    ];

    protected $signature = 'create:uploadable_directories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will create the directories needed in the application and also create the symbolic link between storage and public/storage directory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        # CREATE FOLDER PUBLIC
        foreach (self::DIRECTORY_PATH as $key => $path) {
            if (!File::isDirectory(storage_path($key))) {
                Storage::disk('public')->makeDirectory($path);
            }
        }
    }
}
