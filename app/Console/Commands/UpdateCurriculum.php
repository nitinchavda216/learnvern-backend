<?php

namespace App\Console\Commands;

use App\Models\Course;
use App\Models\Curriculum;
use App\Models\Section;
use App\Models\Units;
use Illuminate\Console\Command;
use Spatie\SchemaOrg\AggregateRating;
use Spatie\SchemaOrg\Schema;
use Spatie\SchemaOrg\VideoObject;

class UpdateCurriculum extends Command
{
    protected $signature = 'update:curriculum {--id=*}';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $queueArray = $this->option('id');
        $course_id = $queueArray[0];
        $course = Course::where('id', $course_id)->first();
//        dd($course->toArray());
        Curriculum::where('course_id', $course_id)->update(['is_active' => 0]);
        /*foreach ($course->relatedSection as $section){
            $existSection = Curriculum::where('curriculum_list_id', $section->id)->where('type', 'section')->first();
            if ($existSection){
                $existSection->update(['name' => $section->name, 'is_active' => 1]);
            }else{
                Curriculum::create([
                    'course_id' => $course_id, 'section_id' => $section->id, 'curriculum_list_id' => $section->id, 'name' => $section->name, 'type' => 'section', 'is_active' => 1
                ]);
            }
            foreach ($section->relatedUnits as $unit){
                $existUnit = Curriculum::where('curriculum_list_id', $unit->id)->where('type', 'unit')->first();
                if ($existUnit){
                    $existUnit->update(['course_id' => $course_id, 'section_id' => $section->id, 'name' => $unit->name, 'is_active' => 1]);
                }else{
                    Curriculum::create([
                        'course_id' => $course_id, 'section_id' => $section->id, 'curriculum_list_id' => $unit->id, 'name' => $unit->name, 'type' => 'unit', 'is_active' => 1
                    ]);
                }
            }
            foreach ($section->relatedAssignments as $assignment){
                $existAssignment = Curriculum::where('curriculum_list_id', $assignment->id)->where('type', 'assignment')->first();
                if ($existAssignment){
                    $existAssignment->update(['course_id' => $course_id, 'section_id' => $section->id, 'name' => $assignment->name, 'is_active' => 1]);
                }else{
                    Curriculum::create([
                        'course_id' => $course_id, 'section_id' => $section->id, 'curriculum_list_id' => $assignment->id, 'name' => $assignment->name, 'type' => 'assignment', 'is_active' => 1
                    ]);
                }
            }
            foreach ($section->relatedQuiz as $quiz){
                $existQuiz = Curriculum::where('curriculum_list_id', $quiz->id)->where('type', 'quiz')->first();
                if ($existQuiz){
                    $existQuiz->update(['course_id' => $course_id, 'section_id' => $section->id, 'name' => $quiz->name, 'is_active' => 1]);
                }else{
                    Curriculum::create([
                        'course_id' => $course_id, 'section_id' => $section->id, 'curriculum_list_id' => $quiz->id, 'name' => $quiz->name, 'type' => 'quiz', 'is_active' => 1
                    ]);
                }
            }
        }*/
    }
}
