<?php

namespace App\Console\Commands;

use App\Models\Course;
use App\Models\Units;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Spatie\SchemaOrg\Schema;
use Spatie\SchemaOrg\VideoObject;

class PartnerCourseAssign extends Command
{
    protected $signature = 'course:partner-assign';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        DB::table('partner_courses')->where('partner_id',1)->delete();
        $coursesList = Course::select('id','price')->get();
        foreach ($coursesList as $courses) {
            DB::table('partner_courses')->insert([
                'partner_id' => 1,
                'course_id' => $courses->id,
                'is_paid_course'=>0,
                'course_fees'=>$courses->price
            ]);
        }

        $this->info("Partner course assign successfully.");
    }
}
