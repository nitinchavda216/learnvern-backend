<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class UpdateUserData extends Command
{
    protected $signature = 'update:users';

    protected $description = 'This command will create the directories needed in the application and also create the symbolic link between storage and public/storage directory';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $users = User::whereBetween('id', [580280, 584096])->where('signup_from', 6)->get();
        foreach ($users as $user){
            $isExist = User::where('email', $user->username)->first();
            if (isset($isExist)){
                $user->forceDelete();
            }else{
                $email = $user->username;
                $username = $user->email;
                $user->username = $username;
                $user->email = $email;
                $user->save();
            }
        }
    }
}
