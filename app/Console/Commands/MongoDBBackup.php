<?php

namespace App\Console\Commands;

use Log;
use File;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class MongoDBBackup extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mongodb:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is used for backing up mongodb data.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() {
        try {
            $directory = 'mongodb_bcpk';
            $base_name = 'mongodb-';
            $remove_db_date = Carbon::now()->subDays(2)->toDateString(); // We will store 2 days backup and remove before 2 day's backup everyday.
            $storage_path = storage_path('app/' . $directory . '/');
            $out_path = $storage_path . $base_name . Carbon::now()->toDateString();
            if (!File::exists($storage_path)) {
                File::makeDirectory($storage_path);
                chmod($storage_path, 0777);
            }
            $command = "mongodump --host=localhost --port=" . env('MONGO_DB_PORT') . " --authenticationDatabase admin --username=" . env('MONGO_DB_USERNAME') . " --password=\"" . env('MONGO_DB_PASSWORD') . "\" --db " . env('MONGO_DB_DATABASE') . " --out=$out_path --gzip";
            shell_exec($command);
            $remove_path = $storage_path . $base_name . $remove_db_date;
            if (file_exists($remove_path)) {
                Storage::deleteDirectory($remove_path);
            }
            $this->info('MongoDB database exported successfully.');
        } catch (\Exception $ex) {
            $this->info('Error while exporting MongoDB database.');
        }
    }

}
