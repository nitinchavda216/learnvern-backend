<?php

namespace App\Console\Commands;

use App\Models\Course;
use Illuminate\Console\Command;

class CreateNewCustomFieldInMautic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:new-custom-field-in-mautic {--course_id=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is for creating new custom fields in mautic system';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $course_id = $this->option('course_id');
        if (isset($course_id)){
            $course = Course::where('id', $course_id)->first();
            $userInfoArray = [
                'label' => $course->name,
                'alias' => "f_".$course_id,
                'type' => "number",
                'isPubliclyUpdatable' => false,
                'defaultValue' => -1,
                'group' =>'core',
                'object'=>'contact',
                'isUniqueIdentifier' => false
            ];
            $siteurl = "market.learnvern.com";
            $loginname = 'apiadmin';
            $password = 'LearnVern2021';
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "https://" . $loginname . ":" . $password . "@" . $siteurl . "/api/fields/contact/new",
                CURLOPT_USERAGENT => 'Mautic Connector',
                CURLOPT_POSTFIELDS => $userInfoArray
            ));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_exec($curl);
            curl_close($curl);
        }
    }
}
