<?php

namespace App\Console\Commands;

use App\Models\Course;
use App\Models\Pages;
use App\Models\Units;
use Illuminate\Console\Command;
use Spatie\SchemaOrg\AggregateRating;
use Spatie\SchemaOrg\Schema;
use Spatie\SchemaOrg\VideoObject;

class UpdateCanonicalUrl extends Command
{
    protected $signature = 'update:canonical_urls';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $units = Units::get();
        foreach ($units as $unit) {
            if (isset($unit->courseDetail)) {
                $unit->update([
                    'canonical_url' => env('FRONT_URL') . $unit->courseDetail->slug . '/' . $unit->slug
                ]);
            }
        }
        /*$courses = Course::get();
        foreach ($courses as $course) {
            $course->update([
                'canonical_url' => env('FRONT_URL').'course/' . $course->slug
            ]);
        }*/
        /*$pages = Pages::get();
        foreach ($pages as $page) {
            $page->update([
                'canonical_url' => env('FRONT_URL') . $page->slug
            ]);
        }*/
        $this->info("Canonical Urls Updated successfully.");
    }
}
