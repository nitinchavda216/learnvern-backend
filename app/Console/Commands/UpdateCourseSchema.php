<?php

namespace App\Console\Commands;

use App\Models\Course;
use Illuminate\Console\Command;
use Spatie\SchemaOrg\AggregateRating;
use Spatie\SchemaOrg\Schema;
use Spatie\SchemaOrg\VideoObject;

class UpdateCourseSchema extends Command
{
    protected $signature = 'update:course_schema';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $courses = Course::where('is_active', 1)->get();
        foreach ($courses as $course) {
            $webPageSchema = Schema::webPage()
                ->name($course->name)
                ->url(env('FRONT_URL') . 'course/' . $course->slug)
                ->speakable(Schema::speakableSpecification()->xpath([
                    "/html/head/title",
                    "/html/head/meta[@name='description']/@content"
                ]))->toScript();

            /*COURSE SCHEMA*/
            $productSchema = Schema::course()
                ->name($course->name)
                ->description($course->brief_description ?? $course->name)
                ->image($course->course_icon)
                ->url(env('FRONT_URL') . 'course/' . $course->slug)
                ->provider(Schema::organization()->name("LearnVern")->url(env('FRONT_URL')));
            if ((isset($course->comment_count) && ($course->comment_count > 0))) {
                $productSchema = $productSchema->aggregateRating(Schema::aggregateRating()
                    ->ratingValue($course->average_rating)
                    ->reviewCount($course->comment_count)
                );
            }
            $reviews = $course->relatedComments->where('is_default_testimonial', 1)->take(16);
            if (count($reviews) > 0) {
                foreach ($reviews as $review) {
                    $reviewData[] = Schema::review()
                        ->reviewRating(Schema::rating()->ratingValue($review->rating))
                        ->reviewBody($review->content)
                        ->author(Schema::person()->name($review->author));
                }
                $productSchema = $productSchema->review($reviewData);
            }
            $productSchema = $productSchema->toScript();
            /*COURSE SCHEMA END*/

            /*FAQS SCHEMA*/
            $faqsSchema = "";
            if (count($course->relatedFaqs) > 0) {
                $faqsSchema = Schema::fAQPage();
                foreach ($course->relatedFaqs as $faq) {
                    $faqMinEntityData[] = Schema::question()->name($faq['question'])->acceptedAnswer(Schema::answer()->text($faq['answer']));
                }
                if (!empty($faqsSchema)) {
                    $faqsSchema->mainEntity($faqMinEntityData);
                }
                $faqsSchema = $faqsSchema->toScript();
            }
            /*FAQS SCHEMA END*/

            /*VIDEO OBJECT SCHEMA*/
            $videoObjectSchema = Schema::videoObject()
                ->name($course->name)
                ->description($course->brief_description ?? $course->name)
                ->thumbnailUrl($course->course_icon)
                ->uploadDate(isset($course->created_at) ? formatDate($course->created_at, 'Y-m-d h:i:s') : formatDate($course->updated_at, 'Y-m-d h:i:s'))
                ->if(isset($course->intro_video), function (VideoObject $schema) use ($course) {
                    $schema->contentUrl(env('FRONT_URL') . 'course/' . $course->slug)
                        ->embedUrl($course->intro_video_embed);
                })->if(isset($course->intro_youtube_video), function (VideoObject $schema) use ($course) {
                    $schema->contentUrl(env('FRONT_URL') . 'course/' . $course->slug)
                        ->embedUrl($course->intro_youtube_video_embed);
                })->toScript();
            /*VIDEO OBJECT SCHEMA END*/

            /*BREADCRUMBS SCHEMA*/
            $breadcrumbsSchema = Schema::breadcrumbList()
                ->itemListElement([
                    Schema::listItem()->position(1)->item(Schema::thing()->identifier(env('FRONT_URL'))->name('Home')),
                    Schema::listItem()->position(2)->item(Schema::thing()->identifier(env('FRONT_URL') . 'course/' . $course->slug)->name($course->name))
                ])->toScript();
            $schemaScript = $webPageSchema . '
        ' . $productSchema . '
        ' . $faqsSchema . '
        ' . $videoObjectSchema . '
        ' . $breadcrumbsSchema;
            $course->update(['schema_script' => $schemaScript]);
        }
        $this->info("Course Schema Updated successfully.");
    }
}
