<?php

namespace App\Console\Commands;

use App\Models\Assignment;
use App\Models\Units;
use Illuminate\Console\Command;
use Spatie\SchemaOrg\Schema;
use Spatie\SchemaOrg\VideoObject;

class UpdateUnitAssignmentTime extends Command
{
    protected $signature = 'update:video_time';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $units = Units::whereNotNull('video_id')->where('time', "00:00:00")->get();
        foreach ($units as $unit){
            $video_url = "https://player.vimeo.com/video/" . $unit->video_id;
            $time = getVimeoVideoDuration($video_url);
            $unit->update(['time' => $time]);
        }
        $assignments = Assignment::whereNotNull('video_id')->where('time', "00:00:00")->get();
        foreach ($assignments as $assignment){
            $video_url = "https://player.vimeo.com/video/" . $assignment->video_id;
            $time = getVimeoVideoDuration($video_url);
            $assignment->update(['time' => $time]);
        }
        $this->info("Time Updated successfully.");
    }
}
