<?php

namespace App\Console\Commands;

use PDF;
use Illuminate\Console\Command;
use App\Services\SendDailyReportService;

class SendDailyReportCommand extends Command
{
    protected $signature = 'send:daily_report_mail';
    protected $sendDailyReportService;

    public function __construct(SendDailyReportService $sendDailyReportService)
    {
        parent::__construct();
        $this->sendDailyReportService = $sendDailyReportService;
    }

    public function handle()
    {
        $this->sendDailyReportService->sendReportMail();
        $this->info("Daily report mail sent successfully.");
    }
}
