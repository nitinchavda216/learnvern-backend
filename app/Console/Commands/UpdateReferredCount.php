<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\UserAmbassadorBadges;
use Illuminate\Console\Command;

class UpdateReferredCount extends Command
{
    protected $signature = 'update:referred_count';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $ids = User::whereNotNull('referral')->distinct('referral')->pluck('referral')->toArray();
        $users = User::whereIn('own_referral_code', $ids)->select('id', 'own_referral_code', 'referral', 'referred_count')->get();
        foreach ($users as $user){
            $user->update(['referred_count' => count($user->myReferrals)]);
            $this->info("User $user->id updated.");
        }
        $this->info("All data updated successfully.");
    }
}
