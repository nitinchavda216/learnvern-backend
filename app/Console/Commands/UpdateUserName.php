<?php

namespace App\Console\Commands;

use App\Models\Units;
use App\Models\User;
use Illuminate\Console\Command;
use Spatie\SchemaOrg\Schema;
use Spatie\SchemaOrg\VideoObject;

class UpdateUserName extends Command
{
    protected $signature = 'update:username';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $users = User::select('id', 'username', 'first_name', 'email')->where('username', 'Like', '%@%')->orderBy('id')->get();
        foreach ($users as $user) {
            $array = explode('@', $user->username);
            if (isset($array[0])) {
                $username = str_replace('.', '-', $array[0]);
                $existUser = User::where('username', $username)->first();
                if (isset($existUser)) {
                    $user->update(['username' => $username . '-' . time()]);
                } else {
                    $user->update(['username' => $username]);
                }
            }
        }
        $this->info("Username Updated successfully");
    }
}
