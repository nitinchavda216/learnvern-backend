<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class SaveReferralLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:referral-logs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::select('id','is_active','created_at','referral')->whereNotNull('referral')->get();

        foreach ($users as $user)
        {
            $affiliate_user=User::select('id', 'referral')->where('own_referral_code',$user->referral)->first();
            if($affiliate_user)
            {
                DB::table('referral_logs')->insert([
                    'user_id' => $user->id,
                    'affiliate_user_id' => $affiliate_user->id,
                    'is_active' => $user->is_active,
                    'created_at' => $user->created_at,
                ]);
            }
        }
    }
}
