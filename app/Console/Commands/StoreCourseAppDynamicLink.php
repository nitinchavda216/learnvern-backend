<?php

namespace App\Console\Commands;

use App\Models\Course;
use Illuminate\Console\Command;

class StoreCourseAppDynamicLink extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:app-dynamic-link';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $courses = Course::select('id', 'slug', 'course_type', 'app_dynamic_link')->get();
        foreach ($courses as $course)
        {
            $key = env('FCM_API_KEY');
            $url = 'https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=' . $key;
            $data = [
                'dynamicLinkInfo' => [
                    'domainUriPrefix' => env('FCM_DYNAMIC_LINK_DOMAIN'),
                    'link' => env('FCM_DYNAMIC_LINK_DOMAIN').'?type='.(($course->course_type == 4) ? 'webinar' : 'course').'&id='.$course->id,
                    'androidInfo' => [
                        'androidPackageName' => 'com.learnvernurvam',
                        "androidFallbackLink" => env('FRONT_URL').'course/'.$course->slug
                    ]
                ],
                "suffix" => [
                    "option" => "UNGUESSABLE"
                ]
            ];
            $headers = array('Content-Type: application/json');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            $data = curl_exec($ch);
            curl_close($ch);
            $short_url = json_decode($data);
            if (isset($short_url->shortLink)) {
                $course->update(['app_dynamic_link' => $short_url->shortLink]);
            }
        }
    }
}
