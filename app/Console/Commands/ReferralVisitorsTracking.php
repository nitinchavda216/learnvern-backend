<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Interfaces\AnalyticsRepositoryInterface;
use Spatie\Analytics\AnalyticsFacade as Analytics;
use Spatie\Analytics\Period;
use Illuminate\Http\Request;

class ReferralVisitorsTracking extends Command
{
    protected $signature = 'referral:visiting-tracking';

    protected $analyticsRepository;

    public function __construct(AnalyticsRepositoryInterface $analyticsRepository)
    {
        parent::__construct();
        $this->analyticsRepository = $analyticsRepository;
    }

    public function handle()
    {
        $this->analyticsRepository->getAnalyticsReferralDataByDate();
    }
}
