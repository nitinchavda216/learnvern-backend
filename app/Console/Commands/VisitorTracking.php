<?php

namespace App\Console\Commands;

use App\Interfaces\AnalyticsRepositoryInterface;
use Illuminate\Console\Command;

class VisitorTracking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'visitor:track';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch tracking from google analytics api';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $analyticsRepository;

    public function __construct(AnalyticsRepositoryInterface $analyticsRepository)
    {
        parent::__construct();
        $this->analyticsRepository = $analyticsRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->analyticsRepository->getHistoricalAnalyticsReferralData();
        $this->info('Fetching data successfully.');
    }
}
