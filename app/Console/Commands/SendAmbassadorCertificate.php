<?php

namespace App\Console\Commands;

use App\Events\SendAmbassadorMailEvent;
use App\Models\AmbassadorProgramSettings;
use App\Models\User;
use Illuminate\Console\Command;
use PDF;

class SendAmbassadorCertificate extends Command
{
    protected $signature = 'send:ambassador_certificate';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $users = User::where('referred_count', '>=', 25)->select('id', 'first_name', 'email', 'referral', 'own_referral_code', 'referred_count',
            'ambassador_email_send_flag', 'ambassador_50_email_send_flag', 'created_at')->get()->toArray();
        foreach ($users as $user){
            if (($user->referred_count >= 25) && ($user->ambassador_email_send_flag == 0)){
                $referral_count = 25;
                $user['referred_count'] = $referral_count;
                $ambassador_program_setting = AmbassadorProgramSettings::where('referrals_count', $referral_count)->first();
                $user['badge_title'] = $ambassador_program_setting->title;
                $path = storage_path('app/public/ambassador_certificates/');
                $fileName = "Learnvern_Recommendation_Letterhead_".$user['id']."_".$referral_count.".pdf";
                $html = view('ambassador_pdf_view', compact('user', 'referral_count'))->render();
                $dompdf = PDF::loadHtml($html);
                $dompdf->save($path . '/' . $fileName);
                event(new SendAmbassadorMailEvent($user));
                $user->ambassador_email_send_flag = 1;
                $user->save();
            }
            if (($user->referred_count >= 50) && ($user->ambassador_50_email_send_flag == 0)){
                $referral_count = 50;
                $user['referred_count'] = $referral_count;
                $ambassador_program_setting = AmbassadorProgramSettings::where('referrals_count', $referral_count)->first();
                $user['badge_title'] = $ambassador_program_setting->title;
                $path = storage_path('app/public/ambassador_certificates/');
                $fileName = "Learnvern_Recommendation_Letterhead_".$user['id']."_".$referral_count.".pdf";
                $html = view('ambassador_pdf_view', compact('user', 'referral_count'))->render();
                $dompdf = PDF::loadHtml($html);
                $dompdf->save($path . '/' . $fileName);
                event(new SendAmbassadorMailEvent($user));
                $user->ambassador_50_email_send_flag = 1;
                $user->save();
            }
        }
    }
}
