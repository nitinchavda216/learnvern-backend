<?php

namespace App\Console\Commands;

use App\Models\CourseUser;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class UpdateCourseUserDuplicateEntries extends Command
{
    protected $signature = 'update:course_users_data';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        /*$duplicate_records = DB::select('SELECT course_id,course_user.user_id,COUNT(*) AS duplicate_count FROM course_user
               GROUP BY course_id, user_id HAVING duplicate_count > 1');
        foreach ($duplicate_records as $duplicate_record){
            $course_users = CourseUser::where('course_id', $duplicate_record->course_id)->where('user_id', $duplicate_record->user_id)->where('paid_status', 0)->orderBy('progress', 'DESC')->get();
            if (count($course_users) > 1){
                $is_from_nsdc = 0;
                $nsdc_course_id = null;
                $deleted_ids = [];
                foreach ($course_users as $key => $course_user){
                    if (($course_user->is_from_nsdc == 1) && ($is_from_nsdc == 0)){
                        $is_from_nsdc = 1;
                        $nsdc_course_id = $course_user->nsdc_course_id;
                    }
                    if ($key > 0){
                        $deleted_ids[] = $course_user->id;
                    }
                }
                $correctData = $course_users[0];
                if (($is_from_nsdc == 1) && ($correctData->is_from_nsdc == 0)){
                    $correctData->update(['nsdc_course_id' => $nsdc_course_id, 'is_from_nsdc' => 1]);
                }
                CourseUser::whereIn('id', $deleted_ids)->forceDelete();
                $logEbayURL = new Logger('Duplicate Data');
                $logEbayURL->pushHandler(new StreamHandler(storage_path('logs/course_users.log')), Logger::INFO);
                $logEbayURL->info('Course User Info!', ['course_id' => $duplicate_record->course_id, 'user_id' => $duplicate_record->user_id, 'correct_id' => $correctData->id, 'deleted_course_users_ids' => implode(',', $deleted_ids)]);
            }
        }*/
        $nsdc_course_id = 45;
        $course_id = 5520;
        $courseUsers = CourseUser::where('course_id', $nsdc_course_id)->get();
        foreach ($courseUsers as $courseUser){
            $recordExist = CourseUser::where('user_id', $courseUser->user_id)->where('course_id', $course_id)->first();
            if (isset($recordExist)){
                $recordExist->update(['is_from_nsdc' => 1, 'nsdc_course_id' => $courseUser->nsdc_course_id]);
                $courseUser->forceDelete();
            }else{
                $courseUser->update(['course_id' => $course_id]);
            }
        }
        $this->info("Duplicate course users entries cleared successfully.");
    }
}
