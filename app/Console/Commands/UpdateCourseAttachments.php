<?php

namespace App\Console\Commands;

use App\Models\Assignment;
use App\Models\CourseAttachment;
use App\Models\Units;
use Illuminate\Console\Command;
use File;

class UpdateCourseAttachments extends Command
{
    protected $signature = 'update:course_attachments';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $units = Units::whereNotNull('attachment')->get();
        foreach ($units as $unit) {
            $oldFilePath = storage_path('app/public/attachments/').$unit->attachment;
            if (file_exists($oldFilePath)){
                $nameArray = explode('.', $unit->attachment);
                $fileName = str_replace('-', '_', $unit->slug).'.'.$nameArray[1];
                $newPath = storage_path('app/public/attachments/').$fileName;
                if (File::copy($oldFilePath , $newPath)) {
                    CourseAttachment::create([
                       'curriculum_id' => $unit->id,
                       'curriculum_type' => 'unit',
                       'title' => $unit->name,
                       'attachment' => $fileName,
                    ]);
                    @unlink($oldFilePath);
                }
                $unit->update(['attachment' => $fileName]);
            }
        }
        $this->info("Unit Attachment Updated.");
        $assignments = Assignment::whereNotNull('attachment')->get();
        foreach ($assignments as $assignment) {
            $oldFilePath = storage_path('app/public/attachments/').$assignment->attachment;
            if (file_exists($oldFilePath)){
                $nameArray = explode('.', $assignment->attachment);
                $fileName = str_replace('-', '_', $assignment->slug).'.'.$nameArray[1];
                $newPath = storage_path('app/public/attachments/').$fileName;
                if (File::copy($oldFilePath , $newPath)) {
                    CourseAttachment::create([
                        'curriculum_id' => $assignment->id,
                        'curriculum_type' => 'assignment',
                        'title' => $assignment->name,
                        'attachment' => $fileName,
                    ]);
                    @unlink($oldFilePath);
                }
                $assignment->update(['attachment' => $fileName]);
            }
        }
        $this->info("Assignment Attachment Updated.");
    }
}
