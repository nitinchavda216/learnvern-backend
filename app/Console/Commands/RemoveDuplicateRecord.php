<?php

namespace App\Console\Commands;

use App\Models\Units;
use App\Models\User;
use Illuminate\Console\Command;
use Spatie\SchemaOrg\Schema;
use Spatie\SchemaOrg\VideoObject;
use Illuminate\Support\Facades\DB;

class RemoveDuplicateRecord extends Command
{
    protected $signature = 'remove:duplicate-record';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $users=DB::select("SELECT id,email,COUNT(*) AS duplicate_count,MAX(created_at) as latest_date FROM users
                    WHERE is_active=0
                    GROUP BY email HAVING duplicate_count > 1");

        foreach ($users as $user)
        {
            $users=User::where('email',$user->email)->where('is_active',0)->where('id','!=',$user->id)->forceDelete();
        }

        $this->info("Duplicate Users Delete successfully.");
    }
}
