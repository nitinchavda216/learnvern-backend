<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => ['validateApiToken']], function () {

    Route::get('latest-version', 'Api\ConfigurationController@getCurrentVersionDetails');
    Route::get('configurations', 'Api\ConfigurationController@index');
    Route::post('app-installation-detail/store', 'Api\ConfigurationController@storeAppInstallationDetails');
    Route::get('get-form-details', 'Api\ConfigurationController@getFormDetails');
    Route::post('sign-up', 'Api\UserAuthController@signUp');
    Route::get('page/{page_type}', 'Api\ConfigurationController@getPageUrl');
    Route::post('login', 'Api\UserAuthController@authenticate');
    Route::post('forgot-password', 'Api\UserAuthController@sendForgotPasswordLink');

    Route::post('social/auth', 'Api\UserAuthController@socialAuth');

    Route::group(['middleware' => ['auth_token']], function () {
        Route::get('home', 'Api\HomeController@index');
        Route::post('referral-info', 'Api\ReferralController@getReferralInfo');
        Route::post('courses_by_category', 'Api\CourseApiController@getCoursesByCategory');
        Route::post('course_details', 'Api\CourseApiController@courseDetails');
        Route::post('unit_details', 'Api\CourseApiController@getUnitDetails');

        Route::post('state', 'Api\ConfigurationController@stateDetails');
        Route::post('city-district', 'Api\ConfigurationController@cityDetails');

        Route::post('get-location-details', 'Api\UserAuthController@getUserLocationDetails');
        Route::post('course_enroll', 'Api\CourseApiController@courseEnroll')->middleware(['requiredApiAuthUrl']);

        Route::post('generate-payment-id', 'Api\PaymentController@generatePaymentId')->middleware(['requiredApiAuthUrl']);
        Route::post('do-payment', 'Api\PaymentController@doPayment')->middleware(['requiredApiAuthUrl']);
        Route::post('payment/abandoned', 'Api\PaymentController@abandonedPayment')->middleware(['requiredApiAuthUrl']);

        Route::post('video-progress', 'Api\CourseApiController@videoProgress')->middleware(['requiredApiAuthUrl']);
        Route::post('mark_unit_complete', 'Api\CourseApiController@markUnitAsCompleted')->middleware(['requiredApiAuthUrl']);
        Route::post('review-rating', 'Api\CourseApiController@courseReviewRating')->middleware(['requiredApiAuthUrl']);

        Route::post('quiz-details', 'Api\QuizController@quizDetails');
        Route::post('submit-quiz', 'Api\QuizController@submitQuiz')->middleware(['requiredApiAuthUrl']);
        Route::post('get-quiz-result', 'Api\QuizController@getQuizResults')->middleware(['requiredApiAuthUrl']);
        Route::post('my-quiz-results', 'Api\QuizController@userQuizResultCourseList')->middleware(['requiredApiAuthUrl']);
        Route::post('quiz-results-by-course', 'Api\QuizController@userQuizResultsByCourse')->middleware(['requiredApiAuthUrl']);

        Route::post('my-courses', 'Api\CourseApiController@getMyCourses')->middleware(['requiredApiAuthUrl']);

        Route::post('update_profile', 'Api\UserAuthController@updateProfile')->middleware(['requiredApiAuthUrl']);

        Route::post('certificates-list', 'Api\CertificateController@getCertificates')->middleware(['requiredApiAuthUrl']);
        Route::post('download-certificate', 'Api\CertificateController@downloadCertificate')->middleware(['requiredApiAuthUrl']);
        Route::post('verify-certificate', 'Api\CertificateController@verifyCertificate');

        Route::post('contact-form-submission', 'Api\HomeController@storeContactRequest');

        Route::get('my-profile', 'Api\UserAuthController@myProfile')->middleware(['requiredApiAuthUrl']);

        Route::post('notifications', 'Api\NotificationController@getUserNotificationList')->middleware(['requiredApiAuthUrl']);
        Route::post('notification-read', 'Api\NotificationController@readNotification')->middleware(['requiredApiAuthUrl']);
        Route::post('notification-delete', 'Api\NotificationController@deleteNotification')->middleware(['requiredApiAuthUrl']);

        Route::post('otp-send', 'Api\NotificationController@sendOtp')->middleware(['requiredApiAuthUrl']);
        Route::post('update/phone-number', 'Api\UserAuthController@updateVerifiedMobileNumber')->middleware(['requiredApiAuthUrl']);

        Route::post('city_details', 'Api\LocationController@getCityDetails');
        Route::post('send-offline-data', 'Api\CourseApiController@storeOfflineProgressData')->middleware(['requiredApiAuthUrl']);
    });
});
