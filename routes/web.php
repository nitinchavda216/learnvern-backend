<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('user/activation/{token}', 'UsersController@activateUser')->name('activation-process');
Route::get('razorpay/payment/captured', 'PaymentController@paymentCaptured');
Route::get('app-view/page-detail/{page}', 'Api\ConfigurationController@getPageAppView')->name('app-view.page-details');
Route::get('flush-all-cache', 'DashboardController@flushAllCache');
Route::any('facebook-user/deleted', 'UsersController@facebookUserDeleted');
Route::post('mautic/webhook', 'MautikApiController@initWebhook');

Route::group(['middleware' => 'web'], function () {
    Route::get('/login', 'LoginController@index')->name('login');
    Route::get('migration/slug', 'MigrationController@slugMigrationAssignmentQuiz');
    Route::get('analytics/referral-visitors-migrate', 'AnalyticsController@referralVisitors')->name('referral_visitors');

    Route::post('login/authenticate', 'LoginController@authenticateAdmin')->name('admin.authenticate');
    Route::get('/logout', 'LoginController@logout')->name('logout');

    Route::get('admin-user/add-password/{token}', 'AdminUserController@setPasswordLink')->name('admin.set_password');
    Route::post('admin-user/add-password', 'AdminUserController@resetPassword')->name('admin-user.password.store');

    Route::get('profile', 'ProfileController@getProfile')->name('profile');
    Route::post('profile/update', 'ProfileController@updateProfile')->name('profile.update');

    Route::get('payment/success', 'PaymentController@doPayment')->name('payment_success');

    //forgotPassword
    Route::get('password/forgot', 'ForgotPasswordController@forgotPassword')->name('forgot_password');
    Route::post('password/forgot', 'ForgotPasswordController@sendPasswordResetInstructions')->name('sent_reset_password_link');
    Route::get('password/reset/{token}', 'ForgotPasswordController@resetPasswordLink')->name('reset_password');
    Route::post('password/reset', 'ForgotPasswordController@resetPassword')->name('reset_admin_password');

    Route::post('send-payment-link', ['uses' => 'PaymentController@sendPaymentLink']);

    Route::group(['middleware' => ['admins']], function () {
        Route::group(['middleware' => ['roleAccess']], function () {
            Route::get('/', 'DashboardController@index')->name('dashboard');
            Route::get('crm/user-performance', 'CrmController@userPerformance')->name('crm.user_performance');

            Route::get('mautic/contact-push', 'MautikApiController@index');

            Route::get('/getDashboardData', 'DashboardController@getAjaxData')->name('dashboard.getAjaxData');
            Route::get('/crm/dashboard', 'CrmController@index')->name('crm.dashboard');
            Route::get('/crm/campaign-report', 'CrmController@campaignReport')->name('crm.campaignReport');
            Route::get('/crm/getCampaignReportForCrm', 'CrmController@getCampaignReportForCrm')->name('crm.getCampaignReportForCrm');
            Route::get('/crm/getAjaxListCampaignData', 'CrmController@getAjaxListCampaignData')->name('crm.getAjaxListCampaignData');
            Route::get('/crm/calling/{id}/{assignTo?}', 'CrmController@calling')->name('crm.calling');
            Route::get('/crm/getAjaxListData', 'CrmController@getAjaxListData')->name('crm.getAjaxListData');
            Route::get('/crm/getAjaxCallingData', 'CrmController@getAjaxCallingData')->name('crm.getAjaxCallingData');
            Route::post('/crm/save-calling-detail', 'CrmController@saveCallingDetail')->name('crm.saveCallingDetail');
            Route::get('/crm/pending', 'CrmController@pending')->name('crm.pending');
            Route::get('/crm/getPendingAjaxCallingData', 'CrmController@getPendingAjaxCallingData')->name('crm.getPendingAjaxCallingData');
            Route::get('/crm/followup-statistics-referral', 'CrmController@followUpStatisticsReferral')->name('crm.followUpStatisticsReferral');
            Route::get('/crm/getFollowUpStatisticsReferral', 'CrmController@getFollowUpStatisticsReferral')->name('crm.getFollowUpStatisticsReferral');
            Route::get('/crm/followup-statistics-revenue', 'CrmController@followUpStatisticsRevenue')->name('crm.followUpStatisticsRevenue');
            Route::get('/crm/getFollowUpStatisticsRevenue', 'CrmController@getFollowUpStatisticsRevenue')->name('crm.getFollowUpStatisticsRevenue');

            Route::get('/crm/user-performance', 'CrmController@crmUserList')->name('crm.user_performance');
            Route::get('/crm/getCrmUserList', 'CrmController@getCrmUserList')->name('crm.getCrmUserList');
            Route::get('/crm/user-performance/{id}', 'CrmController@userPerformance')->name('crm.userPerformance');
            Route::get('/crm/campaign-user/{id}','CrmController@getCampaignUserDetail')->name('crm.getCampaignUserDetail');
            Route::get('/crm/getAjaxCampaignUserDetail','CrmController@getAjaxCampaignUserDetail')->name('crm.getAjaxCampaignUserDetail');

            // Admin Users
            Route::get('admin-users', 'AdminUserController@index')->name('admin_users');
            Route::get('admin-user/create', 'AdminUserController@createNewAdmin')->name('admin_users.create');
            Route::post('admin-user/store', 'AdminUserController@store')->name('admin_users.store');
            Route::get('admin-user/delete/{id}', 'AdminUserController@delete')->name('admin_users.delete');
            Route::get('admin-user/edit/{id}', 'AdminUserController@edit')->name('admin_users.edit');
            Route::post('admin-user/update/{id}', 'AdminUserController@update')->name('admin_users.update');
            //category
            Route::get('categories', 'CategoryController@index')->name('categories');
            Route::post('/category/store', 'CategoryController@store')->name('categories.store');
            Route::get('/category/edit/{id}', 'CategoryController@edit')->name('categories.edit');
            Route::post('/category/update/{id}', 'CategoryController@update')->name('categories.update');
            Route::post('/category/update_status', 'CategoryController@updateStatus')->name('categories.updateStatus');
            Route::get('category/sort_order', 'CategoryController@getSortOrder')->name('categories.get_sort_order');
            Route::post('/category/sortOrder/update', 'CategoryController@updateSortOrder')->name('categories.updateSortOrder');

            //webinars
            Route::get('webinars', 'WebinarsController@index')->name('webinars');
            Route::get('webinars/getAjaxListData', 'WebinarsController@getAjaxListData')->name('webinars.getAjaxListData');
            Route::get('webinars/create', 'WebinarsController@create')->name('webinars.create');
            Route::post('webinars/store', 'WebinarsController@store')->name('webinars.store');
            Route::get('webinars/edit/{id}', 'WebinarsController@edit')->name('webinars.edit');
            Route::post('webinars/update/{id}', 'WebinarsController@update')->name('webinars.update');
            Route::post('webinars/update_status', 'WebinarsController@updateStatus')->name('webinars.updateStatus');

            //Activity Log
            Route::get('activity-logs', 'ActivityLogsController@index')->name('activitylogs');
            Route::get('activity-logs/getAjaxListData', 'ActivityLogsController@getAjaxListData')->name('activitylogs.getAjaxListData');
            Route::post('get_activity_changes', 'ActivityLogsController@get_activity_changes')->name('activitylogs.get_activity_changes');

            //Anonymous User
            Route::get('anonymous-user', 'AnonymousUserController@index')->name('anonymous-user');
            Route::get('anonymousUser/getAjaxListData', 'AnonymousUserController@getAjaxListData')->name('AnonymousUser.getAjaxListData');
            Route::get('anonymousUser/delete', 'AnonymousUserController@getAjaxListData')->name('anonymousUser.delete');

            //courses
            Route::get('courses', 'CourseController@index')->name('courses');
            Route::get('courses/getAjaxListData', 'CourseController@getAjaxListData')->name('courses.getAjaxListData');
            Route::get('course/create', 'CourseController@create')->name('courses.create');
            Route::post('course/store', 'CourseController@store')->name('courses.store');
            Route::get('course/edit/{id}', 'CourseController@edit')->name('courses.edit');
            Route::get('course/activity-log/{id}', 'CourseController@showActivityLog')->name('courses.activity');
            Route::post('course/update/{id}', 'CourseController@update')->name('courses.update');
            Route::post('course/update_status', 'CourseController@updateStatus')->name('courses.updateStatus');
            Route::post('courses/slug-update', 'CourseController@updateCourseSlug')->name('courses.slug-update');
            Route::post('course/price/update', 'CourseController@updateCoursePrice')->name('course.price.update');
            Route::get('course/curriculum/{id}', 'CourseController@getCurriculum')->name('courses.get_curriculum');
            Route::get('course/career-plan-curriculum/{id}', 'CourseController@getCareerPlanCurriculum')->name('courses.career_plan_curriculum');
            Route::post('course/career-plan/store', 'CourseController@storeCareerPlanCurriculum')->name('courses.career_plan.store');
            Route::post('course/curriculum/update', 'CourseController@updateCurriculum')->name('courses.updateCurriculum');
            Route::post('course/curriculum/delete', 'CourseController@deleteCurriculum')->name('courses.deleteCurriculum');
            Route::post('course/curriculum/master-section/update', 'CourseController@editMasterSection')->name('courses.masterSectionUpdate');
            Route::post('/course/sortOrder/update', 'CourseController@updateSortOrder')->name('courses.updateSortOrder');

            Route::post('curriculum/hide_from_backend/update', 'CourseController@updateHideFromBackendStatus')->name('courses.updateHideFromBackendStatus');

            //Course FAQS
            Route::get('faqs', 'FaqsController@index')->name('faqs');
            Route::get('faqs/getAjaxListData', 'FaqsController@getAjaxListData')->name('faqs.getAjaxListData');
            Route::get('faq/create', 'FaqsController@create')->name('faqs.create');
            Route::post('faq/store', 'FaqsController@store')->name('faqs.store');
            Route::get('faq/edit/{id}', 'FaqsController@edit')->name('faqs.edit');
            Route::post('faq/update/{id}', 'FaqsController@update')->name('faqs.update');
            Route::post('faq/update_status', 'FaqsController@updateStatus')->name('faqs.updateStatus');
            Route::get('get-question-by-course{course_id}', 'FaqsController@getFaqsByCourse')->name('faqs.getFaqsByCourse');
            Route::get('faq/delete/{id}', 'FaqsController@delete')->name('faqs.delete');

            //sections
            Route::get('sections', 'SectionController@index')->name('sections');
            Route::get('sections/getAjaxListData', 'SectionController@getAjaxListData')->name('sections.getAjaxListData');
            Route::get('sections/create', 'SectionController@create')->name('sections.create');
            Route::post('sections/get-parent-courses', 'SectionController@getParentSections')->name('sections.getParentSections');
            Route::post('sections/store', 'SectionController@store')->name('sections.store');
            Route::get('sections/edit/{id}', 'SectionController@edit')->name('sections.edit');
            Route::post('sections/update/{id}', 'SectionController@update')->name('sections.update');
            Route::post('sections/update_status', 'SectionController@updateStatus')->name('sections.updateStatus');
            Route::get('get-sections-by-course', 'SectionController@getSectionsByCourse')->name('sections.getSectionsByCourse');
            Route::post('sections/export', 'SectionController@exportSectionData')->name('sections.export');
            Route::post('sections/import', 'SectionController@sectionImport')->name('sections.import');
            //units
            Route::get('units', 'UnitController@index')->name('units');
            Route::get('unit/getAjaxListData', 'UnitController@getAjaxListData')->name('units.getAjaxListData');
            Route::get('units/create', 'UnitController@create')->name('units.create');
            Route::post('units/store', 'UnitController@store')->name('units.store');
            Route::get('units/edit/{id}', 'UnitController@edit')->name('units.edit');
            Route::post('units/update/{id}', 'UnitController@update')->name('units.update');
            Route::post('units/slug-update', 'UnitController@updateSlug')->name('units.slug-update');
            Route::post('unit/update_status', 'UnitController@updateStatus')->name('units.updateStatus');
            Route::post('unit/export', 'UnitController@exportUnitData')->name('units.export');
            Route::post('unit/import', 'UnitController@importUpdateUnits')->name('units.import');
            Route::post('unit/sample-export', 'UnitController@exportSampleUnitExcel')->name('units.sample-export');
            Route::post('unit/import-new', 'UnitController@importNewUnits')->name('units.import-new');
            Route::get('units/faqs/delete/{id}', 'UnitController@deleteUnitFaq')->name('units.faqs.delete');
            Route::post('slugExists', 'UnitController@checkSlugExists')->name('units.slug.exists');

            //Quiz
            Route::get('quiz', 'QuizController@index')->name('quiz');
            Route::get('quiz/getAjaxListData', 'QuizController@getAjaxListData')->name('quiz.getAjaxListData');
            Route::get('quiz/create', 'QuizController@create')->name('quiz.create');
            Route::post('quiz/store', 'QuizController@store')->name('quiz.store');
            Route::get('quiz/edit/{id}', 'QuizController@edit')->name('quiz.edit');
            Route::post('quiz/update/{id}', 'QuizController@update')->name('quiz.update');
            Route::post('quiz/update_status', 'QuizController@updateStatus')->name('quiz.updateStatus');
            Route::post('quiz/export', 'QuizController@exportQuizData')->name('quiz.export');
            Route::post('quiz/import', 'QuizController@quizImport')->name('quiz.import');

            Route::get('quiz/question/create/{id}', 'QuizController@getQuestionListByQuiz')->name('quiz.questions');
            Route::get('quiz/question/download-sample/{id}', 'QuizController@downloadQuestionSampleFile')->name('quiz.questions.export');
            Route::post('quiz/question/import', 'QuizController@importQuestions')->name('quiz.questions.import');
            Route::post('quiz/question/store', 'QuizController@storeQuizQuestion')->name('quiz.questions.store');
            Route::get('quiz/question/edit/{id}', 'QuizController@editQuizQuestion')->name('quiz.questions.edit');
            Route::post('quiz/question/update/{id}', 'QuizController@updateQuizQuestion')->name('quiz.questions.update');
            Route::post('quiz/question/update_status', 'QuizController@updateQuestionStatus')->name('quiz.questions.updateQuestionStatus');

            //Assignment
            Route::get('assignments', 'AssignmentsController@index')->name('assignments');
            Route::get('assignments/getAjaxListData', 'AssignmentsController@getAjaxListData')->name('assignments.getAjaxListData');
            Route::get('assignments/create', 'AssignmentsController@create')->name('assignments.create');
            Route::post('assignments/store', 'AssignmentsController@store')->name('assignments.store');
            Route::get('assignments/edit/{id}', 'AssignmentsController@edit')->name('assignments.edit');
            Route::post('assignments/update/{id}', 'AssignmentsController@update')->name('assignments.update');
            Route::post('assignment/slug-update', 'AssignmentsController@updateSlug')->name('assignment.slug-update');
            Route::post('assignments/update_status', 'AssignmentsController@updateStatus')->name('assignments.updateStatus');
            Route::post('assignments/export', 'AssignmentsController@exportAssignmentData')->name('assignments.export');
            Route::post('assignments/import', 'AssignmentsController@assignmentImport')->name('assignments.import');

            //Review
            Route::get('reviews', 'ReviewController@index')->name('reviews');
            Route::get('reviews/getAjaxListData', 'ReviewController@getAjaxListData')->name('reviews.getAjaxListData');
            Route::get('reviews/create', 'ReviewController@create')->name('reviews.create');
            Route::post('reviews/store', 'ReviewController@store')->name('reviews.store');
            Route::get('reviews/edit/{id}', 'ReviewController@edit')->name('reviews.edit');
            Route::post('reviews/update/{id}', 'ReviewController@update')->name('reviews.update');
            Route::post('reviews/update_status', 'ReviewController@updateStatus')->name('reviews.updateStatus');

            //user
            Route::get('users', 'UsersController@index')->name('users');
            Route::get('users/getAjaxListData', 'UsersController@getAjaxListData')->name('users.getAjaxListData');
            Route::post('users/update_status', 'UsersController@updateStatus')->name('users.updateStatus');
            Route::get('get-user-by-register"', 'UsersController@getUserByRegister')->name('users.getUserByRegister');
            Route::get('users/detail/{id}', 'UsersController@getUserDetails')->name('users.details');
            Route::post('users/update/{id}', 'UsersController@update')->name('users.update');
            Route::get('get-courses', 'UsersController@getCourse')->name('users.getCourse');

            Route::get('/get-states-by-country', 'LocationController@getStatesByCountry')->name('users.getStatesByCountry');
            Route::get('/get-cities-by-state', 'LocationController@getCitiesByState')->name('users.getCitiesByState');
            Route::get('/get-district-by-state', 'LocationController@getDistrictByState')->name('users.getDistrictByState');
            Route::post('/certificate-download', 'CertificateController@downloadCertificate')->name('certificate.download');

            //NSDC candidates
            Route::get('nsdc-candidates', 'UsersController@getNSDCCandidatesList')->name('nsdc_candidate');
            Route::get('nsdc-candidates/getAjaxData', 'UsersController@getNSDCCandidatesListAjaxData')->name('nsdc_candidate.getNSDCCandidatesListAjaxData');
            Route::get('nsdc-candidates/getNSDCExportData', 'UsersController@getNSDCExportData')->name('nsdc_candidate.getNSDCExportData');
            Route::post('nsdc-candidates/getNSDCUploadData', 'UsersController@getNSDCUploadData')->name('nsdc_candidate.getNSDCUploadData');
            Route::post('nsdc-candidates/changeDates', 'UsersController@updateDate')->name('nsdc_candidate.updateDate');

            //International candidates
            Route::get('international-certificates', 'UsersController@getInternationalCandidatesList')->name('international_candidates');
            Route::get('international-certificates/getAjaxData', 'UsersController@getInternationalCandidatesLisAjaxData')->name('international_candidate.getInternationalCandidatesLisAjaxDatat');
            Route::get('international-certificates/getInternationalExportData', 'UsersController@getInternationalExportData')->name('international_candidate.getInternationalExportData');
            Route::post('convert-to-course', 'UsersController@convertInternationalToNSDC')->name('international_candidate.convertInternationalToNSDC');

            //payments
            Route::get('payment-history', 'PaymentController@index')->name('payment_history');
            Route::get('payment-history/getAjaxData', 'PaymentController@getAjaxData')->name('payment_history.getAjaxData');

            //Referral Points
            Route::get('ambassador-report', 'ReferralPointsController@index')->name('referral-points');
            Route::get('send_ambassador_mail', 'ReferralPointsController@sendAmbassadorMail')->name('referral-points.send_ambassador_mail');
            Route::get('ambassador-report/getAjaxData', 'ReferralPointsController@getAjaxListData')->name('referral-points.getAjaxListData');
            Route::get('ambassador-report/export', 'ReferralPointsController@exportData')->name('referral-points.exportData');
            Route::get('download/ambassador-certificate/{user_id}/{count}', 'UsersController@downloadAmbassadorCertificate')->name('referral-points.download-ambassador-certificate');

            //Pages
            Route::get('widget-setting', 'WidgetsSettingsController@index')->name('widget-setting');
            Route::get('widget-setting/create', 'WidgetsSettingsController@create')->name('widget-setting.create');
            Route::post('widget-setting/store', 'WidgetsSettingsController@store')->name('widget-setting.store');
            Route::get('widget-setting/edit/{id}', 'WidgetsSettingsController@edit')->name('widget-setting.edit');
            Route::post('widget-setting/update/{id}', 'WidgetsSettingsController@update')->name('widget-setting.update');
            Route::post('widget-setting/update_status', 'WidgetsSettingsController@updateStatus')->name('widget-setting.updateStatus');

            Route::get('pages', 'PagesController@index')->name('pages');
            Route::get('pages/create', 'PagesController@create')->name('pages.create');
            Route::post('pages/store', 'PagesController@store')->name('pages.store');
            Route::get('pages/edit/{id}', 'PagesController@edit')->name('pages.edit');
            Route::post('pages/update/{id}', 'PagesController@update')->name('pages.update');
            Route::post('pages/update_status', 'PagesController@updateStatus')->name('pages.updateStatus');

            //Location Pages
            Route::get('location-pages', 'LocationPagesController@index')->name('location-pages');
            Route::post('location-pages/update_status', 'LocationPagesController@updateStatus')->name('location-pages.updateStatus');
            Route::post('location-pages/store', 'LocationPagesController@store')->name('location-pages.store');
            Route::post('location-pages/get-course-data', 'LocationPagesController@getCourseDetails')->name('location-pages.getCourseDetails');
            Route::get('location-pages/edit/{id}', 'LocationPagesController@edit')->name('location-pages.edit');
            Route::post('location-pages/update/{id}', 'LocationPagesController@update')->name('location-pages.update');
            Route::get('location-pages/create', 'LocationPagesController@create')->name('location-pages.create');

            //Landing Pages
            Route::get('landing-pages', 'LandingPagesController@index')->name('landing-pages');
            Route::get('landing-pages/getAjaxListData', 'LandingPagesController@getAjaxListData')->name('landing-pages.getAjaxListData');
            Route::get('landing-pages/getCourseDetails/{id}', 'LandingPagesController@getCourseDetails')->name('landing-pages.getCourseDetails');
            Route::get('landing-pages/create', 'LandingPagesController@create')->name('landing-pages.create');
            Route::post('landing-pages/store', 'LandingPagesController@store')->name('landing-pages.store');
            Route::get('landing-pages/edit/{id}', 'LandingPagesController@edit')->name('landing-pages.edit');
            Route::post('landing-pages/update/{id}', 'LandingPagesController@update')->name('landing-pages.update');
            Route::get('landing-pages/delete/{id}', 'LandingPagesController@delete')->name('landing-pages.delete');

            //Course Keywords
            Route::get('search-keywords', 'CourseKeywordsController@index')->name('search-keywords');
            Route::get('search-keywords/edit/{id}', 'CourseKeywordsController@edit')->name('search-keywords.edit');
            Route::post('search-keywords/import', 'CourseKeywordsController@courseKeywordsImport')->name('search-keywords.import');
            Route::post('search-keywords/update/{id}', 'CourseKeywordsController@update')->name('search-keywords.update');
            Route::get('search-keywords/getAjaxExportData', 'CourseKeywordsController@getKeywordExportData')->name('search-keywords.export');
            Route::get('missing-search-keywords', 'CourseKeywordsController@missingCourseKeywordsList')->name('missing-search-keywords');
            Route::get('missing-search-keywords/getListData', 'CourseKeywordsController@getMissingCourseKeywordsListData')->name('missing-search-keywords.getListData');
            Route::post('missing-search-keywords/update', 'CourseKeywordsController@missingCourseKeywordsupdate')->name('missing-search-keywords.update');
            Route::get('missing-search-keywords/destroy/{id}', 'CourseKeywordsController@missingCourseKeywordsdestroy')->name('missing-search-keywords.destroy');
            Route::get('missing-keyword-history/getListData', 'CourseKeywordsController@missingKeywordHistoryListData')->name('missing-keyword-history.getListData');
            Route::get('missing-keyword-history/{id}', 'CourseKeywordsController@missingKeywordHistoryList')->name('missing-keyword-history.index');
            Route::get('keyword-search-analytics', 'CourseKeywordsController@keywordSearchAnalyticsIndex')->name('keyword-search-analytics');
            Route::get('keyword-search-analytics/getListData', 'CourseKeywordsController@keywordSearchAnalyticsList')->name('keyword-search-analytics.getListData');
            Route::get('keyword-search-analytics/getAjaxExportData', 'CourseKeywordsController@getAjaxExportData')->name('keyword-search-analytics.getAjaxExportData');

            //Configuration
            Route::get('configuration', 'SiteConfigurationsController@index')->name('configuration');
            Route::post('configuration/update', 'SiteConfigurationsController@update')->name('configuration.update');
            Route::get('sitemap/update/success', 'SiteConfigurationsController@updateSiteMapSuccess')->name('configuration.updateSiteMapSuccess');

            //Configuration
            Route::get('app-updates', 'AppUpdateController@index')->name('app_updates');
            Route::get('app-update/create', 'AppUpdateController@create')->name('app_updates.create');
            Route::post('app-update/store', 'AppUpdateController@store')->name('app_updates.store');
            Route::get('app-update/view/{id}', 'AppUpdateController@view')->name('app_updates.view');

            //Email Templates
            Route::get('email-templates', 'EmailTemplatesController@index')->name('email-templates');
            Route::get('email-templates/create', 'EmailTemplatesController@create')->name('email-templates.create');
            Route::post('email-templates/store', 'EmailTemplatesController@store')->name('email-templates.store');
            Route::get('email-templates/edit/{id}', 'EmailTemplatesController@edit')->name('email-templates.edit');
            Route::post('email-templates/update/{id}', 'EmailTemplatesController@update')->name('email-templates.update');

            //Event Designer
            Route::get('event-designer', 'EventDesignerController@index')->name('event-designer');

            //Languages
            Route::get('languages', 'LanguageController@index')->name('languages');
            Route::get('languages/edit{id}', 'LanguageController@edit')->name('languages.edit');
            Route::post('languages/store', 'LanguageController@store')->name('languages.store');
            Route::post('languages/update/{id}', 'LanguageController@update')->name('languages.update');

            //User Report
            Route::get('user-report', 'UsersReportController@index')->name('user-report');
            Route::get('user-report/getAjaxListData', 'UsersReportController@getAjaxListData')->name('user-report.getAjaxListData');
            Route::get('user-report/getAjaxExportData', 'UsersReportController@getAjaxExportData')->name('user-report.getAjaxExportData');
            Route::get('/user-search', 'UsersController@search')->name('users-search');

            //User Report
            Route::get('un-enrolled-users', 'UsersReportController@getUnUnrolledUsers')->name('un-enrolled-users');
            Route::get('un-enrolled-users/getAjaxListData', 'UsersReportController@getUnUnrolledUsersAjaxList')->name('un-enrolled-users.getUnUnrolledUsersAjaxList');
            Route::get('un-enrolled-users/export', 'UsersReportController@exportUnEnrolledUsersList')->name('un-enrolled-users.exportUnEnrolledUsersList');

            //course Report
            Route::get('course-report', 'CourseReportController@index')->name('course-report');
            Route::get('course-report/getAjaxListData', 'CourseReportController@getAjaxListData')->name('course-report.getAjaxListData');
            Route::get('course-report/getAjaxExportData', 'CourseReportController@getAjaxExportData')->name('course-report.getAjaxExportData');

            //Referral Report
            Route::get('referral-report', 'UsersReportController@getReferralReport')->name('referral-report');
            Route::get('referral-report/getAjaxListData', 'UsersReportController@getReferralReportAjaxListData')->name('referral-report.getReferralReportAjaxListData');
            Route::get('referral-report/export', 'UsersReportController@exportReferralReportData')->name('referral-report.exportReferralReportData');

            //Course Status Report
            Route::get('course-status-report', 'CourseStatusController@index')->name('course-status-report');
            Route::get('course-status-report/getAjaxListData', 'CourseStatusController@getAjaxListData')->name('course-status-report.getAjaxListData');
            Route::get('course-status-report/export', 'CourseStatusController@exportList')->name('course-status-report.exportList');
            Route::get('course-status-report/certificate-purchase-users', 'CourseStatusController@getCertificatePurchaseUsers')->name('course-status-report.getCertificatePurchaseUsers');
            Route::get('course-status-report/certificate-purchase-users/getAjaxData', 'CourseStatusController@getCertificatePurchaseUsersAjaxData')->name('course-status-report.getCertificatePurchaseUsersAjaxData');

            //User Month Vise Report
            Route::get('user-month-vise-report', 'UsersReportController@userMonthReport')->name('user-month-vise-report');

            //Abandoned Payments Report
            Route::get('abandoned-payments', 'AbandonedPaymentsController@index')->name('abandoned-payments');
            Route::get('abandoned-payments/getAjaxData', 'AbandonedPaymentsController@getAbandonedPaymentsAjaxData')->name('abandoned-payments.getAbandonedPaymentsAjaxData');
            Route::get('abandoned-payments/getAjaxExportData', 'AbandonedPaymentsController@getAjaxExportData')->name('abandoned-payments.getAjaxExportData');

            //Analytics Report
            Route::get('analytics', 'AnalyticsController@index')->name('analytics');

            Route::get('ambassador-program-settings', 'AmbassadorProgramSettingsController@index')->name('ambassador-program-settings');
            Route::get('ambassador-program-settings/create', 'AmbassadorProgramSettingsController@create')->name('ambassador-program-settings.create');
            Route::post('ambassador-program-settings/store', 'AmbassadorProgramSettingsController@store')->name('ambassador-program-settings.store');
            Route::get('ambassador-program-settings/edit/{id}', 'AmbassadorProgramSettingsController@edit')->name('ambassador-program-settings.edit');
            Route::post('ambassador-program-settings/update/{id}', 'AmbassadorProgramSettingsController@update')->name('ambassador-program-settings.update');
            Route::post('ambassador-program-settings/update_status', 'AmbassadorProgramSettingsController@updateStatus')->name('ambassador-program-settings.updateStatus');

            //Report Builder
            Route::get('report-builder', 'ReportBuilderController@index')->name('report-builder');
            Route::get('report-builder/create', 'ReportBuilderController@create')->name('report-builder.create');
            Route::post('report-builder/store', 'ReportBuilderController@store')->name('report-builder.store');
            Route::get('report-builder/edit/{id}', 'ReportBuilderController@edit')->name('report-builder.edit');
            Route::post('report-builder/update/{id}', 'ReportBuilderController@update')->name('report-builder.update');
            Route::get('report-builder/destroy/{id}', 'ReportBuilderController@destroy')->name('report-builder.destroy');
            Route::get('report-builder/view-report/{id}', 'ReportBuilderController@viewReport')->name('report-builder.viewreport');
            Route::get('report-builder/viewReportAjaxData', 'ReportBuilderController@viewReportAjaxData')->name('report-builder.viewReportAjaxData');
            Route::post('report-builder/store/new', 'ReportBuilderController@storeNew')->name('report-builder.store_new');
            Route::get('report-builder/export-report/{id}', 'ReportBuilderController@export')->name('report-builder.export');
            Route::post('report-builder/get-condition-form', 'ReportBuilderController@getConditionForm')->name('report-builder.get_condition_form');

            // AdminUser ROle
            Route::get('admin-user-role', 'AdminUserRoleController@index')->name('admin_role');
            Route::get('admin-user-role/create', 'AdminUserRoleController@create')->name('admin_role.create');
            Route::post('admin-user-role/store', 'AdminUserRoleController@store')->name('admin_role.store');
            Route::get('admin-user-role/edit/{id}', 'AdminUserRoleController@edit')->name('admin_role.edit');
            Route::post('admin-user-role/update/{id}', 'AdminUserRoleController@update')->name('admin_role.update');

            // Resume Template
            Route::get('resume-templates', 'ResumeTemplateController@index')->name('resume_templates');
            Route::get('resume-templates/create', 'ResumeTemplateController@create')->name('resume_templates.create');
            Route::post('resume-templates/store', 'ResumeTemplateController@store')->name('resume_templates.store');
            Route::get('resume-templates/edit/{id}', 'ResumeTemplateController@edit')->name('resume_templates.edit');
            Route::post('resume-templates/update/{id}', 'ResumeTemplateController@update')->name('resume_templates.update');
            Route::get('resume-templates/delete/{id}', 'ResumeTemplateController@delete')->name('resume_templates.delete');

            //Do Manual Payment
            Route::post('do_manual_payment', 'CourseController@doManualPayment')->name('do-manual-payment');

            //Campaign

            Route::get('campaign', 'CampaignController@index')->name('campaign');
            Route::get('campaign/edit/{id}', 'CampaignController@edit')->name('campaign.edit');
            Route::post('campaign/update/{id}', 'CampaignController@update')->name('campaign.update');
            Route::post('campaign/store', 'CampaignController@store')->name('campaign.store');
            Route::post('campaign/verifyUser', 'CampaignController@verifyUser')->name('campaign.verifyUser');
            Route::post('campaign/verifyUserForUpdate', 'CampaignController@verifyUserForUpdate')->name('campaign.verifyUserForUpdate');
            Route::post('campaign/storesubcampaign', 'CampaignController@storesubcampaign')->name('campaign.storesubcampaign');
            Route::get('campaign/destroy/{id}', 'CampaignController@destroy')->name('campaign.destroy');
            Route::get('campaign/view-report/{id}', 'CampaignController@viewReport')->name('campaign.viewReport');
            Route::get('campaign/viewReportAjaxData', 'CampaignController@viewReportAjaxData')->name('campaign.viewReportAjaxData');
            Route::get('campaign/export/{id}', 'CampaignController@exportReport')->name('campaign.export');
            Route::get('campaign/markascomplete/{id}', 'CampaignController@markascomplete')->name('campaign.markascomplete');
            Route::get('campaign/calling/{id}', 'CampaignController@calling')->name('campaign.calling');
            Route::get('campaign/getAjaxListData', 'CampaignController@getAjaxListData')->name('campaign.getAjaxListData');
            Route::get('campaign/getAjaxCallingData', 'CampaignController@getAjaxCallingData')->name('campaign.getAjaxCallingData');
            Route::post('campaign/save-calling-detail', 'CampaignController@saveCallingDetail')->name('campaign.saveCallingDetail');

            // Tenancy Related Settings
            Route::get('tenancy-settings', 'TenancySettingsController@index')->name('tenancy-settings');
            Route::post('tenancy-settings/store', 'TenancySettingsController@store')->name('tenancy-settings.store');

            //Partner Application Request
            Route::get('partners', 'PartnerApplicationRequestController@index')->name('partner-application-request');
            Route::get('partners/getAjaxData', 'PartnerApplicationRequestController@getAjaxData')->name('partner_application_history.getAjaxData');
            Route::get('partners/status/{id}/{status}', 'PartnerApplicationRequestController@updateApplicationStatus')->name('partner_application_history.status');
            Route::get('partners/show/{id}', 'PartnerApplicationRequestController@showApplicationStatus')->name('partner_application_history.show');
            Route::post('partners/partnerType', 'PartnerApplicationRequestController@storePartnerType')->name('partner_application_history.partner_type');
            Route::post('partners/approve', 'PartnerApplicationRequestController@approve')->name('approve_partner_application');
            Route::post('partners/application-update/{id}', 'PartnerApplicationRequestController@update')->name('update_partner_application');

            //Partner Application Request
            Route::get('banner', 'BannerController@index')->name('banner');
            Route::get('banner/getAjaxListData', 'BannerController@getAjaxListData')->name('banner.getAjaxListData');
            Route::get('banner/create', 'BannerController@create')->name('banner.create');
            Route::post('banner/store', 'BannerController@store')->name('banner.store');
            Route::get('banner/edit/{id}', 'BannerController@edit')->name('banner.edit');
            Route::post('banner/update/{id}', 'BannerController@update')->name('banner.update');
            Route::get('banner/delete/{id}', 'BannerController@destroy')->name('banner.delete');

            //Announcement Management
            Route::get('announcement', 'AnnouncementController@index')->name('announcement-management');
            Route::get('announcement/getAjaxListData', 'AnnouncementController@getAjaxListData')->name('announcement.getAjaxListData');
            Route::get('announcement/create', 'AnnouncementController@create')->name('announcement.create');
            Route::post('announcement/store', 'AnnouncementController@store')->name('announcement.store');
            Route::get('announcement/edit/{id}', 'AnnouncementController@edit')->name('announcement.edit');
            Route::post('announcement/update/{id}', 'AnnouncementController@update')->name('announcement.update');
            Route::post('announcement/update_status', 'AnnouncementController@updateStatus')->name('announcement.updateStatus');
            Route::get('announcement/delete/{id}', 'AnnouncementController@delete')->name('announcement.delete');

            //Anonymous user
            Route::get('anonymous-user', 'AnonymousUserController@index')->name('anonymous-user');

            //App Notification
            Route::get('app-notification', 'AppNotificationController@index')->name('app-notification');
        });
    });
});
