$(document).ready(function () {
    $('#date_range').daterangepicker({
        autoUpdateInput: true,
        startDate: moment().subtract(6, 'days'),
        endDate: moment(),
        locale: {
            cancelLabel: 'Clear',
            format: 'DD-MM-YYYY'
        },
        ranges: {
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });

    $('#date_range').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
    });

    $('#date_range').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });

    if ($('#dataTableList').length) {
        var $dataTableList = $('#dataTableList').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            bFilter: false,
            "aaSorting": [],
            ajax: {
                url: $app_url + '/course-status-report/getAjaxListData',
                data: function (d) {
                    d.date_range = $('#date_range').val();
                    d.partner_user_id = $('#partner_user_id').val();
                    d.course_id = $('#course_id').val();
                }
            },
            "columns": [
                {data: 'course_name', 'sortable': false, 'width': "10%"},
                {data: '0-20', 'sortable': false, 'width': "5%"},
                {data: '21-40', 'sortable': false, 'width': "5%"},
                {data: '41-60', 'sortable': false, 'width': "5%"},
                {data: '61-80', 'sortable': false, 'width': "5%"},
                {data: '81-99', 'sortable': false, 'width': "5%"},
                {data: 'completed', 'sortable': false, 'width': "5%"},
                {data: 'total_user_count', 'sortable': false, 'width': "10%"},
                {data: 'total_sell', 'sortable': false, 'width': "10%"}
            ]
        });

        $('#search-form').on('submit', function (e) {
            $dataTableList.draw();
            e.preventDefault();
        });

        $('#reset-filters').on('click', function () {
            $("#course_id").val(null).trigger('change');
            $("#date_range").val(null).trigger('change');
            $("#partner_user_id").val(1).trigger('change');
            $dataTableList.draw();
        });
    }
});
