$(document).ready(function () {
    $('#register_date').daterangepicker({
        autoUpdateInput: true,
        startDate: moment().subtract(6, 'days'),
        endDate: moment(),
        locale: {
            cancelLabel: 'Clear',
            format: 'DD-MM-YYYY'
        },
        ranges: {
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });

    $('#register_date').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
    });

    $('#register_date').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    if ($('#dataTableList').length) {
        var $dataTableList = $('#dataTableList').DataTable({
            processing: true,
            serverSide: true,
            responsive: false,
            bFilter: false,
            "aaSorting": [],
            ajax: {
                url: $app_url + '/course-report/getAjaxListData',
                data: function (d) {
                    d.register_date = $('#register_date').val();
                    d.search = $('#search').val();
                    d.course_id = $('#course_id').val();
                    d.course_status = $('#course_status').val();
                    d.partner_user_id = $('#partner_user_id').val();
                }
            },
            "columns": [
                {data: 'course_name', 'sortable': false},
                {data: 'user_details', 'sortable': false},
                {data: 'course_enroll_date', 'sortable': false},
                {data: 'course_status', 'sortable': false},
                {data: 'certificate_type', 'sortable': false},
                {data: 'certificate_date', 'sortable': false},
                {data: 'last_used_date', 'sortable': false},
                {data: 'actions', 'sortable': false}
            ],
            select: true,
            bStateSave: true,
            fnStateSave: function (settings, data) {
                localStorage.setItem("dataTables_state", JSON.stringify(data));
            },
            fnStateLoad: function (settings) {
                return JSON.parse(localStorage.getItem("dataTables_state"));
            }
        });
        $('#search-form').on('submit', function (e) {
            $dataTableList.draw();
            e.preventDefault();
        });
        $('#reset-filters').on('click', function () {
            $('#search').val('').trigger('change');
            $("#register_date").val(null).trigger('change');
            $("#course_id").val(null).trigger('change');
            $('#course_status').val('').trigger('change');
            $('#partner_user_id').val(1).trigger('change');
            $dataTableList.draw();
        });
    }

    $(document).on('click', '.sendPaymentLink', function () {
        var self = $(this);
        var id = self.data('id');
        $.ajax({
            type: "post",
            dataType: "json",
            url: $app_url + '/send-payment-link',
            data: {'course_user_id': id},
            beforeSend: function () {
                $.blockUI({
                    message: '<h1>Please wait..</h1>',
                    timeout: 2000
                });
            },
            success: function (data) {
                if (data.status){
                    self.addClass('btn-primary').removeClass('btn-info');
                    self.text('Resend Payment Link');
                    toastr.success(data.message, "Success!", {timeOut: 2000});
                }else{
                    toastr.error(data.message, "Error!", {timeOut: 2000});
                }
                $.unblockUI();
            }
        });
    })
});
