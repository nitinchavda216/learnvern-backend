$(document).ready(function () {

    var date = "";
    getAllData(date);
    $("#submit").click(function () {
        var date = $('#datepicker').val();
        getAllData(date);
    });
    function getAllData(date) {
        $.ajax({
            cache: false,
            url: $app_url + '/user-month-vise-report/getAllData',
            data: {date: date},
            dataType: "json",
            success: function (response) {
                $("#chartBar1").empty();
                chart(response);
                // You will get response from your PHP page (what you echo or print)
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }


    function chart(data) {
        var data = {
            labels: data.month,
            datasets: [{
                backgroundColor: "#332E5F",
                borderColor: "",
                borderWidth: 2,
                data: data.counts
            }],

        };
        var options = {
            legend: {
                display: false
            },
            maintainAspectRatio: false,
            tooltips: {enabled: true},
            hover: {mode: null},

        };
        Chart.Bar('chartBar1', {
            options: options,
            data: data
        });
    }

});
