$(document).ready(function () {
    $('#register_date').daterangepicker({
        autoUpdateInput: true,
        startDate: moment().subtract(6, 'days'),
        endDate: moment(),
        locale: {
            cancelLabel: 'Clear',
            format: 'DD-MM-YYYY'
        },
        ranges: {
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });

    $('#register_date').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
    });

    $('#register_date').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });


    $('#last_login_date').daterangepicker({
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        },
        ranges: {
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });

    $('#last_login_date').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
    });

    $('#last_login_date').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });


    if ($('#dataTableList').length) {

        var $dataTableList = $('#dataTableList').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            bFilter: false,
            "autoWidth": false,
            "order": [[ 3, 'desc' ]],
            ajax: {
                url: $app_url + '/user-report/getAjaxListData',
                data: function (d) {
                    d.register_date = $('#register_date').val();
                    d.last_login_date = $('#last_login_date').val();
                    d.searchData = $('#searchData').val();
                    d.sign_up = $('#sign_up').val();
                    d.partner_user_id = $('#partner_user_id').val();
                    d.user_type = $('#user_type').val();
                }
            },
            "columns": [
                {data: 'email', 'sortable': false, 'width': "15%"},
                {data: 'sign_up_from', name: 'signup_from', 'sortable': true, 'width': "5%"},
                {data: 'own_referral_code', 'sortable': false, 'width': "5%"},
                {data: 'referral', 'sortable': false, 'width': "5%"},
                {data: 'registration_date',name:'created_at', 'sortable': true, 'width': "5%"},
                {data: 'is_active',name:'is_active', 'sortable': true, 'width': "5%"},
                {data: 'last_login_date',name:'last_login_date', 'sortable': true, 'width': "10%"},
                {data: 'enrolled_courses', 'sortable': false, 'width': "5%"},
                {data: 'completed_courses', 'sortable': false, 'width': "5%"},
                {data: 'total_refer', 'sortable': false, 'width': "5%"},
                {data: 'paid_certificate', 'sortable': false, 'width': "5%"},
                {data: 'free_certificate', 'sortable': false, 'width': "5%"},
                {data: 'course', 'sortable': false, 'width': "15%"}
            ],
            select: true,
            drawCallback: function (settings) {
                $('body').tooltip({selector: '[data-toggle="tooltip"]'});
            }
        });


        $('#search-form').on('submit', function (e) {
            $dataTableList.draw();
            e.preventDefault();
        });

        $('#reset-filters').on('click', function () {
            $("#searchData").val(null).trigger('change');
            $("#register_date").val(null).trigger('change');
            $("#last_login_date").val(null).trigger('change');
            $("#sign_up").val(null).trigger('change');
            $("#partner_user_id").val(1).trigger('change');
            $("#user_type").val(0).trigger('change');
            $dataTableList.draw();
        });

    }

});
