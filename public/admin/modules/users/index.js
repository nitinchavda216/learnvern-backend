$(document).ready(function () {
    $('#date_range').daterangepicker({
        autoUpdateInput: true,
        startDate: moment().subtract(6, 'days'),
        endDate: moment(),
        locale: {
            cancelLabel: 'Clear',
            format: 'DD-MM-YYYY'
        },
        ranges: {
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });

    $('#date_range').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
    });

    $('#date_range').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });


    if ($('#dataTableList').length) {
        var $dataTableList = $('#dataTableList').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            bFilter: false,
            "aaSorting": [],
            pageLength : 50,
            ajax: {
                url: $app_url + '/users/getAjaxListData',
                data: function (d) {
                    d.date_range = $('#date_range').val();
                    d.search = $('#search').val();
                    d.partner_user_id = $('#partner_user_id').val();
                }
            },
            "columns": [
                {data: 'first_name', 'sortable': true, 'width': "15%"},
                {data: 'username', 'sortable': false, 'width': "15%"},
                {data: 'email', 'sortable': false, 'width': "15%"},
                {data: 'mobile_number', 'sortable': false, 'width': "10%"},
                {data: 'own_referral_code', 'sortable': false, 'width': "10%"},
                {data: 'email_verify', 'sortable': false, 'width': "10%"},
                {data: 'action', 'orderable': false, 'width': "20%"}
            ],
            select: true,
            bStateSave: true,
            fnStateSave: function (settings, data) {
                localStorage.setItem("dataTables_state", JSON.stringify(data));
            },
            fnStateLoad: function (settings) {
                return JSON.parse(localStorage.getItem("dataTables_state"));
            }
        });

        $('#search-form').on('submit', function (e) {
            $dataTableList.draw();
            e.preventDefault();
        });

        $('#reset-filters').on('click', function () {
            $('#search').val('').trigger('change');
            $("#date_range").val(null).trigger('change');
            $("#partner_user_id").val(1).trigger('change');
            $dataTableList.draw();
        });

    }


    // Toggle Switches
    $(document).on('click', '.az-toggle', function () {
        var status = ($(this).toggleClass('on').hasClass('on') == true) ? 0 : 1;
        var id = $(this).data('id');
        $.ajax({
            type: "post",
            dataType: "json",
            url: $app_url + '/users/update_status',
            data: {'is_active': status, 'id': id},
            success: function () {
                toastr.success("Status Updated Successfully!", "Success!", {timeOut: 2000});
            }
        });
    });
});
