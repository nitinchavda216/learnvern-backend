$(document).ready(function () {
    $('#wizard2').steps({
        headerTag: 'h3',
        bodyTag: 'section',
        autoFocus: true,
        showFinishButtonAlways: is_edit,
        enableAllSteps: is_edit,
        titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
        onStepChanging: function (event, currentIndex, newIndex) {
            if (newIndex < currentIndex) {
                return true;
            }
            var isFormValid = $("#courseForm").isValid();
            return isFormValid;
        }
    });

    $(document).on('click', "a[href='#finish']", function () {
        $("#courseForm").submit();
    });
    if (!is_edit) {
        $("#course_name").focusout(function (i, d) {
            var title = $.trim($("#course_name").val());
            if (title.length > 0) {
                title = title.toLowerCase();
                title = title.replace(/[^a-z0-9\s]/gi, "").replace(/  +/g, " ").replace(/[_\s]/g, "-");
            }
            $("#slug").val(title);
            $("#canonical_url").val("https://www.learnvern.com/course/"+title);
        });
    }
    /* SET SLUG CONDITIONS */
    $("#slug").focusout(function (e) {
        var slug = $(this).val().toLowerCase();
        $(this).val(slug);
        $("#canonical_url").val("https://www.learnvern.com/course/"+slug);
    });

    $("#editSlugInput").focusout(function (e) {
        var slug = $(this).val().toLowerCase();
        $(this).val(slug);
    });

    $(".slugInput").keypress(function (e) {
        var regex = new RegExp("^[A-Za-z0-9-]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();
        return false;
    });

    $(document).on('click', '#updateSlugBtn', function (){
        var isFromValid = $("#updateSlugForm").isValid();
        if (isFromValid){
            var formdata = $("#updateSlugForm").serialize();
            $.ajax({
                type: "post",
                dataType: "json",
                url: $app_url + '/courses/slug-update',
                data: formdata,
                beforeSend: function () {
                    $("#updateSlugModal").modal('hide');
                    $.blockUI({
                        message: '<h1>Please wait..</h1>'
                    });
                },
                success: function (data) {
                    if (data.status){
                        $("#slug").val(data.slug);
                        $("#canonical_url").val("https://www.learnvern.com/course/"+data.slug);
                        toastr.success(data.message, "Success!", {timeOut: 2000});
                    }else{
                        $("#updateSlugModal").modal('show');
                        toastr.error(data.message, "Error!", {timeOut: 2000});
                    }
                    $.unblockUI();
                }
            });
        }
    })

    /* SELECT2 INTEGRATION*/
    $('.select2Input').select2({
        placeholder: ''
    });
    if ($(".badgeId").length > 0) {
        $('.badgeId').select2({
            minimumResultsForSearch: Infinity
        });
    }
    // Color picker
    if($('#colorpicker').length > 0){
        $('#colorpicker').spectrum({
            preferredFormat: "hex",
            showInput: true
        });
    }

    $('.form_datetime').datetimepicker({
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1
    });
    /* SELECT2 INTEGRATION*/
    $('.course_image').dropify();

    /* HTML EDITOR INTEGRATION*/
    $('.html_editor').summernote({
        tabsize: 4,
        height: 250
    });

    if($("#tiny_description").length > 0)
    {
        $("#tinyDescriptionCount").html($("#tiny_description").val().length);
        $("#tiny_description").on("keyup", function () {
            checkTextAreaMaxLength(this,  '#tinyDescriptionCount');
        });
    }

    if($("#brief_description").length > 0){
        $("#briefDescriptionCount").html($("#brief_description").val().length);
        $("#brief_description").on("keyup", function () {
            checkTextAreaMaxLength(this,  '#briefDescriptionCount');
        });
    }

    if($("#course_certificate_name").length > 0)
    {
        $("#courseCertificateNameCount").html($("#course_certificate_name").val().length);

        $("#course_certificate_name").on("keyup", function () {
            checkTextAreaMaxLength(this,  '#courseCertificateNameCount');
        });
    }
});


/*
 Checks the MaxLength of the Textarea
 -----------------------------------------------------
 @prerequisite:	textBox = textarea dom element
 e = textarea event
 length = Max length of characters
 */
function checkTextAreaMaxLength(textBox, counterBox) {

    var maxLength = parseInt($(textBox).data("maxallowed"));

    $(counterBox).html($(textBox).val().length);
    if ($(textBox).val().length > maxLength) {
        $(textBox).css('border', "red solid 1px");
    }else{
        $(textBox).css('border', "");
    }

    return true;
}
/*
 Checks if the keyCode pressed is inside special chars
 -------------------------------------------------------
 @prerequisite:	e = e.keyCode object for the key pressed
 */
function checkSpecialKeys(e) {
    if (e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40)
        return false;
    else
        return true;
}
