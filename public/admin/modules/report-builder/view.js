$(document).ready(function () {
    if ($('#dataTableList').length) {
            var $dataTableList = $('#dataTableList').DataTable({
            processing: true,
            serverSide: true,
            responsive: false,
            bFilter: false,
            "aaSorting": [],
            "scrollX": true,
            ajax: {
                url: $app_url + '/report-builder/viewReportAjaxData',
                data: function (d) {
                    d.report_id = report_id;
                    d.conditions = $("#conditionInput").val();
                    d.fields = $("#fieldsInput").val();
                }
            },
            "columns": arr,
                "initComplete": function( settings ) {
                    $('.dataTables_scrollBody thead tr').css({visibility:'collapse'});
                    $('.dataTables_scrollHeadInner thead tr:first th:first').removeClass('sorting_asc');
                },
                "drawCallback": function( settings ) {
                    $("#total_record_count").val(settings._iRecordsTotal);
                    $('.dataTables_scrollBody thead tr').css({visibility:'collapse'});
                    $('.dataTables_scrollHeadInner thead tr:first th:first').removeClass('sorting_asc');
                },

        });

        $("#dataTableList").on('draw.dt', function () {
            var info = $dataTableList.page.info();
            if (checkAllPagesCheckboxes){
                $('.user_id').attr('checked', true);
                for (var i = 0; i < unselect_users.length; i++) {
                    checkboxId = unselect_users[i];
                    $('#' + checkboxId).attr('checked', false);
                }
            }else{
                for (var i = 0; i < users.length; i++) {
                    checkboxId = users[i];
                    $('#' + checkboxId).attr('checked', true);
                }
                if (jQuery.inArray(info.page, allCheckboxesList) != -1) {
                    $("#checkAll").prop('checked', true);

                    $('.user_id').each(function () {
                        $(this).prop('checked', true);
                        var id = $(this).val();
                        if (jQuery.inArray(id, users) == -1) {
                            users.push(id);
                        }
                    });
                    $('[name=course_user_ids]').val(JSON.stringify(users));
                } else {
                    if (!checkAllPagesCheckboxes){
                        $("#checkAll").prop('checked', false);
                    }
                }
            }
        });
    }

    $(document).on('click', '#openCampaignModal', function () {
        if (checkAllPagesCheckboxes || (users.length > 0)){
            // if(checkAllPagesCheckboxes){
            //    var totalUserCount = $("#total_record_count").val();
            //    var unselect_course_user_ids =  $("#unselect_course_user_ids").val();
            //    if(unselect_course_user_ids != ''){
            //        unselect_course_user_ids = JSON.parse(unselect_course_user_ids);
            //        totalUserCount = totalUserCount - unselect_course_user_ids.length;
            //    }
            //    $("#number_of_user_campaign").val(totalUserCount);
            //    $("#number_of_user_campaign_span").html(totalUserCount);
            // }
            // else{
            //
            //     $("#number_of_user_campaign").val(users.length);
            //     $("#number_of_user_campaign_span").html(users.length);
            // }
            $("#addToCampaignModal").modal('show');
        }else{
            toastr.error("Select at least one checkbox!", "Error!", {timeOut: 2000});
        }
    });

    $(document).on('click', '#checkAll', function () {
        var info = $dataTableList.page.info();
        var self = $(this);
        if (info.recordsDisplay > 0) {
            if (self.is(':checked')) {
                $("#checkAll").prop('checked', false);
                $("#checkAllConfirmationModal").modal('show');
            } else {
                $('input[type="checkbox"]').each(function () {
                    var current_user = $(this).prop('checked', false);
                    users = jQuery.grep(users, function (value) {
                        return value != current_user.val();
                    });
                });
                if (jQuery.inArray(info.page, allCheckboxesList) != -1) {
                    allCheckboxesList = jQuery.grep(allCheckboxesList, function (value) {
                        return value != info.page;
                    });
                }
                checkAllPagesCheckboxes = false;
                $("#checkAllUsers").val(checkAllPagesCheckboxes);
            }
            $('[name=course_user_ids]').val(JSON.stringify(users));
        }else{
            $("#checkAll").prop('checked', false);
        }
    });

    $(document).on('click', '.user_id', function () {
        var info = $dataTableList.page.info();
        var self = $(this);
        if (checkAllPagesCheckboxes){
            if (self.is(':checked')) {
                unselect_users = jQuery.grep(unselect_users, function (value) {
                    return value != self.val();
                });
            } else {
                if (jQuery.inArray(self.val(), unselect_users) == -1) {
                    unselect_users.push(self.val());
                }
            }
        }else{
            if (self.is(':checked')) {
                if (jQuery.inArray(self.val(), users) == -1) {
                    users.push(self.val());
                }
            } else {
                users = jQuery.grep(users, function (value) {
                    return value != self.val();
                });
                $("#checkAll").prop('checked', false);
                checkAllPagesCheckboxes = false;
                $("#checkAllUsers").val(checkAllPagesCheckboxes);
                if (jQuery.inArray(info.page, allCheckboxesList) != -1) {
                    allCheckboxesList = jQuery.grep(allCheckboxesList, function (value) {
                        return value != info.page;
                    });
                }
            }
        }
        $('[name=course_user_ids]').val(JSON.stringify(users));
        $('[name=unselect_course_user_ids]').val(JSON.stringify(unselect_users));
    });

    $(document).on('click', '#submitCheckboxEvent', function () {
        $("#checkAllConfirmationModal").modal('hide');
        var info = $dataTableList.page.info();
        var val = $("input[name=check_page]:checked").val();
        $("#checkAll").prop('checked', true);
        if (val == "one") {
            users = [];
            checkAllPagesCheckboxes = false;
            allCheckboxesList.push(info.page);
        } else {
            checkAllPagesCheckboxes = true;
            $("#checkAllUsers").val(checkAllPagesCheckboxes);
            $("#checkAll").prop('disabled', true);
        }
        $('.user_id').each(function () {
            var current_user = $(this).prop('checked', true);
            if (jQuery.inArray($(current_user).val(), users) == -1) {
                users.push($(current_user).val());
            }
        });
        $('[name=course_user_ids]').val(JSON.stringify(users));
    });

    $(document).on('change', '#state_id', function () {
        $("#city_id").val("").trigger('change');
    });

    $('.select2Modal').select2({
        placeholder: '--SELECT--',
        dropdownParent: $("#addToCampaignModal")
    });


    $(document).on('click', "input[name='action']", function () {
        if($(this).val() == "calling-user"){


            if($('input[name="chunk_type"]:checked').val() == 0){
                $("#singleChunck").show();
                $("#multipleChunck").hide();
                $('#pushNotification').hide();
            }else{
                $("#singleChunck").hide();
                $("#multipleChunck").show();
            }


            $.ajax({
                type: "post",
                dataType: "json",
                url: $app_url + '/campaign/verifyUser',
                data: $('#add-campaign').serialize(),
                beforeSend: function () {
                    $.blockUI();
                    $(".blockOverlay").css('z-index','9999');
                    $(".blockPage").css('z-index','9999');
                },
                success: function (response) {

                    $.unblockUI();
                    if(response.total_result_count > 0){
                        $("#number_of_user_campaign").val(response.total_result_count);
                        $("#number_of_user_campaign_span").html(response.total_result_count);
                        $("#filterTotalDiv").show();
                        $("#assignUserMultipleForCalling").show();
                    }else{
                        $("#addToCampaignModal").modal('hide');
                        toastr.error("No user allow in this campaign", "Error!", {timeOut: 2000});
                    }
                },
                error: function (data) {
                    $.unblockUI();
                    toastr.error("There was an Error!", "Error!", {timeOut: 2000});
                }

            });
        }else if($(this).val() == "push-notification"){
                $('#pushNotification').show();
                $('#singleChunck').hide();
        }else{
            $("#filterTotalDiv").hide();
            $("#assignUserMultipleForCalling").hide();
            $("#singleChunck").show();
            $("#multipleChunck").hide();
            $('#pushNotification').hide();
        }
    });


    $(document).on('click', "input[name='chunk_type']", function () {
        if($(this).val() == 0){
            $("#singleChunck").show();
            $("#multipleChunck").hide();

        }else{
            $("#singleChunck").hide();
            $("#multipleChunck").show();
        }
    });

    $(document).on('change', '#number_of_user_assign', function () {

        if($(this).val() != ''){
            var total = $("#number_of_user_campaign").val();
            var numberOfUser = $(this).val();
            var allotmentPerUser = Math.round(total/numberOfUser);
            var lastUserAllotment = Math.abs((allotmentPerUser*(numberOfUser-1))-total);

            $("#assignTable").find("tr:gt(1)").remove();
            var new_line = '';
            for(var i=1;i<=numberOfUser;i++){
                 new_line += '<tr>'+$("#assignTable").find('tr:eq(1)').html()+'</tr>';
            }
            $("#assignTable").append(new_line);
            $("#assignTable").find("tr:gt(1)").find(".distributeQty").val(allotmentPerUser);
            $('#assignTable tr:last').find(".distributeQty").val(lastUserAllotment);
        }
        else
        {
            $("#assignTable").find("tr:gt(1)").remove();
        }
        return true;
    });


    $(document).on('change', 'input[name="is_allow_duplicate"]', function() {

            if($('input[name="action"]:checked').val() == "calling-user"){
                $( 'input[name="action"]:radio:last' ).click();
            }

    });







});