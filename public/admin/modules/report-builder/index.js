var $document = $(document);
$document.ready(function () {
    $('.collegeList').select2({
        placeholder: '--SELECT--'
    });
    $(document).on('change', '.elementIdentifier', function () {
        var identifier = $(this).val();
        var elementSelect = $(this).parents('.conditionsData').find('.elementValue');
        console.log(elementSelect, identifier);
        if (identifier == 'is_one_of' || identifier == 'is_not_one_of') {
            if (elementSelect.length > 0){
                elementSelect.attr('multiple', 'multiple');
                elementSelect.select2("destroy").select2({
                    placeholder: '--SELECT--'
                });
            }
        } else {
            if (elementSelect.length > 0){
                elementSelect.removeAttr('multiple');
                elementSelect.select2("destroy").select2({
                    placeholder: '--SELECT--'
                });
            }
        }
    });

    $(document).on('click', '.removeConditionRule', function () {
        var self = $(this);
        self.parents('.removeRow').remove();
    });

    $document.on("click", ".addCondition", function () {
        var self = $(this);
        $elementType = self.parents('.conditionInputDiv').find('.elementType');
        $elementType.toggle();
        self.parents('.addConditionButton').hide();
        $elementType.addClass('active-btn');
    });

    $document.on("click", ".elementConditionList", function () {
        var self = $(this);
        var key = $(".conditionsData").length+11;
        var element_type = self.data('type');
        var datavalue = {'key': key, 'element_type': element_type}
        $.ajax({
            url: $app_url + "/report-builder/get-condition-form",
            dataType: 'json',
            type: "post",
            data: datavalue,
            success: function (data) {
                self.parents('.conditionInputDiv').find('.conditionElements').append(data.html);
                $('.elementValue').select2({
                    placeholder: '--SELECT--'
                });
            },
            error: function (e) {
                alert('there was an error!');
            }
        });
    });

    initializeSelect2();
    if ($("#filterButton").length > 0){
        $document.on('click', '#filterButton', function () {
            $('#filterDiv').toggle();
            initializeSelect2();
        });
    }

    function initializeSelect2() {
        $('#state_id').select2({
            placeholder: '--SELECT--',
            allowClear: true
        });

        $('.elementValue').select2({
            placeholder: '--SELECT--'
        });

        $("#city_id").select2({
            placeholder: "--SELECT--",
            allowClear: true,
            ajax: {
                url: $app_url + '/get-cities-by-state',
                type: "get",
                dataType: 'json',
                data: function (params) {
                    var searchTerm = params.term;
                    return {
                        searchTerm: searchTerm,
                        stateId: $("#state_id").val()
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
            }
        });
    }
});
