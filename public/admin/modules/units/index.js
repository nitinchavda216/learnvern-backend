var $document =$(document);
$document.ready(function () {
    if ($('#dataTableList').length) {
        var $dataTableList = $('#dataTableList').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            bFilter: false,
            "aaSorting": [],
            ajax: {
                url: $app_url + '/unit/getAjaxListData',
                data: function (d) {
                    d.section_id = $('#section_id').val();
                    d.course_id = $('#course_id').val();
                    // d.hide_from_backend = $('#hide_from_backend').val();
                }
            },
            "columns": [
                {data: 'name', 'sortable': false, 'width': "25%"},
                {data: 'course_name', 'sortable': false, 'width': "20%"},
                {data: 'section_name', 'sortable': false, 'width': "20%"},
                // {data: 'hide_status', 'sortable': false, 'width': "15%"},
                {data: 'action', 'orderable': false, 'width': "20%"}
            ],
            select: true,
            bStateSave: true,
            fnStateSave: function (settings, data) {
                localStorage.setItem("dataTables_state", JSON.stringify(data));
            },
            fnStateLoad: function () {
                return JSON.parse(localStorage.getItem("dataTables_state"));
            }
        });

        $('#search-form').on('submit', function (e) {
            $dataTableList.draw();
            e.preventDefault();
        });

        $('#reset-filters').on('click', function () {
            $("#section_id").val(null).trigger('change');
            $("#course_id").val(null).trigger('change');
            // $("#hide_from_backend").val(0).trigger('change');
            $dataTableList.draw();
        });
    }

    $document.on('change', "#course_id", function () {
        $("#section_id").val(null).trigger('change');
    });

    $("#section_id").select2({
        placeholder: '--ALL--',
        ajax: {
            url: $app_url + "/get-sections-by-course",
            type: "get",
            dataType: 'json',
            data: function (params) {
                var searchTerm = params.term;
                return {
                    searchTerm: searchTerm,
                    courseId: $("#course_id").val()
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        }
    });

    // Toggle Switches
    $document.on('click', '.activeStatus', function () {
        var status = ($(this).toggleClass('on').hasClass('on') == true) ? 1 : 0;
        var id = $(this).data('id');
        $.ajax({
            type: "post",
            dataType: "json",
            url: $app_url + '/unit/update_status',
            data: {'is_active': status, 'id': id},
            success: function () {
                toastr.success("Status Updated Successfully!", "Success!", {timeOut: 2000});
            }
        });
    });

    $document.on('click', '.hideFromBackendStatus', function () {
        var status = ($(this).toggleClass('on').hasClass('on') == true) ? 1 : 0;
        var id = $(this).data('id');
        $.ajax({
            type: "post",
            dataType: "json",
            url: $app_url + '/curriculum/hide_from_backend/update',
            data: {'hide_from_backend': status, 'id': id},
            success: function (data) {
                $dataTableList.draw();
                toastr.success(data.message, "Success!", {timeOut: 2000});
            }
        });
    });
});
