var $document =$(document);
$document.ready(function () {
    $('#wizard2').steps({
        headerTag: 'h3',
        bodyTag: 'section',
        autoFocus: true,
        showFinishButtonAlways: is_edit,
        enableAllSteps: is_edit,
        titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
        onStepChanging: function (event, currentIndex, newIndex) {
            if (newIndex < currentIndex) {
                return true;
            }
            var isFormValid = $("#unitForm").isValid();
            if (isFormValid && (currentIndex == 0) && !is_edit){
                var type='unit';
                var slug=$("#slug").val();
                $.ajax({
                    url: $app_url + '/slugExists',
                    method: 'POST',
                    async:false,
                    data: {type: type,slug:slug},
                    success: function (data) {
                        if (data.status) {
                            toastr.error(data.message, "Error!", {timeOut: 2000});
                            $('#slug').removeClass('valid');
                            $('#slug').addClass('error');
                            $('#slug').parent('div').removeClass('has-success');
                            $('#slug').parent('div').addClass('has-error');
                            isFormValid = false;
                        }else{
                            isFormValid = true;
                        }
                    }
                });
                return isFormValid;
            }else{
                return isFormValid;
            }
        }
    });

    if ($('[name="is_global_unit"]').length > 0) {
        var is_global_unit = $('[name="is_global_unit"]')[0].checked;
        $("#globalConditionsDiv").toggle(is_global_unit);
    }
    $('[name="is_global_unit"]').change(function () {
        $("#globalConditionsDiv").toggle(this.checked);
    });

    $document.on('click', "a[href='#finish']", function () {
        $("#unitForm").submit();
    });

    $document.on('change', "#course_id", function () {
        $("#sectionId").val(null).trigger('change');
    });

    $("#sectionId").select2({
        placeholder: 'Select Section',
        ajax: {
            url: $app_url + "/get-sections-by-course",
            type: "get",
            dataType: 'json',
            data: function (params) {
                var searchTerm = params.term;
                return {
                    searchTerm: searchTerm,
                    courseId: $("#course_id").val()
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        }
    });

    /* SET SLUG CONDITIONS */
    if(!is_edit){
        $("#unit_name").focusout(function (i, d) {
            var title = $.trim($("#unit_name").val());
            if (title.length > 0) {
                title = title.toLowerCase();
                title = title.replace(/[^a-z0-9\s]/gi, "").replace(/  +/g, " ").replace(/[_\s]/g, "-");
            }
            $("#slug").val(title);
        });
    }
    $(".slugInput").focusout(function (e) {
        var slug = $(this).val().toLowerCase();
        $(this).val(slug);
    });

    $(".slugInput").keypress(function (e) {
        var regex = new RegExp("^[A-Za-z0-9-]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();
        return false;
    });

    $(".dropify").dropify();
    $(document).on('click', '.dropify-clear', function () {

        $(this).closest(".form-group").find("#hidden_attachment").val(" ");
        $("#removeAttachment").val("true");

    });
    /* SELECT2 INTEGRATION*/
    $('.select2Input').select2({
        placeholder: 'Select Course'
    });
    /* HTML EDITOR INTEGRATION*/
    $('.html_editor').summernote({
        tabsize: 4,
        height: 250
    });

    $document.on('click', "#addNewRow", function () {
        var newRowCloned = $('.newRowTemplate').last().clone();
        newRowCloned.show();
        newRowCloned.find('.editor').summernote({
            tabsize: 4,
            height: 250
        });
        $("#unitFaqs").prepend(newRowCloned);
    });

    $document.on('click', ".removeRow", function () {
        var self = $(this);
        self.parents(".newRowTemplate").remove();
    });

    $document.on('click', ".deleteFaq", function () {
        var self = $(this);
        var faqId = self.data('id');
        bootbox.confirm({
            message: "Are you sure you want to delete this faq?",
            buttons: {
                confirm: {
                    label: "Yes",
                    className: 'btn-success'
                },
                cancel: {
                    label: "No",
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result === true) {
                    $.ajax({
                        type: "get",
                        url: $app_url + '/units/faqs/delete/'+ faqId,
                        data: {},
                        success: function () {
                            self.parents('.unitFaqs').remove();
                            toastr.success("Faq Deleted Successfully!", "Success!", {timeOut: 2000});
                        }
                    });
                }
            }
        });
    });

    $document.on('click', "#addNewAttachment", function () {
        var html = $(".attachmentDiv").last().clone();
        html.show();
        html.find(".attachment-dropify").dropify();
        $("#sectionRows").append(html);
    });

    $document.on('click', ".removeRow", function () {
        $(this).parents(".attachmentDiv").remove();
    });

    if (is_edit){
        $document.on('click', "#updateSlugBtn", function () {
            var isValid = $("#updateSlugForm").isValid();
            if (isValid){
                $("#updateSlugModal").modal('hide');
                bootbox.confirm({
                    message: "Are you sure you want to update this unit slug?",
                    buttons: {
                        confirm: {
                            label: "Yes",
                            className: 'btn-success'
                        },
                        cancel: {
                            label: "No",
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if (result === true) {
                            $('#updateSlugForm').submit();
                        }else{
                            $("#updateSlugModal").modal('show');
                        }
                    }
                });
            }
        });
    }
});
