$(document).ready(function () {

    $('#date_range').daterangepicker({
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        },
        ranges: {
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });

    $('#date_range').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
    });

    $('#date_range').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });


    var $dataTableList = $('#dataTableList').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        bFilter: false,
        "aaSorting": [],
        ajax: {
            url: $app_url + '/reviews/getAjaxListData',
            data: function (d) {
                d.date_range = $('#date_range').val();
                d.search_input = $('#search').val();
                d.rating = $('#rating').val();
                d.is_default_testimonial = $('#is_default_testimonial').val();
                d.course_id = $('#course_id').val();
            }
        },
        "columns": [
            {data: 'content', 'sortable': false, 'width': "20%"},
            {data: 'rating', 'sortable': false, 'width': "5%"},
            {data: 'author', 'sortable': false, 'width': "10%"},
            {data: 'course_name', 'sortable': false, 'width': "30%"},
            {data: 'date', 'sortable': false, 'width': "10%"},
            {data: 'is_default_testimonial', 'sortable': false, 'width': "10%"},
            {data: 'action', 'orderable': false, 'width': "15%"}
        ],
        select: true,
        bStateSave: true,
        fnStateSave: function (settings, data) {
            localStorage.setItem("dataTables_state", JSON.stringify(data));
        },
        fnStateLoad: function (settings) {
            return JSON.parse(localStorage.getItem("dataTables_state"));
        }
    });

    $('#search-form').on('submit', function (e) {
        $dataTableList.draw();
        e.preventDefault();
    });

    $('#reset-filters').on('click', function () {
        $('#search').val('').trigger('change');
        $("#date_range").val(null).trigger('change');
        $("#rating").val(null).trigger('change');
        $("#is_default_testimonial").val(null).trigger('change');
        $("#course_id").val(null).trigger('change');
        $dataTableList.draw();
    });

    // Toggle Switches
    $(document).on('click', '.az-toggle', function () {
        var status = ($(this).toggleClass('on').hasClass('on') == true) ? 1 : 0;
        var id = $(this).data('id');
        $.ajax({
            type: "post",
            dataType: "json",
            url: $app_url + '/reviews/update_status',
            data: {'is_active': status, 'id': id},
            success: function () {
                toastr.success("Status Updated Successfully!", "Success!", {timeOut: 2000});
            }
        });
    });
});
