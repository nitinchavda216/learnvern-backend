$(document).ready(function () {
    // Apply the wysiwyg editor to the elements with "wysiwyg_editor" class
    if ($('.wysiwyg_editor').length > 0) {
        $('.wysiwyg_editor').summernote({
            tabsize: 4,
            height: 250
        });
    }


    if ($(".file-upload").length > 0) {
        var dropifyInput = $(".file-upload").dropify();
    }

    if ($(".select2").length > 0) {
        $('.select2').select2({
            placeholder: '--SELECT--',
        });
    }

    if ($(".select2_no_search").length > 0) {
        $('.select2_no_search').select2({
            placeholder: '--SELECT--',
            minimumResultsForSearch: Infinity
        });
    }
});

function confirmDelete(delete_url) {
    bootbox.confirm({
        message: "Are you sure you want to delete ?",
        buttons: {
            confirm: {
                label: "Yes",
                className: 'btn-success'
            },
            cancel: {
                label: "No",
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result === true) {
                window.location = delete_url;
            }
        }
    });
}


