@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Announcement Management
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('announcement.create') }}" class="btn btn-success btn-sm mt-negative">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('#dataTableList').length) {
                var $dataTableList = $('#dataTableList').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    bFilter: false,
                    "aaSorting": [],
                    ajax: {
                        url: $app_url + '/announcement/getAjaxListData',
                        data: function (d) {
                            d.course_id = $('#course_id').val();
                        }
                    },
                    "columns": [
                        {data: 'title', 'sortable': false, 'width': "40%"},
                        {data: 'start_date', 'orderable': false, 'width': "20%"},
                        {data: 'end_date', 'orderable': false, 'width': "20%"},
                        {data: 'action', 'orderable': false, 'width': "20%"},
                    ],
                    select: true,
                    bStateSave: true,
                    fnStateSave: function (settings, data) {
                        localStorage.setItem("dataTables_state", JSON.stringify(data));
                    },
                    fnStateLoad: function (settings) {
                        return JSON.parse(localStorage.getItem("dataTables_state"));
                    }
                });

                $('#search-form').on('submit', function (e) {
                    $dataTableList.draw();
                    e.preventDefault();
                });

                $('#reset-filters').on('click', function () {
                    $("#course_id").val(null).trigger('change');
                    $dataTableList.draw();
                });
            }

            $(document).on('click', ".az-toggle", function () {
                if($(this).data('is_expired') == 'yes'){
                    toastr.error("Announcement time is over please edit the time if you want to make it visible for the user again!", "Error!", {timeOut: 2000});
                    return false;
                }
                var status = ($(this).toggleClass('on').hasClass('on') == true) ? 1 : 0;
                var id = $(this).data('id');
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: $app_url + '/announcement/update_status',
                    data: {'is_active': status, 'id': id},
                    success: function () {
                        toastr.success("Status Updated Successfully!", "Success!", {timeOut: 2000});
                    }
                });
            });
        });
    </script>
@endsection

