<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title :</label>
            {!! Form::text('title',isset($announcement) ? $announcement->title : old('title'), ['class'=>'form-control', 'data-validation' => 'required','id'=>'announcement_title_text', 'placeholder' => 'Enter Title']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600"></label>
            <label class="ckbox">

                <input type="checkbox" id="is_title_display" name="is_title_display" @if(!isset($announcement) || (isset($announcement) && ($announcement->is_title_display == 1))) checked @endif><span>Show Title On Website</span>
            </label>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Description :</label>
            {!! Form::textarea('description',isset($announcement) ? $announcement->description : old('description'), ['class'=>'form-control', 'id' => 'announcement_description', 'rows' => 5]) !!}
        </div>
    </div>
</div>
<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Starts At:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="typcn typcn-calendar-outline tx-24 lh--9 op-6"></i>
                    </div>
                </div>
                {!! Form::text('start_date', isset($announcement) ? formatDate($announcement->start_date, 'd-m-Y H:i') : old('start_date'), ['class'=>'form-control', 'data-date-format' => "dd-mm-yyyy hh:ii", 'data-validation' => 'required','autocomplete'=>'off','id'=>'announcement_start_date', 'placeholder' => 'Select date']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">End Date :</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="typcn typcn-calendar-outline tx-24 lh--9 op-6"></i>
                    </div>
                </div>
                {!! Form::text('end_date', isset($announcement) ? formatDate($announcement->end_date, 'd-m-Y H:i') : old('end_date'), ['class'=>'form-control', 'data-date-format' => "dd-mm-yyyy hh:ii", 'data-validation' => 'required','autocomplete'=>'off','id'=>'announcement_end_date', 'placeholder' => 'Select date']) !!}
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-az-primary mg-md-t-9">{{ isset($announcement) ? "Update" : "Save" }}</button>
</div>
<div class="announcement-call alert alert-dismissible mb-0 mt-5" role="alert">
    <p class="mb-0">
        <img src="{{ url('admin/img/bell.png') }}" class="img-fluid" alt="image not found">
        <strong id="announcement_title">{{ $announcement->title ?? "[Title]" }}</strong>
        <span id="announcement_desc">{!! $announcement->description ?? "[Description]" !!}</span>
    </p>
</div>
@section('css')
    <link href="{{asset('admin/lib/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
    <style>
        /*==== Announcement Alert Start ====*/
        .announcement-call {
            background-color: #ffc000;
            color: #000;
            text-align: center;
            border-radius: 0;
        }

        .announcement-call p {
            font-size: 1.4em;
            position: relative;
            display: inline-block;
            padding-left: 2.8em;
        }

        .announcement-call p strong {
            border-right: 2px solid #000;
            padding-right: 0.3em;
        }

        .announcement-call p img {
            position: absolute;
            top: 50%;
            left: 0;
            width: 2.1em;
            transform: translateY(-50%);
        }

        /*==== Announcement Alert End ====*/
    </style>
@endsection
@section('footer_scripts')
    <script type="text/javascript"
            src="{{ asset('admin/lib/bootstrap-datetimepicker/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript">
        var is_edit = "{{ isset($announcement) ? true : false }}";
        var exist_start_date = "{{ isset($announcement) ? $announcement->start_date : "" }}";
        var $document = $(document);
        $document.ready(function () {
            $("#is_title_display").on("change", function () {
                if ($('#is_title_display').is(":checked")) {
                    var value = $("#announcement_title_text").val();
                    $('#announcement_title').show();
                    $('#announcement_title').text(value);
                    $("#announcement_title_text").trigger("keyup");

                } else {

                    $('#announcement_title').hide();
                }
            });
            $("#announcement_title_text").on("keyup", function () {
                var value = $(this).val();
                if (value == "") {
                    $('#announcement_title').text("[Title]");
                } else {
                    $('#announcement_title').text(value);
                }
            });

            $('#card-block').html('');

            $('#announcement_description').summernote({
                tabsize: 4,
                tabDisable: false,
                height: 250,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['insert', ['link']]
                ],
                popover: {
                    link: [
                        ['link', ['linkDialogShow', 'unlink']]
                    ]
                },
                callbacks: {
                    onChange: function (contents) {
                        $("#announcement_desc").html(contents);
                    }
                }
            });

            $('#announcement_description').on('summernote.focus', function () {
                if ($('#announcement_description').summernote('isEmpty')) {
                    $(".note-editable").html('');
                }
            });


            $('#announcement_start_date').datetimepicker({
                weekStart: 1,
                autoclose: 1,
                setStartDate: 0,
                startView: 2,
                forceParse: 0,
                showMeridian: 1
            }).on('changeDate', function (e) {
                var dt = new Date(e.date);
                dt.setDate(dt.getDate());
                $("#announcement_end_date").datetimepicker("setStartDate", dt);
                $(this).validate();
            });


            $('#announcement_end_date').datetimepicker({
                weekStart: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1
            }).on('changeDate', function (e) {
                $(this).validate();
            });

            if (is_edit){
                $('#announcement_end_date').datetimepicker('setStartDate', exist_start_date);
            }
        });
    </script>
@endsection

