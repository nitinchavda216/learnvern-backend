<a href="{{ route('announcement.edit', $item->id) }}" class="btn btn-primary btn-xs">
    <i class="fas fa-edit"></i>
</a>
<a href="javascript:;"
   onclick="confirmDelete('{{ route('announcement.delete',$item->id) }}')"
   class="btn btn-danger btn-xs">
    <i class="fas fa-trash"></i>
</a>
<div class="d-inline az-toggle az-toggle-secondary float-right {{ ($item->is_active == 1) ? 'on' : '' }}"
     data-id="{{ $item->id }}" data-is_expired="{{ (\Carbon\Carbon::parse($item->end_date) < \Carbon\Carbon::now()) ? 'yes' : 'no' }}">
    <span></span>
</div>