<div class="card border border-light">
    <div class="card-header">
        <h5 class="text-center mb-0">Registered Users</h5>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6 text-center border-right">
                <h6>Current Month</h6>
                <h3 class="text-success">{{$current_month_registered_users}}</h3>
                <small>({{ formatDate($first_day_current_month,'d-m-Y') }} - {{ formatDate($current_day_current_month,'d-m-Y') }})</small>
            </div>
            <div class="col-md-6 text-center">
                <h6>Last Month</h6>
                <h3 class="text-warning">{{ $last_month_registered_users }}</h3>
                <small>({{ formatDate($first_day_last_month,'d-m-Y') }} - {{ formatDate($current_day_last_month,'d-m-Y') }})</small>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-md-12">
                @if($percentage_diff_value > 0)
                    <p class="text-center">
                    <span class="tx-success tx-bold">
                        <i class="fas fa-angle-double-up"></i> {{ $percentage_diff_value }}%
                    </span>
                        vs. previous month</p>
                @else
                    <p class="text-center">
                        <span class="tx-danger tx-bold">
                            <i class="fas fa-angle-double-down"></i> {{ $percentage_diff_value }}%
                        </span>
                        vs. previous month</p>
                @endif
            </div>
        </div>
    </div>
</div>