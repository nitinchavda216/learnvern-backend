<div class="row row-sm">
    <div class="col-6 col-sm-4 col-lg">
        <label class="card-label">Courses</label>
        <a href="{{ route('courses') }}">
            <h6 class="card-value">{{ $statistics['courses'] }}</h6>
        </a>
    </div>
    <div class="col-6 col-sm-4 col-lg">
        <label class="card-label">Webinars</label>
        <a href="{{ route('webinars') }}">
            <h6 class="card-value">{{ $statistics['webinars'] }}</h6>
        </a>
    </div>
    <div class="col-6 col-sm-4 col-lg">
        <label class="card-label">Units</label>
        <a href="{{ route('units') }}">
            <h6 class="card-value">{{ $statistics['units'] }}</h6>
        </a>
    </div>
    <div class="col-6 col-sm-4 col-lg">
        <label class="card-label">Assignments</label>
        <a href="{{ route('assignments') }}">
            <h6 class="card-value">{{ $statistics['assignments'] }}</h6>
        </a>
    </div>
    <div class="col-6 col-sm-4 col-lg">
        <label class="card-label">Quizzes</label>
        <a href="{{ route('quiz') }}">
            <h6 class="card-value">{{ $statistics['quizzes'] }}</h6>
        </a>
    </div>
</div>