@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> App Update Details
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('app_updates') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="table-responsive">
                        <table class="table table-bordered mg-b-0">
                            <tbody>
                            <tr>
                                <td width="30%"><b>Title:</b></td>
                                <td width="70%">{{ $version_details->title }}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Version:</b></td>
                                <td width="70%">{{ $version_details->version }}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Platform:</b></td>
                                <td width="70%">{{ ($version_details->platform == "android") ? "Android" : "IOS" }}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Update Type:</b></td>
                                <td width="70%">{{ ($version_details->update_type == "soft") ? 'Soft Update' : 'Hard Update' }}</td>
                            </tr>
                            <tr>
                                <td width="30%"><b>Update Details:</b></td>
                                <td width="70%">{!! $version_details->content ?? '- -' !!}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
