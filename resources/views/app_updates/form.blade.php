<div class="row row-sm">
    <div class="col-sm-3">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title :</label>
            {!! Form::text('title', old('title'), ['class'=>'form-control', 'data-validation' => 'required', 'placeholder' => 'Enter Title']) !!}
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Version :</label>
            {!! Form::text('version', old('version'), ['class'=>'form-control', 'data-validation' => 'required', 'placeholder' => 'Enter Version | Ex. 1, 1.1, 1.2']) !!}
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Platform :</label>
            {!! Form::select('platform', ['android' => 'Android', 'ios' => 'IOS'], old('platform'), ['class'=>'form-control select2_no_search']) !!}
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Update Type :</label>
            {!! Form::select('update_type', ['soft' => 'Soft Update', 'hard' => 'Hard Update'], old('update_type'), ['class'=>'form-control select2_no_search']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Update Details :</label>
            {!! Form::textarea('content', old('content'), ['class'=>'form-control wysiwyg_editor']) !!}
        </div>
    </div>
</div>
<button type="submit"
        class="btn btn-az-primary mg-md-t-9">Submit</button>
