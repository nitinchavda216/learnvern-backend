@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> App Update List
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('app_updates.create') }}" class="btn btn-success btn-sm mt-negative">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="dataTableList" class="display responsive">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Version</th>
                                    <th>Platform</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($updates as $item)
                                    <tr>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->version }}</td>
                                        <td>{{ ($item->platform == "android") ? "Android" : "IOS" }}</td>
                                        <td>
                                            <a href="{{ route('app_updates.view', $item->id) }}"
                                               class="btn btn-secondary btn-xs">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        var $document = $(document);
        $document.ready(function () {
            $('#dataTableList').DataTable({
                responsive: true,
                aaSorting: [],
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ records'
                }
            });
        });
    </script>
@endsection


