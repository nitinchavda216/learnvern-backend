@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Ambassador Program Settings
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('ambassador-program-settings.create') }}"
                           class="btn btn-success btn-sm mt-negative"><i class="fa fa-plus" aria-hidden="true"></i>Add New</a>
                    </span>
                </h2>
                <div id="data-container">
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>Refer From</th>
                            <th>Referrals Count</th>
                            <th>Badge Title</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $item)
                            <tr>
                                <td>{{ $item->refer_from }}</td>
                                <td>{{ $item->referrals_count }}</td>
                                <td>{{ $item->title }}</td>
                                <td>
                                    <a href="{{ route('ambassador-program-settings.edit', $item->id) }}"
                                       class="btn btn-primary btn-xs d-inline">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    <div class="d-inline az-toggle az-toggle-secondary float-right {{ ($item->is_active == 1) ? 'on' : '' }}"
                                         data-id="{{ $item->id }}">
                                        <span></span>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataTableList').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: ''
                }
            });
            // Toggle Switches
            $('.az-toggle').on('click', function () {
                var status = ($(this).toggleClass('on').hasClass('on') == true) ? 1 : 0;
                var id = $(this).data('id');
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: $app_url + '/ambassador-program-settings/update_status',
                    data: {'is_active': status, 'id': id},
                    success: function (data) {
                        toastr.success("Status Updated Successfully!", "Success!", {timeOut: 2000});
                    }
                });
            });
        });
    </script>
@endsection

