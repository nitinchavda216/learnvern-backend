<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Referrals Count :</label>
            {!! Form::text('referrals_count', $program_setting->referrals_count ?? old('referrals_count'), ['class'=>'form-control', 'data-validation' => 'required number']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Badge Title :</label>
            {!! Form::text('title', $program_setting->title ?? old('title'), ['class'=>'form-control', 'data-validation' => 'required']) !!}
        </div>
    </div>
    <input type="hidden" name="refer_from" value="other">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Web Content :</label>
            {!! Form::textarea('content', $program_setting->content ?? old('content'), ['class'=>'form-control', 'data-validation' => 'required', 'rows' => 6]) !!}
        </div>
    </div>

    {{--<div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Refer From :</label>
        </div>
        <div class="col-lg-12 mg-t-20 mg-lg-t-0">
            <label class="rdiobox">
                <input name="refer_from" type="radio" value="course" @if(!isset($program_setting) || ($program_setting->refer_from == "course")) checked @endif>
                <span>Course Page</span>
            </label>
        </div>
        <div class="col-lg-12 mg-t-20 mg-lg-t-0">
            <label class="rdiobox">
                <input name="refer_from" type="radio" value="other" @if(isset($program_setting) && $program_setting->refer_from == "other") checked @endif>
                <span>Other Pages</span>
            </label>
        </div>
    </div>--}}
    @if(isset($program_setting))
        <input type="hidden" name="remove-image" id="removeImage" value="false">
    @endif
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Image :</label>
            <input type="file" accept="image/*" class="file-upload"
                   data-allowed-file-extensions="png jpg jpeg svg"
                   @if(isset($program_setting) && isset($program_setting->image)) data-default-file="{{ showAmbassadorSettingImage($program_setting->image) }}"
                   @endif
                   name="image" id="image">
        </div>
    </div>
    <div class="col-sm-12">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">App Content :</label>
            {{ Form::textarea('app_content', isset($program_setting) ? $program_setting->app_content : old('app_content') ,
                ['class' => 'form-control',
                'id'=>'app_content',
                'data-validation' => 'required',
                'data-validation-error-msg' => "Please enter App Content!"
                ] ) }}
        </div>
    </div>
</div>
<button type="submit"
        class="btn btn-az-primary mg-md-t-9">{{ isset($program_setting) ? "Update" : "Submit" }}</button>
