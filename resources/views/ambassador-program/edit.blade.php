@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Edit Ambassador Program Settings
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('ambassador-program-settings') }}" class="btn btn-warning btn-sm mt-negative"
                           title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="card card-body pd-40">
                        {!! Form::open(['route'=> ['ambassador-program-settings.update', $program_setting->id],'method' => 'POST', 'enctype'=> "multipart/form-data", 'files' => true, 'id' => "courseForm"]) !!}
                        @include('ambassador-program.form')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '.dropify-clear', function () {
                $("#removeImage").val("true");
            })
        })
    </script>
@endsection

