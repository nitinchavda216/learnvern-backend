@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container-fluid">
            <div class="az-content-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="az-content-title">App Notification</h2>
                        <div id="data-container">
                            {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'appnotification']) !!}
                            <div class="mb-3">
                                <div class="row row-xs">
                                    <div class="col-md-3">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Search</label>
                                        <input name="title" id="title" type="text" class="form-control"
                                               placeholder="title">
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Date
                                                :</label>
                                            <textarea id="description" name="description" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md mg-t-10 mt-4">
                                        <button type="submit" class="btn btn-az-success btn-success">send</button>

                                    </div>
                                </div>

                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        /*  var showSubmitButton = "{{ isset($course) ? true : false }}";*/
    </script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
@endsection
