@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Webinars
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('webinars.create') }}" class="btn btn-success btn-sm mt-negative">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-5">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Search :</label>
                                {!! Form::text('search', null,['id' => 'search', 'class' => 'form-control', 'placeholder' => "Search Webinar"]) !!}
                            </div>
                            <div class="col-md-5">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Category :</label>
                                {!! Form::select('category_id', $categories, [],['id' => 'category_id', 'class' => 'form-control select2', 'placeholder' => ""]) !!}
                            </div>
                            <div class="col-md-2 mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>Webinar Name</th>
                            <th>Category</th>
                            <th>Actions</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('#dataTableList').length) {
                var $dataTableList = $('#dataTableList').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    bFilter: false,
                    "aaSorting": [],
                    ajax: {
                        url: $app_url + '/webinars/getAjaxListData',
                        data: function (d) {
                            d.search = $('#search').val();
                            d.category_id = $('#category_id').val();
                        }
                    },
                    "columns": [
                        {data: 'name', 'sortable': false, 'width': "45%"},
                        {data: 'category_name', 'sortable': false, 'width': "40%"},
                        {data: 'actions', 'orderable': false, 'width': "10%"},
                        {data: 'status', 'orderable': false, 'width': "5%"}
                    ],
                    select: true,
                    bStateSave: true,
                    fnStateSave: function (settings, data) {
                        localStorage.setItem("dataTables_state", JSON.stringify(data));
                    },
                    fnStateLoad: function (settings) {
                        return JSON.parse(localStorage.getItem("dataTables_state"));
                    }
                });

                $('#search-form').on('submit', function (e) {
                    $dataTableList.draw();
                    e.preventDefault();
                });

                $('#reset-filters').on('click', function () {
                    $("#search").val(null);
                    $("#category_id").val(null).trigger('change');
                    $dataTableList.draw();
                });
            }
        });
        // Toggle Switches
        $(document).on('click', ".az-toggle", function () {
            var status = ($(this).toggleClass('on').hasClass('on') == true) ? 1 : 0;
            var id = $(this).data('id');
            $.ajax({
                type: "post",
                dataType: "json",
                url: $app_url + '/webinars/update_status',
                data: {'is_active': status, 'id': id},
                success: function () {
                    toastr.success("Status Updated Successfully!", "Success!", {timeOut: 2000});
                }
            });
        });
    </script>
@endsection

