<div id="wizard2">
    <h3>Basic Info</h3>
    <section>
        @include('webinars.tabs.general')
    </section>
    <h3>Additional Info</h3>
    <section>
        @include('webinars.tabs.additional_info')
    </section>
    <h3>Webinar Video</h3>
    <section>
        @include('webinars.tabs.course_videos')
    </section>
    <h3>SEO Details</h3>
    <section>
        @include('webinars.tabs.seo_details')
    </section>
</div>
@section('css')
    <link href="{{asset('admin/lib/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/lib/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/lib/spectrum-colorpicker/spectrum.css')}}" rel="stylesheet">
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('admin/lib/jquery-steps/jquery.steps.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('admin/lib/bootstrap-datetimepicker/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('admin/lib/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/lib/spectrum-colorpicker/spectrum.js') }}"></script>
    <script type="text/javascript">
        var is_edit = "{{ isset($course) ? true : false }}";
    </script>
    <script type="text/javascript" src="{{ asset('admin/modules/courses/form.js') }}"></script>
@endsection

