@if($item->is_active == 1)
    <a href="{{ env('FRONT_URL').'course/'.$item->slug }}"
       class="btn btn-secondary btn-xs d-inline" target="_blank">
        <i class="fas fa-eye"></i>
    </a>
@endif
<a href="{{ route('webinars.edit', $item->id) }}"
   class="btn btn-primary btn-xs d-inline">
    <i class="fas fa-edit"></i>
</a>