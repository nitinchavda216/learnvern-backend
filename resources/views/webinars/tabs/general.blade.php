<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title: </label>
            {!! Form::text('name', $course->name ?? old('name'), ['class'=>'form-control', 'id' => 'course_name', 'data-validation' => 'required']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Slug: </label>
            {!! Form::text('slug', $course->slug ?? old('slug'), ['class'=>'form-control', 'id' => 'slug', 'data-validation' => 'required', isset($course) ? 'readonly' : ""]) !!}
        </div>
    </div>
</div>
<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Webinar Certificate Name :</label>
            <small class="text-danger">To be displayed on the User Certificate, Limit: 45 chars</small>
            {!! Form::text('course_certificate_name', $course->course_certificate_name ?? old('course_certificate_name'), ['class'=>'form-control', 'id' => 'course_certificate_name', 'data-validation' => 'required','data-maxallowed'=>45]) !!}
            <h6><span id="courseCertificateNameCount">0</span> chars</h6>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Choose Color Code :</label>
            <br>
            <input type="text" id="colorpicker" name="color_code" value="{{ $course->color_code ?? '#000000' }}">
        </div>
    </div>
</div>
<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Category :</label>
            {!! Form::select('category_id', $categories, $course->category_id ?? old('category_id'), ['class'=>'form-control select2Input', 'data-validation' => 'required', 'placeholder' => ""]) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Language: </label>
            {!! Form::select('course_language', $languages, $course->course_language ?? old('course_language'), ['class'=>'form-control select2Input', 'id' => 'course_language']) !!}
        </div>
    </div>
</div>
<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Starts At:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="typcn typcn-calendar-outline tx-24 lh--9 op-6"></i>
                    </div>
                </div>
                {!! Form::text('start_date', isset($course) ? formatDate($course->start_date, 'd-m-Y H:i') : old('start_date'), ['class'=>'form-control form_datetime', 'data-date-format' => "dd-mm-yyyy hh:ii", 'data-validation' => 'required']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Completes At:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <i class="typcn typcn-calendar-outline tx-24 lh--9 op-6"></i>
                    </div>
                </div>
                {!! Form::text('complete_date', isset($course) ? formatDate($course->complete_date, 'd-m-Y H:i') : old('complete_date'), ['class'=>'form-control form_datetime', 'data-date-format' => "dd-mm-yyyy hh:ii", 'data-validation' => 'required']) !!}
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Brief Description :</label>
    {!! Form::textarea('brief_description', $course->brief_description ?? old('brief_description'), ['class'=>'form-control', 'id' => 'brief_description', 'rows' => 5]) !!}
</div>
<div class="row row-sm">
    <div class="col-sm-3">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Icon Image :</label>
            <input type="file" accept="image/*" class="course_image" data-show-remove="false"
                   data-allowed-file-extensions="png jpg jpeg svg"
                   @if(isset($course) && ($course->course_icon != "")) data-default-file="{{ $course->course_icon }}"
                   @else
                   data-validation="required" @endif
                   name="course_icon" id="course_icon">
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Image :</label>
            <input type="file" accept="image/*" class="course_image" data-show-remove="false"
                   data-allowed-file-extensions="png jpg jpeg svg"
                   @if(isset($course) && ($course->image != "")) data-default-file="{{ $course->image }}"
                   @else
                   data-validation="required" @endif
                   name="image" id="image">
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">App Icon :</label>
            <input type="file" accept="image/*" class="course_image" data-show-remove="false"
                   data-allowed-file-extensions="png jpg jpeg svg"
                   @if(isset($course) && ($course->app_icon != "")) data-default-file="{{ $course->app_icon }}"
                   @else
                   data-validation="required" @endif
                   name="app_icon" id="app_icon">
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">App Image :</label>
            <input type="file" accept="image/*" class="course_image" data-show-remove="false"
                   data-allowed-file-extensions="png jpg jpeg svg"
                   @if(isset($course) && ($course->app_image != "")) data-default-file="{{ $course->app_image }}"
                   @else
                   data-validation="required" @endif
                   name="app_image" id="app_image">
        </div>
    </div>
</div>
