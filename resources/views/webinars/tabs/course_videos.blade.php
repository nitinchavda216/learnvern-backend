<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Introduction Vimeo Video :</label>
    {!! Form::text('intro_video', $course->intro_video ?? old('intro_video'), ['class'=>'form-control', 'id' => 'intro_video']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Live Streaming Link URL:</label>
    {!! Form::text('intro_youtube_video', $course->intro_youtube_video ?? old('intro_youtube_video'), ['class'=>'form-control', 'id' => 'intro_youtube_video']) !!}
</div>