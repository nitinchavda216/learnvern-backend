<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Content :</label>
    {!! Form::textarea('content', $course->content ?? old('content'), ['class'=>'form-control html_editor', 'id' => 'content']) !!}
</div>