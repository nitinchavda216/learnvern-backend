@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Set Curriculum
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('courses') }}" class="btn btn-warning btn-sm mt-negative"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                    </span>
                </h2>
                <div class="card card-body pd-40">
                    @if(count($sections) > 0)
                        <div class="sortable">
                            @foreach($sections as $section)
                                <div class="group-caption bg-gray-200" data-id="{{ $section->id }}">
                                    <h4>{{ $section->name }}</h4>
                                    <div class="move">+</div>
                                    <div class="group-items">
                                        @foreach($section->getSectionChildData as $data)
                                            @php
                                                $text_color = ($data->type == "unit") ? "green" : ($data->type == "assignment" ? "peru" : ($data->type == "quiz" ? "brown" : "black"))
                                            @endphp
                                            <div class="group-item" data-id="{{ $data->id }}" style="color: {{ $text_color }} !important;">{{ $data->name }}
                                                <div class="move">+</div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="az-content-label text-center"><h3>No Record Found</h3></div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <style>
        .sortable {

        }

        .group-caption {
            width: 100%;
            display: block;
            padding: 20px;
            margin: 0 0 15px 0;
        }

        .group-item {
            background: #ffffff;
            width: 80%;
            height: 30px;
            display: block;
            padding: 3px;
            margin: 5px;
            color: #000;
        }

        .move {
            background: #ff0000;
            width: 30px;
            height: 30px;
            float: right;
            color: #fff;
            text-align: center;
            text-transform: uppercase;
            line-height: 30px;
            font-family: Arial;
            cursor: move;
        }

        .movable-placeholder {
            background: #ccc;
            width: 100%;
            height: 100px;
            display: block;
            padding: 20px;
            margin: 0 0 15px 0;
            border-style: dashed;
            border-width: 2px;
            border-color: #000;
        }
    </style>
@endsection
@section('footer_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"
            integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA=="
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".sortable").sortable({
                containment: "parent",
                items: "> div",
                handle: ".move",
                tolerance: "pointer",
                cursor: "move",
                opacity: 0.7,
                revert: 300,
                delay: 150,
                dropOnEmpty: true,
                placeholder: "movable-placeholder",
                start: function (e, ui) {
                    ui.placeholder.height(ui.helper.outerHeight());
                },
                update: function (e, ui) {
                    var sortData = $('.sortable').sortable('toArray', {attribute: 'data-id'});
                    $.ajax({
                        url: $app_url + "/course/curriculum/update",
                        method: 'POST',
                        data: {ids: sortData},
                        success: function (data) {
                            if (data.status) {
                                toastr.success(data.message, "Success!", {timeOut: 2000});
                            } else {
                                toastr.error(data.message, "Error!", {timeOut: 2000});
                            }
                        }
                    })
                }
            });

            // Sort the children
            $(".group-items").sortable({
                items: "> div",
                tolerance: "pointer",
                containment: "parent",
                update: function (e, ui) {
                    var sortData = $(this).parent(".group-caption").children('.group-items').sortable('toArray', {attribute: 'data-id'});
                    $.ajax({
                        url: $app_url + "/course/curriculum/update",
                        method: 'POST',
                        data: {ids: sortData},
                        success: function (data) {
                            if (data.status) {
                                toastr.success(data.message, "Success!", {timeOut: 2000});
                            } else {
                                toastr.error(data.message, "Error!", {timeOut: 2000});
                            }
                        }
                    })
                }
            });
        });
    </script>
@endsection

