<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <meta name="description" content="Xenon Boostrap Admin Panel"/>
    <meta name="author" content=""/>
    <title>{{ env('APP_NAME') }}</title>
</head>
<body class="page-body">
{!! $page_detail['app_content'] ?? 'No Record Found.' !!}
</body>
</html>
