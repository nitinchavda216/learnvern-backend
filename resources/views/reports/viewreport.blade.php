@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder position-relative">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item active">Reports</li>
            </ul>
            <a href="javascript:;" data-toggle="modal" class="helpTip help-tip-button" data-site_area="admin"
               data-module="reports"
               data-section="form_page">
                <i class="fa fa-info-circle fa-2x"></i>
            </a>
        </div>
    </div>
    <section>
        <div class="container-fluid">
            <div class="row mt-2">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Name : {{ $report->name }}
                            <span class="pull-right ml-auto">
                                 <a href="{{route('admin.reports.export', $report->id)}}" title="Export Report Data">
                                    <button class="btn btn-primary btn-sm">
                                        <i class="fa fa-file-excel-o" aria-hidden="true"></i> Export
                                    </button>
                                </a>
                                 <a href="{{ route('admin.reports') }}" title="Back">
                                    <button class="btn btn-warning btn-sm">
                                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                    </button>
                                </a>
                            </span>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr class="bg-dark text-white">
                                        @foreach($selectedData['fields'] as $field)
                                            <th>{{$fieldList[$field]['label']}}</th>
                                        @endforeach
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($resultSet)>0)
                                        @foreach($resultSet as $item)
                                            <tr>
                                                @foreach($item as $value)
                                                    <td>{{ $value }}</td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="{{count($selectedData['fields'])+1}}" class="text-center">
                                                No record found
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="pagination-wrapper"> {!! $resultSet->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
