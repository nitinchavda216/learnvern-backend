@extends('admin.layouts.master')
@section('content')
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="#" class="active">Reports</a></li>
            </ul>
            <a href="javascript:;" data-toggle="modal" class="helpTip help-tip-button" data-site_area="admin"
               data-module="reports"
               data-section="form_page">
                <i class="fa fa-info-circle"></i>
            </a>
        </div>
    </div>
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Reports
                            <span class="pull-right float-right ml-auto">
                                    <a class="btn mr-2 mb-2 btn-success btn-sm"
                                       href="{{ route('admin.reports.create') }}">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                            Add New
                                    </a>
                                </span>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr class="alert-dark">
                                            <th width="75%" class="text-truncate">Title</th>
                                            <th width="25%" class="text-truncate">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($reports)>0)
                                        @foreach($reports as $item)
                                            <tr>
                                                <td>{{ $item->name }}</td>
                                                <td>
                                                    <a href="{{route('admin.reports.viewreport', $item->id)}}"
                                                       class="btn btn-success btn-sm" title="View Report Data">
                                                        <i class="fa fa-eye" aria-hidden="true"></i> View Report Data
                                                    </a>
                                                    <a href="{{ route('admin.reports.edit', $item->id) }}"
                                                       title="Edit Page">
                                                        <button class="btn btn-primary btn-sm"><i
                                                                    class="fa fa-pencil-square-o"
                                                                    aria-hidden="true"></i>
                                                            Edit
                                                        </button>
                                                    </a>
                                                    <a href="{{route('admin.reports.destroy', $item->id)}}"
                                                       class="btn btn-danger btn-sm" title="Delete"
                                                       onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                        <i class="fa fa-trash" aria-hidden="true"></i> Delete
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="2" class="text-center">No record found</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
