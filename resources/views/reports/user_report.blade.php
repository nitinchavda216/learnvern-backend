@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title"> User Report List
                    <span class="pull-right d-inline-block float-right">
                    <a href="javascript:;" class="btn btn-success btn-sm mt-negative"
                       onclick="this.href='user-report/getAjaxExportData?register_date=' + document.getElementById('register_date').value + '&last_login_date=' + document.getElementById('last_login_date').value + '&searchData=' + document.getElementById('searchData').value + '&sign_up=' + document.getElementById('sign_up').value + '&partner_user_id=' + document.getElementById('partner_user_id').value +'&user_type=' + document.getElementById('user_type').value">
                        <i class="fa fa-download" aria-hidden="true"></i> Export
                    </a>
                    </span>
                </h2>
                <div id="data-container">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Search:</label>
                                <input name="searchData" id="searchData" type="text" class="form-control"
                                       placeholder="Search">
                            </div>
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Registration Date
                                    Range:</label>
                                <input name="register_date" id="register_date" type="text" class="form-control"
                                       autocomplete="off" placeholder="Select Date">
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Last Login Date
                                        Range:</label>
                                    <input name="last_login_date" id="last_login_date" type="text" class="form-control"
                                           autocomplete="off" placeholder="Select Date">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">User Status:</label>
                                {!! Form::select('is_active', [''=>"All Users",'0'=>'Inactive Users','1'=>'Active Users'], $rating->rating ?? old('rating'), ['class'=>'form-control select2', 'id' => 'is_active']) !!}
                            </div>

                            @if(session('partner_id') == 1)
                                <div class="col-md-2">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Partners:</label>
                                    {!! Form::select('partner_user_id', $partners, null,['id' => 'partner_user_id', 'class' => 'form-control select2']) !!}
                                </div>
                            @endif
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Sign UP From:</label>
                                {!! Form::select('sign_up', \App\Models\User::SIGNUP_FROM, $rating->rating ?? old('rating'), ['class'=>'form-control select2', 'id' => 'sign_up', 'placeholder' => "Select Page Section"]) !!}
                            </div>

                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Users Type:</label>
                                {!! Form::select('user_type', array('0' => 'All Users', '1' => 'Indian Users', '2' => 'International Users'), old('user_type'), ['class'=>'form-control select2_no_search', 'id' => 'user_type']) !!}
                            </div>

                            <div class="col-md-2 mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <table id="dataTableList" class="table table-responsive">
                        <thead>
                        <tr>
                            <th colspan="7" style="background-color: #eee; border-bottom:#fff solid 1px;" class="text-center">User Information</th>
                            <th colspan="2" style="background-color: #eed8bd; border-bottom:#fff solid 1px;" class="text-center">Courses</th>
                            <th style="background-color: #ceeecb; border-bottom:#fff solid 1px;" class="text-center">Referrals</th>
                            <th colspan="2" style="background-color: #c4e6ee; border-bottom:#fff solid 1px;" class="text-center">Certificates</th>
                            <th style="border-bottom:#fff solid 1px;" class="text-center">More</th>
                        </tr>
                        <tr>
                            <th style="background-color: #eee;">User</th>
                            <th style="background-color: #eee;">Source</th>
                            <th style="background-color: #eee;">Ref. Code</th>
                            <th style="background-color: #eee;">Affiliate</th>
                            <th style="background-color: #eee;">Registered</th>
                            <th style="background-color: #eee;">Is Active</th>
                            <th style="background-color: #eee;">Last-Login</th>
                            <th style="background-color: #eed8bd; border-right: #fff solid 1px;">Enrolled</th>
                            <th style="background-color: #eed8bd;">Completed</th>
                            <th style="background-color: #ceeecb;">Total</th>
                            <th style="background-color: #c4e6ee; border-right: #fff solid 1px;">Paid</th>
                            <th style="background-color: #c4e6ee;">Free</th>
                            <th>Course Details</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        /*  var showSubmitButton = "{{ isset($course) ? true : false }}";*/
    </script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="{{ asset('admin/modules/reports/index.js') }}"></script>
@endsection

