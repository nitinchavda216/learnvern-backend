@extends('admin.layouts.master')
@section('content')
    <!-- Breadcrumb-->
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item active">Pending Payout Reports</li>
            </ul>
        </div>
    </div>
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                            <div class="heading heading-bg height-auto">
                                <div class="row">
                                    <div class="col-md-2 col-lg-2">
                                        <label class="control-label">Shop</label>
                                        <div class="control-group">
                                            {!! Form::select('shop_id', $shop_titles , null, ['class'=>'form-control','placeholder'=>'Select Shop','id' =>'shop_id']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3">
                                        <label class="control-label">Date Range</label>
                                        <div class="control-group">
                                            {!! Form::text('order_date', null , ['class'=>'form-control','id' =>'order_date']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-2">
                                        <label class="control-label">&nbsp;</label>
                                        <div class="control-group">
                                            <button type="submit"
                                                    class="btn btn-success">Search
                                            </button>
                                            <button type="button" class="btn btn-warning"
                                                    id="reset-filters">Reset
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            {!! Form::close() !!}
                            <div class="table-responsive">
                                <table class="table" id="reportsDataTable">
                                    <thead>
                                    <tr class="bg-dark text-white">
                                        <th>Shop Owner</th>
                                        <th>Pending Amount</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade modal-form" id="orderItemsModal" tabindex="-1" role="dialog"
         aria-labelledby="orderItemsModal"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mb-0">Pending Payout Items</h5>
                    <button type="button" class="close p-0" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- Smart Wizard HTML -->
                    <div class="container">
                        <div class="row px-0 orderItems">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('global/plugins/dataTables/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="{{ asset('admin/modules/reports/pending_payments_report.js') }}"></script>
@endsection
