@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title">Referral Report List
                    <span class="pull-right d-inline-block float-right">
                        <a href="javascript:;" class="btn btn-sm btn-success mt-negative"
                           onclick="this.href='referral-report/export?register_date=' + document.getElementById('register_date').value + '&signup_from=' + document.getElementById('signup_from').value + '&search=' + document.getElementById('search').value + '&usertype=' + document.getElementById('users_type').value + '&partner_user_id=' + document.getElementById('partner_user_id').value">
                            <i class="fa fa-download" aria-hidden="true"></i> Export</a>
                    </span>
                </h2>
                <div id="data-container">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-3">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Search:</label>
                                <input name="search" id="search" type="text" class="form-control"
                                       placeholder="Search">
                            </div>
                            @if(session('partner_id') == 1)
                                <div class="col-md-3">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Partners:</label>
                                    {!! Form::select('partner_user_id', $partners, null,['id' => 'partner_user_id', 'class' => 'form-control select2']) !!}
                                </div>
                            @endif
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">SignUp From:</label>
                                {!! Form::select('signup_from', \App\Models\User::SIGNUP_FROM, [],['id' => 'signup_from', 'class' => 'form-control select2', 'placeholder' => ""]) !!}
                            </div>
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Registration Date Range:</label>
                                <input name="register_date" id="register_date" type="text" class="form-control"
                                       autocomplete="off" placeholder="Select Date">
                            </div>
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Verified Status:</label>
                                <select name="users_type" class="form-control"  id="users_type">
                                    <option value="">All Users</option>
                                    <option value="1" selected>Active Users</option>
                                    <option value="0">Inactive Users</option>
                                </select>
                            </div>
                        </div>
                        <div class="row row-xs">
                            <div class="col-md-12 mt-4 text-right">
                                <button type="submit" class="btn btn-sm btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-sm btn-az-warning btn-warning" id="reset-filters">Reset
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    {{--<a href="javascript:;" class="btn btn-az-success btn-success float-right mg-l-0"
                       onclick="this.href='referral-report/export?register_date=' + document.getElementById('register_date').value + '&signup_from=' + document.getElementById('signup_from').value + '&search=' + document.getElementById('search').value">
                        <i class="fa fa-download" aria-hidden="true"></i> Export
                    </a>--}}
                    <table id="dataTableList" class="table table-responsive">
                        <thead>
                        <tr>
                            <th>User Details</th>
                            <th>Current Occupation</th>
                            <th>Affiliate Referral</th>
                            <th>Affiliate User</th>
                            <th>Sign up From</th>
                            <th>Last Login Date</th>
                            <th>Email Verify</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#register_date').daterangepicker({
                autoUpdateInput: true,
                startDate: moment().subtract(6, 'days'),
                endDate: moment(),
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY'
                },
                ranges: {
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            $('#register_date').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            });
            $('#register_date').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

            if ($('#dataTableList').length) {
                var $dataTableList = $('#dataTableList').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    bFilter: false,
                    "aaSorting": [],
                    ajax: {
                        url: $app_url + '/referral-report/getAjaxListData',
                        data: function (d) {
                            d.search = $('#search').val();
                            d.register_date = $('#register_date').val();
                            d.signup_from = $('#signup_from').val();
                            d.users_type = $('#users_type').val();
                            d.partner_user_id = $('#partner_user_id').val();
                        }
                    },
                    "columns": [
                        {data: 'user_details', 'sortable': false, 'width': "8%"},
                        {data: 'current_occupation', 'sortable': false, 'width': "8%"},
                        {data: 'referral', 'sortable': false, 'width': "10%"},
                        {data: 'affiliate', 'sortable': false, 'width': "10%"},
                        {data: 'sign_up_from', 'sortable': false, 'width': "10%"},
                        {data: 'last_login_date', 'sortable': false, 'width': "10%"},
                        {data: 'email_verify', 'sortable': false, 'width': "8%"}
                    ],
                    select: true,
                    bStateSave: true,
                    fnStateSave: function (settings, data) {
                        localStorage.setItem("dataTables_state", JSON.stringify(data));
                    },
                    fnStateLoad: function (settings) {
                        return JSON.parse(localStorage.getItem("dataTables_state"));
                    }
                });
                $('#search-form').on('submit', function (e) {
                    $dataTableList.draw();
                    e.preventDefault();
                });
                $('#reset-filters').on('click', function () {
                    $('#search').val('').trigger('change');
                    $('#signup_from').val('').trigger('change');
                    $("#register_date").val(null).trigger('change');
                    $('#users_type').val(1).trigger('change');
                    $('#partner_user_id').val(1).trigger('change');
                    $dataTableList.draw();
                });
            }
        });
    </script>
@endsection

