@extends('admin.layouts.master')
@section('content')
    <!-- Breadcrumb-->
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="#" class="active">Reports</a></li>
            </ul>
            <a href="javascript:;" data-toggle="modal" class="helpTip help-tip-button" data-site_area="admin"
               data-module="reports"
               data-section="form_page">
                <i class="fa fa-info-circle"></i>
            </a>
        </div>
    </div>
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Create New Report
                            <span class="pull-right ml-auto">
                                 <a href="{{ route('admin.reports') }}" title="Back">
                                    <button class="btn btn-warning btn-sm">
                                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                    </button>
                                </a>
                            </span>
                        </div>
                        <div class="card-body">
                            <form method="POST" id="createReportForm" action="{{ route('admin.reports.store') }}"
                                  accept-charset="UTF-8"
                                  class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-row bg-light border p-3 mb-3">
                                    <div class="col-md-6">
                                        <div class="position-relative form-group mb-0">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col-2 col-md-1">
                                                    <label for="name" class="mb-0"><b>Name :</b></label>
                                                </div>
                                                <div class="col-10 col-md-11">
                                                    <input name="name" id="name" placeholder="Enter Report Name" type="text"
                                                           class="form-control" data-validation="required"
                                                           data-validation-error-msg="Please Enter Report Title">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="main-card card">
                                            <div class="card-header alert-primary">Fields</div>
                                            <div class="card-body p-1">
                                                <div id="accordion" class="accordion-wrapper">
                                                    <div class="card mb-0">
                                                        <div id="headingOne" class="card-header alert-dark" style="min-height:0;">
                                                            <button type="button" data-toggle="collapse"
                                                                    data-target="#collapseOne1"
                                                                    aria-expanded="false" aria-controls="collapseOne"
                                                                    class="text-left m-0 p-0 btn btn-link btn-block text-dark text-decoration-none">
                                                                <h6 class="m-0 p-0">Orders</h6>
                                                            </button>
                                                        </div>
                                                        <div data-parent="#accordion" id="collapseOne1"
                                                             aria-labelledby="headingOne"
                                                             class="collapse">
                                                            <div class="card-body pre-scrollable">
                                                                @if($reportsData['fieldArray'] && $reportsData['fieldArray']['Order'])
                                                                    @foreach($reportsData['fieldArray']['Order'] as $key => $value)
                                                                        <div class="position-relative form-check form-check-inline d-block">
                                                                            <label class="form-check-label text-truncate">
                                                                                <input type="checkbox"
                                                                                       name="selectField[]"
                                                                                       class="form-check-input"
                                                                                       value="{{$key}}"> {{ $reportsData['fieldArray']['Order'][$key]['label']}}
                                                                            </label>
                                                                        </div>
                                                                    @endforeach
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card mb-0">
                                                        <div id="headingTwo" class="b-radius-0 card-header alert-dark" style="min-height:0;">
                                                            <button type="button" data-toggle="collapse"
                                                                    data-target="#collapseOne2"
                                                                    aria-expanded="false" aria-controls="collapseTwo"
                                                                    class="text-left m-0 p-0 btn btn-link btn-block text-dark text-decoration-none">
                                                                <h6 class="m-0 p-0">Order Items</h6>
                                                            </button>
                                                        </div>
                                                        <div data-parent="#accordion" id="collapseOne2"
                                                             class="collapse">
                                                            <div class="card-body pre-scrollable">
                                                                @if($reportsData['fieldArray'] && $reportsData['fieldArray']['Order Item'])
                                                                    @foreach($reportsData['fieldArray']['Order Item'] as $key => $value)
                                                                        <div class="position-relative form-check form-check-inline d-block">
                                                                            <label class="form-check-label text-truncate">
                                                                                <input type="checkbox"
                                                                                       name="selectField[]"
                                                                                       class="form-check-input"
                                                                                       value="{{$key}}"> {{ $reportsData['fieldArray']['Order Item'][$key]['label']}}
                                                                            </label>
                                                                        </div>
                                                                    @endforeach
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card mb-0">
                                                        <div id="headingThree" class="b-radius-0 card-header alert-dark" style="min-height:0;">
                                                            <button type="button" data-toggle="collapse"
                                                                    data-target="#collapseOne3"
                                                                    aria-expanded="false" aria-controls="collapseThree"
                                                                    class="text-left m-0 p-0 btn btn-link btn-block text-dark text-decoration-none">
                                                                <h6 class="m-0 p-0">Customer</h6>
                                                            </button>
                                                        </div>
                                                        <div data-parent="#accordion" id="collapseOne3"
                                                             class="collapse">
                                                            <div class="card-body pre-scrollable">
                                                                @if($reportsData['fieldArray'] && $reportsData['fieldArray']['Customer'])
                                                                    @foreach($reportsData['fieldArray']['Customer'] as $key => $value)
                                                                        <div class="position-relative form-check form-check-inline d-block">
                                                                            <label class="form-check-label text-truncate">
                                                                                <input type="checkbox"
                                                                                       name="selectField[]"
                                                                                       class="form-check-input"
                                                                                       value="{{$key}}"> {{ $reportsData['fieldArray']['Customer'][$key]['label']}}
                                                                            </label>
                                                                        </div>
                                                                    @endforeach
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="main-card mb-3 card">
                                            <div class="card-header alert-primary">Conditions</div>
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="timelimit" class="">Time Limit</label>
                                                    <select name="conditions[0][timeLimit]" id="timelimit"
                                                            class="form-control">
                                                        <option value="">No Limit</option>
                                                        @foreach($reportsData['timeArray'] as $key => $value)
                                                            <option value="{{$key}}">{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group" id="daterangeBox">
                                                    <label for="daterange" class="">Date Range</label>
                                                    <input type="text" name="conditions[0][dateRange]" id="daterange"
                                                           class="form-control" value=""/>
                                                </div>
                                                @include('admin.reports.partials.conditions', [1])
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" class="mt-2 btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link href="//cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.11/combined/css/gijgo.css" rel="stylesheet" type="text/css"/>
    <link href="{{ asset("global/plugins/multiselect/fSelect.css") }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('global/plugins/select2/select2.min.css') }}">
    <style>
        .fs-wrap {
            display: block;
        }

        .fs-label-wrap {
            padding: 6px;
        }

        .fs-dropdown .fs-options {
            max-width: 100% !important;
        }

        /*.fs-option, .fs-search, .fs-optgroup-label {*/
        /*    padding: 12px 8px;*/
        /*}*/
        .fs-option {
            line-height: 1.75 !important;
        }
    </style>
@endsection
@section('footer_scripts')
    <script src="{{ asset("global/plugins/multiselect/fSelect.js") }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="{{ asset('global/plugins/select2/select2.min.js') }}"></script>
    <script src="{{ asset("admin/modules/reports/index.js") }}"></script>
    <script type="text/javascript">
        var report_id = null;
        $(function () {

            $('#daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('#daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            });

            $('#daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#daterangeBox').hide();


            $("#timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#daterange').val('');
                    $('#daterangeBox').show();
                }
                else {
                    $('#daterange').val('');
                    $('#daterangeBox').hide();
                }
            });

        });
    </script>
@endsection


