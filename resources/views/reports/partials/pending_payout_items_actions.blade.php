<a title="View">
    <button class="btn btn-primary btn-sm showOrderItems" data-shop-id="{{ $data->shop_id }}">
        <i class="fa fa-eye" aria-hidden="true"></i> View
    </button>
</a>
<a href="{{ route('admin.pending_payout_reports.update_payout', $data->shop_id) }}" title="Pay">
    <button class="btn btn-success btn-sm">
        Pay
    </button>
</a>