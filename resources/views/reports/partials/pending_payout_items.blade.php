<table class="table">
    <thead>
    <tr class="bg-dark text-white">
        <th>Order</th>
        <th>Product</th>
        <th>Customer</th>
        <th>Price</th>
        <th>Quantity</th>
    </tr>
    </thead>
    <tbody>
    @foreach($pending_payment_items as $item)
        <tr>
            <td>{{ $item->order_id }}</td>
            <td>
                <div class="row" title="{{ $item->product_name }}">
                    <div class="col-md-12">
                        <img src="{{ showProductImage($item->product->product_image) }}" class="d-inline-block valign-top" width="50px" alt="Image" >
                        <h6 class="d-inline-block pl-2">
                            {{ str_limit($item->product_name, 20) }}
                        </h6>
                    </div>
                </div>
            </td>
            <td>{{ $item->order->customer->first_name }}</td>
            <td>
                <div class="border-bottom"><b>Total : </b>{{ formatPrice($item->sub_total) }}</div>
                <b>Commission : </b>{{ formatPrice($item->itemEarningDetails->system_commission_value) }}
            </td>
            <td>{{ $item->quantity }}</td>
        </tr>
    @endforeach
    </tbody>
</table>