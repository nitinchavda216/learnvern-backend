@php
    $select2_class = ($attribute_detail['control_type'] == 'color_picker') ? 'select2_integrate_for_color' : 'select2_integrate';
@endphp
<label>Value</label>
{!! Form::select("conditions[$key][attribute_values][]", $attribute_values,((isset($selectedData['conditions'][$key]) && isset($selectedData['conditions'][$key]['attribute_values'])) ? $selectedData['conditions'][$key]['attribute_values'] : ''),['class'=>'form-control '.$select2_class,'style'=>'width:100%;','multiple'=>'multiple'])!!}
<script>
    $(".select2_integrate").select2({
        tags: true,
        placeholder: 'Select Options ...',
    });
    function formatColor(state) {
        if (!state.id) {
            return state.text;
        }
        var $state = $(
            '<span style="background-color: ' + state.id + '" class="color-label"></span><span>' + state.text + '</span>'
        );
        return $state;
    }
    ;

    $('.select2_integrate_for_color').select2({
        placeholder: "Select a color",
        templateResult: formatColor,
        templateSelection: formatColor
    });
</script>