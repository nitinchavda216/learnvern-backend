@php
    $element_type = isset($conditions['element_type']) ? $conditions['element_type'] : $element_type;
@endphp
<div class="conditionsData removeRow">
    @if($element_type == 'brand')
        <div class="p-2 mt-2 posi-col">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-orange">Brand</h5>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Condition</label>
                        {!! Form::select("conditions[$key][identifier]", $reportsData['identifier'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['identifier'] : ''), ['class' => 'elementIdentifier form-control']) !!}
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-group elementDiv">
                        <label>Value</label>
                        {!! Form::select("conditions[$key][values][]",$condition_value['brand_list'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['values'] : ''),['class'=>'elementValue form-control',(isset($selectedData['conditions'][$key]) && ($selectedData['conditions'][$key]['identifier'] == 'is_one_of' || $selectedData['conditions'][$key]['identifier'] == 'is_not_one_of'))?'multiple':'']) !!}
                    </div>
                </div>
            </div>
            <a class="btn btn-sm btn-danger removeConditionRule pull-right" href="javascript:;">
                <i class="fa fa-trash"></i>
            </a>
        </div>
    @elseif($element_type == 'category')
        <div class="p-2 mt-2 posi-col">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-orange">Category</h5>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Condition</label>
                        {!! Form::select("conditions[$key][identifier]", $reportsData['identifier'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['identifier'] : ''), ['class' => 'elementIdentifier form-control']) !!}
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-group elementDiv">
                        <label>Value</label>
                        {!! Form::select("conditions[$key][values][]", $condition_value['category_list'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['values'] : ''), ['class' => 'elementValue form-control',(isset($selectedData['conditions'][$key]) && ($selectedData['conditions'][$key]['identifier'] == 'is_one_of' || $selectedData['conditions'][$key]['identifier'] == 'is_not_one_of'))?'multiple':'']) !!}
                    </div>
                </div>
            </div>
            <a class="btn btn-sm btn-danger removeConditionRule pull-right" href="javascript:;">
                <i class="fa fa-trash"></i>
            </a>
        </div>
    @elseif($element_type == 'attribute')
        <div class="p-2 mt-2 posi-col">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-orange">Attributes</h5>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Condition</label>
                        {!! Form::select("conditions[$key][identifier]", $reportsData['attribute_identifier'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['identifier'] : ''), ['class' => 'elementIdentifier form-control']) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group elementDiv">
                        <label>Select Attribute</label>
                        {!! Form::select("conditions[$key][values]", $condition_value['attribute_list'], (isset($selectedData['conditions'][$key]) ?$selectedData['conditions'][$key]['values'] : ''), ['class' => 'elementValue attributeTitleValue form-control', 'data-key' => $key]) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group elementDiv attributeValueField"></div>
                </div>
            </div>
            <a class="btn btn-sm btn-danger removeConditionRule pull-right" href="javascript:;">
                <i class="fa fa-trash"></i>
            </a>
        </div>
    @elseif($element_type == 'product')
        <div class="p-2 mt-2 posi-col">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-orange">Products</h5>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Condition</label>
                        {!! Form::select("conditions[$key][identifier]", $reportsData['identifier'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['identifier'] : ''), ['class' => 'elementIdentifier form-control']) !!}
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-group elementDiv">
                        <label>Value</label>
                        {!! Form::select("conditions[$key][values][]", $condition_value['product_list'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['values'] : ''), ['class' => 'elementValue form-control',(isset($selectedData['conditions'][$key]) && ($selectedData['conditions'][$key]['identifier'] == 'is_one_of' || $selectedData['conditions'][$key]['identifier'] == 'is_not_one_of'))?'multiple':'']) !!}
                    </div>
                </div>
            </div>
            <a class="btn btn-sm btn-danger removeConditionRule pull-right" href="javascript:;">
                <i class="fa fa-trash"></i>
            </a>
        </div>
    @elseif($element_type == 'customer')
        <div class="p-2 mt-2 posi-col">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-orange">Customer</h5>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Condition</label>
                        {!! Form::select("conditions[$key][identifier]", $reportsData['identifier'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['identifier'] : ''), ['class' => 'elementIdentifier form-control']) !!}
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-group elementDiv">
                        <label>Value</label>
                        {!! Form::select("conditions[$key][values][]", $condition_value['customer_list'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['values'] : ''), ['class' => 'elementValue form-control',(isset($selectedData['conditions'][$key]) && ($selectedData['conditions'][$key]['identifier'] == 'is_one_of' || $selectedData['conditions'][$key]['identifier'] == 'is_not_one_of'))?'multiple':'']) !!}
                    </div>
                </div>
            </div>
            <a class="btn btn-sm btn-danger removeConditionRule pull-right" href="javascript:;">
                <i class="fa fa-trash"></i>
            </a>
        </div>
    @elseif($element_type == 'shopowner')
        <div class="p-2 mt-2 posi-col">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-orange">Shop</h5>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Condition</label>
                        {!! Form::select("conditions[$key][identifier]", $reportsData['identifier'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['identifier'] : ''), ['class' => 'elementIdentifier form-control']) !!}
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="form-group elementDiv">
                        <label>Value</label>
                        {!! Form::select("conditions[$key][values][]", $condition_value['shop_list'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['values'] : ''), ['class' => 'elementValue form-control',(isset($selectedData['conditions'][$key]) && ($selectedData['conditions'][$key]['identifier'] == 'is_one_of' || $selectedData['conditions'][$key]['identifier'] == 'is_not_one_of'))?'multiple':'']) !!}
                    </div>
                </div>
            </div>
            <a class="btn btn-sm btn-danger removeConditionRule pull-right" href="javascript:;">
                <i class="fa fa-trash"></i>
            </a>
        </div>
    @else
    @endif
    <div class="col-12">
        <input type="hidden" name="conditions[{{$key}}][element_type]" class="hiddenElementType form-control" value="{{ $element_type }}"/>
    </div>
</div>
