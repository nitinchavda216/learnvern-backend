@if($courseUser->courseDetail->course_type != 4)
    @if($courseUser->paid_status == 1)
        <a href="javascript:void(0);" class="btn btn-dark btn-xs disabled">Activated</a>
    @elseif($courseUser->courseDetail->is_active == 0)
        Inactive Course
    @else
        <a href="javascript:void(0);" class="btn btn-success btn-xs" onclick="return doPayment({{ $courseUser->id }});">Activate
            Certificate</a>
        @if(env('RAZORPAY_ENABLE') && ($courseUser->partner_id == 1))
            <a href="javascript:;"
               class="btn @if(isset($courseUser->customPaymentRequest)) btn-primary @else btn-info @endif btn-xs sendPaymentLink"
               data-id="{{ $courseUser->id }}">
                @if(isset($courseUser->customPaymentRequest)) Resend Payment Link @else Send Payment Link @endif
            </a>
        @endif
    @endif
@else
    - - -
@endif
