@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title"> Un-Enrolled User Report List
                    <span class="pull-right d-inline-block float-right">
                        <a href="javascript:;" class="btn btn-success btn-sm mt-negative"
                           onclick="this.href='un-enrolled-users/export?register_date=' + document.getElementById('register_date').value + '&searchData=' + document.getElementById('searchData').value + '&partner_user_id=' + document.getElementById('partner_user_id').value">
                            <i class="fa fa-download" aria-hidden="true"></i> Export
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-3">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Search:</label>
                                <input name="searchData" id="searchData" type="text" class="form-control"
                                       placeholder="Search">
                            </div>
                            @if(session('partner_id') == 1)
                                <div class="col-md-3">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Partners:</label>
                                    {!! Form::select('partner_user_id', $partners, null,['id' => 'partner_user_id', 'class' => 'form-control select2']) !!}
                                </div>
                            @endif
                            <div class="col-md-3">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Registration Date Range:</label>
                                <input name="register_date" id="register_date" type="text" class="form-control"
                                       autocomplete="off" placeholder="Select Date">
                            </div>
                            <div class="col-md mg-t-10 mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <table id="dataTableList" class="table table-responsive">
                        <thead>
                        <tr>
                            <th>User Details</th>
                            <th>Current Occupation</th>
                            <th>Sign Up From</th>
                            <th>Affiliate Code</th>
                            <th>Register Date</th>
                            <th>Last Login Date</th>
                            <th>Email Verify</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#register_date').daterangepicker({
                autoUpdateInput: true,
                startDate: moment().subtract(6, 'days'),
                endDate: moment(),
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY'
                },
                ranges: {
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            $('#register_date').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            });
            $('#register_date').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });
            if ($('#dataTableList').length) {
                var $dataTableList = $('#dataTableList').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    bFilter: false,
                    "aaSorting": [],
                    ajax: {
                        url: $app_url + '/un-enrolled-users/getAjaxListData',
                        data: function (d) {
                            d.register_date = $('#register_date').val();
                            d.searchData = $('#searchData').val();
                            d.partner_user_id = $('#partner_user_id').val();
                        }
                    },
                    "columns": [
                        {data: 'user_details', 'sortable': false, 'width': "15%"},
                        //{data: 'first_name', 'sortable': false, 'width': "10%"},
                        //{data: 'mobile_number', 'sortable': false, 'width': "15%"},
                        {data: 'current_occupation', 'sortable': false, 'width': "10%"},
                        {data: 'sign_up_from', 'sortable': false, 'width': "10%"},
                        {data: 'referral', 'sortable': false, 'width': "10%"},
                        {data: 'registration_date', 'sortable': false, 'width': "10%"},
                        {data: 'last_login_date', 'sortable': false, 'width': "10%"},
                        {data: 'email_verify', 'sortable': false, 'width': "10%"}
                    ],
                    select: true,
                    bStateSave: true,
                    fnStateSave: function (settings, data) {
                        localStorage.setItem("dataTables_state", JSON.stringify(data));
                    },
                    fnStateLoad: function (settings) {
                        return JSON.parse(localStorage.getItem("dataTables_state"));
                    }
                });
                $('#search-form').on('submit', function (e) {
                    $dataTableList.draw();
                    e.preventDefault();
                });
                $('#reset-filters').on('click', function () {
                    $("#searchData").val(null).trigger('change');
                    $("#register_date").val(null).trigger('change');
                    $("#partner_user_id").val(1).trigger('change');
                    $dataTableList.draw();
                });
            }
        });
    </script>
@endsection

