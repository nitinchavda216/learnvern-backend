@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> User Month Vise Report</h2>
                <div id="data-container">
                    {!! Form::open(['method' => 'GET', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-4">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Year:</label>
                                {{ Form::selectRange('year', 2016, \Carbon\Carbon::now()->year, request('year') ?? \Carbon\Carbon::now()->year,['id' => 'datepicker', 'class'=>'form-control']) }}
                            </div>
                            <div class="col-md-2 mt-4">
                                <button type="submit" class="btn btn-az-success btn-success" id="submit">Search</button>
                                <a href="{{ route('user-month-vise-report') }}" class="btn btn-az-warning btn-warning">Reset
                                </a>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="col-sm-12">
                        <div class="">
                            <canvas id="chartBar1" style="height: 440px; width: 100%;"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
@endsection
@section('footer_scripts')
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="{{asset('admin/lib/chart.js/Chart.bundle.min.js')}}"></script>
    <script type="text/javascript">
        var labels = JSON.parse('{!!  $data['months'] !!}');
        var counts = JSON.parse('{!!  $data['counts'] !!}');
        var referral_users = JSON.parse('{!!  $data['referral_users'] !!}');
        $(function () {
            'use strict';
            var ctx1 = document.getElementById('chartBar1').getContext('2d');
            new Chart(ctx1, {
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: [{
                        label: "Referrals",
                        data: referral_users,
                        backgroundColor: '#332e5f',
                        borderWidth: 1,
                        fill: true
                    },{
                        label: "Users",
                        data: counts,
                        backgroundColor: '#007bff',
                        borderWidth: 1,
                        fill: true
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    legend: {
                        display: true,
                        labels: {
                            display: true
                        }
                    },
                    scales: {
                        yAxes: [{
                            stacked: true,
                            ticks: {
                                beginAtZero:true,
                                fontSize: 11
                            }
                        }],
                        xAxes: [{
                            barPercentage: 0.5,
                            stacked: true,
                            ticks: {
                                fontSize: 11
                            }
                        }]
                    }
                }
            });
        });
    </script>
@endsection
