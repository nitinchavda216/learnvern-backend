@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title"> Course Report List
                    <span class="pull-right d-inline-block float-right">
                        <a href="javascript:;" class="btn btn-success btn-sm mt-negative"
                           onclick="this.href='course-report/getAjaxExportData?course_status=' + document.getElementById('course_status').value + '&search=' + document.getElementById('search').value + '&register_date=' + document.getElementById('register_date').value + '&course_id=' + document.getElementById('course_id').value + '&partner_user_id=' + document.getElementById('partner_user_id').value">
                            <i class="fa fa-download" aria-hidden="true"></i> Export
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Search:</label>
                                <input name="search" id="search" type="text" class="form-control" placeholder="Search">
                            </div>
                            <div class="col-md-3">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Course :</label>
                                {!! Form::select('course_id', $courses, [],['id' => 'course_id', 'class' => 'form-control select2', 'placeholder' => ""]) !!}
                            </div>
                            @if(session('partner_id') == 1)
                                <div class="col-md-3">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Partners:</label>
                                    {!! Form::select('partner_user_id', $partners, null,['id' => 'partner_user_id', 'class' => 'form-control select2']) !!}
                                </div>
                            @endif
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Registration Date Range:</label>
                                <input name="register_date" id="register_date" type="text" class="form-control"
                                       autocomplete="off" placeholder="Select Date">
                            </div>
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Status</label>
                                <select name="course_status" class="form-control select2" id="course_status">
                                    <option></option>
                                    <option value="0-20">0-20</option>
                                    <option value="20-40">20-40</option>
                                    <option value="40-60">40-60</option>
                                    <option value="60-80">60-80</option>
                                    <option value="80-99">80-99</option>
                                    <option value="100">100</option>
                                </select>
                            </div>
                        </div>
                        <div class="row row-xs">
                            <div class="col-md-12 mt-4 text-right">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <table id="dataTableList" class="table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>User Details</th>
                            <th>Course Enroll Date</th>
                            <th>Course Status</th>
                            <th>Certificate Type</th>
                            <th>Certificate Date</th>
                            <th>Last Used Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="nsdcActivateModal" class="modal">
        {!! Form::open(['route'=> 'do-manual-payment','method' => 'POST', 'id' => 'direct-payment']) !!}
        <input type="hidden" name="payment_course_user_id" id="payment_course_user_id" value=""/>
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Activate Certificate</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Payment Method: </label>
                                {!! Form::select('payment_method', \App\Models\CourseUser::PAYMENT_METHOD_TYPE, '', ['class'=>'form-control select2Input', 'id' => 'payment_method', 'placeholder' => "Select Payment Method",'data-validation' => 'required']) !!}
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Referance ID: </label>
                                {!! Form::text('referance_id','', ['class'=>'form-control', 'id' => 'referance_id','data-validation' => 'required']) !!}
                            </div>
                        </div>
                    </div>
                </div><!-- modal-body -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Activate</button>
                </div>
            </div>
        </div><!-- modal-dialog -->
        {!! Form::close() !!}
    </div>

@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="{{asset('admin/js/jquery.blockUI.js')}}"></script>
    <script type="text/javascript" src="{{ asset('admin/modules/reports/course.js') }}"></script>
    <script type="text/javascript">
        $.validate({
            modules: 'security'
        });
        function doPayment(id) {
            $("#payment_course_user_id").val(id);
            $('#nsdcActivateModal').modal('toggle');
        }
    </script>
@endsection

