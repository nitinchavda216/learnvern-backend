@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title"> Course Status Report List
                    <span class="pull-right d-inline-block float-right">
                        <a href="javascript:;" class="btn btn-success btn-sm mt-negative"
                           onclick="return exportExcelSheet();">
                             <i class="fa fa-download" aria-hidden="true"></i> Export
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-3">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Course :</label>
                                {!! Form::select('course_id', $courses, [],['id' => 'course_id', 'class' => 'form-control select2', 'placeholder' => ""]) !!}
                            </div>
                            @if(session('partner_id') == 1)
                                <div class="col-md-3">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Partners:</label>
                                    {!! Form::select('partner_user_id', $partners, null,['id' => 'partner_user_id', 'class' => 'form-control select2']) !!}
                                </div>
                            @endif
                            <div class="col-md-3">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Enroll Date Range:</label>
                                <input name="date_range" id="date_range" type="text" class="form-control"
                                       autocomplete="off" placeholder="Select Date">
                            </div>
                            <div class="col-md mg-t-10 mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <table id="dataTableList" class="table table-responsive">
                        <thead>
                        <tr>
                            <th>Course Name</th>
                            <th>0-20%</th>
                            <th>21-40%</th>
                            <th>41-60%</th>
                            <th>61-80%</th>
                            <th>81-99%</th>
                            <th>Completed 100%</th>
                            <th>Total User Count</th>
                            <th>Certificate Purchased Count</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="{{ asset('admin/modules/reports/courseStatus.js') }}"></script>
    <script>
       function exportExcelSheet(){
            var url = 'course-status-report/export?date_range='+document.getElementById('date_range').value+'&partner_user_id='+document.getElementById('partner_user_id').value+'&course_id='+document.getElementById('course_id').value;
           window.location.href =url;
           return true;
       }
    </script>
@endsection

