@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Certificate Purchase Users</h2>
                <div id="data-container">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-3">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Course :</label>
                                {!! Form::select('course_id', $courses, request('course_id'),['id' => 'course_id', 'class' => 'form-control select2', 'placeholder' => ""]) !!}
                            </div>
                            @if(session('partner_id') == 1)
                                <div class="col-md-3">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Partners:</label>
                                    {!! Form::select('partner_user_id', $partners, request('partner_id'),['id' => 'partner_user_id', 'class' => 'form-control select2']) !!}
                                </div>
                            @endif
                            <div class="col-md-3">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Enroll Date
                                    Range:</label>
                                <input name="enroll_date_range" id="enroll_date_range" type="text" class="form-control"
                                       autocomplete="off" placeholder="Select Date"
                                       value="{{ request('enroll_date_range') }}">
                            </div>
                            <div class="col-md-3">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Transaction Date:</label>
                                <input name="transaction_date" id="transaction_date" type="text" class="form-control"
                                       autocomplete="off" placeholder="Select Date">
                            </div>
                            <div class="col-md mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                @if(request('partner_user_id') != null)
                                    <a href="{{ route('course-status-report.getCertificatePurchaseUsers') }}" class="btn btn-az-warning btn-warning" id="reset-filters">
                                        Reset
                                    </a>
                                @else
                                    <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">
                                        Reset
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile Number</th>
                            <th>Referral Code</th>
                            <th>Transaction Date</th>
                            <th>Amount</th>
                            <th>Course Name</th>
                            <th>Reference ID</th>
                            <th>Payment Method</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#transaction_date').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY'
                },
                ranges: {
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            $('#transaction_date').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            });
            $('#transaction_date').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#enroll_date_range').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY'
                },
                ranges: {
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            $('#enroll_date_range').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            });
            $('#enroll_date_range').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            var $dataTableList = $('#dataTableList').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                bFilter: false,
                "aaSorting": [],
                ajax: {
                    url: $app_url + '/course-status-report/certificate-purchase-users/getAjaxData',
                    data: function (d) {
                        d.course_id = $('#course_id').val();
                        d.transaction_date = $('#transaction_date').val();
                        d.enroll_date_range = $('#enroll_date_range').val();
                        d.partner_user_id = $('#partner_user_id').val();
                    }
                },
                "columns": [
                    {data: 'name', 'sortable': false, 'width': "10%"},
                    {data: 'email', 'sortable': false, 'width': "15%"},
                    {data: 'mobile_number', 'sortable': false, 'width': "10%"},
                    {data: 'referral_code', 'sortable': false, 'width': "10%"},
                    {data: 'transaction_date', 'sortable': false, 'width': "10%"},
                    {data: 'amount', 'sortable': false, 'width': "8%"},
                    {data: 'course', 'sortable': false, 'width': "15%"},
                    {data: 'payment_source', 'sortable': false, 'width': "10%"},
                    {data: 'payment_method', 'sortable': false, 'width': "10%"}
                ],
                select: true,
                bStateSave: true,
                fnStateSave: function (settings, data) {
                    localStorage.setItem("dataTables_state", JSON.stringify(data));
                },
                fnStateLoad: function (settings) {
                    return JSON.parse(localStorage.getItem("dataTables_state"));
                }
            });

            $('#search-form').on('submit', function (e) {
                $dataTableList.draw();
                e.preventDefault();
            });

            $('#reset-filters').on('click', function () {
                $("#course_id").val(null).trigger('change');
                $("#transaction_date").val(null);
                $("#enroll_date_range").val(null);
                $("#partner_user_id").val(1).trigger('change');
                $dataTableList.draw();
            });
        });
    </script>
@endsection

