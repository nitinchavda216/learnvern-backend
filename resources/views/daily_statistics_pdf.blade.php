<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {{--    <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">--}}
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daily Statistics</title>
    <style media="all">
        @page {
            margin: 0;
        }

        body {
            margin: 0;
            padding: 0;
            border: 0;
            font-family: "Poppins", sans-serif;
        }
    </style>
</head>
<body style="margin:0;padding:0;font-family: 'Poppins', sans-serif;background-color:#FFFFFF;">
<!--Header-->
<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#03386f;">
    <tr>
        <td align="left" style="background-color:#03386f;padding:20px 20px;">
            <img src="https://www.learnvern.com/images/logo.png" style="width:180px;">
        </td>
        <td align="right" style="background-color:#03386f;color:#FFF;padding:20px 20px;font-weight:bold;">
            <div style="text-align:center;max-width:80px;margin-left:auto;margin-right:0;">
                Date:
                <br>
                {{ formatDate($reportDate, 'd/m/Y') }}
            </div>
        </td>
    </tr>
</table>
<!--/Header-->

<!--Registered User Section-->
<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#FFFFFF;">
    <tr>
        <td align="left" style="padding:30px 20px 10px 20px;">
            <h3 style="color:#000000;font-weight:bold;margin:0;padding:0;">Registered Users</h3>
        </td>
    </tr>
    <tr>
        <td style="padding:0 20px;">
            <table width="100%" border="0.5" cellpadding="10" cellspacing="0">
                <tr>
                    <th align="center" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:12px;font-weight:bold;">NSDC_signups
                    </th>
                    <th align="center" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:12px;font-weight:bold;">google+_web
                    </th>
                    <th align="center" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:12px;font-weight:bold;">
                        google+_android
                    </th>
                    <th align="center" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:12px;font-weight:bold;">
                        facebook_android
                    </th>
                    <th align="center" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:12px;font-weight:bold;">
                        manual_signup_web
                    </th>
                    <th align="center" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:12px;font-weight:bold;">
                        manual_signup_android
                    </th>
                </tr>
                <tr>
                    <td align="center" valign="center"
                        style="color:#000;font-size:14px;">{{ $pdfData['registered_users']['NSDC_signups'] }}</td>
                    <td align="center" valign="center"
                        style="color:#000;font-size:14px;">{{ $pdfData['registered_users']['google+_web'] }}</td>
                    <td align="center" valign="center"
                        style="color:#000;font-size:14px;">{{ $pdfData['registered_users']['google+_android'] }}</td>
                    <td align="center" valign="center"
                        style="color:#000;font-size:14px;">{{ $pdfData['registered_users']['facebook_android'] }}</td>
                    <td align="center" valign="center"
                        style="color:#000;font-size:14px;">{{ $pdfData['registered_users']['manual_signup_web'] }}</td>
                    <td align="center" valign="center"
                        style="color:#000;font-size:14px;">{{ $pdfData['registered_users']['manual_signup_android'] }}</td>
                </tr>
                <tr>
                    <td align="left" valign="center" colspan="5" style="color:#000;font-size:15px;font-weight:bold;">
                        Total
                    </td>
                    <td align="right" valign="center"
                        style="color:#000;font-size:15px;font-weight:bold;">{{ $pdfData['registered_users']['total'] }}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!--/Registered User Section-->

<!--Certificate Sales Section-->
<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#FFFFFF;">
    <tr>
        <td align="left" style="padding:30px 20px 10px 20px;">
            <h3 style="color:#000000;font-weight:bold;margin:0;padding:0;">Certificate Sales</h3>
        </td>
    </tr>
    <tr>
        <td style="padding:0 20px;">
            <table width="100%" border="0.5" cellpadding="10" cellspacing="0">
                <tr>
                    <th width="50%" align="center" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:12px;font-weight:bold;">Organic Sell
                    </th>
                    <th width="50%" align="center" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:12px;font-weight:bold;">Campaign Sell
                    </th>
                </tr>
                <tr>
                    <td width="50%" align="center" valign="center"
                        style="color:#000;font-size:14px;">{{ $pdfData['sales_statistics']['organic_sell'] }}</td>
                    <td width="50%" align="center" valign="center"
                        style="color:#000;font-size:14px;">{{ $pdfData['sales_statistics']['campaign_sell'] }}</td>
                </tr>
                <tr>
                    <td align="left" valign="center" colspan="1" style="color:#000;font-size:15px;font-weight:bold;">
                        Total
                    </td>
                    <td align="right" valign="center"
                        style="color:#000;font-size:15px;font-weight:bold;">{{ $pdfData['total_certificate_sales'] }}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!--/Certificate Sales Section-->
<!--CRM Users Sales Section-->
<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#FFFFFF;">
    <tr>
        <td align="left" style="padding:30px 20px 10px 20px;">
            <h3 style="color:#000000;font-weight:bold;margin:0;padding:0;">CRM Users Sales Reports</h3>
        </td>
    </tr>
</table>
<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#FFFFFF;">
    <tr>
        <td style="padding:0 20px;">
            <table width="100%" border="0.5" cellpadding="10" cellspacing="0">
                <tr>
                    <th width="70%" align="left" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:14px;font-weight:bold;">Name
                    </th>
                    <th width="30%" align="center" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:14px;font-weight:bold;">Certificate
                        Sell
                    </th>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table align="center" width="100%" border="0.5" cellpadding="5" cellspacing="0"
       style="background-color:#FFFFFF;margin:0 20px 0 20px;border-top:0;">
    @forelse($pdfData['crm_users_history'] as $cmsUserHistory)
        <tr>
            <td width="70%" align="left" valign="center"
                style="color:#000;font-size:12px;border-top:0;">{{ $cmsUserHistory['name'] }}</td>
            <td width="30%" align="center" valign="center"
                style="color:#000;font-size:14px;border-top:0;">{{ $cmsUserHistory['sales_count'] }}</td>
        </tr>
    @empty
        <tr>
            <td colspan="2" align="center" valign="center"
                style="color:#000;font-size:15px;border-top:0;">No Record Found.
            </td>
        </tr>
    @endforelse
</table>
<!--/CRM Users Sales Section-->
<!--Anonymous users Section-->
<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#FFFFFF;">
    <tr>
        <td style="padding:30px 20px 0px 20px">
            <table width="100%" border="1" cellpadding="0" cellspacing="0"
                   style="border-radius:10px 10px 0px 0px;overflow:hidden;">
                <tr>
                    <th width="50%" align="center" valign="center">
                        <div
                            style="background-color:#03386f;color:#fff;width:100%;padding:10px;font-size:14px;font-weight:bold;">
                            Anonymous Users
                        </div>
                        <div style="color:#000;font-size:14px;padding-top:30px;font-weight:normal;">
                            {{ $pdfData['anonymous_users'] }}
                        </div>
                    </th>
                    <th width="50%" align="center" valign="center">
                        <div
                            style="background-color:#03386f;color:#fff;width:100%;padding:10px;font-size:14px;font-weight:bold;">
                            Total Referral
                        </div>
                        <table width="100%" border="0" cellpadding="10" cellspacing="0">
                            <tr>
                                <th width="50%" align="center" valign="center"
                                    style="background-color:#76bdee24;color:#03386f;font-size:12px;font-weight:bold;">
                                    Active
                                </th>
                                <th width="50%" align="center" valign="center"
                                    style="background-color:#76bdee24;color:#03386f;font-size:12px;font-weight:bold;">
                                    Inactive
                                </th>
                            </tr>
                            <tr>
                                <td width="50%" align="center" valign="center"
                                    style="color:#000;font-size:14px;font-weight:normal;">{{ $pdfData['referral']['active'] }}
                                </td>
                                <td width="50%" align="center" valign="center"
                                    style="color:#000;font-size:14px;font-weight:normal;">{{ $pdfData['referral']['inactive'] }}
                                </td>
                            </tr>
                        </table>
                    </th>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!--/Anonymous users Section-->
<!--Course Enrollments Section-->
<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#FFFFFF;">
    <tr>
        <td align="left" style="padding:30px 20px 10px 20px;">
            <h3 style="color:#000000;font-weight:bold;margin:0;padding:0;">Course Enrollments</h3>
        </td>
    </tr>
</table>
<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#FFFFFF;">
    <tr>
        <td style="padding:0 20px;">
            <table width="100%" border="0.5" cellpadding="10" cellspacing="0">
                <tr>
                    <th width="60%" align="left" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:14px;font-weight:bold;">Title
                    </th>
                    <th width="20%" align="center" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:14px;font-weight:bold;">Enroll Count
                    </th>
                    <th width="20%" align="center" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:14px;font-weight:bold;">Certificate
                        Sell
                    </th>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table align="center" width="100%" border="0.5" cellpadding="5" cellspacing="0"
       style="background-color:#FFFFFF;margin:0 20px 0 20px;border-top:0;">
    @forelse($pdfData['course_enrollments']['courses'] as $courseEnrollment)
        <tr>
            <td width="60%" align="left" valign="center"
                style="color:#000;font-size:12px;border-top:0;">{{ $courseEnrollment['title'] }}</td>
            <td width="20%" align="center" valign="center"
                style="color:#000;font-size:14px;border-top:0;">{{ $courseEnrollment['total_count'] }}</td>
            <td width="20%" align="center" valign="center"
                style="color:#000;font-size:14px;border-top:0;">{{ $courseEnrollment['certificate_sales'] }}</td>
        </tr>
    @empty
        <tr>
            <td colspan="3" align="center" valign="center"
                style="color:#000;font-size:15px;border-top:0;">No Record Found.
            </td>
        </tr>
    @endforelse
</table>
<!--/Course Enrollments Section-->
<!--Webinar Enrollments Section-->
<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#FFFFFF;">
    <tr>
        <td align="left" style="padding:30px 20px 10px 20px;">
            <h3 style="color:#000000;font-weight:bold;margin:0;padding:0;">Webinar Enrollments</h3>
        </td>
    </tr>
</table>
<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#FFFFFF;">
    <tr>
        <td style="padding:0 20px;">
            <table width="100%" border="0.5" cellpadding="10" cellspacing="0">
                <tr>
                    <th width="60%" align="left" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:14px;font-weight:bold;">Title
                    </th>
                    <th width="20%" align="center" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:14px;font-weight:bold;">Enroll Count
                    </th>
                    <th width="20%" align="center" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:14px;font-weight:bold;">Certificate
                        Sell
                    </th>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table align="center" width="100%" border="0.5" cellpadding="5" cellspacing="0"
       style="background-color:#FFFFFF;margin:0 20px 0 20px;border-top:0;">
    @forelse($pdfData['course_enrollments']['webinars'] as $webinarEnrollment)
        <tr>
            <td width="60%" align="left" valign="center"
                style="color:#000;font-size:12px;border-top:0;">{{ $webinarEnrollment['title'] }}</td>
            <td width="20%" align="center" valign="center"
                style="color:#000;font-size:14px;border-top:0;">{{ $webinarEnrollment['total_count'] }}</td>
            <td width="20%" align="center" valign="center"
                style="color:#000;font-size:14px;border-top:0;">{{ $webinarEnrollment['certificate_sales'] }}</td>
        </tr>
    @empty
        <tr>
            <td colspan="3" align="center" valign="center"
                style="color:#000;font-size:15px;border-top:0;">No Record Found.
            </td>
        </tr>
    @endforelse
</table>
<!--/Course Enrollments Section-->
</body>
</html>
