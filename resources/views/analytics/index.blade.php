@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body" style="overflow-x: auto">
                <h2 class="az-content-title"> Analytics Dashboard
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('dashboard') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Go To Back
                        </a>
                    </span>
                </h2>

                <div id="data-container">

                    {!! Form::open(['method' => 'GET', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="wd-xl-100p mb-3">
                        <div class="row row-xs">
                            {{--<div class="col-md-3">--}}
                            {{--<label class="az-content-label tx-11 tx-medium tx-gray-600">Search:</label>--}}
                            {{--<input name="search" id="search" type="text" class="form-control" placeholder="Search">--}}
                            {{--</div>--}}
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Date
                                        Range:</label>
                                    <input name="date_range" id="date_range" type="text" class="form-control"
                                           autocomplete="off" placeholder="Select Date"
                                           value="{{ $_GET['date_range'] ?? ''}}">
                                </div>
                            </div>
                            <div class="col-md-2 mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <a href="{{route('analytics')}}" class="btn btn-az-warning btn-warning">Reset</a>
                            </div>
                        </div>

                    </div>
                    {!! Form::close() !!}

                    <table id="dataTableList" class="table">
                        <thead>
                        <tr role="row" class="bg-white">
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Referral Code</th>
                            <th>Total View Count</th>
                            <th>Active Referral</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($analyticsReportData) > 0)
                            @foreach($analyticsReportData as $value)
                                <tr>
                                    <td>{{ $value['owner_name'] ?? '-' }}</td>
                                    <td>{{ $value['owner_email'] ?? '-' }}</td>
                                    <td>{{ $value['owner_phone'] ?? '-' }}</td>
                                    <td>{{ $value['referral_code'] ?? '-' }}</td>
                                    <td>{{ $value['page_views'] ?? '-' }}</td>
                                    <td>{{ $value['active_referral'] ?? '-' }}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#date_range').daterangepicker({
                autoUpdateInput: true,
                @if(!isset($_GET['date_range']))
                startDate: moment().subtract(7, 'days'),
                endDate: moment(),
                @endif
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY'
                },
                ranges: {
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });

            $('#date_range').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            });

            $('#date_range').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });


            $('#dataTableList').DataTable({
                "ordering": false,
                searching: false,
            });

            {{--var $dataTableList = $('#dataTableList').DataTable({--}}
            {{--processing: true,--}}
            {{--serverSide: true,--}}
            {{--responsive: true,--}}
            {{--bFilter: false,--}}
            {{--"aaSorting": [],--}}
            {{--ajax: {--}}
            {{--url: $app_url + '/campaign/getAjaxCallingData',--}}
            {{--data: function (d) {--}}
            {{--d.campaign_id = '{{$campaignDetail->id}}';--}}
            {{--d.search=$("#search").val();--}}
            {{--d.action=$("#action").val();--}}
            {{--}--}}
            {{--},--}}
            {{--"columns": [--}}
            {{--{data: 'user_detail', 'sortable': false,width:'25%'},--}}
            {{--{data: 'course_name','sortable': false, 'orderable': false},--}}
            {{--{data: 'move_to_followup','sortable': false, 'orderable': false,},--}}
            {{--{data: 'ringing','sortable': false, 'orderable': false},--}}
            {{--{data: 'reject','sortable': false, 'orderable': false},--}}
            {{--{data: 'switch_off','sortable': false, 'orderable': false},--}}
            {{--{data: 'invalid_number','sortable': false, 'orderable': false},--}}
            {{--{data: 'recall','sortable': false, 'orderable': false},--}}
            {{--{data: 'not_interest','sortable': false, 'orderable': false},--}}
            {{--{data: 'already_followup','sortable': false, 'orderable': false},--}}
            {{--{data: 'name','sortable': false, 'orderable': false},--}}
            {{--],--}}
            {{--select: true,--}}
            {{--bStateSave: true,--}}
            {{--fnStateSave: function (settings, data) {--}}
            {{--localStorage.setItem("dataTables_state", JSON.stringify(data));--}}
            {{--},--}}
            {{--fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {--}}
            {{--if(aData['calling_action']){--}}
            {{--$('td', nRow).css('background-color', '#a2e29a');--}}
            {{--}--}}
            {{--},--}}
            {{--fnStateLoad: function (settings) {--}}
            {{--return JSON.parse(localStorage.getItem("dataTables_state"));--}}
            {{--},--}}
            {{--drawCallback: function (settings) {--}}
            {{--$('body').tooltip({selector: '[data-toggle="tooltip"]',html: true});--}}
            {{--}--}}
            {{--});--}}


            {{--$('#search-form').on('submit', function (e) {--}}
            {{--$dataTableList.draw();--}}
            {{--e.preventDefault();--}}
            {{--});--}}

            {{--$('#reset-filters').on('click', function () {--}}
            {{--$("#action").val('').trigger('change');--}}
            {{--$("#search").val('');--}}
            {{--$dataTableList.draw();--}}
            {{--});--}}
        });

    </script>
@endsection


