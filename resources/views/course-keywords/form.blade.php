<div class="row row-sm">
    <div class="col-sm-12">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Search Keywords :</label>
            <small class="text-danger">To be used for search course functionality</small><br>
            {!! Form::text('search_keywords', implode(',',(json_decode($courseKeywordsData->keywords, true))) ?? old('courseKeywordsData->keywords'), ['class'=>'form-control', 'id' => 'search_keywords', 'data-role' => 'tagsinput']) !!}
        </div>
    </div>
</div>
<button type="submit" class="btn btn-az-primary mg-md-t-9">{{ isset($courseKeywordsData) ? "Update" : "Submit" }}</button>
@section('css')
<link href="{{asset('admin/lib/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
@endsection
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('admin/lib/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/lib/bootstrap-tagsinput/bootstrap-tagsinput-angular.min.js') }}"></script>
@endsection

