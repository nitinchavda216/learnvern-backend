@extends('layout.master')
@section('content')
<div class="az-content">
    <div class="container">
        <div class="az-content-body">
            <h2 class="az-content-title"> Course Search Keywords</h2>
            <div id="data-container">
                <table id="dataTableList" class="display responsive">
                    <thead>
                        <tr>
                            <th>Course</th>
                            <th>Keywords</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $item)
                        <tr>
                            <td>{{$item->course_name ?? "- - -"}}</td>
                            <td>{{ implode(',', array_slice(json_decode($item->keywords, true), 0, 3)) ?? "- - -" }}</td>
                            <td>@include("course-keywords.partials.actions")</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="uploadFileModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                Upload File
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form method="post" action="{{route('search-keywords.import')}}"
                  enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <input name="file" type="file" id="FileUpload" class="file-upload"
                           data-allowed-file-extensions="xlsx"/>
                    <input name="id" type="hidden"  id="keywordId"  />
                </div>
                <div class="modal-footer">
                    <button class="btn btn-indigo">Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('footer_scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $('#dataTableList').DataTable({
            responsive: true,
            "columns": [
                {'sortable': true, 'width': '40%'},
                {'sortable': false, 'width': '40%'},
                {'sortable': false, 'width': '20%'}
            ],
            language: {
                searchPlaceholder: 'Search...',
                sSearch: ''
            }
        });

        // To get course keyword id from import button to modal's form.
        $('#uploadFileModal').on('show.bs.modal', function (e) {
            var courseKeywordId = $(e.relatedTarget).data('keyword-id');
            $(e.currentTarget).find('input[name="id"]').val(courseKeywordId);
        });
    });
</script>
<script type="text/javascript">
    var $openUploadModal = "{{ session()->pull('openModal') }}";
    if (typeof $openUploadModal !== 'undefined' && $openUploadModal && ($("#uploadFileModal").length > 0)) {
        $("#uploadFileModal").modal('show');
    }
</script>
@endsection


