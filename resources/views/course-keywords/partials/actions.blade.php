<div class="btn-group">
    <a href="{{ route('search-keywords.edit', $item->id) }}" class="btn btn-primary btn-xs">
        <i class="fas fa-edit"></i>
    </a>
    <a href="javascript:;" class="btn btn-xs btn-info"
       data-toggle="modal"
       data-target="#uploadFileModal" data-keyword-id="{{ $item->id }}"><i class="fa fa-upload" aria-hidden="true"></i>
        Import
    </a>
    <a href="javascript:;" class="btn btn-xs btn-success"
       data-course-id="{{ $item->id }}"
       onclick="this.href = 'search-keywords/getAjaxExportData?course_id=' + this.getAttribute('data-course-id')">
        <i class="fa fa-download" aria-hidden="true"></i> Export
    </a>
</div>