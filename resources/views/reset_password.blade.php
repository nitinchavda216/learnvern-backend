<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Azia">
    <meta name="twitter:description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="twitter:image" content="http://themepixels.me/azia/img/azia-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/azia">
    <meta property="og:title" content="Azia">
    <meta property="og:description" content="Responsive Bootstrap 4 Dashboard Template">

    <meta property="og:image" content="http://themepixels.me/azia/img/azia-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/azia/img/azia-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="author" content="ThemePixels">

    <title>LearnVern</title>

    <!-- vendor css -->
    <link href="{{asset('admin/lib/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/lib/typicons.font/typicons.css')}}" rel="stylesheet">
    <link href="{{ asset('admin/css/azia.css')}}" rel="stylesheet"/>


</head>
<body class="az-body">

<div class="az-signin-wrapper">
    <div class="az-card-signin">
        <h1 class="az-logo">LearnVern</h1>
        <div class="az-signin-header">
            <h2>Forgot Password!</h2>
            <form action=" {{ route('reset_admin_password') }}" method="post">
                @csrf
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" class="form-control" placeholder="Enter your email" value="" name="email" data-validation="email">
                </div><!-- form-group -->
                <div class="form-group">
                    <label>Password</label>
                    {!! Form::password('password_confirmation', ['class'=>'form-control', 'placeholder'=>'Enter password', 'data-validation'=>'required length', 'data-validation-length' => 'min6',
                            'data-validation-error-msg-required' => "Please enter a password!", 'data-validation-error-msg-length' => "The input value is shorter than 6 characters"]) !!}
                </div>
                <div class="form-group">
                    <label>Confirm Password</label>
                    {!! Form::password('password',['class'=>'form-control', 'data-validation' => 'confirmation', 'placeholder'=>'Enter confirm password',
                            'data-validation-error-msg' => "Your confirm password does not match with your password!"]) !!}
                </div>
                <button class="btn btn-az-primary btn-block">Submit</button>
            </form>
        </div><!-- az-signin-header -->
    </div><!-- az-card-signin -->
</div><!-- az-signin-wrapper -->
<script type="text/javascript" src="{{asset('admin/lib/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
{{--<script type="text/javascript" src="{{asset('admin/lib/ionicons/ionicons.js')}}"></script>--}}
<script type="text/javascript" src="{{asset('admin/lib/jquery.flot/jquery.flot.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/lib/jquery.flot/jquery.flot.resize.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/lib/peity/jquery.peity.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/azia.js')}}"></script>


<script>
    $(function(){
        'use strict'

    });
</script>
</body>
</html>
