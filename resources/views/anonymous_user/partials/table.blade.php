@if($data)
    <div class="card">
        <div class="card-header">
            Course Faqs
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped mg-b-0">
                    <thead>
                    <tr>
                        <th>Question</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $faq)
                        <tr class="faqTr">
                            <td>{{ $faq['question'] }}</td>
                            <td>
                                <div class="btn-group" role="group">
                                    <a href="javascript:;"
                                       class="btn btn-danger btn-xs deleteFaq" data-id="{{$faq['id']}}">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif
