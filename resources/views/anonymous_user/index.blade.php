@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title"> Anonymous User </h2>
                <div id="data-container">
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>User IP</th>
                            <th>Phone Number</th>
                            <th>User Visited Page</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Country</th>
                            <th>Created Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('#dataTableList').length) {
                var $dataTableList = $('#dataTableList').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    bFilter: false,
                    "aaSorting": [],
                    ajax: {
                        url: $app_url + '/anonymousUser/getAjaxListData',
                        data: function (d) {

                        }
                    },
                    "columns": [
                        {data: 'user_ip', 'sortable': false, 'width': "10%"},
                        {data: 'phone_number', 'orderable': false, 'width': "13%"},
                        {data: 'visited_page', 'orderable': false, 'width': "37%"},
                        {data: 'city', 'orderable': false, 'width': "10%"},
                        {data: 'state', 'orderable': false, 'width': "10%"},
                        {data: 'country', 'orderable': false, 'width': "10%"},
                        {data: 'created_at', 'orderable': false, 'width': "10%"},
                        //{data: 'action', 'orderable': false, 'width': "10%"},
                    ],
                    select: true,
                    bStateSave: true,
                    fnStateSave: function (settings, data) {
                        localStorage.setItem("dataTables_state", JSON.stringify(data));
                    },
                    fnStateLoad: function (settings) {
                        return JSON.parse(localStorage.getItem("dataTables_state"));
                    }
                });

                $('#search-form').on('submit', function (e) {
                    $dataTableList.draw();
                    e.preventDefault();
                });


            }
        });
    </script>
@endsection

