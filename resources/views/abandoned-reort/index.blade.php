@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container-fluid">
            <div class="az-content-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="az-content-title">Abandoned Payment Report
                            <span class="pull-right d-inline-block float-right">
                                <a href="javascript:;" class="btn btn-success btn-sm mt-negative"
                                   onclick="this.href='abandoned-payments/getAjaxExportData?=' + '&search=' + document.getElementById('search-form-field').value + '&date_range=' + document.getElementById('date_range').value + '&partner_user_id=' + document.getElementById('partner_user_id').value">
                                     <i class="fa fa-download" aria-hidden="true"></i> Export
                                </a>
                            </span>
                        </h2>
                        <div id="data-container">
                            {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                            <div class="mb-3">
                                <div class="row row-xs">
                                    <div class="col-md-3">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Search</label>
                                        <input name="search" id="search-form-field" type="text" class="form-control"
                                               placeholder="Search">
                                    </div>
                                    @if(session('partner_id') == 1)
                                        <div class="col-md-3">
                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Partners:</label>
                                            {!! Form::select('partner_user_id', $partners, null,['id' => 'partner_user_id', 'class' => 'form-control select2']) !!}
                                        </div>
                                    @endif
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Date
                                                :</label>
                                            <input name="date_range" id="date_range" type="text" class="form-control"
                                                   autocomplete="off" placeholder="Select Date">
                                        </div>
                                    </div>
                                    <div class="col-md mg-t-10 mt-4">
                                        <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                        <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">
                                            Reset
                                        </button>
                                    </div>
                                </div>

                            </div>
                            {!! Form::close() !!}
                            <table id="dataTableList" class="display responsive">
                                <thead>
                                <tr>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Course Name</th>
                                    <th>Course Progress(%)</th>
                                    <th>Attempts#</th>
                                    <th>Created Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        /*  var showSubmitButton = "{{ isset($course) ? true : false }}";*/
    </script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#date_range').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                },
                ranges: {
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            $('#date_range').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            });
            $('#date_range').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            if ($('#dataTableList').length) {
                var $dataTableList = $('#dataTableList').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    bFilter: false,
                    "aaSorting": [],
                    ajax: {
                        url: $app_url + '/abandoned-payments/getAjaxData',
                        data: function (d) {
                            d.date_range = $('#date_range').val();
                            d.search = $('#search-form-field').val();
                            d.partner_user_id = $('#partner_user_id').val();
                        }
                    },
                    "columns": [
                        {data: 'first_name', 'sortable': false, 'width': "20%"},
                        {data: 'email', 'sortable': false, 'width': "20%"},
                        {data: 'mobile_number', 'sortable': false, 'width': "10%"},
                        {data: 'course_name', 'sortable': false, 'width': "10%"},
                        {data: 'progress', 'sortable': false, 'width': "10%"},
                        {data: 'countid', 'sortable': false, 'width': "10%"},
                        {data: 'paymentcreated_date', 'sortable': false, 'width': "20%"},
                    ],
                    select: true,
                    bStateSave: true,
                    fnStateSave: function (settings, data) {
                        localStorage.setItem("dataTables_state", JSON.stringify(data));
                    },
                    fnStateLoad: function (settings) {
                        return JSON.parse(localStorage.getItem("dataTables_state"));
                    }
                });

                $('#search-form').on('submit', function (e) {
                    $dataTableList.draw();
                    e.preventDefault();
                });
                $('#reset-filters').on('click', function () {
                    $("#search").val(null);
                    $("#date_range").val(null);
                    $("#partner_user_id").val(1).trigger('change');
                    $dataTableList.draw();
                });
            }
        });
    </script>
@endsection
