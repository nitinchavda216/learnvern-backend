@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title"> Set Curriculum
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('courses') }}" class="btn btn-warning btn-sm mt-negative"><i
                                class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                    </span>
                </h2>
                @php
                    $curriculum_list_id_array = [];
                @endphp
                <div id="data-container">
                    <div class="card card-body pd-40">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="card">
                                    <div class="card-header">{{ $course->name }} Curriculum</div>
                                    <div class="card-body">
                                        @if(count($currentCourseCurriculum) > 0)
                                            <div class="alert alert-warning" role="alert">
                                                <i class="fa fa-info-circle"></i> Drag the Handler icon to re-arrange
                                                the units or sections then press <b>Update</b> button.
                                            </div>
                                            <div class="sortable">
                                                @foreach($currentCourseCurriculum as $courseMasterSection)
                                                    <div class="alert alert-info" role="alert">
                                                        <h5>{{ $courseMasterSection->name }}
                                                            <span class="pull-right d-inline-block float-right">
                                                                <a href="javascript:;" class="btn btn-primary btn-xs openUpdateSectionModal" data-id="{{ $courseMasterSection->id }}" data-name="{{ $courseMasterSection->name }}" title="Edit"><i class="fas fa-edit"></i></a>
                                                            </span>
                                                        </h5>
                                                    </div>
                                                    @foreach($courseMasterSection->getMasterSectionChildData as $section)
                                                        <div class="group-caption bg-gray-200"
                                                             id="section_{{ $section->id }}"
                                                             data-id="{{ $section->id }}">
                                                            <div class="d-flex justify-content-between">
                                                                <h4>{{ $section->name }}</h4>
                                                                <a href="javascript:;" data-id="{{ $section->id }}" data-type="all" class="btn btn-danger btn-xs deleteCurriculum" title="Delete"><i class="fas fa-trash"></i></a>
                                                                <div class="move">+</div>
                                                            </div>
                                                            @php
                                                                $curriculum_list_id_array[] = $section->curriculum_list_id;
                                                            @endphp
                                                            <div class="group-items">
                                                                @foreach($section->getSectionChildData as $data)
                                                                    @php
                                                                        $curriculum_list_id_array[] = $data->curriculum_list_id;
                                                                        $text_color = ($data->type == "unit") ? "green" : ($data->type == "assignment" ? "peru" : ($data->type == "quiz" ? "brown" : "black"))
                                                                    @endphp
                                                                    <div class="group-item" data-id="{{ $data->id }}"
                                                                         style="color: {{ $text_color }} !important;">
                                                                        <div
                                                                            class="d-flex justify-content-between align-items-center">
                                                                            {{ $data->name }}
{{--                                                                            <a href="javascript:;" class="btn btn-primary btn-xs" title="Edit"><i class="fas fa-edit"></i></a>--}}
                                                                            <a href="javascript:;" data-id="{{ $data->id }}" data-type="single" class="btn btn-danger btn-xs deleteCurriculum" title="Delete"><i class="fas fa-trash"></i></a>
                                                                            <div class="move">+</div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endforeach
                                            </div>

                                            <button type="button" id="submitCurriculum"
                                                    class="btn btn-az-primary mg-md-t-9">Update
                                            </button>
                                        @else
                                            <div class="az-content-label text-center"><h3>No Record Found</h3></div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="card">
                                    <div class="card-header">
                                        {!! Form::open(['method' => 'GET', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Select
                                                    Course :</label>
                                                {!! Form::select('course_id', $coursesList, request('course_id'), ['id' => "course_id", 'class'=>'form-control select2', 'data-validation' => 'required', 'placeholder' => ""]) !!}
                                            </div>
                                            <div class="col-sm-2 mt-4">
                                                <button type="submit" class="btn btn-az-success btn-sm btn-success">
                                                    Search
                                                </button>
                                            </div>
                                            <div class="col-sm-2 mt-4">
                                                <a href="{{ route('courses.career_plan_curriculum', $course->id) }}"
                                                   type="button" class="btn btn-az-warning btn-sm btn-warning"
                                                   id="reset-filters">
                                                    Reset
                                                </a>
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="card-body">
                                        @if(!empty($searchCourseCurriculumList))
                                            {!! Form::open(['route'=>'courses.career_plan.store','method' => 'POST', 'enctype'=> "multipart/form-data", 'files' => true, 'id' => "courseForm"]) !!}
                                            {{--                                        <input type="hidden" name="course_id_array[]" value="{{ request('course_id') }}">--}}
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Master
                                                            Section Title:</label>
                                                        {!! Form::select('section_id', $sectionList, null, ['id' => "section_id", 'class'=>'form-control sectionListSelect2', 'data-validation' => 'required', 'placeholder' => ""]) !!}
                                                        <input type="hidden" name="course_id" value="{{ $course->id }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-12 py-2">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <input type="checkbox" id="masterCheckboxInput">
                                                            {{ $searchCourseCurriculumList['0']->courseDetail->name }}
                                                        </div>
                                                        <div class="card-body">
                                                            <ul class="tree_structure">
                                                                @foreach($searchCourseCurriculumList as $section)
                                                                    <li>
                                                                        {!! Form::checkbox('section_ids[]',$section->id, in_array($section->curriculum_list_id, $curriculum_list_id_array),['class' => 'parent_module', 'disabled' => in_array($section->curriculum_list_id, $curriculum_list_id_array)]) !!}
                                                                        {{ $section->name }}
                                                                        @if(count($section->getSectionChildData) > 0)
                                                                            <ul>
                                                                                @foreach($section->getSectionChildData as $unit)
                                                                                    <li>
                                                                                        {!! Form::checkbox('curriculum_ids['.$section->id.'][]',$unit->id, in_array($unit->curriculum_list_id, $curriculum_list_id_array),['class' => 'child_module', 'disabled' => in_array($unit->curriculum_list_id, $curriculum_list_id_array)]) !!}
                                                                                        {{ $unit->name }} -
                                                                                        ({{ ucfirst($unit->type) }})
                                                                                    </li>
                                                                                @endforeach
                                                                            </ul>
                                                                        @endif
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit"
                                                    class="btn btn-az-primary mg-md-t-9">Create Section
                                            </button>
                                            {!! Form::close() !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="updateSectionModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    Update Master Section Title
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form method="post" action="{{route('courses.masterSectionUpdate')}}"
                      enctype="multipart/form-data">
                    <div class="modal-body">
                        @csrf
                        <input type="text" class="form-control" name="name" id="masterSectionName" data-validation="required">
                        <input type="hidden" class="form-control" name="curriculum_id" id="masterSectionCurriculumId" data-validation="required">
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-indigo">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link href="{{asset('admin/lib/tree/tree.css')}}" rel="stylesheet">
    <style>
        .sortable {

        }

        .group-caption {
            width: 100%;
            display: block;
            padding: 20px;
            margin: 0 0 15px 0;
        }

        .group-item {
            background: #ffffff;
            width: 80%;
            height: auto;
            display: block;
            padding: 3px;
            margin: 5px;
            color: #000;
        }

        .move {
            background: #ff0000;
            width: 30px;
            height: 30px;
            float: right;
            color: #fff;
            text-align: center;
            text-transform: uppercase;
            line-height: 30px;
            font-family: Arial;
            cursor: move;
        }

        .movable-placeholder {
            background: #ccc;
            width: 100%;
            height: 100px;
            display: block;
            padding: 20px;
            margin: 0 0 15px 0;
            border-style: dashed;
            border-width: 2px;
            border-color: #000;
        }
    </style>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="{{asset('admin/js/jquery.blockUI.js')}}"></script>
    <script type="text/javascript" src="{{ asset('admin/lib/tree/tree.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"
            integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA=="
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        var $courseId = {{ $course->id }}
        $(document).ready(function () {
            if ($(".sectionListSelect2").length > 0) {
                $('.sectionListSelect2').select2({
                    placeholder: '--SELECT Or ENTER--',
                    tags: true
                }).on("change", function (e) {
                    var isNew = $(this).find('[data-select2-tag="true"]');
                    if (isNew.length) {
                        isNew.replaceWith('<option selected value="newTag-' + isNew.val() + '">' + isNew.val() + '</option>');
                    }
                });
            }

            $(".sortable").sortable({
                containment: "parent",
                items: "> div",
                handle: ".move",
                tolerance: "pointer",
                cursor: "move",
                opacity: 0.7,
                revert: 300,
                delay: 150,
                dropOnEmpty: true,
                placeholder: "movable-placeholder",
                start: function (e, ui) {
                    ui.placeholder.height(ui.helper.outerHeight());
                }
            });

            // Sort the children
            $(".group-items").sortable({
                items: "> div",
                tolerance: "pointer",
                containment: "parent"
            });

            $('input[type=checkbox]').click(function () {
                var self = $(this);
                if (this.checked) {
                    self.parents('li').children('input[type=checkbox]').prop('checked', true);
                }
                self.parent().find('input[type=checkbox]').prop('checked', this.checked);
            });

            $('#masterCheckboxInput').click(function () {
                $(".tree_structure").find('.parent_module').prop('checked', this.checked);
                $(".tree_structure").find('.child_module').prop('checked', this.checked);
            });

            $(document).on('click', '.openUpdateSectionModal', function () {
                $("#masterSectionCurriculumId").val($(this).data('id'));
                $("#masterSectionName").val($(this).data('name'));
                $("#updateSectionModal").modal('show');
                $.validate();
            });

            $(document).on('click', "#submitCurriculum", function () {
                var sortArray = {};
                var sortSectionData = $('.sortable').sortable('toArray', {attribute: 'data-id'});
                $.each(sortSectionData, function (key, value) {
                    if (value != "") {
                        var sortData = $('#section_' + value).children('.group-items').sortable('toArray', {attribute: 'data-id'});
                        sortArray[value] = sortData;
                    }
                });
                $.ajax({
                    url: $app_url + "/course/curriculum/update",
                    method: 'POST',
                    data: {sortSectionArray: sortSectionData, sortUnitsArray: sortArray},
                    beforeSend: function () {
                        $.blockUI({
                            message: '<h1>Please wait..</h1>',
                            timeout: 2000
                        });
                    },
                    success: function (data) {
                        if (data.status) {
                            toastr.success(data.message, "Success!", {timeOut: 2000});
                        } else {
                            toastr.error(data.message, "Error!", {timeOut: 2000});
                        }
                        $.unblockUI();
                    }
                });
            })

            $(document).on('click', ".deleteCurriculum", function () {
                var self = $(this);
                var curriculum_id = self.data('id');
                $.ajax({
                    url: $app_url + "/course/curriculum/delete",
                    method: 'POST',
                    data: {id: curriculum_id, course_id: $courseId},
                    beforeSend: function () {
                        $.blockUI({
                            message: '<h1>Please wait..</h1>',
                            timeout: 2000
                        });
                    },
                    success: function (data) {
                        if (data.status) {
                            toastr.success(data.message, "Success!", {timeOut: 2000});
                            if (self.data('type') == "all"){
                                self.parents('.group-caption').remove();
                            }else{
                                self.parents('.group-item').remove();
                            }
                        } else {
                            toastr.error(data.message, "Error!", {timeOut: 2000});
                        }
                        $.unblockUI();
                    }
                });
            })
        })
    </script>
@endsection

