@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Course Activity : ({{ $course->name }})
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('courses') }}" class="btn btn-warning btn-sm mt-negative">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                    </span>
                </h2>
                <div id="data-container">
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th class="text-truncate" style="width:200px;">User Name</th>
                            <th class="text-truncate">Log Action</th>
                            <th class="text-truncate">Date</th>
                            <th class="text-truncate">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($activityData as $item)
                            <tr>
                                <td>{{ $item->userDetail['name'] ?? "- - -" }}</td>
                                <td>{{ ucfirst($item->description) }}</td>
                                <td>{{ formatDate($item->created_at) }}</td>
                                <td>
                                    <a href="javascript:;" onclick="myFunction({{$item->id}})"
                                       class="btn btn-primary btn-xs d-inline">
                                        Changes
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="updateChangesModal" class="modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Details</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3 style="text-align: center">Old Data</h3>
                            <ul id="old">
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <h3 style="text-align: center">Changes</h3>
                            <ul id="attributes" class="list-group">
                            </ul>
                        </div>
                    </div>
                </div><!-- modal-body -->
            </div>
        </div><!-- modal-dialog -->
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataTableList').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                }
            });
        });
        function myFunction(id) {
            $.ajax({
                url: $app_url + '/get_activity_changes',
                data: "id=" + id,
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    var attributes = data.attributes;
                    var set = '';
                    $.each(attributes, function (index, val) {
                        set += '<li class="list-group-item"><b>' + index + '</b> : ' + val + '</li>';
                    });
                    var old = data.old;
                    var setold = ''
                    $.each(old, function (index, val) {
                        setold += '<li class="list-group-item"><b>' + index + '</b> : ' + val + '</li>';
                    });
                    $('#updateChangesModal').modal('toggle');
                    $('#attributes').html(set);
                    $('#old').html(setold);
                }
            });
        }
    </script>
@endsection

