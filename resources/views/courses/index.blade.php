@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Courses
                    @if(session('partner_id') == 1)
                        <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('courses.create') }}" class="btn btn-success btn-sm mt-negative">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                    @endif
                </h2>
                <div id="data-container">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-5">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Search :</label>
                                {!! Form::text('search', null,['id' => 'search', 'class' => 'form-control', 'placeholder' => "Search Course"]) !!}
                            </div>
                            <div class="col-md-5">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Category :</label>
                                {!! Form::select('category_id', $categories, [],['id' => 'category_id', 'class' => 'form-control select2', 'placeholder' => ""]) !!}
                            </div>
                            <div class="col-md-2 mt-4">
                                <button type="submit" class="btn btn-az-success btn-sm btn-success">Search</button>
                                <button type="button" class="btn btn-az-warning btn-sm btn-warning" id="reset-filters">
                                    Reset
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th class="text-truncate" style="width:200px;">Courses Name</th>
                            <th class="text-truncate">Category Name</th>
                            <th class="text-truncate">Statistics</th>
                            <th class="text-truncate">Total Time</th>
                            @if(session('partner_id') == 1)
                                <th class="text-truncate">Default Testimonials</th>
                                <th class="text-truncate">Actions</th>
                                <th></th>
                            @else
                                <th class="text-truncate">Status</th>
                                <th class="text-truncate">Actions</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @if(session('partner_id') != 1)
        <div class="modal fade" id="updatePriceModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5>Update Course Price</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <form method="post" action="{{route('course.price.update')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <input type="hidden" name="course_id" id="courseIdInput" value="">
                            <div class="form-group">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Price :</label>
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <span class="input-group-text">₹</span>
                                    </div>
                                    <input class="form-control" id="priceInput" data-validation="required number" name="price" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-indigo">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        var partner_id = "{{ session('partner_id') }}";
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
            var columns = [];
            if (partner_id == "1") {
                columns = [
                    {data: 'name', 'sortable': false, 'width': "25%"},
                    {data: 'category_name', 'sortable': false, 'width': "15%"},
                    {data: 'statistics', 'sortable': false, 'width': "10%"},
                    {data: 'time', 'orderable': false, 'width': "5%"},
                    {data: 'default_testimonial', 'orderable': false, 'width': "15%"},
                    {data: 'actions', 'orderable': false, 'width': "25%"},
                    {data: 'status', 'orderable': false, 'width': "5%"}
                ];
            } else {
                columns = [
                    {data: 'name', 'sortable': false, 'width': "25%"},
                    {data: 'category_name', 'sortable': false, 'width': "25%"},
                    {data: 'statistics', 'sortable': false, 'width': "10%"},
                    {data: 'time', 'orderable': false, 'width': "10%"},
                    {data: 'status', 'orderable': false, 'width': "10%"},
                    {data: 'actions', 'orderable': false, 'width': "10%"}
                ]
            }
            if ($('#dataTableList').length) {
                var $dataTableList = $('#dataTableList').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    bFilter: false,
                    "aaSorting": [],
                    ajax: {
                        url: $app_url + '/courses/getAjaxListData',
                        data: function (d) {
                            d.search = $('#search').val();
                            d.category_id = $('#category_id').val();
                        }
                    },
                    "columns": columns,
                    select: true,
                    bStateSave: true,
                    fnStateSave: function (settings, data) {
                        localStorage.setItem("dataTables_state", JSON.stringify(data));
                    },
                    fnStateLoad: function (settings) {
                        return JSON.parse(localStorage.getItem("dataTables_state"));
                    },
                    drawCallback: function (settings) {
                        $('body').tooltip({selector: '[data-toggle="tooltip"]'});
                    }
                });

                $('#search-form').on('submit', function (e) {
                    $dataTableList.draw();
                    e.preventDefault();
                });

                $('#reset-filters').on('click', function () {
                    $("#search").val(null);
                    $("#category_id").val(null).trigger('change');
                    $dataTableList.draw();
                });
            }
        });
        // Toggle Switches
        $(document).on('click', ".az-toggle", function () {
            var status = ($(this).toggleClass('on').hasClass('on') == true) ? 1 : 0;
            var id = $(this).data('id');
            $.ajax({
                type: "post",
                dataType: "json",
                url: $app_url + '/course/update_status',
                data: {'is_active': status, 'id': id},
                success: function () {
                    toastr.success("Status Updated Successfully!", "Success!", {timeOut: 2000});
                }
            });
        });
        $(document).on('click', ".openUpdatePriceModal", function () {
            $("#courseIdInput").val($(this).data('id'));
            $("#priceInput").val($(this).data('price'));
            $("#updatePriceModal").modal('show');
        });
    </script>
@endsection

