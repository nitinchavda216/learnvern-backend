<div class="form-group">
    <label class="ckbox">
        <input type="checkbox" name="is_career_plan_course" @if(isset($course) && ($course->is_career_plan_course == 1)) checked @endif><span>Is Career Plan Course?</span>
    </label>
</div>
<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Name :</label>
            <small class="text-danger">Required</small>
            {!! Form::text('name', $course->name ?? old('name'), ['class'=>'form-control', 'id' => 'course_name', 'data-validation' => 'required']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        @if(isset($course))
            <div class="form-group">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Slug :</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="slug" value="{{ $course->slug }}" readonly>
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button" data-toggle="modal"
                                data-target="#updateSlugModal"><i class="fa fa-edit"></i></button>
                    </span>
                </div>
            </div>
        @else
            <div class="form-group">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Slug :</label>
                <small class="text-danger">Required</small>
                {!! Form::text('slug', old('slug'), ['class'=>'form-control slugInput', 'id' => 'slug', 'data-validation' => 'required', isset($course) ? 'disabled' : ""]) !!}
            </div>
        @endif
    </div>
</div>
<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Certificate Name :</label>
            <small class="text-danger">To be displayed on the User Certificate, Limit: 60 chars</small>
            {!! Form::text('course_certificate_name', $course->course_certificate_name ?? old('course_certificate_name'), ['class'=>'form-control', 'id' => 'course_certificate_name', 'data-validation' => 'required','data-maxallowed'=>60]) !!}
            <h6><span id="courseCertificateNameCount">0</span> chars</h6>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Achievement Title :</label>
            <small class="text-danger">Title for app-user when course completed and certificate purchased</small>
            {!! Form::text('certificate_achievement_title', $course->certificate_achievement_title ?? old('certificate_achievement_title'), ['class'=>'form-control', 'id' => 'certificate_achievement_title']) !!}
        </div>
    </div>

</div>
<div class="row row-sm">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Category :</label>
            <small class="text-danger">Required</small>
            {!! Form::select('category_id', $categories, $course->category_id ?? old('category_id'), ['class'=>'form-control select2Input', 'data-validation' => 'required', 'placeholder' => ""]) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Language :</label>
            <small class="text-danger">Required</small>
            {!! Form::select('course_language', $language, $course->course_language ?? old('course_language'), ['class'=>'form-control select2Input', 'id' => 'course_language']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Price :</label>
            <small class="text-danger">Required</small>
            <div class="input-group">
                <div class="input-group-append">
                    <span class="input-group-text">₹</span>
                </div>
                {!! Form::text('price', $course->price ?? old('price'), ['class'=>'form-control', 'id' => 'price', 'data-validation' => "required number"]) !!}
            </div>
        </div>
    </div>
</div>
<div class="row row-sm">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Type :</label>
            {!! Form::select('course_type', \App\Models\Course::COURSE_TYPES, $course->course_type ?? old('course_type'), ['class'=>'form-control select2Input', 'id' => 'course_type', 'placeholder' => ""]) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Pre-Course :</label>
            {!! Form::select('pre_course[]', $courses, $course->pre_course ?? old('pre_course'), ['class'=>'form-control select2Input', 'id' => 'pre_course', 'multiple']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Related Course :</label>
            <small class="text-danger">To be displayed on the Course detail page</small>
            {!! Form::select('related_course[]', $courses, $course->related_course ?? old('related_course'), ['class'=>'form-control select2Input', 'id' => 'related_course', 'multiple']) !!}
        </div>
    </div>
</div>
<div class="row row-sm">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Quiz Retake :</label>
            {!! Form::text('course_retake', $course->course_retake ?? old('course_retake'), ['class'=>'form-control', 'id' => 'course_retake']) !!}
        </div>
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Choose Color Code :</label>
            <br>
            <input type="text" id="colorpicker" name="color_code" value="{{ $course->color_code ?? '#000000' }}">
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Icon Image :</label>
            <small class="text-danger">Required</small>
            <input type="file" accept="image/*" class="course_image dropify" data-show-remove="false"
                   data-allowed-file-extensions="png jpg jpeg svg"
                   @if(isset($course) && ($course->course_icon != "")) data-default-file="{{ $course->course_icon }}"
            @else
            data-validation="required" @endif
            name="course_icon" id="course_icon">
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Image :</label>
            <small class="text-danger">To be displayed on App and Share OG Image</small>
            <input type="file" accept="image/*" class="course_image dropify" data-show-remove="false"
                   data-allowed-file-extensions="png jpg jpeg svg"
                   @if(isset($course) && ($course->image != "")) data-default-file="{{ $course->image }}" @endif
            name="image" id="image">
        </div>
    </div>

</div>
<div class="row row-sm">
    <div class="col-md-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Tiny Description: </label>
            <small class="text-danger">Displayed on Mouseover on Course Cards, Limit: 75 chars</small>
            {!! Form::textarea('tiny_description', $course->tiny_description ?? old('tiny_description'), ['class'=>'form-control', 'id' => 'tiny_description', 'rows' => 2,'data-maxallowed'=>75]) !!}
            <h6><span id="tinyDescriptionCount">0</span> chars</h6>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Brief Description: </label>
            <small class="text-danger">To be displayed on the Course Cards, Limit: 550 chars</small>
            {!! Form::textarea('brief_description', $course->brief_description ?? old('brief_description'), ['class'=>'form-control', 'id' => 'brief_description', 'data-validation' => 'required', 'rows' => 5,'data-maxallowed'=>550]) !!}
            <h6><span id="briefDescriptionCount">0</span> chars</h6>
        </div>
    </div>
</div>
<div class="row row-sm">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">App Icon Image :</label>
            <small class="text-danger">To be displayed on App</small>
            <input type="file" accept="image/*" class="course_image" data-show-remove="false"
                   data-allowed-file-extensions="png jpg jpeg svg"
                   @if(isset($course) && ($course->app_icon != "")) data-default-file="{{ $course->app_icon }}" @endif
            name="app_icon" id="app_icon">
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">App Image :</label>
            <small class="text-danger">To be displayed on App</small>
            <input type="file" accept="image/*" class="course_image" data-show-remove="false"
                   data-allowed-file-extensions="png jpg jpeg svg"
                   @if(isset($course) && ($course->app_image != "")) data-default-file="{{ $course->app_image }}" @endif
            name="app_image" id="app_image">
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Badge :</label>
            {!! Form::select('badge_id', $badges, $course->badge_id ?? old('badge_id'), ['class'=>'form-control badgeId', 'placeholder' => "None of them"]) !!}
        </div>
    </div>
</div>
