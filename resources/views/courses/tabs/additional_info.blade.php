<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Content :</label>
    <small class="text-danger">To be displayed on the Course detail page</small>
    {!! Form::textarea('content', $course->content ?? old('content'), ['class'=>'form-control html_editor', 'id' => 'content']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Pre-Requisites :</label>
    <small class="text-danger">To be displayed on the Course detail page</small>
    {!! Form::textarea('pre_requisites', $course->pre_requisites ?? old('pre_requisites'), ['class'=>'form-control html_editor', 'id' => 'pre_requisites']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">How Can LearnVern Help? :</label>
    <small class="text-danger">To be displayed on the Course detail page</small>
    {!! Form::textarea('how_can_learnvern_help', $course->how_can_learnvern_help ?? old('how_can_learnvern_help'), ['class'=>'form-control html_editor', 'id' => 'how_can_learnvern_help']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Who can take this course :</label>
    {!! Form::textarea('who_can_take', $course->who_can_take ?? old('who_can_take'), ['class'=>'form-control html_editor', 'id' => 'who_can_take']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">What You'll Learn with :</label>
    {!! Form::textarea('what_you_learn', $course->what_you_learn ?? old('what_you_learn'), ['class'=>'form-control html_editor', 'id' => 'what_you_learn']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Why Should You Learn :</label>
    {!! Form::textarea('why_you_learn', $course->why_you_learn ?? old('why_you_learn'), ['class'=>'form-control html_editor', 'id' => 'why_you_learn']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Frequently Asked Questions :</label>
    {!! Form::textarea('faq', $course->faq ?? old('faq'), ['class'=>'form-control html_editor', 'id' => 'faq']) !!}
</div>
