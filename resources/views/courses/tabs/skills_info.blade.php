@if(isset($course) && sizeof($skills) != 0)
    @foreach($skills as $key => $value)
        <div class="row row-sm skills">
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::text('course_skills[]', $value->title, ['class'=>'form-control', 'id' => 'course_retake']) !!}
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    @if($key ==0)
                        <label class="az-content-label tx-11 tx-medium tx-gray-600"></label>
                        <button type="button" class="btn btn-sm btn-primary" id="addNewRow"><i class="fa fa-plus"></i>
                            Add More Skills
                        </button>
                    @else
                        <a href="javascript:;" class="btn btn-danger btn-sm removeRow"> <i class="fas fa-trash"></i>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
    <div id="addskills">
    </div>
@else
    <div id="addskills">
        <div class="row row-sm skills">
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::text('course_skills[]', null, ['class'=>'form-control', 'id' => 'course_skills']) !!}
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600"></label>
                    <button type="button" class="btn btn-sm btn-primary" id="addNewRow"><i class="fa fa-plus"></i> Add
                        More Skills
                    </button>
                </div>
            </div>
        </div>
    </div>
@endif
