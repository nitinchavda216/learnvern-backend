<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Meta Title :</label>
    <small class="text-danger">Use for SEO</small>
    {!! Form::text('meta_title', $course->meta_title ?? old('meta_title'), ['class'=>'form-control', 'id' => 'meta_title']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Meta Description :</label>
    <small class="text-danger">Use for SEO</small>
    {!! Form::text('meta_description', $course->meta_description ?? old('meta_description'), ['class'=>'form-control', 'id' => 'meta_description']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Meta Keyword :</label>
    <small class="text-danger">Use for SEO</small>
    {!! Form::text('meta_keywords', $course->meta_keywords ?? old('meta_keywords'), ['class'=>'form-control', 'id' => 'meta_keywords']) !!}
</div>
@if(isset($course))
    <div class="form-group">
        <label class="az-content-label tx-11 tx-medium tx-gray-600">Schema :</label>
        <small class="text-danger">Use for SEO</small>
        {!! Form::textarea('schema_script', $course->schema_script ?? old('schema_script'), ['class'=>'form-control', 'id' => 'schema_script', 'rows' => 4, 'disabled']) !!}
    </div>
@endif
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Canonical URL :</label>
    <small class="text-danger">Use for SEO</small>
    {!! Form::text('canonical_url', $course->canonical_url ?? old('canonical_url'), ['class'=>'form-control', 'id' => 'canonical_url']) !!}
</div>