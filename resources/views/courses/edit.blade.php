@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Edit Course: {{ $course->name }}
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('courses') }}" class="btn btn-warning btn-sm mt-negative">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    {!! Form::open(['route'=>['courses.update', $course->id],'method' => 'POST', 'enctype'=> "multipart/form-data", 'files' => true, 'id' => "courseForm"]) !!}
                    @include("courses.form", ['formMode' => 'edit'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="updateSlugModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    Update Course Slug
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form method="post" id="updateSlugForm"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" name="course_id" value="{{ $course->id }}">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Slug:</label>
                            {!! Form::text('slug', $course->slug, ['id' => "editSlugInput", 'class'=>'form-control slugInput', 'data-validation' => 'required', 'placeholder' => 'Enter New Slug']) !!}
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-indigo" id="updateSlugBtn">Update</button>
                </div>
            </div>
        </div>
    </div>
@endsection

