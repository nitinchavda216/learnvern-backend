<div id="wizard2">
    <h3>Basic Info</h3>
    <section>
        @include('courses.tabs.general')
    </section>
    <h3>Additional Info</h3>
    <section>
        @include('courses.tabs.additional_info')
    </section>
    <h3>Course Video</h3>
    <section>
        @include('courses.tabs.course_videos')
    </section>
    <h3>Skills Details</h3>
    <section>
        @include('courses.tabs.skills_info')
    </section>
    <h3>SEO Details</h3>
    <section>
        @include('courses.tabs.seo_details')
    </section>
</div>
@section('css')
<link href="{{asset('admin/lib/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
<link href="{{asset('admin/lib/spectrum-colorpicker/spectrum.css')}}" rel="stylesheet">
<link href="{{asset('admin/lib/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
@endsection
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('admin/lib/jquery-steps/jquery.steps.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/lib/bootstrap-datetimepicker/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/lib/spectrum-colorpicker/spectrum.js') }}"></script>
<script type="text/javascript">
var is_edit = "{{ isset($course) ? true : false }}";
</script>
<script type="text/javascript" src="{{ asset('admin/modules/courses/form.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>
<script>
$(document).on('click', "#addNewRow", function () {
    var html = '<div class="row row-sm skills" ><div class="col-sm-6"><div class="form-group">{!! Form::text('course_skills[]',null, ['class'=>'form - control', 'id' => 'course_skills']) !!}</div> </div> <div class="col-sm-1"> <div class="form-group"><a href="javascript:;" class="btn btn-danger btn-sm removeRow"> <i class="fas fa-trash"></i> </a></div></div></div>';
    $("#addskills").append(html);
});
$(document).on('click', ".removeRow", function () {
    var self = $(this);
    self.parents(".skills").remove();
});
</script>
@endsection

