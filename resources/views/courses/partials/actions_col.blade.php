@if(session('partner_id') == 1)
    @if($item->is_active == 1)
        <a href="{{ env('FRONT_URL').'course/'.$item->slug }}"
           class="btn btn-secondary btn-xs" target="_blank">
            <i class="fas fa-eye"></i>
        </a>
    @else
        <a href="{{ env('FRONT_URL').'course/'.$item->slug .'?isDraft=on'}}"
           class="btn btn-secondary btn-xs" target="_blank">
            <i class="fas fa-eye"></i>
        </a>
    @endif
    <a href="{{ route('courses.edit', $item->id) }}"
       class="btn btn-primary btn-xs">
        <i class="fas fa-edit"></i>
    </a>
    <a href="{{ route('faqs.create', ['course_id' => $item->id]) }}"
       class="btn btn-dark btn-xs">
        <i class="fa fa-question-circle"></i>
    </a>
    <a href="{{ route('courses.activity', $item->id) }}" title="Activity Logs"
       class="btn btn-light btn-xs">
        <i class="fa fa-cog"></i>
    </a>
    <a href="{{ ($item->is_career_plan_course == 0) ? route('courses.get_curriculum', $item->id) : route('courses.career_plan_curriculum', $item->id) }}"
       class="btn btn-info btn-xs">
        Curriculum
    </a>
    @if(isset($item->relatedLandingPage))
        <a href="{{ route('landing-pages.edit', $item->relatedLandingPage->id) }}" target="_blank"
           class="btn btn-success btn-xs showTooltip" data-toggle="tooltip" data-title="Landing Page Created">
            LP
        </a>
    @endif
@else
    <a href="javascript:;" data-price="{{ $item->partnerCourses[0]->course_fees }}" data-id="{{ $item->id }}"
       class="btn btn-primary btn-xs openUpdatePriceModal">
        <i class="fas fa-edit"></i>
    </a>
@endif
