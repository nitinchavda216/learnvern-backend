<div class="d-inline hideFromBackendStatus az-toggle az-toggle-secondary float-left {{ ($item->hide_from_backend == 1) ? 'on' : '' }}"
     data-id="{{ $item->id }}">
    <span></span>
</div>
