<span data-toggle="tooltip" data-placement="left" title="" data-original-title="Units in the Course"><i class="fa fa-play-circle mr-1"></i>{{ count($item->relatedUnitCurriculum) }}</span>
<hr class="my-1">
<span><i class="fa fa-file mr-1"></i>{{ count($item->relatedAssignmentCurriculum) }}</span>
<hr class="my-1">
<span><i class="fa fa-question-circle mr-1"></i>{{ count($item->relatedQuizCurriculum) }}</span>
