{{ count($item->relatedComments->where('is_default_testimonial', 1)) }}
<a href="{{ route('reviews.create', ['course_id' => $item->id]) }}"
   class="btn btn-success btn-xs d-inline">
    <i class="fa fa-plus"></i>
</a>