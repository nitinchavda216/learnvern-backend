@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title">Activity Log Report</h2>
                <div id="data-container">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-4">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Search :</label>
                                {!! Form::text('search', null,['id' => 'search', 'class' => 'form-control']) !!}
                            </div>
                            <div class="col-md-3">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Log Action :</label>
                                {!! Form::select('log_action', ['created' => 'Created', 'updated' => 'Updated', 'deleted' => 'Deleted'], null, ['id' => 'log_action', 'class' => 'form-control', 'placeholder' => "Select.."]) !!}
                            </div>
                            <div class="col-md-2 mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>User Name</th>
                            <th>Log Model Name</th>
                            <th>Log Action</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="updateChangesModal" class="modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Details</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3 style="text-align: center">Old Data</h3>
                            <ul id="old">
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <h3 style="text-align: center">Changes</h3>
                            <ul id="attributes" class="list-group">
                            </ul>
                        </div>
                    </div>
                </div><!-- modal-body -->
            </div>
        </div><!-- modal-dialog -->
    </div>

@endsection
@section('footer_scripts')
    <script type="text/javascript">
        function myFunction(id) {
            $.ajax({
                url: $app_url + '/get_activity_changes',
                data: "id=" + id,
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    var attributes = data.attributes;
                    var set = '';
                    $.each(attributes, function (index, val) {
                        set += '<li class="list-group-item"><b>' + index + '</b> : ' + val + '</li>';
                    });
                    var old = data.old;
                    var setold = ''
                    $.each(old, function (index, val) {
                        setold += '<li class="list-group-item"><b>' + index + '</b> : ' + val + '</li>';
                    });
                    $('#updateChangesModal').modal('toggle');
                    $('#attributes').html(set);
                    $('#old').html(setold);
                }
            });
        }
        $(document).ready(function () {

            if ($('#dataTableList').length) {
                var $dataTableList = $('#dataTableList').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    bFilter: false,
                    "aaSorting": [],
                    ajax: {
                        url: $app_url + '/activity-logs/getAjaxListData',
                        data: function (d) {
                            d.search = $('#search').val();
                            d.log_action = $('#log_action').val();
                        }
                    },
                    "columns": [
                        {data: 'name', 'sortable': false, 'width': "40%"},
                        {data: 'log_name', 'sortable': false, 'width': "30%"},
                        {data: 'description', 'sortable': false, 'width': "30%"},
                        {data: 'action', 'orderable': false, 'width': "20%"}
                    ],
                    select: true,
                    bStateSave: true,
                    fnStateSave: function (settings, data) {
                        localStorage.setItem("dataTables_state", JSON.stringify(data));
                    },
                    fnStateLoad: function (settings) {
                        return JSON.parse(localStorage.getItem("dataTables_state"));
                    }
                });

                $('#search-form').on('submit', function (e) {
                    $dataTableList.draw();
                    e.preventDefault();
                });

                $('#reset-filters').on('click', function () {
                    $("#search").val(null);
                    $("#log_action").val(null);
                    $dataTableList.draw();
                });
            }
        });
    </script>
@endsection

