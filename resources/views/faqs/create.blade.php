@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Create Course Faq
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('faqs') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="card card-body pd-40">
                        {!! Form::open(['route'=>'faqs.store','method' => 'POST', 'enctype'=> "multipart/form-data", 'files' => true, 'id' => "courseForm"]) !!}
                        @include("faqs.form")
                        {!! Form::close() !!}
                        <div class="newRowTemplate" style="display: none;">
                            <hr>
                            <div class="pd-30 pd-sm-20 bg-light mg-t-10">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="az-content-label">Question: </label>
                                        {!! Form::text('question[]', null, ['class'=>'form-control', 'data-validation' => 'required' ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="az-content-label">Answer: </label>
                                        {!! Form::textarea('answer[]', null, ['class'=>'form-control editor']) !!}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                <button type="submit" class="btn btn-danger mg-md-t-9 btn-sm removeRow"><i
                                            class="fas fa-trash"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        var course_id = "{{ request('course_id') }}";
        var $document = $(document);
        $document.ready(function () {
            if (course_id != "") {
                getFaqs(course_id);
            }
            $document.on('click', "#addNewRow", function () {
                var newRowCloned = $('.newRowTemplate').last().clone();
                newRowCloned.show();
                newRowCloned.find('.editor').summernote({
                    tabsize: 4,
                    height: 250
                });
                $("#courseFaqs").prepend(newRowCloned);
            });

            $document.on('click', ".removeRow", function () {
                var self = $(this);
                self.parents(".newRowTemplate").remove();
            });
            $(document).on('change', "#course_id", function () {
                var $course_id = $("#course_id").val();
                getFaqs($course_id);
            });
            $document.on('click', ".deleteFaq", function () {
                var self = $(this);
                var faqId = self.data('id');
                bootbox.confirm({
                    message: "Are you sure you want to delete this faq?",
                    buttons: {
                        confirm: {
                            label: "Yes",
                            className: 'btn-success'
                        },
                        cancel: {
                            label: "No",
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if (result === true) {
                            $.ajax({
                                type: "get",
                                url: $app_url + '/faq/delete/' + faqId,
                                data: {},
                                success: function () {
                                    self.parents('.faqTr').remove();
                                    toastr.success("Faq Deleted Successfully!", "Success!", {timeOut: 2000});
                                }
                            });
                        }
                    }
                });
            });
        });
        function getFaqs(courseId) {
            $("#FaqDetails").html("");
            $.ajax({
                type: "get",
                dataType: "json",
                url: $app_url + '/get-question-by-course' + courseId,
                data: {},
                success: function (response) {
                    $("#FaqDetails").html(response.html);
                },
            });
        }
    </script>
@endsection
