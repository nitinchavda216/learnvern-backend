<a href="{{ route('faqs.edit', $item->id) }}" class="btn btn-primary btn-xs">
    <i class="fas fa-edit"></i>
</a>
<a href="javascript:;"
   onclick="confirmDelete('{{ route('faqs.delete',$item->id) }}')"
   class="btn btn-danger btn-xs">
    <i class="fas fa-trash"></i>
</a>
<div class="d-inline az-toggle az-toggle-secondary float-right {{ ($item->is_active == 1) ? 'on' : '' }}"
     data-id="{{ $item->id }}">
    <span></span>
</div>