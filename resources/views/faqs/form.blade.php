<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Course :</label>
            {!! Form::select('course_id', $courses, $faq->course_id ?? request('course_id'), ['id' => 'course_id','class'=>'form-control select2', 'data-validation' => 'required', 'placeholder' => "" ,'data-id' => 'course_id']) !!}
        </div>
    </div>
    @if(!isset($faq))
        <div class="col-sm-6">
            <div class="float-right pt-4">
                <button type="button" class="btn btn-sm btn-primary" id="addNewRow">Add New FAQ</button>
            </div>
        </div>
    @endif
</div>
<div id="FaqDetails"></div>
@if(isset($faq))
    <div class="pd-30 pd-sm-20 bg-gray-200">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Question :</label>
            {!! Form::text('question', $faq->question ?? old('question'), ['class'=>'form-control', 'data-validation' => 'required' ]) !!}
        </div>
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Answer :</label>
            {!! Form::textarea('answer', $faq->answer ?? old('answer'), ['class'=>'form-control wysiwyg_editor']) !!}
        </div>
    </div>
@else
    <div id="courseFaqs">
        <hr>
        <div class="pd-30 pd-sm-20 bg-light mg-t-10">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="az-content-label">Question: </label>
                    {!! Form::text('question[]', old('question'), ['class'=>'form-control', 'data-validation' => 'required' ]) !!}
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="az-content-label">Answer: </label>
                    {!! Form::textarea('answer[]', old('question'), ['class'=>'form-control wysiwyg_editor']) !!}
                </div>
            </div>
        </div>
        <hr>
    </div>
@endif
<button type="submit" class="btn btn-az-primary mg-md-t-9">{{ isset($faq) ? "Update" : "Submit" }}</button>

