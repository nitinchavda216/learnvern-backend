@csrf

<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Page Section</label>
    {!! Form::select('page_type', \App\Models\WidgetsSettings::PAGE_SECTION, $widgetdetails->page_type ?? old('page_type'), ['class'=>'form-control select2Input', 'id' => 'page_type', 'placeholder' => "Select Page Section"]) !!}
</div>
<div class="row row-sm">


    <div class="col-sm-12">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600"> Heading </label>
            {!! Form::text('name', $widgetdetails->name ?? old('name'), ['class'=>'form-control', 'id' => 'name']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Content</label>
    <input type="radio" name="contenttype" id="contenttype" checked value="html"> HTML
    <input type="radio" name="contenttype" id="contenttype" value="text"> Text
    @if(isset($widgetdetails) && !empty($widgetdetails->content) && $widgetdetails->contenttype == 'html')
        {{ Form::textarea('content1', isset($widgetdetails) ? $widgetdetails->content : old('content') ,['class' => 'form-control wysiwyg_editor','id'=>'content1','data-validation' => 'required', 'data-validation-error-msg' => "Please enter a content!"] ) }}
        {!! Form::text('content2',  old('content'), ['class'=>'form-control','style'=>'display:none;', 'id' => 'textcontent']) !!}
    @elseif(isset($widgetdetails) && !empty($widgetdetails->content) && $widgetdetails->contenttype == 'text')
        {{ Form::textarea('content1',  isset($widgetdetails) ? $widgetdetails->content : old('content') ,['class' => 'form-control wysiwyg_editor', 'id'=>'content1', 'data-validation-error-msg' => "Please enter a content!"] ) }}
        {!! Form::text('content2', old('content'), ['class'=>'form-control','style'=>'display:none;', 'id' => 'textcontent']) !!}
    @elseif(isset($widgetdetails) && !empty($widgetdetails->content))
        {{ Form::textarea('content1',  old('content') ,['class' => 'form-control wysiwyg_editor','id'=>'content1','style'=>'display:none;', 'data-validation-error-msg' => "Please enter a content!"] ) }}
        {!! Form::text('content2', old('content'), ['class'=>'form-control','style'=>'display:none;', 'id' => 'textcontent']) !!}
    @else
        {{ Form::textarea('content1', isset($widgetdetails) ? $widgetdetails->content : old('content') ,['class' => 'form-control wysiwyg_editor','id'=>'content1', 'data-validation-error-msg' => "Please enter a content!"] ) }}
        {!! Form::text('content2', isset($widgetdetails) ? $widgetdetails->content : old('content'), ['class'=>'form-control','style'=>'display:none;', 'id' => 'textcontent']) !!}
    @endif
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Image :</label>
    <input type="file" accept="image/*" class="course_image dropify" data-show-remove="false"
           data-allowed-file-extensions="png jpg jpeg"
           @if(isset($widgetdetails) && isset($widgetdetails->image)) data-default-file="{{ shoWidgetsImage($widgetdetails->image) }}"
           @else
           @endif
           name="image" id="image">
    {{--data-default-file--}}
</div>
<button class="btn btn-az-primary">{{ isset($widgetdetails) ? "Update" : "Submit" }}</button>
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('admin/lib/jquery-steps/jquery.steps.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('admin/lib/bootstrap-datetimepicker/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('admin/lib/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script type="text/javascript">

        var showSubmitButton = "{{ isset($course) ? true : false }}";
        $('input[type=radio][name=contenttype]').change(function () {
            var check = $(this).val();
            $('#textcontent').toggle(check == 'text');
            $('.note-editor').toggle(!check == 'text');
        });
    </script>
    <script type="text/javascript" src="{{ asset('admin/modules/courses/form.js') }}"></script>
@endsection