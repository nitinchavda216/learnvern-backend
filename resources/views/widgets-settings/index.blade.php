@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Widgets Settings
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{route('widget-setting.create')}}" class="btn btn-success btn-sm mt-negative"
                           title="Add New">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>Page Section</th>
                            <th>Header</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($widgetdetails)
                            @foreach($widgetdetails as $item)
                                <tr>
                                    <td>{{(isset(\App\Models\WidgetsSettings::PAGE_SECTION[$item->page_type])) ? \App\Models\WidgetsSettings::PAGE_SECTION[$item->page_type] : ''}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>
                                        <a href="{{ route('widget-setting.edit', $item->id) }}"
                                           class="btn btn-primary btn-xs d-inline">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <div class="d-inline az-toggle az-toggle-secondary float-right {{ ($item->is_active == 1) ? 'on' : '' }}"
                                             data-id="{{ $item->id }}">
                                            <span></span>
                                        </div>


                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataTableList').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            $('.az-toggle').on('click', function () {
//                console.log($(this).toggleClass('on'));
                var status = ($(this).toggleClass('on').hasClass('on') == true) ? 1 : 0;
                var id = $(this).data('id');
                //console.log(id);
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: $app_url + '/widget-setting/update_status',
                    data: {'is_active': status, 'id': id},
                    success: function (data) {
                        toastr.success("Status Updated Successfully!", "Success!", {timeOut: 2000});
                    }
                });
            });
        });


    </script>
@endsection

