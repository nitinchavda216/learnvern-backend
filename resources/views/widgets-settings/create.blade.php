@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Create Widgets Settings
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('widget-setting') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="card card-body " id="CoursesInformation">
                        {!! Form::open(['route'=>'widget-setting.store','method' => 'POST', 'enctype'=> "multipart/form-data", 'files' => true, 'id' => "locationForm"]) !!}
                        @include("widgets-settings.form", ['formMode' => 'create'])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
