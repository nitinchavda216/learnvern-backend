@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Edit Widgets Settings
                    {{--<span class="pull-right d-inline-block float-right"></span>--}}
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('widget-setting') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                {!! Form::open(['route'=>['widget-setting.update', $widgetdetails->id],'method' => 'POST', 'enctype'=> "multipart/form-data", 'files' => true, 'id' => "locationForm"]) !!}
                @include("widgets-settings.form", ['formMode' => 'edit'])
                {!! Form::close() !!}
                <div class="pd-20 pd-sm-20 portlet-light"></div>
            </div>
        </div>
    </div>
@endsection
