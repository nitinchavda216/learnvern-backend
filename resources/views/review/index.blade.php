@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Reviews
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('reviews.create') }}" class="btn btn-success btn-sm mt-negative">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add Default Testimonial
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="pd-20 pd-sm-20 bg-gray-200 wd-xl-100p mb-3">
                        <div class="row row-xs">
                            <div class="col-md-4">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Search</label>
                                <input name="search" id="search" type="text" class="form-control" placeholder="Search">
                            </div>
                            <div class="col-md-4">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Course</label>
                                {!! Form::select('course_id', $courses, null, ['class'=>'form-control select2', 'id' => 'course_id', 'placeholder' => ""]) !!}
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Date :</label>
                                    <input name="date_range" id="date_range" type="text" class="form-control"
                                           autocomplete="off" placeholder="Select Date">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Rating </label>
                                <select name="course_status" class="form-control select2" id="rating">
                                    <option></option>
                                    <option value="5">Five Rating</option>
                                    <option value="4">Four Rating</option>
                                    <option value="3">Three Rating</option>
                                    <option value="2">Two Rating</option>
                                    <option value="1">One Rating</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Default Testimonial?</label>
                                {!! Form::select('is_default_testimonial', [1 => 'Yes', 0 => 'No'], null, ['class'=>'form-control select2', 'id' => 'is_default_testimonial', 'placeholder' => ""]) !!}
                            </div>
                            <div class="col-md-2 mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <hr>
                    <b class="pull-right d-inline-block float-right">
                        <div class="badge bg-primary text-white p-2">
                            Active Review: {{ $active['active'] }}
                        </div>
                        <div class="badge bg-primary text-white p-2">
                            Total Reviews: {{ $active['total'] }}
                        </div>
                    </b>
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>Review Content</th>
                            <th>Rating</th>
                            <th>Review Author</th>
                            <th>Course Name</th>
                            <th>Review Date</th>
                            <th>Is Default?</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <script type="text/javascript" src="{{ asset('admin/modules/reviews/index.js') }}"></script>
@endsection

