<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Course :</label>
            @if(!isset($review) || (isset($review) && ($review->is_default_testimonial == 1)))
                {!! Form::select('course_id', $courses, $review->course_id ?? request('course_id'), ['class'=>'form-control selectOption', 'data-validation' => 'required', 'placeholder' => ""]) !!}
            @else
                {!! Form::text('course', $review->courseDetail->name ?? "- - -", ['class'=>'form-control', 'disabled']) !!}
            @endif
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Author :</label>
            {!! Form::text('author', $review->author ?? old('author'), ['class'=>'form-control', 'data-validation' => 'required']) !!}
        </div>
    </div>
</div>
@if(request('course_id') != "")
    <input type="hidden" name="redirect_course_page" value="true">
@endif
<div class="row row-sm">
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Rating:</label>
            {!! Form::select('rating', [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5], $review->rating ?? old('rating'), ['class'=>'form-control select2-no-search', 'data-validation' => 'required']) !!}
        </div>
        @if(isset($review))
            <div class="form-group d-inline-block mr-3">
                <label class="ckbox">
                    <input type="checkbox" @if($review->is_default_testimonial == 1) checked
                           @endif name="is_default_testimonial"><span>Is default testimonial?</span>
                </label>
            </div>
        @endif
    </div>
    <div class="col-sm-8">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Content :</label>
            <div class="input-group">
                {!! Form::textarea('content', $review->content ?? old('content'), ['class'=>'form-control', 'data-validation' => 'required', 'rows' => 4]) !!}
            </div>
        </div>
    </div>
</div>
<button type="submit" class="btn btn-az-primary mg-md-t-9">{{ isset($review) ? "Update" : "Submit" }}</button>
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.selectOption').select2({
                placeholder: '--SELECT--'
            });
        })
    </script>
@endsection