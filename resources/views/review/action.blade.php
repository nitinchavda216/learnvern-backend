<a href="{{ route('reviews.edit', $review->id) }}" class="btn btn-primary btn-xs d-inline">
    <i class="fas fa-edit"></i>
</a>


<div class="d-inline az-toggle az-toggle-secondary float-right {{ ($review->is_active == 1) ? 'on' : '' }}"
     data-id="{{ $review->id }}">
    <span></span>
</div>
