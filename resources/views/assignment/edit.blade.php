@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Edit Assigment
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('assignments') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="card card-body pd-40">
                        {!! Form::open(['route'=>['assignments.update', $assignment->id],'method' => 'POST', 'enctype'=> "multipart/form-data", 'files' => true, 'id' => "courseForm"]) !!}
                        @if(count($relatedCourses) > 0)
                            <div class="card">
                                <div class="card-header">
                                    <strong>Related Courses</strong>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered mg-b-0">
                                        <tbody>
                                        <tr>
                                            <th scope="row">
                                                {{ implode(', ', $relatedCourses) }}
                                            </th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br>
                        @endif
                        @include("assignment.form")
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="attachmentDiv p-2" style="display: none;">
        <div class="row row-sm">
            <div class="col-sm-4">
                <div class="form-group">
                    {!! Form::text('title_attachment[]',old('name'), ['class'=>'form-control', 'id' => 'title_attachment', 'placeholder' => 'Enter Title..', 'data-validation' => 'required']) !!}
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <input type="file" class="attachment-dropify" id="attachment" name="attachment[]" data-show-remove="false" data-validation="required">
                    <input type="hidden" name="remove_attachment" id="removeAttachment" value="false">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <button type="button" class="btn btn-sm btn-danger removeRow"><i
                                class="fa fa-trash"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="updateSlugModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    Update Assignment Slug
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route('assignment.slug-update')}}" id="updateSlugForm"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" name="unit_id" value="{{ $assignment->id }}">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Assignment Slug:</label>
                            {!! Form::text('slug', $assignment->slug, ['class'=>'form-control slugInput', 'data-validation' => 'required', 'placeholder' => 'Enter New Slug']) !!}
                        </div>{{--
                        @if(count($oldSlugList) > 0)
                            <br>
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Old Slugs:</label>
                            <ul>
                                @foreach($oldSlugList as $slugItem)
                                    <li>{{ $slugItem }}</li>
                                @endforeach
                            </ul>
                        @endif--}}
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-indigo" id="updateSlugBtn">Update</button>
                </div>
            </div>
        </div>
    </div>
@endsection

