<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Assignment Name:</label>
            {!! Form::text('name', $assignment->name ?? old('name'), ['class'=>'form-control', 'id' => 'name', 'data-validation' => 'required']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        @if(isset($assignment->slug))
            <div class="form-group">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Assignment Slug:</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="slug" value="{{ $assignment->slug }}" readonly>
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button" data-toggle="modal"
                                data-target="#updateSlugModal"><i class="fa fa-edit"></i></button>
                    </span>
                </div>
            </div>
        @else
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Assignment Slug:</label>
            {!! Form::text('slug', $assignment->slug ?? old('slug'), ['class'=>'form-control', 'id' => 'slug', 'data-validation' => 'required', isset($assignment) ? 'disabled' : ""]) !!}
            <span id="slugmsg"></span>
        </div>
        @endif
    </div>

</div>
<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Course:</label>
            {!! Form::select('course_id', $courses, $assignment->course_id ?? old('course_id'), ['id'=>'course_id', 'class'=>'form-control', 'data-validation' => 'required', 'placeholder' => ""]) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Section:</label>
            {!! Form::select('section_id', $sections, $assignment->section_id ?? old('section_id'), ['id'=>'section_id', 'class'=>'form-control', 'data-validation' => 'required', 'placeholder' => ""]) !!}

        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Assignment Vimeo Video id:</label>
            {!! Form::text('video_id',$assignment->video_id ?? old('video_id'), ['class'=>'form-control', 'id' => 'video_id', 'data-validation' => 'required']) !!}
        </div>
    </div>
</div>
<div class="row row-sm">
    @if(isset($assignment))
        <div class="col-sm-3">
            <div class="form-group">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Assignment Time:</label>
                {!! Form::text('time',$assignment->time ?? old('assignment_time'), ['class'=>'form-control form_time', 'id' => 'assignment_time', 'disabled']) !!}
            </div>
        </div>
    @endif
    <div class="col-sm-3">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Assignment Marks:</label>
            {!! Form::text('mark', $assignment->mark ??old('mark'), ['class'=>'form-control', 'id' => 'mark', 'data-validation' => 'required']) !!}
        </div>
    </div>
    <div class="col-sm-6 mt-4 pr-3">
        <div class="form-group d-inline-block">
            <label class="ckbox">
                <input type="checkbox" @if(isset($assignment) && $assignment->is_free == 1) checked
                       @endif name="is_free"><span>Free Assignment?</span>
            </label>
        </div>
        <div class="form-group d-inline-block">
            <label class="ckbox">
                <input type="checkbox" @if(isset($assignment) && $assignment->include_course_evaluation == 1) checked
                       @endif
                       name="include_course_evaluation"><span>Include Course Evaluation</span>
            </label>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <button type="button" class="btn btn-sm btn-primary" id="addNewAttachment"><i
                    class="fa fa-plus"></i> Add Attachment
        </button>
    </div>
    <div class="card-body p-0">
        <div id="sectionRows">
            @if(isset($assignment) && sizeof($attachment) != 0)
                @foreach($attachment as $key => $value)
                    <div class="attachmentDiv p-2">
                        <div class="row row-sm">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Title:</label>
                                    {!! Form::text('title_attachment[]', $value->title, ['class'=>'form-control', 'id' => 'title_attachment', 'data-validation' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Attachment:</label>
                                    <input type="file" class="dropify"
                                           @if($value->attachment != null) data-default-file="{{ showAttachment($value->attachment) }}"
                                           @endif
                                           id="attachment" name="attachment[]">
                                    <input type="hidden" name="remove_attachment" id="removeAttachment" value="false">
                                    <input type="hidden" name="hidden_attachment{{$key}}"
                                           value="{{$value->attachment}}">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <button type="button" class="btn btn-sm btn-danger removeRow"><i
                                                class="fa fa-trash"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>
<button type="submit" id="assignsubmit"
        class="btn btn-az-primary mg-md-t-9">{{ isset($assignment) ? "Update" : "Submit" }}</button>
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            var is_edit = "{{ isset($assignment) ? true : false }}";
            $('#course_id').select2({
                placeholder: 'Select Course'
            });
            $(document).on('change', "#course_id", function () {
                $("#section_id").val(null).trigger('change');
            });
            $(document).on('click', "#addNewAttachment", function () {
                var html = $(".attachmentDiv").last().clone();
                html.show();
                html.find(".attachment-dropify").dropify();
                $("#sectionRows").append(html);
            });

            $(document).on('click', ".removeRow", function () {
                $(this).parents(".attachmentDiv").remove();
            });

            $(".dropify").dropify();
            $("#section_id").select2({
                placeholder: 'Select Section',
                ajax: {
                    url: $app_url + "/get-sections-by-course",
                    type: "get",
                    dataType: 'json',
                    data: function (params) {
                        var searchTerm = params.term;
                        return {
                            searchTerm: searchTerm,
                            courseId: $("#course_id").val()
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response

                        };
                    },
                    cache: true
                }
            });
            $(document).on('click', '.dropify-clear', function () {
                $(this).closest(".form-group").find("#hidden_attachment").val(" ");
                $("#removeAttachment").val("true");
            })

            $("#name").focusout(function (i, d) {
                var title = $.trim($("#name").val());
                if (title.length > 0) {
                    title = title.toLowerCase();
                    title = title.replace(/[^a-z0-9\s]/gi, "").replace(/  +/g, " ").replace(/[_\s]/g, "-");
                }
                $("#slug").val(title);
                checkValidSlug();
            });

            $(".slugInput").focusout(function (e) {
                var slug = $(this).val().toLowerCase();
                $(this).val(slug);
                checkValidSlug();
            });

            $(".slugInput").keypress(function (e) {
                var regex = new RegExp("^[A-Za-z0-9-]+$");
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str)) {
                    return true;
                }
                e.preventDefault();
                return false;
            });

            function checkValidSlug() {
                if (!is_edit) {
                    var slug = $("#slug").val();
                    $.ajax({
                        url: $app_url + '/slugExists',
                        method: 'POST',
                        data: {slug: slug},
                        success: function (data) {
                            if (data.status) {
                                toastr.error(data.message, "Error!", {timeOut: 2000});
                                $('#slug').removeClass('valid');
                                $('#slug').addClass('error');
                                $('#slug').parent('div').removeClass('has-success');
                                $('#slug').parent('div').addClass('has-error');
                                $('#assignsubmit').prop("disabled", true);
                            } else {
                                $('#slug').removeClass('error');
                                $('#slug').addClass('valid');
                                $('#slug').parent('div').removeClass('has-error');
                                $('#assignsubmit').prop("disabled", false);
                            }
                        }
                    })
                }
            }

            if (is_edit){
                $(document).on('click', "#updateSlugBtn", function () {
                    var isValid = $("#updateSlugForm").isValid();
                    if (isValid){
                        $("#updateSlugModal").modal('hide');
                        bootbox.confirm({
                            message: "Are you sure you want to update this unit slug?",
                            buttons: {
                                confirm: {
                                    label: "Yes",
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: "No",
                                    className: 'btn-danger'
                                }
                            },
                            callback: function (result) {
                                if (result === true) {
                                    $('#updateSlugForm').submit();
                                }else{
                                    $("#updateSlugModal").modal('show');
                                }
                            }
                        });
                    }
                });
            }
        });
    </script>
@endsection
