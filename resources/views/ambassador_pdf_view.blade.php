<html>
<head>
    <meta http-equiv=Content-Type content="text/html; charset=windows-1252">
    <meta name=Generator content="Microsoft Word 14 (filtered)">
    <style>
        /* Font Definitions */
        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
        }

        @font-face {
            font-family: Tahoma;
            panose-1: 2 11 6 4 3 5 4 4 2 4;
        }

        /* Style Definitions */
        p., li.MsoNormal, div.MsoNormal {
            margin-top: 0in;
            margin-right: 0in;
            /*margin-bottom:10.0pt;*/
            margin-left: 0in;
            line-height: 100%;
            font-size: 11.0pt;
            font-family: "Calibri", "sans-serif";
        }

        p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle {
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 0in;
            margin-left: .5in;
            margin-bottom: .0001pt;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", "sans-serif";
        }

        p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast {
            margin-top: 0in;
            margin-right: 0in;
            margin-bottom: 10.0pt;
            margin-left: .5in;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", "sans-serif";
        }

        @page WordSection1 {
            size: 8.268in 11.693in;
            margin: 27.0pt 1.0in 0 1.0in;
        }

        div.WordSection1 {
            page: WordSection1;
        }

    </style>

</head>

<body lang=EN-US>

<div class=WordSection1>
    <div style="margin:0;padding:0;width:100%;text-align: center;background-color: red;">
        <img src="{{ url('images/letterhead/image001.png') }}" style="width:100%;">
    </div>
    <br clear=ALL>
    <br clear=ALL>
    <div style="margin:0;padding:0;width:100%;text-align: right;">
        <p class=MsoListParagraphCxSpMiddle>Date: <?=date('d-m-Y')?>
        </p>
    </div>
    <br clear=ALL>
    <br clear=ALL>
    <div style="margin:0;padding:0;width:100%;text-align: center;">
        <p class=MsoListParagraphCxSpMiddle style='font-size:14.0pt;
line-height:115%'><b><u>To Whomever It May Concern</u></b>
        </p>
    </div>
    @php
        $full_name = ucwords($user['first_name']);
        $datetime1 = date_create(date("Y-m-d",strtotime($user['created_at'])));
        $datetime2 = date_create(date('Y-m-d'));

        $interval = date_diff($datetime1, $datetime2);

        $months = $interval->format('%m');
        $months = $months>0?$months:1;
    @endphp
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'>&nbsp;</p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'>&nbsp;</p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'><?=$full_name?> has been working with LearnVern as an
        Ambassador for the last <?=$months?> months</p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'>&nbsp;</p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'>&nbsp;</p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'><?=$full_name?> was responsible for promoting
        LearnVern at the college campus. In the <?=$months?> months <?=$full_name?> has successfully signed
        up {{ $referral_count }} students from the campus by using various marketing concepts.</p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'>&nbsp;</p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'>&nbsp;</p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'><?=$full_name?> fulfilled the duties assigned with
        little supervision and was able to successfully communicate to various stakeholders across the campus. This
        success was dependent on leadership and interpersonal skills at all the levels.
    </p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'>&nbsp;</p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'>&nbsp;</p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'>I am happy to act as a reference for <?=$full_name?>
        and vouch for the abilities to lead and be successful in the activities assigned. Feel free to contact me if you
        need further information about LearnVern.</p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'>&nbsp;</p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'>&nbsp;</p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'>Regards,</p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'>&nbsp;</p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'>
        <img width=70 height=72 id="Picture 9" src="{{ url('images/letterhead/image002.png') }}"></p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'>Niral Modi</p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'>CEO </p>
    <p class=MsoListParagraphCxSpMiddle style='margin-left:13.5pt'>LearnVern Pvt
        Ltd</p>
    <p class=MsoListParagraphCxSpLast style='margin-left:13.5pt'>Whatsapp
        +91 8849004643</p>
    <p class=MsoNormal>&nbsp;</p>
    <p class=MsoNormal>&nbsp;</p>
    <p class=MsoNormal>&nbsp;</p>
    <p class=MsoNormal>&nbsp;</p>
    <div style="position:absolute;bottom:0;">
        <img src="{{ url('images/letterhead/image003.png') }}"
             alt="LearnVern.com&#13;&#10;&#13;&#10;, 6th Floor, Arjun Tower, Shivranjani Cross Road, Satellite Rd, Shivranjani, Ahmedabad - 380015"
             width="100%">
    </div>
</div>
</body>
</html>
