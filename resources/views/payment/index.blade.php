@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title">Payment History</h2>


                <div id="data-container">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Search:</label>
                                <input name="search" id="search" type="text" class="form-control" placeholder="Search">
                            </div>
                            @if(session('partner_id') == 1)
                                <div class="col-md-2">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Partners:</label>
                                    {!! Form::select('partner_user_id', $partners, null,['id' => 'partner_user_id', 'class' => 'form-control select2']) !!}
                                </div>
                            @endif
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Transaction Date:</label>
                                <input name="transaction_date" id="transaction_date" type="text" class="form-control"
                                       autocomplete="off" placeholder="Select Date">
                            </div>
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Users Type:</label>
                                {!! Form::select('user_type', array('0' => 'Indian Users', '1' => 'International Users'), old('user_type'), ['class'=>'form-control select2_no_search', 'id' => 'user_type']) !!}
                            </div>

                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Payment Method:</label>
                                {!! Form::select('payment_method', \App\Models\PaymentTransaction::PAYMENT_METHOD_TYPE , [], ['class'=>'form-control select2_no_search', 'id' => 'payment_method', 'placeholder'=>'']) !!}
                            </div>
                            <div class="col-md mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}

                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header bg-gray-200 text-center">
                                    <h5>CAMPAIGN SELL</h5>
                                </div><!-- card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <h3 id="campaign_sell"> - </h3>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header bg-gray-200 text-center">
                                    <h5>ORGANIC SELL</h5>
                                </div><!-- card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <h3 id="organic_sell"> - </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>Full Name</th>
                            <th>User Email</th>
                            <th>Mobile Number</th>
                            <th>Referral Code</th>
                            <th>Affiliate Code</th>
                            <th>Transaction Date</th>
                            <th>Amount</th>
                            <th>Course Name</th>
                            <th>Reference ID</th>
                            <th>Payment Method</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#transaction_date').daterangepicker({
                autoUpdateInput: true,
                startDate: moment().subtract(6, 'days'),
                endDate: moment(),
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY'
                },
                ranges: {
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });

            $('#transaction_date').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            });

            $('#transaction_date').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });


            var $dataTableList = $('#dataTableList').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                bFilter: false,
                "aaSorting": [],
                ajax: {
                    url: $app_url + '/payment-history/getAjaxData',
                    data: function (d) {
                        d.search = $('#search').val();
                        d.transaction_date = $('#transaction_date').val();
                        d.partner_user_id = $('#partner_user_id').val();
                        d.user_type = $('#user_type').val();
                        d.payment_method = $('#payment_method').val();
                    }
                },
                "columns": [
                    {data: 'name', 'sortable': false, 'width': "10%"},
                    {data: 'email', 'sortable': false, 'width': "10%"},
                    {data: 'mobile_number', 'sortable': false, 'width': "10%"},
                    {data: 'referral_code', 'sortable': false, 'width': "10%"},
                    {data: 'affiliate_code', 'sortable': false, 'width': "10%"},
                    {data: 'transaction_date', 'sortable': false, 'width': "10%"},
                    {data: 'amount', 'sortable': false, 'width': "8%"},
                    {data: 'course', 'sortable': false, 'width': "15%"},
                    {data: 'referrence_id', 'sortable': false, 'width': "10%"},
                    {data: 'payment_method', 'sortable': false, 'width': "10%"}
                ],
                "preDrawCallback": function( settings ) {
                    $("#organic_sell").html('-');
                    $("#campaign_sell").html('-');
                },
                "drawCallback": function( settings ) {
                 $("#organic_sell").html(settings.json.countForCampaingOrOrganicResult.organic_sell);
                 $("#campaign_sell").html(settings.json.countForCampaingOrOrganicResult.campaign_sell);
                }
            });

            $('#search-form').on('submit', function (e) {
                $dataTableList.draw();
                e.preventDefault();
            });

            $('#reset-filters').on('click', function () {
                $("#search").val(null);
                $("#transaction_date").val(null);
                $("#partner_user_id").val(1).trigger('change');
                $("#user_type").val(0   ).trigger('change');
                $("#payment_method").val(0).trigger('change');
                $dataTableList.draw();
            });
        });
    </script>
@endsection

