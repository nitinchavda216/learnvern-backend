@csrf
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Page Heading</label>
            {{ Form::text('page_title', isset($page) ? $page->page_title : old('page_title') ,['class' => 'form-control', 'data-validation' => 'required'] ) }}

        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Page Slug</label>
            {{ Form::text('slug', isset($page) ? $page->slug : old('slug') ,['class' => 'form-control', 'data-validation' => 'required'] ) }}

        </div>
    </div>
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Content</label>
    {{ Form::textarea('content', isset($page) ? $page->content : old('content') ,['class' => 'form-control wysiwyg_editor','id'=>'content',
       'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a content!"] ) }}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">App Content</label>
    {{ Form::textarea('app_content', isset($page) ? $page->app_content : old('app_content') ,['class' => 'form-control wysiwyg_editor','id'=>'app_content',
       'data-validation' => 'required', 'data-validation-error-msg' => "Please enter a app content!"] ) }}
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Meta Title</label>
            {{ Form::text('meta_title', isset($page) ? $page->meta_title : old('meta_title') ,['class' => 'form-control', 'data-validation' => 'required'] ) }}

        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Meta Description</label>
            {{ Form::text('meta_description', isset($page) ? $page->meta_description : old('meta_description') ,['class' => 'form-control','id'=>'meta_description', 'rows' => 3] ) }}

        </div>
    </div>
</div>

<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Meta Keywords</label>
    {{ Form::text('meta_keywords', isset($page) ? $page->meta_keywords : old('meta_keywords') ,['class' => 'form-control','id'=>'meta_keywords'] ) }}

</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Schema :</label>
    {!! Form::textarea('schema_script', $page->schema_script ?? old('schema_script'), ['class'=>'form-control', 'id' => 'schema_script', 'rows' => 4]) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Canonical URL :</label>
    {!! Form::text('canonical_url', $page->canonical_url ?? old('canonical_url'), ['class'=>'form-control', 'id' => 'canonical_url']) !!}
</div>
<div class="text-right">
    <button class="btn btn-az-primary ">{{ isset($page) ? "Update" : "Create Page" }}</button>
</div>

