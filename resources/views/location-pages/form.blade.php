<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Course :</label>
            {!! Form::select('course_id', $courses, $location_page->course_id ?? old('course_id'), ['id'=>'course_id', 'class'=>'form-control select2-no-search', 'data-validation' => 'required', 'placeholder' => ""]) !!}
        </div>
    </div>
</div>
@if(isset($location_page))
    <div class="row row-sm">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Page Name :</label>
                {!! Form::text('name', $location_page->name ?? old('name'), ['class'=>'form-control', 'id' => 'name', 'data-validation' => 'required']) !!}
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Page Slug:</label>
                {!! Form::text('slug',$location_page->slug ?? old('slug'), ['class'=>'form-control', 'disabled']) !!}
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="az-content-label tx-11 tx-medium tx-gray-600">Page Content:</label>
        {!! Form::textarea('content', $location_page->content ?? old('content'), ['class'=>'form-control wysiwyg_editor', 'id' => 'content']) !!}
    </div>
    <div class="form-group">
        <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Introduction Video :</label>
        {!! Form::text('course_intro_video', $location_page->course_intro_video ?? old('course_intro_video'), ['class'=>'form-control', 'data-validation' => 'required']) !!}
    </div>
    <div class="form-group">
        <label class="az-content-label tx-11 tx-medium tx-gray-600">Meta Title :</label>
        {!! Form::text('meta_title', $location_page->meta_title ?? old('meta_title'), ['class'=>'form-control', 'id' => 'meta_title']) !!}
    </div>
    <div class="form-group">
        <label class="az-content-label tx-11 tx-medium tx-gray-600">Meta Description :</label>
        {!! Form::text('meta_description', $location_page->meta_description ?? old('meta_description'), ['class'=>'form-control', 'id' => 'meta_description']) !!}
    </div>
    <div class="form-group">
        <label class="az-content-label tx-11 tx-medium tx-gray-600">Meta Keyword :</label>
        {!! Form::text('meta_keywords', $location_page->meta_keywords ?? old('meta_keywords'), ['class'=>'form-control', 'id' => 'meta_keywords']) !!}
    </div>
    <div class="form-group">
        <label class="az-content-label tx-11 tx-medium tx-gray-600">Schema :</label>
        {!! Form::textarea('schema_script', $location_page->schema_script ?? old('schema_script'), ['class'=>'form-control', 'id' => 'schema_script', 'rows' => 4, 'disabled']) !!}
    </div>
    <div class="form-group">
        <label class="az-content-label tx-11 tx-medium tx-gray-600">Canonical URL :</label>
        {!! Form::text('canonical_url', $location_page->canonical_url ?? old('canonical_url'), ['class'=>'form-control', 'id' => 'canonical_url']) !!}
    </div>
@else
    <div id="locationDetails"></div>
@endif
<div class="text-right">
    <button class="btn btn-az-primary ">{{ isset($location_page) ? "Update" : "Create" }}</button>
</div>
@section('footer_scripts')
    <script type="text/javascript">
        var is_edit = "{{ isset($location_page) ? true : false }}";
        $(document).ready(function () {
            $(document).on('change', "#course_id", function () {
                if (!is_edit) {
                    var course_id = $(this).val();
                    $("#locationDetails").html("");
                    $.ajax({
                        type: "post",
                        dataType: "json",
                        url: $app_url + '/location-pages/get-course-data',
                        data: {course_id: course_id},
                        success: function (data) {
                            $("#locationDetails").html(data.html);
                            $('.wysiwyg_editor').summernote({
                                tabsize: 4,
                                height: 250
                            });
                        }
                    });
                }
            })
        })
    </script>
@endsection
