@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Location Pages
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{route('location-pages.create')}}" class="btn btn-success btn-sm mt-negative"
                           title="Add New">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                            </span>
                </h2>
                <div id="data-container">
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>Page Name</th>
                            <th>Page Slug</th>
                            <th>Courses Name</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($locationPages)
                            @foreach($locationPages as $item)
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->slug }}</td>
                                    <td>{{ $item->CourseDetail['name'] }}</td>
                                    <td>
                                        @if($item->is_active == 1)
                                            <a href="{{ env('FRONT_URL').'location/'.$item->slug }}"
                                               class="btn btn-secondary btn-xs d-inline" target="_blank">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                        @endif
                                        <a href="{{ route('location-pages.edit', $item->id) }}"
                                           class="btn btn-primary btn-xs d-inline">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <div class="d-inline az-toggle az-toggle-secondary float-right {{ ($item->is_active == 1) ? 'on' : '' }}"
                                             data-id="{{ $item->id }}">
                                            <span></span>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataTableList').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                }
            });
            // Toggle Switches
            $('.az-toggle').on('click', function () {
                var status = $(this).toggleClass('on') == true ? 1 : 0;
                var id = $(this).data('id');
                //console.log(id);
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: $app_url + '/location-pages/update_status',
                    data: {'is_active': status, 'id': id},
                    success: function (data) {
                        toastr.success("Status Updated Successfully!", "Success!", {timeOut: 2000});
                    }
                });
            });
        });
    </script>
@endsection

