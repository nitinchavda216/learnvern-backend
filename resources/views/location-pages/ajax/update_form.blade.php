<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Page Name :</label>
            {!! Form::text('name', $course->name, ['class'=>'form-control', 'id' => 'name', 'data-validation' => 'required']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Page Slug:</label>
            {!! Form::text('slug',$course->slug, ['class'=>'form-control', 'id' => 'brid_video_Id', 'data-validation' => 'required']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Page Content:</label>
    {!! Form::textarea('content', $course->content, ['class'=>'form-control wysiwyg_editor', 'id' => 'content']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Introduction Video :</label>
    {!! Form::text('course_intro_video', $course->intro_video ?? old('intro_video'), ['class'=>'form-control', 'id' => 'course_intro_video', 'data-validation' => 'required']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Meta Title :</label>
    {!! Form::text('meta_title', old('meta_title'), ['class'=>'form-control', 'id' => 'meta_title']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Meta Description :</label>
    {!! Form::text('meta_description', old('meta_description'), ['class'=>'form-control', 'id' => 'meta_description']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Meta Keyword :</label>
    {!! Form::text('meta_keywords', old('meta_keywords'), ['class'=>'form-control', 'id' => 'meta_keywords']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Canonical URL :</label>
    {!! Form::text('canonical_url', old('canonical_url'), ['class'=>'form-control', 'id' => 'canonical_url']) !!}
</div>