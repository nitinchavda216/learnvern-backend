@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Languages</h2>
                <div id="data-container">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2 class="az-content-inner-section-title">Languages List</h2>
                            <table id="dataTableList" class="display responsive">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                @if($language)
                                    @foreach($language as $data)
                                        <tbody>
                                        <tr>
                                            <td>{{ $data->name }}</td>
                                            <td><a href="{{route('languages.edit',$data->id) }}"
                                                   class="btn btn-primary btn-xs d-inline">
                                                    <i class="fas fa-edit"></i>
                                                </a>

                                            </td>
                                        </tr>
                                        </tbody>
                                    @endforeach
                                @endif
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <h2 class="az-content-inner-section-title">Create Language</h2>
                            <div class="card card-body " id="CoursesInformation">
                                <form role="form" method="post" action="{{ route('languages.store') }}">
                                    @csrf
                                    <div class="row row-sm">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="az-content-label tx-11 tx-medium tx-gray-600">
                                                    Title</label>
                                                <input type="text" name="name" placeholder="Enter Language"
                                                       class="form-control" data-validation="required">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-az-primary mg-md-t-9">
                                        {{ isset($language) ? "Submit" : "Update" }}
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataTableList').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        });

    </script>
@endsection

