@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Create Language
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('languages') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div class="card card-body " id="CoursesInformation">
                    <form role="form" method="post" action="{{ route('languages.store') }}"  >
                        @csrf
                        <div class="row row-sm">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600"> Language Name</label>
                                    {{ Form::text('name', isset($language) ? $language->name : old('name') ,['class' => 'form-control', 'data-validation' => 'required'] ) }}
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-az-primary mg-md-t-9">{{ isset($language) ? "Update Language" : "Add Language" }}</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
