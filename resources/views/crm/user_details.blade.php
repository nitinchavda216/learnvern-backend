@extends('layout.master')
@section('content')
    <div class="az-content" style="min-height: 85vh;">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title"> User Campaign History</h2>
                <div class="row p-2">
                    <div class="col-12 text-right">
                        <button type="button" id="#assignCampaign" data-toggle="modal"
                                data-target="#assignCampaignModal" class="btn btn-primary">Assign Campaign
                        </button>
                    </div>
                </div>
                <div class="row p-2">
                    <div class="col-12">
                        <div class="card bd-0">
                            <div class="card-body bd">
                                <div class="row">
                                    <div class="col-3"><label
                                            class="az-content-label tx-11">Name: </label> {{$userDetail->first_name ?? '--'}}
                                    </div>
                                    <div class="col-3"><label
                                            class="az-content-label tx-11">Email: </label> {{$userDetail->email ?? '--'}}
                                    </div>
                                    <div class="col-3"><label class="az-content-label tx-11">Contact
                                            Number: </label> {{$userDetail->mobile_number ?? '--'}}</div>
                                    <div class="col-3"><label class="az-content-label tx-11">Referral
                                            Code: </label> {{$userDetail->own_referral_code ?? '--'}}</div>
                                    <div class="col-3"><label class="az-content-label tx-11">Referral Link: </label>
                                        https://www.learnvern.com/r/{{$userDetail->own_referral_code ?? '--'}}</div>
                                    <div class="col-3"><label class="az-content-label tx-11">Total Referral
                                            Count: </label> {{$userDetail->referred_count ?? '0'}}</div>
                                    <div class="col-3"><label class="az-content-label tx-11">Course Enrolled
                                            #: </label> {{count($courseList)}}</div>
                                    <div class="col-3"><label class="az-content-label tx-11">No Of Certificate
                                            Bought: </label> {{$buyCertificate ?? '0'}}</div>
                                    <div class="col-3"><label class="az-content-label tx-11">Affiliate
                                            Code: </label> {{$userDetail->referral ?? '--'}}</div>
                                    <div class="col-3"><label class="az-content-label tx-11">Last Login
                                            Date: </label> {{$userDetail->last_login_date ? formatDate($userDetail->last_login_date,'d-m-Y'): '--'}}
                                    </div>
                                </div>
                                <div class="az-content-label mg-b-5 mt-3">Course List</div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered mg-b-0">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Course Name</th>
                                                    <th>Percent(%)</th>
                                                    <th>Certificate Purchase</th>
                                                    <th>Payment Date</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($courseList)
                                                    @foreach($courseList as $key => $item)
                                                        <tr>
                                                            <th scope="row">{{$key+1}}</th>
                                                            <td>{{$item->courseDetail->name}}</td>
                                                            <td>{{ round($item->progress)}}</td>
                                                            <td>{{ $item->paid_status == 1 ? 'Yes':'No'}}</td>
                                                            <td>
                                                                {{$item->payment_completed_date ? formatDate($item->payment_completed_date,'d-m-Y') : '--'}}
                                                                @if($item->courseDetail->is_active == 1)
                                                                    @if($item->payment_completed_date == "" && env('RAZORPAY_ENABLE') && ($item->partner_id == 1) && ($item->courseDetail->course_type != 4))
                                                                        <a href="javascript:;"
                                                                           class="btn @if(isset($item->customPaymentRequest)) btn-primary @else btn-info @endif btn-xs sendPaymentLink"
                                                                           data-id="{{ $item->id }}">
                                                                            @if(isset($item->customPaymentRequest))
                                                                                Resend Payment Link @else Send Payment
                                                                            Link @endif
                                                                        </a>
                                                                    @endif
                                                                @else
                                                                    Inactive Course
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- card-body -->
                        </div>
                    </div>
                </div>

                <div id="data-container">
                    <div class="row">
                        <div class="col-12">
                            {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                            <div class="mb-3">
                                <div class="row">
                                    <div class="col-3">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Search:</label>
                                        <input name="search" id="search" type="text" class="form-control"
                                               placeholder="Search">
                                    </div>
                                    <div class="col-md-3">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">FollowUp
                                            Date</label>
                                        <input id="date-range" class="form-control" format="m-d-Y"
                                               placeholder="mm/dd/yyyy" name="date_range" type="date"
                                               value="{{$_GET['date_range'] ?? ''}}">
                                    </div>
                                    <div class="col-3 mt-4">
                                        <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                        <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">
                                            Reset
                                        </button>
                                    </div>

                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">


                            <table id="dataTableList" class="table">
                                <thead>
                                <tr role="row" class="bg-white">
                                    <th>Campaign Name</th>
                                    <th>Marketing Goal</th>
                                    <th>Assign To</th>
                                    <th>FollowUp Date</th>
                                    <th>Move To FollowUp</th>
                                    <th>Not Interest</th>
                                    <th>Other</th>
                                    <th>Notes</th>
                                    <th>Last Call Date</th>
                                    <th>First Call Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- moveToFollow Modal -->
    <div id="assignCampaignModal" class="modal">
        <div class="modal-dialog modal-lg" role="document">
            <form id="assignCampaignForm">
                <div class="modal-content modal-content-demo">
                    <div class="modal-header">
                        <h6 class="modal-title">Assign Campaign To User</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row row-sm mt-2">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Campaign: </label>
                                    <select id="campaign_id" name="campaign_id" class="form-control"
                                            data-validation="required">
                                        <option value="">--Select Campaign--</option>
                                        @foreach($campaignList as $item)
                                            <option value="{{$item->id}}"
                                                    data-goal="{{$item->marketing_goal}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row row-sm mt-2">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Course: </label>
                                    <select id="course_user_id" name="course_user_id" class="form-control"
                                            data-validation="required">
                                        <option value="">--Select Course--</option>
                                        @if($courseList)
                                            @foreach($courseList as $key => $item)
                                                <option value="{{$item->id}}">{{$item->courseDetail->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                        @if (getCurrentAdmin()->relatedRole->is_super_admin == 0 && getCurrentAdmin()->relatedRole->is_crm_manager == 0)

                            <input type="hidden" id="assign_to" name="assign_to" value="{{getCurrentAdmin()->id}}"/>

                        @else
                            <div class="row row-sm mt-2">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Assign To: </label>
                                        {!! Form::select('assign_to', $adminNames , '', ['class'=>'form-control', 'id' => 'assign_to', 'placeholder' => '', 'data-validation' => 'required']) !!}
                                    </div>
                                </div>
                            </div>
                        @endif

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div><!-- modal-dialog -->
    </div>


    <!-- moveToFollow Modal -->
    <div id="moveToFollowModal" class="modal">
        <div class="modal-dialog modal-lg" role="document">
            <form id="moveToFollowForm">
                <div class="modal-content modal-content-demo">
                    <div class="modal-header">
                        <h6 class="modal-title">Moving to Follow Up</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="az-content-label tx-11 tx-medium">Campaign Name: <span
                                                class="nameSection">-</span></label>
                                    </div>
                                    <div class="col-sm-6" style="display: none;">
                                        <label class="az-content-label tx-11 tx-medium">Contact No: <span
                                                class="numberSection">-</span></label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="az-content-label tx-11 tx-medium">MARKETING GOAL: <span
                                                class="emailSection">-</span></label>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row row-sm mt-2">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Next Follow Up
                                        Date: </label>
                                    <input class="form-control" type="date" data-validation="required"
                                           name="move_visit_date" id="move_visit_date">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Note: </label>
                                    <select class="form-control" id="move_notes" name="move_notes"
                                            data-validation="required">
                                        <option value="">- Select Option -</option>
                                        <option value="Will start referrals is a college student">Will start referrals
                                            is a college student
                                        </option>
                                        <option value="Will refer but not a college student">Will refer but not a
                                            college student
                                        </option>
                                        <option value="Interested in purchasing certificate">Interested in purchasing
                                            certificate
                                        </option>
                                        <option value="Interested in referring certificate">Interested in referring
                                            certificate
                                        </option>
                                    </select>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Move To Follow Up</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div><!-- modal-dialog -->
    </div>

    <!-- Not Interested Modal -->
    <div id="notInterestedModal" class="modal">
        <div class="modal-dialog modal-lg" role="document">
            <form id="notInterestedForm">
                <div class="modal-content modal-content-demo">
                    <div class="modal-header">
                        <h6 class="modal-title">Setting Not Interest
                        </h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="az-content-label tx-11 tx-medium">Campaign Name: <span
                                                class="nameSection">-</span></label>
                                    </div>
                                    <div class="col-sm-6" style="display: none;">
                                        <label class="az-content-label tx-11 tx-medium">Contact No: <span
                                                class="numberSection">-</span></label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="az-content-label tx-11 tx-medium">Marketing Goal: <span
                                                class="emailSection">-</span></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row row-sm mt-2">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Not Interest
                                        Reason: </label>
                                    <select class="form-control" id="not_interested_reason" name="not_interested_reason"
                                            data-validation="required">
                                        <option value="">- Select Reason -</option>
                                        <option value="Already walking with another num">Already walking with another
                                            num
                                        </option>
                                        <option value="Doing Job">Doing Job</option>
                                        <option value="Further Studies">Further Studies</option>
                                        <option value="Got Job">Got Job</option>
                                        <option value="High Fees">High Fees</option>
                                        <option value="Joined Other Place">Joined Other Place</option>
                                        <option value="Location Problem">Location Problem</option>
                                        <option value="Not interested in any training">Not interested in any training
                                        </option>
                                        <option value="Other">Other</option>
                                        <option value="Study Abroad">Study Abroad</option>
                                        <option value="Technologies not supporting">Technologies not supporting</option>
                                        <option value="Timing Issues">Timing Issues</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Set Not Interest</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div><!-- modal-dialog -->


    </div>




    <!-- Other Modal -->
    <div id="otherModal" class="modal">
        <div class="modal-dialog modal-lg" role="document">
            <form id="otherForm">
                <div class="modal-content modal-content-demo">
                    <div class="modal-header">
                        <h6 class="modal-title">Other
                        </h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="az-content-label tx-11 tx-medium">Campaign Name: <span
                                                class="nameSection">-</span></label>
                                    </div>
                                    <div class="col-sm-6" style="display: none;">
                                        <label class="az-content-label tx-11 tx-medium">Contact No: <span
                                                class="numberSection">-</span></label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="az-content-label tx-11 tx-medium">Marketing Goal: <span
                                                class="emailSection">-</span></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row row-sm mt-2">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Reason: </label>
                                    <select class="form-control" id="other_reason" name="other_reason"
                                            data-validation="required">
                                        <option value="">- Select Reason -</option>
                                        <option value="Ringing">Ringing</option>
                                        <option value="Reject">Reject</option>
                                        <option value="Switch Off">Switch Off</option>
                                        <option value="Invalid Number">Invalid Number</option>
                                        <option value="Recall">Recall</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div><!-- modal-dialog -->


    </div>




    <input type="hidden" id="current_select_calling_detail" name="current_select_calling_detail" value=""/>
@endsection

@section('css')
    <style>
        .tooltip .tooltip-inner {
            text-align: left;
        }

        .infoIcon {
            font-size: 16px;
            margin-top: 5px;
            margin-left: 10px;
        }

        div.dataTables_wrapper {
            max-width: 100%;
            margin: 0 auto;
        }

        .dataTable thead th {
            background-color: #fff;
            height: 40px;
        }

        .dataTables_scrollHeadInner table.dataTable {
            margin-bottom: 0px;
        }
    </style>

@endsection



@section('footer_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"
            integrity="sha512-eYSzo+20ajZMRsjxB6L7eyqo5kuXuS2+wEbbOkpaur+sA2shQameiJiWEzCIDwJqaB0a4a6tCuEvCOBHUg3Skg=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#moveToFollowForm").submit(function (e) {
                e.preventDefault();
                statusUpdate('move_to_followup', $('#move_visit_date').val(), $("#move_notes").val());
                $('#moveToFollowModal').modal('hide');
            });

            $("#otherForm").submit(function (e) {
                e.preventDefault();
                statusUpdate('other', '', $("#other_reason").val());
                $('#otherModal').modal('hide');
            });

            $("#notInterestedForm").submit(function (e) {
                e.preventDefault();
                statusUpdate('not_interest', '', $("#not_interested_reason").val());
                $('#notInterestedModal').modal('hide');
            });

            function statusUpdate(val, date, notes) {
                var id = $("#current_select_calling_detail").val();
                $.ajax({
                    type: "POST",
                    data: {id: id, action: val, followup_date: date, notes: notes},
                    url: $app_url + '/crm/save-calling-detail',
                    beforeSend: function () {
                        $.blockUI();
                    },
                    success: function (result) {
                        if (result.status) {
                            $dataTableList.draw('page');
                            $.unblockUI();
                        }
                    },
                    error: function (data) {
                        console.log("Cancelled - error");
                        $.unblockUI();
                    }
                });
            }

            $(document).on('click', '.actionPerform', function () {
                var selectedValue = $(this).val();
                var id = $(this).attr('name').split('calling_action_')[1];
                $("#current_select_calling_detail").val(id);
                var firstTdData = $(this).parent().parent().parent().parent();
                var name = firstTdData.find('td:eq(0)').text();
                var email = firstTdData.find('td:eq(1)').text();
                var number = firstTdData.find('td:eq(2)').text();
                $(".nameSection").html(name);
                $(".numberSection").html(number);
                $(".emailSection").html(email);

                if (selectedValue == "move_to_followup") {
                    $("#moveToFollowForm")[0].reset();
                    $('#moveToFollowModal').modal('show');
                } else if (selectedValue == "other") {
                    $("#otherForm")[0].reset();
                    $('#otherModal').modal('show');
                } else if (selectedValue == "not_interest") {
                    $("#notInterestedForm")[0].reset();
                    $('#notInterestedModal').modal('show');
                } else {

                    bootbox.confirm({
                        message: "Are you sure you want to perform this action ?",
                        buttons: {
                            confirm: {
                                label: "Yes",
                                className: 'btn-success'
                            },
                            cancel: {
                                label: "No",
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if (result === true) {
                                statusUpdate(selectedValue, '', '');
                            }
                        }
                    });
                }


                return false;

            });

            $(document).on('click', '.sendPaymentLink', function () {
                var self = $(this);
                var id = self.data('id');
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: $app_url + '/send-payment-link',
                    data: {'course_user_id': id},
                    beforeSend: function () {
                        $.blockUI({
                            message: '<h1>Please wait..</h1>',
                            timeout: 2000
                        });
                    },
                    success: function (data) {
                        if (data.status) {
                            self.addClass('btn-primary').removeClass('btn-info');
                            self.text('Resend Payment Link');
                            toastr.success(data.message, "Success!", {timeOut: 2000});
                        } else {
                            toastr.error(data.message, "Error!", {timeOut: 2000});
                        }
                        $.unblockUI();
                    }
                });
            })


            if ($('#dataTableList').length > 0) {
                var $dataTableList = $('#dataTableList').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    bFilter: false,
//                "scrollX": true,
                    "aaSorting": [],

                    ajax: {
                        url: $app_url + '/crm/getAjaxCampaignUserDetail',
                        data: function (d) {
                            d.userId = {{$userDetail->id}}
                                d.search = $("#search").val();
                            d.date_range = $("#date-range").val();
                        }
                    },
                    "columns": [
                        {data: 'name', 'sortable': false},
                        {data: 'marketing_goal', 'sortable': false},
                        {data: 'assign_to_name', 'sortable': false},
                        {data: 'followup_date', 'sortable': false},
                        {data: 'move_to_followup', 'sortable': false},
                        {data: 'not_interest', 'sortable': false},
                        {data: 'other', 'sortable': false},
                        {data: 'notes', 'sortable': false},
                        {data: 'last_calling_date', 'sortable': false},
                        {data: 'first_call_date', 'sortable': false},
                    ],
                    select: true,
                    bStateSave: true,
                    fnStateSave: function (settings, data) {
                        localStorage.setItem("dataTables_state", JSON.stringify(data));
                    },
                    fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
//                        if(aData['calling_action']){
//                            $('td', nRow).css('background-color', '#a2e29a');
//                        }
                    },
                    fnStateLoad: function (settings) {
                        return JSON.parse(localStorage.getItem("dataTables_state"));
                    },
                    drawCallback: function (settings) {
//                    $('body').tooltip({selector: '[data-toggle="tooltip"]',html: true});
                    }
                });


                $('#search-form').on('submit', function (e) {
                    $dataTableList.draw();
                    e.preventDefault();
                });

                $('#reset-filters').on('click', function () {
                    $("#search").val('');
                    $("#date-range").val('');
                    $dataTableList.draw();
                });

            }
        });

    </script>
@endsection
