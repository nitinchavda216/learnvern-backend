@extends('layout.master')
@section('content')
    <div class="az-content" style="min-height: 85vh;">
        <div class="container-fluid">
            <div class="az-content-body" style="overflow-x: auto" >
                <h2 class="az-content-title"> Pending FollowUp
                    {{--<span class="pull-right d-inline-block float-right">--}}
                        {{--<a href="javascipt:void(0);" onclick="history.go(-1)" class="btn btn-warning btn-sm mt-negative" title="Back">--}}
                            {{--<i class="fa fa-arrow-left" aria-hidden="true"></i> Back--}}
                        {{--</a>--}}
                    {{--</span>--}}
                </h2>

                <div id="data-container">
                    <div class="row">
                        <div class="col-12">
                            {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                            <div class="mb-3">
                                <div class="row">
                                    <div class="col-3">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Search:</label>
                                        <input name="search" id="search" type="text" class="form-control" placeholder="Search">
                                    </div>
                                    <div class="col-md-3">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">FollowUp Date</label>
                                        <input name="date_range" id="date-range" type="text" class="form-control"
                                               autocomplete="off" placeholder="Select Date" value="{{request('date_range')}}">

                                    </div>
                                    <div class="col-3 mt-4">
                                        <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                        <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">


                            <table id="dataTableList" class="table">
                                <thead>
                                    <tr role="row" class="bg-white">
                                        <th>User Name</th>
                                        <th>Email</th>
                                        <th>Contact No.</th>
                                        <th>Referral Code</th>
                                        <th>Referral Code Link</th>
                                        <th>Affiliate Code</th>
                                        <th>Last Login Date</th>
                                        <th>Campaign Name</th>
                                        <th>Marketing Goal</th>
                                        <th>FollowUp Date</th>
                                        <th>Courses Name</th>
                                        <th>No Of Course Enroll</th>
                                        <th>Certificate bought</th>
                                        <th>Referral Count</th>
                                        <th>Move To FollowUp</th>
                                        <th>Not Interest</th>
                                        <th>Other</th>
                                        <th>Notes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- moveToFollow Modal -->
    <div id="moveToFollowModal" class="modal">
        <div class="modal-dialog modal-lg" role="document">
            <form id="moveToFollowForm">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Moving to Follow Up</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Name: <span class="nameSection">-</span></label>
                                </div>
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Contact No: <span class="numberSection">-</span></label>
                                </div>
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Email: <span class="emailSection">-</span></label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row row-sm mt-2">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Next Follow Up Date: </label>
                                <input class="form-control" type="date" data-validation="required" name="move_visit_date" id="move_visit_date">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Note: </label>
                                <select class="form-control" id="move_notes" name="move_notes" data-validation="required">
                                    <option value="">- Select Option -</option>
                                    <option value="Will start referrals is a college student">Will start referrals is a college student</option>
                                    <option value="Will refer but not a college student">Will refer but not a college student</option>
                                    <option value="Interested in purchasing certificate">Interested in purchasing certificate</option>
                                    <option value="Interested in referring certificate">Interested in referring certificate</option>
                                </select>
                            </div>

                        </div>

                    </div>
                </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Move To Follow Up</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div><!-- modal-dialog -->


    </div>

    <!-- Not Interested Modal -->
    <div id="notInterestedModal" class="modal">
        <div class="modal-dialog modal-lg" role="document">
            <form id="notInterestedForm">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Setting Not Interest
                    </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Name: <span class="nameSection">-</span></label>
                                </div>
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Contact No: <span class="numberSection">-</span></label>
                                </div>
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Email: <span class="emailSection">-</span></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row row-sm mt-2">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Not Interest Reason: </label>
                                <select class="form-control" id="not_interested_reason" name="not_interested_reason" data-validation="required">
                                    <option value="">- Select Reason -</option>
                                    <option value="Already walking with another num">Already walking with another num</option>
                                    <option value="Doing Job">Doing Job</option>
                                    <option value="Further Studies">Further Studies</option>
                                    <option value="Got Job">Got Job</option>
                                    <option value="High Fees">High Fees</option>
                                    <option value="Joined Other Place">Joined Other Place</option>
                                    <option value="Location Problem">Location Problem</option>
                                    <option value="Not interested in any training">Not interested in any training</option>
                                    <option value="Other">Other</option>
                                    <option value="Study Abroad">Study Abroad</option>
                                    <option value="Technologies not supporting">Technologies not supporting</option>
                                    <option value="Timing Issues">Timing Issues</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Set Not Interest</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
            </form>
        </div><!-- modal-dialog -->


    </div>




    <!-- Other Modal -->
    <div id="otherModal" class="modal">
        <div class="modal-dialog modal-lg" role="document">
            <form id="otherForm">
                <div class="modal-content modal-content-demo">
                    <div class="modal-header">
                        <h6 class="modal-title">Other
                        </h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="az-content-label tx-11 tx-medium">Name: <span class="nameSection">-</span></label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="az-content-label tx-11 tx-medium">Contact No: <span class="numberSection">-</span></label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="az-content-label tx-11 tx-medium">Email: <span class="emailSection">-</span></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row row-sm mt-2">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Reason: </label>
                                    <select class="form-control" id="other_reason" name="other_reason" data-validation="required">
                                        <option value="">- Select Reason -</option>
                                        <option value="Ringing">Ringing</option>
                                        <option value="Reject">Reject</option>
                                        <option value="Switch Off">Switch Off</option>
                                        <option value="Invalid Number">Invalid Number</option>
                                        <option value="Recall">Recall</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div><!-- modal-dialog -->


    </div>




    <input type="hidden" id="current_select_calling_detail" name="current_select_calling_detail" value=""/>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <style>
        .tooltip .tooltip-inner {
            text-align:left;
        }
        .infoIcon {
            font-size: 16px;
            margin-top: 5px;
            margin-left: 10px;
        }

        div.dataTables_wrapper {
            max-width: 100%;
            margin: 0 auto;
        }
        .dataTable thead th{
            background-color: #fff;
            height: 40px;
        }
        .dataTables_scrollHeadInner table.dataTable{
            margin-bottom: 0px;
        }
    </style>

@endsection



@section('footer_scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js" integrity="sha512-eYSzo+20ajZMRsjxB6L7eyqo5kuXuS2+wEbbOkpaur+sA2shQameiJiWEzCIDwJqaB0a4a6tCuEvCOBHUg3Skg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#date-range').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY',
                },
                ranges: {
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });

            $('#date-range').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            });

            $('#date-range').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });


            $("#moveToFollowForm").submit(function(e) {
                e.preventDefault();
                statusUpdate('move_to_followup',$('#move_visit_date').val(),$("#move_notes").val());
                $('#moveToFollowModal').modal('hide');
            });

            $("#otherForm").submit(function(e) {
                e.preventDefault();
                statusUpdate('other','',$("#other_reason").val());
                $('#otherModal').modal('hide');
            });

            $("#notInterestedForm").submit(function(e) {
                e.preventDefault();
                statusUpdate('not_interest','',$("#not_interested_reason").val());
                $('#notInterestedModal').modal('hide');
            });



            function statusUpdate(val,date,notes){
                var id = $("#current_select_calling_detail").val();
                $.ajax({
                    type:"POST",
                    data:{ id: id,action:val,followup_date:date,notes:notes},
                    url: $app_url+'/crm/save-calling-detail',
                    beforeSend: function () {
                        $.blockUI();
                    },
                    success:function(result)
                    {
                        if(result.status){
                            $dataTableList.draw('page');
                            $.unblockUI();
                        }
                    },
                    error: function (data) {
                        console.log("Cancelled - error");
                        $.unblockUI();
                    }
                });
            }

            $(document).on('click','.actionPerform',function () {
                var selectedValue = $(this).val();
                var id = $(this).attr('name').split('calling_action_')[1];
                $("#current_select_calling_detail").val(id);
                var firstTdData = $(this).parent().parent().parent().parent();
                var name = firstTdData.find('td:eq(0)').text();
                var email = firstTdData.find('td:eq(1)').text();
                var number = firstTdData.find('td:eq(2)').text();
                $(".nameSection").html(name);
                $(".numberSection").html(number);
                $(".emailSection").html(email);

                if(selectedValue == "move_to_followup"){
                    $("#moveToFollowForm")[0].reset();
                    $('#moveToFollowModal').modal('show');
                }else if(selectedValue == "other"){
                    $("#otherForm")[0].reset();
                    $('#otherModal').modal('show');
                }else if(selectedValue == "not_interest"){
                    $("#notInterestedForm")[0].reset();
                    $('#notInterestedModal').modal('show');
                }else{

                    bootbox.confirm({
                        message: "Are you sure you want to perform this action ?",
                        buttons: {
                            confirm: {
                                label: "Yes",
                                className: 'btn-success'
                            },
                            cancel: {
                                label: "No",
                                className: 'btn-danger'
                            }
                        },
                        callback: function (result) {
                            if (result === true) {
                                statusUpdate(selectedValue,'','');
                            }
                        }
                    });
                }


                return false;

            });



            var $dataTableList = $('#dataTableList').DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                bFilter: false,
                "scrollX": true,
                "aaSorting": [],

                ajax: {
                    url: $app_url + '/crm/getPendingAjaxCallingData',
                    data: function (d) {
                        d.search=$("#search").val();
                        d.date_range=$("#date-range").val();
                        d.userId='{{request('user')}}';
                    }
                },
                "columns": [
                    {data: 'first_name', 'sortable': false},
                    {data: 'email','sortable': false},
                    {data: 'mobile_number','sortable': false},
                    {data: 'own_referral_code', 'sortable': false},
                    {data: 'own_referral_link', 'sortable': false},
                    {data: 'referral', 'sortable': false},
                    {data: 'last_login_date', 'sortable': false},
                    {data: 'name','sortable': false},
                    {data: 'marketing_goal','sortable': false},
                    {data: 'followup_date','sortable': false},
                    {data: 'courses_selected', 'sortable': false,},
                    {data: 'number_of_course_enroll', 'sortable': false,},
                    {data: 'total_bought_certificate', 'sortable': false},
                    {data: 'referred_count', 'sortable': false},
                    {data: 'move_to_followup','sortable': false},
                    {data: 'not_interest','sortable': false},
                    {data: 'other','sortable': false},
                    {data: 'notes','sortable': false},
                ],
            });


            $('#search-form').on('submit', function (e) {
                $dataTableList.draw();
                e.preventDefault();
            });

            $('#reset-filters').on('click', function () {
                $("#search").val('');
                $("#date-range").val('');
                $dataTableList.draw();
            });
        });

    </script>
@endsection


