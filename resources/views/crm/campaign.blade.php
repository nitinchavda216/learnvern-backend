@extends('layout.master')
@section('content')

    <div class="az-content" style="min-height: 85vh;">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title">Calling Campaign Report
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('crm.dashboard') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>

                <div id="data-container">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-3">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Search</label>
                                <input name="search" id="search-form-field" type="text" class="form-control"
                                       placeholder="Search">
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Created Date
                                        :</label>
                                    <input name="date_range" id="date_range" type="text" class="form-control"
                                           autocomplete="off" placeholder="Select Date">
                                </div>
                            </div>
                            <div class="col-md mg-t-10 mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">
                                    Reset
                                </button>
                            </div>
                        </div>

                    </div>
                    {!! Form::close() !!}

                    <div class="row">
                        <div class="col-md-12">
                            <table id="dataTableList" class="display responsive">
                                <thead>
                                <tr>
                                    <th>Campaign Name</th>
                                    <th>Marketing Goal</th>
                                    <th>Action</th>
                                    <th>Created By</th>
                                    <th>Assigned To</th>
                                    <th>Created Date</th>
                                    <th>No Of User Assign</th>
                                    <th>Move To FollowUp</th>
                                    <th>Not Interested</th>
                                    <th>Other</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
    <style>
        .previous-link {
            width: 70px;
        }

        .next-link {
            width: 50px;
        }
        .select2-container {
            width: 100% !important;
        }
        div.dataTables_wrapper {
            max-width: 100%;
            margin: 0 auto;
        }
        .dataTable thead th{
            background-color: #fff;
            height: 40px;
        }
        .dataTables_scrollHeadInner table.dataTable{
            margin-bottom: 0px;
        }
    </style>
@endsection

@section('footer_scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#date_range').daterangepicker({
                autoUpdateInput: false,
                locale:{
                    cancelLabel: 'Clear'
                },
                ranges:{
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            $('#date_range').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            });
            $('#date_range').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });


            var $dataTableList = $('#dataTableList').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: false,
                    bFilter: false,
                    "aaSorting": [],
                    "scrollX": true,
                    ajax: {
                        url: $app_url + '/crm/getCampaignReportForCrm',
                        data: function (d) {
                            d.date_range = $('#date_range').val();
                            d.search = $('#search-form-field').val();
                        }
                    },
                    "columns": [
                        {data: 'name', 'sortable': false,width:'10%'},
                        {data: 'marketing_goal', 'sortable': false,width:'10%'},
                        {data: 'action', 'sortable': false,width:'10%'},
                        {data: 'created_by_name', 'sortable': false,width:'10%'},
                        {data: 'assign_to_user_name', 'sortable': false,width:'10%'},
                        {data: 'created_at', 'sortable': false,width:'5%'},
                        {data: 'count_user', 'sortable': false,width:'5%'},
                        {data: 'move_to_followup_count', 'sortable': false,width:'10%'},
                        {data: 'not_interest_count', 'sortable': false,width:'5%'},
                        {data: 'other_count', 'sortable': false,width:'5%'},
                        {data: 'actions_menu', 'orderable': false,width:'20%'},
                    ],
                    select: true,
                    bStateSave: true,
                    fnStateSave: function (settings, data) {
                        localStorage.setItem("dataTables_state", JSON.stringify(data));
                    },
                    fnStateLoad: function (settings) {
                        return JSON.parse(localStorage.getItem("dataTables_state"));
                    },
                    "initComplete": function( settings ) {
                        $('.dataTables_scrollBody thead tr').css({visibility:'collapse'});
                        $('.dataTables_scrollHeadInner thead tr:first th:first').removeClass('sorting_asc');
                    },
                    "drawCallback": function( settings ) {
                        $('.dataTables_scrollBody thead tr').css({visibility:'collapse'});
                        $('.dataTables_scrollHeadInner thead tr:first th:first').removeClass('sorting_asc');
                    }
                });

                $('#search-form').on('submit', function (e) {
                    $dataTableList.draw();
                    e.preventDefault();
                });

                $('#reset-filters').on('click', function () {
                    $("#search-form-field").val(null);
                    $("#date_range").val(null);
                    $dataTableList.draw();
                });

        });
    </script>
@endsection
