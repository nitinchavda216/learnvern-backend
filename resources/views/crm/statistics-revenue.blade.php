@extends('layout.master')
@section('content')
    <div class="az-content" style="min-height: 85vh;">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title"> FollowUp Statistics For Revenue
                    {{--<span class="pull-right d-inline-block float-right">--}}
                        {{--<a href="{{ route('crm.dashboard') }}" class="btn btn-warning btn-sm mt-negative" title="Back">--}}
                            {{--<i class="fa fa-arrow-left" aria-hidden="true"></i> Back--}}
                        {{--</a>--}}
                    {{--</span>--}}
                </h2>

                <div id="data-container">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-3">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Search</label>
                                <input name="search" id="search-form-field" type="text" class="form-control"
                                       placeholder="Search">
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Follow Up Date
                                        :</label>
                                    <input name="date_range" id="date_range" type="text" class="form-control"
                                           autocomplete="off" placeholder="Select Date" value="{{request('date_range')}}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Type:</label>
                                <select class="form-control" id="type" name="type">
                                    <option></option>
                                    <option value="active" {{ (isset($_GET['type']) && $_GET['type']=="active")?'selected':''}}>Active</option>
                                    <option value="passive" {{ (isset($_GET['type']) && $_GET['type']=="passive")?'selected':''}}>Passive</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Payment Completed Status</label>
                                <select class="form-control" id="payment_status" name="payment_status">
                                    <option value="">Both</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Payment Completed Date:</label>
                                    <input name="payment_date_range" id="payment_date_range" type="text" class="form-control" autocomplete="off" placeholder="Select Date" value="{{request('payment_date_range')}}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Last Call Date:</label>
                                    <input name="last_date_range" id="last_date_range" type="text" class="form-control" autocomplete="off" placeholder="Select Date" value="{{request('last_date_range')}}">
                                </div>
                            </div>
                            <div class="col-md mg-t-10 mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">
                                    Reset
                                </button>
                            </div>
                        </div>

                    </div>
                    {!! Form::close() !!}


                            <table id="dataTableList" class="display nowrap" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
                                        <th>Referral Code</th>
                                        <th>Referral Code Link</th>
                                        <th>Affiliate Code</th>
                                        <th>Last Login Date</th>
                                        <th>Campaign Name</th>
                                        <th>Assign To</th>
                                        <th>Course Name</th>
                                        <th>Payment Done</th>
                                        <th>Payment Completed Date</th>
                                        <th>Last Calling Date</th>
                                        <th>Follow UP Date</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        .previous-link {
            width: 70px;
        }

        .next-link {
            width: 50px;
        }
        .select2-container {
            width: 100% !important;
        }
        div.dataTables_wrapper {
            width: 1800px;
            margin: 0 auto;
        }
        .dataTable thead th{
            background-color: #fff;
            height: 40px;
        }
        .dataTables_scrollHeadInner table.dataTable{
            margin-bottom: 0px;
        }
        @media only screen  and (min-width : 1224px) {
            div.dataTables_wrapper {
                width: 1200px;
                margin: 0 auto;
            }
        }

        /* Large screens ----------- */
        @media only screen  and (min-width : 1824px) {
            div.dataTables_wrapper {
                width: 1800px;
                margin: 0 auto;
            }
        }
    </style>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#date_range').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY',
                },
                ranges: {
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            $('#date_range').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            });
            $('#date_range').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });


            $('#payment_date_range').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY',
                },
                ranges: {
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            $('#payment_date_range').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            });
            $('#payment_date_range').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });


            $('#last_date_range').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY',
                },
                ranges: {
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            $('#last_date_range').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            });
            $('#last_date_range').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });


            var columnDefination = [
                        {data: 'first_name', 'sortable': false},
                        {data: 'mobile_number', 'sortable': false},
                        {data: 'email', 'sortable': false},
                        {data: 'own_referral_code', 'sortable': false},
                        {data: 'own_referral_link', 'sortable': false},
                        {data: 'referral', 'sortable': false},
                        {data: 'last_login_date', 'sortable': false},
                        {data: 'campaign_name', 'sortable': false},
                        {data: 'assign_to_name', 'sortable': false},
                        {data: 'course_name', 'sortable': false},
                        {data: 'paid_status', 'sortable': true},
                        {data: 'payment_completed_date', 'sortable': true},
                        {data: 'last_calling_date', 'sortable': true},
                        {data: 'followup_date', 'sortable': true},
                    ];

                var $dataTableList = $('#dataTableList').DataTable({
                        processing: true,
                        serverSide: true,
                        bFilter: false,
                        "aaSorting": [],
                        "scrollX": true,
                        ajax: {
                            url: $app_url + '/crm/getFollowUpStatisticsRevenue',
                            data: function (d) {
                                d.date_range = $('#date_range').val();
                                d.payment_date_range = $('#payment_date_range').val();
                                d.last_date_range = $('#last_date_range').val();
                                d.payment_status = $('#payment_status').val();
                                d.search = $('#search-form-field').val();
                                d.type= $("#type").val();
                                d.campaign='{{$campaignId??''}}';
                                d.userId='{{$userId??''}}';
                            }
                        },
                        "columns": columnDefination,
                        "initComplete": function( settings ) {
                            $('.dataTables_scrollBody thead tr').css({visibility:'collapse'});
                            $('.dataTables_scrollHeadInner thead tr:first th:first').removeClass('sorting_asc');
                        },
                        "drawCallback": function( settings ) {
                            $('.dataTables_scrollBody thead tr').css({visibility:'collapse'});
                            $('.dataTables_scrollHeadInner thead tr:first th:first').removeClass('sorting_asc');
                        },
                    });

                    $('#search-form').on('submit', function (e) {
                        $dataTableList.draw();
                        e.preventDefault();
                    });

                    $('#reset-filters').on('click', function () {
                        $("#search-form-field").val(null);
                        $("#date_range").val(null);
                        $("#payment_date_range").val(null);
                        $("#last_date_range").val(null);
                        $("#type").val(null).trigger('change');
                        $("#payment_status").val(null).trigger('change');
                        $dataTableList.draw();
                    });

        });
    </script>
@endsection
