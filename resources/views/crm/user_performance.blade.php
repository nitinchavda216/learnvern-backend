@extends('layout.master')
@section('content')

    <div class="az-content" style="min-height: 85vh;">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title">Report Of {{ $userDetail['name'] }}</h2>
                <div id="data-container">
                    {!! Form::open(['method' => 'GET', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Filter By Date</label>
                                <input name="date_range" id="date_range" type="text" class="form-control"
                                       autocomplete="off" placeholder="Select Date" value="{{request('date_range')}}">
                            </div>
                            <div class="col-md-2 mg-t-10 mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Filter</button>
                                <a href="{{route('crm.userPerformance',$userDetail['id'])}}" class="btn btn-az-warning btn-warning">
                                    Reset
                                </a>
                            </div>
                            <div class="col-md-2 mg-t-20 mg-md-t-0">
                                <div class="card bd-0">
                                    <div class="card-header tx-medium bd-0 tx-black bg-gray-300 text-center">
                                        Today Pending Follow Up Not Done
                                    </div><!-- card-header -->
                                    <div class="card-body bd bd-t-0">

                                        <div class="row">
                                            <div class="col-md-6 text-center border-right">
                                                <h6>Referral</h6>
                                                <h3>
                                                    <a href="{{route('crm.followUpStatisticsReferral',['type'=>'active','user'=>$userDetail['id'],'date_range'=>date('d-m-Y').' - '.date('d-m-Y')])}}" class="text-success">{{$userStatisticsResult['dayPendingNotDoneResult']['campaign_pending_call_on_day_for_referral']}}</a>
                                                </h3>
                                            </div>
                                            <div class="col-md-6 text-center">
                                                <h6>Revenue</h6>
                                                <h3>
                                                    <a href="{{route('crm.followUpStatisticsRevenue',['type'=>'active','user'=>$userDetail['id'],'date_range'=>date('d-m-Y').' - '.date('d-m-Y')])}}" class="text-success">{{$userStatisticsResult['dayPendingNotDoneResult']['campaign_pending_call_on_day_for_revenue']}}</a>
                                                </h3>
                                            </div>
                                        </div>

                                    </div>

                                </div><!-- card -->
                            </div>
                            <div class="col-md-2 mg-t-20 mg-md-t-0">
                                <div class="card bd-0">
                                    <div class="card-header tx-medium bd-0 tx-black bg-gray-300 text-center">
                                        Today Pending Campaign Call
                                    </div><!-- card-header -->
                                    <div class="card-body bd bd-t-0">
                                        <h1 class="text-center text-danger"> {{$userStatisticsResult['dayCampaignPendingResult']}}</h1>
                                    </div><!-- card-body -->
                                </div><!-- card -->
                            </div>
                            <div class="col-md-4 mg-t-20 mg-md-t-0">
                                <div class="card bd-0">
                                    <div class="card-header tx-medium bd-0 tx-black bg-gray-300 text-center">
                                        Referral Statistics
                                    </div><!-- card-header -->
                                    <div class="card-body bd bd-t-0">
                                        <div class="row">
                                            <div class="col-md-4 text-center border-right">
                                                <h6>Ambassador</h6>
                                                <h3>
                                                    {{$userStatisticsResult['ambassodorStatResult']['ambassador']}}
                                                </h3>
                                            </div>
                                            <div class="col-md-4 text-center border-right">
                                                <h6>Ambassador +</h6>
                                                <h3>
                                                    {{$userStatisticsResult['ambassodorStatResult']['ambassador_plus']}}
                                                </h3>
                                            </div>
                                            <div class="col-md-4 text-center">
                                                <h6>Ambassador Pro</h6>
                                                <h3>
                                                    {{$userStatisticsResult['ambassodorStatResult']['ambassador_pro']}}
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- card -->
                            </div>
                        </div>

                    </div>
                    {!! Form::close() !!}


                    <div class="row mt-5 mb-2">
                        <div class="col-md-3 mg-t-20 mg-md-t-0">
                            <div class="card bd-0">
                                <div class="card-header tx-medium bd-0 tx-black bg-gray-300 text-center">
                                    Total Calls
                                   {{--<a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Included Followup,Not Interested"><i class="fa fa-info-circle" aria-hidden="true"></i></a>--}}
                                </div><!-- card-header -->
                                <div class="card-body bd bd-t-0">
                                    <div class="row">
                                        <div class="col-md-6 text-center border-right">
                                            <h6>FollowUp Calls</h6>
                                            <h3>
                                                {{$userStatisticsResult['callingDayStatusResult']['moveToFollowUpCalls']}}
                                            </h3>
                                        </div>
                                        <div class="col-md-6 text-center">
                                            <h6>Not Interested</h6>
                                            <h3>
                                                {{$userStatisticsResult['callingDayStatusResult']['notInterestedCalls']}}
                                            </h3>
                                        </div>
                                    </div>
                                </div><!-- card-body -->
                            </div><!-- card -->
                        </div>

                        <div class="col-md-3 mg-t-20 mg-md-t-0">
                            <div class="card bd-0">
                                <div class="card-header tx-medium bd-0 tx-black bg-gray-300 text-center">
                                    FollowUp Calls
                                </div><!-- card-header -->
                                <div class="card-body bd bd-t-0">
                                    <div class="row">
                                        <div class="col-md-6 text-center border-right">
                                            <h6>Fresh FollowUp Calls</h6>
                                            <h3>
                                                {{$userStatisticsResult['totalFollowUp']['freshFollowUpCall']}}
                                            </h3>
                                        </div>
                                        <div class="col-md-6 text-center">
                                            <h6>Pending FollowUp Calls</h6>
                                            <h3>
                                                {{$userStatisticsResult['totalFollowUp']['pendingFollowUpCall']}}
                                            </h3>
                                        </div>
                                    </div>
                                </div><!-- card-body -->
                            </div><!-- card -->
                        </div>

                        <div class="col-md-3 mg-t-20 mg-md-t-0">
                            <div class="card bd-0">
                                <div class="card-header tx-medium bd-0 tx-black bg-gray-300 text-center">
                                    Total Leads
                                </div><!-- card-header -->
                                <div class="card-body bd bd-t-0">

                                    <div class="row">
                                        <div class="col-md-6 text-center border-right">
                                            <h6>Referral</h6>
                                            @php
                                                $dateRange = $_GET['date_range'] ?? '';

                                            @endphp
                                            <h3>
                                                <a href="{{route('crm.followUpStatisticsReferral',['type'=>'active','user'=>$userDetail['id'],'filter_date_range'=>$dateRange])}}" class="text-success">{{$userStatisticsResult['referral_active_followup']}}</a>
                                            </h3>
                                        </div>
                                        <div class="col-md-6 text-center">
                                            <h6>Revenue</h6>
                                            <h3>
                                                <a href="{{route('crm.followUpStatisticsRevenue',['type'=>'active','user'=>$userDetail['id'],'payment_date_range'=>$dateRange])}}" class="text-success">{{$userStatisticsResult['revenue_active_followup']}}</a>
                                            </h3>
                                        </div>
                                    </div>

                                </div><!-- card-body -->
                            </div><!-- card -->
                        </div>

                        <div class="col-md-3 mg-t-20 mg-md-t-0">
                            <div class="card bd-0">
                                <div class="card-header tx-medium bd-0 tx-black bg-gray-300 text-center">
                                    Total Others
                                </div><!-- card-header -->
                                <div class="card-body bd bd-t-0">
                                    <h1 class="text-center"> {{$userStatisticsResult['callingDayStatusResult']['otherCalls']}}</h1>
                                </div><!-- card-body -->
                            </div><!-- card -->
                        </div>
                    </div>
                    @if(isset($_GET['date_range']) && $_GET['date_range'])
                    <div class="row">
                        <div class="col-md-12 mg-t-20 mg-md-t-0">
                            <div class="card bd-0">
                                <div class="card-header tx-medium bd-0 tx-black bg-gray-300">
                                    <h5 class="bg-gray-300" style="margin: 0px;"> Report: {{ request('date_range')}}</h5>
                                </div><!-- card-header -->
                                <div class="card-body bd bd-t-0">
                                    @if($userStatisticsResult['entryExistsOrNot'])
                                    <div class="table-responsive" id="slotTableList">
                                        <table class="table table-hover mg-b-0">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>Time Slot</th>
                                                <th>Total Calls</th>
                                                <th>Campaign Calls</th>
                                                <th>Followup Calls</th>
                                                <th>Other Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php
                                                $timeSlotArray = ["before_10" => "Before 10:00 AM","10-12" =>"10:00 AM - 12:00 AM","12-14"=>"12:00 AM - 02:00 PM","14-16"=>"02:00 PM - 04:00 PM","16-18"=>"04:00 PM - 06:00 PM","18-20"=>"06:00 PM - 08:00 PM","after_8"=>"After 08:00 PM"];
                                            @endphp

                                            @foreach($userStatisticsResult['timeStatistics'] as $timeSlot => $value)
                                                @if(($timeSlot == "before_10" ||  $timeSlot == "after_8") AND $value['count'] == 0)
                                                    @continue
                                                @endif
                                                <tr scope="row">
                                                    <td class="{{ $value['count'] >0 ?'childAvailable':''}}"></td>
                                                    <td>{{$timeSlotArray[$timeSlot]}}</td>
                                                    <td>{{$value['totalCalls']}}</td>
                                                    <td>{{$value['campaignCalls']}}</td>
                                                    <td>{{$value['followUpCalls']}}</td>
                                                    <td>{{$value['otherStatus']}}</td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td colspan="6">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="table-responsive">
                                                                    <table class="table table-bordered mg-b-0">
                                                                        <thead>
                                                                        <tr>
                                                                            <th></th>
                                                                            <th>Name</th>
                                                                            <th>Mobile</th>
                                                                            <th>Email</th>
                                                                            <th>Status</th>
                                                                            <th>Call Type</th>
                                                                            <th>Campaign Name</th>
                                                                            <th>Marketing Goal</th>
                                                                            <th>Date</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        @foreach($value['details'] as $i => $item)
                                                                            <tr>
                                                                                <th scope="row">{{$i+1}}</th>
                                                                                <td>{{$item['first_name']}}</td>
                                                                                <td>{{$item['mobile_number']}}</td>
                                                                                <td>{{$item['email']}}</td>
                                                                                <td>{{\App\Models\Campaign::SHOWING_ACTIONS[$item['calling_action']]}}</td>
                                                                                <td>{{$item['parent_id'] ? 'FollowUp':'Campaign' }}</td>
                                                                                <td>{{$item['name']}}</td>
                                                                                <td>{{$item['marketing_goal']}}</td>
                                                                                <td>{{formatDate($item['updated_at'],'d-m-Y')}}</td>

                                                                            </tr>
                                                                        @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                    @else
                                    <p class="text-center">No Record Found</p>
                                    @endif
                                </div><!-- card-body -->
                            </div><!-- card -->
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <style>

       td.childAvailable{
            background: url('{{ asset('admin/img/details_open.png')}}') no-repeat center center;
            cursor: pointer;
        }

        tr.shown td.childAvailable{
            background: url('{{ asset('admin/img/details_close.png')}}') no-repeat center center;
        }
    </style>

@endsection

@section('footer_scripts')


    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#date_range').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY',
                },
                ranges: {
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });

            $('#date_range').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            });

            $('#date_range').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });


            $('[data-toggle="tooltip"]').tooltip();

                $('#slotTableList tbody').on('click', '.childAvailable', function () {
                    var tr = $(this).parent().next();

                    if(tr.is(":visible")){
                        tr.hide();
                        $(this).parent().removeClass('shown');
                    }
                    else{
                        tr.show();
                        $(this).parent().addClass('shown');
                    }

                });

        });
    </script>
@endsection
