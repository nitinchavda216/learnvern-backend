@extends('layout.master')
@section('content')
    <div class="az-content" style="min-height: 85vh;">
        <div class="container-fluid">
            <div class="az-content-body">
            <h2 class="az-content-title text-white">Howdy, {{ getCurrentAdmin()->name }} !
                <span class="float-right d-inline-block">Welcome to the Dashboard</span>
            </h2>

            <div id="data-container">

                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header tx-medium bd-0 tx-white bg-gray-600 text-center">
                                FollowUp Statistics
                            </div><!-- card-header -->
                            <div class="card-body bd bd-t-0">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card">
                                            <div class="card-header bg-gray-200 text-center">
                                                Referral Campaign Follow-Ups
                                            </div><!-- card-header -->
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-6 text-center border-right">
                                                        <h6>Active</h6>
                                                        <h3>
                                                            <a href="{{route('crm.followUpStatisticsReferral',['type'=>'active'])}}"
                                                               class="text-success">{{$statistics['pending_refferal_statistics']['active_followup']}}</a>
                                                        </h3>
                                                    </div>
                                                    <div class="col-md-6 text-center">
                                                        <h6>Passive</h6>
                                                        <h3>
                                                            <a href="{{route('crm.followUpStatisticsReferral',['type'=>'passive'])}}"
                                                               class="text-warning">{{$statistics['pending_refferal_statistics']['passive_followup']}}</a>
                                                        </h3>
                                                    </div>
                                                </div>

                                            </div><!-- card-body -->
                                        </div>


                                    </div>
                                    <div class="col-md-6">
                                        <div class="card">
                                            <div class="card-header bg-gray-200 text-center">
                                                Revenue Campaign Follow-Ups
                                            </div><!-- card-header -->
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-6 text-center border-right">
                                                        <h6>Active</h6>
                                                        <h3>
                                                            <a href="{{route('crm.followUpStatisticsRevenue',['type'=>'active'])}}" class="text-success">
                                                            {{$statistics['pending_revenue_statistics']['active_followup']}}
                                                            </a>
                                                        </h3>
                                                    </div>
                                                    <div class="col-md-6 text-center">
                                                        <h6>Passive</h6>
                                                        <h3>
                                                            <a href="{{route('crm.followUpStatisticsRevenue',['type'=>'passive'])}}" class="text-warning">
                                                                {{$statistics['pending_revenue_statistics']['passive_followup']}}
                                                            </a>
                                                        </h3>
                                                    </div>
                                                </div>
                                            </div><!-- card-body -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        @if(getCurrentAdmin()->relatedRole->is_super_admin == 0 && getCurrentAdmin()->relatedRole->is_crm_manager == 0)
                        <div class="card card-dashboard-finance">
                            <h6 class="card-title">Todays's Activity <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Today all activity included fresh followup,pending followup,not interested,other"><i class="fa fa-info-circle" aria-hidden="true"></i></a></h6>
                            <h2><span></span>{{$statistics['today_followup']}}</h2>
                        </div>
                        @endif
                        <div class="card card-dashboard-finance mt-2">
                            <a class="btn btn-primary btm-sm text-center" href="{{route('crm.pending',['date_range'=>date('d-m-Y').' - '.date('d-m-Y')])}}">Today Pending Followup</a>
                        </div>
                        @if(getCurrentAdmin()->relatedRole->is_super_admin == 1 || getCurrentAdmin()->relatedRole->is_crm_manager == 1)
                            <div class="card card-dashboard-finance mt-2">
                                <a class="btn btn-primary btm-sm text-center" href="{{route('crm.campaignReport')}}">View Campaign Report</a>
                            </div>
                        @endif
                    </div>
                </div>
                @if(getCurrentAdmin()->relatedRole->is_super_admin == 0 && getCurrentAdmin()->relatedRole->is_crm_manager == 0)

                <div class="row mt-3">

                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header bg-gray-200">
                               <h4> Assigned Campaigns</h4>
                            </div><!-- card-header -->
                            <div class="card-body">
                                <div>
                                    <table id="dataTableList" class="">
                                        <thead>
                                        <tr>
                                            <th>Campaign Name</th>
                                            <th>Marketing Goal</th>
                                            <th>Action</th>
                                            <th>Created By</th>
                                            <th>Assigned To</th>
                                            <th>No Of Contact</th>
                                            <th>Move To Followup</th>
                                            <th>Not Interested</th>
                                            <th>Other</th>
                                            <th>Created Date</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>

                            </div><!-- card-body -->
                        </div>



                    </div>
                </div>
                 @endif

            </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <style>
        div.dataTables_wrapper {
            max-width: 100%;
            /*margin: 0 auto;*/
        }

        .dataTable thead th {
            background-color: #fff;
            height: 40px;
        }

        .dataTables_scrollHeadInner table.dataTable {
            margin-bottom: 0px;
        }

        #dataTableList {
            margin: 0px;
        }
    </style>
@endsection


@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            $('[data-toggle="tooltip"]').tooltip();

            if($('#dataTableList').length > 0) {
                var $dataTableList = $('#dataTableList').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: false,
                    bFilter: false,
                    "aaSorting": [],
                    "scrollX": true,
                    ajax: {
                        url: $app_url + '/crm/getAjaxListCampaignData',
                        data: function (d) {
                            d.marketing_goal = $('#marketing_goal').val();
                            d.action = $('#action').val();
                            d.accessible_to = $('#accessible_to').val();
                        }
                    },
                    "columns": [
                        {data: 'name', 'sortable': false,width:'10%'},
                        {data: 'marketing_goal', 'sortable': false,width:'10%'},
                        {data: 'action', 'sortable': false,width:'10%'},
                        {data: 'created_by_name', 'sortable': false,width:'10%'},
                        {data: 'assign_to_user_name', 'sortable': false,width:'10%'},
                        {data: 'count_user', 'sortable': false,width:'10%'},

                        {data: 'move_to_followup_count', 'sortable': false,width:'5%'},
                        {data: 'not_interest_count', 'sortable': false,width:'5%'},
                        {data: 'other_count', 'sortable': false,width:'10%'},

                        {data: 'created_at', 'sortable': false,width:'10%'},
                        {data: 'actions_menu', 'orderable': false,width:'10%'},
                    ],
                });
            }

        });
    </script>
@endsection
