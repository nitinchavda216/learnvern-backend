@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Edit Section
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('sections') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="card card-body pd-40">
                        <form role="form" method="post" action=" {{ route('sections.update', $section->id)  }}">
                            @csrf
                            @include('section.form')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#courseId').select2({
                placeholder: 'Select Course'
            });
        });
    </script>
@endsection
