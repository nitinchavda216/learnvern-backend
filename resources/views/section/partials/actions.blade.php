<a href="{{ route('sections.edit', $section->id) }}" class="btn btn-primary btn-xs d-inline">
    <i class="fas fa-edit"></i>
</a>
<div class="d-inline activeStatus az-toggle az-toggle-secondary float-right {{ ($section->is_active == 1) ? 'on' : '' }}"
     data-id="{{ $section->id }}">
    <span></span>
</div>
