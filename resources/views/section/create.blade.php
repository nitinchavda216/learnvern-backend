@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Create Section
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('sections') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="card card-body pd-40">
                        <form role="form" method="post" action="{{route('sections.store')}}">
                            @csrf
                            @include('section.form')
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row row-xs newRowTemplate" style="display: none;">
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::text('sections[]', null, ['class'=>'form-control', 'data-validation' => 'required', 'placeholder' => "Enter Section Name"]) !!}
                </div>
            </div>
            <div class="col-sm-2">
                <a href="javascript:;" class="btn btn-danger btn-sm removeRow">
                    <i class="fas fa-trash"></i>
                </a>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#courseId').select2({
                placeholder: 'Select Course'
            });

            $(document).on('click', "#addNewRow", function () {
                var newRowCloned = $('.newRowTemplate').last().clone();
                newRowCloned.show();
                $("#sectionRows").append(newRowCloned);
            });
            $(document).on('click', ".removeRow", function () {
                var self = $(this);
                self.parents(".newRowTemplate").remove();
            });

            $(document).on('change', "#courseId", function () {
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: $app_url + '/sections/get-parent-courses',
                    data: {'course_id': $(this).val()},
                    beforeSend: function () {
                        $("#parentSectionsDiv").html("");
                    },
                    success: function (data) {
                        if (data.status){
                            $("#parentSectionsDiv").html(data.html);
                            $('#parentSectionInput').select2({
                                placeholder: 'Select Parent Section'
                            });
                        }
                    }
                });
            });
        });
    </script>
@endsection
