@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Sections List
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('sections.create') }}" class="btn btn-success btn-sm mt-negative">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="az-content-label mg-b-5">For Content Team Import New Sections:</div>
                    {!! Form::open(['route' => 'sections.export', 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'export-form']) !!}
                    <div class="row row-xs">
                        <div class="col-md-5">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Course :</label>
                            {!! Form::select('course_id', $courses, NULL,['id' => 'courses_id', 'class' => 'form-control select2', 'placeholder' => "", 'data-validation' => 'required']) !!}
                        </div>
                        <div class="mg-l-10 mt-4">
                            <button type="submit" class="btn btn-sm btn-success float-right mg-l-0">
                                <i class="fa fa-download" aria-hidden="true"></i> Download Import Sample
                            </button>
                        </div>
                        <div class="mg-l-10 mt-4">
                            <a href="javascript:;" class="btn btn-sm btn-info float-right mg-l-0"
                               data-toggle="modal"
                               data-target="#uploadFileModal"><i class="fa fa-upload" aria-hidden="true"></i> Import New Sections</a>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <hr class="mg-y-20">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-5">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Course :</label>
                                {!! Form::select('course_id', $courses, request('course_id'),['id' => 'course_id', 'class' => 'form-control select2', 'placeholder' => ""]) !!}
                            </div>
                            {{--<div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Hidden Sections Status:</label>
                                {!! Form::select('hide_from_backend',[0 => 'Visible Sections', 1 => "Hidden Sections"], request('hide_from_backend'),['id' => 'hide_from_backend', 'class' => 'form-control select2_no_search']) !!}
                            </div>--}}
                            <div class="col-md-2 mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>Section Name</th>
                            <th>Courses Name</th>
                            {{--                            <th>Hidden Status</th>--}}
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="uploadFileModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    Upload File
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form method="post" action="{{route('sections.import')}}"
                      enctype="multipart/form-data">
                    <div class="modal-body">
                        @csrf
                        <input name="file" type="file" id="FileUpload" class="file-upload" data-validation="required"
                               data-allowed-file-extensions="xlsx"/>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-indigo">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('#dataTableList').length) {
                var $dataTableList = $('#dataTableList').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    bFilter: false,
                    "aaSorting": [],
                    ajax: {
                        url: $app_url + '/sections/getAjaxListData',
                        data: function (d) {
                            d.course_id = $('#course_id').val();
                            // d.hide_from_backend = $('#hide_from_backend').val();
                        }
                    },
                    "columns": [
                        {data: 'name', 'sortable': false, 'width': "30%"},
                        {data: 'course_name', 'sortable': false, 'width': "30%"},
                        // {data: 'hide_status', 'sortable': false, 'width': "20%"},
                        {data: 'action', 'orderable': false, 'width': "20%"}
                    ],
                    select: true,
                    bStateSave: true,
                    fnStateSave: function (settings, data) {
                        localStorage.setItem("dataTables_state", JSON.stringify(data));
                    },
                    fnStateLoad: function (settings) {
                        return JSON.parse(localStorage.getItem("dataTables_state"));
                    }
                });

                $('#search-form').on('submit', function (e) {
                    $dataTableList.draw();
                    e.preventDefault();
                });

                $('#reset-filters').on('click', function () {
                    $("#course_id").val(null).trigger('change');
                    // $("#hide_from_backend").val(0).trigger('change');
                    $dataTableList.draw();
                });
            }

            // Toggle Switches
            $(document).on('click', ".activeStatus", function () {
                var status = ($(this).toggleClass('on').hasClass('on') == true) ? 1 : 0;
                var id = $(this).data('id');
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: $app_url + '/sections/update_status',
                    data: {'is_active': status, 'id': id},
                    success: function () {
                        toastr.success("Status Updated Successfully!", "Success!", {timeOut: 2000});
                    }
                });
            });
/*
            $(document).on('click', '.hideFromBackendStatus', function () {
                var status = ($(this).toggleClass('on').hasClass('on') == true) ? 1 : 0;
                var id = $(this).data('id');
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: $app_url + '/curriculum/hide_from_backend/update',
                    data: {'hide_from_backend': status, 'id': id},
                    success: function (data) {
                        $dataTableList.draw();
                        toastr.success(data.message, "Success!", {timeOut: 2000});
                    }
                });
            });*/
        });
    </script>
@endsection

