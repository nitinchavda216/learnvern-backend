@if(isset($section))
    <div class="row row-xs">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Course:</label>
                {!! Form::select('course_id', $courses, $curriculum->course_id ?? old('course_id'), ['class'=>'form-control', 'data-validation' => 'required', 'placeholder' => "" ,'id' =>'courseId', 'disabled'] ) !!}
            </div>
        </div>
        @if(isset($curriculum->getParentSectionData))
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Parent Section:</label>
                    {!! Form::select('parent_section_id', [$curriculum->getParentSectionData->section_id => $curriculum->getParentSectionData->name], $curriculum->getParentSectionData->section_id , ['class'=>'form-control select2_no_search']) !!}
                </div>
            </div>
        @endif
    </div>
    <div class="row row-xs">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Section Name:</label>
                {!! Form::text('name', $section->name ?? old('name'), ['class'=>'form-control', 'data-validation' => 'required', 'placeholder' => "Enter Section Name"]) !!}
            </div>
        </div>
        @if(count($relatedCourses) > 0)
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        <strong>Related Courses</strong>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered mg-b-0">
                            <tbody>
                            <tr>
                                <th scope="row">
                                    {{ implode(', ', $relatedCourses) }}
                                </th>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
@else
    <div class="row row-xs">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Course :</label>
                {!! Form::select('course_id', $courses, null, ['class'=>'form-control', 'data-validation' => 'required', 'placeholder' => "" ,'id' =>'courseId'] ) !!}
            </div>
        </div>
        <div class="col-sm-6" id="parentSectionsDiv">
        </div>
    </div>
    <div id="sectionRows">
        <div class="row row-xs">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Section Name:</label>
                    {!! Form::text('sections[]', null, ['class'=>'form-control', 'data-validation' => 'required', 'placeholder' => "Enter Section Name"]) !!}
                </div>
            </div>
            <div class="col-sm-2 mt-4">
                <button type="button" class="btn btn-sm btn-primary" id="addNewRow">Add More Section</button>
            </div>
        </div>
    </div>
@endif

<button type="submit" class="btn btn-az-primary mg-md-t-9">{{ isset($section) ? "Update" : "Submit" }}</button>
