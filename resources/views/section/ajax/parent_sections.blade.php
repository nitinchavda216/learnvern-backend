<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Parent Section :</label>
    {!! Form::select('parent_section_id', $sections, old('parent_section_id'), ['class'=>'form-control', 'data-validation' => 'required', 'placeholder' => "" ,'id' =>'parentSectionInput'] ) !!}
</div>
