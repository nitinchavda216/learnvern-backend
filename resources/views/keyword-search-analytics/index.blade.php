@extends('layout.master')
@section('content')
<div class="az-content">
    <div class="container">
        <div class="az-content-body">
            <h2 class="az-content-title"> Keyword Search Analytics</h2>
            <div id="data-container">
                <div class="row">
                    <div class="col-lg-6 mb-4">
                        <div class="card">
                            <div class="card-body bg-gray-200">
                                {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                                <div class="row row-xs">
                                    <div class="col-md-6">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Course :</label>
                                        {!! Form::select('course_id', $courses, [],['id' => 'course_id', 'class' => 'form-control select2', 'placeholder' => ""]) !!}
                                    </div>
                                    <div class="col-md-6 mt-4">
                                        <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                        <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                        </button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="card">
                            <div class="card-body bg-gray-200">
                                {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'export-form']) !!}
                                <div class="row row-xs">
                                    <div class="col-md-6">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Date Range:</label>
                                        <input name="date_range" id="date_range" type="text" class="form-control"
                                               autocomplete="off" placeholder="--SELECT DATE--">
                                    </div>
                                    <div class="col-md-6 mt-4">
                                        <a href="javascript:;" class="btn btn-az-success btn-success"
                                           onclick="this.href = 'keyword-search-analytics/getAjaxExportData?date_range=' + document.getElementById('date_range').value">
                                            <i class="fa fa-download" aria-hidden="true"></i> Export
                                        </a>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
                <table id="dataTableList" class="display responsive">
                    <thead>
                        <tr>
                            <th>Keyword</th>
                            <th>Number of hits</th>
                            <th>Results</th>
                            <th>Courses</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $('#date_range').daterangepicker({
            autoUpdateInput: true,
            startDate: moment().subtract(6, 'days'),
            endDate: moment(),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            },
            ranges: {
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });

        if ($('#dataTableList').length) {
            var $dataTableList = $('#dataTableList').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                bFilter: false,
                "aaSorting": [],
                ajax: {
                    url: $app_url + '/keyword-search-analytics/getListData',
                    data: function (d) {
                        d.course_id = $('#course_id').val();
                    }
                },
                "columns": [
                    {data: 'keyword', 'sortable': false, 'width': "20%"},
                    {data: 'num_of_hits', 'sortable': false, 'width': "15%"},
                    {data: 'results', 'orderable': false, 'width': "15%"},
                    {data: 'course_names', 'orderable': false, 'width': "30%"},
                ],
                select: true,
                bStateSave: true,
                fnStateSave: function (settings, data) {
                    localStorage.setItem("dataTables_state", JSON.stringify(data));
                },
                fnStateLoad: function (settings) {
                    return JSON.parse(localStorage.getItem("dataTables_state"));
                }
            });

            $('#search-form').on('submit', function (e) {
                $dataTableList.draw();
                e.preventDefault();
            });

            $('#reset-filters').on('click', function () {
                $("#course_id").val(null).trigger('change');
                $("#date_range").val(null).trigger('change');
                $dataTableList.draw();
            });
        }
    });
</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
@endsection

