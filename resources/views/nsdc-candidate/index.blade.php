@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title">NSDC Candidates
                    <span class="pull-right d-inline-block float-right">
                        <a href="javascript:;" class="btn btn-sm btn-success mt-negative"
                           onclick="this.href='nsdc-candidates/getNSDCExportData?date_range=' + document.getElementById('date_range').value + '&search=' + document.getElementById('search').value+ '&certificate_type=' + document.getElementById('certificate_type').value+ '&partner_user_id=' + document.getElementById('partner_user_id').value"><i
                                    class="fa fa-download" aria-hidden="true"></i> Export</a>
                        <a href="javascript:;" class="btn btn-sm btn-info  mt-negative" data-toggle="modal"
                           data-target="#uploadFileModal"><i class="fa fa-upload" aria-hidden="true"></i> Import</a>
                    </span>
                </h2>
                <div id="data-container">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Search:</label>
                                <input name="search" id="search" type="text" class="form-control" placeholder="Search">
                            </div>
                            @if(session('partner_id') == 1)
                                <div class="col-md-2">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Partners:</label>
                                    {!! Form::select('partner_user_id', $partners, null,['id' => 'partner_user_id', 'class' => 'form-control select2']) !!}
                                </div>
                            @endif
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Registration Date:</label>
                                <input name="date_range" id="date_range" type="text" class="form-control"
                                       autocomplete="off" placeholder="Select Date">
                            </div>
                            {{--<div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Certificate Purchase Date:</label>
                                <input name="payment_date_range" id="payment_date_range" type="text" class="form-control"
                                       autocomplete="off" placeholder="Select Date">
                            </div>--}}
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Status:</label>
                                <select name="certificate_type" id="certificate_type" class="form-control">
                                    <option value="pending" selected>Pending</option>
                                    <option value="all">All NSDC Users</option>
                                </select>
                            </div>
                            <div class="col-md-2 mt-4">
                                <button type="submit" class="btn btn-sm btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-sm btn-az-warning btn-warning" id="reset-filters">Reset
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <table id="dataTableList" class="table table-responsive">
                        <thead>
                        <tr>
                            <th>User Details</th>
                            <th>Course Details</th>
                            <th>Candidate ID</th>
                            <th>Certificate ID</th>
                            <th>Purchase Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal fade" id="uploadFileModal" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                Upload File
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <form method="post" action="{{route('nsdc_candidate.getNSDCUploadData')}}"
                                  enctype="multipart/form-data">
                                <div class="modal-body">
                                    @csrf
                                    <input name="file" type="file" id="FileUpload" class="file-upload"
                                           data-allowed-file-extensions="xlsx"/>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-indigo">Upload</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="DateChangeModel" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                Change Date
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <form id="update-form">
                                <div class="modal-body">
                                    <input type="hidden" name="course_user_id" id="course_user_id">
                                    <div class="form-group">
                                        <label class="az-content-label">Start Date</label>
                                        {!! Form::date('start_date',  old('start_date'), ['class'=>'form-control', 'data-validation' => 'required' ,'id' => 'start_date']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label class="az-content-label">End Date</label>
                                        {!! Form::date('end_date',  old('end_date'), ['class'=>'form-control', 'data-validation' => 'required' ,'id'=>'end_date']) !!}
                                    </div>
                                </div>
                            </form>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-az-primary mg-md-t-9" id="submitButton">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">
        var $openUploadModal = "{{ session()->pull('openModal') }}";
        if (typeof $openUploadModal !== 'undefined' && $openUploadModal && ($("#uploadFileModal").length > 0)) {
            $("#uploadFileModal").modal('show');
        }
        var DateChangeModel = "{{ session()->pull('openModal') }}";
        if (typeof DateChangeModel !== 'undefined' && DateChangeModel && ($("#DateChangeModel").length > 0)) {
            $("#DateChangeModel").modal('show');
        }

        $(document).ready(function () {
            $('#date_range').daterangepicker({
                autoUpdateInput: true,
                startDate: moment().subtract(6, 'days'),
                endDate: moment(),
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY'
                },
                ranges: {
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            $('#date_range').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            });

            $('#date_range').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            /*$('#payment_date_range').daterangepicker({
                autoUpdateInput: true,
                startDate: moment().subtract(6, 'days'),
                endDate: moment(),
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY'
                },
                ranges: {
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            $('#payment_date_range').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            });

            $('#payment_date_range').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });*/

            var $dataTableList = $('#dataTableList').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                bFilter: false,
                "aaSorting": [],
                ajax: {
                    url: $app_url + '/nsdc-candidates/getAjaxData',
                    data: function (d) {
                        d.search = $('#search').val();
                        d.date_range = $('#date_range').val();
                        d.certificate_type = $('#certificate_type').val();
                        d.partner_user_id = $('#partner_user_id').val();
//                        d.payment_date_range = $('#payment_date_range').val();
                    }
                },
                "columns": [
                    {data: 'userDetails', 'sortable': false, 'width': "25%"},
                    {data: 'courseDetails', 'sortable': false, 'width': "20%"},
                    {data: 'candidate_id', 'sortable': false, 'width': "10%"},
                    {data: 'certificate_id', 'sortable': false, 'width': "10%"},
                    {data: 'payment_date', 'sortable': false, 'width': "10%"},
                    {data: 'action', 'sortable': false, 'width': "15%"}
                ],
                select: true,
                bStateSave: true,
                fnStateSave: function (settings, data) {
                    localStorage.setItem("dataTables_state", JSON.stringify(data));
                },
                fnStateLoad: function (settings) {
                    return JSON.parse(localStorage.getItem("dataTables_state"));
                }
            });

            $('#search-form').on('submit', function (e) {
                $dataTableList.draw();
                e.preventDefault();
            });

            $('#reset-filters').on('click', function () {
                $("#search").val(null);
                $("#date_range").val(null);
//                $("#payment_date_range").val(null);
                $("#certificate_type").val('pending').trigger('change');
                $("#partner_user_id").val(1).trigger('change');
                $dataTableList.draw();
            });

            $(document).on('click', '.openUpdateDateModal', function () {
                $("#course_user_id").val($(this).data('id'));
                $("#DateChangeModel").modal('show');
            });

            $(document).on('click', '#submitButton', function () {
                var dataValue = $("#update-form").serialize();
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: $app_url + '/nsdc-candidates/changeDates',
                    data: dataValue,
                    success: function () {
                        $("#DateChangeModel").modal('hide');
                        toastr.success("Status Updated Successfully!", "Success!", {timeOut: 2000});
                    }
                });
            });
        });
    </script>
@endsection

