<form name="" target="_blank" data-course-id="{{$data->courseDetail->id}}"
      action="{{ route('certificate.download')}}" class="frm_get_user_certificate" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="hdn_course_id" value="{{$data->courseDetail->id}}">
    <input type="hidden" name="hdn_user_id" value="{{$data->userDetail->id}}">
    <button type="submit" class="btn btn-primary btn-xs" name="btn_get_certificate"><i
                class="fa fa-download" aria-hidden="true"></i></button>
</form>
@if (isset($data->userDetail->candidate_id))
    <button class='btn btn-info btn-xs openUpdateDateModal' data-id={{ $data->id }}>Update</button>
@endif



