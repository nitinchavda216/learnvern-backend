@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Create Resume Template
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('resume_templates') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="card card-body pd-40">
                        {!! Form::open(['route'=>'resume_templates.store','method' => 'POST', 'enctype'=> "multipart/form-data", 'files' => true]) !!}
                            @include('resume_templates.form')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
