@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Resume Templates
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('resume_templates.create') }}" class="btn btn-success btn-sm mt-negative">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($templates as $item)
                            <tr>
                                <td>{{ $item->title ?? "- - -" }}</td>
                                <td>
                                    <a href="{{ route('resume_templates.edit', $item->id) }}"
                                       class="btn btn-primary btn-xs">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    <a href="javascript:;"
                                       onclick="confirmDelete('{{ route('resume_templates.delete', $item->id) }}')"
                                       class="btn btn-danger btn-xs">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="exampleModal" class="modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Details</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6" class="list-group">
                            <h3 style="text-align: center">Old Data</h3>
                            <ul id="old">
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <h3 style="text-align: center">Changes</h3>
                            <ul id="attributes" class="list-group">
                            </ul>
                        </div>
                    </div>
                </div><!-- modal-body -->
            </div>
        </div><!-- modal-dialog -->
    </div>

@endsection
@section('footer_scripts')
    <script type="text/javascript">
        function myFunction(id) {
            $.ajax({
                url: $app_url + '/get_activity_changes',
                data: "id=" + id,
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    var attributes = data.attributes;
                    var set = '';
                    $.each(attributes, function (index, val) {
                        set += '<li class="list-group-item"><b>' + index + '</b> : ' + val + '</li>';
                    });
                    var old = data.old;
                    var setold = ''
                    $.each(old, function (index, val) {
                        setold += '<li class="list-group-item"><b>' + index + '</b> : ' + val + '</li>';
                    });
                    $('#exampleModal').modal('toggle');
                    $('#attributes').html(set);
                    $('#old').html(setold);
                }
            });
        }
        $(document).ready(function () {
            $('#dataTableList').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                }
            });
        });
    </script>
@endsection

