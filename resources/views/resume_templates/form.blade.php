<div class="row row-xs">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title :</label>
            {!! Form::text('title', $template->title ?? old('title'), ['class'=>'form-control', 'data-validation' => 'required','id' =>'title'] ) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Image :</label>
            <input type="file" accept="image/*" class="file-upload" data-show-remove="false"
                   data-allowed-file-extensions="png jpg jpeg svg"
                   @if(isset($template) && isset($template->image)) data-default-file="{{ showResumeTemplateImage($template->image) }}"
                   @else data-validation="required" @endif name="image" id="image">
        </div>
    </div>
</div>
<button type="submit" class="btn btn-az-primary mg-md-t-9">{{ isset($template) ? "Update" : "Submit" }}</button>