@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">

                <h2 class="az-content-title">Add new Admin User
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('admin_users') }}" class="btn btn-warning btn-sm mt-negative"
                           title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="card card-body">
                        <form action="{{ route('admin_users.store') }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="name">NAME</label>
                                        <input type="text" name="name" class="form-control" id="name"
                                               value="{{ old('name') }}"
                                               placeholder="User Name" data-validation="required"
                                               data-validation-error-msg="Please enter a name!">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="emailAddress">EMAIL ADDRESS</label>
                                        <input type="email" name="email" class="form-control" id="emailAddress"
                                               value="{{ old('email') }}"
                                               aria-describedby="emailAddressHelp" placeholder="Enter email"
                                               data-validation="email">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="emailAddress">Role</label>
                                        {!! Form::select('role_id', $roles, [],['id' => 'role_id', 'class' => 'form-control select2', 'placeholder' => ""]) !!}

                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-az-primary ">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
