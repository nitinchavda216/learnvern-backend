@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Admin Users
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('admin_users.create') }}" class="btn btn-success btn-sm mt-negative"
                           title="Add New">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="table-responsive">
                        <table class="table table-striped mg-b-0">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Join Date</th>
                                <th>Role</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            @foreach($admins as $item)
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ formatDate($item->created_at, 'Y-m-d') }}</td>
                                    <td>{{ isset($item->relatedRole) ? $item->relatedRole->name : "--" }}</td>
                                    <td>
                                        <a href="{{ route('admin_users.edit', $item->id) }}"
                                           class="btn btn-primary btn-xs">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="javascript:;"
                                           onclick="confirmDelete('{{ route('admin_users.delete',$item->id) }}')"
                                           class="btn btn-danger btn-xs">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
