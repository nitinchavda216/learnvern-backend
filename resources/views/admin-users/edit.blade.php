@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Edit Admin User
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('admin_users') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="card card-body">
                        {!! Form::open(['route'=>['admin_users.update', $admins->id ],'method' => 'POST' ]) !!}
                        @csrf
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="name">NAME</label>
                                    {!! Form::text('name', $admins->name ?? old('name'), ['class'=>'form-control', 'id' => 'name', 'data-validation' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="emailAddress">EMAIL ADDRESS</label>
                                    {!! Form::text('email', $admins->email ?? old('email'), ['class'=>'form-control', 'id' => 'email', 'data-validation' => 'required']) !!}

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="emailAddress">Role</label>
                                    {!! Form::select('role_id', $roles, $admins->role_id,['id' => 'role_id', 'class' => 'form-control select2', 'placeholder' => ""]) !!}

                                </div>
                            </div>
                        </div>
                        <button type="submit"
                                class="btn btn-az-primary mg-md-t-9">{{ isset($review) ? "Update" : "Submit" }}</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop