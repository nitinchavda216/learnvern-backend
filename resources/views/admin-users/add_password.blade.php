<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Azia">
    <meta name="twitter:description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="twitter:image" content="http://themepixels.me/azia/img/azia-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/azia">
    <meta property="og:title" content="Azia">
    <meta property="og:description" content="Responsive Bootstrap 4 Dashboard Template">

    <meta property="og:image" content="http://themepixels.me/azia/img/azia-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/azia/img/azia-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="author" content="ThemePixels">

    <title>LearnVern</title>

    <link href="{{ asset('admin/css/azia.css')}}" rel="stylesheet"/>
    <link defer href="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/theme-default.min.css"
          rel="stylesheet"/>

</head>
<body class="az-body">
<div class="az-signin-wrapper">
    <div class="az-card-signin">
        <h1 class="az-logo">LearnVern</h1>
        <div class="az-signin-header">
            <h2>Welcome {{ $user->name }}!</h2>
            <h4>Please set password to continue</h4>
            @include('layout.partials.flash_messages')
            <form action="{{ route('admin-user.password.store',$user->id) }}" method="post" id="loginForm">
                @csrf
                <div class="form-group">
                    <label>EMAIL</label>
                    <input type="text" name="email" class="form-control" placeholder="Enter your email"
                           value="{{ $user->email }}"
                           data-validation="email" disabled>
                </div><!-- form-group -->
                <div class="form-group">
                    <label>PASSWORD</label>
                    {!! Form::password('password_confirmation', ['class'=>'form-control', 'placeholder'=>'Enter password', 'data-validation'=>'required length', 'data-validation-length' => 'min6',
                            'data-validation-error-msg-required' => "Please enter a password!", 'data-validation-error-msg-length' => "The input value is shorter than 6 characters"]) !!}
                </div>
                <div class="form-group">
                    <label>CONFORM PASSWORD</label>
                    {!! Form::password('password',['class'=>'form-control', 'data-validation' => 'confirmation', 'placeholder'=>'Enter confirm password',
                            'data-validation-error-msg' => "Your confirm password does not match with your password!"]) !!}
                </div>
                <input type="hidden" name="activation_token" value="{{ $user->activation_token }}">
                <button class="btn btn-az-primary btn-block">Proceed</button>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{asset('admin/lib/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<script>
    $.validate({
        modules: 'security'
    });
</script>
</body>
</html>
