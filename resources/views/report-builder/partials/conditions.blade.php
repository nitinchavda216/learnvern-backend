<div class="conditionInputDiv">
    <div class="form-group elementType">
        @foreach($reportsData['element_type'] as $key => $element_type)
            <button class="btn btn-primary btn-sm d-inline-block {{ $element_type }}Condition text-capitalize elementConditionList mt-2"
                    type="button" data-type="{{ $key }}">
                {{ $element_type }}
            </button>
        @endforeach
        <div class="conditionElements" style="margin-top: 20px;">
            @if(isset($selectedData['conditions']))
                @foreach($selectedData['conditions'] as $key => $conditions)
                    @if($key !="0")
                        @include('report-builder.partials.condition_rule', [$conditions])
                    @endif
                @endforeach
            @endif
        </div>
    </div>
</div>