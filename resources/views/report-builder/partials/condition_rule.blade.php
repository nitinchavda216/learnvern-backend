@php
    $element_type = isset($conditions['element_type']) ? $conditions['element_type'] : $element_type;
@endphp
<div class="conditionsData removeRow">
    @if($element_type == 'courses')
        <div class="p-2 mt-2 posi-col">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-orange">Course</h5>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Condition</label>
                        {!! Form::select("conditions[$key][identifier]", $reportsData['identifier'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['identifier'] : ''), ['class' => 'elementIdentifier form-control']) !!}
                        </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group elementDiv">
                        <label>Value</label>
                        {!! Form::select("conditions[$key][values][]",$condition_value['course_list'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['values'] : ''),['class'=>'elementValue form-control',(isset($selectedData['conditions'][$key]) && ($selectedData['conditions'][$key]['identifier'] == 'is_one_of' || $selectedData['conditions'][$key]['identifier'] == 'is_not_one_of'))?'multiple':'']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <a class="btn btn-sm btn-danger removeConditionRule pull-right mt-4" href="javascript:;">
                        <i class="fa fa-trash"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-12">
            <input type="hidden" name="conditions[{{$key}}][element_type]" class="hiddenElementType form-control" value="{{ $element_type }}"/>
        </div>
    @elseif($element_type == 'webinars')
        <div class="p-2 mt-2 posi-col">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-orange">Webinar</h5>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Condition</label>
                        {!! Form::select("conditions[$key][identifier]", $reportsData['identifier'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['identifier'] : ''), ['class' => 'elementIdentifier form-control']) !!}
                        </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group elementDiv">
                        <label>Value</label>
                        {!! Form::select("conditions[$key][values][]",$condition_value['webinar_list'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['values'] : ''),['class'=>'elementValue form-control',(isset($selectedData['conditions'][$key]) && ($selectedData['conditions'][$key]['identifier'] == 'is_one_of' || $selectedData['conditions'][$key]['identifier'] == 'is_not_one_of'))?'multiple':'']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <a class="btn btn-sm btn-danger removeConditionRule pull-right mt-4" href="javascript:;">
                        <i class="fa fa-trash"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-12">
            <input type="hidden" name="conditions[{{$key}}][element_type]" class="hiddenElementType form-control" value="{{ $element_type }}"/>
        </div>
    @elseif($element_type == 'category')
        <div class="p-2 mt-2 posi-col">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-orange">Category</h5>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Condition</label>
                        {!! Form::select("conditions[$key][identifier]", $reportsData['identifier'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['identifier'] : ''), ['class' => 'elementIdentifier form-control']) !!}
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group elementDiv">
                        <label>Value</label>
                        {!! Form::select("conditions[$key][values][]", $condition_value['category_list'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['values'] : ''), ['class' => 'elementValue form-control',(isset($selectedData['conditions'][$key]) && ($selectedData['conditions'][$key]['identifier'] == 'is_one_of' || $selectedData['conditions'][$key]['identifier'] == 'is_not_one_of'))?'multiple':'']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <a class="btn btn-sm btn-danger removeConditionRule pull-right mt-4" href="javascript:;">
                        <i class="fa fa-trash"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-12">
            <input type="hidden" name="conditions[{{$key}}][element_type]" class="hiddenElementType form-control" value="{{ $element_type }}"/>
        </div>
    @elseif($element_type == 'enroll_type')
        <div class="p-2 mt-2 posi-col">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-orange">Certificate Purchase</h5>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Condition</label>
                        {!! Form::select("conditions[$key][identifier]", ['is'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['identifier'] : ''), ['class' => 'elementIdentifier form-control']) !!}
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group elementDiv">
                        <label>Value</label>
                        {!! Form::select("conditions[$key][values][]", $condition_value['enroll_type_list'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['values'] : ''), ['class' => 'elementValue form-control',(isset($selectedData['conditions'][$key]) && ($selectedData['conditions'][$key]['identifier'] == 'is_one_of' || $selectedData['conditions'][$key]['identifier'] == 'is_not_one_of'))?'multiple':'']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <a class="btn btn-sm btn-danger removeConditionRule pull-right mt-4" href="javascript:;">
                        <i class="fa fa-trash"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-12">
            <input type="hidden" name="conditions[{{$key}}][element_type]" class="hiddenElementType form-control" value="{{ $element_type }}"/>
        </div>
    @elseif($element_type == 'from_nsdc')
        <div class="p-2 mt-2 posi-col">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-orange">From NSDC</h5>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Condition</label>
                        {!! Form::select("conditions[$key][identifier]", ['is'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['identifier'] : ''), ['class' => 'elementIdentifier form-control']) !!}
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group elementDiv">
                        <label>Value</label>
                        {!! Form::select("conditions[$key][values][]", $condition_value['from_nsdc_type_list'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['values'] : ''), ['class' => 'elementValue form-control',(isset($selectedData['conditions'][$key]) && ($selectedData['conditions'][$key]['identifier'] == 'is_one_of' || $selectedData['conditions'][$key]['identifier'] == 'is_not_one_of'))?'multiple':'']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <a class="btn btn-sm btn-danger removeConditionRule pull-right mt-4" href="javascript:;">
                        <i class="fa fa-trash"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-12">
            <input type="hidden" name="conditions[{{$key}}][element_type]" class="hiddenElementType form-control" value="{{ $element_type }}"/>
        </div>

        @elseif($element_type == 'current_occupation')
            <div class="p-2 mt-2 posi-col">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="text-orange">Employment Status</h5>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Condition</label>
                            {!! Form::select("conditions[$key][identifier]", ['is'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['identifier'] : ''), ['class' => 'elementIdentifier form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group elementDiv">
                            <label>Value</label>
                            {!! Form::select("conditions[$key][values][]", $condition_value['current_occupation_list'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['values'] : ''), ['class' => 'elementValue form-control',(isset($selectedData['conditions'][$key]) && ($selectedData['conditions'][$key]['identifier'] == 'is_one_of' || $selectedData['conditions'][$key]['identifier'] == 'is_not_one_of'))?'multiple':'']) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <a class="btn btn-sm btn-danger removeConditionRule pull-right mt-4" href="javascript:;">
                            <i class="fa fa-trash"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <input type="hidden" name="conditions[{{$key}}][element_type]" class="hiddenElementType form-control" value="{{ $element_type }}"/>
            </div>
    @elseif($element_type == 'colleges')
        <div class="p-2 mt-2 posi-col">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-orange">Colleges</h5>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Condition</label>
                        {!! Form::select("conditions[$key][identifier]", $reportsData['identifier'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['identifier'] : ''), ['class' => 'elementIdentifier form-control']) !!}
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group elementDiv">
                        <label>Value</label>
                        {!! Form::select("conditions[$key][values][]", $condition_value['colleges_list'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['values'] : ''), ['class' => 'elementValue form-control',(isset($selectedData['conditions'][$key]) && ($selectedData['conditions'][$key]['identifier'] == 'is_one_of' || $selectedData['conditions'][$key]['identifier'] == 'is_not_one_of'))?'multiple':'', 'placeholder' => ""]) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <a class="btn btn-sm btn-danger removeConditionRule pull-right mt-4" href="javascript:;">
                        <i class="fa fa-trash"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-12">
            <input type="hidden" name="conditions[{{$key}}][element_type]" class="hiddenElementType form-control" value="{{ $element_type }}"/>
        </div>
     @elseif($element_type == 'key_skills')
            <div class="p-2 mt-2 posi-col">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="text-orange">Key Skills</h5>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Condition</label>
                            {!! Form::select("conditions[$key][identifier]", $reportsData['identifier'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['identifier'] : ''), ['class' => 'elementIdentifier form-control']) !!}
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group elementDiv">
                            <label>Value</label>
                            {!! Form::select("conditions[$key][values][]", $condition_value['key_skills_list'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['values'] : ''), ['class' => 'elementValue form-control',(isset($selectedData['conditions'][$key]) && ($selectedData['conditions'][$key]['identifier'] == 'is_one_of' || $selectedData['conditions'][$key]['identifier'] == 'is_not_one_of'))?'multiple':'']) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <a class="btn btn-sm btn-danger removeConditionRule pull-right mt-4" href="javascript:;">
                            <i class="fa fa-trash"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <input type="hidden" name="conditions[{{$key}}][element_type]" class="hiddenElementType form-control" value="{{ $element_type }}"/>
            </div>
    @elseif($element_type == 'technical_skills')
        <div class="p-2 mt-2 posi-col">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-orange">Technical Skills</h5>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Condition</label>
                        {!! Form::select("conditions[$key][identifier]", $reportsData['identifier'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['identifier'] : ''), ['class' => 'elementIdentifier form-control']) !!}
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group elementDiv">
                        <label>Value</label>
                        {!! Form::select("conditions[$key][values][]", $condition_value['technical_skills_list'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['values'] : ''), ['class' => 'elementValue form-control',(isset($selectedData['conditions'][$key]) && ($selectedData['conditions'][$key]['identifier'] == 'is_one_of' || $selectedData['conditions'][$key]['identifier'] == 'is_not_one_of'))?'multiple':'']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <a class="btn btn-sm btn-danger removeConditionRule pull-right mt-4" href="javascript:;">
                        <i class="fa fa-trash"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-12">
            <input type="hidden" name="conditions[{{$key}}][element_type]" class="hiddenElementType form-control" value="{{ $element_type }}"/>
        </div>

    @elseif($element_type == 'qualification')
        <div class="p-2 mt-2 posi-col">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-orange">Qualification</h5>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Condition</label>
                        {!! Form::select("conditions[$key][identifier]", $reportsData['identifier'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['identifier'] : ''), ['class' => 'elementIdentifier form-control']) !!}
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group elementDiv">
                        <label>Value</label>
                        {!! Form::select("conditions[$key][values][]", $condition_value['qualification_list'], (isset($selectedData['conditions'][$key]) ? $selectedData['conditions'][$key]['values'] : ''), ['class' => 'elementValue form-control',(isset($selectedData['conditions'][$key]) && ($selectedData['conditions'][$key]['identifier'] == 'is_one_of' || $selectedData['conditions'][$key]['identifier'] == 'is_not_one_of'))?'multiple':'']) !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <a class="btn btn-sm btn-danger removeConditionRule pull-right mt-4" href="javascript:;">
                        <i class="fa fa-trash"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-12">
            <input type="hidden" name="conditions[{{$key}}][element_type]" class="hiddenElementType form-control" value="{{ $element_type }}"/>
        </div>
        @endif
</div>
