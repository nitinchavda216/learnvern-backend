<div class="row">
    <div class="col-md-6">
        <h2>Fields Selection</h2>
        <div class="row">
            <div class="col-md-4">
                <div class="card mb-0">
                    <div class="card-header bg-info">
                        <h6 class="text-white mb-0">Course Progress</h6>
                    </div>
                    <div class="card-body pre-scrollable">
                        @if($reportsData['fieldArray'] && $reportsData['fieldArray']['CourseUser'])
                            @foreach($reportsData['fieldArray']['CourseUser'] as $key => $value)
                                <div class="position-relative form-check form-check-inline d-block">
                                    <label class="form-check-label text-truncate">
                                        <input type="checkbox"
                                               @if(in_array($key, $selectedData['fields'])) checked
                                               @endif
                                               name="selectField[]"
                                               class="form-check-input"
                                               value="{{$key}}"> {{ $reportsData['fieldArray']['CourseUser'][$key]['label']}}
                                    </label>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-0">
                    <div class="card-header bg-info">
                        <h6 class="text-white mb-0">Course Information</h6>
                    </div>
                    <div class="card-body pre-scrollable">
                        @if($reportsData['fieldArray'] && $reportsData['fieldArray']['Course'])
                            @foreach($reportsData['fieldArray']['Course'] as $key => $value)
                                <div class="position-relative form-check form-check-inline d-block">
                                    <label class="form-check-label text-truncate">
                                        <input type="checkbox"
                                               @if(in_array($key, $selectedData['fields'])) checked
                                               @endif
                                               name="selectField[]"
                                               class="form-check-input"
                                               value="{{$key}}"> {{ $reportsData['fieldArray']['Course'][$key]['label']}}
                                    </label>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-0">
                    <div class="card-header bg-info">
                        <h6 class="text-white mb-0">User Information</h6>
                    </div>
                    <div>
                        <div class="card-body pre-scrollable">
                            @if($reportsData['fieldArray'] && $reportsData['fieldArray']['User'])
                                @foreach($reportsData['fieldArray']['User'] as $key => $value)
                                    <div class="position-relative form-check form-check-inline d-block">
                                        <label class="form-check-label text-truncate">
                                            <input type="checkbox"
                                                   @if(in_array($key, $selectedData['fields'])) checked
                                                   @endif
                                                   name="selectField[]"
                                                   class="form-check-input"
                                                   value="{{$key}}"> {{ $reportsData['fieldArray']['User'][$key]['label']}}
                                        </label>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="main-card mb-3 card">
            <div class="card-header alert-primary">Conditions</div>
            <div class="card-body">
                <div class="form-group">
                    <label for="timelimit" class="">Course Enrollment Date Limit</label>
                    <select name="conditions[0][timeLimit]" id="timelimit" class="form-control">
                        <option value="">No Limit</option>
                        @foreach($reportsData['timeArray'] as $key => $value)
                            <option
                                value="{{$key}}" {{ ($selectedData['conditions'][0]['timeLimit'] == $key)?"selected='selected'":'' }}>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" id="daterangeBox"
                     style="display: {{($selectedData['conditions'][0]['timeLimit'] == "Between")?'block':'none'}};">
                    <label for="daterange" class="">Date Range</label>
                    <input type="text" name="conditions[0][dateRange]" id="daterange"
                           class="form-control" data-validation="required"
                           value="{{$selectedData['conditions'][0]['dateRange']}}"/>
                </div>
                <div class="form-group">
                    <label for="timelimit" class="">User Registration Date Limit</label>
                    <select name="conditions[1][registration_timeLimit]"
                            id="registration_timelimit"
                            class="form-control">
                        <option value="">No Limit</option>
                        @foreach($reportsData['timeArray'] as $key => $value)
                            <option
                                value="{{$key}}" {{ ($selectedData['conditions'][1]['registration_timeLimit'] == $key)?"selected='selected'":'' }}>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" id="registration_daterangeBox"
                     style="display: {{($selectedData['conditions'][1]['registration_timeLimit'] == "Between")?'block':'none'}};">
                    <label for="registration_daterange" class="">User Registration Date
                        Custom Range</label>
                    <input autocomplete="off" type="text"
                           name="conditions[1][registration_dateRange]"
                           id="registration_daterange"
                           class="form-control" data-validation="required"
                           value="{{$selectedData['conditions'][1]['registration_dateRange']}}"/>
                </div>
                <div class="form-group">
                    <label for="lastloginlimit" class="">Last Login Date Limit</label>
                    <select name="conditions[2][lastlogin_timeLimit]"
                            id="lastlogin_timelimit"
                            class="form-control">
                        <option value="">No Limit</option>
                        @foreach($reportsData['timeArray'] as $key => $value)
                            <option
                                value="{{$key}}" {{ ($selectedData['conditions'][2]['lastlogin_timeLimit'] == $key)?"selected='selected'":'' }}>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" id="lastlogin_daterangeBox"
                     style="display: {{($selectedData['conditions'][2]['lastlogin_timeLimit'] == "Between")?'block':'none'}};">
                    <label for="lastlogin_daterange" class="">Last Login Date
                        Custom Range</label>
                    <input autocomplete="off" type="text"
                           name="conditions[2][lastlogin_dateRange]"
                           id="lastlogin_daterange"
                           class="form-control" data-validation="required"
                           value="{{$selectedData['conditions'][2]['lastlogin_dateRange']}}"/>
                </div>
                <div class="form-group">
                    <label for="progress_timelimit" class="">Progress Date Limit</label>
                    <select name="conditions[3][progress_timeLimit]"
                            id="progress_timelimit"
                            class="form-control">
                        <option value="">No Limit</option>
                        @foreach($reportsData['timeArray'] as $key => $value)
                            <option
                                value="{{$key}}" {{ ($selectedData['conditions'][3]['progress_timeLimit'] == $key)?"selected='selected'":'' }}>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" id="progress_daterangeBox"
                     style="display: {{($selectedData['conditions'][3]['progress_timeLimit'] == "Between")?'block':'none'}};">
                    <label for="progress_daterange" class="">Progress Date Custom Range</label>
                    <input autocomplete="off" type="text"
                           name="conditions[3][progress_dateRange]"
                           id="progress_daterange"
                           class="form-control" data-validation="required"
                           value="{{$selectedData['conditions'][3]['progress_dateRange']}}"/>
                </div>
                <div class="form-group">
                    <label for="payment_timelimit" class="">Payment Completed Date Limit</label>
                    <select name="conditions[4][payment_timeLimit]"
                            id="payment_timelimit"
                            class="form-control">
                        <option value="">No Limit</option>
                        @foreach($reportsData['timeArray'] as $key => $value)
                            <option
                                value="{{$key}}" {{ ($selectedData['conditions'][4]['payment_timeLimit'] == $key)?"selected='selected'":'' }}>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" id="payment_daterangeBox"
                     style="display: {{($selectedData['conditions'][4]['payment_timeLimit'] == "Between")?'block':'none'}};">
                    <label for="payment_daterange" class="">Payment Completed Date Custom Range</label>
                    <input autocomplete="off" type="text"
                           name="conditions[4][payment_dateRange]"
                           id="payment_daterange"
                           class="form-control" data-validation="required"
                           value="{{$selectedData['conditions'][4]['payment_dateRange']}}"/>
                </div>
                <div class="form-group">
                    <label for="amount">User Course Progress Range</label>
                    <input type="text" id="amount" readonly
                           style="border:0; color:#007bff; font-weight:bold;">
                    <input type="hidden" name="conditions[5][progress]" id="progress"
                           value="{{$selectedData['conditions'][5]['progress']}}">
                </div>
                <div class="form-group">
                    <div id="slider-range"></div>
                </div>
                <div class="form-group">
                    <label for="ref_amount">Referral Progress Range</label>
                    <input type="text" id="ref_amount" readonly
                           style="border:0; color:#007bff; font-weight:bold;">
                    <input type="hidden" name="conditions[6][referral_range]" id="referral_range"
                           value="{{$selectedData['conditions'][6]['referral_range']}}">
                </div>
                <div class="form-group">
                    <div id="ref-slider-range"></div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Select State </label>
                            {!! Form::select('conditions[7][state_id]', $states, $selectedData['conditions'][7]['state_id']??'',['id' => 'state_id', 'class' => 'form-control', 'placeholder' => ""]) !!}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Select City </label>
                            {!! Form::select('conditions[8][city_id]', $cities, $selectedData['conditions'][8]['city_id']??'',['id' => 'city_id', 'class' => 'form-control', 'placeholder' => ""]) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Select Ambassador Level</label>
                            {!! Form::select('conditions[9][ambassador_badge_id]',$ambassadorLevelList, $selectedData['conditions'][9]['ambassador_badge_id']??'',['id' => 'ambassador_badge_id', 'class' => 'form-control', 'placeholder' => "--All--"]) !!}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Assign Badge Date Limit</label>
                            <select name="conditions[10][ambassador_timeLimit]" id="ambassador_timelimit"
                                    class="form-control">
                                <option value="">No Limit</option>
                                @foreach($reportsData['timeArray'] as $key => $value)
                                    <option
                                        value="{{$key}}" {{ ($selectedData['conditions'][10]['ambassador_timeLimit'] == $key)?"selected='selected'":'' }}>{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group" id="ambassador_daterangeBox"
                             style="display: {{($selectedData['conditions'][10]['ambassador_timeLimit'] == "Between")?'block':'none'}};">
                            <label for="ambassador_daterange" class="">Assign Badge Custom Range</label>
                            <input autocomplete="off" type="text"
                                   name="conditions[10][ambassador_dateRange]"
                                   id="ambassador_daterange"
                                   class="form-control" data-validation="required"
                                   value="{{@$selectedData['conditions'][10]['ambassador_dateRange']}}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="abandoned_timelimit" class="">Abandoned Payment Date Limit</label>
                    <select name="conditions[11][abandoned_timeLimit]"
                            id="abandoned_timelimit"
                            class="form-control">
                        <option value="">No Limit</option>
                        @foreach($reportsData['timeArray'] as $key => $value)
                            <option
                                value="{{$key}}" {{ ($selectedData['conditions'][11]['abandoned_timeLimit'] == $key)?"selected='selected'":'' }}>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" id="abandoned_daterangeBox"
                     style="display: {{(@$selectedData['conditions'][11]['abandoned_timeLimit'] == "Between")?'block':'none'}};">
                    <label for="abandoned_daterange" class="">Abandoned Payment Date Custom Range</label>
                    <input autocomplete="off" type="text"
                           name="conditions[11][abandoned_dateRange]"
                           id="abandoned_daterange"
                           class="form-control" data-validation="required"
                           value="{{@$selectedData['conditions'][11]['abandoned_dateRange']}}"/>
                </div>
                @include('report-builder.partials.conditions', [11])
            </div>
        </div>
    </div>
</div>
