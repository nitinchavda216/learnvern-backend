@extends('layout.master')
@section('content')
    <div class="az-content" style="min-height: 85vh;">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title"> Report Name: {{ $report->name }}
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('report-builder') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="bg-gray-200">
                    <div class="pd-30 pd-sm-40 mb-3"
                         style="display: @if(request('selectField') == null) none @else block @endif;" id="filterDiv">
                        {!! Form::open(['method' => 'get', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                        @include('report-builder.partials.filter')
                        <div class="row">
                            <div class="col-md mg-t-10 mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <a href="javascript:;" onclick="this.href=window.location.href.split('?')[0];"
                                   class="btn btn-az-warning btn-warning" id="reset-filters">Reset</a>
                                @if(request('selectField') != null)
                                    <a href="javascript:;" data-toggle="modal" data-target="#addNewReportModal"
                                       class="btn btn-az-primary btn-primary">Add New Report</a>
                                @endif
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right mb-2">
                            <a href="javascript:;" class="btn btn-info btn-sm mt-negative" id="filterButton"
                               title="Filter">
                                <i class="fa fa-filter"></i></a>
                            <a href="javascript:;" class="btn btn-primary btn-sm mt-negative"
                               title="Add To Campaign" id="openCampaignModal">
                                Add To Campaign</a>
                            {{--<a href="{{route('report-builder.export', $report->id)}}"
                               class="btn btn-success btn-sm mt-negative"
                               title="Export Report Data">
                                Export Report Data</a>--}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="dataTableList" class="display nowrap">
                                <thead>
                                <tr>
                                    <th><input type="checkbox" id="checkAll"></th>
                                    @foreach($selectedData['fields'] as $field)
                                        <th>{{$fieldList[$field]['label']}}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('report-builder.modals.add_to_campaign')
    @include('report-builder.modals.check_all_confirmation')
    @include('report-builder.modals.add_new_report')
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        .previous-link {
            width: 70px;
        }

        .next-link {
            width: 50px;
        }

        .select2-container {
            width: 100% !important;
        }
        div.dataTables_wrapper {
            max-width: 100%;
            margin: 0 auto;
        }
        .dataTable thead th{
            background-color: #fff;
            height: 40px;
        }
        .dataTables_scrollHeadInner table.dataTable{
            margin-bottom: 0px;
        }
    </style>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js" integrity="sha512-eYSzo+20ajZMRsjxB6L7eyqo5kuXuS2+wEbbOkpaur+sA2shQameiJiWEzCIDwJqaB0a4a6tCuEvCOBHUg3Skg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ asset("admin/modules/report-builder/index.js") }}"></script>
    <script type="text/javascript">
        var users = [];
        var unselect_users = [];
        var allCheckboxesList = [];
        var checkAllPagesCheckboxes = false;
        var report_id = "{{ isset($report) ? $report['id'] : null }}";
        var arr = new Array();
        var fieldsArray = {!! json_encode($selectedData['fields']) !!};
        arr.push({'data': 'checkbox', 'sortable': false,width:'10%'});
        fieldsArray.forEach(function (a) {
            if(a == "email" || a == "mobile_number" || a == "course_link" || a == "own_referral_code" || a == "key_skill_names" || a == "college_name" || a == "technical_key_skill_names"){
                var obj = {data: a, 'sortable': false,width:'10%'};
            }else {
                var obj = {data: a, 'sortable': true,width:'10%'};
            }

            arr.push(obj);
        });

        $(function () {
            $('.pushNotificationLinkType').change(function() {
                $("#allCourseDropDown").toggle($(this).val() == "specific_course");
                $("#customLinkInput").toggle($(this).val() == "custom_link");
            });
            $('#daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });

            $('#registration_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });

            $('#lastlogin_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });

            $('#progress_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });

            $('#payment_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });

            $('#ambassador_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });

            $('#abandoned_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });

            $('#daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#registration_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });


            $('#lastlogin_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#progress_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#payment_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });


            $('#ambassador_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#abandoned_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });


            $('#daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#registration_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#lastlogin_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#progress_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#payment_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#ambassador_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#abandoned_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });


            $("#timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#daterange').val('');
                    $('#daterangeBox').show();
                }
                else {
                    $('#daterange').val('');
                    $('#daterangeBox').hide();
                }
            });

            $("#registration_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#registration_daterange').val('');
                    $('#registration_daterangeBox').show();
                }
                else {
                    $('#registration_daterange').val('');
                    $('#registration_daterangeBox').hide();
                }
            });

            $("#lastlogin_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#lastlogin_daterange').val('');
                    $('#lastlogin_daterangeBox').show();
                }
                else {
                    $('#lastlogin_daterange').val('');
                    $('#lastlogin_daterangeBox').hide();
                }
            });

            $("#progress_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#progress_daterange').val('');
                    $('#progress_daterangeBox').show();
                }
                else {
                    $('#progress_daterange').val('');
                    $('#progress_daterangeBox').hide();
                }
            });

            $("#payment_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#payment_daterange').val('');
                    $('#payment_daterangeBox').show();
                }
                else {
                    $('#payment_daterange').val('');
                    $('#payment_daterangeBox').hide();
                }
            });

            $("#ambassador_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#ambassador_daterange').val('');
                    $('#ambassador_daterangeBox').show();
                }
                else {
                    $('#ambassador_daterange').val('');
                    $('#ambassador_daterangeBox').hide();
                }
            });

            $("#abandoned_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#abandoned_daterange').val('');
                    $('#abandoned_daterangeBox').show();
                }
                else {
                    $('#abandoned_daterange').val('');
                    $('#abandoned_daterangeBox').hide();
                }
            });

            $("#slider-range").slider({
                range: true,
                min: 0,
                max: 100,
                values: [{{explode('-',$selectedData['conditions'][5]['progress'])[0]

                 }},{{explode('-',$selectedData['conditions'][5]['progress'])[1]}}],
                slide: function (event, ui) {
                    $("#amount").val(ui.values[0] + "% - " + ui.values[1] + "%");
                    $("#progress").val(ui.values[0] + "-" + ui.values[1]);
                }
            });


            $("#ref-slider-range").slider({
                range: true,
                min: 0,
                max: 500,
                values: [{{explode('-',$selectedData['conditions'][6]['referral_range'])[0]}},{{explode('-',$selectedData['conditions'][6]['referral_range'])[1]}}],
                slide: function (event, ui) {
                    $("#ref_amount").val(ui.values[0] + " To " + ui.values[1]);
                    $("#referral_range").val(ui.values[0] + "-" + ui.values[1]);
                }
            });



            $("#amount").val($("#slider-range").slider("values", 0) + "% - " + $("#slider-range").slider("values", 1) + "%");
            $("#ref_amount").val($("#ref-slider-range").slider("values", 0) + " To " + $("#ref-slider-range").slider("values", 1));


            $(".has-success .valid").on('change',function () {
                alert($(this).val());
            });

            /*$('.rdiobox').click(function(e) {
                if($.trim($(this).text()) == 'Push Notifications') {
                    $('#pushNotification').show();
                    $('#singleChunck').hide();
                }else {
                    $('#pushNotification').show();
                    $('#singleChunck').css('display', 'block');
                }
            });
            */
        });
    </script>
    <script src="{{ asset("admin/modules/report-builder/view.js") }}"></script>
@endsection
