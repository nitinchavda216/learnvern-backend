@extends('layout.master')
@section('content')
    <div class="az-content" style="min-height: 85vh;">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title"> Report Builder
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('report-builder.create') }}" class="btn btn-success btn-sm mt-negative">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="dataTableList" class="display dataTable responsive">
                                <thead>
                                <tr>
                                    <th width="50%" class="text-truncate">Title</th>
                                    <th width="50%" class="text-truncate">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($reports)>0)
                                    @foreach($reports as $item)
                                        <tr>
                                            <td width="70%">{{ $item->name }}</td>
                                            <td width="30%">
                                                <a href="{{route('report-builder.viewreport', $item->id)}}"
                                                   class="btn btn-secondary btn-xs" title="View Report Data">
                                                    <i class="fa fa-eye" aria-hidden="true"></i> View Report Data
                                                </a>
                                                <a href="{{route('report-builder.export', $item->id)}}"
                                                   class="btn btn-dark btn-xs" title="Export Report Data">
                                                    <i class="fa fa-file-excel" aria-hidden="true"></i> Export
                                                </a>
                                                <a href="{{ route('report-builder.edit', $item->id) }}"
                                                   class="btn btn-primary btn-xs" title="Edit">
                                                    <i class="fas fa-edit" aria-hidden="true"></i>
                                                </a>
                                                <a href="javascript:;"
                                                   onclick="confirmDelete('{{ route('report-builder.destroy',$item->id) }}')"
                                                   class="btn btn-danger btn-xs" title="Delete">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="2" class="text-center">No record found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataTableList').DataTable({
                responsive: true,
                "ordering": false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                }
            });

        });


    </script>
@endsection

