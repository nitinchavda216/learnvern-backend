@extends('layout.master')
@section('content')
    <div class="az-content" style="min-height: 85vh;">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title">Report Edit
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('report-builder') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="card card-body pd-40">
                        <form method="POST" action="{{ route('report-builder.update', $report->id) }}"
                              accept-charset="UTF-8"
                              class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-row bg-light border p-3 mb-3">
                                <div class="col-md-12">
                                    <div class="position-relative form-group mb-0">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-2 col-md-1">
                                                <label for="name" class="mb-0"><b>Name :</b></label>
                                            </div>
                                            <div class="col-10 col-md-11">
                                                <input name="name" id="name" placeholder="Enter Report Name" type="text"
                                                       class="form-control" data-validation="required"
                                                       data-validation-error-msg="Please Enter Report Title"
                                                       value="{{$report->name}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @include('report-builder.partials.filter')
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group float-right">
                                        <a href="{{route('report-builder')}}" class="mt-2 btn btn-dark">Cancel</a>
                                        <button type="submit" name="submit_save" value="update"
                                                class="mt-2 btn btn-success">Update
                                        </button>
                                        <button type="submit" name="submit_save" value="updateandrun"
                                                class="mt-2 btn btn-primary">Update & Run
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('footer_scripts')


    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset("admin/modules/report-builder/index.js") }}"></script>
    <script type="text/javascript">
        var report_id = "{{ isset($report) ? $report['id'] : null }}";
        $(function () {
            $('#daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });

            $('#registration_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });


            $('#lastlogin_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });

            $('#progress_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });

            $('#payment_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });

            $('#ambassador_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });

            $('#abandoned_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });


            $('#daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#registration_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#lastlogin_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#progress_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#payment_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#ambassador_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#abandoned_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#registration_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#lastlogin_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#progress_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#payment_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#ambassador_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#abandoned_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });


            $("#timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#daterange').val('');
                    $('#daterangeBox').show();
                } else {
                    $('#daterange').val('');
                    $('#daterangeBox').hide();
                }
            });

            $("#registration_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#registration_daterange').val('');
                    $('#registration_daterangeBox').show();
                } else {
                    $('#registration_daterange').val('');
                    $('#registration_daterangeBox').hide();
                }
            });

            $("#lastlogin_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#lastlogin_daterange').val('');
                    $('#lastlogin_daterangeBox').show();
                } else {
                    $('#lastlogin_daterange').val('');
                    $('#lastlogin_daterangeBox').hide();
                }
            });

            $("#progress_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#progress_daterange').val('');
                    $('#progress_daterangeBox').show();
                } else {
                    $('#progress_daterange').val('');
                    $('#progress_daterangeBox').hide();
                }
            });

            $("#payment_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#payment_daterange').val('');
                    $('#payment_daterangeBox').show();
                } else {
                    $('#payment_daterange').val('');
                    $('#payment_daterangeBox').hide();
                }
            });


            $("#ambassador_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#ambassador_daterange').val('');
                    $('#ambassador_daterangeBox').show();
                } else {
                    $('#ambassador_daterange').val('');
                    $('#ambassador_daterangeBox').hide();
                }
            });

            $("#abandoned_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#abandoned_daterange').val('');
                    $('#abandoned_daterangeBox').show();
                } else {
                    $('#abandoned_daterange').val('');
                    $('#abandoned_daterangeBox').hide();
                }
            });

            $("#slider-range").slider({
                range: true,
                min: 0,
                max: 100,
                values: [{{explode('-',$selectedData['conditions'][5]['progress'])[0]

                 }}, {{explode('-',$selectedData['conditions'][5]['progress'])[1]}}],
                slide: function (event, ui) {
                    $("#amount").val(ui.values[0] + "% - " + ui.values[1] + "%");
                    $("#progress").val(ui.values[0] + "-" + ui.values[1]);
                }
            });

            $("#ref-slider-range").slider({
                range: true,
                min: 0,
                max: 500,
                values: [{{explode('-',$selectedData['conditions'][6]['referral_range'])[0]}}, {{explode('-',$selectedData['conditions'][6]['referral_range'])[1]}}],
                slide: function (event, ui) {
                    $("#ref_amount").val(ui.values[0] + " To " + ui.values[1]);
                    $("#referral_range").val(ui.values[0] + "-" + ui.values[1]);
                }
            });

            $("#amount").val($("#slider-range").slider("values", 0) + "% - " + $("#slider-range").slider("values", 1) + "%");
            $("#ref_amount").val($("#ref-slider-range").slider("values", 0) + " To " + $("#ref-slider-range").slider("values", 1));
        });
    </script>
@endsection
