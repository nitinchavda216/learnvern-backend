@extends('layout.master')
@section('content')
    <div class="az-content" style="min-height: 85vh;">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title">Create New Report
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('report-builder') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" id="createReportForm" action="{{ route('report-builder.store') }}"
                                  accept-charset="UTF-8"
                                  class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-row bg-light border p-3 mb-3">
                                    <div class="col-md-12">
                                        <div class="position-relative form-group mb-0">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col-2 col-md-1">
                                                    <label for="name" class="mb-0"><b>Name :</b></label>
                                                </div>
                                                <div class="col-10 col-md-11">
                                                    <input name="name" id="name" placeholder="Enter Report Name"
                                                           type="text"
                                                           class="form-control" data-validation="required"
                                                           data-validation-error-msg="Please Enter Report Title">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h2>Fields Selection</h2>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="card mb-0">
                                                    <div class="card-header bg-info">
                                                        <h6 class="text-white mb-0">Course Progress</h6>
                                                    </div>
                                                    <div class="card-body pre-scrollable">
                                                        @if($reportsData['fieldArray'] && $reportsData['fieldArray']['CourseUser'])
                                                            @foreach($reportsData['fieldArray']['CourseUser'] as $key => $value)
                                                                <div class="position-relative form-check form-check-inline d-block">
                                                                    <label class="form-check-label text-truncate">
                                                                        <input type="checkbox"
                                                                               @if(in_array($key, $autoSelectedFields)) checked
                                                                               @endif
                                                                               name="selectField[]"
                                                                               class="form-check-input"
                                                                               value="{{$key}}"> {{ $reportsData['fieldArray']['CourseUser'][$key]['label']}}
                                                                    </label>
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card mb-0">
                                                    <div class="card-header bg-info">
                                                        <h6 class="text-white mb-0">Course Information</h6>
                                                    </div>
                                                    <div class="card-body pre-scrollable">
                                                        @if($reportsData['fieldArray'] && $reportsData['fieldArray']['Course'])
                                                            @foreach($reportsData['fieldArray']['Course'] as $key => $value)
                                                                <div class="position-relative form-check form-check-inline d-block">
                                                                    <label class="form-check-label text-truncate">
                                                                        <input type="checkbox"
                                                                               @if(in_array($key, $autoSelectedFields)) checked
                                                                               @endif
                                                                               name="selectField[]"
                                                                               class="form-check-input"
                                                                               value="{{$key}}"> {{ $reportsData['fieldArray']['Course'][$key]['label']}}
                                                                    </label>
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card mb-0">
                                                    <div class="card-header bg-info">
                                                        <h6 class="text-white mb-0">User Information</h6>
                                                    </div>
                                                    <div>
                                                        <div class="card-body pre-scrollable">
                                                            @if($reportsData['fieldArray'] && $reportsData['fieldArray']['User'])
                                                                @foreach($reportsData['fieldArray']['User'] as $key => $value)
                                                                    <div class="position-relative form-check form-check-inline d-block">
                                                                        <label class="form-check-label text-truncate">
                                                                            <input type="checkbox"
                                                                                   @if(in_array($key, $autoSelectedFields)) checked
                                                                                   @endif
                                                                                   name="selectField[]"
                                                                                   class="form-check-input"
                                                                                   value="{{$key}}"> {{ $reportsData['fieldArray']['User'][$key]['label']}}
                                                                        </label>
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="main-card mb-3 card">
                                            <div class="card-header alert-primary">Conditions</div>
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="timelimit" class="">Course Enrollment Date Limit</label>
                                                    <select name="conditions[0][timeLimit]" id="timelimit"
                                                            class="form-control">
                                                        <option value="">No Limit</option>
                                                        @foreach($reportsData['timeArray'] as $key => $value)
                                                            <option value="{{$key}}">{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group" id="daterangeBox">
                                                    <label for="daterange" class="">Enrollment Date Custom Range</label>
                                                    <input autocomplete="off" type="text"
                                                           name="conditions[0][dateRange]"
                                                           id="daterange" data-validation="required"
                                                           class="form-control" value=""/>
                                                </div>

                                                <div class="form-group">
                                                    <label for="timelimit" class="">User Registration Date Limit</label>
                                                    <select name="conditions[1][registration_timeLimit]"
                                                            id="registration_timelimit"
                                                            class="form-control">
                                                        <option value="">No Limit</option>
                                                        @foreach($reportsData['timeArray'] as $key => $value)
                                                            <option value="{{$key}}">{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group" id="registration_daterangeBox">
                                                    <label for="registration_daterange" class="">User Registration Date
                                                        Custom Range</label>
                                                    <input autocomplete="off" type="text"
                                                           name="conditions[1][registration_dateRange]"
                                                           id="registration_daterange" data-validation="required"
                                                           class="form-control" value=""/>
                                                </div>


                                                <div class="form-group">
                                                    <label for="lastloginlimit" class="">Last Login Date Limit</label>
                                                    <select name="conditions[2][lastlogin_timeLimit]"
                                                            id="lastlogin_timelimit"
                                                            class="form-control">
                                                        <option value="">No Limit</option>
                                                        @foreach($reportsData['timeArray'] as $key => $value)
                                                            <option value="{{$key}}">{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group" id="lastlogin_daterangeBox">
                                                    <label for="lastlogin_daterange" class="">Last Login Date
                                                        Custom Range</label>
                                                    <input autocomplete="off" type="text"
                                                           name="conditions[2][lastlogin_dateRange]"
                                                           id="lastlogin_daterange" data-validation="required"
                                                           class="form-control"
                                                           value=""/>
                                                </div>


                                                <div class="form-group">
                                                    <label for="progress_timelimit" class="">Progress Date Limit</label>
                                                    <select name="conditions[3][progress_timeLimit]"
                                                            id="progress_timelimit"
                                                            class="form-control">
                                                        <option value="">No Limit</option>
                                                        @foreach($reportsData['timeArray'] as $key => $value)
                                                            <option value="{{$key}}">{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group" id="progress_daterangeBox">
                                                    <label for="progress_daterange" class="">Progress Date Custom
                                                        Range</label>
                                                    <input autocomplete="off" type="text"
                                                           name="conditions[3][progress_dateRange]"
                                                           id="progress_daterange" data-validation="required"
                                                           class="form-control"
                                                           value=""/>
                                                </div>


                                                <div class="form-group">
                                                    <label for="payment_timelimit" class="">Payment Completed Date
                                                        Limit</label>
                                                    <select name="conditions[4][payment_timeLimit]"
                                                            id="payment_timelimit"
                                                            class="form-control">
                                                        <option value="">No Limit</option>
                                                        @foreach($reportsData['timeArray'] as $key => $value)
                                                            <option value="{{$key}}">{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group" id="payment_daterangeBox">
                                                    <label for="payment_daterange" class="">Payment Completed Date
                                                        Custom Range</label>
                                                    <input autocomplete="off" type="text"
                                                           name="conditions[4][payment_dateRange]"
                                                           id="payment_daterange" data-validation="required"
                                                           class="form-control"
                                                           value=""/>
                                                </div>


                                                <div class="form-group">
                                                    <label for="amount">User Course Progress Range</label>
                                                    <input type="text" id="amount" readonly
                                                           style="border:0; color:#007bff; font-weight:bold;">
                                                    <input type="hidden" name="conditions[5][progress]" id="progress"
                                                           value="0-100">
                                                </div>

                                                <div class="form-group">
                                                    <div id="slider-range"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="ref_amount">Referral Progress Range</label>
                                                    <input type="text" id="ref_amount" readonly
                                                           style="border:0; color:#007bff; font-weight:bold;">
                                                    <input type="hidden" name="conditions[6][referral_range]"
                                                           id="referral_range"
                                                           value="0-500">
                                                </div>

                                                <div class="form-group">
                                                    <div id="ref-slider-range"></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Select State </label>
                                                            {!! Form::select('conditions[7][state_id]', $states, $selectedData['conditions'][7]['state_id']??'',['id' => 'state_id', 'class' => 'form-control', 'placeholder' => ""]) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Select City </label>
                                                            {!! Form::select('conditions[8][city_id]', $cities, $selectedData['conditions'][8]['city_id']??'',['id' => 'city_id', 'class' => 'form-control', 'placeholder' => ""]) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Select Ambassador Level</label>
                                                            {!! Form::select('conditions[9][ambassador_badge_id]', $ambassadorLevelList, $selectedData['conditions'][9]['ambassador_badge_id']??'',['id' => 'ambassador_badge_id', 'class' => 'form-control', 'placeholder' => "--All--"]) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Assign Badge Date Limit</label>
                                                            <select name="conditions[10][ambassador_timeLimit]" id="ambassador_timelimit" class="form-control">
                                                                <option value="">No Limit</option>
                                                                @foreach($reportsData['timeArray'] as $key => $value)
                                                                    <option value="{{$key}}">{{$value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group" id="ambassador_daterangeBox">
                                                            <label for="ambassador_daterange" class="">Assign Badge Custom Range</label>
                                                            <input autocomplete="off" type="text"
                                                                   name="conditions[10][ambassador_dateRange]"
                                                                   id="ambassador_daterange"
                                                                   class="form-control"
                                                                   value=""/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="abandoned_timelimit" class="">Abandoned Payment Date Limit</label>
                                                    <select name="conditions[11][abandoned_timeLimit]"
                                                            id="abandoned_timelimit"
                                                            class="form-control">
                                                        <option value="">No Limit</option>
                                                        @foreach($reportsData['timeArray'] as $key => $value)
                                                            <option value="{{$key}}">{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group" id="abandoned_daterangeBox">
                                                    <label for="abandoned_daterange" class="">Abandoned Payment Date Custom Range</label>
                                                    <input autocomplete="off" type="text"
                                                           name="conditions[4][abandoned_dateRange]"
                                                           id="abandoned_daterange"
                                                           class="form-control" data-validation="required"
                                                           value=""/>
                                                </div>
                                                @include('report-builder.partials.conditions', [11])
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group float-right">
                                            <button type="submit" name="submit_save" value="save"
                                                    class="mt-2 btn btn-success">Save
                                            </button>
                                            <button type="submit" name="submit_save" value="saveandrun"
                                                    class="mt-2 btn btn-primary">Save & Run
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset("admin/modules/report-builder/index.js") }}"></script>
    <script type="text/javascript">
        var report_id = null;
        $(document).ready(function () {

            $('#daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });


            $('#registration_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });


            $('#lastlogin_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });


            $('#progress_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });

            $('#payment_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });

            $('#ambassador_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });

            $('#abandoned_daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'YYYY-MM-DD'
                }
            });

            $('#daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#registration_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#lastlogin_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#progress_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#payment_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#ambassador_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#abandoned_daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#registration_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#lastlogin_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#progress_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#payment_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#ambassador_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#abandoned_daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            $('#daterangeBox').hide();
            $('#registration_daterangeBox').hide();
            $('#lastlogin_daterangeBox').hide();
            $('#progress_daterangeBox').hide();
            $('#payment_daterangeBox').hide();
            $('#ambassador_daterangeBox').hide();
            $('#abandoned_daterangeBox').hide();


            $("#timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#daterange').val('');
                    $('#daterangeBox').show();
                }
                else {
                    $('#daterange').val('');
                    $('#daterangeBox').hide();
                }
            });

            $("#registration_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#registration_daterange').val('');
                    $('#registration_daterangeBox').show();
                }
                else {
                    $('#registration_daterange').val('');
                    $('#registration_daterangeBox').hide();
                }
            });

            $("#lastlogin_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#lastlogin_daterange').val('');
                    $('#lastlogin_daterangeBox').show();
                }
                else {
                    $('#lastlogin_daterange').val('');
                    $('#lastlogin_daterangeBox').hide();
                }
            })

            $("#progress_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#progress_daterange').val('');
                    $('#progress_daterangeBox').show();
                }
                else {
                    $('#progress_daterange').val('');
                    $('#progress_daterangeBox').hide();
                }
            });

            $("#payment_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#payment_daterange').val('');
                    $('#payment_daterangeBox').show();
                }
                else {
                    $('#payment_daterange').val('');
                    $('#payment_daterangeBox').hide();
                }
            });

            $("#ambassador_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#ambassador_daterange').val('');
                    $('#ambassador_daterangeBox').show();
                }
                else {
                    $('#ambassador_daterange').val('');
                    $('#ambassador_daterangeBox').hide();
                }
            });

            $("#abandoned_timelimit").change(function () {
                if ($(this).val() == "Between") {
                    $('#abandoned_daterange').val('');
                    $('#abandoned_daterangeBox').show();
                }
                else {
                    $('#abandoned_daterange').val('');
                    $('#abandoned_daterangeBox').hide();
                }
            });

            $("#slider-range").slider({
                range: true,
                min: 0,
                max: 100,
                values: [0, 100],
                slide: function (event, ui) {
                    $("#amount").val(ui.values[0] + "% - " + ui.values[1] + "%");
                    $("#progress").val(ui.values[0] + "-" + ui.values[1]);
                }
            });

            $("#ref-slider-range").slider({
                range: true,
                min: 0,
                max: 500,
                values: [0, 500],
                slide: function (event, ui) {
                    $("#ref_amount").val(ui.values[0] + " - " + ui.values[1]);
                    $("#referral_range").val(ui.values[0] + "-" + ui.values[1]);
                }
            });

            $("#amount").val($("#slider-range").slider("values", 0) + "% - " + $("#slider-range").slider("values", 1) + "%");
            $("#ref_amount").val($("#ref-slider-range").slider("values", 0) + " To " + $("#ref-slider-range").slider("values", 1));

        });
    </script>
@endsection
