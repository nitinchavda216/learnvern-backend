<div class="modal fade" id="addNewReportModal" role="dialog" aria-labelledby="addNewReportModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addNewReportModalLabel">Add New Report</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => 'report-builder.store_new', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
            <div class="modal-body">
                <div class="mb-3">
                    <input type="hidden" name="conditions" value="{{ json_encode($selectedData['conditions']) }}">
                    <input type="hidden" name="fields" value="{{ json_encode($selectedData['fields']) }}">
                    <div class="form-group">
                        <label>Report Name</label>
                        {!! Form::text('name', old('name'), ['class'=>'form-control', 'placeholder' => 'Enter Report Name', 'data-validation' => 'required']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>