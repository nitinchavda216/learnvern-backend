<div class="modal fade" id="addToCampaignModal" role="dialog" aria-labelledby="addToCampaignModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addToCampaignModalLabel">Add Campaign</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => 'campaign.store', 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'add-campaign', 'files' => true]) !!}
            <div class="modal-body">
                <div class="mb-3">
                    <input type="hidden" name="report_id" value="{{ $report['id'] }}">
                    <input type="hidden" id="course_user_ids" name="course_user_ids" value="">
                    <input type="hidden" id="unselect_course_user_ids"  name="unselect_course_user_ids" value="">
                    <input type="hidden" name="conditions" value="{{ isset($selectedData['conditions'])?json_encode($selectedData['conditions']):'' }}"
                           id="conditionInput">
                    <input type="hidden" name="fields" value="{{ isset($selectedData['fields'])?json_encode($selectedData['fields']):'' }}" id="fieldsInput">
                    <input type="hidden" name="checkAllUsers" id="checkAllUsers" value="false">
                    <input type="hidden" name="total_record_count" id="total_record_count" value="0"/>
                    <input type="hidden" name="number_of_user_campaign" id="number_of_user_campaign" value="0"/>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Campaign Name</label>
                                {!! Form::text('name', old('name'), ['class'=>'form-control', 'id' => 'name', 'placeholder' => 'Campaign Name', 'data-validation' => 'required']) !!}
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>General Marketing Goal</label>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label class="rdiobox">
                                            <input name="marketing_goal" type="radio" value="revenue" checked data-validation="required">
                                            <span>Revenue</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="rdiobox">
                                            <input name="marketing_goal" type="radio" value="growth">
                                            <span>Growth</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="rdiobox">
                                            <input name="marketing_goal" type="radio" value="referral">
                                            <span>Referral</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group mt-3">
                                <label class="ckbox">
                                    <input type="checkbox" id="is_allow_duplicate" name="is_allow_duplicate" value="1"><span>Calling User Action Allow Duplicate User<br/><br/> In Same Campaign Goal?</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Campaign Action</label>
                                <div class="row">
                                    @foreach(\App\Models\Campaign::ACTIONS as $key => $value)
                                        <div class="col-sm-3">
                                            <label class="rdiobox">
                                                <input name="action" id="action" type="radio" value="{{$key}}" data-validation="required">
                                                <span>{{$value}}</span>
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-12" id="pushNotification" style="display: none">
                            <div class="form-group">
                                <label>Push Notification</label>
                                <div class="row">
                                    <div class="col-12">
                                        <label class="form-text">
                                            Title:<br/>
                                            <input class="form-control" id="pushTitle"  name="push_notification_details[title]" type="text" placeholder="Title" value="" data-validation ='required'>
                                        </label>
                                    </div>
                                    <div class="col-12">
                                        <label class="form-text">
                                            Description:<br/>
                                            <input class="form-control" id="pushDescription" name="push_notification_details[description]" type="text" placeholder="description" value="" data-validation ='required'>
                                        </label>
                                    </div>
                                </div>
                                <div class="row mg-t-10">
                                    <div class="col-lg-4 mg-t-20 mg-lg-t-0">
                                        <label class="rdiobox">
                                            <input name="push_notification_details[link_type]" class="pushNotificationLinkType" type="radio" value="enroll_course" checked="checked">
                                            <span>Enrolled Course Link</span>
                                        </label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="rdiobox">
                                            <input name="push_notification_details[link_type]" class="pushNotificationLinkType" type="radio" value="specific_course">
                                            <span>Specific Single Course Link</span>
                                        </label>
                                    </div>
                                    <div class="col-lg-4 mg-t-20 mg-lg-t-0">
                                        <label class="rdiobox">
                                            <input name="push_notification_details[link_type]" class="pushNotificationLinkType" type="radio" value="custom_link">
                                            <span>Custom Link</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row" id="allCourseDropDown" style="display: none">
                                    <div class="col-12">
                                        <label class="form-text">
                                            Select Course:<br/>
                                            {!! Form::select('push_notification_details[course_link_id]', $all_course_titles, null, ['class' => 'form-control', 'data-validation' => 'required']) !!}
                                        </label>
                                    </div>
                                </div>
                                <div class="row" id="customLinkInput" style="display: none">
                                    <div class="col-12">
                                        <label class="form-text">
                                            Link: <small class="text-danger">Currently only referral link allowed</small><br/>
                                            <input class="form-control" name="push_notification_details[custom_link]" type="text" placeholder="Enter Custom Link" value="{{ env('FCM_DYNAMIC_LINK_DOMAIN').'?type=referral' }}" data-validation ='required'>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <label class="form-text">
                                            Image:<br/>
                                            <input class="form-control file-upload" id="pushImg" name="push_notification_details[image]" type="file"  value="">
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6" id="assignUserMultipleForCalling" style="display: none">
                            <div class="form-group">
                                <label>Assign To Multiple User </label>
                                <div class="row">

                                    <div class="col-sm-4">
                                        <label class="rdiobox">
                                            <input name="chunk_type" type="radio" value="1">
                                            <span>Yes</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="rdiobox">
                                            <input name="chunk_type" type="radio" value="0" checked>
                                            <span>No</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="row mb-3" id="filterTotalDiv" style="display: none;">
                        <div class="col-6"><h5>Filter Total <span id="number_of_user_campaign_span"></span> Records</h5></div>
                    </div>

                    <div id="singleChunck">
                                <div class="form-group">
                                    <label>Assign To</label>
                                    {!! Form::select('assign_to', $adminNames , old('assign_to'), ['class'=>'form-control', 'id' => 'assign_to', 'placeholder' => '', 'data-validation' => 'required']) !!}
                                </div>

                    </div>
                    <div id="multipleChunck" style="display: none;">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Number Of User</label>
                                    <select name="number_of_user_assign" class="form-control" id="number_of_user_assign" data-validation="required">
                                        <option value=""></option>
                                        @for($i= 1; $i<=10; $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <table id="assignTable" class="dataTable">
                                        <tr>
                                            <th width="50%">Assign To</th>
                                            <th width="50%">Number Of User Assign</th>
                                        </tr>
                                        <tr style="display: none">
                                            <td>
                                                {!! Form::select('chunk_distribute[name][]', $adminNames , '', ['class'=>'form-control', 'placeholder' => '', 'data-validation' => 'required']) !!}
                                            </td>
                                            <td>
                                                {!! Form::text('chunk_distribute[qty][]', '', ['class'=>'form-control distributeQty', 'placeholder' => 'Value', 'data-validation' => 'required']) !!}
                                            </td>
                                        </tr>
                                </table>
                            </div>
                        </div>


                    </div>


                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
