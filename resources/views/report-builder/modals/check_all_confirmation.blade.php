<div class="modal fade modal-form" id="checkAllConfirmationModal" tabindex="-1" role="dialog"
     aria-labelledby="xyzModal"
     aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Choose the Option</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-sm-12">
                    <input type="radio" name="check_page" value="one" id="one" checked> <label for="one">
                        Select the records from the current page only
                    </label>
                </div>
                <div class="col-sm-12">
                    <input type="radio" name="check_page" value="all" id="all"> <label for="all">
                        Check all the records from all the pages
                    </label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">Cancel
                </button>
                <button type="submit" id="submitCheckboxEvent"
                        class="btn btn-success">Submit
                </button>
            </div>
        </div>
    </div>
</div>