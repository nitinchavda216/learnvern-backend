@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Admin Users Role
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('admin_role.create') }}" class="btn btn-success btn-sm mt-negative"
                           title="Add New">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Role Type</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        @if($admin_role)
                            @foreach($admin_role as $data)
                                <tbody>
                                <tr>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ ucwords(str_slug($data->user_type)) }}</td>
                                    <td><a href="{{route('admin_role.edit',$data->id) }}"
                                           class="btn btn-primary btn-xs d-inline">
                                            <i class="fas fa-edit"></i>
                                        </a>

                                    </td>
                                </tr>
                                </tbody>
                            @endforeach
                        @endif
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataTableList').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        });

    </script>
@endsection
