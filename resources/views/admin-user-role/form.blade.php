<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Role Title</label>
            {!! Form::text('name', $adminRole->name ?? old('role_title'), ['class'=>'form-control', 'id' => 'name', 'data-validation' => 'required']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-lg-4 mg-t-20 mg-lg-t-0">
                <label class="rdiobox">
                    <input name="user_type" type="radio" value="crm"
                           @if(isset($adminRole->user_type) && $adminRole->user_type=='crm') checked="checked" @endif>
                    <span>CRM User</span>
                </label>
            </div>
            <div class="col-lg-4 mg-t-20 mg-lg-t-0">
                <label class="rdiobox">
                    <input name="user_type" type="radio" value="lms"
                           @if(isset($adminRole->user_type) && $adminRole->user_type=='lms') checked="checked" @endif>
                    <span>LMS User</span>
                </label>
            </div>
            <div class="col-lg-4 mg-t-20 mg-lg-t-0">
                <label class="rdiobox">
                    <input name="user_type" type="radio"
                           @if(isset($adminRole->user_type) && $adminRole->user_type=='super_admin') checked="checked"
                           @endif @if(!isset($adminRole->user_type)) checked="checked" @endif value="super_admin">
                    <span>Super Admin</span>
                </label>
            </div>
        </div>
    </div>
</div>
<div class="row" id="crmModulesSection"
     style="display: {{ isset($adminRole->user_type)&& $adminRole->user_type == "crm" ?'block':'none' }}">
    <div class="col-md-6 col-sm-12">
        <div class="card">
            <div class="card-header">CRM Modules</div>
            <div class="card-body category-tree p-0">
                <div class="alert alert-info p-2" role="alert">
                    <strong>Heads up!</strong> <b>Is CRM Manager</b> setting will allow the associated users to track, monitor and evaluate the performance of the other CRM team users as well. They will be accessible to all the campaigns in the system.
                </div>
                <div class="row px-2">
                    <div class="col-md-12">
                        <label for="is_crm_manager" class="ckbox">
                            <input type="checkbox" name="is_crm_manager" id="is_crm_manager" @if(isset($adminRole) && $adminRole->is_crm_manager ==1) checked="checked" @endif>
                            <span>Is CRM Manager?</span>
                        </label>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12 py-2">
                        <ul class="tree_structure">
                            @foreach($crm_modules as $module)
                                <li>
                                    {!! Form::checkbox('crm_modules[]', $module->module_keys,(isset($module->module_keys) && !empty($current_selected_modules) && in_array($module->module_keys,$current_selected_modules)),['class' => 'parent_module']) !!}
                                    {{ $module->description }}
                                    @if(count($module->child) > 0)
                                        <ul>
                                            @foreach($module->child as $child)
                                                <li>
                                                    {!! Form::checkbox('crm_modules[]',$child->module_keys,(isset($child->module_keys) && !empty($current_selected_modules) && in_array($child->module_keys,$current_selected_modules)),['class' => 'child_module']) !!}
                                                    {{ $child->description }}
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" id="lmsModulesSection"
     style="display: {{ isset($adminRole->user_type)&& $adminRole->user_type == "lms" ?'block':'none' }}">
    <div class="col-md-6 col-sm-12">
        <div class="card">
            <div class="card-header">
                LMS Modules
            </div>
            <div class="card-body category-tree p-2">
                <ul class="tree_structure">
                    @foreach($lms_modules as $module)
                        <li>
                            {!! Form::checkbox('lms_modules[]', $module->module_keys,(isset($module->module_keys) && !empty($current_selected_modules) && in_array($module->module_keys,$current_selected_modules)),['class' => 'parent_module']) !!}
                            {{ $module->description }}
                            @if(count($module->child) > 0)
                                <ul>
                                    @foreach($module->child as $child)
                                        <li>
                                            {!! Form::checkbox('lms_modules[]',$child->module_keys,(isset($child->module_keys) && !empty($current_selected_modules) && in_array($child->module_keys,$current_selected_modules)),['class' => 'child_module']) !!}
                                            {{ $child->description }}
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>


@section('css')
    <link href="{{asset('admin/lib/tree/tree.css')}}" rel="stylesheet">
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('admin/lib/tree/tree.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('input[type=checkbox]').click(function () {
                var self = $(this);
                if (this.checked) {
                    self.parents('li').children('input[type=checkbox]').prop('checked', true);
                }
                self.parent().find('input[type=checkbox]').prop('checked', this.checked);
            });


            $(document).on("change", "input[name='user_type']", function () {
                if ($(this).val() == "lms") {
                    $("#lmsModulesSection").show();
                    $("#crmModulesSection").hide();
                }
                else if ($(this).val() == "crm") {
                    $("#lmsModulesSection").hide();
                    $("#crmModulesSection").show();
                }
                else if ($(this).val() == "super_admin") {
                    $("#lmsModulesSection").hide();
                    $("#crmModulesSection").hide();
                }
            });
        })
    </script>
@endsection
