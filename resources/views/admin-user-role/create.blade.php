@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Create Admin User Role
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('admin_role') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    {!! Form::open(['route'=>'admin_role.store','method' => 'POST', 'enctype'=> "multipart/form-data", 'files' => true, 'id' => "adminRoleForm"]) !!}
                    <div class="card">
                        <div class="card-body">
                            @include('admin-user-role.form')
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-az-primary">{{ isset($adminRole) ? "Update" : "Submit" }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
