<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Meta -->
    <meta name="description" content="LearnVern Admin Panel">
    <title>LearnVearn</title>
    <!-- vendor css -->
    <link href="{{ asset('admin/css/azia.css')}}" rel="stylesheet"/>
    <link href="{{ asset('admin/css/custom.css')}}" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/theme-default.min.css"
          rel="stylesheet"/>
</head>
<body class="az-body">
<div class="az-signin-wrapper login-background" >
    <div class="az-card-signin" style="background-color: #f8f8f8; color:#000;">
        <h1 class="az-logo text-center">
            <img src="{{ asset('admin/img/logo-blue.png') }}" alt="" class="img-fluid">
            <small>Administration Panel</small>
        </h1>
        @include('layout.partials.flash_messages')
        <div class="az-signin-header">
            <form action="{{route('admin.authenticate')}}" method="post">
                @csrf
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" class="form-control" placeholder="Enter your email" value="" name="email"
                           data-validation="email">
                </div><!-- form-group -->
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" placeholder="Enter your password" value=""
                           name="password"
                           data-validation="required length" data-validation-length="min6">
                </div><!-- form-group -->
                <button class="btn btn-az-primary btn-block">Sign In</button>
            </form>
        </div><!-- az-signin-header -->
        <div class="az-signin-footer">
            <p class="text-center"><a href="{{ route('forgot_password') }}" class="text-center">Forgot
                    password?</a></p>
        </div><!-- az-signin-footer -->
    </div><!-- az-card-signin -->
</div><!-- az-signin-wrapper -->
<script type="text/javascript" src="{{asset('admin/lib/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('admin/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{asset('admin/js/azia.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script type="text/javascript">
    $.validate();
</script>
</body>
</html>
