@if($user->referred_count >= 25)
    <a href="{{ route('referral-points.download-ambassador-certificate', [$user->id, 25]) }}" class="btn btn-info btn-xs">
        <i class="fa fa-download"></i> AMB
    </a>
@endif
@if($user->referred_count >= 50)
    <a href="{{ route('referral-points.download-ambassador-certificate', [$user->id, 50]) }}" class="btn btn-dark btn-xs">
        <i class="fa fa-download"></i> AMB++
    </a>
@endif
@if($user->referred_count >= 250)
    <a href="{{ route('referral-points.download-ambassador-certificate', [$user->id, 250]) }}" class="btn btn-primary btn-xs">
        <i class="fa fa-download"></i> AMB-PRO
    </a>
@endif
@if($user->referred_count < 25)
    - - -
@endif
