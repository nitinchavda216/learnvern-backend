@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title">Ambassador Report
                    <span class="pull-right d-inline-block float-right">
                        <a href="javascript:;" class="btn btn-success btn-sm mt-negative"
                           onclick="this.href='ambassador-report/export?ambassador_badge_id=' + document.getElementById('ambassador_badge_id').value + '&searchData=' + document.getElementById('searchData').value + '&partner_user_id=' + document.getElementById('partner_user_id').value + '&ambassador_badge_date=' + document.getElementById('ambassador_badge_date').value">
                             <i class="fa fa-download" aria-hidden="true"></i> Export
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Search:</label>
                                <input name="searchData" id="searchData" type="text" class="form-control"
                                       placeholder="Search">
                            </div>
                            @if(session('partner_id') == 1)
                                <div class="col-md-3">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Partners:</label>
                                    {!! Form::select('partner_user_id', $partners, null,['id' => 'partner_user_id', 'class' => 'form-control select2']) !!}
                                </div>
                            @endif
                            <div class="col-md-3">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Ambassador Level:</label>
                                {!! Form::select('ambassador_badge_id', $ambassador_levels, [],['id' => 'ambassador_badge_id', 'class' => 'form-control select2', 'placeholder' => ""]) !!}
                            </div>
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Assign Badge Date Range:</label>
                                <input name="ambassador_badge_date" id="ambassador_badge_date" type="text" class="form-control"
                                       autocomplete="off" placeholder="Select Date">
                            </div>
                            <div class="col-md mg-t-10 mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}

                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header bg-gray-200 text-center">
                                    <h5>CAMPAIGN AMBASSADOR</h5>
                                </div><!-- card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <h3 id="campaign_ambassador"> - </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header bg-gray-200 text-center">
                                    <h5>ORGANIC AMBASSADOR</h5>
                                </div><!-- card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <h3 id="organic_ambassador"> - </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>User Details</th>
                            <th>Own Referral Code</th>
                            <th>Referrals</th>
                            <th>NSDC Purchase</th>
                            <th>Level Name</th>
                            <th>Location</th>
                            <th>Education</th>
                            <th>College</th>
                            <th>Last Referral Date</th>
                            <th>Ambassador Certificate</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">
        $('[data-toggle="tooltip"]').tooltip();
        $('#ambassador_badge_date').daterangepicker({
            autoUpdateInput: true,
            startDate: moment().subtract(6, 'days'),
            endDate: moment(),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD-MM-YYYY'
            },
            ranges: {
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
        $('#ambassador_badge_date').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        });
        $('#ambassador_badge_date').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        if ($('#dataTableList').length) {
            var $dataTableList = $('#dataTableList').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                bFilter: false,
                "aaSorting": [],
                ajax: {
                    url: $app_url + '/ambassador-report/getAjaxData',
                    data: function (d) {
                        d.searchData = $('#searchData').val();
                        d.ambassador_badge_id = $('#ambassador_badge_id').val();
                        d.ambassador_badge_date = $('#ambassador_badge_date').val();
                        d.partner_user_id = $('#partner_user_id').val();
                    }
                },
                "columns": [
                    {data: 'user_details', 'sortable': false, 'width': "10%"},
                    {data: 'own_referral_code', 'sortable': false, 'width': "10%"},
                    {data: 'referral', 'sortable': false, 'width': "10%"},
                    {data: 'nsdc_referred_count', 'sortable': false, 'width': "10%"},
                    {data: 'level_name', 'sortable': false, 'width': "10%"},
                    {data: 'location', 'sortable': false, 'width': "10%"},
                    {data: 'education', 'sortable': false, 'width': "10%"},
                    {data: 'college', 'sortable': false, 'width': "10%"},
                    {data: 'last_referral_date', 'sortable': false, 'width': "10%"},
                    {data: 'actions', 'sortable': false, 'width': "20%"}
                ],

                select: true,
                bStateSave: true,
                fnStateSave: function (settings, data) {
                    localStorage.setItem("dataTables_state", JSON.stringify(data));
                },
                fnStateLoad: function (settings) {
                    return JSON.parse(localStorage.getItem("dataTables_state"));
                },
                drawCallback: function (settings) {
                $('body').tooltip({selector: '[data-toggle="tooltip"]'});
                },

                "preDrawCallback": function( settings ) {
                    $("#organic_ambassador").html('-');
                    $("#campaign_ambassador").html('-');
                },
                "drawCallback": function( settings ) {
                    $("#organic_ambassador").html(settings.json.countForCampaignOrOrganicResult.organic_ambassador);
                    $("#campaign_ambassador").html(settings.json.countForCampaignOrOrganicResult.campaign_ambassador);
                }
            });

            $('#search-form').on('submit', function (e) {
                $dataTableList.draw();
                e.preventDefault();
            });

            $('#reset-filters').on('click', function () {
                $('#searchData').val('');
                $('#ambassador_badge_id').val('').trigger('change');
                $('#partner_user_id').val(1).trigger('change');
                $('#ambassador_badge_date').val('');
                $dataTableList.draw();
            });

        }
    </script>
@endsection

