<script type="text/javascript" src="{{asset('admin/lib/jquery/jquery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"
        integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA=="
        crossorigin="anonymous"></script>
<script type="text/javascript" src="{{asset('admin/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
{{--<script type="text/javascript" src="{{asset('admin/lib/ionicons/ionicons.js')}}"></script>--}}
<script type="text/javascript" src="{{asset('admin/lib/peity/jquery.peity.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script type="text/javascript"
        src="{{asset('admin/lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script type="text/javascript"
        src="{{asset('admin/lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/lib/dropify/js/dropify.min.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script type="text/javascript" src="{{asset('admin/js/azia.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/custom.js')}}"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.css"
      integrity="sha512-uq8QcHBpT8VQcWfwrVcH/n/B6ELDwKAdX4S/I3rYSwYldLVTs7iII2p6ieGCM13QTPEKZvItaNKBin9/3cjPAg=="
      crossorigin="anonymous" referrerpolicy="no-referrer"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.min.js"></script>
<style>
    #searchusersdetailscontainer .ui-autocomplete{
        padding:0px!important;
        overflow-x: hidden;
        border-bottom: 1px solid #cccccc;
        height: auto!important;
        max-height:300px!important;
        font-size: 14px!important;
    }
    #searchusersdetailscontainer .table th, .table td {
        padding: 9px 15px;
        line-height: 1.5;
    }
    #searchusersdetailscontainer .ui-menu-item{
        margin-bottom: 0px!important;
        margin-top: 0px!important;
    }
    #searchusersdetailscontainer .ui-menu-item-wrapper:hover {
        background-color: #fff;
    }
    .autocompleteTr{
        cursor: pointer;
    }
</style>
<script>

    $(document).ready(function() {

        @if (getCurrentAdmin()->relatedRole->is_super_admin == 1)
        var autocompleteUrl = $app_url+"/users/detail/";
        @else
        var autocompleteUrl = $app_url+"/crm/campaign-user/";
        @endif

        $.ui.autocomplete.prototype._renderMenu = function (ul, items) {
            var self = this;
            //table definitions
            ul.append("<div class='table-responsive'><table class='table table-bordered mg-b-0'><thead><tr><th>Name</th><th>Email</th><th>Mobile Number</th></tr></thead><tbody></tbody></table></div>");
            $.each(items, function (index, item) {
                self._renderItemData(ul, ul.find("table tbody"), item);
            });
        };

        $.ui.autocomplete.prototype._renderItem = function (table, item) {
            if (item.value == '') {
                return '';
            }

            return $("<tr class='ui-menu-item autocompleteTr' role='presentation' data-href='" + autocompleteUrl + item.id + "'></tr>")
                .data("item.autocomplete", item)
                .append("<td>" + item.value + "</td>" + "<td>" + item.email + "</td>" + "<td>" + item.mobilenumber + "</td>")
                .appendTo(table);
        };

        $.ui.autocomplete.prototype._renderItemData = function (ul, table, item) {

            if (item.id != undefined && item.id != "") {
                return this._renderItem(table, item);
            } else {
                if (item.value == "No Result Found") {
                    return $("<tr class='ui-menu-item' role='presentation'></tr>")
                        .data("item.autocomplete", item)
                        .append("<td colspan='3'>No Result Found</td>")
                        .appendTo(table);
                }
            }

        };

        var path = "{{ route('users-search') }}";
        $("#autocomplete_search").autocomplete({
            minLength: 3,
            source: path,
            appendTo: "#searchusersdetailscontainer",
            change: function (event, ui) {
                if (ui.item === null) {
                    $(this).val('');
                    $('#field_id').val('');
                }
                $("#autocomplete_search").autocomplete("instance").term;
            }
        })

        $(document).on('click', '.autocompleteTr', function () {
            window.location.href = $(this).data("href");
            return false;
        });

    });

</script>
