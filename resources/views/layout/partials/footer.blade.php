<div class="az-footer">
    <div class="container">
        <span>&copy; {{ date("Y") }} Learnvern Pvt. Ltd.</span>
        <span>Designed and Developed by: <a href="http://www.urvam.com" target="_blank">Urvam Technologies</a></span>
    </div><!-- container -->
</div>
