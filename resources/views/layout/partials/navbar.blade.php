<div class="az-navbar">
    <div class="container">
        <ul class="nav m-auto">
            @foreach($admin_module as $data)
                @if($is_super_admin || in_array($data->module_keys, $module_access))
                    <li class="nav-item">
                        <a href="{{ ($data->route_name && (!is_null($data->route_name) || $data->route_name == 'NULL')) ? route($data->route_name) : ""}}" class="nav-link @if(!$data->route_name) with-sub @endif">
                            <i class="{{ $data->icon_class }}"></i>{{ $data->module_title }}
                        </a>
                        <nav class="nav-sub">
                            @foreach($data->child as $child)
                                @if($is_super_admin || in_array($child->module_keys, $module_access))
                                    @if($child->parent_module == $data->id)
                                        <a href="{{route($child->route_name)}}"
                                           class="nav-sub-link">{{ $child->module_title }}</a>
                                    @endif
                                @endif
                            @endforeach
                        </nav>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
</div>
