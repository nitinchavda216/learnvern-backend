<div class="az-header">
    <div class="container">
        <div class="az-header-left">
            <a href="{{ (isset(getCurrentAdmin()->relatedRole->user_type) && getCurrentAdmin()->relatedRole->user_type == 'crm') ? route('crm.dashboard') : route('dashboard') }}"
               class="az-logo">
                <img src="{{ asset('admin/img/logo.png') }}" alt="Learnvern" height="40" class="img-responsive">
            </a>
            <a href="" id="azNavShow" class="az-header-menu-icon d-lg-none"><span></span></a>
        </div><!-- az-header-left -->

        <form class="" name="form_master_search" id="form_master_search">
            <div class="input-group">
                <input size="50" class="form-control typeahead" placeholder="Search User" type="text"
                       id="autocomplete_search" name="autocomplete_search">
                <div class="input-group-append">
                    <div class="input-group-text" id="btnGroupAddon2"><i class="fa fa-search"></i></div>
                </div>
            </div>
        </form>
        <div id="searchusersdetailscontainer"></div>

        <div class="az-header-right">
            <div class="dropdown az-profile-menu">
                <a href="" class="az-img-user test">
                    <div class="image-area">{{ getUserNameInitials( Auth::guard('admins')->user()->name) }}</div>
                </a>
                <div class="dropdown-menu">
                    <div class="az-header-profile">
                        <div class="az-img-user">
                            <div class="image">{{ getUserNameInitials( Auth::guard('admins')->user()->name) }}</div>
                        </div><!-- az-img-user -->
                        <h5>{{ Auth::guard('admins')->user()->name }}</h5>
                    </div><!-- az-header-profile -->
                    <a href="{{route('profile')}}" class="dropdown-item"><i class="typcn typcn-user-outline"></i> My
                        Profile</a>
                    <a href="{{route('logout')}}" class="dropdown-item"><i class="typcn typcn-power-outline"></i>
                        Sign Out</a>
                </div>
            </div>
        </div><!-- az-header-right -->
    </div><!-- container -->
</div><!-- az-header -->




