<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>LearnVern Administration Panel</title>
    <link rel="icon" type="image/png" href="{{ asset('admin/img/favicon.png') }}">
    <meta name="robots" content="noindex, nofollow">
    @include('layout.global.css')
    @yield('css')
</head>
<body>
@include('layout.partials.header')
@include('layout.partials.navbar')
<main>
    @yield('content')
</main>
@include('layout.partials.footer')
@include('layout.global.js')
<script type="text/javascript">
    var $app_url = "<?php echo e(url('/')); ?>";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.validate({
        modules: 'security'
    });

</script>

<script>
    @if(Session::has('success'))
    toastr.success("{{ \Illuminate\Support\Facades\Session::get('success') }}", "Success!", {timeOut: 2000});
    @endif
    @if(Session::has('warning'))
    toastr.warning("{{ \Illuminate\Support\Facades\Session::get('warning') }}", "Warning!");
    @endif
    @if(Session::has('error'))
    toastr.error("{{ \Illuminate\Support\Facades\Session::get('error') }}", "Error!",{timeOut: 2000});
    @endif
</script>
@yield('footer_scripts')
</body>
</html>

