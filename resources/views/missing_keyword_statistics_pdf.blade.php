<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        {{--    <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">--}}
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Missing Search Keyword Statistics</title>
        <style media="all">
            @page {
                margin: 0;
            }

            body {
                margin: 0;
                padding: 0;
                border: 0;
                font-family: "Poppins", sans-serif;
            }
        </style>
    </head>
    <body style="margin:0;padding:0;font-family: 'Poppins', sans-serif;background-color:#FFFFFF;">
        <!--Header-->
        <table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#03386f;">
            <tr>
                <td align="left" style="background-color:#03386f;padding:20px 20px;">
                    <img src="https://www.learnvern.com/images/logo.png" style="width:180px;">
                </td>
                <td align="right" style="background-color:#03386f;color:#FFF;padding:20px 20px;font-weight:bold;">
                    <div style="text-align:center;max-width:80px;margin-left:auto;margin-right:0;">
                        Date:
                        <br>
                        {{ formatDate($reportDate, 'd/m/Y') }}
                    </div>
                </td>
            </tr>
        </table>
        <!--/Header-->

        <!--Missing Search Keywords Section-->
        <table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#FFFFFF;">
            <tr>
                <td align="left" style="padding:30px 20px 10px 20px;">
                    <h3 style="color:#000000;font-weight:bold;margin:0;padding:0;">Missing Search Keywords</h3>
                </td>
            </tr>
            <tr>
            <table width="100%" border="0.5" cellpadding="10" cellspacing="0" style="padding:5 20px;">
                <tr>
                    <th align="center" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:12px;font-weight:bold;">Keyword
                    </th>
                    <th align="center" valign="center"
                        style="background-color:#76bdee24;color:#03386f;font-size:12px;font-weight:bold;">Attempts
                    </th>
                </tr>
                @forelse($get_missing_keyword_datas as $get_missing_keyword_data)
                <tr>
                    <td width="60%" align="left" valign="center"
                        style="color:#000;font-size:12px;border-top:0;">{{ $get_missing_keyword_data->keyword }}</td>
                    <td width="20%" align="center" valign="center"
                        style="color:#000;font-size:14px;border-top:0;">{{ $get_missing_keyword_data->attempts }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="3" align="center" valign="center"
                        style="color:#000;font-size:15px;border-top:0;">No Record Found.
                    </td>
                </tr>
                @endforelse
            </table>
        </tr>
    </table>
    <!--/Missing Search Keywords Section-->
</body>
</html>