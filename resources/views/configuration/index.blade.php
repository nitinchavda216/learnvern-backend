@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Configurations
                    @if(session('partner_id') == 1)
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ env('FRONT_URL').'sitemap/update' }}" class="btn btn-success btn-sm mt-negative">
                            Update Sitemap
                        </a>
                    </span>
                        @endif
                </h2>
                <div id="data-container">
                    <form method="POST" action="{{ route('configuration.update') }}"
                          accept-charset="UTF-8" style="display:inline">
                        @csrf
                        @if(count($configuration_groups)>0)
                            @foreach($configuration_groups as $configuration_group)
                                @if(isset($configuration_group->siteConfigurations) && !empty($configuration_group->siteConfigurations))
                                    <h4 class="pl-2">{{ $configuration_group->title }}</h4>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                            @foreach($configuration_group->siteConfigurations as $item)
                                                <tr>
                                                    <td>{{ $item->configuration_title }}</td>
                                                    <td>
                                                        <input type="hidden" name="configuration_id[]"
                                                               value="{{$item->identifier}}">
                                                        @if($item->control_type == 'select_box')
                                                            @if($item->identifier == 'default_vendor_classification_group')
                                                                {!! Form::select('configuration_value['.$item->identifier."]",$all_vendors,(isset($item->configuration_value) ? $item->configuration_value : ""),['class'=>'form-control']) !!}
                                                            @else
                                                                {!! Form::select('configuration_value['.$item->identifier."]",$order_status_list,(isset($item->configuration_value) ? $item->configuration_value : ""),['class'=>'form-control']) !!}
                                                            @endif
                                                        @elseif($item->control_type == 'checkbox')
                                                            <input type="checkbox"
                                                                   name="configuration_value[{{$item->identifier}}]"
                                                                   @if(isset($item->configuration_value) && $item->configuration_value == 1)checked @endif>
                                                        @elseif($item->control_type == 'multiple_select_box')
                                                            @php
                                                                $selectedData = [];
                                                                if (isset($item->configuration_value)){
                                                                       $selectedData = explode(',', $item->configuration_value);
                                                                }
                                                            @endphp
                                                            {!! Form::select('configuration_value['.$item->identifier."][]",$order_status_list,$selectedData,['class'=>'form-control', 'multiple']) !!}
                                                        @elseif($item->control_type == 'textarea')
                                                            {!! Form::textarea('configuration_value['.$item->identifier.']', $item->configuration_value ?? old('brief_description'), ['class'=>'form-control', 'id' => 'configuration_value['.$item->identifier.']', 'rows' => 2]) !!}
                                                            {{--<textarea class="form-control" name="configuration_value[{{$item->identifier}}]">{{isset($item->configuration_value)?$item->configuration_value:''}}</textarea>--}}

                                                        @else
                                                            <input type="text" class="form-control"
                                                                   name="configuration_value[{{$item->identifier}}]"
                                                                   value="{{isset($item->configuration_value)?$item->configuration_value:''}}">
                                                        @endif

                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                            @endforeach
                            <div class="text-right">
                                <input type="submit" class="btn btn-az-primary" value="Update">
                            </div>
                        @else
                            <h1 class="text-center">No record found</h1>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

