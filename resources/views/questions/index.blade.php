@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Add Quiz Question
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('quiz.questions.export', $quiz->id) }}" class="btn btn-success btn-sm mt-negative" title="Download Question Import Sample">
                            <i class="fa fa-download" aria-hidden="true"></i> Download Import Questions Sample
                        </a>
                        <a href="javascript:;" data-toggle="modal"
                           data-target="#uploadFileModal" class="btn btn-info btn-sm mt-negative" title="Import Questions">
                            <i class="fa fa-upload" aria-hidden="true"></i> Import New Questions
                        </a>
                        <a href="{{ route('quiz') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="card card-body pd-40">
                        {!! Form::open(['route'=>'quiz.questions.store','method' => 'POST', 'enctype'=> "multipart/form-data", 'files' => true, 'id' => "courseForm"]) !!}
                        @include("questions.form")
                        {!! Form::close() !!}
                    </div>
                </div>
                <hr class="mg-y-30 mg-lg-y-50">
                <h2 class="az-content-title">Questions List</h2>
                <div id="data-container">
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>Question Name</th>
                            <th>Quiz</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($questions)
                            @foreach($questions as $item)
                                <tr>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->quizDetail->name ?? '- - -' }}</td>
                                    <td>
                                        <a href="{{ route('quiz.questions.edit', $item->id) }}"
                                           class="btn btn-primary btn-xs d-inline">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <div class="d-inline az-toggle az-toggle-secondary float-right {{ ($item->is_active == 1) ? 'on' : '' }}"
                                             data-id="{{ $item->id }}">
                                            <span></span>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="uploadFileModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    Upload File
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form method="post" action="{{route('quiz.questions.import')}}"
                      enctype="multipart/form-data">
                    <div class="modal-body">
                        @csrf
                        <input name="file" type="file" id="FileUpload" class="file-upload" data-validation="required"
                               data-allowed-file-extensions="xlsx"/>
                    </div>
                    <input type="hidden" name="quiz_id" value="{{ $quiz->id }}">
                    <div class="modal-footer">
                        <button class="btn btn-indigo">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".questionOptions ").select2({
                placeholder: 'Select Right Answer'
            });

            $('#dataTableList').DataTable({
                responsive: true,
                aaSorting: [],
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ records',
                }
            });
        });
        // Toggle Switches
        $(document).on('click', ".az-toggle", function () {
            var status = ($(this).toggleClass('on').hasClass('on') == true) ? 1 : 0;
            var id = $(this).data('id');
            $.ajax({
                type: "post",
                dataType: "json",
                url: $app_url + '/quiz/question/update_status',
                data: {'is_active': status, 'id': id},
                success: function () {
                    toastr.success("Status Updated Successfully!", "Success!", {timeOut: 2000});
                }
            });
        });
    </script>
@endsection

