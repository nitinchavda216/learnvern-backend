<input type="hidden" name="quiz_id" value="{{ $quiz->id ?? $question->quiz_id }}">
<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Quiz Name :</label>
            {!! Form::text('name', $quiz->name ?? $question->quizDetail->name, ['id'=>'name', 'class'=>'form-control', 'disabled']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Backend Question Title :</label>
            {!! Form::text('title', $question->title ?? old('title'), ['class'=>'form-control', 'data-validation' => 'required', 'placeholder' => ""]) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Frontend Question Title :</label>
    {!! Form::textarea('content', $question->content ?? old('content'), ['class'=>'form-control wysiwyg_editor', 'id' => 'content']) !!}
</div>
<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Question Option-1 :</label>
            {!! Form::text('options[1]', $question->relatedOptions[0]->content ?? null, ['class'=>'form-control', 'placeholder' => ""]) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Question Option-2 :</label>
            {!! Form::text('options[2]', $question->relatedOptions[1]->content ?? null ?? null, ['class'=>'form-control', 'placeholder' => ""]) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Question Option-3 :</label>
            {!! Form::text('options[3]', $question->relatedOptions[2]->content ?? null ?? null, ['class'=>'form-control', 'placeholder' => ""]) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Question Option-4 :</label>
            {!! Form::text('options[4]', $question->relatedOptions[3]->content ?? null ?? null, ['class'=>'form-control', 'placeholder' => ""]) !!}
        </div>
    </div>
</div>
<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Question Type :</label>
            {!! Form::select('type', \App\Models\Question::TYPES, $question->type ?? old('type') , ['class'=>'form-control select2']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Question Right Answer :</label>
            {!! Form::select('answer', [1 => "Option 1", 2 => "Option 2", 3 => "Option 3", 4 => "Option 4"], $question->answer ?? old('answer') , ['class'=>'form-control questionOptions', 'data-validation' => 'required', 'placeholder' => ""]) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Question Mark :</label>
            {!! Form::text('mark', $question->mark ?? old('mark'), ['class'=>'form-control', 'data-validation' => 'required', 'placeholder' => ""]) !!}
        </div>
    </div>
</div>
<button type="submit" class="btn btn-az-primary mg-md-t-9">{{ isset($question) ? "Update" : "Submit" }}</button>
