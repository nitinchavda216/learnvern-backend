@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title">International Candidates
                    <span class="pull-right d-inline-block float-right">
                        <a href="javascript:;" class="btn btn-sm btn-success mt-negative"
                           onclick="this.href='international-candidates/getInternationalExportData?date_range=' + document.getElementById('date_range').value + '&search=' + document.getElementById('search').value+ '&partner_user_id=' + document.getElementById('partner_user_id').value"><i
                                class="fa fa-download" aria-hidden="true"></i> Export</a>
                    </span>
                </h2>
                <div id="data-container">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-3">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Search:</label>
                                <input name="search" id="search" type="text" class="form-control" placeholder="Search">
                            </div>
                            @if(session('partner_id') == 1)
                                <div class="col-md-3">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Partners:</label>
                                    {!! Form::select('partner_user_id', $partners, null,['id' => 'partner_user_id', 'class' => 'form-control select2']) !!}
                                </div>
                            @endif
                            <div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Date Range:</label>
                                <input name="date_range" id="date_range" type="text" class="form-control"
                                       autocomplete="off" placeholder="Select Date">
                            </div>
                            <div class="col-md-2 mt-4">
                                <button type="submit" class="btn btn-sm btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-sm btn-az-warning btn-warning" id="reset-filters">
                                    Reset
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>User Details</th>
                            <th>Course Details</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="{{asset('admin/js/jquery.blockUI.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#date_range').daterangepicker({
                autoUpdateInput: true,
                startDate: moment().subtract(6, 'days'),
                endDate: moment(),
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY'
                },
                ranges: {
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            $('#date_range').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            });

            $('#date_range').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

            var $dataTableList = $('#dataTableList').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                bFilter: false,
                "aaSorting": [],
                ajax: {
                    url: $app_url + '/international-certificates/getAjaxData',
                    data: function (d) {
                        d.search = $('#search').val();
                        d.date_range = $('#date_range').val();
                        d.partner_user_id = $('#partner_user_id').val();
                    }
                },
                "columns": [
                    {data: 'userDetails', 'sortable': false, 'width': "30%"},
                    {data: 'courseDetails', 'sortable': false, 'width': "30%"},
                    {data: 'action', 'sortable': false, 'width': "10%"}
                ],
                select: true,
                bStateSave: true,
                fnStateSave: function (settings, data) {
                    localStorage.setItem("dataTables_state", JSON.stringify(data));
                },
                fnStateLoad: function (settings) {
                    return JSON.parse(localStorage.getItem("dataTables_state"));
                }
            });

            $('#search-form').on('submit', function (e) {
                $dataTableList.draw();
                e.preventDefault();
            });

            $('#reset-filters').on('click', function () {
                $("#search").val(null);
                $("#date_range").val(null);
                $("#partner_user_id").val(1).trigger('change');
                $dataTableList.draw();
            });

            $(document).on('click', '.convertToNSDC', function () {
                var self = $(this);
                var id = self.data('id');
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: $app_url + '/convert-to-course',
                    data: {'id': id},
                    beforeSend: function () {
                        $.blockUI({
                            message: '<h1>Please wait..</h1>',
                            timeout: 2000
                        });
                    },
                    success: function () {
                        toastr.success("Certificate converted successfully.", "Success!", {timeOut: 2000});
                        location.reload();
                    }
                });
            });
        });
    </script>
@endsection

