@if($data->progress > 99)
<div class="row">
    <div class="col-md-3">
        <form name="" target="_blank" action="{{ route('certificate.download')}}" class="frm_get_user_certificate" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="hdn_course_id" value="{{$data->courseDetail->id}}">
            <input type="hidden" name="hdn_user_id" value="{{$data->userDetail->id}}">
            <button type="submit" value="submit" class="btn btn-primary btn-xs" name="btn_get_certificate"><i class="fa fa-download" aria-hidden="true"></i></button>
        </form>
    </div>
</div>
@endif
<a href="javascript:;" data-id="{{ $data->id }}"
   class="btn btn-success btn-xs convertToNSDC">
    Convert NSDC
</a>



