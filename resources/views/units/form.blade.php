<div id="wizard2">
    <h3>General Info</h3>
    <section>
        @include('units.tabs.general')
    </section>
    <h3>Video Info</h3>
    <section>
        @include('units.tabs.videos')
    </section>
    <h3>Faqs Details</h3>
    <section>
        @include('units.tabs.faq_title')
    </section>
    <h3>SEO Details</h3>
    <section>
        @include('units.tabs.seo_details')
    </section>
</div>
@section('footer_scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script type="text/javascript" src="{{ asset('admin/lib/jquery-steps/jquery.steps.min.js') }}"></script>
    <script type="text/javascript">
        var is_edit = "{{ isset($unit) ? true : false }}";
    </script>
    <script type="text/javascript" src="{{ asset('admin/modules/units/form.js') }}"></script>
@endsection

