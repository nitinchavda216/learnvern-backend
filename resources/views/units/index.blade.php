@extends('layout.master')
@section('content')

    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Units List
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('units.create') }}" class="btn btn-success btn-sm mt-negative">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="az-content-label mg-b-5">For Content Team Import New Units:</div>
                    {!! Form::open(['route' => 'units.sample-export', 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'export-form']) !!}
                    <div class="row row-xs">
                        <div class="col-md-5">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Course:</label>
                            {!! Form::select('course_id', $courses, Null,['class' => 'form-control select2', 'placeholder' => "", 'data-validation' => 'required']) !!}
                        </div>
                        <div class="mg-l-10 mt-4">
                            <button type="submit" class="btn btn-sm btn-success float-right mg-l-0">
                                <i class="fa fa-download" aria-hidden="true"></i> Download Import Sample
                            </button>
                        </div>
                        <div class="mg-l-10 mt-4">
                            <a href="javascript:;" class="btn btn-sm btn-info float-right mg-l-0"
                               data-toggle="modal"
                               data-target="#uploadNewUnitsModal"><i class="fa fa-upload" aria-hidden="true"></i> Import New Units</a>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <hr class="mg-y-20">
                    <div class="az-content-label mg-b-5">For SEO Team Update Units Meta Information:</div>
                    {!! Form::open(['route' => 'units.export', 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'export-form']) !!}
                    <div class="row row-xs">
                        <div class="col-md-5">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Course:</label>
                            {!! Form::select('course_id', $courses, Null,['class' => 'form-control select2', 'placeholder' => ""]) !!}
                        </div>
                        <div class="mg-l-10 mt-4">
                            <button type="submit" class="btn btn-sm btn-success float-right mg-l-0">
                                <i class="fa fa-download" aria-hidden="true"></i> Export
                            </button>
                        </div>
                        <div class="mg-l-10 mt-4">
                            <a href="javascript:;" class="btn btn-sm btn-info float-right mg-l-0"
                               data-toggle="modal"
                               data-target="#uploadFileModal"><i class="fa fa-upload" aria-hidden="true"></i> Import</a>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <hr class="mg-y-20">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-5">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Course:</label>
                                {!! Form::select('course_id', $courses,request('course_id'),['id' => 'course_id', 'class' => 'form-control select2 course', 'placeholder' => ""]) !!}
                            </div>
                            <div class="col-md-5">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Section:</label>
                                {!! Form::select('section_id',$sections, request('section_id'),['id' => 'section_id', 'class' => 'form-control', 'placeholder' => ""]) !!}
                            </div>
                            {{--<div class="col-md-2">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Hidden Units Status:</label>
                                {!! Form::select('hide_from_backend',[0 => 'Visible Units', 1 => "Hidden Units"], request('hide_from_backend'),['id' => 'hide_from_backend', 'class' => 'form-control select2_no_search']) !!}
                            </div>--}}
                            <div class="col-md-2 mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>Unit Name</th>
                            <th>Courses Name</th>
                            <th>Section Name</th>
                            {{--                            <th>Hidden Status</th>--}}
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="uploadFileModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    Upload Update Meta Information File
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form method="post" action="{{route('units.import')}}"
                      enctype="multipart/form-data">
                    <div class="modal-body">
                        @csrf
                        <input name="file" type="file" class="file-upload" data-validation="required"
                               data-allowed-file-extensions="xlsx"/>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-indigo">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="uploadNewUnitsModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    Upload New Units File
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form method="post" action="{{route('units.import-new')}}"
                      enctype="multipart/form-data">
                    <div class="modal-body">
                        @csrf
                        <input name="file" type="file" class="file-upload" data-validation="required"
                               data-allowed-file-extensions="xlsx"/>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-indigo">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('admin/modules/units/index.js') }}"></script>
@endsection

