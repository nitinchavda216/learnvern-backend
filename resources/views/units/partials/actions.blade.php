<a href="{{ route('units.edit', $item->curriculum_list_id) }}" class="btn btn-primary btn-xs d-inline">
    <i class="fas fa-edit"></i>
</a>
<div class="d-inline activeStatus az-toggle az-toggle-secondary float-right {{ ($item->is_active == 1) ? 'on' : '' }}"
     data-id="{{ $item->curriculum_list_id }}">
    <span></span>
</div>
