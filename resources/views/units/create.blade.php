@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Add Unit
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('units') }}" class="btn btn-warning btn-sm mt-negative"><i
                                    class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="card card-body pd-20">
                        {!! Form::open(['route'=>'units.store','method' => 'POST', 'enctype'=> "multipart/form-data", 'files' => true, 'id' => "unitForm"]) !!}
                        @include("units.form")
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="newRowTemplate" style="display: none;">
        <hr>
        <div class="pd-30 pd-sm-20 bg-light">
            <div class="form-group">
                <label class="az-content-label">Question :</label>
                {!! Form::text('question[]', NULL, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                <label class="az-content-label">Answer :</label>
                {!! Form::textarea('answer[]', NULL, ['class'=>'form-control editor']) !!}
            </div>
            <button type="submit" class="btn btn-danger mg-md-t-9 btn-sm removeRow"><i class="fas fa-trash"></i>
            </button>
        </div>
    </div>
    <div class="attachmentDiv p-2" style="display: none;">
        <div class="row row-sm">
            <div class="col-sm-4">
                <div class="form-group">
                    {!! Form::text('title_attachment[]',old('name'), ['class'=>'form-control', 'id' => 'title_attachment', 'placeholder' => 'Enter Title..', 'data-validation' => 'required']) !!}
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <input type="file" class="attachment-dropify" id="attachment" name="attachment[]" data-show-remove="false" data-validation="required">
                    <input type="hidden" name="remove_attachment" id="removeAttachment" value="false">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <button type="button" class="btn btn-sm btn-danger removeRow"><i
                                class="fa fa-trash"></i></button>
                </div>
            </div>
        </div>
    </div>
@endsection
