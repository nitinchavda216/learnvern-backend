<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Meta Title :</label>
    {!! Form::text('meta_title', $unit->meta_title ?? old('meta_title'), ['class'=>'form-control', 'id' => 'meta_title']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Meta Description :</label>
    {!! Form::text('meta_description', $unit->meta_description ?? old('meta_description'), ['class'=>'form-control', 'id' => 'meta_description']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Meta Keyword :</label>
    {!! Form::text('meta_keywords', $unit->meta_keywords ?? old('meta_keywords'), ['class'=>'form-control', 'id' => 'meta_keywords']) !!}
</div>
@if(isset($unit))
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Schema :</label>
    {!! Form::textarea('schema_script', $unit->schema_script ?? old('schema_script'), ['class'=>'form-control', 'id' => 'schema_script', 'rows' => 4, 'disabled']) !!}
</div>
@endif
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Canonical URL :</label>
    {!! Form::text('canonical_url', $unit->canonical_url ?? old('canonical_url'), ['class'=>'form-control', 'id' => 'canonical_url']) !!}
</div>