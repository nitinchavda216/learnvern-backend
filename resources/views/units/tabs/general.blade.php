<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Unit Name :</label>
            {!! Form::text('name', $unit->name ?? old('name'), ['class'=>'form-control', 'id' => 'unit_name', 'data-validation' => 'required']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        @if(isset($unit->slug))
            <div class="form-group">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Unit Slug :</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="slug" value="{{ $unit->slug }}" readonly>
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button" data-toggle="modal"
                                data-target="#updateSlugModal"><i class="fa fa-edit"></i></button>
                    </span>
                </div>
            </div>
        @else
            <div class="form-group">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Unit Slug :</label>
                {!! Form::text('slug', $unit->slug ?? old('slug'), ['class'=>'form-control slugInput', 'id' => 'slug', 'data-validation' => 'required']) !!}
                <span id="slugmsg"></span>
            </div>
        @endif
    </div>
</div>
<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Course :</label>
            {!! Form::select('course_id', $courses, $unit->course_id ?? old('course_id'), ['id' => "course_id", 'class'=>'form-control select2Input', 'data-validation' => 'required', 'placeholder' => ""]) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Section :</label>
            {!! Form::select('section_id', $sections, $unit->section_id ?? old('section_id'), ['id'=>'sectionId', 'class'=>'form-control', 'data-validation' => 'required', 'placeholder' => ""]) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Unit Content :</label>
    {!! Form::textarea('content', $unit->content ?? old('content'), ['class'=>'form-control html_editor', 'id' => 'content']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Unit Short Description :</label>
    {!! Form::textarea('short_content', $unit->short_content ?? old('short_content'), ['class'=>'form-control html_editor', 'id' => 'short_content']) !!}
</div>
<div class="card">
    <div class="card-header">
        <button type="button" class="btn btn-sm btn-primary" id="addNewAttachment"><i
                class="fa fa-plus"></i> Add Attachment
        </button>
    </div>
    <div class="card-body p-0">
        <div id="sectionRows">
            @if(isset($unit) && sizeof($attachment) != 0)
                @foreach($attachment as $key => $value)
                    <div class="attachmentDiv p-2">
                        <div class="row row-sm">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    {!! Form::text('title_attachment[]', $value->title, ['class'=>'form-control', 'id' => 'title_attachment', 'data-validation' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="file" class="dropify" data-show-remove="false" data-default-file="{{ showAttachment($value->attachment) }}"
                                           id="attachment" name="attachment[]">
                                    <input type="hidden" name="remove_attachment" id="removeAttachment" value="false">
                                    <input type="hidden" name="hidden_attachment{{$key}}" id="hidden_attachment"
                                           value="{{$value->attachment}}">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <button type="button" class="btn btn-sm btn-danger removeRow"><i
                                            class="fa fa-trash"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>
{{--<div class="col-sm-4">
    <div class="form-group">
        <label class="az-content-label tx-11 tx-medium tx-gray-600"></label>
        <label class="ckbox">
            <input type="checkbox" @if(isset($unit) && $unit->is_global_unit == 1) checked @endif
            name="is_global_unit"><span>Is Global Unit?</span>
        </label>
    </div>
    <div class="row" id="globalConditionsDiv">
        <div class="col-sm-12">
            <div class="form-group">
                <label>Add this unit on section position </label>
                <input type="number" name="conditions[section_position]" value="{{ isset($unit) && !empty($unit->global_unit_conditions) ? $unit->global_unit_conditions['section_position'] : 1 }}" style="width: 50px !important;" min="1">
                <label> apply from </label>
                <select name="conditions[apply_condition_on_section]">
                    <option value="start" @if(isset($unit) && !empty($unit->global_unit_conditions) && ($unit->global_unit_conditions['apply_condition_on_section'] == "start")) selected @endif>start</option>
                    <option value="end" @if(isset($unit) && !empty($unit->global_unit_conditions) && ($unit->global_unit_conditions['apply_condition_on_section'] == "end")) selected @endif>end</option>
                </select>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
                <label>Add this unit under section position </label>
                <input type="number" name="conditions[under_section_position]" value="{{ isset($unit) && !empty($unit->global_unit_conditions) ? $unit->global_unit_conditions['under_section_position'] : 1 }}" style="width: 50px !important;" min="1">
                <label> apply from </label>
                <select name="conditions[apply_condition_under_section]">
                    <option value="start" @if(isset($unit) && !empty($unit->global_unit_conditions) && ($unit->global_unit_conditions['apply_condition_under_section'] == "start")) selected @endif>start</option>
                    <option value="end" @if(isset($unit) && !empty($unit->global_unit_conditions) && ($unit->global_unit_conditions['apply_condition_under_section'] == "end")) selected @endif>end</option>
                </select>
            </div>
        </div>
    </div>
</div>--}}



