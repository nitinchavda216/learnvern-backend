<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Unit Vimeo Video Id :</label>
    {!! Form::text('video_id', $unit->video_id ?? old('video_id'), ['class'=>'form-control', 'id' => 'video_id']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Unit Youtube Video Url :</label>
    {!! Form::text('youtube_video_url', $unit->youtube_video_url ?? old('youtube_video_url'), ['class'=>'form-control', 'id' => 'youtube_video_url']) !!}
</div>
<div class="row row-sm">
    @if(isset($unit))
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Course Time :</label>
            <div class="input-group">
                {!! Form::text('time', $unit->time ?? old('time'), ['class'=>'form-control form_time', 'id' => 'time', 'disabled']) !!}
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-clock"></i></span>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="col-sm-4">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600"></label>
            <label class="ckbox">
                <input type="checkbox" @if(isset($unit) && $unit->is_free == 1) checked @endif
                name="is_free"><span>Is Unit Free?</span>
            </label>
        </div>
    </div>
</div>