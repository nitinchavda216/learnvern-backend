<button type="button" class="btn btn-sm btn-primary" id="addNewRow">Add New Faq</button>
@if(isset($unit) && (count($unit->relatedFaqTitles) > 0))
    <div id="unitFaqs">
        @foreach($unit->relatedFaqTitles as $relatedFaqTitle)
            <div class="unitFaqs">
                <hr>
                <div class="pd-30 pd-sm-20 bg-light p-2 ">
                    <div class="form-group">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Question :</label>
                        {!! Form::text('question[]', $relatedFaqTitle->content, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Answer :</label>
                        {!! Form::textarea('answer[]',$relatedFaqTitle->answer, ['class'=>'form-control html_editor']) !!}
                    </div>
                    <a href="javascript:;"
                       class="btn btn-danger btn-sm deleteFaq" data-id="{{ $relatedFaqTitle->id }}">
                        <i class="fas fa-trash"></i>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@else
    <div id="unitFaqs">
        <div class="unitFaqs">
            <hr>
            <div class="pd-30 pd-sm-20 bg-light">
            <div class="form-group">
                <label class="az-content-label">Question :</label>
                {!! Form::text('question[]', NULL, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                <label class="az-content-label">Answer :</label>
                {!! Form::textarea('answer[]',NULL, ['class'=>'form-control html_editor']) !!}
            </div>
            </div>
        </div>
    </div>
@endif

