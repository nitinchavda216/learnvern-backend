@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Partner Application Requests</h2>
                <div id="data-container">
                    {{--{!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="mb-3">
                        <div class="row row-xs">
                            <div class="col-md-3">
                                <input name="search" id="search" type="text" class="form-control" placeholder="Search">
                            </div>
                            <div class="col-md-3">
                                <input name="transaction_date" id="transaction_date" type="text" class="form-control"
                                       autocomplete="off" placeholder="Select Date">
                            </div>
                            <div class="col-md mg-t-10 mg-md-t-0">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}--}}
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Application Date</th>
                            <th>Domain</th>
                            <th>Setup Fees</th>
                            <th>Site Setup</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    @include('partner_app_request.modals.approval')
@endsection
@section('css')
    <link rel="stylesheet" type="text/css"
          href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript"
            src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <script type="text/javascript">
        var $approvalUrl = "{{ route('approve_partner_application') }}";
        var $third_party_service_fees = '{{ $service_fees['third_party_partner_fees'] }}';
        var $content_partner_fees = '{{ $service_fees['content_partner_fees'] }}';
        $(document).ready(function () {
            $('#transaction_date').daterangepicker({
                autoUpdateInput: true,
                startDate: moment().subtract(6, 'days'),
                endDate: moment(),
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD-MM-YYYY'
                },
                ranges: {
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });

            $('#transaction_date').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            });

            $('#transaction_date').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });


            var $dataTableList = $('#dataTableList').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                bFilter: false,
                "aaSorting": [],
                ajax: {
                    url: $app_url + '/partners/getAjaxData',
                    data: function (d) {
                        d.search = $('#search').val();
                        d.transaction_date = $('#transaction_date').val();
                    }
                },
                "columns": [
                    {data: 'name', 'sortable': false, 'width': "10%"},
                    {data: 'email', 'sortable': false, 'width': "10%"},
                    {data: 'created_at', 'sortable': false, 'width': "10%"},
                    {data: 'domain', 'sortable': true, 'width': "10%"},
                    {data: 'setup_fees', 'sortable': true, 'width': "10%"},
                    {data: 'setup_complete', 'sortable': false, 'width': "10%"},
                    {data: 'action', 'sortable': false, 'width': "10%"}
                ],
                select: true,
                bStateSave: true,
                fnStateSave: function (settings, data) {
                    localStorage.setItem("dataTables_state", JSON.stringify(data));
                },
                fnStateLoad: function (settings) {
                    return JSON.parse(localStorage.getItem("dataTables_state"));
                }
            });

            $('#search-form').on('submit', function (e) {
                $dataTableList.draw();
                e.preventDefault();
            })

            $('#reset-filters').on('click', function () {
                $("#search").val(null);
                $("#transaction_date").val(null);
                $dataTableList.draw();
            });
            $(document).on('click', '.openApprovalPopup', function () {
                $("#partner_id").val($(this).attr('id'));
                $("#partnerApprovalModal").modal('show');
            });
            /*$(document).on('click', '#submitButton', function () {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: $app_url + '/partners/approve',
                    data: $("#approvePartnerRequestForm").serialize(),
                    success: function (responseData) {
                        if (responseData.status) {
                            $("#PartnerTypeModel").modal('hide');
                            window.location.reload();
                            toastr.success("Status Updated Successfully!", "Success!", {timeOut: 2000});
                        }else{
                            toastr.error(responseData.message, "Error!", {timeOut: 2000});
                        }
                    }
                });
            });*/

            $(document).on('change', '#partner_type', function () {
                $service_fees_value = $(this).val() == "content_partner" ? $content_partner_fees : $third_party_service_fees;
                $("#setup_fees").attr("value", $service_fees_value);
                $("#serviceFeesRow").show();

            });
        });
        function confirmDelete(delete_url, id) {
            if (id == 1) {
                var status = 'approve application';
            }
            if (id == 2) {
                var status = 'reject application';
            }

            bootbox.confirm({
                message: "Are you sure you want to " + status + "?",
                buttons: {
                    confirm: {
                        label: "Yes",
                        className: 'btn-success'
                    },
                    cancel: {
                        label: "No",
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result === true) {
                        window.location = delete_url;
                    }
                }
            });
        }
    </script>
@endsection

