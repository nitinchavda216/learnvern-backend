<div class="modal fade" id="partnerApprovalModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                Partner Type
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form id="approvePartnerRequestForm" method="post" action="{{ route('approve_partner_application') }}">
                {!! csrf_field() !!}
                <div class="modal-body">
                    <input type="hidden" name="partner_user_id" id="partner_id">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">Select Partner Type:</label>
                                <select name="partner_type" id="partner_type" class="form-control">
                                    <option value="" selected>Select Partner Type</option>
                                    <option value="content_partner">Content Partner</option>
                                    <option value="third_party_partner">Third Party Partner</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="display: none;" id="serviceFeesRow">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">Adjusted Service Fees:</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">₹</span>
                                    </div>
                                    <input type="text" name="setup_fees" id="setup_fees" class="form-control">
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="allow_all_courses">
                                    <input type="checkbox" id="allow_all_courses" name="allow_all_courses">
                                    Allow All Current Courses
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="allow_all_future_courses">
                                    <input type="checkbox" id="allow_all_future_courses"
                                           name="allow_all_future_courses">
                                    Allow All Future Courses
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="show_webinars">
                                    <input type="checkbox" id="show_webinars"
                                           name="show_webinars">
                                    Show Webinars
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="use_own_certificate">
                                    <input type="checkbox" id="use_own_certificate"
                                           name="use_own_certificate">
                                    Use Own Certificate?
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-az-primary mg-md-t-9">submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>