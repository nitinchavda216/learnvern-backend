@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Partner Details
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{route('partner-application-request') }}" class="btn btn-warning btn-sm mt-negative"
                           title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <form method="post" action="{{ route('update_partner_application',[$data->id]) }}">
                    {{ csrf_field() }}
                    <div id="data-container">
                        <div class="row">
                            <div class="col-4">
                                <h3><u>General Information</u></h3>
                                <div class="row row-sm">
                                    <div class="col-sm-12">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Business Name
                                                : </b></label>
                                        {{$data->name}}
                                    </div>
                                </div>
                                <div class="row row-sm">
                                    <div class="col-sm-12">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Email Address
                                                :</b></label>
                                        {{$data->contact_person_email}}
                                    </div>
                                </div>
                                <div class="row row-sm">
                                    <div class="col-sm-12">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Contact Number
                                                :</b></label>
                                        {{$data->contact_person_phone_number}}
                                    </div>
                                </div>
                                <div class="row row-sm">
                                    <div class="col-sm-12">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Short InFormation
                                                :</b></label>
                                        {{$data->description}}
                                    </div>
                                </div>
                                <div class="row row-sm">
                                    <div class="col-sm-12">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Count Of Students
                                                :</b></label>
                                        {{$data->current_students_count}}
                                    </div>
                                </div>
                                <div class="row row-sm">
                                    <div class="col-sm-12">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">
                                            <b>Website: </b>
                                        </label>
                                        {{$data->website}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <h3 class="mt-2"><u>Office Information</u></h3>
                                <div class="row row-sm">
                                    <div class="col-sm-12">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Address Line
                                                :</b></label>
                                        {{$data->address_line1}}
                                    </div>
                                </div>
                                <div class="row row-sm">
                                    <div class="col-sm-12">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">
                                            <b>Landmark: </b>
                                        </label>
                                        {{$data->address_line2}}
                                    </div>
                                </div>
                                <div class="row row-sm">
                                    <div class="col-sm-12">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">
                                            <b>City Name: </b>
                                        </label>
                                        {{$data->city}}
                                    </div>
                                </div>
                                <div class="row row-sm">
                                    <div class="col-sm-12">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>State Name
                                                :</b></label>
                                        {{$data->state}}
                                    </div>
                                </div>
                                <div class="row row-sm">
                                    <div class="col-sm-12">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">
                                            <b>Pin Code: </b>
                                        </label>
                                        {{$data->pin_code}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <h3><u>Branch Information</u></h3>
                                <div class="row row-sm">
                                    <div class="col-sm-12">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">
                                            <b>Address Line: </b>
                                        </label>
                                        {{$data->branch_address_line1}}
                                    </div>
                                </div>
                                <div class="row row-sm">
                                    <div class="col-sm-12">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">
                                            <b>Landmark: </b>
                                        </label>
                                        {{$data->branch_address_line2}}
                                    </div>
                                </div>
                                <div class="row row-sm">
                                    <div class="col-sm-6">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">
                                            <b>City Name: </b>
                                        </label>
                                        {{$data->branch_city}}
                                    </div>
                                </div>
                                <div class="row row-sm">
                                    <div class="col-sm-12">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">
                                            <b>State Name: </b>
                                        </label>
                                        {{$data->branch_state}}
                                    </div>
                                </div>
                                <div class="row row-sm">
                                    <div class="col-sm-12">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">
                                            <b>Pin Code: </b>
                                        </label>
                                        {{$data->branch_pin_code}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h2>Partner Website Information</h2>
                        <div class="row">
                            <div class="col-md-4">
                                @if($data->partner_type !="")
                                    <label for=""><b>Partner Type: </b></label>
                                    {{ \App\Models\Partners::TYPES[$data->partner_type] }}
                                    <hr class="my-1">
                                @endif
                                <label for=""><b>Allow All Courses: </b></label>
                                {{ formatBooleanStatus($data->allow_all_courses) }}
                                <hr class="my-1">
                                <label for=""><b>Show Webinars: </b></label>
                                {{ formatBooleanStatus($data->show_webinars) }}
                                <hr class="my-1">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Setup Fees: </b></label>
                                {{ formatPrice($data->setup_fees)}}
                                <span class="mx-2">
                                @if($data->setup_fees_paid_status == 1)
                                        <span class="badge bg-success text-white">Paid</span>
                                    @else
                                        <span class="badge bg-danger text-white">Unpaid</span>
                                    @endif
                            </span>
                                <hr class="my-1">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Domain
                                        Information: </b></label>
                                {{ formatPartnerWebsite($data->domain)}}
                            </div>
                            <div class="col-md-4">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Logo
                                        Image: </b></label><br>
                                <img src="{{ $data->logo_image }}" alt="" width="200">
                            </div>
                            @if($data->use_own_certificate)
                                <div class="col-md-4">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Certificate
                                            Image: </b></label><br>
                                    <img src="{{ $data->certificate_image }}" alt="" width="200">
                                </div>
                            @endif
                        </div>
                        <hr>
                        <div class="row my-2">
                            <div class="col-md-12 px-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="approve_website" id="approveWebsite"
                                           @if($data->approve_website == 1) checked disabled @endif>
                                    <label class="form-check-label" for="flexCheckDefault">
                                        Approve Website
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div id="updateSection" style="display: @if($data->approve_website == 1) block @else none @endif;">
                            <h2>Revenue Information</h2>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="">Revenue Sharing Type: </label><br/>
                                    <div class="form-check">
                                        <input class="form-check-input revenueSharingOption" type="radio"
                                               name="revenueSharingOption"
                                               @if((count($partner_revenue_fees) == 0) || $data->revenue_sharing_type == 'fixedFees') checked
                                               @endif
                                               id="flexRadioDefault1" value="fixedFees">
                                        <label class="form-check-label" for="flexRadioDefault1">
                                            Fixed Fees Per Enrollment
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input revenueSharingOption" type="radio"
                                               name="revenueSharingOption"
                                               @if($data->revenue_sharing_type == 'sharingByCount') checked @endif
                                               id="flexRadioDefault2" value="sharingByCount">
                                        <label class="form-check-label" for="flexRadioDefault2">
                                            Revenue Sharing by Count of Enrollments
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6" id="fixedFeesSection"
                                     @if((count($partner_revenue_fees) > 0) && ($data->revenue_sharing_type == 'sharingByCount')) style="display: none;" @endif>
                                    <label for="">Enrollment Fees: </label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="fixed_revenue_value"
                                               aria-label="Text input with dropdown button"
                                               value="{{ ($data->revenue_sharing_type == 'fixedFees') ? $partner_revenue_fees[0]->revenue_share_value : null }}">
                                        <div class="input-group-append">
                                            <select name="fixed_revenue_type" class="form-control">
                                                <option value="flat"
                                                        @if(($data->revenue_sharing_type == 'fixedFees') && ($partner_revenue_fees[0]->revenue_share_type == 'flat')) selected @endif>
                                                    ₹
                                                </option>
                                                <option value="percentage"
                                                        @if(($data->revenue_sharing_type == 'fixedFees') && ($partner_revenue_fees[0]->revenue_share_type == 'percentage')) selected @endif>
                                                    %
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6" id="sharingByCountSection"
                                     @if($data->revenue_sharing_type != 'sharingByCount') style="display: none;" @endif>
                                    <label for="">Revenue Sharing: </label>
                                    <div class="input-group" id="sectionRows">
                                        @if((count($partner_revenue_fees) > 0) && ($data->revenue_sharing_type == 'sharingByCount'))
                                            @foreach($partner_revenue_fees as $key => $partner_revenue_fee)
                                                <div class="addRowHtml row row-xs">
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">From</label>
                                                            {!! Form::text('revenue_from[]', $partner_revenue_fee->from_value ?? null, ['class'=>'form-control', 'placeholder' => ""]) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">To</label>
                                                            {!! Form::text('revenue_to[]', $partner_revenue_fee->to_value ?? null, ['class'=>'form-control','placeholder' => ""]) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Revenue
                                                                Share Value</label>
                                                            <div class="input-group">
                                                                {!! Form::text('revenue_value[]', $partner_revenue_fee->revenue_share_value ?? null, ['class'=>'form-control','placeholder' => ""]) !!}
                                                                <div class="input-group-append">
                                                                    <select name="revenue_type[]" class="form-control">
                                                                        <option value="flat"
                                                                                @if($partner_revenue_fee->revenue_share_type == "flat") selected @endif>
                                                                            ₹
                                                                        </option>
                                                                        <option value="percentage"
                                                                                @if($partner_revenue_fee->revenue_share_type == "percentage") selected @endif>
                                                                            %
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1 mt-4">
                                                        @if($key == 0)
                                                            <button type="button" class="btn btn-sm btn-danger"
                                                                    id="removeRow">
                                                                <i class="fas fa-trash"></i></button>
                                                        @else
                                                            <button type="button" class="btn btn-sm btn-primary"
                                                                    id="addNewRow">
                                                                <i class="fa fa-plus"></i></button>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="row row-xs">
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">From</label>
                                                        {!! Form::text('revenue_from[]', null, ['class'=>'form-control', 'placeholder' => ""]) !!}
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">To</label>
                                                        {!! Form::text('revenue_to[]', null, ['class'=>'form-control','placeholder' => ""]) !!}
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Revenue
                                                            Share Value</label>
                                                        <div class="input-group">
                                                            {!! Form::text('revenue_value[]', null, ['class'=>'form-control','placeholder' => ""]) !!}
                                                            <div class="input-group-append">
                                                                <select name="revenue_type[]" class="form-control">
                                                                    <option value="flat">₹</option>
                                                                    <option value="percentage">%</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1 mt-4">
                                                    <button type="button" class="btn btn-sm btn-primary"
                                                            id="addNewRow">
                                                        <i class="fa fa-plus"></i></button>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="col-sm-12">
                                <label for="">Payment Method: </label><br/>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio"
                                           name="is_other_payment_gateway"
                                           @if($data->is_other_payment_gateway == 0) checked
                                           @endif
                                           id="payuMoney" value="0">
                                    <label class="form-check-label" for="payuMoney">
                                        Payu Money
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio"
                                           name="is_other_payment_gateway"
                                           @if($data->is_other_payment_gateway == 1) checked @endif
                                           id="razorpay" value="1">
                                    <label class="form-check-label" for="razorpay">
                                        Razorpay
                                    </label>
                                </div>
                            </div>
                            <hr>
                            <h2>Courses Information</h2>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="allow_all_courses"
                                               @if($data->allow_all_courses == 1) checked @endif
                                               id="allowAllCourses">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Allow All Courses
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="allow_all_future_courses"
                                               @if($data->allow_all_future_courses == 1) checked @endif>
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Allow All Future Courses
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="show_webinars"
                                               @if($data->show_webinars == 1) checked @endif>
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Show Webinars
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="coursesSection" @if($data->allow_all_courses == 1) style="display: none;" @endif>
                                @foreach($courses as $course)
                                    <div class="col-md-3">
                                        <div class="card my-3">
                                            <div class="card-header">
                                                {{ $course->name }}
                                            </div>
                                            <div class="card-body pre-scrollable">
                                                @if(!empty($course->courses))
                                                    <ul class=" list-unstyled">
                                                        @foreach($course->courses as $category_course)
                                                            <li>
                                                                <label for="{{ $category_course->id }}">
                                                                    <input type="checkbox"
                                                                           value="{{ $category_course->id }}"
                                                                           @if(in_array($category_course->id, $partner_courses)) checked
                                                                           @endif
                                                                           id="{{ $category_course->id }}" name="courses[]">
                                                                    {{ $category_course->name }}
                                                                </label>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="row pt-3">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-success">Save Partner Settings</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="addRowHtml row row-xs" style="display: none;">
        <div class="col-sm-2">
            <div class="form-group">
                {!! Form::text('revenue_from[]', null, ['class'=>'form-control', 'placeholder' => ""]) !!}
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                {!! Form::text('revenue_to[]', null, ['class'=>'form-control','placeholder' => ""]) !!}
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('revenue_value[]', null, ['class'=>'form-control','placeholder' => ""]) !!}
                    <div class="input-group-append">
                        <select name="revenue_type[]" class="form-control">
                            <option value="flat">₹</option>
                            <option value="percentage">%</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-1">
            <button type="button" class="btn btn-sm btn-danger"
                    id="removeRow">
                <i class="fas fa-trash"></i></button>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        var $document = $(document);
        $document.ready(function () {
            $document.on('click', "#addNewRow", function () {
                var html = $(".addRowHtml").last().clone();
                html.show();
                $("#sectionRows").append(html);
            });
            $document.on('click', ".removeRow", function () {
                var self = $(this);
                self.parents(".addRowHtml").remove();
            });

            $document.on("click", "#allowAllCourses", function () {
                $("#coursesSection").toggle();
            });

            $document.on("click", "#approveWebsite", function () {
                $("#updateSection").toggle();
            });

            $document.on("click", ".revenueSharingOption", function () {
                if ($(this).val() == "fixedFees") {
                    $("#fixedFeesSection").show();
                    $("#sharingByCountSection").hide();
                }
                if ($(this).val() == "sharingByCount") {
                    $("#fixedFeesSection").hide();
                    $("#sharingByCountSection").show();
                }
            });
        });
    </script>
@endsection