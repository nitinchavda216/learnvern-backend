@if($data->partner_application_status == 0 || $data->partner_application_status == null)
    <a href="javascript:;" class="btn btn-primary btn-xs openApprovalPopup" id="{{ $data->id }}">
        <i class="fas fa-check"></i>
    </a>
@endif
@if($data->setup_fees_paid_status == 1/* && $data->setup_complete == 1*/)
    <a href="{{ route('partner_application_history.show',$data->id) }}" class="btn btn-primary btn-xs">
        <i class="fas fa-eye"></i>
    </a>
@endif
<a href="javascript:;"
   onclick="confirmDelete('{{ route('partner_application_history.status',['id'=>$data->id,'status'=>2]) }}',2)"
   class="btn btn-danger btn-xs">
    <i class="fas fa-trash"></i>
</a>