@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Detail Of Partner Application
                    <span class="pull-right d-inline-block float-right">
                    </span>
                </h2>
                <div id="data-container">
                    <div class="row row-xs">
                        <div class="col-md-4">
                            <h3><u>General Information</u></h3>
                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Business Name : </b></label>
                                        {{$data->name}}
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Email Address :</b></label>
                                        {{$data->contact_person_email}}
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Contact Number :</b></label>
                                        {{$data->contact_person_phone_number}}
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Short InFormation :</b></label>
                                        {{$data->description}}
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Count Of Students :</b></label>
                                        {{$data->current_students_count}}
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Website :</b></label>
                                        {{$data->website}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h3><u>Offical Information</u></h3>
                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Address Line :</b></label>
                                        {{$data->address_line1}}
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Landmark :</b></label>
                                        {{$data->address_line2}}
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>City Name :</b></label>
                                        {{$data->city}}
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>State Name :</b></label>
                                        {{$data->state}}
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Pin Code :</b></label>
                                        {{$data->pin_code}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h3><u>Branch Information</u></h3>
                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Address Line :</b></label>
                                        {{$data->branch_address_line1}}
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Landmark :</b></label>
                                        {{$data->branch_address_line2}}
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>City Name :</b></label>
                                        {{$data->branch_city}}
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>State Name :</b></label>
                                        {{$data->branch_state}}
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600"><b>Pin Code :</b></label>
                                        {{$data->branch_pin_code}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    View Agreement Document

                </div>
            </div>
        </div>
    </div>

@endsection
