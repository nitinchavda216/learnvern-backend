<a href="{{ route('quiz.edit', $quiz->curriculum_list_id) }}" class="btn btn-primary btn-xs d-inline">
    <i class="fas fa-edit"></i>
</a>
<a href="{{ route('quiz.questions', $quiz->curriculum_list_id) }}" class="btn btn-info btn-xs d-inline">
    Questions
</a>
<div class="d-inline activeStatus az-toggle az-toggle-secondary float-right {{ ($quiz->is_active == 1) ? 'on' : '' }}"
     data-id="{{ $quiz->curriculum_list_id }}">
    <span></span>
</div>
