<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Course :</label>
            {!! Form::select('course_id', $courses, $quiz->course_id ?? old('course_id'), ['id'=>'course_id', 'class'=>'form-control', 'data-validation' => 'required', 'placeholder' => ""]) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Section :</label>
            {!! Form::select('section_id', $sections, $quiz->section_id ?? old('section_id'), ['id'=>'section_id', 'class'=>'form-control', 'data-validation' => 'required', 'placeholder' => ""]) !!}
        </div>
    </div>
</div>
<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Quiz Name :</label>
            {!! Form::text('name', $quiz->name ?? old('name'), ['class'=>'form-control', 'id' => 'name', 'data-validation' => 'required']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Quiz Slug :</label>
            {!! Form::text('slug', $quiz->slug ?? old('slug'), ['class'=>'form-control', 'id' => 'slug', 'data-validation' => 'required', isset($quiz) ? 'disabled' : ""]) !!}
            <span id="slugmsg"></span>
        </div>
    </div>

</div>
<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Quiz Sub Title :</label>
            {!! Form::text('sub_title', $quiz->sub_title ?? old('sub_title'), ['class'=>'form-control', 'id' => 'sub_title']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Quiz Time :</label>
            <div class="input-group">
                {!! Form::text('time', $quiz->time ?? old('time'), ['class'=>'form-control form_time', 'id' => 'time', 'data-validation' => 'required']) !!}
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-clock"></i></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Quiz Retake :</label>
            {!! Form::text('retake', $quiz->retake ?? old('retake'), ['class'=>'form-control', 'id' => 'retake', 'data-validation' => 'required']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="ckbox">
                <input type="checkbox" @if(isset($quiz) && $quiz->evaluate == 1) checked @endif name="evaluate"><span>Auto Evaluate Results</span>
            </label>
        </div>
        <div class="form-group">
            <label class="ckbox">
                <input type="checkbox" @if(isset($quiz) && $quiz->is_master_quiz == 1) checked @endif
                name="is_master_quiz"><span>Master Quiz</span>
            </label>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Quiz Content :</label>
    {!! Form::textarea('content', $quiz->content ?? old('content'), ['class'=>'form-control wysiwyg_editor', 'id' => 'content']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Quiz Short Content :</label>
    {!! Form::textarea('short_content', $quiz->short_content ?? old('short_content'), ['class'=>'form-control wysiwyg_editor', 'id' => 'short_content']) !!}
</div>
<button type="submit" id="quizsubmit" class="btn btn-az-primary mg-md-t-9">{{ isset($quiz) ? "Update" : "Submit" }}</button>
@section('css')
    <link href="{{asset('admin/lib/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet">
@endsection
@section('footer_scripts')
    <script type="text/javascript"
            src="{{ asset('admin/lib/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script type="text/javascript">

        var is_edit = "{{ isset($quiz) ? true : false }}";

        $(document).ready(function () {
            $('#course_id').select2({
                placeholder: 'Select Course'
            });
            $(document).on('change', "#course_id", function () {
                $("#section_id").val(null).trigger('change');
            });
            $('.form_time').timepicker({format: 'hh:mm:ss', showSeconds: true, showMeridian: false, minuteStep: 1, secondStep: 1});
            $("#section_id").select2({
                placeholder: 'Select Section',
                ajax: {
                    url: $app_url + "/get-sections-by-course",
                    type: "get",
                    dataType: 'json',
                    data: function (params) {
                        var searchTerm = params.term;
                        return {
                            searchTerm: searchTerm,
                            courseId: $("#course_id").val()
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $("#name").focusout(function (i, d) {
                var title = $.trim($("#name").val());
                if (title.length > 0) {
                    title = title.toLowerCase();
                    title = title.replace(/[^a-z0-9\s]/gi, "").replace(/  +/g, " ").replace(/[_\s]/g, "-");
                }
                $("#slug").val(title);
                checkValidSlug();
            });
            $("#slug").focusout(function (e) {
                var slug = $("#slug").val().toLowerCase();
                $("#slug").val(slug);
                checkValidSlug();
            });
            function checkValidSlug() {
                if (!is_edit){
                    var slug=$("#slug").val();
                    $.ajax({
                        url: $app_url + '/slugExists',
                        method: 'POST',
                        data: {slug:slug},
                        success: function (data) {
                            if (data.status) {
                                toastr.error(data.message, "Error!", {timeOut: 2000});
                                $('#slug').removeClass('valid');
                                $('#slug').addClass('error');
                                $('#slug').parent('div').removeClass('has-success');
                                $('#slug').parent('div').addClass('has-error');
                                $('#quizsubmit').prop("disabled",true);


                            }else
                            {
                                $('#slug').removeClass('error');
                                $('#slug').addClass('valid');
                                $('#slug').parent('div').removeClass('has-error');
                                $('#quizsubmit').prop("disabled",false);
                            }
                        }
                    })
                }
            }
        })
    </script>
@endsection
