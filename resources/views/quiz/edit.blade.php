@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Edit Quiz
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('quiz') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="card card-body pd-40">
                        {!! Form::open(['route'=>['quiz.update', $quiz->id],'method' => 'POST', 'enctype'=> "multipart/form-data", 'files' => true, 'id' => "courseForm"]) !!}
                        @if(count($relatedCourses) > 0)
                            <div class="card">
                                <div class="card-header">
                                    <strong>Related Courses</strong>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered mg-b-0">
                                        <tbody>
                                        <tr>
                                            <th scope="row">
                                                {{ implode(', ', $relatedCourses) }}
                                            </th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br>
                        @endif
                        @include("quiz.form")
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

