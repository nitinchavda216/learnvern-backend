<h2 class="az-content-inner-section-title mx-0"> Add Category</h2>
<div class="card card-body">
    <form role="form" method="post" action="{{ route('categories.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Category Name</label>
                    {{ Form::text('name', old('name') ,['class' => 'form-control', 'data-validation' => 'required'] ) }}
                </div>
                <div class="form-group d-inline-block mr-3">
                    <label class="ckbox">
                        <input type="checkbox" name="is_webinar_category"><span>Is webinar category ?</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Category Icon </label>
                    <input type="file" accept="image/*" class="file-upload dropify"
                           data-show-remove="false" data-allowed-file-extensions="png jpg jpeg svg"
                           id="category_icon" name="category_icon"/>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Category Image </label>
                    <input type="file" accept="image/*" class="file-upload dropify"
                           data-show-remove="false" data-allowed-file-extensions="png jpg jpeg svg"
                           id="image" name="image"/>
                </div>
            </div>
        </div>
        <button class="btn btn-az-primary btn-sm">Add Category</button>
    </form>
</div>
