<h2 class="az-content-inner-section-title mx-0">Edit Category
    <span class="pull-right d-inline-block float-right">
        <a href="{{ route('categories') }}" class="btn btn-warning btn-sm mt-negative">
            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
        </a>
    </span>
</h2>
<div class="card card-body">
    <form role="form" method="post" action="{{ route('categories.update', $category->id) }} "
          enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Category Name</label>
                    {{ Form::text('name', $category->name ?? old('name') ,['class' => 'form-control', 'data-validation' => 'required'] ) }}
                </div>
                <div class="form-group d-inline-block mr-3">
                    <label class="ckbox">
                        <input type="checkbox" @if($category->is_webinar_category == 1) checked @endif name="is_webinar_category"><span>Is webinar category ?</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Category Icon </label>
                    <input type="file" class="dropify" data-show-remove="false"
                           data-allowed-file-extensions="png jpg jpeg svg"
                           @if(isset($category->category_icon)) data-default-file="{{ $category->category_icon }}"
                           @else data-validation="required"
                           @endif id="category_icon" name="category_icon"/>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Category Image </label>
                    <input type="file" class="dropify" data-show-remove="false"
                           data-allowed-file-extensions="png jpg jpeg svg"
                           @if(isset($category->image)) data-default-file="{{ $category->image }}"
                           @else data-validation="required"
                           @endif id="image" name="image"/>
                </div>
            </div>
        </div>
        <button class="btn btn-az-primary btn-sm">Update Category</button>
    </form>
</div>
