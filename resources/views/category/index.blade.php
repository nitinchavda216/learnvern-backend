@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> LMS Categories</h2>
                <div id="data-container">
                    <div class="row">
                        <div class="col-md-6">
                            <h2 class="az-content-inner-section-title border-bottom mx-0">Category List
                                <span class="pull-right d-inline-block float-right">
                                    <a href="{{ route('categories.get_sort_order') }}"
                                       class="btn btn-success btn-sm mt-negative">Sort Category Order</a>
                                </span>
                            </h2>
                            <table id="dataTableList" class="display responsive">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Category Icon</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $item)
                                    <tr>
                                        <td>{{$item->name}}</td>
                                        <td>
                                            <img src="{{ $item->category_icon }}"
                                                 class="img-fluid"
                                                 style="height: 40px;" alt="">
                                        </td>
                                        <td>
                                            <a href="javascript:;" data-id="{{ $item->id }}"
                                               class="btn btn-primary btn-xs d-inline editCategory">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <div class="d-inline az-toggle az-toggle-secondary float-right {{ ($item->is_active == 1) ? 'on' : '' }}"
                                                 data-id="{{ $item->id }}">
                                                <span></span>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6" id="categoryForm">
                            {!! $add_category !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        var $document = $(document);
        $document.ready(function () {
            $('#dataTableList').DataTable({
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                }
            });
            // Toggle Switches
            $document.on('click', ".az-toggle", function () {
                var status = ($(this).toggleClass('on').hasClass('on') == true) ? 1 : 0;
                var id = $(this).data('id');
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: $app_url + '/category/update_status',
                    data: {'is_active': status, 'id': id},
                    success: function () {
                        toastr.success("Status Updated Successfully!", "Success!", {timeOut: 2000});
                    }
                });
            });
            $document.on('click', ".editCategory", function () {
                var category_id = $(this).data('id');
                $.ajax({
                    type: "get",
                    dataType: "json",
                    url: $app_url + '/category/edit/' + category_id,
                    data: {},
                    beforeSend: function () {
                        $("#categoryForm").html("");
                    },
                    success: function (data) {
                        $("#categoryForm").html(data.html);
                        $(".dropify").dropify();
                        $.validate();
                    }
                });
            });
        });
    </script>
@endsection
