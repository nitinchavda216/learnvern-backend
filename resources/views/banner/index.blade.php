@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Banner List
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('banner.create') }}" class="btn btn-success btn-sm mt-negative">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="row">
                        <div class="col-md-12">

                            <table id="dataTableList" class="display responsive">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Image</th>
                                    <th>Type</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($allbanners)
                                    @foreach($allbanners as $item)
                                        <tr>
                                            <td>{{$item->title}}</td>
                                            <td>
                                                <img src="{{ showBannerImage($item->image) }}"
                                                     class="img-fluid"
                                                     style="height: 40px;" alt="">
                                            </td>
                                            <td>{{$item->type}}</td>
                                            <td>
                                                <a href="{{ route('banner.edit', $item->id) }}"
                                                   class="btn btn-primary btn-xs">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <a href="javascript:;"
                                                   onclick="confirmDelete('{{ route('banner.delete',$item->id) }}')"
                                                   class="btn btn-danger btn-xs">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        var $document = $(document);
        $document.ready(function () {
            $('#dataTableList').DataTable({
                responsive: true,
                aaSorting: [],
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ records'
                }
            });
            // Toggle Switches
            $document.on('click', ".az-toggle", function () {
                var status = ($(this).toggleClass('on').hasClass('on') == true) ? 1 : 0;
                var id = $(this).data('id');
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: $app_url + '/category/update_status',
                    data: {'is_active': status, 'id': id},
                    success: function () {
                        toastr.success("Status Updated Successfully!", "Success!", {timeOut: 2000});
                    }
                });
            });
            $document.on('click', ".editCategory", function () {
                var category_id = $(this).data('id');
                $.ajax({
                    type: "get",
                    dataType: "json",
                    url: $app_url + '/category/edit/' + category_id,
                    data: {},
                    beforeSend: function () {
                        $("#categoryForm").html("");
                    },
                    success: function (data) {
                        $("#categoryForm").html(data.html);
                        $(".dropify").dropify();
                        $.validate();
                    }
                });
            });

            // sort Order

            var target = $('.sort_menu');
            target.sortable({
                connectWith: ".connected",
                onDragOver: function (e, ui) {
                    ui.placeholder.height(ui.helper.outerHeight());
                },

                onDragOver: function (e, ui) {
                    var sortData = target.sortable('toArray', {attribute: 'data-id'});
                    updateToDatabase(sortData.join(','));
                }

            });

            function updateToDatabase(idString) {
                $.ajax({
                    url: $app_url + '/category/sort_order/update',
                    method: 'POST',
                    data: {ids: idString},

                    success: function () {
                        if (data.status) {
                            toastr.success(data.message, "Success!", {timeOut: 2000});
                        } else {
                            toastr.error(data.message, "Error!", {timeOut: 2000});
                        }
                    }
                })
            }

        });

    </script>
@endsection


