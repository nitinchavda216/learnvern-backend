<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title:</label>
            {!! Form::text('title', $banner->title ?? old('title'), ['class'=>'form-control', 'id' => 'name', 'data-validation' => 'required']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Type:</label>
            {!! Form::select('type', \App\Models\Banners::TYPE_BANNER, $banner->type ?? old('type'), ['id'=>'bannerType', 'class'=>'form-control select2_no_search', 'data-validation' => 'required', 'placeholder' => ""]) !!}
        </div>
    </div>
</div>
<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Image:</label>
            <input type="file" accept="image/*" class="course_image file-upload" data-show-remove="false"
                   data-allowed-file-extensions="png jpg jpeg svg"
                   @if(isset($banner) && isset($banner->image)) data-default-file="{{ showBannerImage($banner->image) }}"
                   @else
                   data-validation="required" @endif
                   name="image" id="course_icon">
        </div>
    </div>
    <div class="col-sm-6">
        <div id="linkTypeSelectInputDiv" style="display: @if(isset($banner) && ($banner->type == "promotion_banner")) block @else none @endif;">
            <div class="form-group">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Link Type:</label>
                {!! Form::select('link_type', \App\Models\Banners::LINK_TYPES, $banner->link_type ?? old('link_type'), ['id'=>'linkType', 'class'=>'form-control', 'placeholder' => "--SELECT--"]) !!}
            </div>
            <div class="form-group">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">User Auth Type:</label>
                {!! Form::select('user_auth_type', \App\Models\Banners::USER_AUTH_TYPES, $banner->user_auth_type ?? old('user_auth_type'), ['class'=>'form-control userAuthTypes']) !!}
            </div>
        </div>
        <div class="form-group" id="courseSelectInputDiv" style="display: @if(isset($banner) && ($banner->link_type == "course_page")) block @else none @endif;">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Course/Webinar:</label>
            {!! Form::select('link_course_id', $courses, $banner->link_course_id ?? old('link_course_id'), ['class'=>'form-control linkCourseId', 'placeholder' => "--SELECT--", 'data-validation' => 'required']) !!}
        </div>
    </div>
</div>
<button type="submit" id="assignsubmit"
        class="btn btn-az-primary mg-md-t-9">{{ isset($banner) ? "Update" : "Submit" }}</button>
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('change', '#bannerType', function () {
                $("#linkTypeSelectInputDiv").toggle($(this).val() == 'promotion_banner');
                $('#linkType').select2({
                    minimumResultsForSearch: Infinity
                });
                $('.userAuthTypes').select2();
                if($(this).val() != 'promotion_banner'){
                    $("#courseSelectInputDiv").hide();
                    $("#linkType").val(null).trigger('change');
                }
            });
            $(document).on('change', '#linkType', function () {
                $("#courseSelectInputDiv").toggle($(this).val() == 'course_page');
                $('.linkCourseId').select2();
            });

            $('#linkType').select2({
                minimumResultsForSearch: Infinity
            });
            $('.linkCourseId').select2();
            $('.userAuthTypes').select2();
        });
    </script>
@endsection
