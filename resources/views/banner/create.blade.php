@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Create Banner
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('banner') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="card card-body pd-40">
                        {!! Form::open(['route'=>'banner.store','method' => 'POST', 'enctype'=> "multipart/form-data", 'files' => true, 'id' => "bannerForm"]) !!}
                        @include("banner.form", ['formMode' => 'create'])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="attachmentDiv p-2" style="display: none;">
        <div class="row row-sm">
            <div class="col-sm-4">
                <div class="form-group">
                    {!! Form::text('title_attachment[]',old('name'), ['class'=>'form-control', 'id' => 'title_attachment', 'placeholder' => 'Enter Title..', 'data-validation' => 'required']) !!}
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <input type="file" class="attachment-dropify" id="attachment" name="attachment[]" data-show-remove="false" data-validation="required">
                    <input type="hidden" name="remove_attachment" id="removeAttachment" value="false">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <button type="button" class="btn btn-sm btn-danger removeRow"><i
                                class="fa fa-trash"></i></button>
                </div>
            </div>
        </div>
    </div>
@endsection

