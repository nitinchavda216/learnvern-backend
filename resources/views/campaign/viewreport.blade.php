@extends('layout.master')
@section('content')
    <div class="az-content" style="min-height: 85vh;">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title"> Campaign Name: {{ $report->name }}
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('campaign') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>

                <div id="data-container">
                    <div class="pd-20 pd-sm-20 mb-3 bg-gray-200" id="filterDiv" style="display: {{count($_GET)?'block':'none'}};">

                        {!! Form::open(['method' => 'get', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                        <div class="row">
                            @if($report->marketing_goal == "revenue")
                            <div class="col-md-4">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Payment Done</label>
                                <select class="form-control" id="payment_status" name="payment_status">
                                    <option value="">--Both--</option>
                                    <option value="1" {{ isset($_GET['payment_status']) && $_GET['payment_status']=="1"?'selected':''  }}>Yes</option>
                                    <option value="0" {{ isset($_GET['payment_status']) && $_GET['payment_status']=="0"?'selected':''  }}>No</option>
                                </select>
                            </div>
                           @endif

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="amount">Course Completed Range</label>
                                    <span id="amount" readonly style="border:0; color:#0062cc; font-weight:bold;">
                                        @if(isset($_GET['progress']) && $_GET['progress'])
                                            @php
                                                     $progressArray = explode('-',$_GET['progress']);
                                            @endphp

                                            ({{$progressArray[0].'% - '.$progressArray[1]}}%)
                                        @else
                                            (0% - 100%)
                                        @endif
                                    </span>
                                    <input type="hidden" name="progress" id="progress" value="{{ $_GET['progress']??'' }}">
                                </div>
                                <div class="form-group">
                                    <div id="slider-range"></div>
                                </div>
                            </div>


                            <div class="col-md-4 mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <a href="{{route('campaign.viewReport',$report->id)}}" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                </a>
                            </div>

                        </div>



                        {!! Form::close() !!}

                    </div>


                    <div class="row">
                        <div class="col-md-9">

                          <div class="row">
                        <div class="col-md-6">
                            <h5>Type Of Message: {{ $report->action }}</h5>
                        </div>
                        <div class="col-md-6">
                            <h5>Number Of Contacts: {{ $count }}</h5>
                        </div>
                        @if($report->marketing_goal == "referral")
                            @if($summaryData['totalReferalCountBeforeCampaign'] && $summaryData['totalReferalCountAfterCampaign'])
                            <div class="col-md-6">
                                <h5>Engage (%): {{ round(($summaryData['totalReferalCountAfterCampaign'] / $summaryData['totalReferalCountBeforeCampaign'])*100) }}</h5>
                            </div>
                            @endif
                            <div class="col-md-6">
                                <h5>All Referral Count Before Campaign: {{ $summaryData['totalReferalCountBeforeCampaign'] ? $summaryData['totalReferalCountBeforeCampaign'] : 0 }}</h5>
                            </div>
                            <div class="col-md-6">
                                <h5>All Referral Count After Campaign: {{ $summaryData['totalReferalCountAfterCampaign'] ? $summaryData['totalReferalCountAfterCampaign'] : 0 }}</h5>
                            </div>

                        @endif

                        @if($report->marketing_goal == "revenue")
                            <div class="col-md-6">
                                <h5>Number of Payment Completed: {{ $summaryData['totalNumberOfPaymentCompleted']? $summaryData['totalNumberOfPaymentCompleted'] : 0 }}</h5>
                            </div>
                        @endif

                        <div class="col-md-6">
                            <h5>Agenda: {{ $report->marketing_goal }}</h5>
                        </div>
                    </div>

                        </div>

                        <div class="col-md-3 text-right">

                                    <a href="javascript:;" class="btn btn-info btn-sm mt-negative" id="filterButton" title="Filter">
                                        <i class="fa fa-filter"></i></a>

                                    {{--<a href="javascript:;" class="btn btn-primary btn-sm mt-negative" title="Add To Campaign" id="openCampaignModal">--}}
                                        {{--Add New Campaign</a>--}}

                        </div>

                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            @if($report->marketing_goal == "referral")
                            <table id="dataTableList" class="display responsive">
                                <thead>
                                <tr>
                                    {{--<th><input type="checkbox" id="checkAll"></th>--}}
                                    <th>Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Referral Code</th>
                                    <th>Referral Code Link</th>
                                    <th>Number Of Courses Enrolled</th>
                                    <th>Course Name Of Highest %</th>
                                    <th>Highest(%) Of Course Completed</th>
                                    <th>Number Of Referrals Signed Up Before Campaign</th>
                                    <th>Number Of Referrals Signed Up Since Campaign</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            @elseif($report->marketing_goal == "revenue")
                                <table id="dataTableList" class="display responsive">
                                    <thead>
                                    <tr>
                                        {{--<th><input type="checkbox" id="checkAll"></th>--}}
                                        <th>Name</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
                                        <th>Referral Code</th>
                                        <th>Referral Code Link</th>
                                        <th>Course Name</th>
                                        <th>(%) Course Completed</th>
                                        <th>Payment Done</th>
                                        <th>Last Watch Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            @else

                                <table id="dataTableList" class="display responsive">
                                    <thead>
                                    <tr>
                                        {{--<th><input type="checkbox" id="checkAll"></th>--}}
                                        <th>Name</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
                                        <th>Referral Code</th>
                                        <th>Referral Code Link</th>
                                        <th>Course Name</th>
                                        <th>(%)Improvement from before to after</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>


                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('campaign.modals.add_to_sub_campaign')
    @include('campaign.modals.check_all_confirmation')
@endsection
@section('css')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        .previous-link {
            width: 70px;
        }

        .next-link {
            width: 50px;
        }
        .select2-container {
            width: 100% !important;
        }
    </style>
@endsection
@section('footer_scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">
        var users = [];
        var unselect_users = [];
        var allCheckboxesList = [];
        var checkAllPagesCheckboxes = false;
        var report_id = {{ $report->id }};

        @if(isset($progressArray))

        var sliderPrefiled = [{{$progressArray[0].','.$progressArray[1]}}];
        @else
        var sliderPrefiled = [0,100];

        @endif

        $(document).ready(function () {

                        @if($report->marketing_goal == "referral")
                var columnDefination = [
//                        {data: 'checkbox', 'sortable': false, 'width': "5%",},
                        {data: 'first_name', 'sortable': false, 'width': "15%"},
                        {data: 'mobile_number', 'sortable': false, 'width': "10%"},
                        {data: 'email', 'sortable': false, 'width': "10%"},
                        {data: 'own_referral_code', 'sortable': false, 'width': "5%"},
                        {data: 'own_referral_link', 'sortable': false, 'width': "15%"},
                        {data: 'no_of_course_enroll', 'sortable': true, 'width': "5%"},
                        {data: 'course_name', 'sortable': false, 'width': "20%"},
                        {data: 'high_progress', 'sortable': true, 'width': "5%"},
                        {data: 'total_referral_before_campaign', 'sortable': true, 'width': "7%"},
                        {data: 'total_referral_after_campaign', 'sortable': true, 'width': "8%"},
                    ];

                      @elseif($report->marketing_goal == "revenue")
                var columnDefination = [
//                        {data: 'checkbox', 'sortable': false, 'width': "5%",},
                        {data: 'first_name', 'sortable': false, 'width': "10%"},
                        {data: 'mobile_number', 'sortable': false, 'width': "5%"},
                        {data: 'email', 'sortable': false, 'width': "10%"},
                        {data: 'own_referral_code', 'sortable': false, 'width': "5%"},
                        {data: 'own_referral_link', 'sortable': false, 'width': "20%"},
                        {data: 'course_name', 'sortable': false, 'width': "20%"},
                        {data: 'high_progress', 'sortable': true, 'width': "10%"},
                        {data: 'paid_status', 'sortable': true, 'width': "8%"},
                        {data: 'last_used_date', 'sortable': true, 'width': "12%"},
                    ];

                        @else
                var columnDefination = [
//                        {data: 'checkbox', 'sortable': false, 'width': "5%",},
                        {data: 'first_name', 'sortable': false, 'width': "20%"},
                        {data: 'mobile_number', 'sortable': false, 'width': "15%"},
                        {data: 'email', 'sortable': false, 'width': "15%"},
                        {data: 'own_referral_code', 'sortable': false, 'width': "5%"},
                        {data: 'own_referral_link', 'sortable': false, 'width': "15%"},
                        {data: 'course_name', 'sortable': false, 'width': "25%"},
                        {data: 'campaign_after_progress', 'sortable': true, 'width': "5%"},
                    ];


                        @endif

                var $dataTableList = $('#dataTableList').DataTable({
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        bFilter: false,
                        "aaSorting": [],
                        ajax: {
                            url: $app_url + '/campaign/viewReportAjaxData',
                            data: function (d) {
                                d.report_id = report_id;
                                d.progress='{{$_GET['progress']??''}}';
                                d.payment_status='{{$_GET['payment_status']??''}}';
                            }
                        },
                        "columns": columnDefination,
                        select: true,
                        bStateSave: true,
                        fnStateSave: function (settings, data) {
                            localStorage.setItem("dataTables_state", JSON.stringify(data));
                        },
                        fnStateLoad: function (settings) {
                            return JSON.parse(localStorage.getItem("dataTables_state"));
                        }
                    });


            $("#dataTableList").on('draw.dt', function () {
                var info = $dataTableList.page.info();
                if (checkAllPagesCheckboxes){
                    $('.user_id').attr('checked', true);
                    for (var i = 0; i < unselect_users.length; i++) {
                        checkboxId = unselect_users[i];
                        $('#' + checkboxId).attr('checked', false);
                    }
                }else{
                    for (var i = 0; i < users.length; i++) {
                        checkboxId = users[i];
                        $('#' + checkboxId).attr('checked', true);
                    }
                    if (jQuery.inArray(info.page, allCheckboxesList) != -1) {
                        $("#checkAll").prop('checked', true);

                        $('.user_id').each(function () {
                            $(this).prop('checked', true);
                            var id = $(this).val();
                            if (jQuery.inArray(id, users) == -1) {
                                users.push(id);
                            }
                        });
                        $('[name=course_user_ids]').val(JSON.stringify(users));
                    } else {
                        if (!checkAllPagesCheckboxes){
                            $("#checkAll").prop('checked', false);
                        }
                    }
                }
            });

            $(document).on('click', '#openCampaignModal', function () {
                if (checkAllPagesCheckboxes || (users.length > 0)){
                    $("#addToCampaignModal").modal('show');
                }else{
                    toastr.error("Select at least one checkbox!", "Error!", {timeOut: 2000});
                }
            });

            $(document).on('click', '#checkAll', function () {
                var info = $dataTableList.page.info();
                var self = $(this);
                if (info.recordsDisplay > 0) {
                    if (self.is(':checked')) {
                        $("#checkAll").prop('checked', false);
                        $("#checkAllConfirmationModal").modal('show');
                    } else {
                        $('input[type="checkbox"]').each(function () {
                            var current_user = $(this).prop('checked', false);
                            users = jQuery.grep(users, function (value) {
                                return value != current_user.val();
                            });
                        });
                        if (jQuery.inArray(info.page, allCheckboxesList) != -1) {
                            allCheckboxesList = jQuery.grep(allCheckboxesList, function (value) {
                                return value != info.page;
                            });
                        }
                        checkAllPagesCheckboxes = false;
                        $("#checkAllUsers").val(checkAllPagesCheckboxes);
                    }
                    $('[name=course_user_ids]').val(JSON.stringify(users));
                }else{
                    $("#checkAll").prop('checked', false);
                }
            });


            $(document).on('click', '#submitCheckboxEvent', function () {
                $("#checkAllConfirmationModal").modal('hide');
                var info = $dataTableList.page.info();
                var val = $("input[name=check_page]:checked").val();
                $("#checkAll").prop('checked', true);
                if (val == "one") {
                    users = [];
                    checkAllPagesCheckboxes = false;
                    allCheckboxesList.push(info.page);
                } else {
                    checkAllPagesCheckboxes = true;
                    $("#checkAllUsers").val(checkAllPagesCheckboxes);
                    $("#checkAll").prop('disabled', true);
                }
                $('.user_id').each(function () {
                    var current_user = $(this).prop('checked', true);
                    if (jQuery.inArray($(current_user).val(), users) == -1) {
                        users.push($(current_user).val());
                    }
                });
                $('[name=course_user_ids]').val(JSON.stringify(users));
            });

            $('.select2Modal').select2({
                placeholder: '--SELECT--',
                dropdownParent: $("#addToCampaignModal")
            });


            $(document).on('click', '#filterButton', function () {
                $('#filterDiv').toggle();
            });


            $(document).on('click', '.user_id', function () {
                var info = $dataTableList.page.info();
                var self = $(this);
                if (checkAllPagesCheckboxes){
                    if (self.is(':checked')) {
                        unselect_users = jQuery.grep(unselect_users, function (value) {
                            return value != self.val();
                        });
                    } else {
                        if (jQuery.inArray(self.val(), unselect_users) == -1) {
                            unselect_users.push(self.val());
                        }
                    }
                }else{
                    if (self.is(':checked')) {
                        if (jQuery.inArray(self.val(), users) == -1) {
                            users.push(self.val());
                        }
                    } else {
                        users = jQuery.grep(users, function (value) {
                            return value != self.val();
                        });
                        $("#checkAll").prop('checked', false);
                        checkAllPagesCheckboxes = false;
                        $("#checkAllUsers").val(checkAllPagesCheckboxes);
                        if (jQuery.inArray(info.page, allCheckboxesList) != -1) {
                            allCheckboxesList = jQuery.grep(allCheckboxesList, function (value) {
                                return value != info.page;
                            });
                        }
                    }
                }
                $('[name=course_user_ids]').val(JSON.stringify(users));
                $('[name=unselect_course_user_ids]').val(JSON.stringify(unselect_users));
            });

            if($("#slider-range").length > 0) {
                $("#slider-range").slider({
                    range: true,
                    min: 0,
                    max: 100,
                    values: sliderPrefiled,
                    slide: function (event, ui) {
                        $("#amount").html("("+ui.values[0] + "% - " + ui.values[1] + "%)");
                        $("#progress").val(ui.values[0] + "-" + ui.values[1]);
                    }
                });
            }

        });
    </script>
@endsection
