<a href="{{route('campaign.viewReport', $item->id)}}"
    class="btn btn-secondary btn-xs" title="View Report Data">
    <i class="fa fa-chart-bar" aria-hidden="true"></i> Statistics
</a>
<a href="{{route('campaign.export', $item->id)}}" class="btn btn-dark btn-xs" title="View Report Data">
    <i class="fa fa-download" aria-hidden="true"></i> Export
</a>
@if($item->is_complete == 0)
{{--<a href="{{route('campaign.markascomplete', $item->id)}}" class="btn btn-success btn-xs" title="View Report Data">--}}
    {{--<i class="fa fa-check" aria-hidden="true"></i> Mark As Complete--}}
{{--</a>--}}
@endif
{{--@if($item->action == "Calling Users")--}}
    {{--<a href="{{route('campaign.calling', $item->id)}}" class="btn btn-primary btn-xs" title="View Report Data">--}}
        {{--<i class="fa fa-phone" aria-hidden="true"></i> Calling Report--}}
    {{--</a>--}}
{{--@endif--}}
<a href="javascript:;" onclick="confirmDelete('{{ route('campaign.destroy',$item->id) }}')" class="btn btn-danger btn-xs" title="Delete">
    <i class="fa fa-trash" aria-hidden="true"></i>
</a>