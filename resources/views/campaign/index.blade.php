@extends('layout.master')
@section('content')
    <div class="az-content" style="min-height: 85vh;">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title"> Campaign </h2>
                <div id="data-container">
                    <div class="row">
                        <div class="col-12">
                            {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                            <div class="mb-3">
                                <div class="row">
                                    <div class="col-3">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Marketing Goal: </label>
                                        {!! Form::select('marketing_goal', ['growth'=>'Growth','referral'=>'Referral','revenue'=>'Revenue'], [],['id' => 'marketing_goal', 'class' => 'form-control select2', 'placeholder' => ""]) !!}
                                    </div>
                                    <div class="col-3">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Action Type :</label>
                                        {!! Form::select('action', \App\Models\Campaign::ACTIONS, [],['id' => 'action', 'class' => 'form-control select2', 'placeholder' => ""]) !!}
                                    </div>
                                    <div class="col-3">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Assignee Name: </label>
                                        {!! Form::select('accessible_to',$adminNames, [],['id' => 'accessible_to', 'class' => 'form-control select2', 'placeholder' => ""]) !!}
                                    </div>
                                    <div class="col-3 mt-4">
                                        <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                        <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <table id="dataTableList" class="display responsive">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Action Type</th>
                                    <th>Marketing Goal</th>
                                    <th>Created By</th>
                                    <th>Assignee Name</th>
                                    <th>No Of User In Campaign</th>
                                    <th>Created Date</th>
                                    <th></th>
                                </tr>

                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
                var $dataTableList = $('#dataTableList').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    bFilter: false,
                    "aaSorting": [],
                    ajax: {
                        url: $app_url + '/campaign/getAjaxListData',
                        data: function (d) {
                            d.marketing_goal = $('#marketing_goal').val();
                            d.action = $('#action').val();
                            d.accessible_to = $('#accessible_to').val();
                        }
                    },
                    "columns": [
                        {data: 'name', 'sortable': false, 'width': "15%"},
                        {data: 'action', 'sortable': false, 'width': "10%"},
                        {data: 'marketing_goal', 'sortable': false, 'width': "10%"},
                        {data: 'created_by_name', 'sortable': false, 'width': "10%"},
                        {data: 'assign_to_user_name', 'sortable': false, 'width': "10%"},
                        {data: 'number_of_user_in_campaign', 'sortable': false, 'width': "5%"},
                        {data: 'created_at', 'sortable': false, 'width': "10%"},
                        {data: 'actions_menu', 'orderable': false, 'width': "30%"},
                    ],
                    select: true,
                    bStateSave: true,
                    fnStateSave: function (settings, data) {
                        localStorage.setItem("dataTables_state", JSON.stringify(data));
                    },
                    fnStateLoad: function (settings) {
                        return JSON.parse(localStorage.getItem("dataTables_state"));
                    }
                });


            $('#search-form').on('submit', function (e) {
                $dataTableList.draw();
                e.preventDefault();
            });

            $('#reset-filters').on('click', function () {
                $("#marketing_goal").val('').trigger('change');
                $("#action").val('').trigger('change');
                $("#accessible_to").val('').trigger('change');
                $dataTableList.draw();
            });

        });

    </script>
@endsection
