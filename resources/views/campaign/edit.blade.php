@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Edit Campaign
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('campaign') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>
                <div id="data-container">
                    <div class="card card-body">
                        {!! Form::open(['route'=>['campaign.update', $campaignDetail->id],'method' => 'POST', 'enctype'=> "multipart/form-data", 'files' => true, 'id' => "update-campaign"]) !!}
                        <input type="hidden" name="campaign_id" id="campaign_id" value="{{$campaignDetail->id}}"/>
                        <input type="hidden" name="number_of_user_campaign" id="number_of_user_campaign"
                               value="{{$numberOfUserCampaign}}"/>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Campaign Name</label>
                                {!! Form::text('name', $campaignDetail->name, ['class'=>'form-control', 'id' => 'name', 'placeholder' => 'Campaign Name', 'data-validation' => 'required']) !!}
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>General Marketing Goal</label>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label class="rdiobox">
                                            <input name="marketing_goal" type="radio" value="revenue"
                                                   data-validation="required" {{ $campaignDetail->marketing_goal == 'revenue'? 'checked':'' }}>
                                            <span>Revenue</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="rdiobox">
                                            <input name="marketing_goal" type="radio"
                                                   value="growth" {{ $campaignDetail->marketing_goal == 'growth'? 'checked':'' }}>
                                            <span>Growth</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="rdiobox">
                                            <input name="marketing_goal" type="radio"
                                                   value="referral" {{ $campaignDetail->marketing_goal == 'referral'? 'checked':'' }}>
                                            <span>Referral</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group mt-3">
                                <label class="ckbox">
                                    <input type="checkbox" id="is_allow_duplicate" name="is_allow_duplicate"
                                           value="1"><span>Calling User Action Allow Duplicate User In Same Campaign Goal?</span>
                                </label>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label>Campaign Action</label>
                                <div class="row">
                                    @foreach(\App\Models\Campaign::ACTIONS as $key => $value)
                                        @if($key != 'push-notification')
                                        <div class="col-sm-3">
                                            <label class="rdiobox">
                                                <input name="action" type="radio" value="{{$key}}"
                                                       {{$key == $campaignDetail->action ? 'checked':''}}  data-validation="required">
                                                <span>{{$value}}</span>
                                            </label>
                                        </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-6" id="assignUserMultipleForCalling"
                             style="display: {{$campaignDetail->action == 'calling-user' ?'block':'none' }};">
                            <div class="form-group">
                                <label>Assign To Multiple User </label>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label class="rdiobox">
                                            <input name="chunk_type" type="radio"
                                                   value="1" {{count($campaignAssignDetail) > 1 ? 'checked':''}}>
                                            <span>Yes</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="rdiobox">
                                            <input name="chunk_type" type="radio"
                                                   value="0" {{count($campaignAssignDetail) == 1 ? 'checked':''}}>
                                            <span>No</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3" id="filterTotalDiv"
                             style="display: {{$campaignDetail->action == "calling-user" ? 'block':'none'}};">
                            <div class="col-6"><h5>Filter Total <span
                                        id="number_of_user_campaign_span">{{$numberOfUserCampaign}}</span> Records</h5>
                            </div>
                        </div>
                        <div id="singleChunck" style="display: {{count($campaignAssignDetail) == 1 ? 'block':'none'}};">
                            <div class="form-group">
                                <label>Assign To</label>
                                {!! Form::select('assign_to', $adminNames ,count($campaignAssignDetail) == 1 ? $campaignAssignDetail[0]['assign_to']:'', ['class'=>'form-control', 'id' => 'assign_to', 'placeholder' => '', 'data-validation' => 'required']) !!}
                            </div>
                        </div>
                        <div id="multipleChunck" style="display: {{count($campaignAssignDetail) > 1 ?'block':'none'}};">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Number Of User</label>
                                        <select name="number_of_user_assign" class="form-control"
                                                id="number_of_user_assign" data-validation="required">
                                            <option value=""></option>
                                            @for($i= 1; $i<=10; $i++)
                                                <option
                                                    value="{{$i}}" {{count($campaignAssignDetail) == $i ?'selected':''}}>{{$i}}</option>
                                            @endfor
                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <table id="assignTable" class="dataTable">
                                        <tr>
                                            <th width="50%">Assign To</th>
                                            <th width="50%">Number Of User Assign</th>
                                        </tr>
                                        <tr style="display: none">
                                            <td>
                                                {!! Form::select('chunk_distribute[name][]', $adminNames , '', ['class'=>'form-control', 'placeholder' => '', 'data-validation' => 'required']) !!}
                                            </td>
                                            <td>
                                                {!! Form::text('chunk_distribute[qty][]', '', ['class'=>'form-control distributeQty', 'placeholder' => 'Value', 'data-validation' => 'required']) !!}
                                            </td>
                                        </tr>
                                        @if(count($campaignAssignDetail) > 0)
                                            @foreach($campaignAssignDetail as $item)
                                                <tr>
                                                    <td>
                                                        {!! Form::select('chunk_distribute[name][]', $adminNames , $item->assign_to, ['class'=>'form-control', 'placeholder' => '', 'data-validation' => 'required']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('chunk_distribute[qty][]', $item->number_of_user, ['class'=>'form-control distributeQty', 'placeholder' => 'Value', 'data-validation' => 'required']) !!}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Update
                            </button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('footer_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"
            integrity="sha512-eYSzo+20ajZMRsjxB6L7eyqo5kuXuS2+wEbbOkpaur+sA2shQameiJiWEzCIDwJqaB0a4a6tCuEvCOBHUg3Skg=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript">
        $(function () {


            $(document).on('click', "input[name='action']", function () {
                if ($(this).val() == "calling-user") {
                    if ($('input[name="chunk_type"]:checked').val() == 0) {
                        $("#singleChunck").show();
                        $("#multipleChunck").hide();
                    } else {
                        $("#singleChunck").hide();
                        $("#multipleChunck").show();
                    }


                    $.ajax({
                        type: "post",
                        dataType: "json",
                        url: $app_url + '/campaign/verifyUserForUpdate',
                        data: $('#update-campaign').serialize(),
                        beforeSend: function () {
                            $.blockUI();
                            $(".blockOverlay").css('z-index', '9999');
                            $(".blockPage").css('z-index', '9999');
                        },
                        success: function (response) {

                            $.unblockUI();
                            if (response.total_result_count > 0) {
                                $("#number_of_user_campaign").val(response.total_result_count);
                                $("#number_of_user_campaign_span").html(response.total_result_count);
                                $("#filterTotalDiv").show();
                                $("#assignUserMultipleForCalling").show();
                            } else {
                                $("#addToCampaignModal").modal('hide');
                                toastr.error("No user allow in this campaign", "Error!", {timeOut: 2000});
                            }
                        },
                        error: function (data) {
                            $.unblockUI();
                            toastr.error("There was an Error!", "Error!", {timeOut: 2000});
                        }

                    });
                } else {
                    $("#filterTotalDiv").hide();
                    $("#assignUserMultipleForCalling").hide();
                    $("#singleChunck").show();
                    $("#multipleChunck").hide();
                }
            });


            $(document).on('click', "input[name='chunk_type']", function () {
                if ($(this).val() == 0) {
                    $("#singleChunck").show();
                    $("#multipleChunck").hide();

                } else {
                    $("#singleChunck").hide();
                    $("#multipleChunck").show();
                }
            });

            $(document).on('change', '#number_of_user_assign', function () {

                if ($(this).val() != '') {
                    var total = $("#number_of_user_campaign").val();
                    var numberOfUser = $(this).val();
                    var allotmentPerUser = Math.round(total / numberOfUser);
                    var lastUserAllotment = Math.abs((allotmentPerUser * (numberOfUser - 1)) - total);

                    $("#assignTable").find("tr:gt(1)").remove();
                    var new_line = '';
                    for (var i = 1; i <= numberOfUser; i++) {
                        new_line += '<tr>' + $("#assignTable").find('tr:eq(1)').html() + '</tr>';
                    }
                    $("#assignTable").append(new_line);
                    $("#assignTable").find("tr:gt(1)").find(".distributeQty").val(allotmentPerUser);
                    $('#assignTable tr:last').find(".distributeQty").val(lastUserAllotment);
                } else {
                    $("#assignTable").find("tr:gt(1)").remove();
                }
                return true;
            });


            $(document).on('change', 'input[name="is_allow_duplicate"]', function () {

                if ($('input[name="action"]:checked').val() == "calling-user") {
                    $('input[name="action"]:radio:last').click();
                }

            });

        });
    </script>
@endsection
