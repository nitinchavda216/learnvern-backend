@extends('layout.master')
@section('content')
    <div class="az-content" style="min-height: 85vh;">
        <div class="container">
            <div class="az-content-body" style="overflow-x: auto" >
                <h2 class="az-content-title"> Calling Report For Campaign Name: {{$campaignDetail->name}}
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('campaign') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span>
                </h2>

                <div class="card card-dashboard-seven" style="margin:10px;">
                    <div class="card-header">
                        <div class="row row-sm">
                            <div class="col-6 col-md-4 col-xl">
                                <div class="media">
                                    <div class="bg-primary"><i class="icon ion-ios-people"></i></div>
                                    <div class="media-body">
                                        <h6>Total Records</h6>
                                        <div class="date">
                                            <span id="heading_numberOfTotalRecord">{{$statistics['numberOfTotalRecord']}}</span>
                                        </div>
                                    </div>
                                </div><!-- media -->
                            </div>
                            <div class="col-6 col-md-4 col-xl">
                                <div class="media">
                                    <div class="bg-success"><i class="icon ion-md-checkmark"></i></div>
                                    <div class="media-body">
                                        <h6>Total Completed</h6>
                                        <div class="date">
                                            <span id="heading_callingCompleted">{{$statistics['callingCompleted']}}</span>
                                        </div>
                                    </div>
                                </div><!-- media -->
                            </div>

                        </div><!-- row -->
                    </div><!-- card-header -->
                    <div class="card-body">
                        <div class="row row-sm">
                            <div class="col-6 col-lg-2">
                                <label class="az-content-label">Total Move to Followup</label>
                                <h2><span id="heading_move_to_followup">{{$statistics['move_to_followup']}}</span></h2>
                            </div><!-- col -->
                            <div class="col-6 col-lg-2">
                                <label class="az-content-label">Total Ringing</label>
                                <h2><span id="heading_ringing">{{$statistics['ringing']}}</span></h2>
                            </div><!-- col -->
                            <div class="col-6 col-lg-2">
                                <label class="az-content-label">Total Reject</label>
                                <h2><span id="heading_reject">{{$statistics['reject']}}</span></h2>
                            </div>
                            <div class="col-6 col-lg-2">
                                <label class="az-content-label">Total Switch Off</label>
                                <h2><span id="heading_switch_off">{{$statistics['switch_off']}}</span></h2>
                            </div>
                            <div class="col-6 col-lg-2">
                                <label class="az-content-label">Total Invalid Number</label>
                                <h2><span id="heading_invalid_number">{{$statistics['invalid_number']}}</span></h2>
                            </div>
                            <div class="col-6 col-lg-2">
                                <label class="az-content-label">Total Recall</label>
                                <h2><span id="heading_recall">{{$statistics['recall']}}</span></h2>
                            </div>
                        </div>
                        <div class="row row-sm mt-3">

                            <div class="col-6 col-lg-2">
                                <label class="az-content-label">Total Not Interest</label>
                                <h2><span id="heading_not_interest">{{$statistics['not_interest']}}</span></h2>
                            </div>
                            <div class="col-6 col-lg-2">
                                <label class="az-content-label">Total Already Followup</label>
                                <h2><span id="heading_already_followup">{{$statistics['already_followup']}}</span></h2>
                            </div>
                        </div><!-- row -->
                    </div><!-- card-body -->
                </div>


                <div id="data-container">
                    <div class="row">
                        <div class="col-12">
                            {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                            <div class="mb-3">
                                <div class="row">
                                    <div class="col-3">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Search:</label>
                                        <input name="search" id="search" type="text" class="form-control" placeholder="Search">
                                    </div>
                                    <div class="col-3">
                                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Actions:</label>
                                        {!! Form::select('action', \App\Models\Campaign::CALLING_ACTIONS, [],['id' => 'action', 'class' => 'form-control select2', 'placeholder' => ""]) !!}
                                    </div>
                                    <div class="col-3 mt-4">
                                        <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                        <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">


                            <table id="dataTableList" class="table">
                                <thead>
                                    <tr role="row" class="bg-white">
                                        <th>User Details</th>
                                        <th>Interested Courses</th>
                                        <th>Move to Followup</th>
                                        <th>Ringing</th>
                                        <th>Reject</th>
                                        <th>Switch Off</th>
                                        <th>Invalid Number</th>
                                        <th>Recall</th>
                                        <th>Not Interest</th>
                                        <th>Already Followup</th>
                                        <th>Action Taken By</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- moveToFollow Modal -->
    <div id="moveToFollowModal" class="modal">
        <div class="modal-dialog modal-lg" role="document">
            <form id="moveToFollowForm">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Moving to Follow Up
                    </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Name: <span class="nameSection">-</span></label>
                                </div>
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Contact No: <span class="numberSection">-</span></label>
                                </div>
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Email: <span class="emailSection">-</span></label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row row-sm mt-2">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Follow Up Date: </label>
                                <input class="form-control" type="date" data-validation="required" name="move_visit_date" id="move_visit_date">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Follow Up Notes: </label>
                                <textarea class="form-control" id="move_notes" name="move_notes" data-validation="required"></textarea>
                            </div>
                        </div>

                    </div>
                </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Move To Follow Up</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div><!-- modal-dialog -->


    </div>


    <!-- Recalling Modal -->
    <div id="recallModal" class="modal">
        <div class="modal-dialog modal-lg" role="document">
            <form id="recallingForm">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Recalling
                    </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Name: <span class="nameSection">-</span></label>
                                </div>
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Contact No: <span class="numberSection">-</span></label>
                                </div>
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Email: <span class="emailSection">-</span></label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row row-sm mt-2">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Recall Date: </label>
                                <input class="form-control" id="recall_date" data-validation="required" name="recall_date" type="date">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Notes: </label>
                                <textarea class="form-control" id="recall_notes" data-validation="required" name="recall_notes"></textarea>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Recall</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
            </form>

            </div>
        </div><!-- modal-dialog -->


    </div>


    <!-- Not Interested Modal -->
    <div id="notInterestedModal" class="modal">
        <div class="modal-dialog modal-lg" role="document">
            <form id="notInterestedForm">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Setting Not Interest
                    </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Name: <span class="nameSection">-</span></label>
                                </div>
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Contact No: <span class="numberSection">-</span></label>
                                </div>
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Email: <span class="emailSection">-</span></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row row-sm mt-2">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Not Interest Reason: </label>
                                <select class="form-control" id="not_interested_reason" name="not_interested_reason" data-validation="required">
                                    <option value="">- Select Reason -</option>
                                    <option value="Already walking with another num">Already walking with another num</option>
                                    <option value="Doing Job">Doing Job</option>
                                    <option value="Further Studies">Further Studies</option>
                                    <option value="Got Job">Got Job</option>
                                    <option value="High Fees">High Fees</option>
                                    <option value="Joined Other Place">Joined Other Place</option>
                                    <option value="Location Problem">Location Problem</option>
                                    <option value="Not interested in any training">Not interested in any training</option>
                                    <option value="Other">Other</option>
                                    <option value="Study Abroad">Study Abroad</option>
                                    <option value="Technologies not supporting">Technologies not supporting</option>
                                    <option value="Timing Issues">Timing Issues</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Set Not Interest</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
            </form>
        </div><!-- modal-dialog -->


    </div>


    <!-- Alrady Follow Up Modal -->
    <div id="alreadyFollowUpModal" class="modal">
        <div class="modal-dialog modal-lg" role="document">
            <form id="alreadyFollowupForm">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">Already Follow Up

                    </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Name: <span class="nameSection">-</span></label>
                                </div>
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Contact No: <span class="numberSection">-</span></label>
                                </div>
                                <div class="col-sm-6">
                                    <label class="az-content-label tx-11 tx-medium">Email: <span class="emailSection">-</span></label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row row-sm mt-2">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Follow Up Notes: </label>
                                <textarea class="form-control" name="already_follow_notes" id="already_follow_notes" data-validation="required"></textarea>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
            </form>
        </div><!-- modal-dialog -->


    </div>
<input type="hidden" id="current_select_calling_detail" name="current_select_calling_detail" value=""/>
@endsection

@section('css')
    <style>
        .tooltip .tooltip-inner {
            text-align:left;
        }
        .infoIcon {
            font-size: 16px;
            margin-top: 5px;
            margin-left: 10px;
        }

    </style>

@endsection



@section('footer_scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js" integrity="sha512-eYSzo+20ajZMRsjxB6L7eyqo5kuXuS2+wEbbOkpaur+sA2shQameiJiWEzCIDwJqaB0a4a6tCuEvCOBHUg3Skg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#moveToFollowForm").submit(function(e) {
                e.preventDefault();
                statusUpdate('move_to_followup',$('#move_visit_date').val(),$("#move_notes").val());
                $('#moveToFollowModal').modal('hide');
            });

            $("#recallingForm").submit(function(e) {
                e.preventDefault();
                statusUpdate('recall',$('#recall_date').val(),$("#recall_notes").val());
                $('#recallModal').modal('hide');
            });

            $("#notInterestedForm").submit(function(e) {
                e.preventDefault();
                statusUpdate('not_interest','',$("#not_interested_reason").val());
                $('#notInterestedModal').modal('hide');
            });

            $("#alreadyFollowupForm").submit(function(e) {
                e.preventDefault();
                statusUpdate('already_followup','',$("#already_follow_notes").val());
                $('#alreadyFollowUpModal').modal('hide');
            });


            function statusUpdate(val,date,notes){
                var id = $("#current_select_calling_detail").val();
                $.ajax({
                    type:"POST",
                    data:{ campaign_id:'{{$campaignDetail->id}}', campaign_user_id: id,action:val,followup_date:date,notes:notes},
                    url: $app_url+'/campaign/save-calling-detail',
                    beforeSend: function () {
                        $.blockUI();
                    },
                    success:function(result)
                    {
                        if(result.status){
                            var statistics = result.data.statistics;
                            for (var key in statistics) {
                                $("#heading_"+key).html(statistics[key]);
                            }
                            $dataTableList.draw('page');
                            $.unblockUI();
                        }

                    },
                    error: function (data) {
                        console.log("Cancelled - error");
                        $.unblockUI();
                    }
                });
            }

            $(document).on('click','.actionPerform',function () {
                var selectedValue = $(this).val();
                var id = $(this).attr('name').split('calling_action_')[1];
                $("#current_select_calling_detail").val(id);
                if(selectedValue == "move_to_followup" || selectedValue == "recall" || selectedValue == "already_followup" || selectedValue == "not_interest"){
                    var firstTdData = $(this).parent().parent().parent().parent().children('td:first');
                    var name = firstTdData.find('span:nth-child(1)').text();
                    var email = firstTdData.find('span:nth-child(3)').text();
                    var number = firstTdData.find('span:nth-child(5)').text();
                    $(".nameSection").html(name);
                    $(".numberSection").html(number);
                    $(".emailSection").html(email);

                }

                if(selectedValue == "move_to_followup"){
                    $("#moveToFollowForm")[0].reset();
                    $('#moveToFollowModal').modal('show');
                }else if(selectedValue == "recall"){
                    $("#recallingForm")[0].reset();
                    $('#recallModal').modal('show');
                }else if(selectedValue == "not_interest"){
                    $("#notInterestedForm")[0].reset();
                    $('#notInterestedModal').modal('show');
                }else if(selectedValue == "already_followup"){
                    $("#alreadyFollowupForm")[0].reset();
                    $('#alreadyFollowUpModal').modal('show');
                }else{



                        bootbox.confirm({
                            message: "Are you sure you want to perform this action ?",
                            buttons: {
                                confirm: {
                                    label: "Yes",
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: "No",
                                    className: 'btn-danger'
                                }
                            },
                            callback: function (result) {
                                if (result === true) {
                                    statusUpdate(selectedValue,'','');
                                }
                            }
                        });


                }


                return false;

            });



            var $dataTableList = $('#dataTableList').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                bFilter: false,
//                "scrollX": true,
                "aaSorting": [],

                ajax: {
                    url: $app_url + '/campaign/getAjaxCallingData',
                    data: function (d) {
                        d.campaign_id = '{{$campaignDetail->id}}';
                        d.search=$("#search").val();
                        d.action=$("#action").val();
                    }
                },
                "columns": [
                    {data: 'user_detail', 'sortable': false,width:'25%'},
                    {data: 'course_name','sortable': false, 'orderable': false},
                    {data: 'move_to_followup','sortable': false, 'orderable': false,},
                    {data: 'ringing','sortable': false, 'orderable': false},
                    {data: 'reject','sortable': false, 'orderable': false},
                    {data: 'switch_off','sortable': false, 'orderable': false},
                    {data: 'invalid_number','sortable': false, 'orderable': false},
                    {data: 'recall','sortable': false, 'orderable': false},
                    {data: 'not_interest','sortable': false, 'orderable': false},
                    {data: 'already_followup','sortable': false, 'orderable': false},
                    {data: 'name','sortable': false, 'orderable': false},
                ],
                drawCallback: function (settings) {
                    $('body').tooltip({selector: '[data-toggle="tooltip"]',html: true});
                }
            });


            $('#search-form').on('submit', function (e) {
                $dataTableList.draw();
                e.preventDefault();
            });

            $('#reset-filters').on('click', function () {
                $("#action").val('').trigger('change');
                $("#search").val('');
                $dataTableList.draw();
            });
        });

    </script>
@endsection


