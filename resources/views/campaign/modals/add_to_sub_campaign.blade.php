<div class="modal fade" id="addToCampaignModal" role="dialog" aria-labelledby="addToCampaignModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addToCampaignModalLabel">Add Campaign</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route' => 'campaign.storesubcampaign', 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'add-campaign']) !!}
            <div class="modal-body">
                <div class="mb-3">
                    <input type="hidden" name="report_id" value="{{ $report['id'] }}">
                    <input type="hidden" name="course_user_ids" value="">
                    <input type="hidden" name="unselect_course_user_ids" value="">
                    <input type="hidden" name="checkAllUsers" id="checkAllUsers" value="false">
                    <input type="hidden" name="marketing_goal" id="marketing_goal" value="{{$report['marketing_goal']}}">
                    <input type="hidden" name="payment_status" id="payment_status_campaign" value="{{$_GET['payment_status']??''}}">
                    <input type="hidden" name="progress" id="progress_campaign" value="{{$_GET['progress']??''}}">
                    <div class="form-group">
                        <label>Campaign Name</label>
                        {!! Form::text('name', old('name'), ['class'=>'form-control', 'id' => 'name', 'placeholder' => 'Campaign Name', 'data-validation' => 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label>Campaign Action</label>
                        {!! Form::select('action', \App\Models\Campaign::ACTIONS , old('action'), ['class'=>'form-control select2Modal', 'id' => 'action', 'placeholder' => '', 'data-validation' => 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label>Assign To</label>
                        {!! Form::select('assign_to', $adminNames , old('assign_to'), ['class'=>'form-control select2Modal', 'id' => 'assign_to', 'placeholder' => '', 'data-validation' => 'required']) !!}
                    </div>
                    <div class="form-group">
                        <label>Accessible To</label>
                        {!! Form::select('accessible_to', $adminNames , old('accessible_to'), ['class'=>'form-control select2Modal', 'id' => 'accessible_to', 'multiple', 'data-validation' => 'required']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>