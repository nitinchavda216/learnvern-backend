@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Partners Revenue Settings</h2>
                <div id="data-container">
                    <form role="form" method="post" action="{{ route('tenancy-settings.store') }}"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{isset($PartnersSettings)?$PartnersSettings->id :null}}">
                        <div class="row">
                            <div class="col-md-5 border-right">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <h3>Third Party Partner Settings</h3>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="alert alert-info">
                                            Third Party Partners are partners who will be selling their own certificate.
                                            Like CDAC, NIIT, Aptech, Etc. and just use the technology from Learnvern
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Setup
                                                Fees</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="">₹</span>
                                                </div>
                                                <input type="text" class="form-control" name="third_party_partner_fees"
                                                       value="{{ isset($PartnersSettings)?$PartnersSettings->third_party_partner_fees :old('third_party_partner_fees') }}"
                                                       data-validation="required">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Agreement
                                                Document</label>
                                            <input type="file" accept="image/*" class="file-upload dropify"
                                                   data-show-remove="true"
                                                   @if(isset($PartnersSettings) && isset($PartnersSettings->third_party_agreement_document))
                                                   data-default-file="{{ showdocument($PartnersSettings->third_party_agreement_document) }}"
                                                   @endif
                                                   id="third_party_agreement_document"
                                                   name="third_party_agreement_document"/>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <h3>Content Partner Settings</h3>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="alert alert-info">
                                            Content Partners are partners who will be helping Learnvern for promoting
                                            NSDC certificates and gain userbase for learnvern. Ex. Tops Technologies
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Setup Fees For
                                                Content
                                                Partner</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="">₹</span>
                                                </div>
                                                <input type="text" class="form-control" name="content_partner_fees"
                                                       value="{{ isset($PartnersSettings)?$PartnersSettings->content_partner_fees :old('content_partner_fees') }}"
                                                       data-validation="required">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Agreement
                                                Document</label>
                                            <input type="file" class="file-upload dropify"
                                                   data-show-remove="true"
                                                   @if(isset($PartnersSettings) && isset($PartnersSettings->content_partner_agreement_document))
                                                   data-default-file="{{ showdocument($PartnersSettings->content_partner_agreement_document) }}"
                                                   @endif
                                                   id="content_partner_agreement_document"
                                                   name="content_partner_agreement_document"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-az-primary btn-sm">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("[data-toggle='tooltip']").tooltip();
            $('#courseId').select2({
                placeholder: 'Select Course'
            });


        });
    </script>
@endsection
