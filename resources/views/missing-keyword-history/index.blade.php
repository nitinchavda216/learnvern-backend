@extends('layout.master')
@section('content')
<div class="az-content">
    <div class="container">
        <div class="az-content-body">
            <h2 class="az-content-title"> Missing Keyword History
                <span class="pull-right d-inline-block float-right">
                    <a href="{{ route('missing-search-keywords') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                    </a>
                </span>
            </h2>
            <div id="data-container">
                <div class="row">
                    <div class="col-lg-12 mb-4">
                        <div class="alert alert-warning" role="alert">
                            <h5> Showing results for  "{{ $response->keyword ?? "- - -" }}" keyword:</h5>
                        </div>
                    </div>
                </div>
                <table id="dataTableList" class="display responsive">
                    <thead>
                        <tr>
                            <th>User Name</th>
                            <th>IP</th>
                            <th>ISD Code</th>
                            <th>Mobile Number</th>
                            <th>Email</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Country</th>
                            <th>Pincode</th>
                            <th>Attempts</th>
                            <th>Last Attempt</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
<script type="text/javascript">
    $(document).ready(function () {
        var url = window.location.pathname;
        var id = url.substring(url.lastIndexOf('/') + 1);
        if ($('#dataTableList').length) {
            var $dataTableList = $('#dataTableList').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                bFilter: false,
                "aaSorting": [],
                ajax: {
                    url: $app_url + '/missing-keyword-history/getListData',
                    data: function (d) {
                        d.keyword_id = id;
                    }
                },
                "columns": [
                    {data: 'full_name', 'sortable': true},
                    {data: 'ip', 'sortable': true},
                    {data: 'isd_code', 'sortable': true},
                    {data: 'mobile_number', 'orderable': true},
                    {data: 'email', 'orderable': true},
                    {data: 'city', 'orderable': true},
                    {data: 'state', 'orderable': true},
                    {data: 'country', 'orderable': true},
                    {data: 'pin_code', 'orderable': true},
                    {data: 'attempts', 'orderable': true},
                    {data: 'last_attempt', 'orderable': true}
                ],
                select: true,
                bStateSave: true,
                fnStateSave: function (settings, data) {
                    localStorage.setItem("dataTables_state", JSON.stringify(data));
                },
                fnStateLoad: function (settings) {
                    return JSON.parse(localStorage.getItem("dataTables_state"));
                }
            });
        }
    });
</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
@endsection

