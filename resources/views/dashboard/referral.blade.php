<div class="card-body">
    <div class="table-responsive" >
        <table class="table" id="table">
            <thead>
            <tr>
                <th class="wd-45p">Referral Code</th>
                <th>Count</th>
                <th>Action</th>
            </tr>

            </thead>
            <tbody id="tbody">
            @foreach($referralData as $data)
                <tr>
                    <td>{{$data['own_referral_code']}}</td>
                    <td>{{$data['referred_count']}}</td>
                    <td>----</td>
                </tr>
            @endforeach
            </tbody>

        </table>
    </div>

</div>
