@extends('layout.master')
@section('content')
    <div class="az-content az-content-dashboard-eight">
        <div class="container d-block">
            <div class="row">
                <div class="col-md-12 px-0">
                    <h2 class="az-content-title text-white">Howdy, {{ auth()->guard('admins')->user()->name }}!
                        <span class="float-right d-inline-block">Welcome to the Learnvern Admin Dashboard</span>
                    </h2>
                </div>
            </div>
            <div class="row my-3">
                <div class="col-md-4 col-sm-12">
                @asyncWidget('RegisteredUsers')
                </div>
                <div class="col-md-4 col-sm-12">
                @asyncWidget('EnrolledUsers')
                </div>
                <div class="col-md-4 col-sm-12">
                    @asyncWidget('TotalIncome')
                </div>
            </div>
        </div>
    </div><!-- container -->
@endsection
