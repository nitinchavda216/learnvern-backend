@extends('layout.master')
@section('content')
    <div class="az-content az-content-dashboard-eight">
        <div class="container d-block">
            <div class="row">
                <div class="col-md-12 px-0">
                    <h2 class="az-content-title text-white">Howdy, {{ auth()->guard('admins')->user()->name }}!
                        <span class="float-right d-inline-block">Welcome to the Learnvern Admin Dashboard</span>
                    </h2>
                </div>
            </div>
            <div class="card card-body card-dashboard-twentyfive mg-b-20">
                <h6 class="card-title">LMS Statistics</h6>
                @asyncWidget('CatalogStatistics')
            </div>
            <div class="card">
                <div class="card-header">
                    <h4>Latest Courses</h4>
                </div>
                <div class="card-body card-dashboard-twentyfive mg-b-20">
                    <div class="table-responsive">
                        <table class="table mg-b-0">
                            <thead>
                            <tr>
                                <th>Courses Name</th>
                                <th>Category Name</th>
                                <th>Statistics</th>
                                <th>Total Time</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($courses as $item)
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->categoryDetail->name ?? "- - -" }}</td>
                                    <td>
                                    <span data-toggle="tooltip" data-placement="left" title=""
                                          data-original-title="Units in the Course"><i
                                                class="fa fa-play-circle mr-1"></i>{{ count($item->relatedUnitCurriculum) }}</span>
                                        <hr class="my-1">
                                        <span><i class="fa fa-file mr-1"></i>{{ count($item->relatedAssignmentCurriculum) }}</span>
                                        <hr class="my-1">
                                        <span><i class="fa fa-question-circle mr-1"></i>{{ count($item->relatedQuizCurriculum) }}</span>
                                    </td>
                                    <td>{{ $item->time }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
