@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Create Email Templates
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{ route('email-templates') }}" class="btn btn-warning btn-sm mt-negative"
                           title="Back">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </span></h2>
                <div id="data-container">
                    {!! Form::open(['route'=>['email-templates.update', $email_template->id],'method' => 'POST', 'enctype'=> "multipart/form-data", 'files' => true, 'id' => "courseForm"]) !!}
                    @include("email-templates.form", ['formMode' => 'create'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

