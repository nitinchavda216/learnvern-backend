@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">Email Templates
                    @if(session('partner_id') == 1)
                        <span class="pull-right d-inline-block float-right">
                            <a href="{{route('email-templates.create')}}" class="btn btn-success btn-sm mt-negative"
                               title="Add New"><i class="fa fa-plus" aria-hidden="true"></i> Add New
                            </a>
                        </span>
                    @endif
                </h2>
                <div id="data-container">
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Identifier</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($email_templates as $email_template)
                            <tr>
                                <td>{{ $email_template->title ?? "- - -" }}</td>
                                <td>{{ (isset(\App\Models\EmailTemplates::EMAIL_ACTION[$email_template->identifier])) ? \App\Models\EmailTemplates::EMAIL_ACTION[$email_template->identifier] : '' }}</td>
                                <td>
                                    <div class="btn-group" role="group">
                                        <a href="{{ route('email-templates.edit', $email_template->id) }}"
                                           class="btn btn-primary btn-xs">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#dataTableList').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        });
    </script>
@endsection

