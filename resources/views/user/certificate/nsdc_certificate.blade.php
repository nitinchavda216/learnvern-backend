<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Learnvern Certificate</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    {{--<link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond:400,400i,500,500i,600,600i,700|Muli:400,600" rel="stylesheet">--}}
    <style type="text/css" media="all">
        body {
            /*font-family: 'Cormorant Garamond', serif;*/
            font-family: 'Muli', sans-serif;
        }

        .nameTitle {
            color: black;
            font-size: 32px;
            font-weight: 900;
            /*font-family: 'Cormorant Garamond', serif;*/
            text-align: center;
            position: absolute;
            top: 315px;
            left: 50%;
            transform: translateX(-50%);
        }

        .certificateNo {
            font-family: 'tahoma', serif;
            text-align: center;
            position: absolute;
            padding-top: 20px;
            top: 52.5%;
            left: 50%;
            transform: translate(-50%, -50%);
            font-size: 18px;
        }

        .courseName {
            text-align: center;
            position: absolute;
            top: 62%;
            left: 50%;
            transform: translate(-50%, -50%);
            font-size: 30px;
            text-align: center;
        }

        .periodDate {
            position: absolute;
            top: 68.5%;
            left: 57.8%;
            transform: translate(-50%, -50%);
            font-weight: 900;
            font-family: 'tahoma', serif;
            font-size: 20px;
        }

        .issueDate {
            position: absolute;
            bottom: 18%;
            left: 17.5%;
            /*transform: translate(-50%, -50%);*/
            font-size: 15px;
            font-family: 'tahoma', serif;
            font-weight: 900;
        }

        .certificateId {
            position: absolute;
            bottom: 17%;
            left: 50%;
            transform: translate(-50%, -50%);
            font-size: 15px;
            font-family: 'tahoma', serif;
            font-weight: 900;
            text-align: center;
        }

        .qrCode {
            position: absolute;
            bottom: 0;
            left: 50%;
            transform: translate(-50%, -50%);
            text-align: center;
        }

        .qrCodeImg {
            width: 50px;
            height: 50px;
        }
    </style>
</head>

<body media="print">
@php
    $image_name = ($certificate_details->is_international_certificate == 1) ? 'admin/img/Certificate_International.png' : 'admin/img/Certificate.png';
@endphp
<div align="center"
     style="position: relative; background: url({{ $image_name }}); background-size: contain; background-position: center center; background-repeat: no-repeat; width:100%; height:100%;margin: 0 auto;">

    <div align="center" class="nameTitle"
         style="font-family: 'Cormorant Garamond', serif;">{{ucfirst($certificate_details->userDetail->certificate_name)}}</div>

    <div class="certificateNo">having
        <b>{{ $candidate_id }}</b>
        has successfully completed training in
    </div>
    <br/>
    <div class="courseName">{{ $course_name }}</div>

    <div class="periodDate"><b>
            @php
                $date = $certificate_details->certificate_date;
                $start_date = $certificate_details->start_date;
                $end_date = $certificate_details->end_date;
            @endphp
            @if(isset($start_date) && isset($end_date))
                {{ date('M', strtotime($start_date)) . ' ' . date('Y', strtotime($start_date)) . " - " . date('M', strtotime($end_date)) . ' ' . date('Y', strtotime($end_date)) }}
            @else
                @php $day = date('d', strtotime($date)); $month = date('m', strtotime($date)); @endphp
                @if($day == 31 && $month != 03)
                    {{date('M Y', strtotime("-1 month -1 Day ".$date))." - ".date('M Y', strtotime($date))}}
                @elseif(($day == 29 || $day == 30 || $day == 31) && $month == 03)
                    {{'Feb '. date('Y', strtotime("-1 month ".$date))." - ".date('M Y', strtotime($date))}}
                @else
                    {{date('M Y', strtotime("-1 month ".$date))." - ".date('M Y', strtotime($date))}}
                @endif
            @endif
        </b>
    </div>
    <div class="issueDate">{{date('d-m-Y', strtotime($date))}}</div>
    <div class="certificateId">Certificate Id: {{$certificate_details->certificate_id}}</div>

    <div class="qrCode">
        <img src="data:image/png;base64, {!! base64_encode($qr_code) !!}" class="qrCodeImg">
        <br/>
        <span>{{ env('FRONT_URL') }}</span>
    </div>
</div>
</body>
</html>
