<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Learnvern Cwrtificate</title>

    <style type="text/css">
        body {
            padding: 0;
            margin: 0;

        }
        * {
            font-family: Verdana, Arial, sans-serif;
        }
    </style>
</head>
    <body style="background-image: url({{ url('images/learnvern_certificate.jpg') }}); height: 88rem; background-size: 100%;
        background-repeat: no-repeat; width: 90%; background-position: center; margin: 0 5%; position: relative">
<div style="font-size: 2.5rem;color: #232162;font-family: 'Times New Roman', Times, serif;width: 100%; text-align: center;position: absolute;left: 0;right: 0; top:15.8rem;
                 font-weight: normal;">{{ $certificate_details->userDetail->certificate_name }}
</div>
<div style="font-size: 2.1rem;color: #232162;font-family: 'Times New Roman', Times, serif;width: 100%; text-align: center;position: absolute;left: 0;right: 0; top:23rem;
                 font-weight: normal;">{{ $certificate_details->courseDetail->course_certificate_name }}</div>
<div style=" position: absolute;left: 14.8rem; bottom: 9.5rem; color: #232162;font-size: 1.2rem;">{{ formatDate($certificate_details->courseDetail->start_date, 'd-m-Y') }}</div>

<div style="position: absolute; left: 1.2rem; bottom:4.8rem; font-size: 1.1rem;text-align: center; text-transform: uppercase; color: #232162; right: 0;">
    {{ $certificate_details->certificate_id }}
</div>
</body>
</html>
