@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container-fluid">
            <div class="az-content-body">
                <h2 class="az-content-title"> User List</h2>
                <div id="data-container">
                    {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                    <div class="wd-xl-100p mb-3">
                        <div class="row row-xs">
                            <div class="col-md-3">
                                <label class="az-content-label tx-11 tx-medium tx-gray-600">Search:</label>
                                <input name="search" id="search" type="text" class="form-control" placeholder="Search">
                            </div>
                            @if(session('partner_id') == 1)
                                <div class="col-md-3">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Partners:</label>
                                    {!! Form::select('partner_user_id', $partners, null,['id' => 'partner_user_id', 'class' => 'form-control select2']) !!}
                                </div>
                            @endif
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Date Range:</label>
                                    <input name="date_range" id="date_range" type="text" class="form-control"
                                           autocomplete="off" placeholder="Select Date">
                                </div>
                            </div>
                            <div class="col-md-2 mt-4">
                                <button type="submit" class="btn btn-az-success btn-success">Search</button>
                                <button type="button" class="btn btn-az-warning btn-warning" id="reset-filters">Reset
                                </button>
                            </div>
                        </div>

                    </div>
                    {!! Form::close() !!}
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>Full Name</th>
                            <th>User Name</th>
                            <th>User Email</th>
                            <th>Mobile No</th>
                            <th>Referral Code</th>
                            <th>Email Verify</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="{{ asset('admin/modules/users/index.js') }}"></script>
@endsection

