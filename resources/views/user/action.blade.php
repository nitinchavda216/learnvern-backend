<a href="{{ route('users.details', $user->id) }}" class="btn btn-primary btn-xs d-inline">
    <i class="fas fa-edit"></i>
</a>
<div class="d-inline az-toggle az-toggle-secondary float-right {{ ($user->is_blocked == 0) ? 'on' : '' }}"
     data-id="{{ $user->id }}">
    <span></span>
</div>
