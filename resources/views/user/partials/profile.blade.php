<div id="accordion" class="accordion" role="tablist" aria-multiselectable="true">
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                Personal Details
            </a>
        </div><!-- card-header -->

        <div id="collapseOne" data-parent="#accordion" class="collapse show" role="tabpanel"
             aria-labelledby="headingOne">
            <div class="card-body">
                <div class="row row-sm">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Salutation
                                :</label>
                            {!! Form::select('salutation', config('info.salutations'),$user->salutation ?? old('salutation'), ['class'=>'form-control select2', 'data-validation' => 'required']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Name:</label>
                            {!! Form::text('first_name', $user->first_name ?? old('first_name'), ['class'=>'form-control', 'data-validation' => 'required']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Certificate Name:</label>
                            {!! Form::text('certificate_name', $user->certificate_name ?? old('certificate_name'), ['class'=>'form-control', 'data-validation' => 'required']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Username:</label>
                            {!! Form::text('username', $user->username ?? null, ['class' => 'form-control ',
                                                                'placeholder' => "Username", 'data-validation' => 'required','id'=>'username' ]) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Email:</label>
                            {!! Form::text('email', $user->email, ['class'=>'form-control', 'disabled']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Mobile Number:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"
                                          id="basic-addon1">{{ getIsdCode($user->isd_code) }}</span>
                                </div>
                                {!! Form::text('mobile_number', $user->mobile_number ?? old('mobile_number'), ['class'=>'form-control', 'data-validation' => 'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Gender:</label>
                            {!! Form::select('gender', config('info.genders'),$user->gender ?? old('gender'), ['class'=>'form-control', 'data-validation' => 'required']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Birth Date:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="typcn typcn-calendar-outline tx-24 lh--9 op-6"></i>
                                    </div>
                                </div>
                                {!! Form::date('birth_date', $user->birth_date ?? null, ['class' => 'form-control editproflie-frm','format'=>'m-d-Y',
                                                    'placeholder' => "mm/dd/yyyy", 'data-validation' => 'required','max'=>date("Y-m-d", strtotime("-11 years"))]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Marital Status:</label>
                            {!! Form::select('marital_status', config('info.marital_status'), $user->marital_status ?? null, ['class' => 'form-control select2',
                                                                    'placeholder' => "Select", 'data-validation' => 'required','id'=>'marital_status']) !!}

                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Father Name:</label>
                            {!! Form::text('father_name', $user->father_name ?? old('father_name'), ['class'=>'form-control', 'data-validation' => 'required']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Mother Name:</label>
                            {!! Form::text('mother_name', $user->mother_name ?? old('mother_name'), ['class'=>'form-control', 'data-validation' => 'required']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Cast Category
                                :</label>
                            {!! Form::select('cast_category', config('info.cast_categories'),$user->cast_category ?? old('cast_category'), ['class'=>'form-control select2', 'data-validation' => 'required']) !!}
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Religion:</label>
                            {!! Form::select('religion', config('info.religions'), $user->religion ?? null, ['class' => 'form-control select2',
                            'placeholder' => "Select", 'data-validation' => 'required','id'=>'religion']) !!}
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3 mb-3">
                        <label for="">Education level</label>
                        <div class="profile-select">
                            {!! Form::select('education', config('info.education_list'), $user->education ?? null, ['class' => 'form-select editproflie-frm select2',
                            'placeholder' => "Select", 'data-validation' => 'required']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">ID Proof:</label>
                            {!! Form::select('id_proof', config('info.nsdc_id_proof'), $user->id_proof ?? null, ['class' => 'form-control select2',
                            'placeholder' => "Select", 'data-validation' => 'required','id'=>'id_proof']) !!}
                        </div>
                    </div><!-- Col Ends-->
                    <!-- Col -->
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">ID Proof Number:</label>
                            {!! Form::text('id_number', $user->id_number ?? null, ['class' => 'form-control',
                            'placeholder' => "ID Proof Number", 'data-validation' => 'required','id'=>'id_number','maxlength'=>50]) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label class="az-content-label tx-11 tx-medium tx-gray-600">Disability:</label>
                        <div class="radioBoxContainer">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="disability" id="disability_option_1"
                                       value="1" {{ isset($user->disability)&& $user->disability == 1 ?'checked':'' }}>
                                <label class="form-check-label" for="disability_option_1">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="disability" id="disability_option_2"
                                       value="0" {{ isset($user->disability)&& $user->disability == 0 ?'checked':'' }}>
                                <label class="form-check-label" for="disability_option_2">No</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3" id="disabilityTypeSection"
                         style="display: {{ $user->disability == 1 ?'block':'none' }};">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Types Of Disability:</label>
                            {!! Form::select('disability_type', config('info.disability_categories'), $user->disability_type ?? null, ['class' => 'form-control select2',
                            'placeholder' => "Select", 'data-validation' => 'required','id'=>'disability_type']) !!}
                        </div>
                    </div><!-- Col Ends-->
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" role="tab" id="headingTwo">
            <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false"
               aria-controls="collapseTwo">
                Address Details
            </a>
        </div>
        <div id="collapseTwo" class="collapse" data-parent="#accordion" role="tabpanel" aria-labelledby="headingTwo">
            <div class="card-body">
                <div class="row row-sm">
                    <div class="col-sm-9">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Address
                                :</label>
                            {!! Form::text('address', $user->address ?? old('address'), ['class'=>'form-control', 'data-validation' => 'required']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Country
                                :</label>
                            {!! Form::select('country_id', $countries,$user->country_id ?? old('country_id'), ['class'=>'form-control select2', 'id' => 'country_id', 'data-validation' => 'required']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">state
                                :</label>
                            {!! Form::select('state_id', $states,$user->state_id ?? old('state_id'), ['class'=>'form-control select2', 'id' => 'state_id', 'data-validation' => 'required']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">City
                                :</label>
                            {!! Form::select('city_id', $cities,$user->city_id ?? old('city_id'), ['class'=>'form-control select2' , 'id' => 'city_id', 'data-validation' => 'required']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">District
                                :</label>
                            {!! Form::select('district', $districts, $user->district ?? old('district'), ['class'=>'form-control select2', 'id' => 'district_id']) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Pincode
                                :</label>
                            {!! Form::text('pin_code', $user->pin_code ?? old('pin_code'), ['class'=>'form-control', 'data-validation' => 'required']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" role="tab" id="headingThree">
            <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false"
               aria-controls="collapseThree">
                Career
            </a>
        </div>
        <div id="collapseThree" class="collapse" data-parent="#accordion" role="tabpanel"
             aria-labelledby="headingThree">
            <div class="card-body">
                <div class="row row-sm">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">
                                Training Status</label>
                            <div class="radioBoxContainer">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="current_occupation"
                                           id="current_occupation_fresher"
                                           value="Fresher" {{ isset($user->current_occupation)&& $user->current_occupation == 'Fresher' ?'checked':'' }}>
                                    <label class="form-check-label" for="current_occupation_fresher">Fresher</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="current_occupation"
                                           id="current_occupation_experience"
                                           value="Experience" {{ isset($user->current_occupation)&& $user->current_occupation == 'Experience' ?'checked':'' }}>
                                    <label class="form-check-label"
                                           for="current_occupation_experience">Experienced</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3 dependUponExperience"
                        style="display:{{$user->current_occupation == 'Experience' ?'block':'none' }};">

                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Employed</label>
                            <div class="radioBoxContainer">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="is_employed" id="is_employed_1"
                                           value="1" {{ isset($user->is_employed)&& $user->is_employed == 1 ?'checked':'' }}>
                                    <label class="form-check-label" for="is_employed_1">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="is_employed" id="is_employed_0"
                                           value="0" {{ isset($user->is_employed)&& $user->is_employed == 0 ?'checked':'' }}>
                                    <label class="form-check-label" for="is_employed_0">No</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 dependUponExperience"
                         style="display:{{$user->current_occupation == 'Experience' ?'block':'none' }};">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">
                                Previous Experience Sector</label>
                            {!! Form::text('previous_experience_sector', $user->previous_experience_sector ?? null, ['class' => 'form-control ',
                            'placeholder' => "Previous Experience Sector", 'data-validation' => 'required','id'=>'previous_experience_sector']) !!}
                        </div><!-- Col Ends-->
                    </div>
                    <div class="col-md-3 dependUponExperience"
                         style="display:{{$user->current_occupation == 'Experience' ?'block':'none' }};">
                    <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">
                                No of months of previous experience</label>
                            {!! Form::number('no_of_months_of_experience', $user->no_of_months_of_experience ?? null, ['class' => 'form-control ',
                            'placeholder' => "No of months of previous experience", 'data-validation' => 'required','id'=>'no_of_months_of_experience']) !!}
                        </div>
                    </div><!-- Col Ends-->

                    <div class="col-md-3 dependUponExperience" id="dependUponExperience"
                         style="display:{{$user->current_occupation == 'Experience' ?'block':'none' }};">
                        <label for="">Employment Status</label>
                        {!! Form::select('employment_status', config('info.employment_status'), $user->employment_status ?? null, ['class' => 'form-control select2',
                        'placeholder' => "Select", 'data-validation' => 'required','id'=>'employment_status']) !!}
                    </div><!-- Col Ends-->

                    <div class="col-md-3 dependUponExperience" id="dependUponExperience1"
                    style="display: {{ $user->current_occupation == 'Experience' ?'block':'none' }};">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">
                                Employment Details</label>
                            <textarea class="form-control " name="employment_details"
                                      id="employment_details" rows="2"
                                      data-validation="required"
                                      maxlength="100"
                                      placeholder="Employment Details">{{ $user->employment_details ?? null }}</textarea>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Heard About Us</label>
                            {!! Form::select('heard_about_us', config('info.heard_about_us'), $user->heard_about_us ?? null, ['class' => 'form-control select2',
                            'placeholder' => "Select", 'data-validation' => 'required','id'=>'heard_about_us']) !!}
                        </div>
                    </div><!-- Col Ends-->

                </div>
            </div>
        </div><!-- collapse -->
    </div><!-- card -->
</div>
<button type="submit"
        class="btn btn-az-primary mg-md-t-9">{{ isset($user) ? "Update" : "Submit" }}</button>


