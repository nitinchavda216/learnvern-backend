@php
    $resume_details = $user->resumeDetails;
    $image = $resume_details['image'] ?? 'http://i.pravatar.cc/500?img=7';
@endphp
<div class="row g-0 mx-auto align-items-center">
    <div class="col-9">
        <div class="name-title">
            {{ $user->first_name }}
        </div>
        <div class="job-title mb-3">
            {{ $resume_details->title }}
        </div>
        <div>
            <div class="contact-links">
                <i class="fa fa-phone"></i>
                <span>{{ $user->mobile_number }}</span>
            </div>
        </div>
        <div>
            <div class="contact-links ">
                <i class="fa fa-envelope"></i>
                <span>{{ $user->email }}</span>
            </div>
        </div>
        <div>
            <div class="contact-links">
                <i class="fa fa-map-marker"></i>
                <span>{{ $resume_details->address }}</span>
            </div>
        </div>
    </div>
    <div class="col-3">
        <div class="contact-links-view">
            <img src="{{ $image }}" alt="" width="100px;">
        </div>
        <!--/Contact Links-->
    </div>
</div>
<hr class="mg-y-30 mg-lg-y-20">
@if($resume_details['summary']['is_enable'] == "true")
    <div class="az-content-label mg-b-5">SUMMARY</div>
    <div class="btn-wraper">
        <div class="summary-para">
            {{ $resume_details['summary']['details'] }}
        </div>
    </div>
@endif
<!--/Summary-->
<hr class="mg-y-30 mg-lg-y-20">
@if($resume_details['experience_details']['is_enable'] == "true")
    <div class="az-content-label mg-b-5">EXPERIENCE</div>
    <div class="btn-wraper mt-4">
        @foreach($resume_details['experience_details']['values'] as $key => $experience_value_details)
            <div class="experience-view btn-wraper">
                <div class="row g-0">
                    <div class="col-2">
                        <div class="year-txt">
                            {{ $experience_value_details['from_year'] }}-{{ $experience_value_details['to_year'] }}
                        </div>
                    </div>
                    <div class="col-10">
                        <div class="company-post-title">
                            {{ $experience_value_details['job_title'] }}
                        </div>
                        <div class="company-title">
                            {{ $experience_value_details['company'] }}
                        </div>
                        <div class="company-descrip-title">
                            Description
                        </div>
                        <div class="company-description">
                            {{ $experience_value_details['description'] }}
                        </div>
                    </div>
                </div>
            </div><!--/Company Detail View-->
        @endforeach
    </div>
@endif
<hr class="mg-y-30 mg-lg-y-20">
<!--/Professional Experience-->
@if($resume_details['education_details']['is_enable'] == "true")
    <div class="az-content-label mg-b-5">EDUCATION</div>
    <div class="btn-wraper mt-4">
        @foreach($resume_details['education_details']['values'] as $key => $education_value_details)
            <div class="experience-view btn-wraper">
                <div class="row g-0">
                    <div class="col-2">
                        <div class="year-txt">
                            {{ $education_value_details['from_year'] }}-{{ $education_value_details['to_year'] }}
                        </div>
                    </div>
                    <div class="col-10">
                        <div class="row g-0 mx-auto education-row">
                            <div class="col-10">
                                <div class="company-post-title">
                                    {{ $education_value_details['education_title'] }}
                                </div>
                                <div class="company-title">
                                    {{ $education_value_details['college_name'] }}
                                </div>
                            </div>
                            <div class="col-2 percentage-col">
                                        <span class="percentage-txt">
                                            Result -
                                        </span>
                                <span class="percentage-txt2">
                                            {{ $education_value_details['result'] }}
                                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/Education View-->
        @endforeach
    </div>
@endif
<!--/Education-->
<hr class="mg-y-30 mg-lg-y-20">
<div class="row">
    @if($resume_details['hobby_details']['is_enable'] == "true")
        <div class="col-6">
            <div class="az-content-label mg-b-5">HOBBIES</div>
            <ul>
                @foreach($resume_details['hobby_details']['values'] as $hobby_title)
                    <li>{{ $hobby_title }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if($resume_details['language_details']['is_enable'] == "true")
        <div class="col-6">
            <div class="az-content-label mg-b-5">LANGUAGES</div>
            <ul>
                @foreach($resume_details['language_details']['values'] as $language_title)
                    <li>{{ $language_title }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</div>
<!--/Hobbies & Language Row-->
<hr class="mg-y-30 mg-lg-y-20">
<div class="row">
    @if($resume_details['key_skill_details']['is_enable'] == "true")
        <div class="col-6">
            <div class="az-content-label mg-b-5">KEY SKILLS</div>
            <ul>
                @foreach($resume_details['key_skill_details']['values'] as $key_title)
                    <li>{{ $key_title }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if($resume_details['technical_skill_details']['is_enable'] == "true")
        <div class="col-6">
            <div class="az-content-label mg-b-5">TECHNICAL SKILLS</div>
            <ul>
                @foreach($resume_details['technical_skill_details']['values'] as $technical_skill_title)
                    <li>{{ $technical_skill_title }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</div>