@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title">User Details
                    <span class="pull-right d-inline-block float-right">
                            <a href="{{ route('users') }}" class="btn btn-warning btn-sm mt-negative" title="Back">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                            </a>
                        </span>
                </h2>
                <div id="data-container">
                    <div class="card bd-0">
                        <div class="card-header bg-gray-400 bd-b-0-f pd-b-0">
                            <nav class="nav nav-tabs">
                                <a class="nav-link @if(($active_tab!= "" && $active_tab == "details") || $active_tab== "") active show @endif"
                                   data-toggle="tab" href="#profile">Profile</a>
                                <a class="nav-link @if($active_tab!= "" && $active_tab== "courses")active show @endif"
                                   data-toggle="tab" href="#courses">Courses</a>
                                <a class="nav-link @if($active_tab!= "" && $active_tab== "result") active show @endif"
                                   data-toggle="tab" href="#result">Result</a>
                                {{--  <a class="nav-link" data-toggle="tab" href="#bookings">Bookings</a>--}}
                                <a class="nav-link @if($active_tab!= "" && $active_tab== "referrals") active show @endif"
                                   data-toggle="tab" href="#referral">Referral</a>
                                <a class="nav-link @if($active_tab!= "" && $active_tab== "resume_details") active show @endif"
                                   data-toggle="tab" href="#resume_details">Resume Details</a>
                                <a class="nav-link @if($active_tab!= "" && $active_tab== "certificates") active show @endif"
                                   data-toggle="tab" href="#certificates">Certificates</a>

                                <a class="nav-link" href="{{ route('crm.getCampaignUserDetail', $user->id) }}">Campaign History</a>

                            </nav>
                        </div>
                        <div class="card-body bd bd-t-0 tab-content">
                            <div id="profile"
                                 class="tab-pane  @if(($active_tab!= "" && $active_tab == "details") || $active_tab== "") active show @endif">
                                @include('layout.partials.flash_messages')
                                <form role="form" method="post" action=" {{ route('users.update', $user->id)  }}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    @include('user.partials.profile')
                                </form>
                            </div>
                            <div id="courses"
                                 class="tab-pane  @if($active_tab!= "" && $active_tab== "courses") active show @endif">
                                <table id="dataTableList" class="display responsive" width="100%">
                                    <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th>Course Name</th>
                                        <th>Course Status</th>
                                        <th>Course Progress</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($user->courses as $course)
                                        <tr>
                                            <td>{{$course->courseDetail->id ?? "- - -"}}</td>
                                            <td>{{ $course->courseDetail->name ?? "- - -" }}</td>
                                            <td>{{ isset($course->userStatus) ? $course-> userStatus->name : "- - -" }}</td>
                                            <td>{{ $course->progress }} %</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div id="result"
                                 class="tab-pane  @if($active_tab!= "" && $active_tab== "result")active show @endif">
                                <div id="accordionResult" class="accordion" role="tablist" aria-multiselectable="true">
                                    @forelse($results as $key => $resultArray)
                                        <div class="card">
                                            <div class="card-header" role="tab" id="heading_{{ $key }}">
                                                <a data-toggle="collapse" href="#collapse_{{ $key }}"
                                                   aria-expanded="false" aria-controls="collapse_{{ $key }}"
                                                   class="@if($key != 0) collapsed @endif">
                                                    {{ $resultArray['name'] }}
                                                    <span class="pull-right d-inline-block float-right">
                                                        <p>Master Result:
                                                            @if(isset($resultArray['master_quiz']))
                                                                <span>{{ $resultArray['master_quiz']['marks'] }}
                                                                    /{{ $resultArray['master_quiz']['total_mark'] }}</span>
                                                            @else
                                                                <span class="text-info">Not Completed</span>
                                                            @endif
                                                        </p>
                                                    </span>
                                                </a>
                                            </div>
                                            <div id="collapse_{{ $key }}" data-parent="#accordionResult"
                                                 class="collapse @if($key == 0) show @endif" role="tabpanel"
                                                 aria-labelledby="heading_{{ $key }}" style="">
                                                <div class="card-body">
                                                    <table class="table az-table-reference m-0">
                                                        <thead>
                                                        <tr>
                                                            <th class="wd-40p">Quiz</th>
                                                            <th class="wd-60p">Result</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($resultArray['quiz'] as $quiz)
                                                            <tr>
                                                                <td>{{ $quiz['quiz_name'] }}</td>
                                                                <td>{{ $quiz['marks'] }}
                                                                    /{{ $quiz['total_mark'] }}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    @empty
                                        <div class="az-content-label mg-b-5">No Result Found..!!</div>
                                    @endforelse
                                </div>
                            </div>
                            <div id="referral"
                                 class="tab-pane @if($active_tab != "" && $active_tab== "referrals") active show @endif">
                                <table id="dataTableList1" class="display responsive" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Full Name</th>
                                        <th>User Name</th>
                                        <th>User Email</th>
                                        <th>Mobile Number</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($referralCount as $data)
                                        <tr>
                                            <td>{{$data->first_name  ?? "- - -"}}</td>
                                            <td>{{ $data->username ?? "- - -" }}</td>
                                            <td>{{ $data->email ?? "- - -" }}</td>
                                            <td>{{ $data->isd_code .''.$data->mobile_number ?? "- - -" }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div id="resume_details"
                                 class="tab-pane @if($active_tab != "" && $active_tab== "resume_details") active show @endif">
                                @if(isset($user->resumeDetails))
                                    @include('user.partials.resume_details')
                                @else
                                    <div class="az-content-label mg-b-5">No Result Found..!!</div>
                                @endif
                            </div>
                            <div id="certificates"
                                 class="tab-pane  @if($active_tab!= "" && $active_tab== "courses") active show @endif">
                                <table id="dataTableList3" class="display responsive" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Course Name</th>
                                        <th>Download</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($certificates as $certificate)
                                        <tr>
                                            <td>{{ $certificate->courseDetail->name ?? "- - -" }}</td>
                                            <td>
                                                @if(isset($certificate->userDetail->candidate_id) || ($certificate->is_international_certificate == 1))
                                                    @if($certificate->courseDetail->course_type != 4)
                                                        @if($certificate->progress > 99)
                                                            <form class="form" method="POST"
                                                                  action="{{route('certificate.download')}}"
                                                                  autocomplete="off">
                                                                @csrf
                                                                <input type="hidden" name="hdn_course_id"
                                                                       value="{{ $certificate->courseDetail->id }}">
                                                                <input type="hidden" name="hdn_user_id"
                                                                       value="{{ $certificate->user_id }}">
                                                                <button type="submit"
                                                                        class="btn btn-primary btn-xs">
                                                                    <i class="fa fa-download" aria-hidden="true"></i>
                                                                </button>
                                                            </form>
                                                        @else
                                                            Pending Course Progress
                                                        @endif
                                                    @else
                                                        @if(new DateTime() < new DateTime(date('Y-m-d H:i:s',strtotime($certificate->courseDetail->complete_date))))
                                                            Webinar In Progress
                                                        @else
                                                            <form class="form" method="POST"
                                                                  action="{{route('certificate.download')}}"
                                                                  autocomplete="off">
                                                                @csrf
                                                                <input type="hidden" name="hdn_course_id"
                                                                       value="{{ $certificate->courseDetail->id }}">
                                                                <input type="hidden" name="hdn_user_id"
                                                                       value="{{ $certificate->user_id }}">
                                                                <button type="submit"
                                                                        class="btn btn-primary btn-xs">
                                                                    <i class="fa fa-download" aria-hidden="true"></i>
                                                                </button>
                                                            </form>
                                                        @endif
                                                    @endif
                                                @else
                                                    Certificate In Progress
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <style>
        .select2-container {
            width: 100% !important;
        }
    </style>
@endsection
@section('footer_scripts')
    <script type="text/javascript" src="{{asset('admin/js/datepicker.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/lib/select2/js/select2.min.js')}}"></script>
    <script type="text/javascript">
        var $document = $(document);
        $document.ready(function () {
            loadingSelect2();
            $('#dataTableList').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: ''
                }
            });
            $('#dataTableList1').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: ''
                }
            });
            $('#dataTableList3').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: ''
                }
            });

            $('.fc-datepicker').datepicker({
                maxDate: new Date,
                showOtherMonths: true,
                selectOtherMonths: true
            });

            $document.on('change', '#country_id', function () {
                $("#state_id").val("").trigger('change');
                $("#city_id").val("").trigger('change');
                $("#district_id").val("").trigger('change');

            });

            $document.on('change', '#state_id', function () {
                $("#city_id").val("").trigger('change');
                $("#district_id").val("").trigger('change');
            });

            $(document).on("change", "input[name='disability']", function () {
                if ($('input:radio[name=disability]:checked').val() == 1) {
                    $("#disabilityTypeSection").show();
                }
                else {
                    $("#disabilityTypeSection").hide();
                }
            });

            $(document).on("change", "input[name='current_occupation']", function () {
                if ($('input:radio[name=current_occupation]:checked').val() == 'Experience') {
                    $(".dependUponExperience").show();
                }
                else {
                    $(".dependUponExperience").hide();
                }
            })

        });
        function loadingSelect2() {
            $("#state_id").select2({
                placeholder: "SELECT",
                ajax: {
                    url: $app_url + "/get-states-by-country",
                    type: "get",
                    dataType: 'json',
                    data: function (params) {
                        var searchTerm = params.term;
                        return {
                            searchTerm: searchTerm,
                            countryId: $("#country_id").val()
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $("#city_id").select2({
                placeholder: "SELECT",
                ajax: {
                    url: $app_url + "/get-cities-by-state",
                    type: "get",
                    dataType: 'json',
                    data: function (params) {
                        var searchTerm = params.term;
                        return {
                            searchTerm: searchTerm,
                            stateId: $("#state_id").val()
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $("#district_id").select2({
                placeholder: "SELECT",
                ajax: {
                    url: $app_url + "/get-district-by-state",
                    type: "get",
                    dataType: 'json',
                    data: function (params) {
                        var searchTerm = params.term;
                        return {
                            searchTerm: searchTerm,
                            stateId: $("#state_id").val()
                        };
                    },
                    processResults: function (response) {
                        return {

                            results: response
                        };
                    },
                    cache: true
                }
            });
        }
    </script>
@endsection
