@extends('layout.master')
@section('content')
<div class="az-content">
    <div class="container">
        <div class="az-content-body">
            <h2 class="az-content-title"> Missing Search Keywords</h2>
            <div id="data-container">
                {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
                <div class="mb-3">
                    <div class="row row-xs">
                        <div class="col-md-5">
                            <label class="az-content-label tx-11 tx-medium tx-gray-600">Search :</label>
                            {!! Form::text('search', null,['id' => 'search', 'class' => 'form-control', 'placeholder' => "Search Keyword"]) !!}
                        </div>
                        <div class="col-md-2 mt-4">
                            <button type="submit" class="btn btn-az-success btn-sm btn-success">Search</button>
                            <button type="button" class="btn btn-az-warning btn-sm btn-warning" id="reset-filters">
                                Reset
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
                <table id="dataTableList" class="display responsive">
                    <thead>
                        <tr>
                            <th>Keywords</th>
                            <th>Attempts</th>
                            <th>Last attempt</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="assignCourseModal"  role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                Assign Course(s)
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            {!! Form::open(['route'=>['missing-search-keywords.update'], 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'search-form']) !!}
            <input name="missingKeyword" type="hidden"  id="missingKeyword"  />
            <input name="missingKeywordId" type="hidden"  id="missingKeywordId"  />
            <div class="pd-20 pd-sm-20 bg-gray-200 wd-xl-100p mb-3">
                <label class="az-content-label tx-11 tx-medium tx-gray-600">Select Course(s) :</label>
                <div class="row row-xs">
                    <div class="col-sm-9">
                        {!! Form::select('course_id[]', $courses, [], ['id' => 'course_id', 'class' => 'form-control select2', 'style' => 'width:20rem', 'multiple'=>'multiple']) !!}
                    </div>
                    <div class="col-sm-2">    
                        <button type="submit" class="btn btn-az-success btn-success">Update</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
@section('footer_scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $.fn.modal.Constructor.prototype.enforceFocus = $.noop;  // To remove extra scrollbar while opening BS modal's dropdown.
        $('#course_id').select2({
            placeholder: "Select Courses",
            dropdownParent: $('#assignCourseModal'),
        });
        if ($('#dataTableList').length) {
            var $dataTableList = $('#dataTableList').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                bFilter: false,
                "aaSorting": [],
                ajax: {
                    url: $app_url + '/missing-search-keywords/getListData',
                    data: function (d) {
                        d.search = $('#search').val().trim();
                    }
                },
                "columns": [
                    {data: 'keyword', 'sortable': true},
                    {data: 'attempts', 'sortable': true},
                    {data: 'updated_at', 'sortable': true},
                    {data: 'actions', 'sortable': false}
                ],
                select: true,
                bStateSave: true,
                fnStateSave: function (settings, data) {
                    localStorage.setItem("dataTables_state", JSON.stringify(data));
                },
                fnStateLoad: function (settings) {
                    return JSON.parse(localStorage.getItem("dataTables_state"));
                }
            });
        }
        $('#search-form').on('submit', function (e) {
            $dataTableList.draw();
            e.preventDefault();
        });

        $('#reset-filters').on('click', function () {
            $("#search").val(null);
            $dataTableList.draw();
        });

        // To get course keyword id from import button to modal's form.
        $('#assignCourseModal').on('show.bs.modal', function (e) {
            var missingKeyword = $(e.relatedTarget).data('missing-keyword');
            var missingKeywordId = $(e.relatedTarget).data('missing-keyword-id');
            $(e.currentTarget).find('input[name="missingKeyword"]').val(missingKeyword);
            $(e.currentTarget).find('input[name="missingKeywordId"]').val(missingKeywordId);
        });
    });
</script>
<script type="text/javascript">
    var $openUploadModal = "{{ session()->pull('openModal') }}";
    if (typeof $openUploadModal !== 'undefined' && $openUploadModal && ($("#assignCourseModal").length > 0)) {
        $("#assignCourseModal").modal('show');
    }
</script>
@endsection



