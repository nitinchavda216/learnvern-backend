<a href="{{ route('missing-keyword-history.index',$item->id) }}" class="btn btn-dark btn-xs">
    <i class="fas fa-eye"></i>
</a>
<a href="javascript:;" class="btn btn-xs btn-primary"
   data-toggle="modal"
   data-target="#assignCourseModal" data-missing-keyword="{{ $item->keyword }}" data-missing-keyword-id="{{ $item->id }}"><i class="fa fa-edit" aria-hidden="true"></i>
</a>
<a href="javascript:;"
   onclick="confirmDelete('{{ route('missing-search-keywords.destroy',$item->id) }}')"
   class="btn btn-danger btn-xs">
    <i class="fas fa-trash"></i>
</a>