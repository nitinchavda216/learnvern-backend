<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Course:</label>
            <small class="text-danger">Required</small>
            {!! Form::select('course_id', $courses, $landing_page->course_id ?? old('course_id'), ['id'=>'courseId', 'class'=>'form-control select2', 'data-validation' => 'required', 'placeholder' => ""]) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Title:</label>
            <small class="text-danger">Required</small>
            {!! Form::text('title', $landing_page->title ?? old('title'), ['class'=>'form-control', 'id' => 'title', 'data-validation' => 'required']) !!}
        </div>
    </div>
</div>
<div class="row row-sm">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Slug:</label>
            <small class="text-danger">Required</small>
            {!! Form::text('slug', $landing_page->slug ?? old('slug'), ['class'=>'form-control', 'id' => 'slug', 'data-validation' => 'required']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="az-content-label tx-11 tx-medium tx-gray-600">Vimeo Video URL:</label>
            <small class="text-danger">Required</small>
            {!! Form::text('video_url', $landing_page->video_url ?? old('video_url'), ['id'=>'videoURL', 'class'=>'form-control', 'data-validation' => 'required']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">Description:</label>
    <small class="text-danger">Required</small>
    {!! Form::text('description', $landing_page->description ?? old('description'), ['class'=>'form-control', 'data-validation' => 'required']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">What you will learn?:</label>
    {!! Form::textarea('what_you_learn', $landing_page->what_you_learn ?? old('what_you_learn'), ['class'=>'form-control wysiwyg_editor', 'id' => 'what_you_learn']) !!}
</div>
<div class="form-group">
    <label class="az-content-label tx-11 tx-medium tx-gray-600">What you will get?:</label>
    {!! Form::textarea('what_you_get', $landing_page->what_you_get ?? old('what_you_get'), ['class'=>'form-control wysiwyg_editor', 'id' => 'what_you_get']) !!}
</div>
<button type="submit" class="btn btn-az-primary mg-md-t-9">{{ isset($landing_page) ? "Update" : "Submit" }}</button>
@section('footer_scripts')
    <script type="text/javascript">
        var is_edit = "{{ isset($landing_page) ? true : false }}";
        $(document).ready(function () {
            /* SET SLUG CONDITIONS */
            if (!is_edit){
                $("#title").focusout(function (i, d) {
                    var title = $.trim($("#title").val());
                    if (title.length > 0) {
                        title = title.toLowerCase();
                        title = title.replace(/[^a-z0-9\s]/gi, "").replace(/  +/g, " ").replace(/[_\s]/g, "-");
                    }
                    $("#slug").val(title);
                });

                $(document).on('change', "#courseId", function () {
                    if (($("#videoURL").val() == "") && ($("#title").val() == "")) {
                        var courseId = $(this).val();
                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: $app_url + '/landing-pages/getCourseDetails/'+courseId,
                            data: {},
                            success: function (data) {
                                if (data.status){
                                    if ($("#videoURL").val() == ""){
                                        $("#videoURL").val(data.course_data.intro_video);
                                    }
                                    if ($("#title").val() == ""){
                                        $("#slug").val(data.course_data.slug);
                                        $("#title").val(data.course_data.name);
                                    }
                                }
                            }
                        });
                    }
                })
            }

            $("#slug").focusout(function (e) {
                var slug = $("#slug").val().toLowerCase();
                $("#slug").val(slug);
            });

            $("#slug").keypress(function (e) {
                var regex = new RegExp("^[A-Za-z0-9-]+$");
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str)) {
                    return true;
                }
                e.preventDefault();
                return false;
            });
        })
    </script>
@endsection
