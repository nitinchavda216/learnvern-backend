@extends('layout.master')
@section('content')
    <div class="az-content">
        <div class="container">
            <div class="az-content-body">
                <h2 class="az-content-title"> Course Landing Pages
                    <span class="pull-right d-inline-block float-right">
                        <a href="{{route('landing-pages.create')}}" class="btn btn-success btn-sm mt-negative"
                           title="Add New">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                            </span>
                </h2>
                <div id="data-container">
                    <table id="dataTableList" class="display responsive">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Course</th>
                            <th>Created Date</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            var $dataTableList = $('#dataTableList').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                bFilter: false,
                "aaSorting": [],
                ajax: {
                    url: $app_url + '/landing-pages/getAjaxListData',
                    data: function (d) {
                    }
                },
                "columns": [
                    {data: 'title', 'sortable': false, 'width': "30%"},
                    {data: 'course_title', 'sortable': false, 'width': "30%"},
                    {data: 'created_at', 'sortable': false, 'width': "20%"},
                    {data: 'actions', 'orderable': false, 'width': "20%"},
                ],
                select: true,
                bStateSave: true,
                fnStateSave: function (settings, data) {
                    localStorage.setItem("dataTables_state", JSON.stringify(data));
                },
                fnStateLoad: function (settings) {
                    return JSON.parse(localStorage.getItem("dataTables_state"));
                }
            });

            $('#search-form').on('submit', function (e) {
                $dataTableList.draw();
                e.preventDefault();
            });

            $('#reset-filters').on('click', function () {
                $dataTableList.draw();
            });
        });
    </script>
@endsection

