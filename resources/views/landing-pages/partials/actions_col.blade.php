<a href="{{ env('FRONT_URL').'lp/'.$item->slug }}"
   class="btn btn-secondary btn-xs" target="_blank">
    <i class="fas fa-eye"></i>
</a>
<a href="{{ route('landing-pages.edit', $item->id) }}"
   class="btn btn-primary btn-xs">
    <i class="fas fa-edit"></i>
</a>
<a href="javascript:;"
   onclick="confirmDelete('{{ route('landing-pages.delete',$item->id) }}')"
   class="btn btn-danger btn-xs">
    <i class="fas fa-trash"></i>
</a>
